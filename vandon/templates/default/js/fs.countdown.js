/**
 * Created by Administrator on 25/06/2017.
 */
(function( $ ){
    $.fn.fsCountdown = function () {
        return this.each(function () {
            var selObject = $(this);

            message = "Đã thông báo";

            var countDownDate = new Date(selObject.attr('data-time')).getTime();

            var countdown = setInterval(function () {

                var now = new Date().getTime();

                var distance = countDownDate - now;

                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                // var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var hours = Math.floor(distance / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                // document.getElementById("demo").innerHTML = days + "d " + hours + "h " + minutes + "m " + seconds + "s ";

                selObject.find('.hours').html(hours);
                selObject.find('.minutes').html(minutes);
                selObject.find('.seconds').html(seconds);

                // console.log(hours + ':' + minutes + ':' + seconds);

                if (distance < 0) {
                    clearInterval(countdown);
                    selObject.html(message);
                }

            }, 1000);
        });
    };
})( jQuery );