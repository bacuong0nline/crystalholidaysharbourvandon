
$(document).ready(function () {

  // console.log($(".logo").attr("href").includes("vi"));

  if (localStorage.getItem("language") == null)
    $(".choose-language-header-box").addClass("d-flex").removeClass("d-none")

  if (localStorage.getItem("language") == null) {
    // $(".logo").attr("href", "/" + localStorage.getItem("language"))

    $(".dropdown-menu-language .dropdown-item").click(function () {
      $("#select-lang").val($(this).attr("data-lang"))
      $(".dropdown-toggle-language").html(($(this).html()))
    })

    $(".continue-language").click(function () {
      localStorage.setItem("language", $("#select-lang").val());
      $(".choose-language-header-box").remove();
      location.href = localStorage.getItem("language")
    })

    $(".close-btn-language").click(function () {
      $(".choose-language-header-box").remove();
    })
    localStorage.setItem("loadpage", 1)

  }

  // if(!window.location.pathname.includes("vi") && !window.location.pathname.includes("en") && localStorage.getItem("language") !== null && localStorage.getItem("loadpage") == 0) {
  //   // console.log(document.URL.includes("vi"))
  //   location.href = localStorage.getItem("language")
  // }


})
