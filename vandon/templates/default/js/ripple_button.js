$(document).ready(function() {
  $(".ripple-btn").click(function(e) {
    let x = e.pageX - $(this).offset().left;
    let y = e.pageY - $(this).offset().top;


    let ripples = $(this).append(`<span class="ripple-effect" style="left: ${x}px; top: ${y}px" ></span>`)

    setTimeout(() => {
      $('.ripple-effect').fadeOut(300, function() {
        $(this).remove()
      })
    }, 700)
  })
})