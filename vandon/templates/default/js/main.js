
$(document).ready(function () {

  // console.log($(".logo").attr("href").includes("vi"));

  $("#nav-icon1").click(function () {
    openMenu();
    // if (!$(this).hasClass("open")) {
    //   closeMenu();
    // }
  })

  $("#wrappper").click(function () {
    closeMenu()
  })

  $("#nav-icon1-2").click(function () {
    closeMenu();
  })
  let height_menu = $(".header-mobile").height();

  $(".navbar-nav .item").click(function () {
    let a = $(this).children().attr("href");
    let target = $(`${a.replace('/', '')}`);
    // $("#nav-icon1").toggleClass("open");
    closeMenu();
    $("html, body").animate({
      scrollTop: target.offset().top - height_menu - 20
    }, 1000)
  })

  // $(document).on('click', '.open', function() {
  // })
  $(".up-to-top").click(function () {
    $("html, body").animate({ scrollTop: 0 }, "slow");
    return false;
  });
  $(".up-page").click(function () {
    $("html, body").animate({ scrollTop: 0 }, "slow");
    return false;
  });
  document.addEventListener("scroll", (event) => {
    if (window.scrollY > 0) {
      $(".up-page").fadeIn().removeClass("d-none")
    } else if (window.scrollY == 0) {
      $(".up-page").fadeOut().addClass("d-none")
    }
  });


  if ($(document).width() > 768) {
    let language = getCookie('language')

    if (language == null)
      $(".choose-language-header-box").addClass("d-flex").removeClass("d-none")

    $(".dropdown-menu-language .dropdown-item").click(function () {
      $("#select-lang").val($(this).attr("data-lang"))
      $(".dropdown-toggle-language").html(($(this).html()))
    })

    $(".continue-language").click(function () {
      setCookie('language', $("#select-lang").val(), 365);
      $(".choose-language-header-box").remove();
    })

    $(".close-btn-language").click(function () {
      $(".choose-language-header-box").remove();
    })
  }

  $(window).scroll(function () {
    scroll = $(window).scrollTop();
    if (scroll > 0) {
      $(".header-mobile").css({ "background-color": "#290f62", "transition": ".3s" });
    } else if (scroll == 0) {
      $(".header-mobile").css({ "transition": ".3s" });
    }
  });

  // $(window).click(function () {
  //   closeMenu();
  // });

  // $('.mobile-nav').click(function (event) {
  //   event.stopPropagation();
  // });

})

$(document).ready(function  () {
  var alert_info = $('#alert_info').val();
  alert_info1 = alert_info ? JSON.parse(alert_info) : [];
  $('.send-contact-modal').click(function () {
    var submit = '#contact-form';
    if (checkFormsubmit3()) {
      $(this).css("pointer-events", "none");
      $("#contact-form-modal").submit();
      $('.send-contact-modal').css("pointer-events", "none")

    }
  })
});

function checkFormsubmit3() {
  $('label.label_error').prev().remove();
  $('label.label_error').remove();
  // email_new = $('#email_new').val();
  if (!notEmpty("fullname-modal", alert_info1[1])) {
      return false;
  }
  if (!notEmpty("phone-modal", alert_info1[2])) {
      return false;
  }

  if (!emailValidator("email-modal", alert_info1[3])) {
      return false;
  }

  else {

  }
  // return false;
  return true;
}



function openMenu() {
  // $("#nav-icon1").toggleClass("open");
  $(".mobile-nav").css({ 'left': '0px', 'transition': '.5s' })
  $(".content-mobile-menu").css({ 'opacity': '1', "pointer-events": "initial", 'transition': '.2s' })
  $("html").css("overflow", "hidden");
}
function closeMenu() {
  $(".mobile-nav").css({ 'left': '-70vw', 'transition': '.5s' })
  $(".content-mobile-menu").css({ 'opacity': '0', "pointer-events": "none", 'transition': '.2s' })
  $("html").css("overflow", "auto");
}
function setCookie(name, value, days) {
  var expires = "";
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
}
function eraseCookie(name) {
  document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}