$("document").ready(function () {
  $(".order").click(function (e) {
    e.preventDefault();

    let id = $(this).attr('data-id');
    let quantity = 1;
    let has_options = $(this).attr('data-options') !== '' || $(this).attr('has-options') !== undefined;
    let options = $(this).attr('data-options');
    let root = $("#url-root").attr("data-root");

    get_detail_product(id, root)
  })

  $(".order-modal").click(function () {
    $("#task-modal").val("order_by_modal");
    $("#cat-modal").val("cat");
    $('#modal-form-product').attr('action', 'index.php?module=products&view=cat&task=order_by_modal')
    $('#modal-form-product').submit();
  })

  $(".register-btn").click(function () {
    if (validateContactEmail())
      $("#register-contact-form").submit();
  })
  $("#email-input").keydown(function (event) {
    if (event.keyCode == 13) {
      event.preventDefault();
      if (validateContactEmail())
        $("#register-contact-form").submit();
    }
  })
  $(".add-to-cart-modal").click(function () {
    let cart_btn = $(".fa-shopping-cart");
    let pos;
    if($("#cartModal-payment").offset()) {
      pos = $("#cartModal-payment").offset().left;
    }
    var btnOffsetLeft = cart_btn.offset().left || pos;
    $(".btn-cart").removeClass("jello-horizontal")
    $(".modal-backdrop").fadeOut();
    $("#modal-product").animate({
      top: 50,
      left: btnOffsetLeft + 10,
      width: 0,
      height: 0,
      opacity: 0.1
    }, 500, function () {
      $(".btn-cart").addClass("jello-horizontal")

    });
    setTimeout(function () {
      $("#task-modal").val("buy2");
      $("#cat-modal").val("product");
      $('#modal-form-product').attr('action', 'index.php?module=products&view=product&task=buy')
      // $('#modal-form-product').submit()

      $.ajax({
        url: "index.php?module=products&view=product&task=buy&raw=1",
        data: $('#modal-form-product').serialize(),
        success: function (data) {
          window.location.reload()
        }
      })
    }, 1000)

    // $.ajax({
    //   url: 'index.php?module=products&view=product&task=buy',
    //   data: 
    // })
  })

  $(".select-options-cart").change(function () {
    let id_prd = $(this).attr("data-id-prd");
    let option = $(this).attr("data-option");
    let order_item = $(this).attr("data-order");

    // select_option = select_option.concat(', ', select_option)

    $.ajax({
      type: 'POST',
      url: `index.php?module=products&view=product&raw=1&task=updateOptionsCart`,
      data: { id_prd, option, order_item, select_option: $(this).val(), options: $(this).attr("data-options") },
      success: function (data) {
        console.log(data);
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert('Có lỗi trong quá trình tải lên máy chủ. Xin bạn vui lòng kiểm tra lại kết nối.');
      }
    });

  })

  $(".btn-options-cart").click(function () {
    let data = $("#options-cart-form").serialize();

    $.ajax({
      type: 'GET',
      url: `index.php?module=products&view=product&raw=1&task=updateOptionCart&${data}`,
      success: function (data) {
        $(".cart-items").html(data);
        $.ajax({
          type: 'GET',
          url: `index.php?module=products&view=product&raw=1&task=updatePrice`,
          success: function (data) {
            let res = JSON.parse(data);
            $('.total-end').html(res.total_end);
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert('Có lỗi trong quá trình tải lên máy chủ. Xin bạn vui lòng kiểm tra lại kết nối.');
          }
        });
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert('Có lỗi trong quá trình tải lên máy chủ. Xin bạn vui lòng kiểm tra lại kết nối.');
      }
    });


  })
})

function validateContactEmail() {
  $('label.label_error').prev().remove();
  $('label.label_error').remove();
  if (!emailValidator(`email-input`, "Email định dạng không đúng")) {
    return false;
  };
  return true;
}

function validate() {
  $('label.label_error').prev().remove();
  $('label.label_error').remove();
  let count = parseInt($("#count-select-modal").val());
  for (let i = 0; i < count; i++) {
    if (!notEmpty(`select-modal-${i}`, "Bạn chưa chọn loại")) {
      return false;
    };
  }
  return true;
}
function get_detail_product(id, root) {
  $.ajax({
    url: root + "index.php?module=products&view=cat&task=ajax_detail_sp&raw=1",
    data: {
      id: id
    },
    dataType: "html",
    success: function (html) {
      $("#modal-product").modal('show');
      $('.modal-body-cat').html(html);
    }
  });
}