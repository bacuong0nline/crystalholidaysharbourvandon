<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Tìm kiếm</h5>
        <select name="" id="" class="select-search ml-3" style="border-right: 1px solid #E9B852">
          <option value="">Sản phẩm</option>
        </select>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="search-form-mobile pb-4" action="" name="search_form" id="search_form_mobile" method="get" onsubmit="javascript: submitSearchMobile();return false;">
          <div class="row no-gutters">
            <?php $link = FSRoute::_('index.php?module=search&view=search'); ?>
            <input id="keyword-mobile" name="keyword" class="search" type="text" placeholder="Bạn tìm kiếm sản phẩm nào?" />
            <input type='hidden' name="module" value="search" />
            <input type='hidden' name="module" id="link_search_mobile" value="<?php echo $link; ?>">
            <input type='hidden' name="view" value="search" />
            <input type='hidden' name="Itemid" value="135" />
          </div>
        </form>
      </div>
    </div>
  </div>
</div>