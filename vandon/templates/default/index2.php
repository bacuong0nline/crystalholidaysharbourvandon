<?php
global $config, $tmpl, $user;
$Itemid = FSInput::get('Itemid', 1, 'int');
$lang = FSInput::get('lang');
$logo = URL_ROOT . $config['logo'];

$tmpl->addScript('search', 'modules/search/assets/js');
$tmpl->addScript('form', 'templates/default/js');
$tmpl->addScript('form1', 'templates/default/js');
$tmpl->addScript('ripple_button', 'templates/default/js');
$tmpl->addScript('hover_hamburger', 'templates/default/js');

$link_info_members = FSRoute::_("index.php?module=members&view=info&Itemid=209");
$link_signin_signup = FSRoute::_("index.php?module=members&view=members&lang=vi");
?>
<?php $isHomePage = FSInput::get('Itemid'); ?>

<?php echo $tmpl->load_direct_blocks('product_menu', array('style' => 'default_mobile')); ?>
<div id="page">
  <?php if ($isHomePage == 1) { ?>
    <header>
      <div class="container header-info">
        <div class="row no-gutters pt-2 d-md-flex justify-content-md-center">
          <div class="col-md-2 show-room d-flex">
            <a href="<?php echo FSRoute::_('index.php?module=contact&Itemid=132&lang=vi') ?>" class="d-flex grey--text">
              <i class="fa fa-map-marker mr-2" aria-hidden="true"></i>
              <p class="no-gutters grey--text">Hệ thống Showroom</p>
              <i class="fa fa-angle-down ml-1 pt-1" aria-hidden="true"></i>
            </a>
          </div>
          <div class="col d-flex thi-cong-trangchu justify-content-center">
            <a href="#" class="d-flex">
              <i class="fa fa-phone mr-2 ml-2 grey--text" aria-hidden="true"></i>
              <p class="no-gutters"><span class="grey--text">Thi công - Dự án: </span><span class="bold">0912 65 33 55</span></p>
            </a>
          </div>
          <div class="col d-flex kinh-doanh-trangchu justify-content-center">
            <a href="#" class="d-flex">
              <i class="fa fa-phone mr-2 ml-2 grey--text" aria-hidden="true"></i>
              <p class="no-gutters"><span class="grey--text">Kinh doanh - Phân phối: </span><span class="bold">083 719 7997</span></p>
            </a>
          </div>
          <div class="col-md-4 d-flex tu-van-trangchu justify-content-center">
            <a href="#" class="d-flex">
              <i class="fa fa-comment mr-2 ml-2 grey--text" aria-hidden="true"></i>
              <p class="no-gutters"><span class="grey--text">Tư vấn & mua hàng trực tuyến: </span><span class="bold">024 667 26 888</span></p>
            </a>
          </div>
          <div class="col-md-1 d-flex thanh-vien d-flex justify-content-lg-end justify-content-md-center">
            <a href="<?php echo !empty($_COOKIE["username"]) ? $link_info_members : $link_signin_signup  ?>" class="d-flex">
              <i class="fa fa-user mr-2 ml-2 grey--text" aria-hidden="true"></i>
              <p class="no-gutters grey--text"><?php echo !empty($_COOKIE["username"]) ? $_COOKIE["username"] : 'Thành viên'?> </p>
            </a>
          </div>
        </div>
      </div>

      <nav class="col-12">
        <div class="container">
          <div class="row no-gutters header-homepage">
            <div class="col-md-2 col-3 logo d-flex align-items-center justify-content-md-center justify-content-xl-start">
              <a href="<?php echo URL_ROOT ?>">
                <img src="/img/footer/logo-trang.svg" src="logo" />
              </a>
            </div>
            <div class="col pl-3 pr-4 hamburger">
              <img src="/img/trangchu/hamburger.png" alt="hamburger">
            </div>
            <div class="col-md-7 col-6 search d-flex align-items-center justify-content-md-center justify-content-lg-center justify-content-xl-start">
              <?php echo $tmpl->load_direct_blocks("search", array("style" => "default")) ?>
            </div>
            <div class="col-md-2 col-1 cart-box d-flex align-items-center justify-content-end">
              <?php echo $tmpl->load_direct_blocks("cart", array("style" => "default")) ?>
              <span class="popup" id="popup"></span>
            </div>
            <!-- hamburger btn mobile !-->
            <div class="col col-1 bars d-flex align-items-center justify-content-center">
              <a href="#navigation-menu">
                <i class="fa fa-bars" aria-hidden="true"></i>
              </a>
            </div>
          </div>
      </nav>
    </header>
    <div class="container banner">
      <div class="row no-gutters">
        <div class="col-md-3 col-xl-3 no-gutters menu">
          <?php echo $tmpl->load_direct_blocks('product_menu', array('style' => 'default')); ?>
        </div>
        <div class="col-md-12 col-lg-12 col-xl-9">
          <?php if ($tmpl->count_block('top_default')) { ?>
            <?php echo $tmpl->load_position('top_default'); ?>
          <?php } ?>
        </div>
      </div>
    </div>

  <?php } else { ?>
    <header>
      <div class="container header-info">
        <div class="row no-gutters pt-2">
          <div class="col-md-2 show-room d-flex">
            <a href="<?php echo FSRoute::_('index.php?module=contact&Itemid=132&lang=vi') ?>" class="d-flex grey--text">
              <i class="fa fa-map-marker mr-2" aria-hidden="true"></i>
              <p class="no-gutters">Hệ thống Showroom</p>
              <i class="fa fa-angle-down ml-1 pt-1" aria-hidden="true"></i>
            </a>
          </div>
          <div class="col d-flex thi-cong justify-content-center">
            <a href="#" class="d-flex">
              <i class="fa fa-phone mr-2 ml-2 grey--text" aria-hidden="true"></i>
              <p class="no-gutters"><span class="grey--text">Thi công - Dự án: </span><span class="bold">0912 65 33 55</span></p>
            </a>
          </div>
          <div class="col d-flex kinh-doanh justify-content-center">
            <a href="#" class="d-flex">
              <i class="fa fa-phone mr-2 ml-2 grey--text" aria-hidden="true"></i>
              <p class="no-gutters"><span class="grey--text">Kinh doanh - Phân phối: </span><span class="bold">083 719 7997</span></p>
            </a>
          </div>
          <div class="col-md-4 d-flex tu-van justify-content-center">
            <a href="#" class="d-flex">
              <i class="fa fa-comment mr-2 ml-2 grey--text" aria-hidden="true"></i>
              <p class="no-gutters"><span class="grey--text">Tổng đài(8-18h): </span><span class="bold">(04) 665 28 661</span></p>
            </a>
          </div>
          <div class="col-md-1 d-flex thanh-vien d-flex justify-content-end">
            <a href="<?php echo !empty($_COOKIE["username"]) ? $link_info_members : $link_signin_signup  ?>" class="d-flex">
              <i class="fa fa-user mr-2 ml-2 grey--text" aria-hidden="true"></i>
              <p class="no-gutters grey--text"><?php echo !empty($_COOKIE["username"]) ? $_COOKIE["username"] : 'Thành viên'?> </p>
            </a>
          </div>
        </div>
      </div>

      <nav class="col-12">
        <div class="container">
          <div class="row no-gutters header-homepage">
            <div class="col-md-2 col-3 logo d-flex align-items-center justify-content-md-center justify-content-xl-start">
              <a href="<?php echo URL_ROOT ?>">
                <img src="/img/footer/logo-trang.svg" src="logo" />
              </a>
            </div>
            <div class="col pl-3 pr-4 hamburger">
              <img src="/img/trangchu/hamburger.png" alt="hamburger">
            </div>
            <div class="col-md-7 col-6 search d-flex align-items-center justify-content-md-center justify-content-lg-center justify-content-xl-start">
              <?php echo $tmpl->load_direct_blocks("search", array("style" => "default")) ?>
            </div>
            <div class="col-md-2 col-1 cart-box d-flex align-items-center justify-content-end">
              <?php echo $tmpl->load_direct_blocks("cart", array("style" => "default")) ?>
              <span class="popup" id="popup"></span>
            </div>
            <!-- hamburger btn mobile !-->
            <div class="col col-1 bars d-flex align-items-center justify-content-center">
              <a href="#navigation-menu">
                <i class="fa fa-bars" aria-hidden="true"></i>
              </a>
            </div>
          </div>
      </nav>
    </header>
    <div class="container banner">
      <div class="row no-gutters">
        <div class="col-md-3 no-gutters menu">
          <?php echo $tmpl->load_direct_blocks('product_menu', array('style' => 'default')); ?>
        </div>
      </div>
    </div>
  <?php } ?>

  <?php echo $main_content ?>

  <?php $tmpl->load_direct_blocks('banners', array('style' => 'banner-footer'));?>
  <?php if ($isHomePage == 1) { ?>
    <footer>
      <div class="row no-gutters mt-2 first-footer">
        <div class="container">
          <div class="row no-gutters">
            <?php echo $config['contact-phone'] ?>
            <div class="col-12 col-md-6 justify-content-start d-flex project-partner call no-gutters justify-content-md-center align-items-center p-2">
              <img alt="call" class="mr-4" src="/img/footer/duan-doitac.svg" />
              <div class="m-2"><a class="title" href="<?php echo FSRoute::_('index.php?module=projectpartner&view=list')?>">Dự án <br/> đối tác</a></div>
            </div>
          </div>
        </div>
      </div>
      <div class="row infomation no-gutters pt-3">
        <div class="container">
          <?php echo $config['info_footer_homepage'] ?>
        </div>
      </div>

      <div class="row no-gutters menu-footer">
        <div class="container d-md-flex align-items-md-center">
          <div class="col d-flex no-gutters">
            <div class="col-md-3 d-flex no-gutters align-items-center copyright">
              <p>Copyright © Suntek Viet Nam 2020.</p>
            </div>
            <div class="col-md-1 col-2 col-sm-1 no-gutters d-flex justify-content-start align-items-center ml-3">
              <a href="index.html">Trang chủ</a>
            </div>
            <div class="col-md-1 col-2 col-sm-1 no-gutters d-flex justify-content-center align-items-center">
              <a href="<?php echo FSRoute::_('index.php?module=introduce&view=home&Itemid=2&lang=vi') ?>">Giới thiệu</a>
            </div>
            <div class="col-md-2 col-3 col-sm-3 no-gutters d-flex justify-content-center align-items-center chinh-sach">
              <a href="<?php echo FSRoute::_('index.php?module=introduce&view=home&Itemid=2&lang=vi') ?>">Chính sách & Điều khoản</a>
            </div>
            <div class="col-md-2 col-2 col-sm-3 no-gutters d-flex justify-content-center align-items-center mua-hang">
              <a href="<?php echo FSRoute::_('index.php?module=policy&view=policy&lang=vi') ?>">Chính sách đại lý</a>
            </div>
            <div class="col-md-1 col-3 col-sm-1 no-gutters d-flex justify-content-center align-items-center tin-tuc">
              <a href="<?php echo FSRoute::_('index.php?module=news&view=home&Itemid=2&lang=vi') ?>">Tin tức</a>
            </div>
            <div class="col-md-1 col-3 col-sm-1 no-gutters d-flex justify-content-center align-items-center hoi-dap">
              <a href="index.php?module=faq&view=home">Hỏi đáp</a>
            </div>
            <div class="col-md-1 no-gutters col-sm-2 d-flex justify-content-end align-items-center social-network">
              <!-- <img src="/img/trangchu/google.png" class="mr-2" alt="google" /> -->
              <a href="<?php echo $config['facebook'] ?>" class="mr-4"><img src="/img/trangchu/fb.png" alt="fb" /></a>
              <a href="<?php echo $config['youtube'] ?>"><img src="/img/trangchu/youtube.png" alt="youtube" /></a>
            </div>
          </div>
        </div>
      </div>
      <!-- menu mobile -->
      <div class="menu-footer-mobile">
        <div class="row no-gutters p-2">
          <div class="col-6 no-gutters d-flex justify-content-start align-items-center">
            <a href="index.html">Trang chủ</a>
          </div>
          <div class="col-6 no-gutters d-flex justify-content-start align-items-center pl-2">
            <a href="<?php echo FSRoute::_('index.php?module=introduce&view=home&Itemid=2&lang=vi') ?>">Giới thiệu</a>
          </div>
        </div>
        <div class="row no-gutters p-2">
          <div class="col-6 no-gutters d-flex justify-content-start align-items-center tin-tuc">
            <a href="<?php echo FSRoute::_('index.php?module=news&view=home&Itemid=2&lang=vi') ?>">Tin tức</a>
          </div>
          <div class="col-6 no-gutters d-flex justify-content-start align-items-center hoi-dap pl-2">
            <a href="index.php?module=faq&view=home">Hỏi đáp</a>
          </div>
        </div>
        <div class="row no-gutters p-2">
          <div class="col-6 no-gutters d-flex justify-content-start align-items-start chinh-sach">
            <a href="<?php echo FSRoute::_('index.php?module=introduce&view=home&Itemid=2&lang=vi') ?>">Chính sách & Điều khoản</a>
          </div>
          <div class="col-6 no-gutters d-flex justify-content-start align-items-center mua-hang pl-2">
            <a href="<?php echo FSRoute::_('index.php?module=policy&view=policy&lang=vi') ?>">Chính sách đại lý</a>
          </div>
        </div>
        <div class="row no-gutters p-2" style="background-color: #333; color: white">
          <div class="col-9 no-gutters align-items-end copyright-mobile">
            <p class="mb-0">Copyright © Suntek Viet Nam 2020.</p>
          </div>
          <div class="col-3 no-gutters  d-flex justify-content-end align-items-center social-network-mobile">
            <!-- <img src="/img/trangchu/google.png" style="height: 80%" class="mr-2" alt="google" /> -->
            <a href="<?php echo $config['facebook'] ?>" class="mr-4"><img src="/img/trangchu/fb.png" alt="fb" /></a>
            <a href="<?php echo $config['youtube'] ?>"><img src="/img/trangchu/youtube.png" alt="youtube" /></a>
          </div>
        </div>
      </div>
      <!--end-->
    </footer>
  <?php } else { ?>
    <footer>
      <div class="row no-gutters mt-2">
        <div class="container">
          <div class="row no-gutters">
            <?php echo $config['contact_footer'] ?>
            <div class="col-12 col-md-6 col-xl-3 d-flex contact no-gutters justify-content-xs-start justify-content-md-start justify-content-xl-center align-items-center p-2">
              <img src="/img/footer/map.svg" alt="contact" class="mr-4" />
              <a href="<?php echo FSRoute::_('index.php?module=contact&view=&Itemid=132&lang=vi') ?>" class="m-2">
                <p class="title">Hệ thống showroom</p>
                <p>Xem hệ thống showroom
                  của Suntek ngay!!!</p>
              </a>
            </div>
            <div class="col-12 col-md-6 col-xl-3 d-flex call no-gutters justify-content-xs-start justify-content-md-start justify-content-xl-center align-items-center p-2">
              <img src="/img/footer/duan-doitac.svg" alt="projectpartner" class="mr-4" />
              <a href="<?php echo FSRoute::_('index.php?module=projectpartner&view=list&lang=vi') ?>" class="m-2">
                <p class="title">Dự án và đối tác</p>
                <p>Xem các Đối tác của Suntek.</p>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="row infomation no-gutters pt-3">
        <?php echo $config['info_footer'] ?>
      </div>
      <div class="row no-gutters menu-footer">
        <div class="container d-md-flex align-items-md-center">
          <div class="col d-flex no-gutters">
            <div class="col-md-3 d-flex no-gutters align-items-center copyright">
              <p>Copyright © Suntek Viet Nam 2020.</p>
            </div>
            <div class="col-md-1 col-2 col-sm-1 no-gutters d-flex justify-content-center align-items-center">
              <a href="index.html">Trang chủ</a>
            </div>
            <div class="col-md-1 col-2 col-sm-1 no-gutters d-flex justify-content-center align-items-center">
              <a href="<?php echo FSRoute::_('index.php?module=introduce&view=home&Itemid=2&lang=vi') ?>">Giới thiệu</a>
            </div>
            <div class="col-md-2 col-3 col-sm-3 no-gutters d-flex justify-content-center align-items-center chinh-sach">
              <a href="<?php echo FSRoute::_('index.php?module=introduce&view=home&Itemid=2&lang=vi') ?>">Chính sách & Điều khoản</a>
            </div>
            <div class="col-md-2 col-3 col-sm-3 no-gutters d-flex justify-content-center align-items-center mua-hang">
              <a href="<?php echo FSRoute::_('index.php?module=policy&view=policy&lang=vi') ?>">Chính sách đại lý</a>
            </div>
            <div class="col-md-1 col-1 col-sm-1 no-gutters d-flex justify-content-center align-items-center tin-tuc">
              <a href="<?php echo FSRoute::_('index.php?module=news&view=home&Itemid=2&lang=vi') ?>">Tin tức</a>
            </div>
            <div class="col-md-1 col-2 col-sm-1 no-gutters d-flex justify-content-center align-items-center hoi-dap">
              <a href="index.php?module=faq&view=home">Hỏi đáp</a>
            </div>
            <div class="col-md-1 no-gutters col-sm-2 d-flex justify-content-end align-items-center social-network">
              <a href="<?php echo $config['facebook'] ?>" class="mr-4"><img src="/img/trangchu/fb.png" alt="fb" /></a>
              <!-- <img src="/img/trangchu/google.png" class="mr-2" alt="google" /> -->
              <a href="<?php echo $config['youtube'] ?>"><img src="/img/trangchu/youtube.png" alt="youtube" /></a>
            </div>
            <div class="row no-gutters menu-footer">
              <div class="col-8 no-gutters align-items-end copyright-mobile">
                <p>Copyright © Suntek Viet Nam 2020.</p>
              </div>
              <div class="col-4 no-gutters  d-flex justify-content-end align-items-center social-network-mobile">
                <!-- <img src="/img/trangchu/google.png" class="mr-2" alt="google" /> -->
                <a href="<?php echo $config['facebook'] ?>" class="mr-4"><img src="/img/trangchu/fb.png" alt="fb" /></a>
                <a href="<?php echo $config['youtube'] ?>"><img src="/img/trangchu/youtube.png" alt="youtube" /></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- menu mobile -->
      <div class="menu-footer-mobile mb-xs-4">
        <div class="row no-gutters p-2">
          <div class="col-6 no-gutters d-flex justify-content-start align-items-center">
            <a href="index.html">Trang chủ</a>
          </div>
          <div class="col-6 no-gutters d-flex justify-content-start align-items-center pl-2">
            <a href="<?php echo FSRoute::_('index.php?module=introduce&view=home&Itemid=2&lang=vi') ?>">Giới thiệu</a>
          </div>
        </div>
        <div class="row no-gutters p-2">
          <div class="col-6 no-gutters d-flex justify-content-start align-items-center tin-tuc">
            <a href="<?php echo FSRoute::_('index.php?module=news&view=home&Itemid=2&lang=vi') ?>">Tin tức</a>
          </div>
          <div class="col-6 no-gutters d-flex justify-content-start align-items-center hoi-dap pl-2">
            <a href="index.php?module=faq&view=home">Hỏi đáp</a>
          </div>
        </div>
        <div class="row no-gutters p-2">
          <div class="col-6 no-gutters d-flex justify-content-start align-items-start chinh-sach">
            <a href="<?php echo FSRoute::_('index.php?module=introduce&view=home&Itemid=2&lang=vi') ?>">Chính sách & Điều khoản</a>
          </div>
          <div class="col-6 no-gutters d-flex justify-content-start align-items-center mua-hang pl-2">
            <a href="<?php echo FSRoute::_('index.php?module=contents&view=content&Itemid=8&lang=vi') ?>">Chính sách đại lý</a>
          </div>
        </div>
        <div class="row no-gutters p-2" style="background-color: #333; color: white">
          <div class="col-9 no-gutters align-items-end copyright-mobile">
            <p class="mb-0">Copyright © Suntek Viet Nam 2020.</p>
          </div>
          <div class="col-3 no-gutters  d-flex justify-content-end align-items-center social-network-mobile">
            <!-- <img src="/img/trangchu/google.png" style="height: 80%" class="mr-2" alt="google" /> -->
            <a href="<?php echo $config['facebook'] ?>" class="mr-4"><img src="/img/trangchu/fb.png" alt="fb" /></a>
            <a href="<?php echo $config['youtube'] ?>"><img src="/img/trangchu/youtube.png" alt="youtube" /></a>
          </div>
        </div>
      </div>
      <!--end-->
    </footer>
  <?php } ?>
</div>

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      xfbml: true,
      version: 'v8.0'
    });
  };

  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>

<!-- Your Chat Plugin code -->
<div class="fb-customerchat" attribution=setup_tool page_id="1736444963332226" theme_color="#561844" logged_in_greeting="Xin chào! tôi có thể giúp gì bạn" logged_out_greeting="Xin chào! tôi có thể giúp gì bạn">
</div>