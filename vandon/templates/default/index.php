<?php
global $config, $tmpl, $user;
$Itemid = FSInput::get('Itemid', 1, 'int');
$lang = FSInput::get('lang');
$logo = URL_ROOT . $config['logo'];

$tmpl->addStylesheet('w3', 'templates/default/css');
$tmpl->addStylesheet('lightgallery', 'templates/default/css');
$tmpl->addStylesheet('lg-video', 'templates/default/css');
$tmpl->addStylesheet('lg-thumbnail', 'templates/default/css');
$tmpl->addStylesheet('lg-zoom', 'templates/default/css');
$tmpl->addStylesheet('fullpage', 'templates/default/js/fullPage/dist');

$tmpl->addScript("fullpage.min", "templates/default/js/fullPage/dist");
$tmpl->addScript("lightgallery.min", "templates/default/js");
$tmpl->addScript("lg-video.min", "templates/default/js");
$tmpl->addScript("lg-thumbnail.min", "templates/default/js");
$tmpl->addScript("lg-zoom.min", "templates/default/js");
$tmpl->addScript("form1", "templates/default/js");
$tmpl->addScript("home1", "templates/default/js");

$alert_info = array(
  0 => FSText::_('Nhập Từ Khóa'),
  1 => FSText::_('Bạn chưa nhập họ và tên'),
  2 => FSText::_('Bạn chưa nhập nội dung'),
  3 => FSText::_('Bạn chưa nhập email'),
  4 => FSText::_('Email không hợp lệ'),
  5 => FSText::_('Bạn chưa nhập số điện thoại'),
  6 => FSText::_('Số điện thoại không hợp lệ'),
  7 => FSText::_('Vui lòng nhập từ'),
  8 => FSText::_('số'),
  9 => FSText::_('đến'),
  10 => FSText::_('Bạn chưa nhập địa chỉ'),
);
?>

<input type="hidden" id="alert_info" value='<?php echo json_encode($alert_info) ?>'>

<header>
  <div class="header-top--white d-flex align-items-center mobile">
    <div class="container d-flex justify-content-between">
      <span class="email-us">
        <img src="/images/EnvelopeOpen.svg" alt="evelopeopen">
        <span>Email us at : info@crhvandon.vn</span>
      </span>
      <span class="d-flex align-items-center hotline-wrapper">
        <span class="me-4" style="height:20px; width: 1px; background-color: #333; display: inline-block"></span>
        <svg class="me-4" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-telephone" viewBox="0 0 16 16">
          <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z" />
        </svg>
        <span>
          <?php echo $config['hotline'] ?>
        </span>
      </span>
    </div>
  </div>
  <div class="header-main desktop">
    <div class="container d-flex align-items-center justify-content-between">
      <div class="logo">
        <a href="<?php echo URL_ROOT ?>">
          <img src="/images/logo.svg" alt="logo">
        </a>
      </div>
      <div class="d-flex align-items-center menu-main-top-wrapper">
        <div class="menu-main-top d-flex align-items-center">
          <?php echo $tmpl->load_direct_blocks('mainmenu', array('style' => 'main_activities2', 'group' => 1)) ?>
        </div>
        <div class="hotline-main d-flex align-items-center">
          <div class="line-2"></div>
          <svg viewBox="0 0 39 49" fill="none" xmlns="http://www.w3.org/2000/svg">
            <mask id="path-2-outside-1_59_4454" maskUnits="userSpaceOnUse" x="4" y="2" width="36" height="45" fill="black">
              <rect fill="white" x="4" y="2" width="36" height="45" />
              <path fill-rule="evenodd" clip-rule="evenodd" d="M31.9948 32.4949C35.6556 29.5628 38 25.0552 38 20C38 11.1634 30.8366 4 22 4C13.1634 4 6 11.1634 6 20C6 28.5007 12.6293 35.4531 21 35.9693V43L32 32.4999L31.9948 32.4949Z" />
            </mask>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M31.9948 32.4949C35.6556 29.5628 38 25.0552 38 20C38 11.1634 30.8366 4 22 4C13.1634 4 6 11.1634 6 20C6 28.5007 12.6293 35.4531 21 35.9693V43L32 32.4999L31.9948 32.4949Z" fill="#8D03BD" />
            <path d="M31.9948 32.4949L31.057 31.3242L29.7187 32.3961L30.9591 33.58L31.9948 32.4949ZM21 35.9693H22.5V34.5589L21.0923 34.4721L21 35.9693ZM21 43H19.5V46.5055L22.0357 44.085L21 43ZM32 32.4999L33.0357 33.585L34.1725 32.4999L33.0357 31.4148L32 32.4999ZM36.5 20C36.5 24.5806 34.3778 28.6645 31.057 31.3242L32.9325 33.6657C36.9335 30.4611 39.5 25.5298 39.5 20H36.5ZM22 5.5C30.0081 5.5 36.5 11.9919 36.5 20H39.5C39.5 10.335 31.665 2.5 22 2.5V5.5ZM7.5 20C7.5 11.9919 13.9919 5.5 22 5.5V2.5C12.335 2.5 4.5 10.335 4.5 20H7.5ZM21.0923 34.4721C13.5077 34.0044 7.5 27.7032 7.5 20H4.5C4.5 29.2982 11.7508 36.9017 20.9077 37.4664L21.0923 34.4721ZM22.5 43V35.9693H19.5V43H22.5ZM30.9643 31.4149L19.9643 41.915L22.0357 44.085L33.0357 33.585L30.9643 31.4149ZM30.9591 33.58L30.9643 33.585L33.0357 31.4148L33.0304 31.4098L30.9591 33.58Z" fill="white" mask="url(#path-2-outside-1_59_4454)" />
            <path d="M30.9994 25.4765V28.1862C31.0005 28.4377 30.9488 28.6867 30.8479 28.9172C30.7469 29.1477 30.5988 29.3546 30.413 29.5247C30.2273 29.6947 30.008 29.8242 29.7693 29.9048C29.5305 29.9854 29.2775 30.0153 29.0265 29.9927C26.2415 29.6907 23.5664 28.7409 21.216 27.2197C19.0293 25.8329 17.1753 23.9827 15.7858 21.8003C14.2563 19.4439 13.3044 16.7611 13.0073 13.9692C12.9847 13.7194 13.0145 13.4676 13.0947 13.23C13.1749 12.9923 13.3038 12.7739 13.4732 12.5887C13.6426 12.4034 13.8489 12.2554 14.0787 12.1541C14.3086 12.0528 14.5571 12.0003 14.8084 12.0001H17.5235C17.9627 11.9958 18.3885 12.151 18.7215 12.4368C19.0545 12.7227 19.2721 13.1196 19.3335 13.5537C19.4481 14.4208 19.6607 15.2723 19.9671 16.0918C20.0888 16.4151 20.1152 16.7664 20.043 17.1042C19.9708 17.442 19.8031 17.752 19.5598 17.9976L18.4104 19.1447C19.6988 21.406 21.5748 23.2784 23.8406 24.5642L24.99 23.4171C25.2361 23.1742 25.5467 23.0069 25.8852 22.9348C26.2236 22.8628 26.5757 22.8891 26.8996 23.0106C27.7207 23.3164 28.5739 23.5285 29.4428 23.6429C29.8824 23.7048 30.2839 23.9258 30.5709 24.2639C30.858 24.6019 31.0105 25.0335 30.9994 25.4765Z" fill="white" />
          </svg>
          <span style="color: #8D03BD; font-weight: 600; padding-left: 5px ">
            <?php echo $config['hotline'] ?>

          </span>
        </div>
      </div>
    </div>
  </div>
  <div class="header-mobile mobile justify-content-between align-items-center">
    <div id="nav-icon1">
      <span></span>
      <span></span>
      <span></span>
    </div>
    <a href="<?php echo URL_ROOT ?>" class="d-flex justify-content-center">
      <img src="<?php echo $config['logo'] ?>" alt="logo">
    </a>
    <a data-bs-toggle="modal" data-bs-target="#staticBackdrop" class="btn-style-1 text-uppercase" style="font-size: 11px">Đăng ký</a>
  </div>
</header>
<div class="mobile">
  <div class="mobile-nav">
    <div id="nav-icon1-2" style="position: absolute; right: 10px; top: 10px">
      <svg width="50" height="50" viewBox="0 0 70 70" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M52.5 17.5L17.5 52.5" stroke="#2C3A61" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M17.5 17.5L52.5 52.5" stroke="#2C3A61" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
      </svg>
    </div>
  </div>
  <div class="content-mobile-menu" style="opacity: 0; pointer-events: none">
    <?php echo $tmpl->load_direct_blocks('mainmenu', array('style' => 'default', 'group' => 1)) ?>
  </div>
</div>

<div id="wrappper">
  <?php echo $main_content ?>

</div>
<footer>
  <div class="container">
    <div class="footer d-flex">
      <div class="left">
        <div class="logo-footer d-flex" style="position: relative">
          <div style="width: 60%;" class="d-flex justify-content-center logo-footer-1">
            <img style="padding: 15px" src="/images/logo-white.svg" alt="logo-white">
          </div>
          <div class="line-3"></div>
          <img class="logo-footer-2" style="width: 30%; padding: 15px; height: 100px; object-fit: contain" src="/images/EVLVANDON-09.png" alt="partner-white">
        </div>
        <div>
          <h5 class="text-white title-american-2 text-center mt-3 harbour-vandon" style="font-size: 19px">CRYSTAL HOLIDAYS HARBOUR VÂN ĐỒN</h5>
          <h6 class="text-center title-american-2 mt-3" style="font-size: 19px; color: #1affff">"The World in the HeriTage"</h6>
        </div>
      </div>
      <div class="right">
        <?php echo $tmpl->load_direct_blocks('mainmenu', array('style' => 'bottom2', 'group' => 2)) ?>
        <div class="d-flex contact-info">
          <div class="text-white section-footer-1" style="width: 50%">
            <p class="fw-semibold">Liên hệ</p>
            <?php echo $config['info_footer'] ?>
          </div>
          <div class="section-footer-2" style="width: 50%">
            <p class="fw-semibold text-white">Follow Us</p>
            <div style="position: relative">
              <input type="text" class="input-footer" placeholder="Địa chỉ email">
              <a class="register-btn">Đăng ký</a>
            </div>
          </div>
        </div>
        <p class="text-white mt-3">(*) Lưu ý: Thông tin và hình ảnh dự án chỉ mang tính chất tương đối và có thể được điều chỉnh theo quyết định của Chủ đầu tư tại từng thời điểm, đảm bảo phù hợp với quy hoạch và thi công thực tế. Các thông tin cam kết chính thức sẽ được quy định cụ thể tại HĐMB.</p>
        <!-- <span class="up-to-top" style="cursor: pointer">
            <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M11 21C16.5228 21 21 16.5228 21 11C21 5.47715 16.5228 1 11 1C5.47715 1 1 5.47715 1 11C1 16.5228 5.47715 21 11 21Z" stroke="white" stroke-linecap="round" stroke-linejoin="round" />
              <path d="M15 11L11 7L7 11" stroke="white" stroke-linecap="round" stroke-linejoin="round" />
              <path d="M11 15V7" stroke="white" stroke-linecap="round" stroke-linejoin="round" />
            </svg>
          </span> -->
      </div>
    </div>
  </div>

</footer>
</body>


<div class="modal fade" id="staticBackdrop" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body p-0 d-flex">
        <div class="left">
          <img src="/images/image-demo-2.jpeg" alt="demo">
        </div>
        <div class="right">
          <h2 class="title-american text-uppercase mb-0 text-white title-form-contact">Đăng ký nhận thông tin dự án</h2>
          <p class="text-white mt-4 mb-2 summary-form-contact" style="font-style: italic; font-weight: 300">Quý Khách hàng quan tâm sản phẩm vui lòng điển thông tin theo biểu mẫu</p>
          <form action="index.php?module=contact&view=contact&task=save_contact2" method="POST" id="contact-form-modal">
            <div>
              <input type="text" id="fullname-modal" class="input-contact" placeholder="Họ và tên" />
            </div>
            <div>
              <input type="text" id="phone-modal" class="input-contact" placeholder="Số điện thoại" />
            </div>
            <div>
              <input type="text" id="email-modal" class="input-contact" placeholder="Email" />
            </div>
            <div>
              <input type="text" id="content-modal" class="input-contact" placeholder="Ghi chú" />
            </div>
            <div class="mt-5">
              <label class="check-box d-flex align-items-center">
                <input type="checkbox" name="filter_new_1" class="filter_new_1 common-selector" value="88">
                <span class="text-white ms-4 fw-semibold">Nhận thông tin, giá bán</span>
                <span class="checkmark"></span>
              </label>
              <label class="check-box mt-4 d-flex align-items-center">
                <input type="checkbox" name="filter_new_1" class="filter_new_1 common-selector" value="88">
                <span class="text-white ms-4 fw-semibold">Tham quan Sa bàn dự án, nhà mẫu</span>
                <span class="checkmark"></span>
              </label>
            </div>
            <a class="btn-style-1 m-auto mt-3 send-contact-modal">Gửi ngay</a>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="staticBackdrop2" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content" style="position: relative;">
      <button type="button" class="btn-close" style="position: absolute; right: 10px; top: 10px; z-index: 10" data-bs-dismiss="modal" aria-label="Close"></button>
      <div class="modal-body">
        <h2 class="title-american text-uppercase mb-0 text-center title-form-contact-modal">ĐẶC QUYỀN DANH GIÁ - XỨNG TẦM TINH HOA</h2>
        <div class="d-md-flex">
          <div class="left">
            <div>
              <p class="fw-semibold">I - Đặc quyền sở hữu kiệt tác giới hạn</p>
              <p>Là một trong những số ít chủ nhân căn hộ có tầm nhìn triệu đô trong lòng một khu nghỉ dưỡng phức hợp hội tụ 8 yếu tố Unique giữa lòng di sản khan hiếm.</p>
              <p class="fw-semibold">II - Đặc quyền thành viên thượng lưu</p>
              <p>Gia nhập Câu lạc bộ nghỉ dưỡng CRH và Câu lạc bộ du thuyền Crystal Holidays Harbour Vân Đồn - nơi gặp gỡ của cộng đồng thượng lưu và giới tinh hoa.</p>
              <p class="fw-semibold">III - Đặc quyền đầu tư siêu phẩm</p>
              <p>Cơ hội sinh lời hấp dẫn với BĐS nghỉ dưỡng ở khu vực có nhiều xung lực mạnh nhất với tiềm năng lợi nhuận bất tận. Nơi đã và đang quy tụ hàng loạt cơ sở hạ tầng quy mô lớn nhất mang tầm vóc quốc tế như: Bến cảng quốc tế Ao Tiên, Sân bay quốc tế, những tuyến đường cao tốc huyết mạch, Casino dành cho người Việt đầu tiên…</p>
            </div>
          </div>
          <div class="right">
            <h2 class="title-american title-modal text-white text-uppercase text-center">LIÊN HỆ NHẬN NGAY BẢNG GIÁ</h2>
            <form action="index.php?module=contact&view=contact&task=save_contact2" method="POST" id="contact-form-modal">
              <div>
                <input type="text" name="fullname" class="input-contact" placeholder="Họ và tên" />
              </div>
              <div>
                <input type="text" name="phone" class="input-contact" placeholder="Số điện thoại" />
              </div>
              <div>
                <input type="text" class="input-contact" placeholder="Email" />
              </div>
              <a class="text-uppercase btn-style-1 m-auto mt-4 send-contact">Đăng ký ngay</a>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div data-bs-toggle="modal" data-bs-target="#staticBackdrop2" class="infomation">
  <span class="text">Chính sách</span>
  <svg width="55" height="55" viewBox="0 0 55 55" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    <g clip-path="url(#clip0_904_163)">
      <circle cx="27.5" cy="27.5" r="27.5" fill="currentColor" />
      <path d="M33.4473 38.3446C33.3154 38.8585 33.2132 39.3051 33.0787 39.7437C33.0491 39.8378 32.9199 39.9334 32.8164 39.9683C31.6689 40.3625 30.5403 40.8495 29.3605 41.1037C27.7261 41.4562 26.0863 41.4373 24.6173 40.4203C23.5371 39.671 23.0784 38.6379 23.1443 37.3384C23.2547 35.147 24.1196 33.1372 24.6402 31.044C24.9456 29.8172 25.2819 28.5849 25.4285 27.3339C25.5805 26.0384 24.9119 25.4196 23.6017 25.5057C22.843 25.5568 22.0951 25.7586 21.2866 25.9012C21.3996 25.4586 21.5153 24.9689 21.6552 24.4873C21.6794 24.4053 21.7951 24.3232 21.8866 24.2909C23.7537 23.6291 25.5872 22.8529 27.6427 22.9766C29.3915 23.0829 31.003 24.1336 31.2734 25.6927C31.4429 26.6666 31.3783 27.7428 31.1537 28.7127C30.6452 30.9028 29.9538 33.0498 29.3807 35.2264C29.2139 35.86 29.1466 36.5286 29.113 37.185C29.0713 38.0124 29.4735 38.4993 30.2793 38.6702C31.2479 38.876 32.1989 38.7603 33.1271 38.428C33.2024 38.4025 33.2845 38.3863 33.4473 38.3446Z" fill="white" />
      <path d="M33.7551 16.9366C33.6972 18.3396 32.9843 19.2436 31.7978 19.8113C30.3558 20.5014 28.5491 20.1382 27.4811 18.9585C26.2623 17.6119 26.5139 15.59 28.042 14.4613C30.0652 12.9654 33.1296 13.9596 33.6717 16.2922C33.7228 16.5155 33.7322 16.7496 33.7551 16.9366Z" fill="white" />
    </g>
    <defs>
      <clipPath id="clip0_904_163">
        <rect width="55" height="55" fill="white" />
      </clipPath>
    </defs>
  </svg>
</div>

<div data-bs-toggle="modal" data-bs-target="#staticBackdrop" class="register-fixed">
  <span class="text">Đăng ký</span>
  <svg width="55" height="55" viewBox="0 0 55 55" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    <circle cx="27.5" cy="27.5" r="27.5" fill="currentColor" />
    <g clip-path="url(#clip0_915_3324)">
      <path d="M25.721 32.3798L23.7149 32.3798C23.2465 32.3798 22.8239 32.5074 22.5177 32.7669C22.2138 33.0242 22.0264 33.4135 22.0264 33.9326C22.0264 34.9707 22.7759 35.5932 23.7149 35.5932L33.0531 35.5932C33.8962 35.5932 34.6799 35.4568 35.3494 35.2786C37.2184 34.7838 38.5024 34.511 40.2161 34.4341C40.849 34.4033 41.4865 34.4297 42.1171 34.5L42.1331 34.5022C42.6814 34.5616 43.2207 34.2867 43.5131 33.7698C43.9929 32.9319 44.5778 31.3769 44.5778 28.8014C44.5778 26.0741 43.8627 24.5961 43.3258 23.8747C43.047 23.4964 42.6289 23.2764 42.1879 23.2764L42.1788 23.2764C41.3585 23.2149 40.9792 23.1555 40.5679 23.0345C39.1148 22.6144 39.0897 22.7508 37.3372 21.5213C36.0028 20.5844 33.0805 20.2127 31.8558 19.4934L27.4027 15.8402C26.7195 15.297 25.7713 15.1276 24.9031 16.1371C24.0988 17.0741 24.6974 17.978 25.0721 18.4047L29.1894 22.555L15.5421 22.555C15.0737 22.555 14.6487 22.7398 14.3379 23.0345C14.0318 23.3314 13.8398 23.7405 13.8398 24.1936C13.8398 25.0778 14.571 25.7992 15.4826 25.8278L25.721 25.8278L25.721 25.83L21.5009 25.8278C20.5619 25.8278 19.801 26.5602 19.801 27.4641C19.801 28.3681 20.5619 29.1005 21.5009 29.1005L25.721 29.1027L22.5611 29.1027C21.622 29.1027 20.8612 29.8351 20.8612 30.739C20.8612 31.1899 21.0531 31.599 21.3593 31.8981C21.6677 32.1929 22.0927 32.3776 22.5634 32.3776L23.7149 32.3776L25.721 32.3776L25.721 32.3798Z" fill="white" />
    </g>
    <defs>
      <clipPath id="clip0_915_3324">
        <rect width="21" height="36" fill="white" transform="matrix(4.37114e-08 1 1 -4.37114e-08 9 15)" />
      </clipPath>
    </defs>
  </svg>
</div>

<div class="up-page d-none">
  <svg width="60" height="60" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
    <g filter="url(#filter0_d_248_4704)">
      <path d="M15 21C20.5228 21 25 16.5228 25 11C25 5.47715 20.5228 1 15 1C9.47715 1 5 5.47715 5 11C5 16.5228 9.47715 21 15 21Z" stroke="white" stroke-linecap="round" stroke-linejoin="round" />
      <path d="M19 11L15 7L11 11" stroke="white" stroke-linecap="round" stroke-linejoin="round" />
      <path d="M15 15V7" stroke="white" stroke-linecap="round" stroke-linejoin="round" />
    </g>
    <defs>
      <filter id="filter0_d_248_4704" x="0.5" y="0.5" width="29" height="29" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix" />
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />
        <feOffset dy="4" />
        <feGaussianBlur stdDeviation="2" />
        <feComposite in2="hardAlpha" operator="out" />
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_248_4704" />
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_248_4704" result="shape" />
      </filter>
    </defs>
  </svg>
</div>