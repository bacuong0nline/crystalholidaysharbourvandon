<?php
/*
 * Huy write
 */
// controller

class NewsControllersHome extends FSControllers
{
	function display()
	{
		// call models
		$model = $this->model;
		$list_cat = $model->get_records('published = 1 and level = 0', FSTable::_("fs_news_categories", 1));

		foreach($list_cat as $item) {
			$item->cat_child = $model->get_records('published = 1 and parent_id = '.$item->id.'', FSTable::_("fs_news_categories", 1));
			$item->news_child = $model->get_records('published = 1 and category_id = '.$item->id.'', FSTable::_('fs_news', 1), '*', 'id desc', '4');
			foreach($item->cat_child as $val) {
				$val->news = $model->get_records('published = 1 and category_id = '.$val->id.'', FSTable::_('fs_news', 1), '*', 'id desc', '12');
			}
		}


		$list_hot = $model->get_list_hot();
		$list_hit = $model->get_list_hit();

		$query_body = $model->set_query_body();
		$list = $model->get_list($query_body);

		$i = 0;
		foreach ($list as $cat) {
			$fstable = FSFactory::getClass('fstable');

			$list[$i]->cat_child = $this->model->get_records('published = 1 and display_category = 1 and parent_id = ' .$cat->id, $fstable->_('fs_news_categories',1), 'name, alias, id', 'ordering asc');
			foreach($cat->cat_child as $item) {
				$item->news = $this->model->get_list_news($item->id);
			}
			$i++;
		}

		// $breadcrumbs = array();
		// $breadcrumbs[] = array(0 => FSText::_('Tin tức'), 1 => '');

		global $tmpl;
		$tmpl->set_seo_special();
		// $tmpl->assign('breadcrumbs', $breadcrumbs);

		// call views			
		include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
	}

	function load_more()
	{
		// call models
		$model = $this->model;
		$list = $model->load_more();
		$limit = FSInput::get('data-limit');
		if ($list) {
			include 'modules/' . $this->module . '/views/' . $this->view . '/loadmore.php';
			if (count($list) < $limit) {
				echo '
                        <script type="text/javascript">
                            $(document).ready(function () {
                                 $(".col-btn-show-more").fadeOut();
                            })
                        </script>
                ';
			}
		} else echo 0;
	}
	function tag() {
		$model = $this->model;
		$where = '';
		if (FSInput::get('tag')) {
            $where .= 'AND tags like "%'.FSInput::get('tag').'%"';
        }
        $fs_table = FSFactory::getClass('fstable');
		// $query_body = $model->set_query_body();
		$query = " FROM " . $fs_table->getTable('fs_news', 1) . "
		WHERE 
					published = 1
					" . $where .
		" ORDER BY created_time ASC, id DESC 
			";
		// echo $where;die;
		$list = $model->get_list($query);

		$total = $model->getTotal($query);
		$pagination = $model->getPagination($total);
		
		include 'modules/' . $this->module . '/views/' . $this->view . '/tag.php';

	}
	function load_right()
	{
		//            $page = FSInput::get('page');
		$next_page = FSInput::get('next_page');
		//            $list_name = FSInput::get('list');
		$cat = FSInput::get('cat');
		$list_news_pro = $this->model->get_list_right($next_page, $cat);
		if ($list_news_pro) {
			include 'modules/' . $this->module . '/views/' . $this->view . '/loadright.php';
		} else echo 0;
	}
}
