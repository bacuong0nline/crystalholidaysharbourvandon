$(document).ready(function () {
    $("#toc").toc({ content: ".goodstudy-box-detail", headings: "h2,h3,h4" });
    if ($("ul#toc li").length > 0) {
        $(".toc-box").addClass("style-toc").removeClass("d-none");
    }
    $('.news-home').owlCarousel({
        loop: true,
        margin: 33,
        nav: true,
        dots: false,
        center: false,
        responsive: {
            0: {
                items: 1.5,
            },
            600: {
                items: 4
            }
        }
    })
})