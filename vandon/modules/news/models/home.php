<?php

class NewsModelsHome extends FSModels {

    function __construct() {
    parent::__construct();
    global $module_config;
    $fstable = FSFactory::getClass('fstable');
    $this->table_news = $fstable->_('fs_news',1);
    $this->table_category = $fstable->_('fs_news_categories',1);
    $this->table_tags = $fstable->_('fs_tags',1);
    $this->limit = 9;
    }


    function set_query_body() {
        $where = "";
        if (FSInput::get('tag')) {
            $where .= 'AND tags like "%'.FSInput::get('tag').'%"';
        }
        $fs_table = FSFactory::getClass('fstable');
        $query = " FROM " . $fs_table->getTable('fs_news', 1) . "
						  WHERE 
                               published = 1
						  	" . $where .
            " ORDER BY created_time DESC, id DESC 
						 ";
        return $query;
    }

    function get_list($query_body) {
        if (!$query_body)
            return;

        global $db;
        $query = " SELECT *";
        $query .= $query_body;
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    function get_list_categories() {
        global $db;
        $query = "SELECT id, name, alias
                  FROM ".$this->table_category."
                  WHERE published = 1
                  ORDER BY id ASC";
        $sql = $db->query($query);
        $result = $db->getObjectList();
        // print_r($result);die;
        return $result;
    }
    function get_list_news($cat_id){
        global $db;
        $query = "  SELECT id, summary, content, alias, category_alias, created_time, title, image
                    FROM " . $this->table_news . "
                    WHERE published = 1 AND category_id_wrapper like '%," . $cat_id . ",%' order by ordering desc, created_time desc limit 12";
        $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }
    function console_log($output, $with_script_tags = true) {
        $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . 
    ');';
        if ($with_script_tags) {
            $js_code = '<script>' . $js_code . '</script>';
        }
        echo $js_code;
    }
    /*
     * return products list in category list.
     * These categories is Children of category_current
     */

    function getTotal($query_body) {
        if (!$query_body)
            return;
        global $db;
        $query = "SELECT count(*)";
        $query .= $query_body;
        $sql = $db->query($query);
        $total = $db->getResult();
        return $total;
    }

    function getPagination($total) {
        FSFactory::include_class('Pagination');
        $pagination = new Pagination($this->limit, $total, $this->page);
        return $pagination;
    }

    function getNews($id) {
        if ($id) {
            $where = " id = '$id' ";
        } else {
            $code = FSInput::get('code');
            if (!$code)
                die('Not exist this url');
            $where = " alias = '$code' ";
        }
        $fs_table = FSFactory::getClass('fstable');
        $query = " SELECT id,title,content,category_id,category_alias, summary,hits, video, is_video,
                        alias, tags, created_time, updated_time,seo_title,image,optimal_seo,
                        seo_keyword,seo_description,news_related,author_id,products_related, is_hot
						FROM " . $fs_table->getTable('fs_news',1) . " 
						WHERE 
						" . $where . " ";
        //print_r($query) ;   
        global $db;
        $sql = $db->query($query);
        $result = $db->getObject();
        return $result;
    }

    function get_list_showhome(){
        global $db;
        $query = "SELECT id,alias,title,image,created_time,author,category_id,category_name,category_alias
                  FROM ".$this->table_news."
                  WHERE published = 1 and show_in_homepage = 1 ORDER by RAND(), id desc, created_time desc";
        $sql = $db->query_limit($query,4);
        $result = $db->getObjectList();
        return $result;
    }

    function get_list_hot(){
        global $db;
        $query = "SELECT id,alias,title,image,created_time
                  FROM ".$this->table_news."
                  WHERE published = 1 and is_hot = 1 order by id desc, created_time desc";
        $sql = $db->query_limit($query,4);
        $result = $db->getObjectList();
        return $result;
    }

    function get_list_hit(){
        global $db;
        $query = "SELECT id,alias,title,image,created_time
                  FROM ".$this->table_news."
                  WHERE published = 1 order by hits desc, created_time desc";
        $sql = $db->query_limit($query,10);
        $result = $db->getObjectList();
        return $result;
    }

    function get_list_tag(){
        global $db;
        $query = "SELECT id,alias,name
                  FROM ".$this->table_tags."
                  WHERE published = 1 order by ordering desc";
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    function get_list_new(){
        global $db;
        $query = "SELECT id,alias,title,image,created_time,author,summary
                  FROM ".$this->table_news."
                  WHERE published = 1 order by id desc, created_time desc";
        $sql = $db->query_limit($query,5);
        $result = $db->getObjectList();
        return $result;
    }

    function get_list_left(){
        global $db;
        $query = "SELECT id,alias,name,image
                  FROM ".$this->table_category."
                  WHERE published = 1 and is_left = 1 order by id desc, created_time desc";
        $sql = $db->query_limit($query,5);
        $result = $db->getObjectList();
        return $result;
    }

    function get_list_item_left($id){
        global $db;
        $query = "SELECT id,alias,title,image,created_time,author,summary
                  FROM ".$this->table_news."
                  WHERE published = 1 and category_id_wrapper like '%,".$id.",%' order by id desc, created_time desc";
        $sql = $db->query_limit($query,3);
        $result = $db->getObjectList();
        return $result;
    }

    function get_list_bottom(){
        global $db;
        $query = "SELECT id,alias,title,image,created_time,author,summary
                  FROM ".$this->table_news."
                  WHERE published = 1 order by rand(), id desc, created_time desc";
        $sql = $db->query_limit($query,5);
        $result = $db->getObjectList();
        return $result;
    }

    function load_more()
    {
        $id = FSInput::get('id');
        $page = FSInput::get('page');
        $limit = FSInput::get('limit');
        $data_cat = FSInput::get('data_cat');

        $where = '';
        if ($data_cat)
            $where = ' AND category_id_wrapper like "%,' . $data_cat . ',%" ';
        global $db;
        $query = 'SELECT  id,alias,title,image,created_time,author,summary
                  FROM ' . $this->table_news . ' 
                  WHERE published = 1 ' . $where . '
                  ORDER BY rand(), ordering DESC, id DESC' ;
        $result = $db->query_limit($query, $limit, $page);
        return $db->getObjectList();

    }


    function get_list_right($page,$cat=null){
        global $db;
        if($cat != 0){
            $query = " SELECT id, alias, image, summary, created_time, title, author
					FROM " . $this->table_news . " 
					WHERE published = 1 and category_id_wrapper like '%,".$cat.",%'
					ORDER BY id desc, created_time DESC
							";
        }else{
            $query = ' SELECT id, alias, image, summary, created_time, title, author
					FROM ' . $this->table_news . ' 
					WHERE published = 1 and is_new_video = 1
					ORDER BY id desc, created_time DESC
							';
        }
        $db->query_limit($query, 5, $page);
        $result = $db->getObjectList();
        return $result;
    }
}

?>