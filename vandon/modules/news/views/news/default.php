<?php
global $tmpl, $config;

// $tmpl->addStylesheet('home', 'modules/news/assets/css');
$tmpl->addStylesheet('detail', 'modules/news/assets/css');
// $tmpl->addStylesheet('tintucchitiet', 'modules/news/assets/css');
// $tmpl->addStylesheet('tintuc', 'modules/news/assets/css');
$tmpl->addScript('jquery.toc', 'modules/news/assets/js');
$tmpl->addScript('home', 'modules/news/assets/js');
$tmpl->addScript('detail', 'modules/news/assets/js');
$tmpl->addStyleSheet("flickity", "templates/default/css");
$tmpl->addScript("flickity", "templates/default/js");


?>
<div class="banner-style-4">
    <?php
        $image = str_replace(['jpeg', 'png', 'jpg'], ['webp', 'webp', 'webp'], $data->image);
        $image = str_replace('original', 'resized', $image);
    ?>
    <img src="<?php echo $data->banner ?>" alt="<?php echo $image ?>" onerror="this.src='<?php echo $data->image ?>'" >
    <div class="container" style="position: relative">
        <div class="title-banner">
            <?php if (FSInput::get("Itemid") == 81) { ?>
                <h2 class="title" style="color: white" class="mb-5">
                    <?php echo FSText::_("Kiến thức") ?>
                </h2>
            <?php } else { ?>
                <h2 class="title-news" style="color: white;" class="mb-5">
                    <?php if (@$cat_name) { ?>
                        <?php echo @$cat_name ?>
                    <?php } else { ?>
                        <?php echo @$item->title_banner ?>
                    <?php } ?>
                </h2>
            <?php } ?>
        </div>
    </div>
</div>
<div class="container">
    <div class="news-detail">
        <div class="left">
            <h2 class="title-news mb-md-3 mt-3" class="">
                <?php echo @$data->title ?>
            </h2>
            <p class="text-center" style="font-style: italic; font-weight: 300"><span>Ngày đăng: <?php echo date('d/m/Y', strtotime($data->created_time)) ?></span></p>
            <div>
                <?php echo $data->content ?>
            </div>
            <div class="addthis_inline_share_toolbox_o4hb"></div>
        </div>
        <div class="right">
            <div class="related-news">
                <p class="fw-bold title-relate text-capitalize text-blue"><?php echo FSText::_("Bài viết mới") ?></p>
                <div class="owl-carousel owl-theme" id="list-news">
                    <?php foreach ($relate_news_list as $val2) {
                        $tag = $this->model->get_record_by_id($val2->category_id, FSTable::_('fs_news_categories', 1))->tag;
                    ?>
                        <?php echo $tmpl->news_item($val2, $tag) ?>
                    <?php } ?>
                </div>
            </div>
            <div class="register">
                <form action="">
                    <p class="text-capitalize  title-relate text-blue fw-semibold">Đăng ký nhận tư vấn</p>
                    <div>
                        <input type="text" class="form-control" placeholder="Họ và tên">
                    </div>
                    <div>
                        <input type="text" class="form-control" placeholder="Số điện thoại">
                    </div>
                    <div>
                        <input type="text" class="form-control" placeholder="Email">
                    </div>
                    <div>
                        <textarea class="form-control" placeholder="Ghi chú thêm"></textarea>
                    </div>
                    <a href="" class="btn-style-1 m-auto">Gửi ngay</a>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-6365db9bc04aa38b"></script>


<script>
    function share_click(width, height, type) {
        var leftPosition, topPosition;
        //Allow for borders.
        leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
        //Allow for title and status bars.
        topPosition = (window.screen.height / 2) - ((height / 2) + 50);
        var windowFeatures = "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";
        u = location.href;
        t = document.title;
        if (type === 'fb') {
            window.open('http://www.facebook.com/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t), 'sharer', windowFeatures);
        }
        if (type === 'tw') {
            window.open('http://twitter.com/intent/tweet?url=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t), 'sharer', windowFeatures);
        }
        return false;
    }
</script>