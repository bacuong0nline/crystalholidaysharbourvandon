<?php if (!empty(@$list_comment)) { ?>
<div style="width: 65%" class="pl-0 pr-0 m-left comment pt-3 pb-3" style="border-top: 1px solid #E5E5E5; border-bottom: 1px solid #E5E5E5">
  <?php foreach ($list_comment as $item) { ?>

    <div class="d-flex align-items-start">
      <img class="" src="/images/no-avatar.png" alt="no-avatar" />
      <div style="margin-left: 35px">
        <p class="mb-1"><span class="font-weight-bold mr-2"><?php echo $item->name ?></span><span><?php echo format_date($item->created_time) ?></span></p>
        <p class="mb-1"><?php echo $item->comment ?></p>
        <div class="action d-flex">
          <a data-id="<?php echo $item->id ?>" class="remove-link mr-4 reply" style="color: #4D68A4">Trả lời</a>
        </div>
      </div>
    </div>
    <?php foreach ($item->childs as $val) { ?>
      <div class="d-flex align-items-start pl-5 p-2 comment-reply">
        <img class="" src="/images/no-avatar.png" alt="no-avatar" />
        <div class="pl-3">
          <p class="mb-1"><span class="font-weight-bold mr-2"><?php echo $val->name ?></span><span><?php echo format_date($val->created_time) ?></span></p>
          <p class="mb-1"><?php echo $val->comment ?></p>
        </div>
      </div>
    <?php } ?>
  <?php } ?>
  <!-- <a class="more-comment">Tải thêm bình luận</a> -->
</div>
<?php } ?>