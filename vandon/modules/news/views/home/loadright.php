<?php foreach ($list_news_pro as $item) {
    $link = FSRoute::_('index.php?module=news&view=news&code='.$item->alias.'&id='.$item->id);
    ?>
    <div class="item-right item-news item-news-product">
        <div class="content-news">
            <a class="c-title" href="<?php echo $link ?>" title="<?php echo $item->title ?>">
                <?php echo $item->title ?>
            </a>
            <div class="news-info">
                <p class="mg-0 author">
                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="14" viewBox="0 0 12 14">
                        <text data-name="" transform="translate(0 12)" fill="#2e6ed5" font-size="14" font-family="FontAwesome"><tspan x="0" y="0"></tspan></text>
                    </svg>
                    <?php echo $item->author ?>
                </p>
                <p class="mg-0 created_time">
                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="14" viewBox="0 0 12 14">
                        <text data-name="" transform="translate(0 12)" fill="#2e6ed5" font-size="14" font-family="FontAwesome"><tspan x="0" y="0"></tspan></text>
                    </svg>
                    <?php echo date('d/m/Y',strtotime($item->created_time)) ?>
                </p>
            </div>

        </div>
        <a class="a_img" href="<?php echo $link ?>" title="<?php echo $item->title ?>">
            <img src="<?php echo URL_ROOT.str_replace('original','resize',$item->image) ?>" alt="<?php echo $item->title ?>" class="img-responsive img_resize">
        </a>

    </div>
<?php } ?>
