<?php
global $tmpl;
$tmpl->addStylesheet('cat', 'modules/news/assets/css');
// $tmpl->addScript('home', 'modules/news/assets/js');
$cr = date_create(date('Y-m-d H:i:s'));

?>

<div>
    <?php echo $tmpl->load_direct_blocks('banners', array('style' => 'style4', 'category_id_image' => 5)) ?>
    <div class="container-7">
        <div class="news-box">
            <div class="grid-section-news">
                <?php foreach ($list as $item) { ?>
                    <a class="news-item" href="<?php echo FSRoute::_("index.php?module=news&view=news&code=$item->alias&id=$item->id") ?>" style="display: block; text-decoration: none" >
                        <img src="<?php echo format_image($item->image) ?>" onerror="this.src = '/images/no-image.png'" alt="image-demo-1">
                        <div class="description-news">
                            <p class="text-uppercase text-yellow mb-0">Tin tức</p>
                            <h3 class="text-blue text-uppercase title-news-item fw-semibold" style="font-weight: 400"><?php echo $item->title?></h3>
                            <p class="summary-news">
                                <?php echo $item->summary?>
                            </p>
                            <span class="text-yellow text-uppercase" style="text-decoration: none" href="">Xem thêm</span>
                        </div>
                        <div class="time-box d-flex flex-column align-items-center">
                            <span class="title-montserrat fw-bold text-center day-time"><?php echo date('d', strtotime($item->created_time)) ?></span>
                            <span class="title-montserrat fw-bold month-time">TH<?php echo date('m', strtotime($item->created_time)) ?></span>
                        </div>
                    </a>
                <?php } ?>

            </div>

        </div>
    </div>

</div>