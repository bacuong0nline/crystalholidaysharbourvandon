<?php
global $tmpl;

$tmpl->addStylesheet('cat', 'modules/news/assets/css');
$tmpl->addTitle($cat->name);

?>


<?php echo $tmpl->load_direct_blocks('banners', array('style' => 'style4', 'category_id_image' => 3)) ?>

<div class="news-wrapper">
  <div class="container">
    <div class="project-wrapper">
      <div class="grid-box">
        <?php foreach ($cat->child as $val2) {
          $tag = $this->model->get_record_by_id($val2->category_id, FSTable::_('fs_news_categories', 1))->tag;
        ?>
          <?php echo $tmpl->news_item($val2, $tag) ?>
        <?php } ?>
      </div>
    </div>
  </div>
  <?php if ($pagination) echo $pagination->showPagination(3); ?>

</div>