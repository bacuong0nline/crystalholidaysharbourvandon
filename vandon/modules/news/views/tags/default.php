
<?php
global $tmpl, $config;
$tmpl->addStylesheet('home', 'modules/news/assets/css');
$tmpl->addTitle($tag->name);
?>

<main>
    <div class="breadcrumbs">
        <div class="container">
            <?php echo $tmpl->load_direct_blocks('breadcrumbs', array('style' => 'simple')); ?>
        </div>
    </div>

    <div class="container">
        <div class="grid-container">
            <div class="grid-left">
                <div class="item-grid">
                    <h1 class="h3_left"><?php echo $tag->name ?></h1>
                    <div class="list-news-cate">
                        <?php foreach ($data as $i=>$item) {
                            $link = FSRoute::_('index.php?module=news&code='.$item->alias.'&id='.$item->id);
                            ?>
                            <div class="item <?php echo $i==0 ? 'item-first' : '' ?>">
                                <a target="_blank" href="<?php echo $link ?>" title="<?php echo $item->title ?>">
                                    <?php if($i == 0) {?>
                                        <img src="<?php echo URL_ROOT.str_replace('/original/','/large/',$item->image) ?>" alt="<?php echo $item->title ?>" class="img-responsive">
                                    <?php } else { ?>
                                        <img src="<?php echo URL_ROOT.str_replace('/original/','/resize/',$item->image) ?>" alt="<?php echo $item->title ?>" class="img-responsive">
                                    <?php } ?>
                                </a>
                                <div class="content">
                                    <a target="_blank" class="name" href="<?php echo $link ?>" title="<?php echo $item->title ?>">
                                        <?php echo $item->title ?>
                                    </a>
                                    <p class="detail-time-hit">
                                        <i class="fa fa-clock-o"></i>
                                        <?php echo date('H:i:s d-m-Y',strtotime($item->created_time)) ?>
                                    </p>
                                    <p class="sum"><?php echo $item->summary ?></p>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <?php if(@$pagination) {?>
                        <?php echo $pagination->showPagination(4);?>
                    <?php } ?>
                </div>
            </div>
            <div class="grid-right block-newslist">
                <?php echo $tmpl->load_direct_blocks("newslist", array("style" => "default")) ?>
            </div>
        </div>
    </div>
</main>
