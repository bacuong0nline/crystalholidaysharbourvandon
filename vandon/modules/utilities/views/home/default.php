<?php
global $tmpl;
$tmpl->addStylesheet('utilities', 'modules/utilities/assets/css');
$tmpl->addStylesheet('fancybox', 'templates/default/css');

// $tmpl->addScript("project", 'modules/project/assets/js');
$tmpl->addScript("home", 'modules/utilities/assets/js');
$tmpl->addScript("fancybox", 'templates/default/js');

?>


<main>
  <?php echo $tmpl->load_direct_blocks('banners', array('style' => 'style6', 'category_id_image' => 2)) ?>
  <?php foreach ($list as $item) { ?>
    <span class="line m-auto mobile"></span>
    <div class="container content">
      <div class="d-flex align-items-center justify-content-center" style="position: relative">
        <span class="line me-2"></span>
        <h4 class="title-american-2 text-cyan text-center title-content">
          <?php echo $item->title ?>
        </h4>
        <span class="line me-2"></span>
      </div>
      <div class="description text-white">
        <?php echo $item->content ?>
      </div>
      <div class="owl-carousel owl-theme carousel">
        <?php foreach ($item->images as $val) { ?>
          <div data-caption="<?php echo $val->title ?>" class="item" data-fancybox="gallery" href="<?php echo format_image($val->image) ?>">
            <img src="<?php echo format_image($val->image) ?>" alt="<?php echo format_image($val->image) ?>">
            <p class="fw-bold mt-3 ms-md-4 title-image"><?php echo $val->title ?></p>
          </div>
        <?php } ?>
      </div>
    </div>
  <?php } ?>
</main>

<script>
  window.onload = function() {
    $('[data-fancybox]').fancybox({
      buttons: [
        "zoom",
        //"share",
        "slideShow",
        "fullScreen",
        "download",
        "thumbs",
        "close"
      ],
      protect: true,
      preventCaptionOverlap: true,
    });
  }
</script>