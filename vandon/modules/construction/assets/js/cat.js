$(document).ready(function () {
    // $('select').select2();

    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    })
})

function changeCity22($city_id,$id){
    $.ajax({
		type : 'get',
		url : 'index.php?module=address&view=address&raw=1&task=loadDistricts',
		dataType : 'html',
		data: {city_id:$city_id},
		success : function(data){
            location.reload();
        },
		error : function(XMLHttpRequest, textStatus, errorThrown) {}
	});
    return false;
}

function changeSelectedCentral($city_id,$id){
    $.ajax({
		type : 'get',
		url : 'index.php?module=address&view=address&raw=1&task=loadSelectedCentral',
		dataType : 'html',
		data: {city_id:$city_id, id: $id},
		success : function(data){
            location.reload();
        },
		error : function(XMLHttpRequest, textStatus, errorThrown) {}
	});
    return false;
}

function removeSelected(){
    $.ajax({
		type : 'get',
		url : 'index.php?module=central&view=central&raw=1&task=removeSelected',
		dataType : 'html',
		success : function(data){
            location.reload();
        },
		error : function(XMLHttpRequest, textStatus, errorThrown) {}
	});
    return false;
}