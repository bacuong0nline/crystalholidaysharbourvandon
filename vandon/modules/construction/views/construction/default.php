<?php
global $tmpl, $config;
$tmpl->addStylesheet('default', 'modules/project/assets/css');
$tmpl->addStylesheet('detail', 'modules/news/assets/css');
$tmpl->addStylesheet('project', 'modules/project/assets/css');

$tmpl->addStyleSheet("flickity", "templates/default/css");
$tmpl->addScript("flickity", "templates/default/js");
$tmpl->addScript('jquery.toc', 'modules/news/assets/js');

$tmpl->addScript('project', 'modules/project/assets/js');
$tmpl->addScript('construction', 'modules/construction/assets/js');

$alert_info = array(
	0 => FSText::_('Nhập Từ Khóa'),
	1 => FSText::_('Bạn chưa nhập họ và tên'),
	2 => FSText::_('Bạn chưa nhập nội dung'),
	3 => FSText::_('Bạn chưa nhập email'),
	4 => FSText::_('Email không hợp lệ'),
	5 => FSText::_('Bạn chưa nhập số điện thoại'),
	6 => FSText::_('Số điện thoại không hợp lệ'),
	7 => FSText::_('Vui lòng nhập từ'),
	8 => FSText::_('số'),
	9 => FSText::_('đến'),
	10 => FSText::_('Bạn chưa nhập địa chỉ'),
);
?>

<input type="hidden" id="alert_info" value='<?php echo json_encode($alert_info) ?>'>


<main class="pb-5">
  <div class="container pt-5 project-detail">
    <p><?php echo $cat_parent->name ?> | <?php echo @$data_city->name ? $data_city->name : $data->address2 ?></p>
    <h1 class="title-prj"><?php echo $data->title ?></h1>
    <div class="d-flex mb-4 mt-4">
      <span style="font-size: 14px;" class="me-2"><?php echo format_date4($data->start_time) ?></span>
      |
      <span class="ms-2"><?php echo $data->hits . ' ' .  FSText::_("lượt xem") ?></span>
    </div>
    <div class="toc-box style-toc mb-5" style="position: relative">

      <p class="font-weight-bold mb-0 fw-bold" style="color: #333">
        <?php echo FSText::_("Mục lục bài chia sẻ") ?>
        <a type="button" data-bs-toggle="collapse" href="#toc" role="button" aria-expanded="false" aria-controls="toc">
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-down" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
          </svg>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-up" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M7.646 4.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1-.708.708L8 5.707l-5.646 5.647a.5.5 0 0 1-.708-.708l6-6z" />
          </svg>
        </a>
      </p>
      <ul id="toc" class="collapse show" style="margin-bottom: 0px">
      </ul>
    </div>
    <?php if (!empty($images)) { ?>
      <div class="container" style="padding-bottom: 20px;">
        <div class="carousel carousel-main" id="animated-thumbnails" data-flickity='{"pageDots": false}'>
          <?php foreach ($images as $item) { ?>
            <a data-lg-size="1600-1067" data-src="<?php echo str_replace('original', 'resized', $item->image) ?>" data-sub-html="" class="carousel-cell">
              <img src="<?php echo str_replace('original', 'resized', $item->image) ?>" alt="<?php echo $item->image ?>">
            </a>
          <?php } ?>
        </div>
        <div class="carousel carousel-nav" data-flickity='{ "asNavFor": ".carousel-main", "contain": true, "pageDots": false, "cellAlign": "left", "prevNextButtons": false }'>
          <?php foreach ($images as $item) { ?>
            <div class="carousel-cell">
              <img src="<?php echo str_replace('original', 'resized', $item->image) ?>" alt="<?php echo $item->image ?>">
            </div>
          <?php } ?>
        </div>
      </div>
    <?php } ?>
    <div>
      <?php echo html_entity_decode($data->summary) ?>
    </div>
  </div>

  <!-- <div class="img-banner">
  <img src="<?php echo str_replace('original', 'resized', $data->image2) ?>" alt="prj1" />
</div> -->

  <div class="container">
    <div class="description" id="description">
      <?php echo $data->content ?>
    </div>
  </div>

  <div class="container">
    <?php if (!empty($data->customer)) { ?>
      <div class="customer">
        <p class="fw-bold"><?php echo $data->customer ?></p>
        <p>
          <svg width="23" height="21" viewBox="0 0 23 21" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M8.48 0.639997L4.96 15.44L4.32 12C5.6 12 6.64 12.4 7.44 13.2C8.24 13.9467 8.64 14.96 8.64 16.24C8.64 17.52 8.24 18.56 7.44 19.36C6.64 20.16 5.65333 20.56 4.48 20.56C3.2 20.56 2.16 20.16 1.36 19.36C0.613334 18.5067 0.24 17.4667 0.24 16.24C0.24 15.76 0.266667 15.3333 0.32 14.96C0.373334 14.5867 0.48 14.1333 0.64 13.6C0.8 13.0667 1.01333 12.4267 1.28 11.68L4.64 0.639997H8.48ZM22.08 0.639997L18.56 15.44L17.92 12C19.2 12 20.24 12.4 21.04 13.2C21.84 13.9467 22.24 14.96 22.24 16.24C22.24 17.52 21.84 18.56 21.04 19.36C20.24 20.16 19.2533 20.56 18.08 20.56C16.8 20.56 15.76 20.16 14.96 19.36C14.2133 18.5067 13.84 17.4667 13.84 16.24C13.84 15.76 13.8667 15.3333 13.92 14.96C13.9733 14.5867 14.08 14.1333 14.24 13.6C14.4 13.0667 14.6133 12.4267 14.88 11.68L18.24 0.639997H22.08Z" fill="#0E0E0E" />
          </svg>
        </p>
        <p><?php echo $data->customer_review ?></p>
      </div>
    <?php } ?>

    <?php if (!empty($questions)) { ?>
      <?php echo $tmpl->list_question($questions) ?>
    <?php } ?>

    <div class="advice mt-5">
      <p style="font-size: 30px" class="fw-bold"><?php echo FSText::_("Bạn cần tư vấn") ?></p>
      <?php echo $config['advice'] ?>
    </div>


    <div class="container d-flex mt-5 mb-3">
      <?php if (!empty($list_tag)) { ?>
        <div class="d-flex col-md-9 flex-wrap">
          <p class="ml-1"><?php echo FSText::_("Chủ đề") ?>:</p>
          <div>
            <?php foreach (@$list_tag as $item) { ?>
              <a href="<?php echo FSText::_("index.php?module=news&view=home&task=tag&tag=$item") ?>" class="tags">
                <?php echo $item ?>
              </a>
            <?php } ?>
          </div>
        </div>
      <?php  } ?>
    </div>

    <div class="binhluan">
      <form action="index.php?module=construction&view=construction&task=set_comment" method="post" id="form-comment" name="form-comment" enctype="multipart/form-data">
        <div class="col-12 mb-4">
          <input type="text" placeholder="<?php echo FSText::_("Nhập tên của bạn") ?>" id="name-guest" name="name-guest" class="name-guest"></textarea>
        </div>
        <div class="col-12">
          <textarea placeholder="<?php echo FSText::_("Nội dung") ?>" id="comment" name="comment" class="your-comment"></textarea>
        </div>
        <input type="hidden" id="id-news" name="id-news" value="<?php echo $data->id ?>" />

        <div class="w-100">
          <div class="col no-gutters d-flex justify-content-center justify-content-md-end align-items-center send-comment">
            <a id="send-comment" class="font-weight-bold"><?php echo FSText::_("Gửi bình luận") ?></a>
          </div>
        </div>
      </form>

      <?php if (!empty($list_cmt)) { ?>
        <?php foreach (@$list_cmt as $item) { ?>
          <div style="background-color: white" class="col-12 pl-4 pr-4 pt-4 ms-5">
            <div class="col ">
              <div class="d-flex">
                <p class="mr-3 user-comment font-weight-bold mb-0 d-flex align-items-center">
                  <img class="mr-1" style="width: 20px!important" src="<?php echo $config['logo'] ?>" alt="logo" />
                  <span class="fw-bold ms-3" style="font-size: 24px"><?php echo $item->name ?></span>
                </p>
              </div>
              <div class="mt-3 ">
                <p class="mb-0 mt-2 mb-3"> <?php echo $item->comment ?></p>
                <p class="mb-0">
                  <span class="me-2">
                    <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M9.23503 0C7.40851 0 5.62302 0.469192 4.10432 1.34824C2.58563 2.22729 1.40196 3.47672 0.702978 4.93853C0.00400136 6.40034 -0.178883 8.00887 0.177453 9.56072C0.533788 11.1126 1.41334 12.538 2.70488 13.6569C3.99642 14.7757 5.64195 15.5376 7.43336 15.8463C9.22478 16.155 11.0816 15.9965 12.7691 15.391C14.4566 14.7855 15.8989 13.7602 16.9137 12.4446C17.9284 11.129 18.4701 9.58225 18.4701 8C18.4627 5.88022 17.4874 3.84906 15.7571 2.35014C14.0268 0.851219 11.6821 0.00633091 9.23503 0V0ZM9.23503 14C7.86514 14 6.52602 13.6481 5.387 12.9888C4.24798 12.3295 3.36022 11.3925 2.83599 10.2961C2.31176 9.19974 2.17459 7.99334 2.44185 6.82946C2.7091 5.66557 3.36876 4.59647 4.33742 3.75736C5.30607 2.91824 6.54022 2.3468 7.88378 2.11529C9.22734 1.88378 10.62 2.0026 11.8856 2.45672C13.1512 2.91085 14.2329 3.67988 14.994 4.66658C15.7551 5.65327 16.1613 6.81331 16.1613 8C16.1558 9.58984 15.4243 11.1132 14.1266 12.2374C12.8288 13.3616 11.0703 13.9953 9.23503 14ZM10.3894 7.6L13.5062 10.3L11.8901 11.7L8.08065 8.4V3H10.3894V7.6Z" fill="#CBD0D3" />
                    </svg>
                  </span>
                  <?php echo format_time_ago(strtotime($item->created_time)) ?>
                </p>
              </div>
            </div>
          </div>
        <?php } ?>
      <?php } ?>
    </div>
    <div class="d-flex mt-5 justify-content-center" style="width: 100%">
      <div class="addthis_inline_share_toolbox_439y"></div>
    </div>

    <div class="advice mt-5">
      <p style="font-size: 30px" class="fw-bold"><?php echo FSText::_("Nhận cập nhật") ?></p>
      <p><?php echo FSText::_("Bài viết mới sẽ được gửi tới email mà bạn đăng ký") ?></p>
      <form>
        <div class="d-flex">
          <div style="width: 80%">
            <input class="email-register" type="text" name="email-register" placeholder="<?php echo FSText::_("Địa chỉ email") ?>" />
          </div>
          <a class="email-register-btn"><?php echo FSText::_("Đăng ký") ?></a>
        </div>
      </form>
    </div>
  </div>
</main>

<?php if (!empty($relate_news_list)) { ?>
  <div class="related-news">
    <p class="fw-bold title-relate"><?php echo FSText::_("Nội dung liên quan") ?></p>
    <div class="project-wrapper">
      <?php foreach ($relate_news_list as $val2) {?>
        <?php echo $tmpl->project_item($val2, $this->model) ?>
      <?php } ?>
    </div>
  </div>
<?php } ?>

<?php if (!empty($relate_news_list_2)) { ?>
  <div class="related-news">
    <p class="fw-bold title-relate"><?php echo FSText::_("Chủ đề khác") ?></p>
    <div class="project-wrapper">
      <?php foreach ($relate_news_list_2 as $val2) {?>
        <?php echo $tmpl->project_item($val2, $this->model) ?>
      <?php } ?>
    </div>
  </div>
<?php } ?>

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-6365db9bc04aa38b"></script>