<?php

class ContactControllersContact extends FSControllers
{

    function display()
    {
        $model = $this->model;

        $submitbt = FSInput::get('submitbt');
        $msg = '';
        $address = $model->get_address_list();

        $list_city_name = array(); //cities select2
        foreach ($address as $item) {
            if (!empty($item->city_id)) {
                $item->city_name = $model->get_record_by_id($item->city_id, 'fs_local_cities')->name;
            }
            if (!empty($item->city_id) && !empty($item->city_name)) {
                $list_city_name[] = array(
                    "id" => $item->city_id,
                    "name" => $item->city_name
                );
            }
        }
        $list_city_name = $this->my_array_unique($list_city_name);
        // print_r($list_city_name);die;
        // $list = $model -> get_address_list2($address[0]->city_id);

        // call views
        include 'modules/' . $this->module . '/views/' . $this->view . '/' . 'default.php';
    }
    function my_array_unique($array, $keep_key_assoc = false)
    {
        $duplicate_keys = array();
        $tmp = array();

        foreach ($array as $key => $val) {
            // convert objects to arrays, in_array() does not support objects
            if (is_object($val))
                $val = (array)$val;

            if (!in_array($val, $tmp))
                $tmp[] = $val;
            else
                $duplicate_keys[] = $key;
        }

        foreach ($duplicate_keys as $key)
            unset($array[$key]);

        return $keep_key_assoc ? $array : array_values($array);
    }

    function save()
    {
        $model = $this->model;

        // $title = FSInput::get('title');
        // $company = FSInput::get('company');
        $name = FSInput::get('fullname');
        $birthday = FSInput::get('birthday');

        $parent = FSInput::get('parent');
        $fcentral = FSInput::get('fcentral');
        if($fcentral == "OSIR") {
            $name_central = "OSIR";
        } else {
            $central = $model->get_record_by_id($fcentral, 'fs_address');
            $name_central = $central->code_crm;
        }
        $email = FSInput::get('email');
        $fstable = FSFactory::getClass('fstable');
        $table_edu = $fstable->_('fs_education_categories', 1);
        $fedu = FSInput::get('fedu');
            if($fedu == 'ielts/toeic') {
                $edu_name = "Other";
            } else if($fedu == 'cambridge') {
                $edu_name = "Other";
            } else {
                $edu_name = $model->get_record_by_id($fedu, $table_edu)->code_crm;
            }
        // $ftarget = FSInput::get('ftarget');
        $content = FSInput::get('content');
        $phone = FSInput::get('fphone');
        $table_central = $fstable->_('fs_address', 1);

        $site_url     = 'ems.vmg.edu.vn'; //Domain truy cập hệ thống EMS
        $api_client = "dotb@ems"; //OAuth Key truy cập API
        $api_secret = "dotb@ems!$%"; //OAuth Password truy cập API
        $api_user   = "admin";  //User tạo ra dữ liệu - Mặc định là user apps_admin

        // $site_url     = 'edu.dotb.cloud'; //Domain truy cập hệ thống EMS
        // $api_client = "key111111"; //OAuth Key truy cập API
        // $api_secret = "sec22222"; //OAuth Password truy cập API
        // $api_user     = "apps_admin";  //User tạo ra dữ liệu - Mặc định là user apps_admin

        $curl_1 = curl_init();
        $options = array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://' . $site_url . '/rest/v11_3/get_api_access_token?key=' . $api_client . '&secret=' . $api_secret,
        );
        curl_setopt_array($curl_1, $options);
        $result = curl_exec($curl_1);
        $token = json_decode($result, 1);
        curl_close($curl_1);

        if($token['success']){
            $data = [
                'name'  => $name,
                'email1' => $email,
                'birthdate' => $birthday,
                'phone_mobile' => $phone, 
                'parent_name1' => $parent, 
                'team_name'         => $name_central, 
                'description'    => $content, 
                'utm_source'     => 'Website',
                'utm_medium'     => '',
                'utm_agent'      => URL_ROOT,
                'type' 				 => 'Lead',
                'api_user' 			 => $api_user,
                'prefer_level' 			 => $edu_name,
                //... Có thể thêm nhiều field
                'access_token' 		 => $token['access_token'],
            ];
            $curl = curl_init();
            $options = array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'https://'.$site_url.'/rest/v11_3/cap_lead_v2',
                CURLOPT_SSL_VERIFYPEER => true,  //CHÚ Ý:  Ko có SSL thì phải tắt SSL đi
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $data
            );
            curl_setopt_array($curl, $options);
            $curl_response = curl_exec($curl);
            print_r($curl_response);
            setRedirect(URL_ROOT, 'Gửi thông tin thành công', 'success');
        }
        else{
            print_r($result);
            setRedirect(URL_ROOT, 'Có lỗi xảy ra, vui lòng thử lại', 'error');
        }

    }
    function save_contact2()
    {
        $model = $this->model;

        // $title = FSInput::get('title');
        // $company = FSInput::get('company');
        $name = FSInput::get('form1-fullname');
        $phone = FSInput::get('form1-phone');
        $email = FSInput::get('form1-email');
        $content = FSInput::get('form1-content');
        $topic = FSInput::get('form1-topic');


        $rs = $this->sendMailFS2($name, $phone, $content);

        if ($rs == 1) {
            $row = array();
            // $row['title'] = $title;
            $row['name'] = $name;
            $row['phone'] = $phone;
            $row['email'] = $email;
            $row['topic'] = $topic;
            $row['content'] = $content;
            $row['created_time'] = date('Y-m-d H:i:s');
            $row['published'] = 1;

            $model->_add($row, 'fs_send_contact');
            setRedirect(FSRoute::_('index.php?module=contact&view=default'), 'Cảm ơn bạn đã gửi thông tin liên hệ!', 'success');
        } else {
            setRedirect(URL_ROOT, 'Lỗi', 'error');
        }
    }
    function sendMailFS2($name, $phone, $content)
    {
        global $config, $tmpl;

        $email_bcc = $config['admin_email'];
        require_once(PATH_BASE . 'libraries/PHPMailer_v5.1/class.phpmailer.php');
        $smtpHost = 'smtp.gmail.com';
        $smtpPort = '465';
        $smtpEmail = 'noreply.hr@vmg.edu.vn';
        $smtpPass = 'HRloveVMG123!@';
        $nFrom = "Admin: " . $_SERVER['SERVER_NAME'];
        $mail = new PHPMailer();
        $body = '';
        $body .= '<p align="left">Bạn nhận được thông tin liên hệ từ ' . $name . '</p>';
        // $body .= '<p align="left">Tiêu đề: <span>' . $title . '</span></p>';
        $body .= '<p align="left">Tên: <span>' . $name . '</span></p>';
        // $body .= '<p align="left">Tên công ty: <span>' . $company . '</span></p>';
        $body .= '<p align="left">Số điện thoại: <span>' . $phone . '</span></p>';
        $body .= '<p align="left">Nội dung liên hệ: <span>' . $content . '</span></p>';

        $mail->IsSMTP();
        $mail->CharSet = "utf-8";
        $mail->SMTPDebug = 0;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = "ssl";
        $mail->Host = $smtpHost;
        $mail->Port = $smtpPort;
        $mail->Username = $smtpEmail;
        $mail->Password = $smtpPass;
        $mail->FromName = "Trung tâm anh ngữ Việt Mỹ";

        $mail->AddBCC($email_bcc);

        $mail->Subject = 'Bạn nhận được thông tin liên hệ từ ' . $name . '';
        $mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
        $mail->MsgHTML($body);

        if (!$mail->Send()) {
            return 0;
        } else {
            return 1;
        }
    }
    function sendMailFS($title, $company, $name, $phone, $email, $content)
    {
        global $config, $tmpl;

        $email_bcc = $config['admin_email'];
        require_once(PATH_BASE . 'libraries/PHPMailer_v5.1/class.phpmailer.php');
        $smtpHost = 'smtp.gmail.com';
        $smtpPort = '465';
        $smtpEmail = 'hotrocvb@gmail.com';
        $smtpPass = 'hotrocvb@123';
        $nFrom = "Admin: " . $_SERVER['SERVER_NAME'];
        $mail = new PHPMailer();
        $body = '';
        $body .= '<p align="left">Bạn nhận được thông tin liên hệ từ ' . $name . '</p>';
        // $body .= '<p align="left">Tiêu đề: <span>' . $title . '</span></p>';
        $body .= '<p align="left">Tên: <span>' . $name . '</span></p>';
        // $body .= '<p align="left">Tên công ty: <span>' . $company . '</span></p>';
        $body .= '<p align="left">Số điện thoại: <span>' . $phone . '</span></p>';
        $body .= '<p align="left">Địa chỉ Email: <span>' . $email . '</span></p>';
        $body .= '<p align="left">Nội dung liên hệ: <span>' . $content . '</span></p>';

        //$body             = $content2;
        $mail->IsSMTP();
        $mail->CharSet = "utf-8";
        $mail->SMTPDebug = 0;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = "ssl";
        $mail->Host = $smtpHost;
        $mail->Port = $smtpPort;
        $mail->Username = $smtpEmail;
        $mail->Password = $smtpPass;
        $mail->FromName = "CÔNG TY TNHH SUNTEK VIỆT NAM";

        $mail->AddBCC($email_bcc);

        $mail->Subject = 'Bạn nhận được thông tin liên hệ từ ' . $name . '';
        $mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
        $mail->MsgHTML($body);
        // $mail->AddAddress($email, $name);

        if (!$mail->Send()) {
            return 0;
        } else {
            return 1;
        }
    }

    function save_mail()
    {

        $model = $this->model;

        $email = FSInput::get('email');

        $rs = $this->sendMailRegister($email);
        if ($rs == 1) {
            $row = array();
            $row['email'] = $email;
            $row['created_time'] = date('Y-m-d H:i:s');
            $row['published'] = 1;
            $model->_add($row, 'fs_send_contact');
            setRedirect(URL_ROOT, 'Cảm ơn bạn đã gửi email đăng ký!', 'success');
        } else {
            setRedirect(URL_ROOT, 'Lỗi', 'error');
        }
        //        }
    }

    function sendMailRegister($email)
    {
        global $config, $tmpl;

        $email_bcc = $config['admin_email'];
        require_once(PATH_BASE . 'libraries/PHPMailer_v5.1/class.phpmailer.php');
        $smtpHost = 'smtp.gmail.com';
        $smtpPort = '465';
        $smtpEmail = 'mail.finalstyle@gmail.com';
        $smtpPass = 'fs123456';
        $nFrom = "Admin: " . $_SERVER['SERVER_NAME'];
        $mail = new PHPMailer();
        $body = '';
        $body .= '<p align="left">Bạn nhận email đăng ký nhận tin từ ' . $email . '</p>';
        $body .= '<p align="left">Địa chỉ Email: <span>' . $email . '</span></p>';

        //$body             = $content2;
        $mail->IsSMTP();
        $mail->CharSet = "utf-8";
        $mail->SMTPDebug = 0;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = "ssl";
        $mail->Host = $smtpHost;
        $mail->Port = $smtpPort;
        $mail->Username = $smtpEmail;
        $mail->Password = $smtpPass;
        $mail->FromName = "Công ty CP SX và TM Bắc Thăng Long";

        $mail->AddBCC($email_bcc);
        $mail->Subject = 'Bạn nhận được email đăng ký nhận tin từ ' . $email . '';
        $mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
        $mail->MsgHTML($body);

        if (!$mail->Send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
            die;
        } else {
            echo "Message has been sent successfully";
        }
    }

    function check_captcha()
    {
        $captcha = FSInput::get('txtCaptcha');

        if ($captcha == $_SESSION["security_code"]) {
            return true;
        } else {
        }
        return false;
    }

    function getmap()
    {
        $id = FSInput::get("id");
        $model = new ContactModelsContact();
        $list = $model->get_address_list2($id);
        $latitude = $list[0]->latitude;
        $longitude = $list[0]->longitude;
        $address = $model->get_address_list();
        $list_city_name = array();
        foreach ($address as $item) {
            if (!empty($item->city_id)) {
                $item->city_name = $model->get_record_by_id($item->city_id, 'fs_local_cities')->name;
            }
            if (!empty($item->city_id) && !empty($item->city_name)) {
                $list_city_name[] = array(
                    "id" => $item->city_id,
                    "name" => $item->city_name
                );
            }
        }
        $list_city_name = $this->my_array_unique($list_city_name);

        include 'modules/' . $this->module . '/views/' . $this->view . '/' . 'filter_city.php';
    }
}
