<?php
global $config, $tmpl;

$tmpl->addStylesheet('contact', 'modules/contact/assets/css');
$tmpl->addScript('form', 'modules/contact/assets/js');
$tmpl->addScript('contact', 'modules/contact/assets/js');
// $tmpl->addStylesheet('gioithieu', 'modules/introduce/assets/css');


$tmpl->addTitle(FSText::_('Liên hệ'));
$alert_info = array(
	0 => FSText::_('Nhập Từ Khóa'),
	1 => FSText::_('Bạn chưa nhập họ và tên'),
	2 => FSText::_('Bạn chưa nhập nội dung'),
	3 => FSText::_('Bạn chưa nhập email'),
	4 => FSText::_('Email không hợp lệ'),
	5 => FSText::_('Bạn chưa nhập số điện thoại'),
	6 => FSText::_('Số điện thoại không hợp lệ'),
	7 => FSText::_('Vui lòng nhập từ'),
	8 => FSText::_('số'),
	9 => FSText::_('đến'),
	10 => FSText::_('Bạn chưa nhập địa chỉ'),
);
?>

<?php echo $tmpl->load_direct_blocks('banners', array('style' => 'style4', 'category_id_image' => 7)) ?>

<input type="hidden" id="alert_info" value='<?php echo json_encode($alert_info) ?>'>
<div class="content">
	<div class="container-2">
		<div class="block-1">
			<div class="left">
				<iframe style="max-height: 600px" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12952.187270222372!2d107.451103!3d21.081273!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x314b059002a5db9d%3A0x7432660df7063a63!2sCrystal%20Holidays%20Harbour%20Van%20Don!5e1!3m2!1sen!2s!4v1672988306041!5m2!1sen!2s" width="600" height="600" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
			</div>
			<div class="right">
				<div class="d-md-flex">
					<div class="logo-contact">
						<img src="/images/logo.svg" alt="logo">
					</div>
					<div>
						<div class="d-flex" style="padding-right: 20px"><svg fill="none" height="19" viewbox="0 0 16 22" width="30" xmlns="http://www.w3.org/2000/svg">
								<path d="M6.82274 21.5057C4.7066 18.7399 0 12.0366 0 8.27141C0 3.70318 3.48475 0 7.78352 0C12.0807 0 15.567 3.70318 15.567 8.27141C15.567 12.0366 10.824 18.7399 8.7443 21.5057C8.24567 22.1648 7.32137 22.1648 6.82274 21.5057ZM7.78352 11.0285C9.21455 11.0285 10.378 9.79214 10.378 8.27141C10.378 6.75067 9.21455 5.51427 7.78352 5.51427C6.35249 5.51427 5.18901 6.75067 5.18901 8.27141C5.18901 9.79214 6.35249 11.0285 7.78352 11.0285Z" fill="#DB801A"></path>
							</svg> <span class="ms-2">KĐT Du lịch và Bến cảng cao cấp Ao Tiên, Huyện Vân Đồn, Tỉnh Quảng Ninh</span></div>
						<div class="d-flex mt-2"><svg fill="none" height="19" viewbox="0 0 20 19" width="20" xmlns="http://www.w3.org/2000/svg">
								<path d="M1.78043 3.00218L4.16964 0.548787C4.33818 0.374797 4.5385 0.236748 4.7591 0.14257C4.9797 0.0483923 5.21623 -5.88438e-05 5.45511 5.36324e-08C5.94192 5.36324e-08 6.39958 0.195995 6.74282 0.548787L9.31375 3.19126C9.48303 3.36449 9.61734 3.57039 9.70896 3.79713C9.80059 4.02387 9.84773 4.26698 9.84767 4.5125C9.84767 5.01286 9.65699 5.48325 9.31375 5.83604L7.43378 7.76833C7.87384 8.76527 8.48566 9.67222 9.23972 10.4454C9.99188 11.2223 10.8742 11.8534 11.8443 12.3085L13.7243 10.3762C13.8928 10.2022 14.0931 10.0642 14.3137 9.97C14.5343 9.87582 14.7709 9.82737 15.0097 9.82743C15.4965 9.82743 15.9542 10.0234 16.2974 10.3762L18.8706 13.0164C19.0401 13.1899 19.1745 13.3962 19.2662 13.6234C19.3578 13.8505 19.4048 14.094 19.4045 14.3399C19.4045 14.8403 19.2139 15.3107 18.8706 15.6635L16.4859 18.1146C15.9385 18.6795 15.1825 19 14.4063 19C14.2425 19 14.0854 18.9862 13.9307 18.9585C10.9066 18.4466 7.90714 16.7933 5.48651 14.3076C3.06813 11.8243 1.46186 8.74369 0.957099 5.62621C0.804549 4.67391 1.11189 3.69393 1.78043 3.00218Z" fill="#DB801A"></path>
							</svg> <span class="ms-2">0938 090 666</span>
						</div>
					</div>
				</div>
				<div class="pt-4 logo-partner-wrapper">
					<h3 class="title-contact">ĐẠI LÝ PHÂN PHỐI CHÍNH THỨC</h3>
					<div class="logo-authorized-deadler mb-4">
						<?php foreach ($list_agency as $item) { ?>
							<?php foreach ($item->images as $val) { ?>
								<div class="logo-contact p-0">
									<img src="<?php echo $val->image ?>" alt="<?php echo $val->image ?>">
								</div>
							<?php } ?>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<div class="block-2">
			<div class="left">
				<h2 class="title-american text-uppercase mb-0 text-white title-form-contact">Đăng ký nhận thông tin dự án</h2>
				<p class="text-white mt-4 mb-4 summary-form-contact" style="font-style: italic; font-weight: 300">Quý Khách hàng quan tâm sản phẩm vui lòng điển thông tin theo biểu mẫu</p>
				<form id="contact-form" action="index.php?module=contact&view=contact&task=save_contact2" method="POST">
					<div>
						<input type="text" id="fullname" name="fullname" class="input-contact" placeholder="Họ và tên" />
					</div>
					<div>
						<input type="text" id="phone" name="phone" class="input-contact" placeholder="Số điện thoại" />
					</div>
					<div>
						<input type="text" id="email" name="email" class="input-contact" placeholder="Email" />
					</div>
					<div>
						<input type="text" id="content" name="content" class="input-contact" placeholder="Ghi chú" />
					</div>
					<div class="mt-5">
						<label class="check-box d-flex align-items-center">
							<input type="checkbox" name="info" class="filter_new_1 common-selector" value="88">
							<span class="text-white ms-4 fw-semibold">Nhận thông tin, giá bán</span>
							<span class="checkmark"></span>
						</label>
						<label class="check-box mt-4 d-flex align-items-center">
							<input type="checkbox" name="visit" class="filter_new_1 common-selector" value="88">
							<span class="text-white ms-4 fw-semibold">Tham quan Sa bàn dự án, nhà mẫu</span>
							<span class="checkmark"></span>
						</label>
					</div>
					<a class="btn-style-1 m-auto mt-5 send-contact">Gửi ngay</a>
				</form>
			</div>
			<div class="right">
				<img src="/images/service.png" alt="service" />
			</div>
		</div>
		<div class="des-bottom" style="padding: 80px 0px 80px 0px">
			<p class="text-center mb-0 text-white" style="margin-top: 50px;">(*) Trong quá trình triển khai dự án, có thể một số thông tin/ thiết kế sẽ thay đổi cho phù hợp với lợi ích chung của khách hàng.</p>
			<p class="text-center mb-0 text-white">Hình ảnh mang tính chất minh hoạ.</p>
		</div>
	</div>
</div>