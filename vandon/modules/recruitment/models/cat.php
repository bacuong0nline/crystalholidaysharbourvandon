<?php

class RecruitmentModelsCat extends FSModels
{
    function __construct()
    {
        $limit = 9;
        $page = FSInput::get('page');
        $this->limit = $limit;
        $this->page = $page;
        $fstable = FSFactory::getClass('fstable');
        $this->table_name = $fstable->_('fs_recruitment', 1);
        $this->table_category = $fstable->_('fs_recruitment_categories', 1);
        $this->table_position = $fstable->_('fs_recruitment_position', 1);
    }

    function get_category_by_id($category_id)
    {
        if (!$category_id)
            return "";
        $query = " SELECT id,name,name_display,is_comment, alias, display_tags,display_title,display_sharing,display_comment,
                        display_category,display_created_time,display_related,updated_time,display_summary,products_related
						FROM " . $this->table_category . "  
						WHERE id = $category_id ";
        global $db;
        $sql = $db->query($query);
        $result = $db->getObject();
        return $result;
    }

    function getPosition() {
        global $db;
        $fs_table = FSFactory::getClass('fstable');
        $query = " SELECT *
                    FROM " . $this->table_position . " 
                    WHERE published = 1 ORDER BY created_time DESC";
        //print_r($query);
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }
    function get_jobs($where = '') {
        $fs_table = FSFactory::getClass('fstable');
        $date = date('Y-m-d');
        $query = " SELECT * FROM " . $fs_table->getTable('fs_recruitments', 1) . " where  published_date > '$date'  $where ";
        global $db;
        $sql = $db->query_limit($query, $this->limit, $this->page);
        $result = $db->getObjectList();
        return $result;
    }
	function list_province()
	{
		global $db;
		$where = '';

		$query  = " SELECT t.id, t.name, t.alias
						FROM fs_cities as t, fs_recruitments as p
						WHERE p.province_id = t.id $where
						GROUP BY p.province_id 
						ORDER BY name ASC
						";
		$db->query($query);
        // echo $query;die;
		$result = $db->getObjectList();
		return $result;
	}


    function get_list()
    {
        global $db;
        $query = " SELECT id, title, created_time, image, summary, alias
						FROM " . $this->table_name . "
						WHERE published = 1 AND is_hot = 1
						ORDER BY created_time DESC LIMIT 3
						";
        $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    function getListCategory() {
        global $db;
        $query = " SELECT id, name, alias
						FROM ".$this->table_category."
						WHERE published = 1
						";
        $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }
    function getTotal2($where)
    {
        global $db;
        $fs_table = FSFactory::getClass('fstable');
        $query = "SELECT count(*) FROM " . $fs_table->getTable('fs_recruitments', 1) . " where 1 = 1 $where ";
        // print_r($query);die;
        $sql = $db->query($query);
        $total = $db->getResult();
        return $total;
    }

    function getPagination($total)
    {
        FSFactory::include_class('Pagination');
        $pagination = new Pagination($this->limit, $total, $this->page);
        return $pagination;
    }
    function getListSimilar($id) {
        global $db;
        $query = " SELECT id, alias, title, address, salary, numb, event_end
						FROM ".$this->table_name."
						WHERE published = 1 AND category_id = $id ORDER BY created_time DESC LIMIT 3 
						";
        $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

}

?>