<?php

class RecruitmentModelsHome extends FSModels {

    function __construct() {
        parent::__construct();
        global $module_config;
//        FSFactory::include_class('parameters');
//        $current_parameters = new Parameters($module_config->params);
//        $limit = $current_parameters->getParams('limit');
        //$limit = 6;
        $fstable = FSFactory::getClass('fstable');
        $this->table_province = $fstable->_('fs_cities', 1);
        $this->table_position = $fstable->_('fs_recruitment_position', 1);
        $this->table_salary = $fstable->_('fs_salary', 1);

        $this->limit;
    }

    /*
     * select cat list is children of catid
     */

    function set_query_body() {
        $date1 = FSInput::get("date_search");
        $where = "";
        $fs_table = FSFactory::getClass('fstable');
        $query = " FROM " . $fs_table->getTable('fs_contents',1) . "
						  WHERE 
						  	 published = 1
						  	" . $where .
            " ORDER BY id asc
						 ";

        return $query;
    }
    function getProvince()
    {
      global $db;
      $query = "SELECT *
                FROM " . $this->table_province . "
                ORDER BY ordering ASC
      ";
      $db->query($query);
      $result = $db->getObjectList();
      return $result;
    }
    function get_list() {
        global $db;
        $fs_table = FSFactory::getClass('fstable');
        $query = " SELECT id, title, alias, content, created_time, published, category_id
						FROM " . $fs_table->getTable('fs_contents') . " 
						WHERE published = 1 AND category_id = 13 ORDER BY created_time DESC
						 ";
        //print_r($query);
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    /*
     * return products list in category list.
     * These categories is Children of category_current
     */

    function getTotal($query_body) {
        if (!$query_body)
            return;
        global $db;
        $query = "SELECT count(*)";
        $query .= $query_body;
        $sql = $db->query($query);
        $total = $db->getResult();
        return $total;
    }
    function countJobs() {
        global $db;
        $query = "SELECT count(*)";
        $query .= " from "  . FSTable::_("fs_recruitments", 1) . " where 1 = 1";
        $sql = $db->query($query);
        $total = $db->getResult();
        return $total;
    }
    function getPagination($total) {
        FSFactory::include_class('Pagination');
        $pagination = new Pagination($this->limit, $total, $this->page);
        return $pagination;
    }

    function getPosition() {
        global $db;
        $fs_table = FSFactory::getClass('fstable');
        $query = " SELECT *
                    FROM " . $this->table_position . " 
                    WHERE published = 1 ORDER BY created_time DESC";
        //print_r($query);
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }
    function getSalary() {
        global $db;
        $fs_table = FSFactory::getClass('fstable');
        $query = " SELECT *
                    FROM " . $this->table_salary . " 
                    WHERE published = 1 ORDER BY created_time ASC";
        //print_r($query);
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }
    
}

?>