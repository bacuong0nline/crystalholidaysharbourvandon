<?php

class RecruitmentModelsRecruitment extends FSModels
{
    function __construct()
    {
        $limit = 10;
        $page = FSInput::get('page');
        $this->limit = $limit;
        $this->page = $page;
        $fstable = FSFactory::getClass('fstable');
        $this->table_name = $fstable->_('fs_recruitment', 1);
        $this->table_category = $fstable->_('fs_recruitment_categories', 1);
        $this->table_comment = $fstable->_('fs_news_comments', 1);
    }

    function get_data_job()
    {
        $id = FSInput::get2('id', 0, 'int');
        if ($id) {
            $where = " id = '$id' ";
        } else {
            $code = FSInput::get('code');
            if (!$code)
                die('Not exist this url');
            $where = " alias = '$code' ";
        }
        $fs_table = FSFactory::getClass('fstable');
        $query = " SELECT *
						FROM " . $fs_table->getTable('fs_recruitments', 1) . " 
						WHERE
						" . $where . " order by created_time desc";
        //print_r($query) ;
        global $db;
        $sql = $db->query($query);
        $result = $db->getObject();
        return $result;
    }
    function get_content_category_ids($str_ids)
    {
        if (!$str_ids)
            return;
        $fs_table = FSFactory::getClass('fstable');

        // search for category

        $query = " SELECT id,alias
                          FROM " . $fs_table->getTable('fs_recruitment_categories', 1) . "
                          WHERE id IN (" . $str_ids . ")
                         ";

        global $db;
        $sql = $db->query($query);
        $result = $db->getObjectList();
        $array_alias = array();
        if ($result)
            foreach ($result as $item) {
                $array_alias[$item->id] = $item->alias;
            }
        return $array_alias;
    }

    function get_list()
    {
        global $db;
        $query = " SELECT id, title, created_time, image, summary, alias
						FROM " . $this->table_name . "
						WHERE published = 1 AND is_hot = 1
						ORDER BY created_time DESC LIMIT 3
						";
        $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    function getListCategory() {
        global $db;
        $query = " SELECT id, name, alias
						FROM ".$this->table_category."
						WHERE published = 1
						";
        $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }



}

?>