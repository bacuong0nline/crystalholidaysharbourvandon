<?php

use Guzzle\Http\Url;

class RecruitmentControllersRecruitment extends FSControllers
{
	var $module;
	var $view;
	function display()
	{
		// call models
		$model = $this->model;
		$data = $model->get_data_job();
		// print_r($data);die;
		include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
	}

	function save_contact2()
	{
		$model = $this->model;

		$alias = FSInput::get('alias');
		$job_id = FSInput::get('job_id');
		$name = FSInput::get('fullname');
		$phone = FSInput::get('telephone');
		$email = FSInput::get('email');
		$content = FSInput::get('content');
		$cyear = date('Y');
		$path = PATH_BASE . 'images' . DS . 'upload_file' . DS . $cyear . DS;
		require_once(PATH_BASE . 'libraries' . DS . 'fsfiles.php');
		$fsFile = FSFactory::getClass('FsFiles');
		$fsFile->create_folder($path);
		$file_upload = $_FILES["cv_name"]["name"];
		$file_ext = $fsFile->getExt($_FILES["cv_name"]["name"]);

		if ($file_ext != 'doc' && $file_ext != 'docx' && $file_ext != 'pdf') {
			setRedirect(FSRoute::_("index.php?module=recruitment&view=recruitment&code=$alias&id=$job_id"), FSText::_("Định dạng hồ sơ không hợp lệ. Chỉ chấp nhận file pdf hoặc docx"), 'error');
			return;
		}
		if ($file_upload) {
			$path_original = $path;
			// remove old if exists record and img
			if ($id) {
				$img_paths = array();
				$img_paths[] = $path_original;
				// special not remove when update
			}

			// upload
			$file_upload_name = $fsFile->upload_file("cv_name", $path_original, 200000000, '_' . time());
			if (!$file_upload_name)
				return false;
		}
		$rs = $this->sendMailFS2($name, $phone, $content);

		if ($rs == 1) {
			$row = array();
			// $row['title'] = $title;
			$row['name'] = $name;
			$row['email'] = $email;
			$row['phone'] = $phone;
			$row['content'] = $content;
			$row['created_time'] = date('Y-m-d H:i:s');
			$row['published'] = 1;
			$row['file'] = 'images/upload_file/' . $cyear . '/' . $file_upload_name;
			$row['alias'] = $alias;
			$row['job_id'] = $job_id;

			$model->_add($row, 'fs_recruitment_apply');
			setRedirect(FSRoute::_("index.php?module=recruitment&view=recruitment&code=$alias&id=$job_id"), FSText::_('Cảm ơn bạn đã gửi thông tin liên hệ!'), 'success');
		} else {
			setRedirect(URL_ROOT, 'Lỗi', 'error');
		}
	}

	function sendMailFS2($name, $phone, $content)
	{
		global $config, $tmpl;

		$email_bcc = $config['admin_email'];
		require_once(PATH_BASE . 'libraries/PHPMailer_v5.1/class.phpmailer.php');
		$smtpHost = 'smtp.gmail.com';
		$smtpPort = '465';
		$smtpEmail = 'mail.finalstyle.noreply@gmail.com';
		$smtpPass = 'oidlkfwxzckmqlaw';
		$nFrom = "Admin: " . $_SERVER['SERVER_NAME'];
		$mail = new PHPMailer();
		$body = '';
		$body .= '<p align="left">Bạn nhận được thông tin liên hệ từ ' . $name . '</p>';
		// $body .= '<p align="left">Tiêu đề: <span>' . $title . '</span></p>';
		$body .= '<p align="left">Tên: <span>' . $name . '</span></p>';
		// $body .= '<p align="left">Tên công ty: <span>' . $company . '</span></p>';
		$body .= '<p align="left">Số điện thoại: <span>' . $phone . '</span></p>';
		$body .= '<p align="left">Nội dung liên hệ: <span>' . $content . '</span></p>';

		$mail->IsSMTP();
		$mail->CharSet = "utf-8";
		$mail->SMTPDebug = 0;
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = "ssl";
		$mail->Host = $smtpHost;
		$mail->Port = $smtpPort;
		$mail->Username = $smtpEmail;
		$mail->Password = $smtpPass;
		$mail->FromName = "IKAY Group";

		$mail->AddBCC($email_bcc);

		$mail->Subject = FSText::_('Bạn nhận được thông tin liên hệ từ') . ' ' . $name . '';
		$mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
		$mail->MsgHTML($body);

		if (!$mail->Send()) {
			return 0;
		} else {
			return 1;
		}
	}
}
