<?php
/*
 * Huy write
 */
// controller

class RecruitmentControllersHome extends FSControllers
{
    function display()
    {
        // call models
        $model = $this -> model;

        global $tags_group;

        $query_body = $model->set_query_body();
        $list = $model->get_list();

        $total = $model->getTotal($query_body);
        $data = $model->get_record_by_id(11, FSTable::_('fs_recruitments_2', 1));

        $images = $model->get_records('record_id = 11', FSTable::_('fs_recruitment_images', 1), '*', 'ordering asc, id desc');
        $pagination = $model->getPagination($total);
		$province = $model->getProvince();

        $departments = $model->get_records('published = 1', FSTable::_('fs_departments', 1));

        // print_r($departments);die;
        $list_cat = $model->get_records('published = 1', FSTable::_("fs_recruitment_position", 1));

        foreach($list_cat as $item) {
            $item->job = $model->get_records('list_position ='.$item->id.' ', FSTable::_("fs_recruitments", 1));
        }
        $count_job = $model->countJobs();
        // echo $count_job;die;

        include 'modules/'.$this->module.'/views/'.$this->view.'/default.php';
    }
    function sendMailFS2($name, $email, $content)
    {
        global $config, $tmpl;

        $email_bcc = $config['admin_email'];
        require_once(PATH_BASE . 'libraries/PHPMailer_v5.1/class.phpmailer.php');
        $smtpHost = 'smtp.gmail.com';
        $smtpPort = '465';
        $smtpEmail = 'mail.finalstyle.noreply@gmail.com';
        $smtpPass = 'fs123456!@#$%^';
        $nFrom = "Admin: " . $_SERVER['SERVER_NAME'];
        $mail = new PHPMailer();
        $body = '';
        $body .= '<p align="left">Bạn nhận được thông tin liên hệ từ '.$name.'</p>';
        // $body .= '<p align="left">Tiêu đề: <span>' . $title . '</span></p>';
        $body .= '<p align="left">Tên: <span>' . $name . '</span></p>';
        // $body .= '<p align="left">Tên công ty: <span>' . $company . '</span></p>';
        $body .= '<p align="left">Nội dung liên hệ: <span>' . $content . '</span></p>';

        $mail->IsSMTP();
        $mail->CharSet = "utf-8";
        $mail->SMTPDebug = 0;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = "ssl";
        $mail->Host = $smtpHost;
        $mail->Port = $smtpPort;
        $mail->Username = $smtpEmail;
        $mail->Password = $smtpPass;
        $mail->FromName = "Quà 36 - Xúc cảm vị thiên nhiên";

        $mail->AddBCC($email_bcc);

        $mail->Subject = 'Bạn nhận được thông tin liên hệ từ '.$name.'';
        $mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
        $mail->MsgHTML($body);

        if (!$mail->Send()) {
            return 0;
        } else {
            return 1;
        }
    }

    function search() {
        $keyword = strtolower(FSInput::get("job-keyword"));
        $location = FSInput::get("location");
        $job_type = FSInput::get("job-type");

        $where = "";
        if($keyword) {
            $where .= ' and (name_position like "%'.strtolower($keyword).'%" or name like "%'.strtolower($keyword).'%")';
        }
        if($location) {
            $where .= ' and address like "%'.strtolower($location).'%"';
        }
        if($job_type) {
            $where .= ' and working_day = "'.$job_type.'"';
        }
        $list_cat = $this->model->get_records('published = 1', FSTable::_("fs_recruitment_position", 1));

        foreach($list_cat as $item) {
            $item->job = $this->model->get_records('1 = 1 and list_position ='.$item->id.''  . $where, FSTable::_("fs_recruitments", 1));
        }

        echo json_encode($list_cat);
    }
    function add_info() {
        $row['first_name'] = FSInput::get("firstname");
        $row['last_name'] =FSInput::get("lastname");
        $row['email'] =FSInput::get("email");
        $row['telephone'] =FSInput::get("phone");
        $row['address'] =FSInput::get("address");
        $row['province_id'] = (int)FSInput::get("fprovince");
        $row['district_id'] = (int)FSInput::get("fdistrict");
        $row['position'] =FSInput::get("position");
        $row['working_day'] =FSInput::get('working_day', '', 'array');
        $row['working_time'] =FSInput::get("working_time");
        $row['salary'] =FSInput::get("salary");
        $row['reply'] =FSInput::get("reply");
        $row['created_time'] = date("Y-m-d H:i:s");
        $row['qualification'] = FSInput::get("qualification");
        $row['experience'] = FSInput::get("experience");

        $name = $row['first_name'] . $row['last_name'];
        FSInput::get("shift") ? $row['shift'] = FSInput::get("shift") : null;
        FSInput::get("shift_day") ? $row['shift_day'] = FSInput::get("shift_day") : null;

        $row['working_day'] = implode(',', $row['working_day']);

        $rs = $this->model->_add($row, 'fs_recruitments');

        if($rs) {
            $this->sendMailFS2($name, $row['email'], 'Nộp đơn ứng tuyển');
        }
        if ($rs) {
            setRedirect(URL_ROOT, 'Gửi thông tin thành công!', 'success');
        } else {
            setRedirect(URL_ROOT, 'Có lỗi xảy ra vui lòng thử lại', 'error');
        }

    }

}

?>