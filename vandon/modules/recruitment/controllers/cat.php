<?php	
	class RecruitmentControllersCat extends FSControllers
	{
		var $module;
		var $view;
		function display()
		{
			// call models
		    $model = $this -> model;
            $jobs = $model->get_jobs();
            $positions = $model->getPosition();
            $list_province = $model->list_province();

            $total = $this->model->getTotal2('');
            $pagination = $this->model->getPagination($total);
            include 'modules/'.$this->module.'/views/'.$this->view.'/default.php';
		}

        function save()
        {

            $model = $this->model;
            $id_job = FSInput::get('id_job');
            $job = FSInput::get('job');
            $title = FSInput::get('title');
            $name = FSInput::get('name');
            $tel = FSInput::get('tel');
            $day = FSInput::get('day');
            $month = FSInput::get('month');
            $year = FSInput::get('year');
            $add = FSInput::get('add');
            $email = FSInput::get('mail');
            $content = FSInput::get('content');
//            $file = FSInput::get('file');
            $file = $_FILES["file"]["name"];

            $row = array();
            $row['id_job'] = $id_job;
            $row['job'] = $job;
            $row['title'] = $title;
            $row['name'] = $name;
            $row['tel'] = $tel;
            $row['day_birth'] = $day;
            $row['month_birth'] = $month;
            $row['year_birth'] = $year;
            $row['address'] = $add;
            $row['email'] = $email;
            $row['content'] = $content;
            $row['created_time'] = date("Y-m-d H:i:s");

            if($file){
                $path_original = 'images/upload_file/';
                $path = str_replace('/', DS, $path_original);
                $path = PATH_BASE.$path;
                $fsFile = FSFactory::getClass('FsFiles');
                // upload
                $file_upload_name = $fsFile -> upload_file("file", $path ,30000000, '_'.time());
                if(!$file_upload_name)
                    return false;
                $row['file'] = 'images/upload_file/'.$file_upload_name;
            }
            $row['created_time'] = date('Y-m-d H:i:s');
            $row['published'] = 1;
            $id = $model->_add($row, 'fs_recruitment_register');

            $link = $model->get_record_by_id($id,'fs_recruitment_register','file')->file;

            $rs = $this->sendMailFS($link,$id_job, $job, $title, $name, $tel, $day, $month, $year, $add, $email, $content, $file);
            if ($rs == 1) {
                setRedirect(FSRoute::_('index.php?module=recruitment&view=home&Itemid=213'), 'Cảm ơn bạn đã ứng tuyển!', 'success');
            } else {
                setRedirect(URL_ROOT, 'Lỗi', 'error');
            }
//        }
        }
        function search_job() {
            $where = '';
            if(FSInput::get("search-recruitment")) {
                $keyword = FSInput::get("search-recruitment");
                $where .= " and name like '%$keyword%' ";
            }
            if(FSInput::get("position")) {
                $position = FSInput::get("position");
                $where .= " and list_position like '%,$position,%' ";

            }
            if(!empty(FSInput::get("time_working"))) {
                $time_working = FSInput::get("time_working");
                $where .= " and working_day = $time_working ";
            }
            
            if(FSInput::get("location")) {
                $location = FSInput::get("location");
                $where .= " and province_id = $location ";

            }
            $jobs = $this->model->get_jobs($where);
            $positions = $this->model->getPosition();
            $list_province = $this->model->list_province();
            $total = $this->model->getTotal2($where);
            $pagination = $this->model->getPagination($total);
            include 'modules/'.$this->module.'/views/'.$this->view.'/default.php';

        }
        function sendMailFS($link,$id_job, $job, $title, $name, $tel, $day, $month, $year, $add, $email, $content, $file)
        {
            global $config, $tmpl;

            $email_bcc = $config['admin_email'];
            require_once(PATH_BASE . 'libraries/PHPMailer_v5.1/class.phpmailer.php');
            $smtpHost = 'smtp.gmail.com';
            $smtpPort = '465';
            $smtpEmail = 'lanam.noreply@gmail.com';
            $smtpPass = '123lanam456';
            $nFrom = "Admin: " . $_SERVER['SERVER_NAME'];
            $mail = new PHPMailer();
            $body = '';
            $body .= '<p align="left">Bạn nhận được thông tin đăng ký đăng ký tuyển dụng từ '.$name.'</p>';
            $body .= '<p align="left">Tên: <span>' . $name . '</span></p>';
            $body .= '<p align="left">Số điện thoại: <span>' . $tel . '</span></p>';
            $body .= '<p align="left">Ngày sinh: <span>' . $day . '/'.$month.'/'.$year.'</span></p>';
            $body .= '<p align="left">Địa chỉ: <span>' . $add . '</span></p>';
            $body .= '<p align="left">Địa chỉ Email: <span>' . $email . '</span></p>';
            $body .= '<p align="left">Công việc đăng ký ứng tuyển: <span>' . $job . '</span> - id-'.$id_job.'</p>';
            $body .= '<p align="left">Tiêu đề hồ sơ: <span>' . $title . '</span></p>';
            $body .= '<p align="left">Nội dung liên hệ: <span>' . $content . '</span></p>';
            $body .= '<p align="left">Hồ sơ đính kèm: Bên dưới</p>';
            //$body             = $content2;
            $mail->IsSMTP();
            $mail->CharSet = "utf-8";
            $mail->SMTPDebug = 0;
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = "ssl";
            $mail->Host = $smtpHost;
            $mail->Port = $smtpPort;
            $mail->Username = $smtpEmail;
            $mail->Password = $smtpPass;
            $mail->FromName = "Lanam";
            $mail->AddAttachment( $link , $file );
//        $mail->setfrom('hieunb96@gmail.com','nbk');
//        $mail->addreplyto('hieunb96@gmail.com','nbk');
            $mail->AddBCC($email_bcc);
//        if($mCc != '')
//        $mail->AddBCC($mCc, $nFrom);
            $mail->Subject    = 'Lanam - Tuyển dụng';
            $mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
            $mail->MsgHTML($body);
//        $mail->AddAddress($email, $fullname);

            if (!$mail->Send()) {
                return 0;
            } else {
                return 1;
            }
        }
	}
	
?>