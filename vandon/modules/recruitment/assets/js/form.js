$(document).ready(function(){
    $("#search-recruitment").keydown(function(e) {
        if (e.which == 13) {

        }
            
    })

    $(".apply-job-btn").click(function() {
        if(validate_job())
            $("#apply-job").submit();
    })

	$(".filter-job").click(function(e) {
		$("#search-job").submit();
	})
})

function validate_job() {
	$('label.label_error').prev().remove();
	$('label.label_error').remove();

	if (!notEmpty("fullname", "Bạn chưa nhập họ tên")) {
		return false;
	}
    if (!isPhone("telephone", "Số điện thoại không hợp lệ")) {
		return false;
	}
	if (!emailValidator("email", "Bạn chưa nhập email")) {
		return false;
	}
    if (!notEmpty("description", "Thư xin việc không được bỏ trống")) {
		return false;
	}
	if (!notEmpty("file", "Hồ sơ xin việc của bạn")) {
		return false;
	}

	if (!notEmpty("info", "Bạn biết chúng tôi qua đâu?")) {
		return false;
	}

	else {

	}
	return true;
}