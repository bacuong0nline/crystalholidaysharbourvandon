$(document).ready(function () {
  var $video = $("#video-introduce"), //jquery-wrapped video element
    mousedown = false;

  $video.click(function () {
    $(".play-btn").css("display", "none")
    if (this.paused) {
      this.play();
      return false;
    }
    return true;
  });
  $(".play-btn").click(function () {
    // console.log($video)
    $(".play-btn").css("display", "none")
    $video.get(0).play();
  })
  $video.on('mousedown', function () {
    mousedown = true;
  });

  $(window).on('mouseup', function () {
    mousedown = false;
  });

  $video.on('play', function () {
    $video.attr('controls', '');
  });

  $video.on('pause', function () {
    $(".play-btn").css("display", "block")

    if (!mousedown) {
      $video.removeAttr('controls');
    }
  });


  $('#department').owlCarousel({
    loop: true,
    nav: true,
    dots: true,
    autoplay:true,
    autoplayTimeout:4000,
    autoplayHoverPause:false,
    navText: ["<img src='/images/prev.svg'>","<img src='/images/next.svg'>"],
    responsive: {
      0: {
        items: 1
      },
      600: {
        margin: 40,
        items: 2
      },
      1900: {
        margin: 60,
        items: 2
      }
    }
  })

  $(".search-job").click(function () {
    let keyword;
    keyword = $("#job-form").serialize();
    let html = '';
    $(".list-job-wrapper").html("Loading...")

    $.ajax({
      url: 'index.php?module=recruitment&view=home&task=search&raw=1&' + keyword,
      success: function (data) {
        let res = JSON.parse(data);
        res.forEach(function (item) {
          if (item.job.length > 0) {
            html += `
            <div class="mb-5">
                <h5 style="font-weight: 500; margin-top: 15px; margin-bottom: 15px;">${item.name}</h5>
                ${list_job(item.job)}
            </div>
          `
          }

        })
        $(".list-job-wrapper").html(html)

      },
      error: function (e) {
        alert("Có lỗi xảy ra")
      }
    })
  })

  function list_job(list) {
    let html2 = '';
    list.forEach(function (val) {
      html2 += `
      <a href="/${val.alias}-r${val.id}.html" class="job-item d-flex flex-column" style="position: relative">
          <span class="title-job" style="font-weight: 600">${val.name}</span>
          <span class="summary-job">${val.address}</span>
          <span style="position: absolute; right: 0px; top: 20px;">
              <svg xmlns="http://www.w3.org/2000/svg" width="9.772" fill="currentColor" height="14.451" viewBox="0 0 9.772 14.451">
                  <g id="button_next" transform="translate(0.665 0.747)">
                      <path id="Path_294" data-name="Path 294" d="M1803.616,4362.342l7.568,6.729-7.568,6.2" transform="translate(-1803.616 -4362.342)" fill="none" stroke="currentColor" stroke-width="2" />
                  </g>
              </svg>
          </span>
      </a>
      `
    })
    return html2
  }

  var flkty = new Flickity('.carousel-intro', {
    pageDots: false,
    cellAlign: 'left',
    groupCells: '40%',
    // prevNextButtons: false,
    // wrapAround: true,
    // fade: true,
    on: {
      ready: function () {

        $(".title-image-2").html($((".is-selected > .title-image")).attr("data-title"))
      }
    }
  });

  flkty.on('select', function () {
      $(".title-image-2").html($((".is-selected > .title-image")).attr("data-title"))
  });
})


