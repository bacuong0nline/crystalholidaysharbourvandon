$(document).ready(function () {


  $('.send-contact').click(function () {
    var submit = '#contact-form';
    if (checkFormsubmit2()) {
      $("#contact-form").submit();
      $(this).css("pointer-events", "none");
    }
  })

  $('#cv-input').change(function () {
    var filename = document.getElementById("cv-input").files[0].name;
    $('.name-file').html(filename);
  });


});
var alert_info = $('#alert_info').val();
let alert_info1 = alert_info ? JSON.parse(alert_info) : [];

function checkFormsubmit2() {
  $('label.label_error').prev().remove();
  $('label.label_error').remove();

  if (!notEmpty("address", alert_info1[10])) {
    return false;
  }
  if (!notEmpty("fullname", alert_info1[1])) {
    return false;
  }
  if (!emailValidator("email", alert_info1[4])) {
    return false;
  }
  if (!notEmpty("telephone", alert_info1[5])) {
    return false;
  }
  if (!isPhone("telephone", alert_info1[6])) {
    return false;
  }
  if (!lengthRestriction("telephone", "10", "12", alert_info1[7] + ' 10 ' + alert_info1[9] + ' 12 ' + alert_info1[8])) {
    return false;
  }
  if (document.getElementById("cv-input").files.length == 0) {
    invalid("cv-input", alert_info1[11]);
    return false;
  }
  if (!notEmpty("content", alert_info1[2])) {
    return false;
  }


  else {

  }
  // return false;
  return true;
}
