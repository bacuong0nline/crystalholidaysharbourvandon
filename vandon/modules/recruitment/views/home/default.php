<?php
global $tmpl;
$tmpl->addStyleSheet('home', 'modules/recruitment/assets/css');
$tmpl->addStyleSheet("flickity", "templates/default/css");
$tmpl->addScript("flickity", "templates/default/js");
$tmpl->addTitle(FSText::_('Tuyển dụng'));
$tmpl->addScript("recruitment", 'modules/recruitment/assets/js');

?>

<div>
    <?php echo $tmpl->load_direct_blocks('banners', array('style' => 'style4', 'category_id_image' => 5)) ?>

    <div class="container space"  style="position:relative">
        <?php echo $data->content1 ?>
        <?php if (!empty($images)) { ?>
            <div class="carousel-intro" data-flickity='{ "groupCells": "40%", "cellAlign": "left", "pageDots": false }'>
                <?php foreach ($images as $item) {
                    $image = str_replace(['jpeg', 'png', 'jpg'], ['webp', 'webp', 'webp'], $item->image);
                    $image = str_replace('original', 'resized', $image);
                ?>
                    <div class="carousel-cell-intro">
                        <img src="<?php echo $image ?>" alt="<?php echo str_replace('original', 'resized', $item->image) ?>" />
                        <p class="title-image" data-title="<?php echo $item->title ?>"><?php echo $item->title ?></p>
                    </div>

                <?php } ?>
            </div>
        <?php } ?>
        <p style="position: absolute; left: 140px; bottom: -70px; color: #707070; right: 20px" class="title-image-2">test</p>
    </div>
    <div class="container space we-work">
        <?php echo $data->content3 ?>

        <div class="owl-carousel owl-theme" id="department">
            <?php foreach ($departments as $item) { ?>
                <div class="item">
                    <img src="<?php echo str_replace('original', 'resized', $item->image) ?>" alt="<?php echo str_replace('original', 'resized', $item->image) ?>">
                    <h3 class="text-center text-md-start" style="margin-top: 24px"><?php echo $item->title ?></h3>
                    <div style="color: #707070">
                        <?php echo $item->experience ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="space discover">
        <div class="container">
            <div class="btgrid">
                <div class="row row-1 justify-content-around">
                    <?php echo $data->content4 ?>
                    <div class="col col-md-6">
                        <div class="content video-discover-wrap">
                            <div class="play-btn"></div>
                            <video poster="<?php echo $data->image ?>" id="video-introduce">
                                <source src="<?php echo $data->file_upload ?>" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                            <!-- <img src="/images/home4.jpeg" alt="home4.jpg"> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="space why-choose-ikay">
        <div class="container">
            <?php echo $data->content5 ?>
            <p>&nbsp;</p>
        </div>

    </div>
    <div class="space come-to-us container">
        <?php echo $data->content6 ?>
    </div>
    <div style="background-color: #fcfcfc">
        <div class="space job-box container pt-5 pb-5" id="job-box-wrapper">
            <div class="d-flex">
                <div class="col-md-6">
                    <h3 class="title-recruitment-2 text-center text-md-start text-capitalize"><?php echo FSText::_("Cơ hội việc làm") ?></h3>
                    <p class="summary-recruitment text-center text-md-start pt-md-3 pb-3"><?php echo FSText::_("Nếu bạn muốn sử dụng năng lượng của mình cho mục đích tốt, chúng tôi rất muốn nghe ý kiến của bạn.") ?></p>
                </div>
            </div>
            <div class="d-md-flex">
                <div class="col-md-4 pe-md-5">
                    <div class="job-filter-box">
                        <h4><?php echo $count_job . ' ' . FSText::_("Công việc") ?></h4>
                        <form method="post" id="job-form">
                            <div class="input-box d-flex flex-column">
                                <label for="input-1"><?php echo FSText::_("Tìm kiếm công việc") ?></label>
                                <input type="text" name="job-keyword" id="input-1" />
                            </div>
                            <div class="input-box d-flex flex-column">
                                <label for="input-2"><?php echo FSText::_("Địa điểm") ?></label>
                                <input type="text" name="location" id="input-2" />
                            </div>
                            <div class="checkbox-wrap">
                                <label class="fw-bold"><?php echo FSText::_("Loại công việc") ?></label>
                                <div class="d-flex flex-wrap">
                                    <div class="input-box" style="width: 50%">
                                        <input type="checkbox" class="form-check-input" name="job-type" id="job-type-1" value="1" />
                                        <label for="job-type-1" style="padding: 5px">
                                            <?php echo FSText::_("Hợp đồng") ?>
                                        </label>
                                    </div>
                                    <div class="input-box" style="width: 50%">
                                        <input type="checkbox" class="form-check-input" name="job-type" id="job-type-2" value="2" />
                                        <label for="job-type-2" style="padding: 5px">
                                            <?php echo FSText::_("Toàn thời gian") ?>
                                        </label>
                                    </div>
                                    <div class="input-box">
                                        <input type="checkbox" class="form-check-input" name="job-type" id="job-type-3" value="3" />
                                        <label for="job-type-3" style="padding: 5px">
                                            <?php echo FSText::_("Thực tập") ?>
                                        </label>
                                    </div>

                                </div>
                            </div>
                            <a class="btn-style-3 ms-auto search-job"><?php echo FSText::_("Tìm kiếm") ?></a>
                        </form>


                    </div>
                </div>
                <div class="col-md-8 ps-md-5">
                    <h4 class="title-recruitment-2"><?php echo FSText::_("Công việc") ?></h4>
                    <div class="list-job-wrapper">
                        <?php foreach ($list_cat as $item) { ?>
                            <div class="mb-5">
                                <h5 style="font-weight: 500; margin-top: 15px; margin-bottom: 15px;"><?php echo $item->name ?></h5>
                                <?php foreach ($item->job as $val) { ?>
                                    <a href="<?php echo FSRoute::_("index.php?module=recruitment&view=recruitment&code=$val->alias&id=$val->id") ?>" class="job-item d-flex flex-column" style="position: relative">
                                        <span class="title-job" style="font-weight: 600"><?php echo $val->name ?></span>
                                        <span class="summary-job"><?php echo $val->address ?></span>
                                        <span style="position: absolute; right: 0px; top: 20px;">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.772" fill="currentColor" height="14.451" viewBox="0 0 9.772 14.451">
                                                <g id="button_next" transform="translate(0.665 0.747)">
                                                    <path id="Path_294" data-name="Path 294" d="M1803.616,4362.342l7.568,6.729-7.568,6.2" transform="translate(-1803.616 -4362.342)" fill="none" stroke="currentColor" stroke-width="2" />
                                                </g>
                                            </svg>
                                        </span>
                                    </a>
                                <?php  } ?>
                            </div>
                        <?php  } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


<script>

</script>