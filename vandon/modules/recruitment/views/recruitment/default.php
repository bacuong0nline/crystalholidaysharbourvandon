<?php
global $tmpl, $config;

$tmpl->addStylesheet('detail', 'modules/recruitment/assets/css');
$tmpl->addStylesheet('contact', 'modules/contact/assets/css');
$tmpl->addScript('form', 'modules/recruitment/assets/js');
$tmpl->addScript('detail', 'modules/recruitment/assets/js');
$alert_info = array(
    0 => FSText::_('Nhập Từ Khóa'),
    1 => FSText::_('Bạn chưa nhập họ và tên'),
    2 => FSText::_('Bạn chưa nhập nội dung'),
    3 => FSText::_('Bạn chưa nhập email'),
    4 => FSText::_('Email không hợp lệ'),
    5 => FSText::_('Bạn chưa nhập số điện thoại'),
    6 => FSText::_('Số điện thoại không hợp lệ'),
    7 => FSText::_('Vui lòng nhập từ'),
    8 => FSText::_('số'),
    9 => FSText::_('đến'),
    10 => FSText::_('Bạn chưa nhập địa chỉ'),
    11 => FSText::_('Bạn chưa đính kèm CV'),

);
?>

<input type="hidden" id="alert_info" value='<?php echo json_encode($alert_info) ?>'>

<main>
    <div class="container contact-wrapper">
        <div class="d-flex flex-column flex-md-row">
            <div class="col-md-6 pe-4">
                <div class="container">
                    <h1 class="mb-5"><?php echo $data->name ?></h1>
                </div>
                <div class="container">
                    <?php echo $data->description ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="contact-box">
                    <form action="index.php?module=recruitment&view=recruitment&task=save_contact2" id="contact-form" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="alias" value="<?php echo $data->alias ?>">
                        <input type="hidden" name="job_id" value="<?php echo $data->id ?>">

                        <div class="input-form">
                            <label for="address"><?php echo FSText::_("Địa chỉ*") ?></label><br>
                            <input type="text" id="address" name="address" />
                        </div>
                        <div class="input-form">
                            <label for="fullname"><?php echo FSText::_("Họ và tên*") ?></label><br>
                            <input type="text" id="fullname" name="fullname" />
                        </div>
                        <div class="d-flex input-form" style="grid-gap: 20px">
                            <div style="width: 50%">
                                <label for="email"><?php echo FSText::_("Email*") ?></label><br>
                                <input type="text" id="email" name="email" />
                            </div>
                            <div style="width: 50%">
                                <label for="telephone"><?php echo FSText::_("Số điện thoại*") ?></label><br>
                                <input type="text" id="telephone" name="telephone" />
                            </div>
                        </div>
                        <div class="input-form">
                            <div class="d-flex justify-content-between">
                                <label for="content"><?php echo FSText::_("Nội dung yêu cầu*") ?></label><br>
                                <label for="cv-input" style="cursor: pointer">
                                    <span style="
                                        text-decoration: underline;
                                        max-width: 250px;
                                        white-space: nowrap;
                                        overflow: hidden;
                                        text-overflow: ellipsis;
                                        display: block;
                                        " class="name-file"><?php echo FSText::_("Đính kèm CV") ?></span>
                                    <input type="file" name="cv_name" id="cv-input" class="d-none">
                                </label>
                            </div>

                            <textarea id="content" name="content"></textarea>
                        </div>
                    </form>
                    <a class="send-contact"><?php echo FSText::_("Gửi") ?></a>
                </div>
            </div>
        </div>
    </div>
</main>