<?php
global $tmpl, $config;

$tmpl->addStylesheet('home', 'modules/recruitment/assets/css');
$tmpl->addScript('form', 'modules/recruitment/assets/js');
$alert_info = array(
    0 => FSText::_('Bạn chưa xác nhận Captcha'),
    1 => FSText::_('Bạn chưa nhập họ và tên'),
    2 => FSText::_('Bạn chưa nhập tiêu đề hồ sơ'),
    3 => FSText::_('Bạn chưa nhập email'),
    4 => FSText::_('Email không hợp lệ'),
    5 => FSText::_('Bạn chưa nhập số điện thoại'),
    6 => FSText::_('Số điện thoại không hợp lệ'),
    7 => FSText::_('Vui lòng nhập từ'),
    8 => FSText::_('số'),
    9 => FSText::_('đến'),
    10 => FSText::_('Bạn chưa nhập địa chỉ'),
    11 => FSText::_('Bạn chưa chọn ngày sinh'),
    12 => FSText::_('Bạn chưa chọn tháng sinh'),
    13 => FSText::_('Bạn chưa chọn năm sinh'),
    14 => FSText::_('Bạn chưa tải lên hồ sơ'),
    15 => FSText::_('Tập tin không đúng định dạng. Tải lại!'),
);
?>

<main>
    <?php if ($tmpl->count_block('top_default')) { ?>
        <?php $tmpl->load_position('top_default'); ?>
    <?php } ?>
    <div class="snowflake1 desktop" style="width: 100%; background-size: 100%; background-repeat: no-repeat; background-position: center center; position: relative; padding-bottom: 150px;">
        <div class="container pb-3">
            <h5 class="title text-center text-uppercase mb-4"><?php echo FSText::_("tìm kiếm công việc mong muốn của bạn") ?></h5>
        </div>
        <form id="search-job" action="index.php?module=recruitment&view=cat&task=search_job&Itemid=34" method="POST">
            <div class="container d-flex justify-content-between">
                <div class="input-feild">
                    <i class="fa fa-search" aria-hidden="true"></i>
                    <input type="text" id="search-recruitment" name="search-recruitment" placeholder="<?php echo FSText::_("Tìm kiếm") ?>..." />
                </div>
                <div class="input-feild">
                    <select name="position" id="">
                        <option value="" selected><?php echo FSText::_("Vị trí tuyển dụng") ?></option>
                        <?php foreach ($positions as $item) { ?>
                            <option <?php echo @$position == $item->id ? 'selected' : '' ?> value="<?php echo $item->id ?>"><?php echo $item->name ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="input-feild">
                    <select name="time_working" id="">
                        <option value="" selected><?php echo FSText::_("Thời gian làm việc") ?></option>
                        <?php foreach (array(1 => FSText::_('Bán thời gian'), 2 => FSText::_('Chính thức')) as $key => $item) { ?>
                            <option <?php echo @$time_working == $key ? 'selected' : '' ?> value="<?php echo $key ?>"><?php echo $item ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="input-feild">
                    <select name="location" id="">
                        <option value="" selected><?php echo FSText::_("Khu vực làm việc") ?></option>
                        <?php foreach ($list_province as $item) { ?>
                            <option <?php echo @$location == $item->id ? 'selected' : '' ?> value="<?php echo $item->id ?>"><?php echo $item->name ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <br>
            <div class="d-flex w-100 justify-content-center">
                <a class="btn-register-2 filter-job"><?php echo FSText::_("Tìm kiếm") ?></a>
            </div>
        </form>
        <img style="position: absolute; bottom: 0px; width: 100%" src="./images/wave2.png" alt="wave2" />
    </div>
    <div class="snowflake2 choose-vmg-box" id="job-wrap" style="width: 100%; background-size: 100%; background-repeat: no-repeat; background-position: center center; position: relative; padding-bottom: 200px; background-color: #F8F9F9;">
        <h5 class="text-uppercase font-weight-strong text-center mb-4">
            <?php $count_job = count($jobs);
            echo FSText::_("Có tất cả") . ' ' . "<span class='red--text font-weight-bold'>$count_job</span>" . ' ' . FSText::_("việc làm dành cho bạn") ?></h5>
        <div class="container box-ielts-2 d-flex flex-wrap">
            <?php foreach ($jobs as $item) { ?>
                <div class="col-md-4 mt-md-4">
                    <?php $tmpl->job_item($item) ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="col no-gutters d-flex justify-content-center">
        <?php if (@$pagination) {
            echo $pagination->showPagination(1);
        }
        ?>
    </div>
    <?php $tmpl->load_direct_blocks('form', array('style' => 'style2')) ?>
</main>