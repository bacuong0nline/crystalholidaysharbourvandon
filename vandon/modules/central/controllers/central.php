<?php

class CentralControllersCentral extends FSControllers
{
    var $module;
    var $view;

    function display()
    {
        $query_body = $this->model->set_query_body();
        $data = $this->model->getData();
        $list_image = $this->model->getImages();
        $list_news_central = $this->model->get_list_news_central();

        $list_central = $this->model->get_list_central($data);
        $dataDistrict = $this->model->get_district();

        // unset($_SESSION['selected_central']);
        include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
    }

    function detail()
    {
        $data = $this->model->getDataNews();
        $list = $this->model->getRelateNews($data->category_id);

        include 'modules/' . $this->module . '/views/' . $this->view . '/news.php';
    }
    function removeSelected() {
        unset($_SESSION['selected_central']);
    }
    function get_agency()
    {
        $city_id = FSInput::get('district_id');
        if ($city_id == 0) {
            unset($_SESSION["district"]);
        } else {
            $_SESSION["district"] = $city_id;
        }
    }

    function loadDistricts()
    {
        $city_id = FSInput::get('city_id');
        $_SESSION["city"] = $city_id;
        global $config;
        $listDistricts = $this->model->getListDistricts($city_id);
        $html = '';
        foreach ($listDistricts as $item) {
            $html .= '<option  value="' . $item->id . '">' . $item->name . '</option>';
        }
        echo $html;
    }
    function loadSelectedCentral()
    {
        $id = FSInput::get('id');
        $_SESSION["selected_central"] = $id;
        $district_id = FSInput::get('city_id');
        $_SESSION["district"] = $district_id;
        // global $config;
        // $listDistricts = $this->model->getListDistricts($city_id);
        // $html = '';
        // foreach ($listDistricts as $item) {
        //     $html .= '<option  value="' . $item->id . '">' . $item->name . '</option>';
        // }
        // echo $html;
    }
    function loadADDStore()
    {
        $id = FSInput::get('id');

        global $config;
        $infoStore = $this->model->get_record('id=' . $id, 'fs_address');
        $html = '';

        $html .= '<p>' . $infoStore->address . '<p>';

        echo $html;
        return true;
    }
}