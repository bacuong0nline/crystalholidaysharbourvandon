<?php
global $tmpl, $config;

$tmpl->addStylesheet('home', 'modules/news/assets/css');
$tmpl->addStylesheet('detail', 'modules/news/assets/css');
$tmpl->addStylesheet('detail', 'modules/central/assets/css');

// $tmpl->addStylesheet('tintucchitiet', 'modules/news/assets/css');
$tmpl->addStylesheet('tintuc', 'modules/news/assets/css');
$tmpl->addScript('news', 'modules/central/assets/js', 'bottom');


?>
<main>
    <div class="container news-central-wrapper" style="width: 60%">
        <div class="grid-container">
            <div class="grid-left">
                <div class="item-grid">
                    <h1 class="detail_title"><?php echo $data->title ?></h1>
                    <div class="time-hit">
                        <p class="detail-time-hit">
                            <i class="fa fa-clock-o"></i>
                            <?php echo date('d-m-Y', strtotime($data->created_time)) ?>
                        </p>
                        <p class="detail-time-hit">
                            <i class="fa fa-eye"></i>
                            <?php echo FSText::_('Lượt xem:') ?> <?php echo $data->hits ?>
                        </p>
                        <p class="mr-3 detail-time-hit"><?php echo $data->author ?></p>
                        <svg class="mt-2 detail-time-hit" xmlns="http://www.w3.org/2000/svg" width="5" height="5" viewBox="0 0 5 5">
                            <circle id="Ellipse_97" data-name="Ellipse 97" cx="2.5" cy="2.5" r="2.5" fill="#666" />
                        </svg>
                        <div class="ml-3 detail-time-hit">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14.458" height="16" viewBox="0 0 14.458 16">
                                <path id="Path_315" data-name="Path 315" d="M15.048,13.309a2.339,2.339,0,0,0-1.574.618L7.747,10.594a2.629,2.629,0,0,0,.072-.562,2.629,2.629,0,0,0-.072-.562l5.663-3.3a2.4,2.4,0,1,0-.771-1.759,2.629,2.629,0,0,0,.072.562l-5.663,3.3a2.41,2.41,0,1,0,0,3.518l5.719,3.341a2.266,2.266,0,0,0-.064.522,2.345,2.345,0,1,0,2.345-2.345Z" transform="translate(-3 -2)" fill="#777" />
                            </svg>
                            <a href="#" onclick="share_click(600, 600, 'tw')" target="_blank" class="mr-1 ml-3">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="13" viewBox="0 0 16 13">
                                    <path id="Path_316" data-name="Path 316" d="M16,49.533a6.839,6.839,0,0,1-1.89.518,3.262,3.262,0,0,0,1.443-1.813,6.555,6.555,0,0,1-2.079.794A3.28,3.28,0,0,0,7.8,51.276a3.378,3.378,0,0,0,.076.747,9.284,9.284,0,0,1-6.761-3.431,3.282,3.282,0,0,0,1.007,4.384,3.239,3.239,0,0,1-1.481-.4v.036a3.3,3.3,0,0,0,2.628,3.223,3.274,3.274,0,0,1-.86.108,2.9,2.9,0,0,1-.621-.056,3.311,3.311,0,0,0,3.065,2.285,6.591,6.591,0,0,1-4.067,1.4A6.143,6.143,0,0,1,0,59.522a9.235,9.235,0,0,0,5.032,1.472,9.272,9.272,0,0,0,9.336-9.334c0-.145-.005-.285-.012-.424A6.544,6.544,0,0,0,16,49.533Z" transform="translate(0 -47.994)" fill="#777" />
                                </svg>
                            </a>
                            <a href="#" onclick="share_click(600, 600, 'fb')" target="_blank" class="mr-1 ml-3">
                                <svg xmlns="http://www.w3.org/2000/svg" width="8.309" height="16" viewBox="0 0 8.309 16">
                                    <path id="Brand_Logos" data-name="Brand Logos" d="M12.007,18V10.7h2.45l.367-2.844H12.007V6.041c0-.823.229-1.385,1.41-1.385h1.506V2.112A20.187,20.187,0,0,0,12.728,2,3.427,3.427,0,0,0,9.07,5.76v2.1H6.614V10.7H9.07V18Z" transform="translate(-6.614 -2)" fill="#777" />
                                </svg>
                            </a>
                        </div>
                    </div>
                    <?php if (strpos($data->content, 'h2') == true) { ?>
                        <div class="row">
                            <div class=" col-md-8">
                                <div class="toc-content rounded mb-4" id="left1">
                                    <div class="title-toc-list d-flex justify-content-between p-3">
                                        <h3 class="title-toc"><i class="fa fa-bars mr-1"></i><?php echo FSText::_('Nội dung chính') ?></h3>
                                        <span class="button-select">
                                            <i class="fa fa-angle-down"></i>
                                        </span>
                                    </div>
                                    <div class="list-toc">
                                        <ol id="toc" class="p-3 pb-0"></ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    <?php } ?>
                    <div class="detail-content">
                        <?php echo $data->content ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="container" style="padding-bottom: 100px">
        <h4 class="text-center"><?php echo FSText::_("Bài viết liên quan") ?></h4>
        <div class="row no-gutters news-home tin-tuc-chung__content d-flex mt-3 owl-carousel owl-theme" style="height: 380px">
            <?php foreach ($list as $item) { ?>
                <?php $tmpl->news_item($item, 'index.php?module=central&view=central&task=detail') ?>
            <?php } ?>
        </div>
    </div>
</main>

<script>
    function share_click(width, height, type) {
        var leftPosition, topPosition;
        //Allow for borders.
        leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
        //Allow for title and status bars.
        topPosition = (window.screen.height / 2) - ((height / 2) + 50);
        var windowFeatures = "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";
        u = location.href;
        t = document.title;
        if (type === 'fb') {
            window.open('http://www.facebook.com/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t), 'sharer', windowFeatures);
        }
        if (type === 'tw') {
            window.open('http://twitter.com/intent/tweet?url=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t), 'sharer', windowFeatures);
        }
        return false;
    }
</script>