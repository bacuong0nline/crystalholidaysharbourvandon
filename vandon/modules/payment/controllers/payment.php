<?php
class PaymentControllersPayment extends FSControllers
{
  function display()
  {

    $model = $this->model;
    $provice = $model->getProvince();
    // print_r($provice);
    $link = URL_ROOT;

    $mba = $model->getAllMemberAddress();

    if (empty($_SESSION['cart']))
      setRedirect(URL_ROOT, 'Giỏ hàng trống');
    $list_cart = $_SESSION['cart'];

    $data = array();
    $price_options_value = 0;


    $total_price = 0;
    $total_end = 0;
    $data = array();
    if (!empty($list_cart))
      foreach ($list_cart as $key1 => $item) {
        $data[$item[0]] = $model->get_record_by_id($item[0], 'fs_products');
        $products_info = $data[$item[0]];
        $total = 0;
        $total = $products_info->price * $item[1];
        $total_price += $total;
        $options_display = '';

        if (!empty($item[3])) {
          foreach ($item[3] as $key => $val) {
            if (empty($val['id'])) {
              foreach ($val as $val2) {
                $price = @$val2['price'] != "0.00" ? format_money_0(@$val2['quantity'] * @$val2['price']) : null;
                $quantity_option = @$val2['quantity'] > 1 ? 'x ' . @$val2['quantity'] . '' : '';
                $options_display .= '<p class="mb-0" style="font-size: 12px; color: #6e6e6e">' . @$val2['name_display'] . ' ' . $quantity_option . ' ' . $price . ' </p>';
              };
            } else {
              $price = @$val['price'] != "0.00" ? format_money_0(@$val['quantity'] * @$val['price']) : null;
              $quantity_option = @$val['quantity'] > 1 ? 'x ' . @$val['quantity'] . '' : '';
              $options_display .= '<p class="mb-0" style="font-size: 12px; color: #6e6e6e">' . @$val['name_display'] . ' ' . $quantity_option . ' ' . $price . ' </p>';
            };
          };
        }
        $list_cart[$key1][4] = $options_display;
      };

    if (@$_SESSION['discount'])
      $total_end = $total_price - $_SESSION['discount'] + $price_options_value;
    else
      $total_end = $total_price + $price_options_value;

    for ($i = 0; $i < count($mba); $i++) {
      $mba[$i]->province = $model->get_record_by_id($mba[$i]->province_id, 'fs_cities')->name;
      $mba[$i]->district = $model->get_record_by_id($mba[$i]->district_id, 'fs_districts')->name;
      // $mba[$i]->ward = $model->get_record_by_id($mba[$i]->ward_id, 'fs_wards')->name;
    }
    $province = $model->getProvince();


    $breadcrumbs = array();
    $breadcrumbs[] = array(
      0 => FSText::_('Thanh toán'),
      1 => ""
    );

    global $tmpl;
    // $tmpl -> assign('breadcrumbs', $breadcrumbs);

    include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
  }
  function get_options($cat_child)
  {

    $model = $this->model;
    $temp = $this->model->get_records("id in (0" . $cat_child->options . "0)", "fs_products_options", "*", "ordering asc");

    $list_options = array();

    $temp = $this->cleanArray($temp);
    $list_options = array();

    if (!empty($temp)) {
      foreach ($temp as $key => $item) {
        $list_options[$item->id] = $model->get_records("parent_id = $item->id ", 'fs_products_values', '*', 'id asc');
      };
    }

    return $list_options;
  }
  function voucher()
  {
    $voucher = FSInput::get("voucher");
    $total = FSInput::get("total");
    $fstable = FSFactory::getClass('fstable');

    if (!$voucher) {
      $res = array(
        'voucher_detail' => 'Bạn chưa nhập mã giảm giá',
        // 'total' => format_money($total),
        'discount' => 0,
        'error' => true
      );
      echo json_encode($res);
      return;
    }
    $date = date("Y-m-d");

    $voucher_detail = $this->model->get_record("code = '$voucher' AND limit_used > 0", 'fs_discount');

    if ($voucher_detail) {
      if ($voucher_detail->order_price > $total) {
        unset($_SESSION['voucher']);
        unset($_SESSION['discount']);
        $res = array(
          'voucher_detail' => 'Đơn hàng phải lớn hơn ' . format_money($voucher_detail->order_price),
          // 'total' => format_money($total),
          'discount' => 0,
          'error' => true
        );
        echo json_encode($res);
        return;
      } 
  
      if ($voucher_detail && $voucher_detail->unit == 2) {
        $discount = $total * $voucher_detail->discount / 100;
        if ($discount > $voucher_detail->maximum) {
          $_SESSION['discount'] = $voucher_detail->maximum;
        } else {
          $_SESSION['discount'] = $discount;
        }
        $_SESSION['voucher'] = $voucher_detail;

      } else if ($voucher_detail && $voucher_detail->unit == 1) {
        $_SESSION['discount'] = $voucher_detail->discount;
        $_SESSION['voucher'] = $voucher_detail;
      }
  
      if ($total >= $_SESSION['discount'] && $voucher_detail && $voucher_detail->created_time <= $date && $voucher_detail->expiration_date > $date) {
        $total = $total - $_SESSION['discount'];
        $_SESSION['total_price'] = $total;
        $_SESSION['voucher'] = $voucher_detail;

        $res = array(
          'voucher_detail' => $voucher_detail->name,
          'total' => format_money($total),
          'discount' => $_SESSION['discount'],
          'error' => false
        );
      } else {
        unset($_SESSION['voucher']);
        unset($_SESSION['discount']);
        $res = array(
          'voucher_detail' => 'Mã giảm giá không hợp lệ',
          'total' => format_money($total),
          'discount' => 0,
          'error' => true
        );
        echo json_encode($res);
        return;
      }
      $temp = '';
  
      foreach ($_SESSION['cart'] as $key => $item) {
        $temp .= "," . $item[0];
        if ($key + 1 == count($_SESSION['cart'])) {
          $temp .= ",";
        }
      }
      $products_promotion2 = $this->model->get_records("id in (0" . $temp . "0)", $fstable->_('fs_products', 1), "id");
      sort($products_promotion2);
  
      foreach($products_promotion2 as $item)
      {
          $prd2[] = $item->id;
      }
      $str1 = implode(',',$prd2);
      if ($voucher_detail->choose_type == 1) {
        $cat = $this->model->get_record_by_id($voucher_detail->category_promotion, 'fs_promotion_type_categories');
        $products_promotion = $this->model->get_records("id in (0" . $cat->products_related . "0)", $fstable->_('fs_products', 1), "id");
        sort($products_promotion);
        foreach($products_promotion as $item)
        {
            $prd[] = $item->id;
        }
        $check = false;
        foreach($prd2 as $item) {
          foreach($prd as $val) {
            if ($item == $val) {
              $check = true;
              break;
            } else {
              $check = false;
            }
          }
        }
        if ($check) {
          $res = array(
            'voucher_detail' => $voucher_detail->name,
            'total' => format_money($total),
            'discount' => $_SESSION['discount'],
            'error' => false
          );
          $_SESSION['voucher'] = $voucher_detail;

        } else {
          unset($_SESSION['voucher']);
          unset($_SESSION['discount']);
          $res = array(
            'voucher_detail' => 'Giỏ hàng có sản phẩm không hợp lệ',
            'total' => format_money($total),
            'discount' => 0,
            'error' => true
          );
          echo json_encode($res);
          return;
        }
      }
      if ($voucher_detail->choose_type == 2) {

        $products_promotion = $this->model->get_records("id in (0" . $voucher_detail->apply_for . "0)", $fstable->_('fs_products', 1), "id");
        sort($products_promotion);
        foreach($products_promotion as $item)
        {
            $prd[] = $item->id;
        }
        $check = false;
        foreach($prd2 as $item) {
          foreach($prd as $val) {
            if ($item == $val) {
              $check = true;
              break;
            } else {
              $check = false;
            }
          }
        };
        if ($check) {
          $res = array(
            'voucher_detail' => $voucher_detail->name,
            'total' => format_money($total),
            'discount' => $_SESSION['discount'],
            'error' => false
          );
          $_SESSION['voucher'] = $voucher_detail;

        } else {
          unset($_SESSION['voucher']);
          unset($_SESSION['discount']);
          $res = array(
            'voucher_detail' => 'Giỏ hàng có sản phẩm không hợp lệ',
            'total' => format_money($total),
            'discount' => 0,
            'error' => true
          );
          echo json_encode($res);
          return;
        }
      }
    } else {
      $_SESSION["total_price"] = $total;
      unset($_SESSION['voucher']);
      unset($_SESSION['discount']);
  
      $res = array(
        'voucher_detail' => 'Mã giảm giá không hợp lệ',
        'total' => format_money($total),
        'discount' => 0,
        'error' => true
      );
    }


    echo json_encode($res);
  }
  function set_info_signin()
  {
    $model = $this->model;
    global $user;
    $id_address = FSInput::get('id-members-address');

    $row = array();
    $info = $model->get_record_by_id($id_address, "fs_members_address");
    $row['vat'] = FSInput::get('vat', 0, 'int');
    $row['name'] = $info->first_name . ' ' . $info->last_name;
    // $row['last_name'] = $info->last_name;

    $row['telephone'] = $info->telephone;
    $row['address'] = $info->content;
    $row['email'] = $user->userInfo->email;
    $row['province_id'] = $info->province_id;
    $row['district_id'] = $info->district_id;
    // $row['wards_id'] = $info->ward_id;
    $row['user_id'] = $user->userID;
    $row['receive_date'] = FSInput::get('receive_date');
    // if(FSInput::get('fcompany-member')){
    //   $row['company'] = FSInput::get('fcompany-member');
    // }
    // if(FSInput::get('faddress-company-member')){
    //   $row['address-company'] = FSInput::get('faddress-company-member');
    // }
    // if(FSInput::get('ftax_code-member')){
    //   $row['tax_code'] = FSInput::get('ftax_code-member');
    // }
    // if(FSInput::get('fnote-member')){
    //   $row['note'] = FSInput::get('fnote-member');
    // }

    $_SESSION['info_guest'] = $row;
    // $this->id = $model->save();
    // if (!$this->id) {
    //   setRedirect(URL_ROOT, 'Có lỗi trong quá trình đặt hàng, bạn vui lòng thử lại', 'error');
    // }
    // $this->success();
    setRedirect('index.php?module=payment&view=payment&Itemid=3&task=step2_payment', '', '');
  }

  function set_info()
  {
    global $user;
    $model = $this->model;

    $row = array();
    $row['username'] = addslashes(FSInput::get('fullname'));
    $row['telephone'] = addslashes(FSInput::get('phone'));
    $row['content'] = addslashes(FSInput::get('address'));
    $row['email'] = addslashes(FSInput::get('email'));
    $row['province_id'] = FSInput::get('fprovince', 0, 'int');
    $row['district_id'] = FSInput::get('fdistrict', 0, 'int');
    $row['note'] = addslashes(FSInput::get('note'));
    $row['receive_date'] = addslashes(FSInput::get('receive_date'));

    $info_guest['name'] = $row['username'];
    $info_guest['telephone'] = $row['telephone'];
    $info_guest['email'] = $row['email'];

    if ($user->userID) {
      $info_guest['user_id'] = $user->userID;
    }

    $info_guest['address'] = $row['content'];
    $info_guest['province_id'] = $row['province_id'];
    $info_guest['district_id'] = $row['district_id'];
    $info_guest['receive_date'] = $row['receive_date'];
    if (!empty($row['note'])) {
      $info_guest['note_adc'] = $row['note'];
    }


    $_SESSION['info_guest'] = $info_guest;

    $this->step2_payment();
  }

  function step2_payment()
  {
    $model = $this->model;
    if (!empty($_SESSION["cart"])) {
      $list_cart = $_SESSION["cart"];
      $data = array();

      $total_price = 0;
      $total_end = 0;
      $price_options_value = 0;

      if (empty($_SESSION['cart']))
        setRedirect(URL_ROOT, 'Giỏ hàng trống');
      $list_cart = $_SESSION['cart'];

      $data = array();


      $total_price = 0;
      $total_end = 0;
      $data = array();
      if (!empty($list_cart))
        foreach ($list_cart as $key1 => $item) {
          $data[$item[0]] = $model->get_record_by_id($item[0], 'fs_products');
          $products_info = $data[$item[0]];
          $total = 0;
          $total = $products_info->price * $item[1];
          $total_price += $total;
          $options_display = '';

          if (!empty($item[3])) {
            foreach ($item[3] as $key => $val) {
              if (empty($val['id'])) {
                foreach ($val as $val2) {
                  $price = @$val2['price'] != "0.00" ? format_money_0(@$val2['quantity'] * @$val2['price']) : null;
                  $quantity_option = @$val2['quantity'] > 1 ? 'x ' . @$val2['quantity'] . '' : '';
                  $options_display .= '<p class="mb-0" style="font-size: 12px; color: #6e6e6e">' . @$val2['name_display'] . ' ' . $quantity_option . ' ' . $price . ' </p>';
                };
              } else {
                $price = @$val['price'] != "0.00" ? format_money_0(@$val['quantity'] * @$val['price']) : null;
                $quantity_option = @$val['quantity'] > 1 ? 'x ' . @$val['quantity'] . '' : '';
                $options_display .= '<p class="mb-0" style="font-size: 12px; color: #6e6e6e">' . @$val['name_display'] . ' ' . $quantity_option . ' ' . $price . ' </p>';
              };
            };
          }
          $list_cart[$key1][4] = $options_display;
        };
        $fee_shipping = $model->get_record_by_id($_SESSION['info_guest']['district_id'], 'fs_districts')->fee_shipping;

      if (@$_SESSION['discount'])
        $total_end = $total_price - $_SESSION['discount'] + $fee_shipping + $price_options_value;
      else
        $total_end = $total_price + $price_options_value + $fee_shipping;
    }

    include 'modules/' . $this->module . '/views/' . $this->view . '/payment_step2.php';
  }
  function confirm()
  {
    $model = $this->model;

    if (empty($_SESSION['cart'])) {
      $msg = FSText::_('Không có sản phẩm trong giỏ hàng');
      setRedirect(URL_ROOT, $msg, 'error');
    }
    $row['shipping'] = addslashes(FSInput::get('fshipping'));
    if($row['shipping'] == 1)
      $fee_shipping = $model->get_record_by_id($_SESSION['info_guest']['district_id'], 'fs_districts')->fee_shipping;
    else
      $fee_shipping = 0;

    $_SESSION['info_shipping'] = $row;
    $shipping = 0;
    $shipping =  $_SESSION['info_shipping']['shipping'] == 1 ? $fee_shipping : 0;
    $_SESSION['total_end'] = $_SESSION['total_price'] + $shipping;

    $total_price = 0;
    $price_options_value = 0;
    if (!empty($_SESSION["cart"])) {
      $list_cart = $_SESSION["cart"];
      $data = array();
      foreach ($list_cart as $key1 => $item) {
        $data[$item[0]] = $model->get_record_by_id($item[0], 'fs_products');
        $products_info = $data[$item[0]];
        $total = 0;
        $total = $products_info->price * $item[1];
        $total_price += $total;
        $options_display = '';

        if (!empty($item[3])) {
          foreach ($item[3] as $key => $val) {
            if (empty($val['id'])) {
              foreach ($val as $val2) {
                $price = @$val2['price'] != "0.00" ? format_money_0(@$val2['quantity'] * @$val2['price']) : null;
                $quantity_option = @$val2['quantity'] > 1 ? 'x ' . @$val2['quantity'] . '' : '';
                $options_display .= '<p class="mb-0" style="font-size: 12px; color: #6e6e6e">' . @$val2['name_display'] . ' ' . $quantity_option . ' ' . $price . ' </p>';
                $price_options_value += @$val2['quantity'] * @$val2['price'];
              };
            } else {
              $price_options_value += @$val['quantity'] * @$val['price'];
              $price = @$val['price'] != "0.00" ? format_money_0(@$val['quantity'] * @$val['price']) : null;
              $quantity_option = @$val['quantity'] > 1 ? 'x ' . @$val['quantity'] . '' : '';
              $options_display .= '<p class="mb-0" style="font-size: 12px; color: #6e6e6e">' . @$val['name_display'] . ' ' . $quantity_option . ' ' . $price . ' </p>';
            };
          };
        }
        $list_cart[$key1][4] = $options_display;
      }
    }

    if (@$_SESSION['discount'])
      $total_end = $total_price - $_SESSION['discount'] + $fee_shipping + $price_options_value;
    else
      $total_end = $total_price + $price_options_value + $fee_shipping;
  
    // echo $fee_shipping;die;

    include 'modules/' . $this->module . '/views/' . $this->view . '/payment_step3.php';
  }
  function last_step()
  {
    $model = $this->model;
    $this->id = $model->save();
    if (!$this->id) {
      setRedirect(URL_ROOT, 'Có lỗi trong quá trình đặt hàng, bạn vui lòng thử lại', 'error');
    }
    $this->success();
  }
  function success()
  {
    $link = URL_ROOT;
    // print_r($_SESSION);die;
    if (empty($_SESSION['cart'])) {
      $msg = FSText::_('Không có sản phẩm trong giỏ hàng');
      setRedirect($link, $msg, 'error');
      return;
    } else {
      $list_cart = $_SESSION['cart'];
      $info_guest = $_SESSION['info_guest'];
    }
    // $id = $this->id;
    unset($_SESSION['cart']);

    include 'modules/' . $this->module . '/views/' . $this->view . '/success.php';
  }
}
