<?php
class PaymentModelsPayment extends FSModels
{
  function __construct()
  {
    parent::__construct();

    $fstable = FSFactory::getClass('fstable');
    $this->table_name = $fstable->_('fs_cities', 1);
    $this->table_products = $fstable->_('fs_products', 1);
    $this->table_order = $fstable->_('fs_order', 1);
    $this->table_province = $fstable->_('fs_cities', 1);
    $this->table_units = $fstable->_('fs_products_units', 1);
  }
  function getMeasure($id)
  {
    $where = "";
    $where .= " AND id = " . $id;
    $query = " SELECT *
            FROM " . $this->table_units . " 
            WHERE published = 1 " . $where . "";
    global $db;
    $sql = $db->query($query);
    $result = $db->getObject();
    return $result;
  }
  function getAllMemberAddress()
  {
    global $db, $user;
    $user_id = '';
    if ($user->userID) {
      $user_id = $user->userID;
    }
    $sql = " SELECT *
        FROM fs_members_address
        WHERE member_id  = '$user_id'";
    // $db->query($sql);
    //echo $sql;die;
    return $db->getObjectList($sql);
  }
  function getProvince()
  {
    global $db;
    $query = "SELECT *
                FROM " . $this->table_province . "
                where published = 1
                ORDER BY ordering ASC
      ";
    $db->query($query);
    $result = $db->getObjectList();
    return $result;
  }
  function getProduct($id)
  {
    $where = "";
    $where .= " AND id = " . $id;
    $query = " SELECT id,name, alias,price,price_old,image,category_alias,discount,weight, category_id
						FROM " . $this->table_products . " 
						WHERE published = 1 " . $where . "";
    global $db;
    $sql = $db->query($query);
    $result = $db->getObject();
    return $result;
  }

  function getProduct2($id)
  {
    global $db;
    $query = "  SELECT a.*,b.name as product_name, b.alias as product_alias,b.category_alias,b.weight, b.image, b.alias, b.old_id
					FROM fs_order_items AS a
					INNER JOIN fs_products AS b on a.product_id = b.id
					WHERE
						a.order_id = $id
					";
    $db->query($query);
    $result = $db->getObjectList();
    return $result;
  }
  function save()
  {
    global $db, $user, $config;;
    // $pay_book = FSInput::get('pay_type', '1', 'int');
    $info_guest = $_SESSION['info_guest'];
    $list_cart = $_SESSION['cart'];

    $pay_type = FSInput::get('pay_type', '1', 'int');

    $row = array();

    $row['name'] = $info_guest["name"];
    $row['email'] = $info_guest["email"];
    $row['telephone'] = $info_guest["telephone"];
    $row['address'] = $info_guest["address"];
    $row['district'] = $this->get_record('id=' . $info_guest["district_id"], 'fs_districts')->name;
    // $row['wards'] =  $this->get_record('id=' . $info_guest["wards_id"], 'fs_wards')->name;
    $row['province'] =  $this->get_record('id=' . $info_guest["province_id"], 'fs_cities')->name;
    $row['payment_method'] = $pay_type;
    $row['published'] = 1;
    $row['shipping'] = $_SESSION['info_shipping']['shipping'];

    $row['receive_date'] = date("Y-m-d H:i:s", strtotime($info_guest["receive_date"]));
    if (!empty($info_guest["user_id"])) {
      $row['user_id'] = $info_guest["user_id"];
    }

    if (!empty($info_guest["note_adc"])) {
      $row['note_adc'] = $info_guest["note_adc"];
    }
    if (!empty($info_guest['company']) && !empty($info_guest['address-company']) && !empty($info_guest['tax-code'])) {
      $row['name_company'] = $info_guest["company"];
      $row['address_company'] = $info_guest["address-company"];
      $row['tax_code'] = $info_guest["tax-code"];
    }

    $products_count = 0;
    $total = 0;

    foreach ($list_cart as $item) {
      $prd_id_array[] = $item[0];
      $data = $this->getProduct($item[0]);
      $products_count += $item[1];
      $total += $item[1] * $data->price;
    }

    $row['total_end'] = $_SESSION['total_end'];
    if (!empty(@$_SESSION['voucher']))
      $row['discount_code'] = @$_SESSION['voucher']->id;
    $row['products_count'] = $products_count;

    $vat = 0.1;
    $prd_id_str = implode(',', $prd_id_array);
    $row['products_id'] = $prd_id_str;
    $row['products_count'] = $products_count;
    // if ($row['vat'] == 1) {
    //   $row['total_end'] = $total + $total*$vat;
    // } else {
    //   $row['total_end'] = $total;
    // }

    $time = date("Y-m-d H:i:s");
    $row['created_time'] = $time;

    if (!isset($_SESSION['cart']))
      return false;

    $id = $this->_add($row, $this->table_order);


    if ($id) {
      $this->save_order_items($id);

      if (!empty(@$_SESSION['voucher'])) {
        $row2['limit_used'] = @$_SESSION['voucher']->limit_used - 1;
        $rs = $this->_update($row2, 'fs_discount', 'id = ' . @$_SESSION['voucher']->id . '');
      }

      unset($_SESSION['voucher']);
      unset($_SESSION['discount']);
      unset($_SESSION['total_end']);
      $this->send_mail_order($id, $list_cart, $row, $info_guest["email"]);
    }

    return $id;
  }
  function save_order_items($order_id)
  {
    if (!$order_id)
      return false;

    global $db;

    // remove before update or inser
    $sql = " DELETE FROM fs_order_items
					WHERE order_id = '$order_id'";

    $db->query($sql);
    $rows = $db->affected_rows();

    // insert data
    $prd_id_array = array();
    // Repeat estores
    if (!isset($_SESSION['cart']))
      return false;

    $product_list = $_SESSION['cart'];
    $sql = " INSERT INTO fs_order_items (order_id,product_id,price, price_old, count,total, options, price_has_options)
					VALUES ";

    $array_insert = array();

    // Repeat products
    for ($i = 0; $i < count($product_list); $i++) {
      $prd = $product_list[$i];
      $product = $this->getProduct($prd[0]);
      $this->update_number_buy($prd[0]);

      $total = $product->price * $prd[1];
      $price = $product->price;
      $price_old = $product->price_old;
      $options2 = serialize($prd[3]);


      foreach ($prd[3] as $item) {
        $price_has_options = 0;
        if (!empty($prd[3]) && is_array($prd[3])) {
          foreach ($prd[3] as $key => $val) {
            if (empty($val['id'])) {
              foreach ($val as $val2) {
                $price_has_options += $val2['price'] * $val2['quantity'];
              };
            } else {
              $price_has_options += $val['price'] * $val['quantity'];
            };
          };
        }
        $price_has_options += $total;
      }

      if(!empty($price_has_options))
        $array_insert[] = "('$order_id','$prd[0]','$price', '$price_old','$prd[1]','$total', '$options2', '$price_has_options') ";
      else
        $array_insert[] = "('$order_id','$prd[0]','$price', '$price_old','$prd[1]','$total', '$options2', 0) ";
    }
    if (count($array_insert)) {
      $sql_insert = implode(',', $array_insert);
      // print_r($sql_insert);die;
      $sql .= $sql_insert;
      $db->query($sql);
      $rows = $db->affected_rows();
      return true;
    } else {
      return;
    }
  }
  function update_number_buy($product_id)
  {
    if (USE_MEMCACHE) {
      $fsmemcache = FSFactory::getClass('fsmemcache');
      $mem_key = 'array_hits';

      $data_in_memcache = $fsmemcache->get($mem_key);
      if (!isset($data_in_memcache))
        $data_in_memcache = array();
      if (isset($data_in_memcache[$product_id])) {
        $data_in_memcache[$product_id]++;
      } else {
        $data_in_memcache[$product_id] = 1;
      }
      $fsmemcache->set($mem_key, $data_in_memcache, 10000);
    } else {
      if (!$product_id)
        return;

      // count
      global $db, $econfig;
      $sql = " UPDATE fs_products
                                    SET number_buy = number_buy + 1 
                                    WHERE  id = '$product_id' 
                             ";
      $db->query($sql);
      $rows = $db->affected_rows();
      return $rows;
    }
  }
  function send_mail_order($id, $list_cart, $row, $email)
  {
    // body
    global $config;
    $body = '';
    $body .= $config['order_mail'];
    $body = str_replace('{name_pay}', $row['name'], $body);
    $body = str_replace('{time}', $row['created_time'], $body);
    $body = str_replace('{mail_pay}', $row['email'], $body);
    $body = str_replace('{phone_pay}', $row['telephone'], $body);
    $body = str_replace('{id_order}', 'DH' . str_pad($id, 8, "0", STR_PAD_LEFT), $body);

    if ($row['payment_method'] == 1) {
      $payType = 'Thanh toán khi giao hàng';
    }
    $body = str_replace('{pay_type}', $payType, $body);
    $body = str_replace('{address_pay}', $row['address'], $body);

    $fee_shipping = $this->get_record_by_id($_SESSION['info_guest']['district_id'], 'fs_districts')->fee_shipping;


    if ($row['shipping'] == 1) {
      $body = str_replace("{shipping}", format_money($fee_shipping), $body);
    } else if ($row['shipping'] == 2) {
      $body = str_replace("{shipping}", 'Không mất phí', $body);
    }
    $bodyitem2 = '<table border="0" cellpadding="0" cellspacing="0" style="background: #81954E" width="100%">
										<thead>
											<tr>
												<th align="left" bgcolor="#81954E" style="padding:6px 9px;color:#fff;font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:14px">Sản phẩm</th>
												<th align="center" bgcolor="#81954E" style="padding:6px 9px;color:#fff;font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:14px">Đơn giá</th>
												<th align="center" bgcolor="#81954E" style="padding:6px 9px;color:#fff;font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:14px">Số lượng</th>
												<th align="right" bgcolor="#81954E" style="padding:6px 9px;color:#fff;font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:14px">Tổng tạm</th>
											</tr>
										</thead>
										<tbody bgcolor="#eee" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#444;line-height:18px">
										{item2}
										</tbody>
									</table>';
    $bodyitem = "";

    $i = 1;
    $total = 0;
    foreach ($list_cart as $item) {
      //var_dump($list_cart);die;
      // $size = $item[2] != ''?'Size '.$item[2].'':'';
      // $mau = $item[2] != '' ? 'Màu ' . $item[2] . '' : '';

      $prd_id_array[] = $item[0];
      $data = $this->getProduct($item[0]);
      $options_display = '';

      $options_price = 0;
      if (!empty($item[3])) {
        foreach ($item[3] as $key => $val) {
          if (empty($val['id'])) {
            foreach ($val as $val2) {
              $price = @$val2['price'] != "0.00" ? format_money_0(@$val2['quantity'] * @$val2['price']) : null;
              $quantity_option = @$val2['quantity'] > 1 ? 'x ' . @$val2['quantity'] . '' : '';
              $options_display .= '<p class="mb-0" style="font-size: 10px; color: #6e6e6e; margin-top: 0px; margin-bottom: 0px">' . @$val2['name_display'] . ' ' . $quantity_option . ' ' . $price . ' </p>';
              $options_price += $val2['price'] * @$val2['quantity'];
            };
          } else {
            $price = @$val['price'] != "0.00" ? format_money_0(@$val['quantity'] * @$val['price']) : null;
            $quantity_option = @$val['quantity'] > 1 ? 'x ' . @$val['quantity'] . '' : '';
            $options_display .= '<p class="mb-0" style="font-size: 10px; color: #6e6e6e; margin-top: 0px; margin-bottom: 0px">' . @$val['name_display'] . ' ' . $quantity_option . ' ' . $price . ' </p>';
            $options_price += $val['price'] * @$val['quantity'];
          };
        };
      }

      $total += ($data->price) * $item[1] + $options_price;

      if ($i % 2 == 1) {
        $style = 'background: #fff;';
      } else
        $style = '';

      $bodyitem .= '<tr style="' . $style . '">
                                <td class="clearfix" style="padding: 5px">';
      $bodyitem .= '                  <a style="text-decoration: none" class="clearfix" href="' . FSRoute::_('index.php?module=products&view=product&ccode=' . $data->category_alias . '&code=' . $data->alias . '&id=' . $data->id) . '">';
      $bodyitem .= $data->name . $options_display . '
                                    </a>
                                </td>
                                <td class="text-center" style="text-align: center">' . format_money($data->price) . '</td>
                                <td class="text-center" style="text-align: center">' . $item[1] . ' </td>
                                <td class="text-right" style="text-align: right">' . format_money($data->price * $item[1])  . '</td>
                            </tr>';
      $i++;
    }
    $bodyitem2 = str_replace("{item2}", $bodyitem, $bodyitem2);
    $body = str_replace("{item}", $bodyitem2, $body);
    // $body = str_replace("{options}", format_money($_SESSION['price_options_value']), $body);
    $body = str_replace('{money}', format_money($row['total_end']), $body);
    $body = str_replace("{total}", format_money($total), $body);
    // $body = str_replace("{discount}", 0, $body);

    if(@$row['discount_code'])
    $voucher_detail = $this->get_record_by_id($row['discount_code'], 'fs_discount');

    $discount = 0;
    if (@$voucher_detail && @$voucher_detail->unit == 2) {
      $discount = $total * @$voucher_detail->discount / 100;
      if (@$discount > @$voucher_detail->maximum) {
        $discount = @$voucher_detail->maximum;
      }
    } else if (@$voucher_detail && @$voucher_detail->unit == 1) {
      $discount = @$voucher_detail->discount;
    }

    $body = str_replace("{discount}", '- ' . format_money($discount), $body);

    $admin_email = 'mail.finalstyle.noreply@gmail.com';
    $rs = $this->send_email1("Oganic - Xác nhận đơn hàng DH" . str_pad($id, 8, "0", STR_PAD_LEFT), $body, $row['name'], $email, $admin_email, 1);
    if (!$rs)
      return false;
    return true;

    //en
  }

  function get_options($cat_child)
  {

    $temp = $this->get_records("id in (0" . $cat_child->options . "0)", "fs_products_options", "*", "ordering asc");

    $list_options = array();

    $temp = $this->cleanArray($temp);
    $list_options = array();

    if (!empty($temp)) {
      foreach ($temp as $key => $item) {
        $list_options[$item->id] = $this->get_records("parent_id = $item->id ", 'fs_products_values', '*', 'id asc');
      };
    }

    return $list_options;
  }

  function cleanArray($array)
  {
    if (is_array($array)) {
      foreach ($array as $key => $sub_array) {
        $result = $this->cleanArray($sub_array);
        if ($result === false) {
          unset($array[$key]);
        } else {
          $array[$key] = $result;
        }
      }
    }

    if (empty($array)) {
      return false;
    }

    return $array;
  }
}
