$(document).ready(function () {
  let date = new Date();
  let str_date = `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`

  var alert_info = $('#alert_info').val();
  alert_info1 = alert_info ? JSON.parse(alert_info) : [];


  $("#datetime2").change(function () {
    $(".datetime1").val($(this).val())
  })
  $(".continue-step2").on('click', function () {
    let check_other_addess = $("#check_other_addess").val()
    if (check_other_addess === "0") {
      $("#select-address").submit();
    } else {
      if (validateStep1Payment()) {
        $("#form-step1").submit()
      }
    }
  })

  $("#datetime2").flatpickr({
    enableTime: true,
    dateFormat: "d-m-Y H:i",
    locale: "vn",
    minDate: new Date(),
    defaultDate: new Date()
  })
  $(".datetime1").val($("#datetime2").val())

  // $("#dtBox").DateTimePicker({
  //   language: 'vi',
  //   dateFormat: "dd-MM-yyyy",
  //   timeFormat: "HH:mm",
  //   dateTimeFormat: "dd-MM-yyyy HH:mm",
  //   minDateTime: str_date,
  //   addEventHandlers: function () {
  //     var dtPickerObj = this;
  //     dtPickerObj.setDateTimeStringInInputField($("#datetime2"));
  //   }
  // });
  setTimeout(() => {
    if ($(".address-member:checked").val() != "other-address" && $(".address-member:checked").val() != undefined) {
      $("#form-step1").css("display", "none")
    }
  }, 500)

  $(".address-member").on('change', function () {
    if ($(this).val() == "other-address") {
      $("#select-address").hide()
      $("#form-step1").css("display", "block")
      $("#check_other_addess").val(1)
    }
    else {
      $("#select-address").show()
      $("#form-step1").css("display", "none")
      $("#input-select-address").val($(this).attr("data-id"));
      $("#check_other_addess").val(0)
    }
  })

  $(".cart-payment-mobile").click(function() {
    $(".right-column-payment").slideToggle();
    $(".sidenav").slideToggle()

  })
})

function validateStep1Payment() {
  $('label.label_error').prev().remove();
  $('label.label_error').remove();

  if (!notEmpty("fullname", "Bạn chưa nhập tên")) {
    return false;
  }
  if (!isPhone("phone", "Bạn chưa nhập số điện thoại")) {
    return false;
  }
  // if (!emailValidator("email", alert_info1[3])) {
  //   return false;
  // }

  if (!notEmpty("address", "Bạn chưa nhập địa chỉ nhận hàng")) {
    return false;
  }

  return true;
}
$('#fprovince, #fcommune, #fdistrict').select2({
  minimumResultsForSearch: -1
});

$('#fprovince').select2({
  templateSelection: function (data, container) {
    let idProvince = $(data.element).attr('data-id');
    loadDistrict(idProvince)
    return data.text;
  }
});
$('#fdistrict').select2({
  templateSelection: function (data, container) {
    let idDistrict = $(data.element).attr('data-id');
    //   console.log(idDistrict);
    // loadCommunes(idDistrict)
    return data.text;
  }
});
$('#fcommune').select2({
  templateSelection: function (data, container) {

    return data.text;
  }
});
function loadDistrict(idProvince) {
  $.ajax({
    type: 'get',
    url: '/index.php?module=members&view=members&raw=1&task=ajax_load_district',
    // contentType: "application/json",
    dataType: "text",
    data: { idCity: idProvince },
    success: function (data) {
      // console.log(data);
      selectDistrict(JSON.parse(data))
      return true;
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
      console.log(textStatus)
    }
  });
}

function selectDistrict(data) {
  let option = ''
  data.forEach(item => {
    option += `<option data-id="${item.id}" value="${item.id}">${item.name}</option>`
  })
  $("#fdistrict").html(option);
}
function selectCommune(data) {
  let option = ''
  data.forEach(item => {
    option += `<option data-id="${item.id}" value="${item.id}" >${item.name}</option>`
  })
  $("#fcommune").html(option);
}

let cacheDom =
  `
<div class="col no-gutters input-row">
    <label for="fcompany">Công ty/Tổ chức <span class="red--text bold">*</span></label><br />
    <input type="text" id="fcompany" name="fcompany" placeholder="Tên công ty hoặc tổ chức"><br />
</div>
<div class="w-100"></div>
    <div class="col no-gutters input-row">
    <label for="faddress-company">Địa chỉ Công ty/Tổ chức <span class="red--text bold">*</span></label><br />
<input type="text" id="faddress-company" name="faddress-company" placeholder="Địa chỉ của công ty/tổ chức"><br />
</div>
<div class="w-100"></div>
    <div class="col no-gutters input-row">
    <label for="ftax">Mã số thuế</label><br />
    <input type="text" id="ftax" name="ftax_code" placeholder="Mã số thuế công ty"><br />
</div>
`

let getDefaultAddress = $("input[name='faddress-member']:checked").attr("data-id")
$("#input-select-address").val(getDefaultAddress);
// default address is not exist
if (!!getDefaultAddress) {
  $("#check_other_addess").val(0)
} else {
  $("#check_other_addess").val(1)
}

