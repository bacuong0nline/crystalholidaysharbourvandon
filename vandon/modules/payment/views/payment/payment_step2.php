<?php
global $tmpl;

$global_class = FSFactory::getClass('FsGlobal');
$config = $global_class->get_all_config();

$tmpl->addStylesheet('select2.min', 'modules/payment/assets/css');
$tmpl->addStylesheet('payment', 'modules/payment/assets/css');

$tmpl->addScript('select2.min', 'modules/payment/assets/js');
$tmpl->addScript('shipping', 'modules/payment/assets/js');

// foreach ($list_cart as $item) {
//   $quantity = $item[1];
//   $prod = $data[$item[0]];
//   $total += $quantity * $prod->price;
// }

$total_price = 0;
$vat = 0.1;
$check_total_price = false;
$total_price = 0;
$quantity = 0;
if (isset($_SESSION['cart'])) {
  $product_list = $_SESSION['cart'];
  foreach ($product_list as $item) {
    $quantity += $item[1];
  }
}

$alert_info = array(
  0 => FSText::_('Nhập Từ Khóa'),
  1 => FSText::_('Bạn chưa nhập họ và tên'),
  2 => FSText::_('Bạn chưa nhập tên công ty'),
  3 => FSText::_('Bạn chưa nhập email'),
  4 => FSText::_('Email không hợp lệ'),
  5 => FSText::_('Bạn chưa nhập số điện thoại'),
  6 => FSText::_('Số điện thoại không hợp lệ'),
  7 => FSText::_('Vui lòng nhập địa chỉ chi tiết'),
  8 => FSText::_('Vui lòng nhập tên công ty'),
  9 => FSText::_('Vui lòng nhập địa chỉ công ty')
);

?>

<input type="hidden" id="alert_info" value='<?php echo json_encode($alert_info) ?>' />

<main>
  <div class="cart-payment-mobile">
    <div class="count-products" style="top: 5px; right: 10px;">
      <?php echo $quantity ?>
    </div>
    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
  </div>
  <form id="form-step2" action="index.php?module=payment&view=payment&Itemid=3&task=confirm" method="post">
    <div class="container container-payment d-flex pl-0 pr-0 pt-3">
      <div class="left-column-payment col-md-8">
        <div class="col-md-12 d-flex justify-content-between align-items-center mb-3">
          <a href="<?php echo URL_ROOT ?>" class="logo">
            <img src="/<?php echo $config['logo'] ?>" alt="logo" class="logo-image" />
          </a>
          <img class="step-image" src="/images/step2.svg" alt="payment-step1" style="width: auto" />
        </div>
        <div class="list-address-member">
          <div class="pb-3">
            <h6 style="font-weight: 700" class="text-uppercase"><?php echo FSText::_("Tùy chọn giao hàng") ?></h6>
            <a style="font-weight: 600" onclick="window.history.back()" class="link">
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                <path id="Path_505" data-name="Path 505" d="M26.611,15.509l-1.44,1.441a.37.37,0,0,1-.531,0l-3.473-3.44a.372.372,0,0,1,0-.532l1.44-1.448a1.512,1.512,0,0,1,2.126,0l1.854,1.856a1.492,1.492,0,0,1,.025,2.122Zm-2.675,2.153-3.472-3.477a.37.37,0,0,0-.531,0l-8.218,8.222-.661,3.8a.754.754,0,0,0,.871.872l3.8-.668,8.212-8.222a.372.372,0,0,0,0-.532Zm-8.65,7.813-2.02.353-.97-.971.352-2.023H13.8v1.5h1.5Zm5.06-8.593L15.527,21.7a.439.439,0,0,1-.618-.619l4.819-4.819a.439.439,0,0,1,.618.619Z" transform="translate(-11.042 -11.095)" fill="#81954e" />
              </svg>
              <?php echo FSText::_("Thay đổi địa chỉ nhận hàng") ?>
            </a>
          </div>
          <h6 style="font-weight: 700" class="text-uppercase"><?php echo FSText::_("Cách thức nhận hàng") ?></h6>
          <div class="row no-gutters address-1 style-3">
            <label class="radio-container col-md-10 no-gutters address-1__div-info mt-3 mb-3">
              <input data-shipping-fee="<?php echo $fee_shipping ? $fee_shipping : 0 ?>" data-currency="<?php echo $fee_shipping ?>" style="position: absolute; top: 45%" type="radio" name="fshipping" value="1" class="fshipping" checked="">
              <div style="margin-left: 30px">
                <p class="mb-0"><b><?php echo FSText::_("Dịch vụ ship của cửa hàng") ?> <?php echo format_money($fee_shipping) ?></b></p>
                <p class="mb-0"><span class="grey--text"><?php echo FSText::_("Nhận hàng ngay trong ngày") ?></p>
              </div>
              <span class="style-radio"></span>
            </label>
            <label class="radio-container col-md-10 no-gutters address-1__div-info mt-3 mb-3">
              <input data-shipping-fee="0" data-currency="Miễn phí" style="position: absolute; top: 45%" type="radio" name="fshipping" value="2" class="fshipping">
              <div style="margin-left: 30px">
                <p class="mb-0"><b><?php echo FSText::_("Tôi sẽ đến lấy hàng (Không mất phí)") ?></b></p>
                <p><?php echo FSText::_("(Khách hàng đến cửa hàng để nhận đơn)") ?></p>
              </div>
              <span class="style-radio"></span>
            </label>
          </div>
          <h6 style="font-weight: 700" class="text-uppercase"><?php echo FSText::_("Phí vận chuyển") ?></h6>
          <p><?php echo format_money($fee_shipping) ?></p>
        </div>
        <div class="mt-4">
          <a class="continue-step3"><?php echo FSText::_("Tiếp tục") ?></a>
        </div>
      </div>
      <div class="right-column-payment col-md-4">
        <div id="cartModal-payment" class="sidenav sidenav-payment">
          <div class="d-flex ml-2 mb-3">
            <h6 class="font-weight-bold text-uppercase d-flex align-items-center justify-content-center"><?php echo FSText::_("Giỏ hàng") ?></h6>
          </div>
          <div class="line"></div>
          <?php if (!empty($list_cart)) { ?>
            <div style="height: 70%; overflow: auto;" class="cart-items" id="style-3">
              <?php
              foreach ($list_cart as $key1 => $row) {
                $products_info = $data[$row[0]];
                $total = 0;
                $total = $products_info->price * $row[1];
                $total_price += $total;
                if ($products_info->price == 0) {
                  $check_total_price = true;
                }
                foreach ($row[3] as $item2) {
                  if (!is_array(@$item2[0])) {
                    $total_price += $item2['price'] * $item2['quantity'];
                  } else {
                    foreach ($item2 as $val) {
                      $total_price += $val['price'] * $val['quantity'];
                    }
                  }
                }
                $cat_child = $model->get_record_by_id($products_info->category_id, 'fs_products_categories');
                $list_options = $this->get_options($cat_child);
                @$name = '';

                if (!empty($list_options)) {
                  $i = 0;
                  foreach ($list_options as $key => $item) {
                    $obj = $model->get_record_by_id($key, 'fs_products_options');
                    if ($i > 0) {
                      @$name .= ', ';
                    }
                    @$name .= $obj->name;
                    $i++;
                  };
                }
              ?>
                <?php echo $tmpl->cart_item($products_info, $row, $key1, $total, @$name) ?>
              <?php } ?>
            </div>
            <div class="footer-cart">
              <!-- <a class="font-weight-bold font-italic discount-btn"><?php echo FSText::_("+ Mã giảm giá") ?></a> -->
              <div class="line-discount"></div>
              <div class="d-flex justify-content-between">
                <p><?php echo FSText::_("Tạm tính") ?></p>
                <p data-total="<?php echo $total_price ?>" class="font-weight-bold total-end"><?php echo format_money($total_price) ?></p>
              </div>
              <div class="d-flex justify-content-between voucher-box" style="display: <?php echo !empty($_SESSION['voucher']) ? '' : 'none !important' ?>">
                <p><?php echo FSText::_("Mã giảm giá") ?></p>
                <p class="font-weight-bold voucher-end"><?php echo '-' . format_money($_SESSION['discount']) ?></p>
              </div>
              <div class="d-flex justify-content-between" style="display: <?php echo !empty($price_options_value) ? '' : 'none !important' ?>">
                <p><?php echo FSText::_("Lựa chọn thêm") ?></p>
                <p class="font-weight-bold voucher-end"><?php echo format_money($price_options_value) ?></p>
              </div>
              <div class="d-flex justify-content-between">
                <p><?php echo FSText::_("Phí ship") ?></p>
                <p class="font-weight-bold shipping"><?php echo format_money($fee_shipping) ?></p>
              </div>
              <!-- <div class="d-flex justify-content-between">
                <p><?php echo FSText::_("Tổng thanh toán") ?></p>
                <p data-total="<?php echo $total_price ?>" class="font-weight-bold total-end"><?php echo format_money($total_price + $fee_shipping) ?></p>
              </div> -->
              <div class="d-flex align-items-center justify-content-center flex-column">
                <!-- <a href="" class="order-btn">Đặt hàng</a> -->
                <!-- <a href="" class="guest-checkout-btn">Guest Checkout</a> -->
                <!-- <p class="text-center mt-3" style="font-size: 12px; color: #777"><?php echo FSText::_("Chấp nhận thanh toán qua") ?></p> -->
                <!-- <img src="/images/hinhthucthanhtoan.jpg" alt="hinhthucthanhtoan"> -->
              </div>
            </div>
          <?php } else { ?>
            <div style="height: 70%" class="d-flex flex-column justify-content-center align-items-center">
              <svg color="#ccc" xmlns="http://www.w3.org/2000/svg" width="75" height="75" fill="currentColor" class="bi bi-basket" viewBox="0 0 16 16">
                <path d="M5.757 1.071a.5.5 0 0 1 .172.686L3.383 6h9.234L10.07 1.757a.5.5 0 1 1 .858-.514L13.783 6H15a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1v4.5a2.5 2.5 0 0 1-2.5 2.5h-9A2.5 2.5 0 0 1 1 13.5V9a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h1.217L5.07 1.243a.5.5 0 0 1 .686-.172zM2 9v4.5A1.5 1.5 0 0 0 3.5 15h9a1.5 1.5 0 0 0 1.5-1.5V9H2zM1 7v1h14V7H1zm3 3a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 4 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 6 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 8 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 1 .5-.5zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 1 .5-.5z" />
              </svg>
              <p class="mt-3"><?php echo FSText::_("Giỏ hàng trống!") ?></p>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </form>
  <div class="container">
    <p>©2021 Quà 36. All rights reserved. </p>
  </div>
</main>