<?php
global $tmpl;

$global_class = FSFactory::getClass('FsGlobal');
$config = $global_class->get_all_config();

$tmpl->addStylesheet('select2.min', 'modules/payment/assets/css');
$tmpl->addStylesheet('payment', 'modules/payment/assets/css');

$tmpl->addScript('select2.min', 'modules/payment/assets/js');
$tmpl->addScript('last-step', 'modules/payment/assets/js');

// foreach ($list_cart as $item) {
//   $quantity = $item[1];
//   $prod = $data[$item[0]];
//   $total += $quantity * $prod->price;
// }

$total_price = 0;
$vat = 0.1;
$check_total_price = false;
$total_price = 0;
$quantity = 0;
if (isset($_SESSION['cart'])) {
  $product_list = $_SESSION['cart'];
  foreach ($product_list as $item) {
    $quantity += $item[1];
  }
}
$alert_info = array(
  0 => FSText::_('Nhập Từ Khóa'),
  1 => FSText::_('Bạn chưa nhập họ và tên'),
  2 => FSText::_('Bạn chưa nhập tên công ty'),
  3 => FSText::_('Bạn chưa nhập email'),
  4 => FSText::_('Email không hợp lệ'),
  5 => FSText::_('Bạn chưa nhập số điện thoại'),
  6 => FSText::_('Số điện thoại không hợp lệ'),
  7 => FSText::_('Vui lòng nhập địa chỉ chi tiết'),
  8 => FSText::_('Vui lòng nhập tên công ty'),
  9 => FSText::_('Vui lòng nhập địa chỉ công ty')
);
?>

<input type="hidden" id="alert_info" value='<?php echo json_encode($alert_info) ?>' />

<main>
  <div class="cart-payment-mobile">
    <div class="count-products" style="top: 5px; right: 10px;">
      <?php echo $quantity ?>
    </div>
    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
  </div>
  <form id="form-step3" action="index.php?module=payment&view=payment&task=last_step" method="post">
    <div class="container container-payment d-flex pl-0 pr-0 pt-3">
      <div class="left-column-payment col-md-8">
        <div class="col-md-12 d-flex justify-content-between align-items-center mb-3">
          <a href="<?php echo URL_ROOT ?>" class="logo">
            <img src="/<?php echo $config['logo'] ?>" alt="logo" class="logo-image" />
          </a>
          <img class="step-image" src="/images/step3.svg" alt="payment-step3" style="width: auto" />
        </div>
        <div class="col-md-10 pl-0 pr-0 list-address-member">
          <div class="pb-3 d-flex">
            <h6 style="font-weight: 700" class="text-uppercase"><?php echo FSText::_("Thanh toán và xác nhận") ?></h6>
            <a style="font-weight: 600; font-style: italic" class="link ml-3 d-flex align-items-center" onclick="window.history.back(); window.history.back();">Chỉnh sửa</a>
          </div>
          <div style="border-bottom: 1px solid #E1E1E1">
            <p class="mb-2" style="font-weight: 700"><?php echo FSText::_("Địa chỉ nhận hàng") ?></p>
            <p class="mb-2 text-capitalize"><?php echo $_SESSION['info_guest']['name'] ?></p>
            <p class="mb-2"><?php echo $_SESSION['info_guest']['telephone'] ?></p>
            <p class="mb-2 pb-3"><?php echo $_SESSION['info_guest']['address'] ?></p>
          </div>
          <div style="border-bottom: 1px solid #E1E1E1">
            <p class="mb-2 pt-3" style="font-weight: 700"><?php echo FSText::_("Thời gian nhận hàng") ?></p>
            <p class="mb-1 pb-3"><?php echo $_SESSION['info_guest']['receive_date'] ?></p>
          </div>
          <?php if (@$_SESSION['info_guest']['note_adc']) { ?>
            <div style="border-bottom: 1px solid #E1E1E1">
              <p class="mb-2 pt-3" style="font-weight: 700"><?php echo FSText::_("Ghi chú") ?></p>
              <p class="mb-0 pb-3"><?php echo @$_SESSION['info_guest']['note_adc'] ? @$_SESSION['info_guest']['note_adc'] : '' ?></p>
            </div>
          <?php } ?>
        </div>
        <div class="mt-4 col-md-10 pl-0 pr-0 pb-4" style="border-bottom: 1px solid #E1E1E1">
          <div class="d-flex align-items-start">
            <p style="font-weight: 700"><?php echo FSText::_("Chấp nhận thanh toán qua:") ?> </p>
            <img class="ml-3" src="/images/hinhthucthanhtoan.jpg" alt="hinhthucthanhtoan">
          </div>
          <a onclick="alert('Chức năng đang được xây dựng')" class="confirm-step"><?php echo FSText::_("Thanh toán online") ?></a>
        </div>
        <div class="mt-4 col-md-10 pl-0 pr-0">
          <a class="confirm-step-2 confirm-last-step"><?php echo FSText::_("Thanh toán khi nhận hàng") ?></a>
        </div>
      </div>
      <div class="right-column-payment col-md-4">
        <div id="cartModal-payment" class="sidenav sidenav-payment">
          <div class="d-flex ml-2 mb-3">
            <h6 class="font-weight-bold text-uppercase"><?php echo FSText::_("Giỏ hàng") ?></h6>
          </div>
          <div class="line"></div>
          <?php if (!empty($list_cart)) { ?>
            <div style="height: 70%; overflow: auto;" class="cart-items" id="style-3">
              <?php
              foreach ($list_cart as $key1 => $row) {
                $products_info = $data[$row[0]];
                $total = 0;
                $total = $products_info->price * $row[1];
                $total_price += $total;
                if ($products_info->price == 0) {
                  $check_total_price = true;
                }
                foreach ($row[3] as $item2) {
                  if (!is_array(@$item2[0])) {
                    $total_price += $item2['price'] * $item2['quantity'];
                  } else {
                    foreach ($item2 as $val) {
                      $total_price += $val['price'] * $val['quantity'];
                    }
                  }
                }
                $cat_child = $model->get_record_by_id($products_info->category_id, 'fs_products_categories');
                $list_options = $this->get_options($cat_child);
                @$name = '';

                if (!empty($list_options)) {
                  $i = 0;
                  foreach ($list_options as $key => $item) {
                    $obj = $model->get_record_by_id($key, 'fs_products_options');
                    if ($i > 0) {
                      @$name .= ', ';
                    }
                    @$name .= $obj->name;
                    $i++;
                  };
                }
              ?>
                <?php echo $tmpl->cart_item($products_info, $row, $key1, $total, @$name) ?>
              <?php } ?>
            </div>
            <div class="footer-cart">
              <!-- <a class="font-weight-bold font-italic discount-btn"><?php echo FSText::_("+ Mã giảm giá") ?></a> -->
              <div class="line-discount"></div>
              <div class="d-flex justify-content-between">
                <p><?php echo FSText::_("Tạm tính") ?></p>
                <p class="font-weight-bold total"><?php echo format_money($total_price) ?></p>
              </div>
              <div class="d-flex justify-content-between voucher-box" style="display: <?php echo !empty($_SESSION['voucher']) ? '' : 'none !important' ?>">
                <p><?php echo FSText::_("Mã giảm giá") ?></p>
                <p class="font-weight-bold voucher-end"><?php echo '-' . format_money($_SESSION['discount']) ?></p>
              </div>
              <div class="d-flex justify-content-between">
                <p><?php echo FSText::_("Phí ship") ?></p>
                <p class="font-weight-bold shipping"><?php echo $_SESSION['info_shipping']['shipping'] == 1 ? format_money($fee_shipping) : 'Miễn phí' ?></p>
              </div>
              <div class="d-flex justify-content-between">
                <p><?php echo FSText::_("Tổng thanh toán") ?></p>
                <p class="font-weight-bold total-end"><?php echo format_money($total_end) ?></p>
              </div>
              <div class="d-flex align-items-center justify-content-center flex-column">
                <!-- <a href="" class="order-btn">Đặt hàng</a> -->
                <!-- <a href="" class="guest-checkout-btn">Guest Checkout</a> -->
                <!-- <p class="text-center mt-3" style="font-size: 12px; color: #777"><?php echo FSText::_("Chấp nhận thanh toán qua") ?></p> -->
                <!-- <img src="/images/hinhthucthanhtoan.jpg" alt="hinhthucthanhtoan"> -->
              </div>
            </div>
          <?php } else { ?>
            <div style="height: 70%" class="d-flex flex-column justify-content-center align-items-center">
              <svg color="#ccc" xmlns="http://www.w3.org/2000/svg" width="75" height="75" fill="currentColor" class="bi bi-basket" viewBox="0 0 16 16">
                <path d="M5.757 1.071a.5.5 0 0 1 .172.686L3.383 6h9.234L10.07 1.757a.5.5 0 1 1 .858-.514L13.783 6H15a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1v4.5a2.5 2.5 0 0 1-2.5 2.5h-9A2.5 2.5 0 0 1 1 13.5V9a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h1.217L5.07 1.243a.5.5 0 0 1 .686-.172zM2 9v4.5A1.5 1.5 0 0 0 3.5 15h9a1.5 1.5 0 0 0 1.5-1.5V9H2zM1 7v1h14V7H1zm3 3a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 4 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 6 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 8 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 1 .5-.5zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 1 .5-.5z" />
              </svg>
              <p class="mt-3"><?php echo FSText::_("Giỏ hàng trống!") ?></p>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
    <input id="pay_type" type="hidden" name="pay_type" value="">
  </form>
  <div class="container">
    <p>©2021 Quà 36. All rights reserved. </p>
  </div>
</main>