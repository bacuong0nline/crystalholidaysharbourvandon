<?php
global $tmpl, $user;

$global_class = FSFactory::getClass('FsGlobal');
$config = $global_class->get_all_config();

$tmpl->addStylesheet('select2.min', 'modules/payment/assets/css');
$tmpl->addStylesheet('payment', 'modules/payment/assets/css');
$tmpl->addStylesheet('flatpicker', 'modules/payment/assets/css');

$tmpl->addScript('select2.min', 'modules/payment/assets/js');
$tmpl->addScript('payment', 'modules/payment/assets/js');
$tmpl->addScript('flatpicker', 'modules/payment/assets/js');
$tmpl->addScript('vn', 'modules/payment/assets/js');

// foreach ($list_cart as $item) {
//   $quantity = $item[1];
//   $prod = $data[$item[0]];
//   $total += $quantity * $prod->price;
// }
$total_price = 0;
$quantity = 0;
if (isset($_SESSION['cart'])) {
  $product_list = $_SESSION['cart'];
  foreach ($product_list as $item) {
    $quantity += $item[1];
  }
}
$vat = 0.1;
$check_total_price = false;

$alert_info = array(
  0 => FSText::_('Nhập Từ Khóa'),
  1 => FSText::_('Bạn chưa nhập họ và tên'),
  2 => FSText::_('Bạn chưa nhập tên công ty'),
  3 => FSText::_('Bạn chưa nhập email'),
  4 => FSText::_('Email không hợp lệ'),
  5 => FSText::_('Bạn chưa nhập số điện thoại'),
  6 => FSText::_('Số điện thoại không hợp lệ'),
  7 => FSText::_('Vui lòng nhập địa chỉ chi tiết'),
  8 => FSText::_('Vui lòng nhập tên công ty'),
  9 => FSText::_('Vui lòng nhập địa chỉ công ty')
);


?>

<input type="hidden" id="alert_info" value='<?php echo json_encode($alert_info) ?>' />

<main>
  <div class="cart-payment-mobile">
    <div class="count-products" style="top: 5px; right: 10px;">
      <?php echo $quantity ?>
    </div>
    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
  </div>
  <div class="container container-payment d-flex pl-0 pr-0 pt-3">
    <div class="left-column-payment col-md-8">
      <div class="col-md-12 d-flex justify-content-between align-items-center mb-3 header-box">
        <a href="<?php echo URL_ROOT ?>" class="logo">
          <img src="/<?php echo $config['logo'] ?>" alt="logo" class="logo-image" />
        </a>
        <img class="step-image" src="/images/step1.svg" alt="payment-step1" style="width: auto" />
      </div>
      <h6 style="font-weight: 700" class="text-uppercase"><?php echo FSText::_("Thông tin tài khoản") ?></h6>
      <?php if (!$user->is_loaded()) { ?>
        <div class="d-flex">
          <p><?php echo FSText::_("Bạn đã có tài khoản") ?></p>
          <a class="link-btn btn-login">&nbsp;<?php echo FSText::_("Đăng nhập") ?></a>
        </div>
      <?php } else { ?>
        <div class="list-address-member">
          <div class="row no-gutters address-1 style-3">
            <?php foreach ($mba as $item) { ?>
              <label class="radio-container col-md-10 no-gutters address-1__div-info">
                <input style="position: absolute; top: 45%" type="radio" data-id="<?php echo $item->id ?>" name="faddress-member" class="address-member" <?php echo $item->default ? 'checked' : null ?> />
                <div style="margin-left: 30px">
                  <p style="font-weight: 700" class="text-capitalize mb-0"><?php echo $item->first_name . ' ' . $item->last_name ?></p>
                  <p class="mb-0"><span class="grey--text"><?php echo FSText::_("Địa chỉ:") ?></span> <?php echo $item->content ?>, <?php echo $item->district ?>, <?php echo $item->province ?></p>
                  <p class="mb-0"><span class="grey--text"><?php echo FSText::_("Điện thoại:") ?></span> <?php echo $item->telephone ?></p>
                </div>
                <span class="style-radio"></span>
              </label>
            <?php } ?>
          </div>
        </div>
        <label class=" radio-container col-md-10 no-gutters address-1__div-info">
          <input style="position: absolute; top: 15%" type="radio" value="other-address" name="faddress-member" class="address-member" />
          <div style="margin-left: 30px">
            <p class="mb-0 mt-3"><span class="grey--text"><?php echo FSText::_("Vận chuyển đến địa chỉ khác") ?></p>
          </div>
          <span class="style-radio"></span>
        </label>
      <?php } ?>
      <input type="hidden" id="check_other_addess" value="1" />
      <form action="index.php?module=payment&view=payment&Itemid=3&task=set_info_signin" method="post" name="select-address" id="select-address">
        <input type="hidden" name="id-members-address" id="input-select-address" value="<?php echo $item->id ?>" />
        <input type="hidden" name="receive_date" class="datetime1">
      </form>
      <form id="form-step1" action="index.php?module=payment&view=payment&Itemid=3&task=set_info" method="post">
        <input type="hidden" name="receive_date" class="datetime1">
        <h6 style="font-weight: 700" class="text-uppercase"><?php echo FSText::_("Địa chỉ nhận hàng") ?></h6>
        <div class="col-md-10 pl-0 pr-0">
          <div class="d-flex justify-content-between pb-3">
            <div style="width: 49%">
              <input type="text" id="fullname" name="fullname" placeholder="Họ và tên *" />
            </div>
            <div style="width: 49%">
              <input type="text" id="phone" name="phone" placeholder="Số điện thoại *" />
            </div>
          </div>
          <div class="pb-3">
            <input type="text" id="email" name="email" placeholder="Email của bạn *" />
          </div>
          <div class="d-flex justify-content-between pb-3">
            <div style="width: 49%">
              <select style="width: 100%" name="fprovince" id="fprovince" class="select2">
                <?php foreach ($province as $item) { ?>
                  <option data-id=<?php echo $item->id ?> value=<?php echo $item->id ?>><?php echo $item->name ?></option>
                <?php } ?>
              </select>
            </div>
            <div style="width: 49%">
              <select style="width: 100%" name="fdistrict" class="select2" id="fdistrict">
              </select>
            </div>
          </div>
          <div class="pb-3">
            <input type="text" name="address" id="address" placeholder="Địa chỉ (số nhà, tên đường, phường/xã...) *" />
          </div>
          <div>
            <input type="text" name="note" placeholder="Ghi chú" />
          </div>
        </div>

      </form>
      <h6 style="font-weight: 700" class="text-uppercase mt-3"><?php echo FSText::_("Thời gian nhận hàng") ?></h6>
      <div class="date-input" style="width: 50%">
        <input type="text" name="receive_date" data-field="datetime" id="datetime2" value="" readonly>
        <div id="dtBox"></div>
      </div>
      <div class="mt-4">
        <a class="continue-step2"><?php echo FSText::_("Tiếp tục") ?></a>
      </div>
      <div class="container mt-4">
        <p>©2021 Quà 36. All rights reserved. </p>
      </div>
    </div>
    <div class="right-column-payment col-md-4">
      <div id="cartModal-payment" class="sidenav sidenav-payment">
        <div class="d-flex ml-2 mb-3">
          <h6 class="font-weight-bold text-uppercase d-flex align-items-center justify-content-center"><?php echo FSText::_("Giỏ hàng") ?></h6>
        </div>
        <div class="line"></div>
        <?php if (!empty($list_cart)) { ?>
          <div style="height: 70%; overflow: auto;" class="cart-items" id="style-3">
            <?php
            foreach ($list_cart as $key1 => $row) {
              $products_info = $data[$row[0]];
              $total = 0;
              $total = $products_info->price * $row[1];
              $total_price += $total;
              if ($products_info->price == 0) {
                $check_total_price = true;
              }
              foreach ($row[3] as $item2) {
                if (!is_array(@$item2[0])) {
                  $total_price += $item2['price'] * $item2['quantity'];
                } else {
                  foreach ($item2 as $val) {
                    $total_price += $val['price'] * $val['quantity'];
                  }
                }
              }
              $cat_child = $model->get_record_by_id($products_info->category_id, 'fs_products_categories');
              $list_options = $this->get_options($cat_child);
              @$name = '';

              if (!empty($list_options)) {
                $i = 0;
                foreach ($list_options as $key => $item) {
                  $obj = $model->get_record_by_id($key, 'fs_products_options');
                  if ($i > 0) {
                    @$name .= ', ';
                  }
                  @$name .= $obj->name;
                  $i++;
                };
              }
            ?>
              <?php echo $tmpl->cart_item($products_info, $row, $key1, $total, @$name) ?>
            <?php } ?>
          </div>
          <div class="footer-cart">
            <!-- <a class="font-weight-bold font-italic discount-btn"><?php echo FSText::_("+ Mã giảm giá") ?></a> -->
            <div class="d-flex">
              <p class="add-voucher font-weight-bold text-right" style="color: #f68b58; font-size: 13px; cursor: pointer"><?php echo FSText::_("Mã giảm giá") ?></p>
              <i title="Làm mới" class="fa fa-refresh ml-2" style="color: orange; cursor: pointer" aria-hidden="true"></i>
            </div>
            <p class="check-voucher">
            </p>
            <div class="line-discount"></div>
            <div class="d-flex justify-content-between">
              <p><?php echo FSText::_("Tạm tính") ?></p>
              <p class="font-weight-bold total-end"><?php echo format_money($total_price) ?></p>
            </div>
            <div class="d-flex justify-content-between voucher-box" style="display: <?php echo !empty($_SESSION['voucher']) ? '' : 'none !important' ?>">
              <p><?php echo FSText::_("Mã giảm giá") ?></p>
              <p class="font-weight-bold voucher-end"><?php echo '-' . format_money($_SESSION['discount']) ?></p>
            </div>

            <!-- <div class="d-flex justify-content-between">
              <p><?php echo FSText::_("Tổng thanh toán") ?></p>
              <p data-total="<?php echo $total_price ?>" class="font-weight-bold total-end"><?php echo format_money($total_price) ?></p>
            </div> -->
            <!-- <div class="d-flex justify-content-between total-box-end" style="display: <?php echo !empty($_SESSION['voucher']) ? '' : 'none !important' ?>">
              <p><?php echo FSText::_("Tổng thanh toán") ?></p>
              <p class="font-weight-bold total-end-discount"><?php echo format_money($_SESSION['total_price']) ?></p>
            </div> -->
            <div class="d-flex align-items-center justify-content-center flex-column">
              <!-- <a href="" class="order-btn">Đặt hàng</a> -->
              <!-- <a href="" class="guest-checkout-btn">Guest Checkout</a> -->
              <!-- <p class="text-center mt-3" style="font-size: 12px; color: #777"><?php echo FSText::_("Chấp nhận thanh toán qua") ?></p> -->
              <!-- <img src="/images/hinhthucthanhtoan.jpg" alt="hinhthucthanhtoan"> -->
            </div>
          </div>
        <?php } else { ?>
          <div style="height: 70%" class="d-flex flex-column justify-content-center align-items-center">
            <svg color="#ccc" xmlns="http://www.w3.org/2000/svg" width="75" height="75" fill="currentColor" class="bi bi-basket" viewBox="0 0 16 16">
              <path d="M5.757 1.071a.5.5 0 0 1 .172.686L3.383 6h9.234L10.07 1.757a.5.5 0 1 1 .858-.514L13.783 6H15a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1v4.5a2.5 2.5 0 0 1-2.5 2.5h-9A2.5 2.5 0 0 1 1 13.5V9a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h1.217L5.07 1.243a.5.5 0 0 1 .686-.172zM2 9v4.5A1.5 1.5 0 0 0 3.5 15h9a1.5 1.5 0 0 0 1.5-1.5V9H2zM1 7v1h14V7H1zm3 3a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 4 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 6 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 8 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 1 .5-.5zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 1 .5-.5z" />
            </svg>
            <p class="mt-3"><?php echo FSText::_("Giỏ hàng trống!") ?></p>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
</main>