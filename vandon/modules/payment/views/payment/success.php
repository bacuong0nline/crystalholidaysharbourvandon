<?php
global $tmpl;

$tmpl->addStylesheet('success', 'modules/payment/assets/css');
// print_r($tmpl->load_direct_blocks('breadcrumbs', array('style' => 'simple')));die;
?>
<main>
  <div class="container">
    <div class="content-order-success">
      <div class="row no-gutters mt-md-5">
        <div class="col no-gutters d-flex justify-content-center flex-column align-items-center mt-4 mt-md-0">
          <h1 class="bold text--uppercase red--text title-success mb-4"><?php echo FSText::_("Đặt hàng thành công") ?></h1>
          <img src="./images/box.png" alt="box" />
          <!-- <div class="code-order d-flex justify-content-center align-items-center bold m-4">
            <p class="mb-0"><?php echo FSText::_("Mã đơn hàng:") ?> <?php echo 'DH' . str_pad($id, 8, "0", STR_PAD_LEFT) ?></p>
          </div> -->
          <div class="thank-you mt-3">
            <p class="mb-0 text-center"><?php echo FSText::_("Cảm ơn quý khách") ?> <span style="font-weight: bold; font-size: 14px"><?php echo $info_guest["name"] ?></span> <?php echo FSText::_("đã mua hàng trên Quà 36!") ?></p>
            <p class="mb-0 text-center">
              <?php echo FSText::_("Quà 36 sẽ gọi điện thông qua số điện thoại đặt hàng của quý khách để xác nhận đơn hàng trong thời gian sớm nhất.") ?>
              <?php echo FSText::_("Nếu quý khách có nhu cầu xem lại thông tin đặt hàng, quý khách vui lòng") ?> <span style="font-weight: bold"><?php echo FSText::_("kiểm tra xác nhận đơn hàng đã được gửi qua email") ?></span>.
            </p>
          </div>
        </div>
      </div>
      <div class="row no-gutters d-flex justify-content-center mt-3">
        <div class="col no-gutters d-flex justify-content-center back-to-home  mr-4">
          <a href="<?php echo URL_ROOT ?>"><?php echo FSText::_("Trở về trang chủ") ?></a>
        </div>
        <div class="col go-shopping">
          <a href="<?php echo "index.php?module=products&view=cat&task=cat2&Itemid=3" ?>"><?php echo FSText::_("Tiếp tục mua hàng") ?></a>
        </div>
      </div>
      <div class="row no-gutters">
        <div class="col no-gutters"></div>
        <div class="col no-gutters"></div>
      </div>
    </div>
  </div>
</main>