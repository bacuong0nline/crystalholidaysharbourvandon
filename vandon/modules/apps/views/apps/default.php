<?php
global $config, $tmpl, $device;
$tmpl->addStylesheet("home", "modules/apps/assets/css");

?>
<main>
  <?php if ($tmpl->count_block('top_default')) { ?>
    <?php $tmpl->load_position('top_default'); ?>
  <?php } ?>
  <?php if ($tmpl->count_block('middle')) { ?>
    <?php $tmpl->load_position('middle'); ?>
  <?php } ?>
</main>