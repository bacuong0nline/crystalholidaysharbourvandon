$(document).ready(function () {
    // $('select').select2();

    $('.central-news').owlCarousel({
        loop:true,
		dots:false,
        margin:32,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 4
            }
        }
    });
})

function changeCity22($city_id,$id){
    $.ajax({
		type : 'get',
		url : 'index.php?module=address&view=address&raw=1&task=loadDistricts',
		dataType : 'html',
		data: {city_id:$city_id},
		success : function(data){
            location.reload();
        },
		error : function(XMLHttpRequest, textStatus, errorThrown) {}
	});
    return false;
}

function changeSelectedCentral($city_id,$id){
    $.ajax({
		type : 'get',
		url : 'index.php?module=address&view=address&raw=1&task=loadSelectedCentral',
		dataType : 'html',
		data: {city_id:$city_id, id: $id},
		success : function(data){
            location.reload();
        },
		error : function(XMLHttpRequest, textStatus, errorThrown) {}
	});
    return false;
}

function removeSelected(){
    $.ajax({
		type : 'get',
		url : 'index.php?module=central&view=central&raw=1&task=removeSelected',
		dataType : 'html',
		success : function(data){
            location.reload();
        },
		error : function(XMLHttpRequest, textStatus, errorThrown) {}
	});
    return false;
}