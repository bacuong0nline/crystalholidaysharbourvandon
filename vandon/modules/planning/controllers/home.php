<?php

class PlanningControllersHome extends FSControllers
{
    var $module;
    var $view;

    function display()
    {
        $query_body = $this->model->set_query_body();
        $data = $this->model->getData();

        $partners = $this->model->get_records('published = 1 and type = 3', FSTable::_('fs_products_thuong_hieu', 1));
        // unset($_SESSION['selected_central']);
        $comments = $this->model->get_records('published = 1 and category_id = 3', FSTable::_('fs_comments', 1));

        $list_prj_cat = $this->model->get_records('published = 1 and level = 0 and type = 3', FSTable::_('fs_project_categories', 1), '*', 'ordering asc');

        foreach($list_prj_cat as $item) {
            $item->child = $this->model->get_records('published = 1 and parent_id = '.$item->id.'', FSTable::_('fs_project_categories', 1), '*', 'ordering ASC');
            foreach($item->child as $val) {
                $val->prjs = $this->model->get_records('published = 1 and category_id = '.$val->id.'', FSTable::_('fs_project', 1), 'id, image, title, cost, area, city_id, alias, category_id', 'id desc', '8');
            }
            $item->rand = $this->model->get_records('published = 1 and category_id_wrapper like "%,'.$item->id.',%" ', FSTable::_('fs_project', 1), '*', 'created_time DESC', 8);

        }

        include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
    }

}