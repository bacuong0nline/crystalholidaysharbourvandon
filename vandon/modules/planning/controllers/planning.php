<?php

class PlanningControllersPlanning extends FSControllers
{
    var $module;
    var $view;

    function display()
    {
        $model = $this->model;
        $id = FSInput::get('id');
        $data = $model->get_record('published = 1 and id = ' . $id . '', FSTable::_('fs_planning', 1));
        $data_city = $model->get_record_by_id($data->city_id, 'fs_local_cities');
        $cat = $model->get_record_by_id($data->category_id, FSTable::_("fs_planning_categories", 1));
        $cat_parent = $model->get_record('id in (0' . $cat->list_parents . '0) and level = 0', FSTable::_("fs_planning_categories", 1));
        $images = $model->get_records('record_id = ' . $id . '', FSTable::_('fs_planning_images', 1), '*', 'ordering asc');
        $questions = $model->get_records('record_id = ' . $data->id . ' and type = "' . $this->view . '" ', FSTable::_("fs_question", 1));

        // $list_cmt = $this->model->get_comments($data->id, $this->model->limit);

        $this->update_hits();

        if (!$data) {
            setRedirect(URL_ROOT, FSText::_('Sản phẩm chưa được kích hoạt'));
        }

        $relate_news_list = $model->getRelateNewsList($data->category_id);
        $relate_news_list_2 = $model->getRelateNewsList2($data->category_id);

        include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
    }
    function update_hits()
    {
        $model = $this->model;
        $news_id = FSInput::get('id');
        $model->update_hits($news_id);
    }
}
