<?php

/*
 * Huy write
 */

// controller

class GalleryControllersVideo extends FSControllers
{

    var $module;
    var $view;

    function display()
    {
        // call models
        $model = $this->model;
        $list = $model->get_list_video();
        $query_body = $model->set_query_body();
        $total = $model->getTotal($query_body);
        $pagination = $model->getPagination($total);


        $breadcrumbs = array();
        $breadcrumbs[] = array(0 => FSText::_('Thư viện ảnh'), 1 => '');
        global $tmpl;
        $tmpl->assign('breadcrumbs', $breadcrumbs);


        // $tmpl -> assign('alias', $cat->alias);
        // $tmpl -> assign('id', $cat->id);

        include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
    }

    function filter()
    {
        $time_tour = FSInput::get('time_tour');
        $price_tour = FSInput::get('price_tour');
        $return = base64_decode(FSInput::get('return'));

        if ($time_tour) {
            setRedirect(FSRoute::addParameters('time_tour', $time_tour));
        } else if ($price_tour) {
            setRedirect(FSRoute::addParameters('price_tour', $price_tour));
        } else {
            return $return;
        }
    }
    function video_item($item)
    {
?>
        <?php if ($item->file_upload) { ?>
            <a class="news-item" class="video-item" data-video='{"source": [{"src":"<?php echo $item->file_upload ?>", "type":"video/mp4"}], "attributes": {"preload": false, "controls": true}}' data-poster="https://img.youtube.com/vi/egyIeygdS_E/maxresdefault.jpg" data-sub-html="">
                <div class="play-btn" href="#"></div>
                <img onerror="this.src='./images/no-image.png'" style="width: 100%; object-fit: cover" class="img-responsive" src="/<?php echo format_image($item->image) ?>" />
                <div class="play-btn">
                    <svg width="100%" height="100%" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="18" cy="18" r="17.5" fill="#D9D9D9" fill-opacity="0.33" stroke="#D9D9D9" />
                        <path d="M13 10L27 19L13 28V10Z" stroke="white" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                </div>
            </a>
        <?php } else { ?>
            <a class="video-item" data-src="<?php echo $item->link_video . '&mute=0' ?>" data-sub-html="">
                <div class="play-btn" href="#"></div>
                <?php
                $youtube = $item->link_video;
                if (!empty($youtube)) {
                    $youtube = explode('?v=', $item->link_video);
                    $id_youtube = $youtube[1];
                }
                ?>
                <img style="width: 100%; object-fit: cover" src="<?php echo !$item->image ? 'https://img.youtube.com/vi/' . $id_youtube . '/0.jpg' : '/' . format_image($item->image) ?>" alt="<?php echo format_image($item->image) ?>" />
                <div class="play-btn">
                    <svg width="100%" height="100%" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="18" cy="18" r="17.5" fill="#D9D9D9" fill-opacity="0.33" stroke="#D9D9D9" />
                        <path d="M13 10L27 19L13 28V10Z" stroke="white" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                </div>
            </a>
        <?php } ?>
<?php
    }
    public function load_ajax()
    {
        global $tmpl;
        $model = $this->model;
        $page = FSInput::get("page", 1, "int");
        $cat = FSInput::get("cat", 1, "int");
        $query_body = $model->set_query_body();
        $list = $model->get_list_video($query_body);

        $total = $model->getTotal($query_body);
        $pagination = $model->getPagination($total);


        include 'modules/' . $this->module . '/views/' . $this->view . '/ajax_video.php';
    }
}

?>