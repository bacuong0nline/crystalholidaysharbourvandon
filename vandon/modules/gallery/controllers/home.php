<?php

/*
 * Huy write
 */

// controller

class GalleryControllersHome extends FSControllers {

    var $module;
    var $view;

    function display() {
        // call models
        $model = $this->model;
        $list = $model->get_list_image();
        $query_body = $model->set_query_body();
        $total = $model->getTotal($query_body);
        $pagination = $model->getPagination($total);


        $breadcrumbs = array();
        $breadcrumbs[] = array(0 =>FSText::_('Thư viện ảnh'), 1 => '');
        global $tmpl;
        $tmpl->assign('breadcrumbs', $breadcrumbs);


        // $tmpl -> assign('alias', $cat->alias);
        // $tmpl -> assign('id', $cat->id);

        include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
    }

    function filter(){
        $time_tour = FSInput::get('time_tour');
        $price_tour = FSInput::get('price_tour');
        $return = base64_decode(FSInput::get('return'));

        if($time_tour){
            setRedirect(FSRoute::addParameters('time_tour', $time_tour));
        }else if($price_tour){
            setRedirect(FSRoute::addParameters('price_tour', $price_tour));
        }else{
            return $return;
        }
    }

    public function load_ajax()
    {
        $model = $this->model;
        $page = FSInput::get("page", 1, "int");
        $cat = FSInput::get("cat", 1, "int");
        $query_body = $model->set_query_body();
        if($cat == 1) {
            $list = $model->get_list_image($query_body);
        } else if($cat == 2) {
            $list = $model->get_list_video($query_body);
        }
        $total = $model->getTotal($query_body);
        $pagination = $model->getPagination($total);

        include 'modules/' . $this->module . '/views/' . $this->view . '/ajax_image.php';

    }

}

?>