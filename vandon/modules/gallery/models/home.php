<?php

class GalleryModelsHome extends FSModels {

    function __construct() {
        parent::__construct();
        global $module_config;
        $this->limit = 13;
        $fstable = FSFactory::getClass('fstable');
        $this->table_name = $fstable->_('fs_gallery',1);
        $this->table_name_images = $fstable->_('fs_gallery_images',1);
        $this->table_video = $fstable->_('fs_video',1);

    }

    function set_query_body() {
        $query_ordering = '';
        $where = "";
        $query = ' FROM ' . $this->table_name_images . '
						  WHERE 1 = 1
						    '.$where.' ';
        return $query;
    }
    /*
     * get Category current
     * By Id or By code
     */
    function get_list_categories() {
        global $db;
        $query = "SELECT id, name, alias
                  FROM ".$this->table_cat_news."
                  WHERE published = 1
                  ORDER BY id ASC";
        $sql = $db->query($query);
        $result = $db->getObjectList();
        // print_r($result);die;
        return $result;
    }
    function get_list_image() {
        $query = "  SELECT id,title, image
				    FROM " . $this->table_name_images . " 
					WHERE record_id = 10 order by ordering asc, id desc";
        global $db;
        $sql = $db->query_limit($query, $this->limit, $this->page);
        $result = $db->getObjectList();
        return $result;
    }
    function get_list_video() {
        $query = "  SELECT id,title, image, file_upload, link_video
				    FROM " . $this->table_video . "";
        global $db;
        $sql = $db->query_limit($query, $this->limit, $this->page);
        echo $sql;
        $result = $db->getObjectList();
        return $result;
    }
    function getTotal($query_body) {
        if (!$query_body)
            return;
        global $db;
        $query = "SELECT count(*)";
        $query .= $query_body;
        $sql = $db->query($query);
        $total = $db->getResult();
        return $total;
    }

    function getPagination($total)
    {
        $cat = FSInput::get("cat");
        FSFactory::include_class('ajaxpagination');
        $pagination = new AjaxPagination($this->limit, $this->page, $total, 'index.php?module=gallery&view=home&task=load_ajax','list_image',$cat);
        return $pagination;
    }

}

?>