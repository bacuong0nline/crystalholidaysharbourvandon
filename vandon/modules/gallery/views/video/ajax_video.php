<div class="first-news-item">
  <div class="video-box">
    <?php echo $this->video_item($list[0]) ?>
  </div>
</div>
<div class="image-box">
  <div class="grid-gallery video-box">
    <?php foreach ($list as $key => $item) { ?>
      <?php if ($key > 0) { ?>
        <?php echo $this->video_item($item) ?>
      <?php } ?>
    <?php } ?>
  </div>
</div>

<div class="row no-gutters" style="padding-bottom: 60px">
  <div class="col no-gutters d-flex justify-content-center">
    <?php if ($pagination) {
      echo $pagination->showPagination(1);
    }
    ?>
  </div>
</div>

<script>
  let elements = document.getElementsByClassName('video-box');

  for (let item of elements) {
    lightGallery(item, {
      thumbnail: false,
      // toggleThumb: true,
      getCaptionFromTitleOrAlt: false,
      plugins: [lgThumbnail, lgVideo, lgZoom]
    })
  }
</script>