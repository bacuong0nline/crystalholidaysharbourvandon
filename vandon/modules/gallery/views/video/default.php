<?php
global $tmpl;
$tmpl->addStylesheet('gallery', 'modules/gallery/assets/css');
$tmpl->addScript('gallery', 'modules/gallery/assets/js');


// print_R($list);die;
?>


<main>
  <?php echo $tmpl->load_direct_blocks('banners', array('style' => 'style5', 'category_id_image' => 4)) ?>
  <div class="container pl-0 pr-0 pt-3">
    <div class="row no-gutters">
      <div class="col no-gutters d-flex justify-content-center mt-xs-2 mt-md-0">
        <!-- <h1 class="title-news text-uppercase font-weight-bold">Tin tức mới nhất </h1> -->
      </div>
    </div>
    <div class="row no-gutters tin-tuc-chung__content d-flex">
      <div class="desktop">
        <ul class="nav-tabs">
          <li data-cat="1">
            <a href="<?php echo FSRoute::_("index.php?module=gallery&view=home") ?>">Hình Ảnh</a>
          </li>
          <li class="active" data-cat="2">
            <a href="<?php echo FSRoute::_("index.php?module=gallery&view=video") ?>">Video</a>
          </li>
        </ul>
      </div>
      <div class="mobile">
        <div style="text-align: center; text-transform: uppercase; " name="" id="" class="mb-3" onchange="location = this.value;">
          <a class="mb-3 p-2 me-3" style="width: 100px; display: inline-block; <?php echo  'background: #FFE9D4'  ?>" href="<?php echo FSRoute::_("index.php?module=gallery&view=home") ?>">
            Hình Ảnh
          </a>
          <a class="p-2" style="width: 100px; display: inline-block; color: white; <?php echo  'background: #db801a'  ?>" href="<?php echo FSRoute::_("index.php?module=gallery&view=video") ?>">
            Video
          </a>
        </div>
      </div>
      <div class="w-100"></div>
      <div id="list_image">
        <?php if (!IS_MOBILE) {  ?>
          <div class="first-news-item ">
            <div class="video-box">
              <?php echo $this->video_item($list[0]) ?>
            </div>
          </div>
          <div class="">
            <div class="grid-gallery video-box">
              <?php foreach ($list as $key => $item) { ?>
                <?php if ($key > 0) { ?>
                  <?php echo $this->video_item($item) ?>
                <?php } ?>
              <?php } ?>
            </div>
          </div>
        <?php } ?>
        <?php if (IS_MOBILE) {  ?>
          <div class="">
            <div class="grid-gallery video-box">
              <?php foreach ($list as $key => $item) { ?>
                <?php echo $this->video_item($item) ?>
                <p style="color:white;" class="fw-semibold text-center mt-3 mb-0"><?php echo $item->title ?></p>

              <?php } ?>
            </div>
          </div>
        <?php } ?>
        <div class="row no-gutters" style="padding-bottom: 60px">
          <div class="col no-gutters d-flex justify-content-center">
            <?php if ($pagination) {
              echo $pagination->showPagination(1);
            }
            ?>
          </div>
        </div>
      </div>
    </div>

  </div>
</main>

<script>
  window.onload = function() {
    let elements = document.getElementsByClassName('video-box');
    for (let item of elements) {
      lightGallery(item, {
        thumbnail: false,
        toggleThumb: false,
        getCaptionFromTitleOrAlt: false,
        plugins: [lgThumbnail, lgVideo]
      })
    }
  }
</script>