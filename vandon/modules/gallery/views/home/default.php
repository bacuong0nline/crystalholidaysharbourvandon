<?php
global $tmpl;
$tmpl->addStylesheet('gallery', 'modules/gallery/assets/css');
$tmpl->addStylesheet('lightgallery', 'modules/gallery/assets/css');
$tmpl->addScript('gallery', 'modules/gallery/assets/js');
?>

<main>
  <?php echo $tmpl->load_direct_blocks('banners', array('style' => 'style5', 'category_id_image' => 4)) ?>
  <div class="container pl-0 pr-0 pt-3">
    <div class="row no-gutters">
      <div class="col no-gutters d-flex justify-content-center mt-xs-2 mt-md-0">
        <!-- <h1 class="title-news text-uppercase font-weight-bold">Tin tức mới nhất </h1> -->
      </div>
    </div>

    <div class="row no-gutters tin-tuc-chung__content d-flex">
      <div class="desktop">
        <ul class="nav-tabs">
          <li data-cat="1">
            <a class="active" href="<?php echo FSRoute::_("index.php?module=gallery&view=home") ?>">Hình Ảnh</a>
          </li>
          <li data-cat="2">
            <a href="<?php echo FSRoute::_("index.php?module=gallery&view=video") ?>">Video</a>
          </li>
        </ul>
      </div>
      <div class="mobile">
        <div style="text-align: center; text-transform: uppercase; " name="" id="" class="mb-3" onchange="location = this.value;">
          <a class="mb-3 p-2 me-3" style="width: 100px; display: inline-block; color: white; <?php echo  'background: #db801a'  ?>" href="<?php echo FSRoute::_("index.php?module=gallery&view=home") ?>">
            Hình Ảnh
          </a>
          <a class="p-2" style="background: #FFE9D4; width: 100px; display: inline-block" href="<?php echo FSRoute::_("index.php?module=gallery&view=video") ?>">
            Video
          </a>
        </div>
      </div>
      <div class="w-100"></div>
      <div id="list_image">
        <?php if (!IS_MOBILE) { ?>
          <div class="first-news-item">
            <a class="image-box" href="<?php echo $list[0]->image ?>" data-sub-html="<?php echo $list[0]->title ?>">
              <img style="width: 100%" src="<?php echo URL_ROOT . str_replace('/original/', '/resized/', $list[0]->image) ?>">
            </a>
          </div>
          <div class="grid-gallery image-box" id="lightgallery">
            <?php foreach ($list as $key => $item) { ?>
              <?php if ($key > 0) { ?>
                <a class="news-item" href="<?php echo $item->image ?>" data-sub-html="<?php echo $item->title ?>">
                  <img src="<?php echo URL_ROOT . str_replace('/original/', '/resized/', $item->image) ?>">
                </a>
              <?php } ?>
            <?php } ?>
          </div>
        <?php } ?>
        <?php if (IS_MOBILE) { ?>
          <div class="grid-gallery image-box" id="lightgallery">
            <?php foreach ($list as $key => $item) { ?>
              <a class="news-item" href="<?php echo $item->image ?>" data-sub-html="<?php echo $item->title ?>">
                <img src="<?php echo URL_ROOT . str_replace('/original/', '/resized/', $item->image) ?>">
                <p style="color:white;" class="fw-semibold text-center mt-3 mb-0"><?php echo $item->title?></p>
              </a>
            <?php } ?>
          </div>
        <?php } ?>
        <div class="row no-gutters" style="padding-bottom: 60px">
          <div class="col no-gutters d-flex justify-content-center">
            <?php if ($pagination) {
              echo $pagination->showPagination(1);
            }
            ?>
          </div>
        </div>
      </div>
    </div>

  </div>
</main>