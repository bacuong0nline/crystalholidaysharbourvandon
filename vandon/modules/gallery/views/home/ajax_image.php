<div class="first-news-item">
  <a class="image-box" href="<?php echo $list[0]->image ?>" data-sub-html="<?php echo $list[0]->title ?>">
    <img style="width: 100%" src="<?php echo URL_ROOT . str_replace('/original/', '/resized/', $list[0]->image) ?>">
  </a>
</div>
<div class="grid-gallery image-box" id="lightgallery">
  <?php foreach ($list as $key => $item) { ?>
    <?php if ($key > 0) { ?>
      <a class="news-item" href="<?php echo $item->image ?>" data-sub-html="<?php echo $item->title ?>">
        <img src="<?php echo URL_ROOT . str_replace('/original/', '/resized/', $item->image) ?>">
      </a>
    <?php } ?>
  <?php } ?>
</div>

<div class="row no-gutters" style="padding-bottom: 60px">
  <div class="col no-gutters d-flex justify-content-center">
    <?php if ($pagination) {
      echo $pagination->showPagination(1);
    }
    ?>
  </div>
</div>

<script>
    let elements = document.getElementsByClassName('image-box');

    for (let item of elements) {
      lightGallery(item, {
        thumbnail: true,
        animateThumb: true,
        // zoomFromOrigin: false,
        // allowMediaOverlap: true,
        toggleThumb: true,
        // plugins: [lgThumbnail, lgVideo, lgZoom]
      })
    }
  
</script>