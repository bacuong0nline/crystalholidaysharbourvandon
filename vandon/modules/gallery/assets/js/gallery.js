$(document).ready(function () {
  // lightGallery(document.getElementById('lightgallery'));

  let elements = document.getElementsByClassName('image-box');

  for (let item of elements) {
    lightGallery(item, {
      thumbnail: true,
      animateThumb: true,
      // zoomFromOrigin: false,
      // allowMediaOverlap: true,
      toggleThumb: true,
      // plugins: [lgThumbnail, lgVideo, lgZoom]
    })
  }


  $(".nav-tabs li").click(function () {
    $(".nav-tabs li").removeClass("active");
    $(this).addClass("active"); 
  })
})