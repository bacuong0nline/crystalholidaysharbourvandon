<?php

class ScheduleControllersHome extends FSControllers
{
    var $module;
    var $view;

    function display()
    {
        $query_body = $this->model->set_query_body();
        $list = $this->model->get_list();

        // print_r($data);die;
        include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
    }

    function detail()
    {
        $data = $this->model->getDataNews();
        $list = $this->model->getRelateNews($data->category_id);

        include 'modules/' . $this->module . '/views/' . $this->view . '/news.php';
    }

    function get_agency()
    {
        $city_id = FSInput::get('district_id');
        if ($city_id == 0) {
            unset($_SESSION["district"]);
        } else {
            $_SESSION["district"] = $city_id;
        }
    }

    function loadDistricts()
    {
        $city_id = FSInput::get('city_id');
        $_SESSION["city"] = $city_id;
        global $config;
        $listDistricts = $this->model->getListDistricts($city_id);
        $html = '';
        foreach ($listDistricts as $item) {
            $html .= '<option  value="' . $item->id . '">' . $item->name . '</option>';
        }
        echo $html;
    }

    function loadADDStore()
    {
        $id = FSInput::get('id');

        global $config;
        $infoStore = $this->model->get_record('id=' . $id, 'fs_address');
        $html = '';

        $html .= '<p>' . $infoStore->address . '<p>';

        echo $html;
        return true;
    }
}