<?php
global $tmpl;
$tmpl->addStylesheet('default', 'modules/schedule/assets/css');
$tmpl->addStylesheet('default', 'modules/education/assets/css');

$tmpl->addScript('cat', 'modules/schedule/assets/js', 'bottom');

?>

<?php if ($tmpl->count_block('top_default')) { ?>
    <?php $tmpl->load_position('top_default'); ?>
<?php } ?>

<?php if ($tmpl->count_block('middle')) { ?>
    <?php $tmpl->load_position('middle'); ?>
<?php } ?>