<?php
global $tmpl;
$tmpl->addStylesheet('default', 'modules/schedule/assets/css');
$tmpl->addStylesheet('default', 'modules/education/assets/css');

$tmpl->addScript('cat', 'modules/schedule/assets/js', 'bottom');

?>

<?php if ($tmpl->count_block('top_default')) { ?>
    <?php $tmpl->load_position('top_default'); ?>
<?php } ?>
<?php if ($cat->block_1) { ?>
    <div class="snowflake1" style="width: 100%; background-size: 100%; background-repeat: no-repeat; background-position: center center; position: relative; padding-bottom: 100px;">
        <div class="container pb-3 snowflake1-box">
            <p class="text-uppercase text-center"><?php echo $cat->name ?></p>
            <?php if (@$cat->name_block_1) { ?>
                <h5 class="title text-center text-uppercase mb-4">
                    <?php echo $cat->name_block_1 ?>
                </h5>
            <?php } else { ?>
                <h5 class="title text-center text-uppercase mb-4">
                    <?php echo $cat->name ?>
                </h5>
            <?php } ?>
        </div>
        <div class="container target-box">
            <?php echo $cat->block_1 ?>
        </div>
        <img class="wave2" src="./images/wave2.png" alt="wave2" />
    </div>
<?php } ?>
<?php if ($cat->block_2) { ?>
    <div class="snowflake2" style="width: 100%; background-size: 100%; background-repeat: no-repeat; background-position: center center; position: relative; padding-bottom: 100px; background-color: #F8F9F9;">
        <div class="container pb-3">
            <p class="text-uppercase text-center grey--text"><?php echo $cat->name  ?></p>
            <?php if (@$cat->name_block_2) { ?>
                <h5 class="title text-center text-uppercase mb-4">
                    <?php echo $cat->name_block_2 ?>
                </h5>
            <?php } else { ?>
                <h5 class="title text-center text-uppercase mb-4">
                    <?php echo FSText::_("Địa điểm thi tốt nhất") ?>
                </h5>
            <?php } ?>
        </div>
        <div class="container target-box">
            <?php echo $cat->block_2 ?>
        </div>
        <img class="wave1" src="./images/wave1.png" alt="wave1" />
    </div>
<?php } ?>

<?php if (!empty($list_image)) { ?>
    <div class="snowflake1" style="width: 100%; background-size: 100%; background-repeat: no-repeat; background-position: center center; position: relative; padding-bottom: 100px;">
        <div class="container pb-3 snowflake1-box">
            <p class="text-uppercase text-center"><?php echo $cat->name ?></p>
            <h5 class="title text-center text-uppercase mb-4">
                <?php echo FSText::_("Lịch thi chứng chỉ") ?>
            </h5>
        </div>
        <div class="container">
            <div class="schedule owl-carousel owl-theme ">
                <?php foreach ($list_image as $item) { ?>
                    <div class="schedule-item">
                        <img src="<?php echo $item->image ?>" alt="<?php echo $item->image ?>" />
                    </div>
                <?php } ?>
            </div>
        </div>
        <img class="wave2" src="./images/wave2.png" alt="wave2" />

    </div>
<?php } ?>

<?php if ($tmpl->count_block('middle')) { ?>
    <?php $tmpl->load_position('middle'); ?>
<?php } ?>