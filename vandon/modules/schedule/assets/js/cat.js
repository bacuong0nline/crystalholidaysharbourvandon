$(document).ready(function () {
    // $('select').select2();
    $('.schedule').owlCarousel({
        loop:false,
        margin:10,
        nav:true,
        autoplay: true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:4
            }
        }
    })

    $('.comments').owlCarousel({
        loop:false,
        margin:10,
        loop:true,
        nav:true,
        autoplay: true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:3
            }
        }
    })
})