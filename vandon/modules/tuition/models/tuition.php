
<?php
class TuitionModelsTuition extends FSModels
{
    function __construct()
    {
        $this->limit = 9;
        $fstable = FSFactory::getClass('fstable');
        $this->table_name = $fstable->_('fs_products', 1);
        $this->table_menus_items = $fstable->_('fs_menus_items', 1);

        $this->table_add = $fstable->_('fs_address', 1);
    }

    function set_query_body()
    {

        $date1 = FSInput::get("date_search");
        $where = "";
        if (FSInput::get('tag')) {
            $where .= 'AND tags like "%' . FSInput::get('tag') . '%"';
        }
        $fs_table = FSFactory::getClass('fstable');
        $query = " FROM " . $fs_table->getTable('fs_news', 1) . "
						  WHERE 
                               published = 1 and is_hot = 1
						  	" . $where .
            " ORDER BY created_time DESC, id DESC 
						 ";
        return $query;
    }
    function getTitleNews() {
        global $db;
        $query = " SELECT * FROM " . $this->table_menus_items . "";
        $query .= " where link like '%module=news%'";
        $query .= " ORDER BY ordering ASC, created_time DESC ";
        $sql = $db->query($query);
        $result = $db->getObject();
        return $result;
    }
    function getProductsList($query_body)
    {
        if (!$query_body)
            return;

        global $db;
        $query = " SELECT is_new, is_hot, name,price_old,price,discount,unit,id,alias,category_name,category_id,image,category_alias,measure, summary FROM " . $this->table_name . "";
        $query .= " WHERE " . $query_body . "";
        $query .= " ORDER BY ordering ASC, created_time DESC LIMIT 6";
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }
    function get_list($query_body)
    {
        if (!$query_body)
            return;

        global $db;
        $query = " SELECT *";
        $query .= $query_body;
        //print_r($query); 
        $sql = $db->query_limit($query, $this->limit, $this->page);
        $result = $db->getObjectList();
        return $result;
    }
}

?>