<?php
global $config, $tmpl, $device;
$tmpl->addScript('default', 'modules/home/assets/js');
$tmpl->addStylesheet("home", "modules/experience/assets/css");

?>
<main>
  <?php if ($tmpl->count_block('top_default')) { ?>
    <?php $tmpl->load_position('top_default'); ?>
  <?php } ?>
  <?php if ($tmpl->count_block('middle')) { ?>
    <?php $tmpl->load_position('middle'); ?>
  <?php } ?>
</main>