<?php
class ApplicationControllersStep2 extends FSControllers
{
    var $module;
    var $view;
    function display()
    {

        global $config;

        $application_types_categories = $this->model->get_records('published = 1 and more = 0 and level = 0', FSTable::_('fs_application_type_categories', 1), '*', 'ordering asc'); 

        foreach($_SESSION['step1']['option'] as $item) {
            $data = $this->model->get_records('published = 1 and more = 1 and list_parents like "%,'.$item.',%" ', FSTable::_('fs_application_type_categories', 1), '*', 'ordering asc');
            foreach($data as $val) {
                $application_types_categories_more[] = $val; 

            }
        }


        foreach($application_types_categories as $item) {
            $item->child = $this->model->get_records('published = 1 and category_id = '.$item->id.' ', FSTable::_('fs_application_type', 1), 'id, title, price, coefficient, alias', 'ordering asc');
        }
        
        foreach($application_types_categories_more as $item) {
            $item->child = $this->model->get_records('published = 1 and category_id = '.$item->id.' ', FSTable::_('fs_application_type', 1), 'id, title, price, coefficient, alias', 'ordering asc');
        }

        include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
    }

    function confirm() {
        global $config;
        $array = array();
        foreach($_POST as $key => $item) {
            if(strpos($key, 'input2-tang-') !== false) {
                $array[str_replace('input2-tang-', FSText::_('Tầng '), $key)] = new \stdClass();
                $array[str_replace('input2-tang-', FSText::_('Tầng '), $key)]->input = $item;
                $array[str_replace('input2-tang-', FSText::_('Tầng '), $key)]->select->price = @$config['unit_price'];
                $array[str_replace('input2-tang-', FSText::_('Tầng '), $key)]->select->title = str_replace('input2-tang-', FSText::_('Tầng '), $key);
                $array[str_replace('input2-tang-', FSText::_('Tầng '), $key)]->select->coefficient = '100';
            }
            if(strpos($key, 'input-') !== false) {
                $cate = $this->model->get_record('alias = "'.str_replace('input-', '', $key).'" ', FSTable::_('fs_application_type_categories'));
                $array[$cate->name] = new \stdClass();
                $array[$cate->name]->input = $item;
            }
            if(strpos($key, 'select') !== false) {
                $cate = $this->model->get_record('alias = "'.str_replace('select-', '', $key).'" ', FSTable::_('fs_application_type_categories'));
                $array[$cate->name]->select = $this->model->get_record_by_id($item, FSTable::_('fs_application_type', 1), 'id, title, price,coefficient');
            }

        }

        $_SESSION['step2'] = $array;
        setRedirect(FSRoute::_("index.php?module=application&view=step3&Itemid=79"));

    }
}
?>