<?php
class ApplicationControllersStep1 extends FSControllers
{
    var $module;
    var $view;
    function display()
    {

        global $config;

        // print_r($list_cat_news);die;
        $cities = $this->model->get_records('published = 1', 'fs_local_cities');

        $application_types_categories_more = $this->model->get_records('published = 1 and more = 1 and level = 0', FSTable::_('fs_application_type_categories', 1), '*', 'ordering asc'); 


        include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
    }

    function confirm() {
        $row['location'] = FSInput::get("location");
        $row['type_of_construction'] = FSInput::get("type-of-construction");
        $row['area'] = FSInput::get("area");
        $row['build_area'] = FSInput::get("build-area");
        $row['select_number_of_floor'] = FSInput::get("select-number-of-floor");
        $row['option'] = FSInput::get("option", array(), 'array');

        $_SESSION['step1'] = $row;

        setRedirect(FSRoute::_("index.php?module=application&view=step2&Itemid=79"));
    }
}
?>