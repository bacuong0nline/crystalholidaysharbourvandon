$(document).ready(function () {
  $(".confirm").click(function () {
    if (checkFormsubmit2()) {
      $("#form-step1").submit();
    }
  })

  $("#area").keyup(function () {
    if ($(this).val() !== '') {
      $("#build-area").val(parseInt($(this).val()) * 80 / 100)
    } else {
      $("#build-area").val(0)
    }
  })

  $("#area").change(function () {
    if ($(this).val() !== '') {
      $("#build-area").val(parseInt($(this).val()) * 80 / 100)
    } else {
      $("#build-area").val(0)
    }
  })
})
let validate_text = JSON.parse($("#validate-step1").val());
function checkFormsubmit2() {
  $('label.label_error').prev().remove();
  $('label.label_error').remove();
  email_new = $('#email_new').val();

  if (!notEmpty("type-of-construction", validate_text[0])) {
    return false;
  }

  if (!notEmpty("area", validate_text[1])) {
    return false;
  }

  if (!notEmpty("build-area", validate_text[1])) {
    return false;
  }
  // return false;
  return true;
}
