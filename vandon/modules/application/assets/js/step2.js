$("document").ready(function () {
  $(".select-option").change(function () {
    let chosse_class = $(this).attr('data-class');
    let coefficient = $('option:selected', this).attr('data-coefficient')

    if ($(this).val() == 0) {
      $(`.${chosse_class}`).empty();

    } else {
      $(`.${chosse_class}`).html(`${coefficient}%`);
    }
  })

  $(document).on('click', '.clear', function () {
    let data_class = $(this).attr("data-class");

    if (window.confirm('Bạn muốn xoá trường này')) {
      $(`.${data_class}`).remove();
    }
    else {
      // They clicked no
    }
  })


  $(".confirm").click(function () {
    if (checkFormsubmit2()) {
      $("#form-step2").submit();
    }
  })
})




function checkFormsubmit2() {
  $('.label_error').remove();
  email_new = $('#email_new').val();
  let i = 0;
  $('.select-option option').each(function () {
    let id = $(this).parent().attr("id");
    if ($(this).parent().val() == 0) {
      invalid(`${id}`, 'Bạn chưa chọn giá trị')
      return false;
    }
    i++;
  })

  if (i < $('.select-option option').length) {
    return false;
  } else if (i == $('.select-option option').length - 1) {
    return false;

  }
  // return false;
  return true;
}
