$(document).ready(function () {
  $('.news').owlCarousel({
    loop:true,
    margin:10,
    // nav:true,
    dots:true,
    autoplay: true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:4
        },
        1000:{
            items:4
        }
    }
    })
    $('.partner-box').owlCarousel({
        loop:true,
        margin:10,
        // nav:true,
        dots:true,
        autoplay: true,
        responsive:{
            0:{
                items:2
            },
            600:{
                items:4
            },
            1000:{
                items:4
            }
        }
        })


    $('.central-box-mobile').owlCarousel({
        loop:true,
        margin:10,
        // nav:true,
        dots:true,
        autoplay: true,
        responsive:{
            0:{
                items:1
            }
        }
    })
})

