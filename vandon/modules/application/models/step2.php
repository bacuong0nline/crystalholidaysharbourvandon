
<?php
class ApplicationModelsStep2 extends FSModels
{
    function __construct()
    {
        $this->limit = 9;
        $fstable = FSFactory::getClass('fstable');
        $this->table_name = $fstable->_('fs_products', 1);
        $this->table_menus_items = $fstable->_('fs_menus_items', 1);

        $this->table_add = $fstable->_('fs_address', 1);
    }

    function set_query_body()
    {

        $date1 = FSInput::get("date_search");
        $where = "";
        if (FSInput::get('tag')) {
            $where .= 'AND tags like "%' . FSInput::get('tag') . '%"';
        }
        $fs_table = FSFactory::getClass('fstable');
        $query = " FROM " . $fs_table->getTable('fs_news', 1) . "
						  WHERE 
                               published = 1 and is_hot = 1
						  	" . $where .
            " ORDER BY created_time DESC, id DESC 
						 ";
        return $query;
    }

}

?>