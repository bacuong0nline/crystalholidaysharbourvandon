<?php
global $config, $tmpl, $device;
$tmpl->addStylesheet("step1", "modules/application/assets/css");

$tmpl->addScript('step2', 'modules/application/assets/js');

$total = 0;
?>

<div class="space">
  <h1 class="fw-bold pb-3 text-center"><?php echo FSText::_("Ứng dụng tính chi phí Xây Dựng") ?></h1>
  <p class="text-center"><?php echo FSText::_("Tại Ikay chúng tôi tin rằng con người là là nhân tố quyết định mọi sự thành công.") ?></p>
  <div class="d-flex wrapper justify-content-center pt-3">
    <div class="box-wrapper">
      <h6><?php echo FSText::_("Gói phổ thông") ?></h6>
      <div>
        <table style="width: 100%">
          <tbody>
            <?php foreach ($_SESSION['step2'] as $key => $item) {
                $total += $item->input * ($item->select->coefficient / 100) * $item->select->price;
              ?>
              <tr>
                <td>
                  <span class="font-weight-600">
                    <?php echo $key ?>
                  </span>
                  <span>(<?php echo $item->input . ' x ' . $item->select->coefficient . '%' . ' = ' . $item->input * ($item->select->coefficient / 100) . ' m' ?><sup>2</sup>)
                  </span>
                </td>
                <td class="font-weight-600 text-end"><?php echo format_money($item->input * ($item->select->coefficient / 100) * $item->select->price) ?></td>
              </tr>
              <tr>
                <td style="color: #BABABA"><?php echo $item->select->title . ' (' . $item->select->coefficient . ' % ' . FSText::_("Đơn vị diện tích") . ')' ?></td>
              </tr>
            <?php } ?>
            <tr>
              <td>
                <span class="font-weight-600">
                  <?php echo FSText::_("Tổng cộng") ?>
                </span>
              </td>
              <td class="font-weight-600 text-end"><?php echo format_money($total) ?></td>
            </tr>
          </tbody>
        </table>
      </div>
      <a href="<?php echo FSRoute::_("index.php?module=application&view=final&Itemid=79")?>" class="confirm"><?php echo FSText::_("Xác nhận") ?></a>

    </div>

  </div>
</div>