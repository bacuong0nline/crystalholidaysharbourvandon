<?php
global $config, $tmpl, $device;
$tmpl->addStylesheet("home", "modules/application/assets/css");

?>

<div class="container space">
  <h1 class="fw-bold pb-3"><?php echo FSText::_("Ứng dụng") ?></h1>
  <p><?php echo FSText::_("Tại Ikay chúng tôi tin rằng con người là là nhân tố quyết định mọi sự thành công.")?></p>
  <div class="d-flex wrapper pt-3">
    <!-- <div class="col-md-4">
      <div>
        <p class="fw-semibold">Bộ sưu tập</p>
        <label for="application">
          <input type="checkbox" name="application" id="application" />
          <?php echo FSText::_("Bộ sưu tập") ?>
        </label>
      </div>
    </div> -->
    <div class="col-md-12 application-wrapper-box">
      <div href="" class="application-item">
        <a href="<?php echo FSRoute::_("index.php?module=application&view=step1&Itemid=79") ?>">
          <div class="thumbnail">
            <img src="/images/prj4.png" alt="application" />
          </div>
        </a>
        <div class="p-3 pb-0 summary-application-item">
          <a href="<?php echo FSRoute::_("index.php?module=application&view=step1") ?>">
            <div class="fw-bold">
              <h3><?php echo FSText::_("Ứng dụng tính chi phí Xây Dựng")?></h3>
            </div>
            <p><?php echo FSText::_("Là ứng dụng dự toán thông minh, sau khi có kết quả sẽ gợi ý các công trình phù hợp với yêu cầu của chủ đầu tư")?></p>
          </a>
          <!-- <div>
            <a href="" class="fw-semibold unit-price">
              Cập nhật đơn giá ngày 20/03/2022
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
              </svg>
            </a>
          </div> -->
        </div>

      </div>
    </div>
  </div>
</div>