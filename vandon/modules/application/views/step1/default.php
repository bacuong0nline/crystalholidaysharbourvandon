<?php
global $config, $tmpl, $device;
$tmpl->addStylesheet("step1", "modules/application/assets/css");
$tmpl->addScript('step1', "modules/application/assets/js");

?>

<input type="hidden" id="validate-step1" value='<?php echo json_encode(array(
                                                  0 => FSText::_("Bạn chưa nhập loại công trình"),
                                                  1 => FSText::_("Bạn chưa nhập diện tích đất"),
                                                  2 => FSText::_("Bạn chưa nhập diện tích xây dựng")
                                                )) ?>'>
<div class="space">
  <h1 class="fw-bold pb-3 text-center"><?php echo FSText::_("Ứng dụng tính chi phí Xây Dựng") ?></h1>
  <p class="text-center"><?php echo FSText::_("Tại Ikay chúng tôi tin rằng con người là là nhân tố quyết định mọi sự thành công.") ?></p>
  <div class="d-flex wrapper justify-content-center pt-3">
    <div class="box-wrapper">
      <h4><?php echo FSText::_("Thông tin công trình") ?></h4>
      <form action="index.php?module=application&view=step1&task=confirm&Itemid=79" method="POST" id="form-step1">
        <div>
          <div class="my-formcontrol">
            <label for="location"><?php echo FSText::_("Địa điểm xây dựng") ?></label><br>
            <select name="location" id="location">
              <?php foreach ($cities as $item) { ?>
                <option value="<?php echo $item->id ?>"><?php echo $item->name ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="d-flex flex-wrap">
            <div class="my-formcontrol col-md-6 pe-2">
              <label for="type-of-construction"><?php echo FSText::_("Loại công trình") ?></label>
              <select name="type-of-construction" id="type-of-construction">
                <option value="1"><?php echo FSText::_("Nhà Phố") ?></option>
                <option value="2"><?php echo FSText::_("Biệt Thự") ?></option>
                <option value="3"><?php echo FSText::_("Nhà cấp bốn") ?></option>
              </select>
            </div>
            <div class="my-formcontrol col-md-6">
              <label for="area"><?php echo FSText::_("Diện tích đất") ?></label>
              <input type="text" id="area" name="area" placeholder="<?php echo FSText::_("m2") ?>" />
            </div>
            <div class="my-formcontrol col-md-6 pe-2">
              <label for="build-area"><?php echo FSText::_("Diện tích xây dựng") ?></label>
              <input type="text" id="build-area" name="build-area" placeholder="<?php echo FSText::_("m2") ?>" />
            </div>
            <div class="my-formcontrol col-md-6">
              <label for="select-number-of-floor"><?php echo FSText::_("Số tầng") ?></label>
              <select name="select-number-of-floor" id="select-number-of-floor">
                <option value="1"><?php echo FSText::_("Tầng 1 (Tầng trệt)") ?></option>
                <option value="2"><?php echo FSText::_("Tầng 2") ?></option>
                <option value="3"><?php echo FSText::_("Tầng 3") ?></option>
                <option value="4"><?php echo FSText::_("Tầng 4") ?></option>
                <option value="5"><?php echo FSText::_("Tầng 5") ?></option>
              </select>
            </div>
          </div>
        </div>
        <div class="check-select">
          <label class="mb-3" for=""><?php echo FSText::_("Thêm lựa chọn") ?></label>
          <div class="more-option" style="display: grid; grid-template-columns: 1fr 1fr 1fr 1fr">
            <?php foreach ($application_types_categories_more as $item) { ?>
              <div class="d-flex align-items-center mb-3">
                <input <?php echo @$item->is_checked ? "checked" : null ?> class="form-check-input mt-0" name="option[]" type="checkbox" value="<?php echo $item->id ?>" id="checkOption<?php echo $item->id?>">
                <label class="form-check-label ms-2" for="checkOption<?php echo $item->id?>">
                  <?php echo $item->name ?>
                </label>
              </div>
            <?php } ?>
          </div>
        </div>
      </form>
      <a class="confirm"><?php echo FSText::_("Xác nhận") ?></a>
      <a href="" class="d-block text-end mt-3" style="text-decoration: none; font-weight: 600"><?php echo FSText::_("Chính sách bảo mật") ?></a>
    </div>
  </div>
</div>