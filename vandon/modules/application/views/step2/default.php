<?php
global $config, $tmpl, $device;
$tmpl->addStylesheet("step1", "modules/application/assets/css");

$tmpl->addScript('step2', 'modules/application/assets/js');

?>

<div class="space">
  <h1 class="fw-bold pb-3 text-center"><?php echo FSText::_("Ứng dụng tính chi phí Xây Dựng") ?></h1>
  <p class="text-center"><?php echo FSText::_("Tại Ikay chúng tôi tin rằng con người là là nhân tố quyết định mọi sự thành công.") ?></p>
  <div class="d-flex wrapper justify-content-center pt-3">
    <div class="box-wrapper">
      <ul class="nav nav-pills mb-3 nav-step2" id="pills-tab" role="tablist">
        <li class="nav-item" role="presentation">
          <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true"><?php echo FSText::_("Xây thô tiêu chuẩn") ?></button>
        </li>
        <li class="nav-item" role="presentation">
          <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false"><?php echo FSText::_("Tuỳ chọn") ?></button>
        </li>
      </ul>
      <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
          <h6><?php echo FSText::_("Thông tin công trình") ?></h6>
          <form id="form-step2" action="index.php?module=application&view=step2&task=confirm" method="POST">
            <div>
              <?php foreach ($application_types_categories as $item) { ?>
                <div class="item-option-<?php echo $item->id ?>">
                  <label style="font-weight: 500" for=""><?php echo $item->name ?></label>
                  <div class="d-md-flex" style="position: relative">
                    <div class="my-formcontrol col-md-6 m-0 pe-md-4">
                      <input type="text" id="input-<?php echo $item->alias ?>" name="input-<?php echo $item->alias ?>" value="<?php echo $_SESSION['step1']['build_area'] * $item->percent / 100 ?>" placeholder="<?php echo FSText::_("Nhập diện tích") . ' ' . $item->name  ?>" />
                    </div>
                    <div class="my-formcontrol col-md-6 m-0">
                      <div>
                        <span class="choose-select choose-select-<?php echo $item->id ?>"></span>
                        <span class="clear" data-class="item-option-<?php echo $item->id ?>"><?php echo FSText::_("Xoá") ?></span>
                      </div>
                      <select class="select-option" data-class="choose-select-<?php echo $item->id ?>" name="select-<?php echo $item->alias ?>" id="select-<?php echo $item->alias ?>">
                        <!-- <option style="text-transform: lowercase " selected value="0"><?php echo FSText::_("--Chọn " . $item->name . "--") ?></option> -->
                        <?php foreach ($item->child as $val) { ?>
                          <option data-price="<?php echo $val->price ?>" data-coefficient="<?php echo $val->coefficient ?>" value="<?php echo $val->id ?>"><?php echo $val->title ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
              <?php } ?>
              <?php for ($i = 1; $i <= $_SESSION['step1']['select_number_of_floor']; $i++) { ?>
                <div class="item-tang-<?php echo $i ?>">
                  <label style="font-weight: 500" for=""><?php echo FSText::_("Tầng") . ' ' . $i ?></label>
                  <div class="d-flex">
                    <div class="my-formcontrol col-md-6 m-0 pe-4">
                      <input type="text" id="tang-<?php echo $i ?>" name="input2-tang-<?php echo $i ?>" value="<?php echo $_SESSION['step1']['build_area'] ?>" placeholder="<?php echo FSText::_("Nhập diện tích tầng") . ' ' . $i ?>" />
                    </div>
                    <div class="my-formcontrol col-md-6 m-0">
                      <div style="position: relative">
                        <span class="choose-select"></span>
                        <span class="clear" data-class="item-tang-<?php echo $i ?>"><?php echo FSText::_("Xoá") ?></span>
                      </div>
                      <select>
                        <option value="1"><?php echo FSText::_("100%") ?></option>
                      </select>
                    </div>
                  </div>
                </div>
              <?php } ?>
              <?php if (!empty(@$application_types_categories_more)) { ?>
                <?php foreach (@$application_types_categories_more as $item) { ?>
                  <div class="item-option-more-<?php echo $item->id ?>">
                    <label style="font-weight: 500" for=""><?php echo $item->name ?></label>
                    <div class="d-md-flex" style="position: relative">
                      <div class="my-formcontrol col-md-6 m-0 pe-md-4">
                        <input type="text" id="input-<?php echo $item->alias ?>" name="input-<?php echo $item->alias ?>" value="<?php echo $_SESSION['step1']['build_area'] * $item->percent / 100  ?>" placeholder="<?php echo FSText::_("Nhập diện tích") . ' ' . $item->name  ?>" />
                      </div>
                      <div class="my-formcontrol col-md-6 m-0">
                        <div>
                          <span class="choose-select choose-select-<?php echo $item->id ?>">

                          </span>
                          <span class="clear" data-class="item-option-more-<?php echo $item->id ?>"><?php echo FSText::_("Xoá") ?></span>
                        </div>
                        <select class="select-option" data-class="choose-select-<?php echo $item->id ?>" name="select-<?php echo $item->alias ?>" id="select-<?php echo $item->alias ?>">
                          <!-- <option style="text-transform: lowercase " value="0"><?php echo FSText::_("--Chọn " . $item->name . "--") ?></option> -->
                          <?php foreach ($item->child as $val) { ?>
                            <option data-price="<?php echo $val->price ?>" data-coefficient="<?php echo $val->coefficient ?>" value="<?php echo $val->id ?>"><?php echo $val->title ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                  </div>
                <?php } ?>
              <?php } ?>
              <!-- <a class="btn btn-secondary" style="border-radius: 20px; width: 100%">
                <?php echo FSText::_("Thêm lựa chọn") ?>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                  <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                </svg>
              </a> -->
              <a class="confirm"><?php echo FSText::_("Xác nhận") ?></a>
              <a href="" class="d-block text-end mt-3" style="text-decoration: none; font-weight: 600"><?php echo FSText::_("Chính sách bảo mật") ?></a>
            </div>
          </form>
        </div>
        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
        </div>
      </div>
    </div>
  </div>
</div>