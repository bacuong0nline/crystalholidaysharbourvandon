<?php
global $config, $tmpl, $device;
$tmpl->addStylesheet("step1", "modules/application/assets/css");

$tmpl->addScript('step2', 'modules/application/assets/js');

$total = 0;
?>

<div class="container main space" style="height: 940px">
  <h1 class="fw-bold pb-3 text-left"><?php echo FSText::_("Cảm ơn") ?></h1>
  <p class="text-left"><?php echo FSText::_("Là ứng dụng dự toán thông minh, sau khi có kết quả sẽ gợi ý các công trình phù hợp với yêu cầu của chủ đầu tư") ?></p>

  <div class="d-md-flex">
    <a class="btn-style-1 me-2" href=""><?php echo FSText::_("Liên hệ để nhận báo giá chi tiết")?></a>
    <a class="btn-style-1 mt-3 mt-md-0" href=""><?php echo FSText::_("Tải xuống pdf")?></a>
  </div>

  <h2 class="fw-bold pb-3 text-left mt-5"><?php echo FSText::_("Trợ giúp") ?></h2>
  <p class="text-left"><?php echo FSText::_("Báo giá cơ bản các loại vật lieu tháng 12/ 2022") ?></p>

  <div class="d-flex">
    <a class="btn-style-1 me-2" href=""><?php echo FSText::_("Tải Xuống file PDF")?></a>
  </div>
</div>