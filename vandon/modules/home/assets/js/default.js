$(document).ready(function () {

  let elements = document.getElementsByClassName('play-video-home');

  $('.owl-section-1').owlCarousel({
    loop: false,
    nav: false,
    dots: false,
    smartSpeed: 1500,
    autoplay: 3000,
    // nav: true,
    items: 1
  })
  
  for (let item of elements) {
    lightGallery(item, {
      thumbnail: true,
      animateThumb: true,
      zoomFromOrigin: false,
      allowMediaOverlap: true,
      toggleThumb: true,
      fullScreen: true,
      plugins: [lgVideo, lgZoom]
    })
  }

  $('#utilities-box').owlCarousel({
    loop: false,
    margin: 24,
    nav: true,
    responsive: {
      0: {
        items: 2
      }
    }
  })
  $('#departments').owlCarousel({
    autoplay: true,
    loop: true,
    margin: 24,
    nav: true,
    dots: true,
    smartSpeed: 3000,
    responsive: {
      0: {
        items: 1
      }
    }
  })
  $('#ground-box').owlCarousel({
    // margin:80,
    autoplay: true,
    dots: false,
    center: true,
    loop: true,
    smartSpeed: 1000,
    responsive: {
      0: {
        items: 2,
        nav: false,
      },
      1000: {
        items: 2,
        nav: true
      }
    }
  })
  $('#ground-box-mobile').owlCarousel({
    // margin:80,
    nav: false,
    dots: false,
    center: true,
    loop: true,
    smartSpeed: 1000,
    responsive: {
      0: {
        items: 1
      }
    }
  })
  $('#project-mobile').owlCarousel({
    // margin:80,
    nav: false,
    dots: true,
    center: true,
    loop: true,
    smartSpeed: 1000,
    responsive: {
      0: {
        items: 1
      }
    }
  })
  let prev = `
  <svg width="20" height="24" viewBox="0 0 20 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M18.1113 1L1.00022 12L18.1113 23V1Z" stroke="url(#paint0_linear_938_10137)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    <defs>
    <linearGradient id="paint0_linear_938_10137" x1="22.1606" y1="-2.54072" x2="-9.5606" y2="4.89151" gradientUnits="userSpaceOnUse">
    <stop stop-color="#005176"/>
    <stop offset="0.1302" stop-color="#024D75"/>
    <stop offset="0.2514" stop-color="#094271"/>
    <stop offset="0.3689" stop-color="#142F6B"/>
    <stop offset="0.4836" stop-color="#241562"/>
    <stop offset="0.5056" stop-color="#280F60"/>
    <stop offset="0.5731" stop-color="#2B0F63"/>
    <stop offset="0.6314" stop-color="#350D6E"/>
    <stop offset="0.6863" stop-color="#450B80"/>
    <stop offset="0.7391" stop-color="#5C0899"/>
    <stop offset="0.7904" stop-color="#7905B9"/>
    <stop offset="0.8399" stop-color="#9C00E0"/>
    <stop offset="0.8432" stop-color="#9F00E3"/>
    <stop offset="0.9788" stop-color="#5D0091"/>
    </linearGradient>
    </defs>
  </svg>
  `
  let next = `
  <svg width="20" height="24" viewBox="0 0 20 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M1.11133 1L18.2224 12L1.11133 23V1Z" stroke="url(#paint0_linear_938_10138)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    <defs>
    <linearGradient id="paint0_linear_938_10138" x1="-2.93798" y1="-2.54072" x2="28.7833" y2="4.89151" gradientUnits="userSpaceOnUse">
    <stop stop-color="#005176"/>
    <stop offset="0.1302" stop-color="#024D75"/>
    <stop offset="0.2514" stop-color="#094271"/>
    <stop offset="0.3689" stop-color="#142F6B"/>
    <stop offset="0.4836" stop-color="#241562"/>
    <stop offset="0.5056" stop-color="#280F60"/>
    <stop offset="0.5731" stop-color="#2B0F63"/>
    <stop offset="0.6314" stop-color="#350D6E"/>
    <stop offset="0.6863" stop-color="#450B80"/>
    <stop offset="0.7391" stop-color="#5C0899"/>
    <stop offset="0.7904" stop-color="#7905B9"/>
    <stop offset="0.8399" stop-color="#9C00E0"/>
    <stop offset="0.8432" stop-color="#9F00E3"/>
    <stop offset="0.9788" stop-color="#5D0091"/>
    </linearGradient>
    </defs>
  </svg>

  `
  $('#partner-mobile').owlCarousel({
    // margin:80,
    nav: true,
    dots: false,
    center: true,
    loop: true,
    smartSpeed: 1000,
    navText: [prev, next],
    responsive: {
      0: {
        items: 1
      }
    }
  })

  AOS.init({ once: true });

  let svg1 = `
  <svg width="1010" height="413" viewBox="0 0 1010 413" fill="none" xmlns="http://www.w3.org/2000/svg">
  <g filter="url(#filter0_f_249_1471)">
  <path class="test-path" d="M5 7H338V406H672V207H1005" stroke="white" stroke-width="3"/>
  </g>
  <defs>
  <filter id="filter0_f_249_1471" x="0" y="0.5" width="1010" height="412" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
  <feFlood flood-opacity="0" result="BackgroundImageFix"/>
  <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
  <feGaussianBlur stdDeviation="2.5" result="effect1_foregroundBlur_249_1471"/>
  </filter>
  </defs>
  </svg>
  
  `
  let svg2 = `
  <svg width="1009" height="213" viewBox="0 0 1009 213" fill="none" xmlns="http://www.w3.org/2000/svg">
  <g filter="url(#filter0_f_247_1478)">
  <path class="test-path" d="M4 207H671V6.5L1004 7" stroke="white" stroke-width="3"/>
  </g>
  <defs>
  <filter id="filter0_f_247_1478" x="0" y="0.997559" width="1008" height="211.502" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
  <feFlood flood-opacity="0" result="BackgroundImageFix"/>
  <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
  <feGaussianBlur stdDeviation="2" result="effect1_foregroundBlur_247_1478"/>
  </filter>
  </defs>
  </svg>
  
  
  `

  let svg3 = `
  <svg width="1312" height="515" viewBox="0 0 1312 515" fill="none" xmlns="http://www.w3.org/2000/svg">
  <g filter="url(#filter0_f_244_1475)">
    <path class="test-path" d="M6.5 7.5H438.5V506.631L873.5 506.5V257.5H1306" stroke="white" stroke-width="3" />
  </g>
  <defs>
    <filter id="filter0_f_244_1475" x="0.5" y="0" width="1311.5" height="514.131" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
      <feFlood flood-opacity="0" result="BackgroundImageFix" />
      <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
      <feGaussianBlur stdDeviation="3" result="effect1_foregroundBlur_244_1475" />
    </filter>
  </defs>
</svg>
  `

  let svg4 = `
  <svg width="1310" height="263" viewBox="0 0 1310 263" fill="none" xmlns="http://www.w3.org/2000/svg">
  <g filter="url(#filter0_f_247_1476)">
    <path class="test-path" d="M5 256.5H872.5V6.5H1305" stroke="white" stroke-width="3" />
  </g>
  <defs>
    <filter id="filter0_f_247_1476" x="0" y="0" width="1310" height="263" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
      <feFlood flood-opacity="0" result="BackgroundImageFix" />
      <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
      <feGaussianBlur stdDeviation="2.5" result="effect1_foregroundBlur_247_1476" />
    </filter>
  </defs>
</svg>
  `

  if ($(document).width() > 1599) {
    $(".line-neon").html(svg3);
    $(".line-neon-2").html(svg4);
  } else {
    $(".line-neon").html(svg1);
    $(".line-neon-2").html(svg2);

  }

});

$(document).ready(function () {
  var alert_info = $('#alert_info').val();
  alert_info1 = alert_info ? JSON.parse(alert_info) : [];
  $('.send-contact').click(function () {
    var submit = '#contact-form';
    if (checkFormsubmit2()) {
      $(this).css("pointer-events", "none");
      $("#contact-form").submit();
      $('.send-contact').css("pointer-events", "none")

    }
  })
});

function checkFormsubmit2() {
  $('label.label_error').prev().remove();
  $('label.label_error').remove();
  // email_new = $('#email_new').val();
  if (!notEmpty("fullname", alert_info1[1])) {
    return false;
  }
  if (!notEmpty("phone", alert_info1[2])) {
    return false;
  }

  if (!emailValidator("email", alert_info1[3])) {
    return false;
  }

  else {

  }
  // return false;
  return true;
}


window.onload = function () {
  let elements = document.getElementsByClassName('gallery-mixed-content-demo');
  lightGallery(document.getElementById('gallery-mixed-content-demo'), {
    thumbnail: true,
    animateThumb: true,
    zoomFromOrigin: false,
    allowMediaOverlap: true,
    toggleThumb: true,
    plugins: [lgVideo, lgZoom]
  });



  if ($(window).width() > 1200) {
    $('#fullpage').fullpage({
      licenseKey: "F4D71089-214F4315-958D1E92-109DD6D4",
      menu: "#menu-desktop",
      anchors: ['', "tong-quan", "vi-tri", "tien-ich", "san-pham", "gia-tri-doc-nhat", "doi-tac"],
      normalScrollElements: '#myElement',
      navigation: true,
      afterLoad: function () {
        if ($(".section-1").hasClass("active")) {
          $(".up-page").fadeOut().delay(1000).addClass("d-none")
        } else {
          $(".up-page").fadeIn().delay(300).removeClass("d-none")

        }
        if ($(".section-2").hasClass("active")) {
          $(".gradient-wrapper-victory").css("display", "flex").hide().fadeIn()
          $(".gradient-wrapper-oasis").delay(500).css("display", "flex").hide().fadeIn()
          $(".gradient-wrapper-harmony").delay(1000).css("display", "flex").hide().fadeIn()
          $(".gradient-wrapper-liberty").delay(1500).css("display", "flex").hide().fadeIn()
          $(".gradient-wrapper-heritage").delay(2000).css("display", "flex").hide().fadeIn()
        } else {
          $(".gradient-wrapper").css("display", "none")
        }

        if ($(".section-3").hasClass("active")) {
          let _delay = 0;
          $(".position-box").each(function (i, item) {
            let timeout;
            setTimeout(function () {
              timeout = $(item).delay(200).removeClass('d-none').addClass('d-flex').addClass("do-animation")
            }, 200 * i)
            clearTimeout(timeout);
          })
        } else {
          $(".position-box").removeClass("do-animation").removeClass('d-flex').addClass('d-none')
        }

        if ($(".section-9").hasClass("active")) {
          $(".section-9").animate({ scrollTop: 10 }, "slow");
        }
      }
      // autoScrolling: false

    });
  }


  $(".section-9").scroll(function (e) {
    if ($(".section-9").scrollTop().valueOf() === 0) {
      fullpage_api.moveSectionUp();
    }
  });

  $(".up-to-top").click(function () {
    fullpage_api.moveTo(1, 0);
  });
  $(".up-page").click(function () {
    fullpage_api.moveTo(1, 0);
  });

}