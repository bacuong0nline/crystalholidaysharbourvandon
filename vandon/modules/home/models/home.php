
<?php
class HomeModelsHome extends FSModels
{
    function __construct()
    {
        $this->limit = 4;
        $fstable = FSFactory::getClass('fstable');
        $this->table_name = $fstable->_('fs_products', 1);
        $this->table_menus_items = $fstable->_('fs_menus_items', 1);
        $this->table_add = $fstable->_('fs_address', 1);
    }

    function set_query_body()
    {

        $date1 = FSInput::get("date_search");
        $where = "";
        if (FSInput::get('tag')) {
            $where .= 'AND tags like "%' . FSInput::get('tag') . '%"';
        }
        $fs_table = FSFactory::getClass('fstable');
        $query = " FROM " . $fs_table->getTable('fs_news', 1) . "
						  WHERE 
                               published = 1
						  	" . $where .
            " ORDER BY created_time DESC, id DESC
						 ";
        return $query;
    }
    function get_list_schedule()
    {
        $fs_table = FSFactory::getClass('fstable');

        global $db;
        $query = " SELECT *";
        $query .= " FROM " . $fs_table->getTable('fs_schedule_categories') . "
        WHERE  published = 1 and show_in_homepage = 1 ORDER BY ordering ASC, id DESC LIMIT 4";
        $sql = $db->query($query);
        $result = $db->getObjectList();

        return $result;
    }

    function get_central_system()
    {
        global $db;
        $query = " SELECT id, title, image, alias FROM fs_central";
        $sql = $db->query_limit($query, 6, $this->page);
        $result = $db->getObjectList();

        return $result;
    }
    function getTitleNews() {
        global $db;
        $query = " SELECT * FROM " . $this->table_menus_items . "";
        $query .= " where link like '%module=news%'";
        $query .= " ORDER BY ordering ASC, created_time DESC ";
        $sql = $db->query($query);
        $result = $db->getObject();
        return $result;
    }
    function getProductsList($query_body)
    {
        if (!$query_body)
            return;

        global $db;
        $query = " SELECT is_new, is_hot, name,price_old,price,discount,unit,id,alias,category_name,category_id,image,category_alias,measure, summary FROM " . $this->table_name . "";
        $query .= " WHERE " . $query_body . "";
        $query .= " ORDER BY ordering ASC, created_time DESC LIMIT 6";
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }
    function get_list($query_body)
    {
        if (!$query_body)
            return;

        global $db;
        $query = " SELECT *";
        $query .= $query_body;

        // print_r($query); die;
        $sql = $db->query_limit($query, $this->limit, $this->page);
        $result = $db->getObjectList();
        return $result;
    }
}

?>