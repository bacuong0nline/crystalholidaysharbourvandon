<?php
class HomeControllersHome extends FSControllers
{
    var $module;
    var $view;
    function display()
    {

        global $config, $tmpl;
        // $list_hot = $this->model->getProductsList("show_in_home = 1");
        $query_body = $this->model->set_query_body();
		$list_news = $this->model->get_list($query_body);
        // print_r($list_news);die;

        $grounds = $this->model->get_records('published = 1 and show_in_homepage = 1', 'fs_ground');
        $utilities = $this->model->get_records('published = 1', 'fs_utilities');
        $news = $this->model->get_records('published = 1', 'fs_news', '*', 'created_time desc', '3');
        $banners = $this->model->get_records('published = 1 and category_id = 9', 'fs_banners', '*', 'ordering asc');

        $fstable = FSFactory::getClass('fstable');
        $table_cat = $fstable->_('fs_news_categories', 1);
        $table_news = $fstable->_('fs_news', 1);

        $projects = $this->model->get_records('published = 1 and is_hot = 1', 'fs_project', '*', 'ordering asc, created_time desc');

        $tmpl->assign('og_image', URL_ROOT . 'images/banner.png');


        include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
    }
}
?>