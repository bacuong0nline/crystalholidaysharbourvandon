<?php
global $tmpl;
$tmpl->addStylesheet('hoidap', 'modules/faq/assets/css');
?>

<?php echo $tmpl->load_direct_blocks('breadcrumbs', array('style' => 'simple')); ?>

<main>
  <div class="container">
    <div class="row no-gutters">
      <div class="col-md-3 no-gutters">
        <div class="left-col">
          <div class="title-list d-flex align-items-center">
            <p>Danh mục hỏi đáp</p>
          </div>
          <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            <?php foreach ($list_categories as $key=>$item_category) { ?>
              <a class="nav-link <?php echo ($key == 0) ? 'active' : null  ?>" id="v-pills-<?php echo $item_category->id ?>-tab" data-toggle="pill" href="#v-pills-<?php echo $item_category->id ?>" role="tab" aria-controls="v-pills-<?php echo $item_category->id ?>" aria-selected="true">
                <?php echo $item_category->name ?>
              </a>
            <?php } ?>
          </div>
        </div>
      </div>
      <div class="col-md-9 no-gutters right-col">
        <div class="tab-content" id="v-pills-tabContent">
          <?php foreach ($list_categories as $key=>$item_category) { ?>
            <div class="tab-pane fade show <?php echo $key == 0 ? 'active' : null ?>" id="v-pills-<?php echo $item_category->id ?>" role="tabpanel" aria-labelledby="v-pills-<?php echo $item_category->id ?>-tab">
              <div class="line">
                <h1 class="text--uppercase title-intro"><?php echo $item_category->name ?></h1>
              </div>
              <div class="accordion" id="accordionExample">
                <?php foreach ($list as $item) {
                  if ($item->category_id === $item_category->id) { ?>
                    <div class="card">
                      <div class="card-header d-flex" id="headingOne">
                        <h2 class="mb-0">
                          <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse<?php echo $item->id ?>" aria-expanded="false" aria-controls="collapse<?php echo $item->id ?>">
                            <?php echo $item->title ?>
                          </button>
                        </h2>
                      </div>
                      <div id="collapse<?php echo $item->id ?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                          <?php echo $item->content ?>
                        </div>
                      </div>
                    </div>
                  <?php } ?>
                <?php } ?>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</main>