<?php

class FaqModelsHome extends FSModels {

    function __construct() {
        parent::__construct();
        global $module_config;
//        FSFactory::include_class('parameters');
//        $current_parameters = new Parameters($module_config->params);
//        $limit = $current_parameters->getParams('limit');
        //$limit = 6;
        $fstable = FSFactory::getClass('fstable');
        $this->table_news = $fstable->_('fs_faq',1);
        $this->table_category = $fstable->_('fs_faq_categories',1);
        $this->limit = 9;
    }

    /*
     * select cat list is children of catid
     */


    function set_query_body() {
        $date1 = FSInput::get("date_search");
        $where = "";
        $fs_table = FSFactory::getClass('fstable');
        $query = " FROM " . $fs_table->getTable('fs_faq',1) . "
						  WHERE 
						  	 published = 1
						  	" . $where .
                " ORDER BY created_time DESC, id DESC 
						 ";

        return $query;
    }
    

    function get_list($query_body) {
        if (!$query_body)
            return;

        global $db;
        $query = " SELECT id,title,content,category_id,category_name, category_alias, alias";
        $query .= $query_body;
        //print_r($query); 
        $sql = $db->query_limit($query, $this->limit, $this->page);
        $result = $db->getObjectList();
        return $result;
    }
    function get_list_categories() {
        global $db;
        $query = "SELECT id, name, alias
                  FROM ".$this->table_category."
                  WHERE published = 1 ORDER BY ordering ASC";
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }
    function console_log($output, $with_script_tags = true) {
        $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . 
    ');';
        if ($with_script_tags) {
            $js_code = '<script>' . $js_code . '</script>';
        }
        echo $js_code;
    }
    /*
     * return products list in category list.
     * These categories is Children of category_current
     */

    function getTotal($query_body) {
        if (!$query_body)
            return;
        global $db;
        $query = "SELECT count(*)";
        $query .= $query_body;
        $sql = $db->query($query);
        $total = $db->getResult();
        return $total;
    }

    function getPagination($total) {
        FSFactory::include_class('Pagination');
        $pagination = new Pagination($this->limit, $total, $this->page);
        return $pagination;
    }

    function getFaq($id) {
        if ($id) {
            $where = " id = '$id' ";
        } else {
            $code = FSInput::get('code');
            if (!$code)
                die('Not exist this url');
            $where = " alias = '$code' ";
        }
        $fs_table = FSFactory::getClass('fstable');
        $query = " SELECT id,title,content,category_id,category_alias, summary,hits, video, is_video,
                        alias, tags, created_time, updated_time,seo_title,image,optimal_seo,
                        seo_keyword,seo_description,news_related,author_id,products_related, is_hot
						FROM " . $fs_table->getTable('fs_news',1) . " 
						WHERE 
						" . $where . " ";
        //print_r($query) ;   
        global $db;
        $sql = $db->query($query);
        $result = $db->getObject();
        return $result;
    }

}

?>