<?php

class ContentsControllersOld extends FSControllers
{
	var $module;
	var $view;

	function display()
	{
		$id = FSInput::get('id', 0, 'int');
		if (!$id) {
			$list_child = $this->model->getListChild(12);
			setRedirect(FSRoute::_("index.php?module=contents&view=content&id=".$list_child[0]->id."&code=".$list_child[0]->alias.""));
		} else {
			$data  = $this->model->getData();
			$cat = $this->model->get_record_by_id($data->category_id, 'fs_contents_categories');
			$list_child = $this->model->getListChild($data->category_id);
			$list_hot = $this->model->getListHot();

			// $breadcrumbs = array();
			// $breadcrumbs[] = array(0 =>FSText::_($data->title), 1 => '');
			// global $tmpl;
			// $tmpl -> set_seo_special();
			// $tmpl -> assign('breadcrumbs', $breadcrumbs);
		}
		// $list = $this->model->getList($data->category_id);



		include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
	}
}
