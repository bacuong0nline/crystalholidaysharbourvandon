<?php
global $tmpl;
$tmpl->addStylesheet('home', 'modules/contents/assets/css');
?>

<?php
global $tmpl;
$tmpl->addStylesheet('detail', 'modules/contents/assets/css');

?>


<main>

    <div class="container-fluid pl-0 pr-0">
        <div class="row no-gutters" style="width: 100%">
            <div class="right-col pr-0 col mt-2">
                <?php if ($data->content) { ?>
                    <div class='contents-description p-3'>
                        <?php echo $data->content ?>
                    </div>
                <?php } ?>
                <a class=""></a>
            </div>
        </div>
    </div>
</main>