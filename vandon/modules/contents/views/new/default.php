<?php
global $tmpl;
$tmpl->addStylesheet('home', 'modules/contents/assets/css');
$tmpl->addStylesheet('detail', 'modules/contents/assets/css');

?>



<main>
  <?php if ($tmpl->count_block('top_default')) { ?>
    <?php $tmpl->load_position('top_default'); ?>
  <?php } ?>
  <?php if ($tmpl->count_block('middle')) { ?>
    <?php $tmpl->load_position('middle'); ?>
  <?php } ?>
</main>