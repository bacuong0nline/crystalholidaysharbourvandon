<?php
global $tmpl;
$tmpl->addStylesheet('cat', 'modules/contents/assets/css');
// $tmpl->addStylesheet('default', 'modules/address/assets/css');

$tmpl->addScript('cat', 'modules/contents/assets/js');

?>


<?php if ($tmpl->count_block('top_default')) { ?>
	<?php $tmpl->load_position('top_default'); ?>
<?php } ?>
<?php if ($tmpl->count_block('middle')) { ?>
	<?php $tmpl->load_position('middle'); ?>
<?php } ?>

