<?php
/*
 * Huy write
 */
// controller

class IntroduceControllersHome extends FSControllers
{
	function display()
	{
		// call models
		$model = $this->model;
		//
		//			global $tags_group;

		$query_body = $model->set_query_body();
		$list = $model->get_list($query_body);
		$list_categories = $model->get_list_categories();

		$data = $model->get_record_by_id(1, FSTable::_('fs_introduce', 1));

		$total = $model->getTotal($query_body);
		$pagination = $model->getPagination($total);
		
		$images = $model->get_records('record_id = '.$data->id.'', FSTable::_('fs_introduce_images', 1));

		$partners = $this->model->get_records('published = 1', FSTable::_('fs_products_thuong_hieu', 1));

		$breadcrumbs = array();
		$breadcrumbs[] = array(0 => FSText::_('Giới thiệu'), 1 => '');

		global $tmpl;
		$tmpl->set_seo_special();
		$tmpl->assign('breadcrumbs', $breadcrumbs);

		// call views
		include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
	}

	//                function show_home(){
	//                    $id = FSInput::get('id');
	//                    $model = $this -> model;
	//                    $error_image =  URL_ROOT.'images/logo1lduoc.jpg';
	//                    $data = $model->getNews($id);
	//                    $link = FSRoute::_("index.php?module=news&view=news&id=" . $data->id . "&code=" . $data->alias . "&ccode=" . $data->category_alias);
	//                    $html = '<a class="new-item" href='.$link.' title="'.$data->title.'" >
	//                        <img src="'.URL_ROOT. str_replace('original', 'large_2', $data->image).'" class="img-responsive" onerror="this.onerror=null;this.src="'.$error_image.'";" >
	//                        <p>'.getWord(20, $data->title).'</p>
	//                    </a>';
	//
	//                    $result = array();
	//                    $result['result'] = true;
	//                     $result['html'] = $html;
	//                    echo json_encode($result);
	//                    return $result;
	//                }

}
