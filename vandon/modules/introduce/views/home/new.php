<?php
global $tmpl;
$tmpl->addStylesheet('gioithieu', 'modules/introduce/assets/css');
$tmpl->addStyleSheet("flickity", "templates/default/css");
$tmpl->addScript("flickity", "templates/default/js");
$tmpl->addScript("default", "modules/introduce/assets/js");

?>


<div class="container">
  <div class="d-md-flex justify-content-md-center align-items-md-center flex-column title-intro">
    <h6 class="text-md-center title-intro-1"><?php echo FSText::_("Giới thiệu") ?></h6>
    <h4 class="title text-md-center title-intro-2" style="width: 50%"><?php echo FSText::_("Nhà phát triển quy hoạch thiết kế xây dựng hạ tầng dân sinh") ?></h4>
    <p class="text-md-center title-intro-1"><?php echo FSText::_("Hàng đầu Việt Nam") ?></p>
  </div>
</div>
<div class="banner-video">
  <video id="myVideo" muted autoplay controls preload="metadata" poster="" class="video">
    <source src="<?php echo $data->file_upload ?>" type="video/mp4" />
  </video>
</div>
<!-- <div class="video-box" style="position: relative">
  <div class="video-item" data-lg-size="1280-720" data-video='{"source": [{"src":"<?php echo $data->file_upload ?>", "type":"video/mp4"}], "attributes": {"preload": false, "controls": true}}' data-pinterest-text="Pin it3" data-tweet-text="lightGallery slide  4">
    <div class="play-btn"></div>
    <img style="width: 100%; object-fit: cover" class="img-responsive image-video thumbnail-video" src="<?php echo $data->image ?>" />
  </div>
</div> -->
<div class="container">
  <div class="block-intro-1">
    <h2 class="title-intro-2"><?php echo FSText::_("Về IKAY") ?></h2>
    <p>
      ELKAY VIỆT NAM được thành lập năm 2016 tại. Đội ngũ sáng lập công ty là các nhà thiết kế kiến trúc,nội thất và quy hoạch đô thị có 20 năm kinh nghiệm trong lĩnh vực thiết kế kiến trúc nội thất và cung cấp nội thất cao cấp. Tại Việt Nam và một số nước Đông Nam Á, Châu Âu ELKAY VIỆT NAM nổi tiếng với những thiết kế ấn tượng, sang trọng, đẳng cấp và những dự án quy hoạch đô thị tầm cỡ quốc gia.
    </p>
    <p>
    <ul style="list-style-position: inside;padding-left: 0;">
      <li style="padding: 10px">Năm 2018 ElKAY VIỆT NAM có văn phòng tại AMERICA( USA) và hệ thống showroom, chi nhánh trải dài trên 5 thành phố lớn nhất Việt Nam.</li>
      <li style="padding: 10px">Công ty ELKAY Việt Nam mang đến những thiết kế giao thoa tinh tế giữa văn hoá phương Đông và phương Tây, chinh phục được những khách hàng khó tính nhất.</li>
    </ul>
    </p>
    <a href="" class="btn-style-1" style="margin-auto">Brochure PDF</a>
    <div class="d-flex flex-column-reverse flex-md-row align-item-center pt-5">
      <div class="left-1">
        <div class="box-left-1" style="overflow: hidden">
          <img src="/images/intro2.png" alt="intro2">
        </div>
      </div>
      <div class="right-1 d-flex d-flex flex-column justify-content-center">
        <h2 class="title-intro-2 ">Đội ngũ lãnh đạo</h2>
        <div>
          <p>ELKAY VIỆT NAM được thành lập năm 2016 tại. Đội ngũ sáng lập công ty là các nhà thiết kế kiến trúc,nội thất và quy hoạch đô thị có 20 năm kinh nghiệm trong lĩnh vực thiết kế kiến trúc nội thất và cung cấp nội thất cao cấp.</p>
          <p>Tại Việt Nam và một số nước Đông Nam Á, Châu Âu ELKAY VIỆT NAM nổi tiếng với những thiết kế ấn tượng, sang trọng, đẳng cấp và những dự án quy hoạch đô thị tầm cỡ quốc gia</p>
        </div>
      </div>
    </div>
    <div class="d-flex flex-column flex-md-row align-item-center pt-5">
      <div class="left-2">
        <h2 class="title-intro-2 ">Tầm nhìn</h2>
        <div>
          <p>ELKAY VIỆT NAM được thành lập năm 2016 tại. Đội ngũ sáng lập công ty là các nhà thiết kế kiến trúc,nội thất và quy hoạch đô thị có 20 năm kinh nghiệm trong lĩnh vực thiết kế kiến trúc nội thất và cung cấp nội thất cao cấp.</p>
          <p>Tại Việt Nam và một số nước Đông Nam Á, Châu Âu ELKAY VIỆT NAM nổi tiếng với những thiết kế ấn tượng, sang trọng, đẳng cấp và những dự án quy hoạch đô thị tầm cỡ quốc gia</p>
        </div>
      </div>
      <div class="right-2 d-flex d-flex flex-column justify-content-center">
        <div style="overflow: hidden">
          <img src="/images/intro8.jpeg" alt="intro8">
        </div>
      </div>
    </div>
    <div class="d-flex flex-column-reverse flex-md-row align-item-center pt-5">
      <div class="left-3">
        <div style="overflow: hidden">
          <img src="/images/intro3.png" alt="intro3">
        </div>
      </div>
      <div class="right-3 d-flex d-flex flex-column justify-content-center">
        <h2 class="title-intro-2 ">Đội ngũ lãnh đạo</h2>
        <div>
          <p>ELKAY VIỆT NAM được thành lập năm 2016 tại. Đội ngũ sáng lập công ty là các nhà thiết kế kiến trúc,nội thất và quy hoạch đô thị có 20 năm kinh nghiệm trong lĩnh vực thiết kế kiến trúc nội thất và cung cấp nội thất cao cấp.</p>
          <p>Tại Việt Nam và một số nước Đông Nam Á, Châu Âu ELKAY VIỆT NAM nổi tiếng với những thiết kế ấn tượng, sang trọng, đẳng cấp và những dự án quy hoạch đô thị tầm cỡ quốc gia</p>
        </div>
      </div>
    </div>
  </div>
  <div class="block-intro-2 d-flex align-items-center flex-column justify-content-center">
    <h2 class="title-intro-2 text-center">Giá trị cốt lõi được khẳng định qua 5 giá trị chủ lực</h2>
    <div class="core-box">
      <div class="core-item">
        <div class="summary-core-item">
          <p class="core-title">Tinh hoa</p>
          <p>ELKAY quan niệm đội ngũ có mạnh, công ty mới mạnh và vững vàng. Vì vậy ELKAY luôn lựa chọn những người tinh hoa để hoàn thành được những sản phẩm, dịch vụ tinh hoa. </p>
        </div>
      </div>
      <div class="core-item">
        <div class="summary-core-item">
          <p class="core-title">Tốc độ</p>
          <p>ELKAY đề cao khát vọng với phương châm ” Vinh quang chỉ dành cho những người về đích đúng hẹn”. ELKAY lấy tốc độ, hiệu quả trong từng hành động, từng dự án là tôn chỉ hành động.</p>
        </div>
      </div>
      <div class="core-item">
        <div class="summary-core-item">
          <p class="core-title">Tận tình</p>
          <p>Với phương châm khách hàng là trọng tâm, ELKAY phục vụ khách hàng với tinh thần tự nguyện, chỉ nhận và thực hiện các dự án khi thấy đủ năng lực và khả thi.</p>
        </div>
      </div>
      <div class="core-item">
        <div class="summary-core-item">
          <p class="core-title">Tâm</p>
          <p>ELKAY luôn lấy chữ TÂM làm nền tảng kinh doanh của doanh nghiệp. Đối với nhà nước ELKAY thượng tôn pháp luật, cùng đội ngữ đồng nghiệp duy trì.</p>
        </div>
      </div>
      <div class="core-item">
        <div class="summary-core-item">
          <p class="core-title">Tín</p>
          <p>ELKAY luôn đặt chữ TÍN lên vị trí hàng đầu, lấy chữ TÍN là vũ khí cạnh tranh, và bảo vệ chữ TÍN như bảo vệ danh dự của mình.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="block-intro-3">
  <div class="container">
    <div class="d-md-flex justify-content-between">
      <h2 class="title-intro-2">Đến với chúng tôi</h2>
      <a href="" class="btn-style-3">Tham gia</a>
    </div>
    <div>
      <div class="carousel-intro" data-flickity='{ "groupCells": "80%", "cellAlign": "left", "pageDots": false }'>
        <div class="carousel-cell-intro">
          <img src="/images/intro6.png" alt="intro6" />
        </div>
        <div class="carousel-cell-intro">
          <img src="/images/intro7.png" alt="intro7" />
        </div>
        <div class="carousel-cell-intro">
          <img src="/images/intro8.jpeg" alt="intro8" />
        </div>
      </div>
    </div>

    <div class="d-flex justify-content-between" style="margin-top: 100px;">
      <h2 class="title-intro-2">Giai đoạn phát triển</h2>
    </div>

    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item" role="presentation">
        <button class="nav-link active" id="tab1-tab" data-bs-toggle="tab" data-bs-target="#tab1" type="button" role="tab" aria-controls="tab1" aria-selected="true">
          <?php echo FSText::_("Khách hàng") ?>
        </button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link" id="tab2-tab" data-bs-toggle="tab" data-bs-target="#tab2" type="button" role="tab" aria-controls="tab2" aria-selected="false">
          <?php echo FSText::_("Đối tác toàn cầu") ?>
        </button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link" id="tab3-tab" data-bs-toggle="tab" data-bs-target="#tab3" type="button" role="tab" aria-controls="tab3" aria-selected="false">
          <?php echo FSText::_("Đội nhóm") ?>
        </button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link" id="tab4-tab" data-bs-toggle="tab" data-bs-target="#tab4" type="button" role="tab" aria-controls="tab4" aria-selected="false">
          <?php echo FSText::_("Văn phòng") ?>
        </button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link" id="tab4-tab" data-bs-toggle="tab" data-bs-target="#tab5" type="button" role="tab" aria-controls="tab5" aria-selected="false">
          <?php echo FSText::_("Giá trị") ?>
        </button>
      </li>
    </ul>
    <div class="tab-content" id="myTabContent">
      <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="tab1-tab">
        <div class="customer-box">
          <div>
            <p class="year">2017</p>
            <p class="customer">38</p>
            <p class="text-customer">Khách hàng</p>
          </div>
          <div>
            <p class="year">2018</p>
            <p class="customer">64</p>
            <p class="text-customer">Khách hàng</p>
          </div>
          <div>
            <p class="year">2019</p>
            <p class="customer">125</p>
            <p class="text-customer">Khách hàng</p>
          </div>
          <div>
            <p class="year">2020</p>
            <p class="customer">420</p>
            <p class="text-customer">Khách hàng</p>
          </div>
          <div>
            <p class="year">2021</p>
            <p class="customer">621</p>
            <p class="text-customer">Khách hàng</p>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab2-tab">
        <div class="customer-box">

          <div>
            <p class="year">2017</p>
            <p class="customer">2 công ty tập đoàn</p>
          </div>
          <div>
            <p class="year">2018</p>
            <p class="customer">8 công ty tập đoàn</p>
          </div>
          <div>
            <p class="year">2019</p>
            <p class="customer">11 công ty tập đoàn</p>
          </div>
          <div>
            <p class="year">2020</p>
            <p class="customer">25 công ty tập đoàn</p>
          </div>
          <div>
            <p class="year">2021</p>
            <p class="customer">18 công ty tập đoàn</p>
          </div>
          <div>
            <p class="year">2022</p>
            <p class="customer">33 công ty tập đoàn</p>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="tab3-tab">
        <div class="customer-box">

          <div>
            <p class="year">2017</p>
            <p class="customer">17 thành viên</p>
          </div>
          <div>
            <p class="year">2018</p>
            <p class="customer">38 thành viên</p>
          </div>
          <div>
            <p class="year">2019</p>
            <p class="customer">135 thành viên</p>
          </div>
          <div>
            <p class="year">2020</p>
            <p class="customer">215 thành viên</p>
          </div>
          <div>
            <p class="year">2021</p>
            <p class="customer">175 thành viên</p>
          </div>
          <div>
            <p class="year">2022</p>
            <p class="customer">321 thành viên</p>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="tab4" role="tabpanel" aria-labelledby="tab4-tab">
        <div class="customer-box">

          <div>
            <p class="year">2017</p>
            <p class="customer">3 trụ sở văn phòng</p>
          </div>
          <div>
            <p class="year">2018</p>
            <p class="customer">6 trụ sở văn phòng</p>
          </div>
          <div>
            <p class="year">2019</p>
            <p class="customer">5 trụ sở văn phòng</p>
          </div>
          <div>
            <p class="year">2020</p>
            <p class="customer">5 trụ sở văn phòng</p>
          </div>
          <div>
            <p class="year">2021</p>
            <p class="customer">5 trụ sở văn phòng</p>
          </div>
          <div>
            <p class="year">2022</p>
            <p class="customer">7 trụ sở văn phòng</p>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="tab5" role="tabpanel" aria-labelledby="tab5-tab">
        <div class="customer-box">
          <div>
            <p class="year">2017</p>
            <p class="customer">18 tỷ đồng</p>
          </div>
          <div>
            <p class="year">2018</p>
            <p class="customer">72 tỷ đồng</p>
          </div>
          <div>
            <p class="year">2019</p>
            <p class="customer">175 tỷ đồng</p>
          </div>
          <div>
            <p class="year">2020</p>
            <p class="customer">380 tỷ đồng</p>
          </div>
          <div>
            <p class="year">2021</p>
            <p class="customer">298 tỷ đồng</p>
          </div>
          <div>
            <p class="year">2022</p>
            <p class="customer">423 tỷ đồng</p>
          </div>
        </div>
      </div>
    </div>

  </div>

</div>
<div class="container d-flex justify-content-center align-items-center flex-column intro-partner">
  <div class="title-intro-partner">
    <h4 class="title text-center title-intro-2"><?php echo FSText::_("Đối tác") ?></h4>
    <p class="text-center"><?php echo FSText::_("Khẳng định khả năng và giá trị của bản thân trong từng công việc mà bạn cống hiến. Những đội nhóm phòng ban tốt tạo nên một công ty tốt và thành quả xứng đáng với bạn.") ?></p>
  </div>
  <div style="width: 100%">
    <div class="carousel-partner carousel-nav-partner" data-flickity='{  "cellAlign": "left", "contain": true, "pageDots": false, "prevNextButtons": false }'>
      <div class="carousel-cell-partner">
        <img src="/images/viettel.png" alt="viettel" />
      </div>
      <div class="carousel-cell-partner">
        <img src="/images/viettel.png" alt="viettel" />
      </div>
      <div class="carousel-cell-partner">
        <img src="/images/viettel.png" alt="viettel" />
      </div>
      <div class="carousel-cell-partner">
        <img src="/images/viettel.png" alt="viettel" />
      </div>
      <div class="carousel-cell-partner">
        <img src="/images/viettel.png" alt="viettel" />
      </div>
    </div>

  </div>
</div>