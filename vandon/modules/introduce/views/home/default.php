<?php
global $tmpl;
$tmpl->addStylesheet('gioithieu', 'modules/introduce/assets/css');
$tmpl->addStyleSheet("flickity", "templates/default/css");
$tmpl->addScript("flickity", "templates/default/js");
$tmpl->addScript("default", "modules/introduce/assets/js");

?>


<div class="container banner-intro" style="position: relative">
  <div class="d-md-flex justify-content-md-center align-items-md-center flex-column title-intro">
    <h6 class="text-md-center title-intro-1"><?php echo FSText::_("Giới thiệu") ?></h6>
    <h4 class="title text-md-center title-intro-2"><?php echo FSText::_("Nhà phát triển quy hoạch thiết kế xây dựng hạ tầng dân sinh") ?></h4>
    <p class="text-md-center title-intro-1"><?php echo FSText::_("Hàng đầu Việt Nam") ?></p>
    <img class="mobile" style="position: absolute; top: 0; z-index: -1; left: 0; height: 100%; width: 100%; object-fit: cover;" src="<?php echo $data->image_mobile ?>" alt="<?php echo $data->image_mobile ?>">
    <div class="video-box mobile">
      <div data-lg-size="1280-720" data-video='{"source": [{"src":"<?php echo $data->file_upload ?>", "type":"video/mp4"}], "attributes": {"preload": false, "controls": true}}' data-pinterest-text="Pin it3" data-tweet-text="lightGallery slide  4">
        <svg id="Group_167178" data-name="Group 167178" xmlns="http://www.w3.org/2000/svg" width="70" height="70" viewBox="0 0 70 70">
          <path id="Path_2870" data-name="Path 2870" d="M35,0A35,35,0,1,1,0,35,35,35,0,0,1,35,0Z" fill="#d3a455" />
          <path id="Polygon_2" data-name="Polygon 2" d="M7,0l7,9H0Z" transform="translate(40 28.382) rotate(90)" fill="#fff" />
        </svg>
      </div>
    </div>
  </div>
</div>
<?php if (!IS_MOBILE) { ?>
  <div class="banner-video">
    <video id="myVideo" muted autoplay controls preload="metadata" poster="" class="video">
      <source src="<?php echo $data->file_upload ?>" type="video/mp4" />
    </video>
  </div>
<?php } ?>
<!-- <div class="video-box" style="position: relative">
  <div class="video-item" data-lg-size="1280-720" data-video='{"source": [{"src":"<?php echo $data->file_upload ?>", "type":"video/mp4"}], "attributes": {"preload": false, "controls": true}}' data-pinterest-text="Pin it3" data-tweet-text="lightGallery slide  4">
    <div class="play-btn"></div>
    <img style="width: 100%; object-fit: cover" class="img-responsive image-video thumbnail-video" src="<?php echo $data->image ?>" />
  </div>
</div> -->
<div class="container">
  <?php echo $data->content ?>
</div>
<div class="block-intro-2 d-flex align-items-center flex-column justify-content-center">
  <h2 class="title-intro-2 text-center"><?php echo FSText::_("Giá trị cốt lõi được khẳng định qua 3 giá trị chủ lực") ?></h2>

  <div class="container core-box justify-content-center">
    <div class="core-item">
      <div class="summary-core-item">
        <p class="core-title"><?php echo FSText::_("Trung thực") ?></p>

        <p><?php echo FSText::_("Không điều gì quý giá nếu không có sự trung thực. Vậy nên, mỗi sản phẩm Ikay tạo ra đều trung thực từ những điều nhỏ nhất") ?></p>
      </div>
    </div>

    <div class="core-item">
      <div class="summary-core-item">
        <p class="core-title"><?php echo FSText::_("Trách nhiệm") ?></p>

        <p><?php echo FSText::_("Xuất phát từ lòng tự trọng, với tinh thần không đổ lỗi, không ỷ lại, không trốn tránh, chúng tôi luôn chủ động đề xuất và theo sát mục tiêu.") ?></p>
      </div>
    </div>

    <div class="core-item">
      <div class="summary-core-item">
        <p class="core-title"><?php echo FSText::_("Tử tế") ?></p>

        <p><?php echo FSText::_("Chúng tôi tử tế từ những hành động nhỏ nhất với phong thái chỉn chu, tôn trọng với đồng nghiệp và khách hàng.") ?></p>
      </div>
    </div>
  </div>
</div>
<div class="container pt-5">
  <?php echo $tmpl->load_direct_blocks('person_slide', array('style' => 'style1')); ?>
</div>
<div class="block-intro-3">
  <div class="container">
    <div class="d-md-flex justify-content-between">
      <h2 class="title-intro-2 text-capitalize"><?php echo FSText::_("Đến với chúng tôi") ?></h2>
      <a href="<?php echo FSRoute::_("index.php?module=recruitment&view=home") ?>" class="btn-style-3"><?php echo FSText::_("Tham gia") ?></a>
    </div>
    <div>
      <div class="carousel-intro" data-flickity='{ "groupCells": "80%", "cellAlign": "left", "pageDots": false }'>
        <?php foreach ($images as $item) { ?>
          <div class="carousel-cell-intro">
            <img src="<?php echo str_replace(['original', 'png', 'jpg', 'jpeg'], ['resized', 'webp', 'webp', 'webp'], $item->image) ?>" alt="intro6" />
          </div>
        <?php } ?>
      </div>
    </div>

    <div class="d-flex justify-content-between" style="margin-top: 100px;">
      <h2 class="title-intro-2"><?php echo FSText::_("Giai đoạn phát triển") ?></h2>
    </div>

    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item" role="presentation">
        <button class="nav-link active" id="tab1-tab" data-bs-toggle="tab" data-bs-target="#tab1" type="button" role="tab" aria-controls="tab1" aria-selected="true">
          <?php echo FSText::_("Khách hàng") ?>
        </button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link" id="tab2-tab" data-bs-toggle="tab" data-bs-target="#tab2" type="button" role="tab" aria-controls="tab2" aria-selected="false">
          <?php echo FSText::_("Đối tác toàn cầu") ?>
        </button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link" id="tab3-tab" data-bs-toggle="tab" data-bs-target="#tab3" type="button" role="tab" aria-controls="tab3" aria-selected="false">
          <?php echo FSText::_("Đội nhóm") ?>
        </button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link" id="tab4-tab" data-bs-toggle="tab" data-bs-target="#tab4" type="button" role="tab" aria-controls="tab4" aria-selected="false">
          <?php echo FSText::_("Văn phòng") ?>
        </button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link" id="tab4-tab" data-bs-toggle="tab" data-bs-target="#tab5" type="button" role="tab" aria-controls="tab5" aria-selected="false">
          <?php echo FSText::_("Giá trị") ?>
        </button>
      </li>
    </ul>
    <div class="tab-content" id="myTabContent">
      <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="tab1-tab">
        <div class="customer-box">
          <div>
            <p class="year">2017</p>
            <p class="customer">38</p>
            <p class="text-customer"><?php echo FSText::_("Khách hàng") ?></p>
          </div>
          <div>
            <p class="year">2018</p>
            <p class="customer">64</p>
            <p class="text-customer"><?php echo FSText::_("Khách hàng") ?></p>
          </div>
          <div>
            <p class="year">2019</p>
            <p class="customer">125</p>
            <p class="text-customer"><?php echo FSText::_("Khách hàng") ?></p>
          </div>
          <div>
            <p class="year">2020</p>
            <p class="customer">420</p>
            <p class="text-customer"><?php echo FSText::_("Khách hàng") ?></p>
          </div>
          <div>
            <p class="year">2021</p>
            <p class="customer">621</p>
            <p class="text-customer"><?php echo FSText::_("Khách hàng") ?></p>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab2-tab">
        <div class="customer-box">

          <div>
            <p class="year">2017</p>
            <p class="customer">2</p>
            <p class="text-customer"><?php echo FSText::_("Công ty tập đoàn") ?></p>
          </div>
          <div>
            <p class="year">2018</p>
            <p class="customer">8</p>
            <p class="text-customer"><?php echo FSText::_("Công ty tập đoàn") ?></p>
          </div>
          <div>
            <p class="year">2019</p>
            <p class="customer">11</p>
            <p class="text-customer"><?php echo FSText::_("Công ty tập đoàn") ?></p>
          </div>
          <div>
            <p class="year">2020</p>
            <p class="customer">25</p>
            <p class="text-customer"><?php echo FSText::_("Công ty tập đoàn") ?></p>
          </div>
          <div>
            <p class="year">2021</p>
            <p class="customer">18</p>
            <p class="text-customer"><?php echo FSText::_("Công ty tập đoàn") ?></p>
          </div>
          <!-- <div>
            <p class="year">2022</p>
            <p class="customer">33 công ty tập đoàn</p>
          </div> -->
        </div>
      </div>
      <div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="tab3-tab">
        <div class="customer-box">

          <div>
            <p class="year">2017</p>
            <p class="customer">17</p>
            <p class="text-customer"><?php echo FSText::_("Thành viên") ?></p>
          </div>
          <div>
            <p class="year">2018</p>
            <p class="customer">38</p>
            <p class="text-customer"><?php echo FSText::_("Thành viên") ?></p>
          </div>
          <div>
            <p class="year">2019</p>
            <p class="customer">135</p>
            <p class="text-customer"><?php echo FSText::_("Thành viên") ?></p>
          </div>
          <div>
            <p class="year">2020</p>
            <p class="customer">215</p>
            <p class="text-customer"><?php echo FSText::_("Thành viên") ?></p>
          </div>
          <div>
            <p class="year">2021</p>
            <p class="customer">175</p>
            <p class="text-customer"><?php echo FSText::_("Thành viên") ?></p>
          </div>
          <!-- <div>
            <p class="year">2022</p>
            <p class="customer">321 thành viên</p>
          </div> -->
        </div>
      </div>
      <div class="tab-pane fade" id="tab4" role="tabpanel" aria-labelledby="tab4-tab">
        <div class="customer-box">
          <div>
            <p class="year">2017</p>
            <p class="customer">3</p>
            <p class="text-customer"><?php echo FSText::_("Trụ sở văn phòng") ?></p>
          </div>
          <div>
            <p class="year">2018</p>
            <p class="customer">6</p>
            <p class="text-customer"><?php echo FSText::_("Trụ sở văn phòng") ?></p>
          </div>
          <div>
            <p class="year">2019</p>
            <p class="customer">5</p>
            <p class="text-customer"><?php echo FSText::_("Trụ sở văn phòng") ?></p>
          </div>
          <div>
            <p class="year">2020</p>
            <p class="customer">5</p>
            <p class="text-customer"><?php echo FSText::_("Trụ sở văn phòng") ?></p>
          </div>
          <div>
            <p class="year">2021</p>
            <p class="customer">5</p>
            <p class="text-customer"><?php echo FSText::_("Trụ sở văn phòng") ?></p>
          </div>
          <!-- <div>
            <p class="year">2022</p>
            <p class="customer">7 trụ sở văn phòng</p>
          </div> -->
        </div>
      </div>
      <div class="tab-pane fade" id="tab5" role="tabpanel" aria-labelledby="tab5-tab">
        <div class="customer-box">
          <div>
            <p class="year">2017</p>
            <p class="customer">18</p>
            <p class="text-customer"><?php echo FSText::_("Tỷ đồng") ?></p>
          </div>
          <div>
            <p class="year">2018</p>
            <p class="customer">72</p>
            <p class="text-customer"><?php echo FSText::_("Tỷ đồng") ?></p>
          </div>
          <div>
            <p class="year">2019</p>
            <p class="customer">175</p>
            <p class="text-customer"><?php echo FSText::_("Tỷ đồng") ?></p>
          </div>
          <div>
            <p class="year">2020</p>
            <p class="customer">380</p>
            <p class="text-customer"><?php echo FSText::_("Tỷ đồng") ?></p>
          </div>
          <div>
            <p class="year">2021</p>
            <p class="customer">298</p>
            <p class="text-customer"><?php echo FSText::_("Tỷ đồng") ?></p>
          </div>
          <!-- <div>
            <p class="year">2022</p>
            <p class="customer">423 tỷ đồng</p>
          </div> -->
        </div>
      </div>
    </div>

  </div>

</div>
<div class="container d-flex justify-content-center align-items-center flex-column intro-partner">
  <div class="title-intro-partner">
    <h4 class="title text-center title-intro-2 m-auto"><?php echo FSText::_("Đối Tác") ?></h4>
    <p class="text-center summary"><?php echo FSText::_("Khẳng định khả năng và giá trị của bản thân trong từng công việc mà bạn cống hiến. Những đội nhóm phòng ban tốt tạo nên một công ty tốt và thành quả xứng đáng với bạn.") ?></p>
  </div>
  <div style="width: 100%">
    <div class="carousel-partner carousel-nav-partner" data-flickity='{  "cellAlign": "left", "contain": true, "pageDots": false, "prevNextButtons": false }'>
      <?php foreach ($partners as $item) { ?>
        <div class="carousel-cell-partner">
          <img src="<?php echo $item->image ?>" alt="<?php echo $item->image ?>" />
        </div>
      <?php } ?>
    </div>
  </div>
</div>