
$(document).ready(function () {
    $("#toc").toc({ content: ".detail-content", headings: "h1,h2,h3,h4" });
    $(".button-select").click(function () {
        $('.fa-angle-down').toggleClass('active');
        $('.list-toc').slideToggle();
    });


    $('#toc a').on('click', function (e) {
        e.preventDefault();
        let target = $(this).attr('href');
        target = target.replace(/\./g, '\\.');
        $('html, body').animate({
            'scrollTop': $(target).offset().top - 50
        }, 800);
    });

    $("#send-comment").click(function () {
        var submit = '#form-comment';
        if (checkFormsubmit2()) {
            $(submit).submit()
        }
    })
    lightGallery(document.getElementById('animated-thumbnails'), {
        autoplayFirstVideo: false,
        // animateThumb: false,
        zoomFromOrigin: false,
        pager: false,
        galleryId: "nature",
        plugins: [lgZoom, lgThumbnail],
        mobileSettings: {
            controls: false,
            showCloseIcon: false,
            download: false,
            rotate: false
        }
    });

    $('img').each(function () {
        $(this).attr('data-src', $(this).attr("src"))
        $(this).attr('data-thumb-src', $(this).attr("src"))

    //    $(this).wrap(`<a data-src='${$(this).attr("src")}' ></a>`)
    })

    let elements = document.getElementsByTagName('img');

    lightGallery(document.getElementById('detail-content'), {
        selector: 'img',
        autoplayFirstVideo: false,
        // animateThumb: false,
        zoomFromOrigin: false,
        pager: false,
        galleryId: "nature",
        plugins: [lgZoom, lgThumbnail],
        exThumbImage: 'data-src',
        mobileSettings: {
            controls: false,
            showCloseIcon: false,
            download: false,
            rotate: false
        }
    })


});

function replyComment() {
    news_id = $("#news_id").val();
    comment_id = $(this).attr('data-id');
    link = $("#link").val();
    let form_comment =
        `
<form class="frmCmt frmReply" id="reply-form-${comment_id}" action="index.php?module=news&view=news&task=save_comment&id=${news_id}&comment_id=${comment_id}" method="post">
    <div class="input-cmt input-cmt50">
       <input id="fullname-${comment_id}" name="fullname" class="form-control" type="text" placeholder="Họ và tên" />
    </div>
    <div class="input-cmt input-cmt50">
       <input id="email-${comment_id}" name="email" class="form-control" type="text" placeholder="Email của bạn" />
    </div>
    <div class="input-cmt">
       <textarea id="content-${comment_id}" class="form-control" placeholder="Cảm nghĩ của bạn" name="content"></textarea>
    </div>
    <div class="input-cmt">
       <a class="send-comment-news remove-link">Gửi</a>
       <input type="hidden" name="link" value="${link}">
    </div>
</form>
`
    $(this).parent().after(form_comment);
    $(this).css("pointer-events", "none");
}

function saveReply() {
    if (checkFormsubmit3())
        $(`#reply-form-${comment_id}`).submit()
}

function checkFormsubmit3() {

    $('label.label_error').prev().remove();
    $('label.label_error').remove();

    if (!notEmpty(`fullname-${comment_id}`, "Bạn chưa nhập họ tên")) {
        return false;
    }
    if (!notEmpty(`email-${comment_id}`, "Bạn chưa nhập email")) {
        return false;
    }
    if (!emailValidator(`email-${comment_id}`, "Email chưa đúng định dạng")) {
        return false;
    }
    if (!notEmpty(`content-${comment_id}`, "Bạn chưa nhập nội dung bình luận")) {
        return false;
    }

    else {

    }
    return true;
}

function checkFormsubmit2() {

    $('label.label_error').prev().remove();
    $('label.label_error').remove();

    if (!notEmpty("name-guest", "Bạn chưa nhập tên")) {
        return false;
    }


    if (!notEmpty("comment", "Bạn chưa nhập nội dung bình luận")) {
        return false;
    }

    else {

    }
    return true;
}
