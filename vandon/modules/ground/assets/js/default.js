$(document).ready(function () {
    $("#slide-hot-news").owlCarousel({
        autoplay: true, //Set AutoPlay to 3 seconds
        // autoplaySpeed: 1000,
        loop: true,
        margin: 30,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        nav: true,
        dots: false,
        items: 1
    });

    $('.ShowMore').click(function () {
        var pagecurrent = $(this).attr("data-pagecurrent");
        var nextpage = $(this).attr("data-nextpage");
        var limit = $(this).attr("data-limit");
        var data_cat = $(this).attr("data-cat");
        var data_continue = $(this).attr("data-end");
        pagecurrent = Number(pagecurrent);
        nextpage = Number(nextpage);
        if (data_continue == 1) {
            $(this).attr("data-pagecurrent", nextpage);
            $(this).attr("data-nextpage", nextpage + 1);
            // end_load = true;
            $.ajax({
                type: 'POST',
                dataType: 'html',
                url: '/index.php?module=news&view=home&raw=1&task=load_more',
                data: '&page=' + nextpage + '&limit=' + limit + '&data_cat=' + data_cat,
                success: function (html) {
                    // console.log(html);
                    if (html == 0) {
                        // end_load = true;
                        $(this).attr("data-end", '2');
                        $(".col-btn-show-more").remove();
                    } else {
                        $('.list-more').append(html);
                    }
                }
            });
        }
    });

    $('.next-btn').click(function () {
        var page = $(this).attr('data-page');
        var next_page = $(this).attr('data-next');
        var list = $(this).attr('data-list');
        var total = $(this).attr('data-total');
        var cat = $(this).attr('data-cat');
        next_page = Number(next_page);
        total = Number(total);
        cat = Number(cat);
        $(this).attr("data-page", next_page);
        if(next_page < total){
            $(this).attr("data-next", next_page + 1);
        } else {
            $(this).attr("data-next", 1);
        }

        $(this).prev('.prev-btn').attr('data-page',next_page);

        if(next_page == 1){
            $(this).prev('.prev-btn').attr('data-prev',total);
        } else {
            $(this).prev('.prev-btn').attr('data-prev',next_page-1);
        }

        $.ajax({
            type: 'POST',
            dataType: 'html',
            url: '/index.php?module=news&view=home&raw=1&task=load_right',
            data: '&page=' + page + '&next_page='+ next_page + '&list='+ list + '&cat='+ cat,
            success:function (html) {
                if (html == 0) {

                } else {
                    $('#'+list).html(html);
                }
            }
        })
    });

    $('.prev-btn').click(function () {
        var page = $(this).attr('data-page');
        var prev_page = $(this).attr('data-prev');
        var list = $(this).attr('data-list');
        var total = $(this).attr('data-total');
        var cat = $(this).attr('data-cat');
        prev_page = Number(prev_page);
        total = Number(total);
        cat = Number(cat);
        $(this).attr("data-page", prev_page);

        if(prev_page == 1){
            $(this).attr("data-prev", total);
        } else {
            $(this).attr("data-prev", prev_page - 1);
        }

        $(this).next('.next-btn').attr('data-page',prev_page);
        if(prev_page == total){
            $(this).next('.next-btn').attr('data-next',1);
        } else {
            $(this).next('.next-btn').attr('data-next',prev_page + 1);
        }


        $.ajax({
            type: 'POST',
            dataType: 'html',
            url: '/index.php?module=news&view=home&raw=1&task=load_right',
            data: '&page=' + page + '&next_page='+ prev_page + '&list='+ list + '&cat='+ cat,
            success:function (html) {
                if (html == 0) {

                } else {
                    $('#'+list).html(html);
                }
            }
        })
    })
});