$(document).ready(function () {
    $(".nav-tabs li").click(function () {
        $(".nav-tabs li").removeClass("active");
        $(this).addClass("active");
    })

    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        dots: false,
        navText: [`
        <svg width="18" height="22" viewBox="0 0 18 22" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M17.1111 0L-2.67029e-05 11L17.1111 22V0Z" fill="url(#paint0_linear_1078_4226)"/>
            <defs>
            <linearGradient id="paint0_linear_1078_4226" x1="21.1604" y1="-3.54072" x2="-10.5608" y2="3.89151" gradientUnits="userSpaceOnUse">
            <stop stop-color="#005176"/>
            <stop offset="0.1302" stop-color="#024D75"/>
            <stop offset="0.2514" stop-color="#094271"/>
            <stop offset="0.3689" stop-color="#142F6B"/>
            <stop offset="0.4836" stop-color="#241562"/>
            <stop offset="0.5056" stop-color="#280F60"/>
            <stop offset="0.5731" stop-color="#2B0F63"/>
            <stop offset="0.6314" stop-color="#350D6E"/>
            <stop offset="0.6863" stop-color="#450B80"/>
            <stop offset="0.7391" stop-color="#5C0899"/>
            <stop offset="0.7904" stop-color="#7905B9"/>
            <stop offset="0.8399" stop-color="#9C00E0"/>
            <stop offset="0.8432" stop-color="#9F00E3"/>
            <stop offset="0.9788" stop-color="#5D0091"/>
            </linearGradient>
            </defs>
        </svg>

        `, `<svg width="19" height="22" viewBox="0 0 19 22" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M0.997803 0L18.1089 11L0.997803 22V0Z" fill="url(#paint0_linear_1078_4227)"/>
        <defs>
        <linearGradient id="paint0_linear_1078_4227" x1="-3.0515" y1="-3.54072" x2="28.6697" y2="3.89151" gradientUnits="userSpaceOnUse">
        <stop stop-color="#005176"/>
        <stop offset="0.1302" stop-color="#024D75"/>
        <stop offset="0.2514" stop-color="#094271"/>
        <stop offset="0.3689" stop-color="#142F6B"/>
        <stop offset="0.4836" stop-color="#241562"/>
        <stop offset="0.5056" stop-color="#280F60"/>
        <stop offset="0.5731" stop-color="#2B0F63"/>
        <stop offset="0.6314" stop-color="#350D6E"/>
        <stop offset="0.6863" stop-color="#450B80"/>
        <stop offset="0.7391" stop-color="#5C0899"/>
        <stop offset="0.7904" stop-color="#7905B9"/>
        <stop offset="0.8399" stop-color="#9C00E0"/>
        <stop offset="0.8432" stop-color="#9F00E3"/>
        <stop offset="0.9788" stop-color="#5D0091"/>
        </linearGradient>
        </defs>
        </svg>
        
        `],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    })
})