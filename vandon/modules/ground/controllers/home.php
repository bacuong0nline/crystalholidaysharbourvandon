<?php

class GroundControllersHome extends FSControllers
{
	function display()
	{
		$cid = FSInput::get("cid");
		$ground_categories = $this->model->get_records('published = 1', 'fs_ground_categories');

		foreach($ground_categories as $item) {
			$item->ground = $this->model->get_records('published = 1 and category_id = '.$item->id.' ', 'fs_ground', 'title, image, alias', 'created_time desc');
		}

		if($cid) {
			$list = $this->model->get_records('published = 1 and category_id = '.$cid.' ', 'fs_ground', '*', 'created_time desc');
		} else {
			$list = $this->model->get_records('published = 1', 'fs_ground', '*', 'created_time desc');
		}
		
		global $tmpl, $module_config;

		// $tmpl->assign('title', $ground_categories->seo_title);
		// $tmpl->assign('description', $ground_categories->seo_description);
		// $tmpl->assign('keyword', URL_ROOT . $ground_categories->seo_keyword);


		include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
	}
}
