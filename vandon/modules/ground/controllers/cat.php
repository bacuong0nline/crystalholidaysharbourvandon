<?php

class GroundControllersCat extends FSControllers {

    var $module;
    var $view;

    function display() {
        $model = $this->model;
        $cat = $model->getCategory();
		$cid = FSInput::get("cid");

        if (!$cat) {
            setRedirect(URL_ROOT, 'Không tồn tại danh mục này', 'error');
        }
        $ground_categories = $this->model->get_records('published = 1', 'fs_ground_categories');
		if($cid) {
			$list = $this->model->get_records('published = 1 and category_id = '.$cid.' ', 'fs_ground', '*', 'created_time desc');
		} else {
			$list = $this->model->get_records('published = 1', 'fs_ground', '*', 'created_time desc');
		}


        $query_body = $model->set_query_body($cat->id);
        $list = $model->getNewsList($query_body);

        $fstable = FSFactory::getClass('fstable');

        // $body = $model->set_query_body();
        $cat->child = $model->get_list_news(FSInput::get("cid"));
        // $list_categories = $model->get_list_categories($cat_parent);
        $total = $model->getTotal($query_body);
        $pagination = $model->getPagination($total);

		global $tmpl, $module_config;

		$tmpl->assign('title', @$cat->seo_title);
		$tmpl->addMetades(@$cat->seo_description);
		$tmpl->addMetakey(@$cat->seo_keyword);


        include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
    }

    function filter(){
        $time_tour = FSInput::get('time_tour');
        $price_tour = FSInput::get('price_tour');
        $return = base64_decode(FSInput::get('return'));

        if($time_tour){
            setRedirect(FSRoute::addParameters('time_tour', $time_tour));
        }else if($price_tour){
            setRedirect(FSRoute::addParameters('price_tour', $price_tour));
        }else{
            return $return;
        }
    }

}

?>