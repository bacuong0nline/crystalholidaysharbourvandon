<?php
/*
 * Huy write
 */
// controller

class NewsControllersTags extends FSControllers
{
    var $module;
    var $view;
    function display()
    {
        // call models
        $model = $this -> model;

        $tag_name = FSInput::get('tag');
        $tag = $model->get_record('published = 1 and alias = "'.$tag_name.'" ',FSTable::_('fs_tags',1));
        $data = $model->getNews($tag_name);

        $breadcrumbs = array();
        $breadcrumbs[] = array(0 =>FSText::_('Tin tức'), 1 => FSRoute::_('index.php?module=news&view=home'));
        $breadcrumbs[] = array(0 =>$tag->name, 1 => '');

        $total = $model->getTotal($tag_name);
        $pagination = $model->getPagination($total);

        global $tmpl,$module_config;
        $tmpl -> assign('breadcrumbs', $breadcrumbs);
//        $tmpl -> assign('title', $data->title);
//        $tmpl -> assign('tags', $data->tags);
//        $tmpl -> assign ( 'description', $data->content );
//        $tmpl -> assign ( 'og_image', URL_ROOT.$data->image );

        // seo
        $tmpl -> set_data_seo($data);

        // call views
        include PATH_BASE . 'modules/'.$this->module.'/views/'.$this->view.'/default.php';
    }
}

?>