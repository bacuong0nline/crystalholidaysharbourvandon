<?php
global $tmpl;
$tmpl->addStylesheet('cat', 'modules/news/assets/css');
$tmpl->addStylesheet('home', 'modules/news/assets/css');
$tmpl->addScript('default', 'modules/news/assets/js');
?>
<main>
  <div class="container pl-md-0 pr-md-0">
    <div class="row no-gutters">
      <div class="col no-gutters d-flex justify-content-center mt-xs-2 mt-md-0">
      </div>
    </div>
    <div class="row no-gutters tin-tuc-chung__content d-flex mt-3">
      <?php if (FSInput::get("tag")) { ?>
        <h6 class="tag">Tag: <?php echo FSInput::get("tag") ?></h6>
      <?php } ?>
      <div class="w-100"></div>
      <div class="col-md-12 pt-3">
        <div class="grid-box">
          <?php foreach ($list as $item) {
            $tag = $this->model->get_record_by_id($item->category_id, FSTable::_('fs_news_categories', 1))->tag;

          ?>
            <?php echo $tmpl->news_item($item, $tag) ?>
          <?php } ?>
        </div>
      </div>
    </div>
    <div class="row no-gutters" style="margin-bottom: 60px">
      <div class="col no-gutters d-flex justify-content-center">
        <?php if ($pagination) {
          echo $pagination->showPagination(1);
        }
        ?>
      </div>
    </div>
  </div>
</main>