<?php
global $tmpl;
$tmpl->addStylesheet('home', 'modules/ground/assets/css');
$tmpl->addScript('home', 'modules/ground/assets/js');
$tmpl->addStylesheet('fancybox', 'templates/default/css');
$tmpl->addScript("fancybox", 'templates/default/js');
$cr = date_create(date('Y-m-d H:i:s'));

?>

<div>
    <?php echo $tmpl->load_direct_blocks('banners', array('style' => 'style4', 'category_id_image' => 3)) ?>
    <?php if (!IS_MOBILE) {  ?>
        <div class="container">
            <div class="ground-wrapper">
                <div>
                    <ul class="nav-tabs">
                        <li class="<?php echo FSInput::get("view") == "home" ? "active" : '' ?>">
                            <a href="<?php echo FSRoute::_("index.php?module=ground&view=home") ?>">
                                Tổng quan
                            </a>
                        </li>
                        <?php foreach ($ground_categories as $item) { ?>
                            <li>
                                <a href="<?php echo FSRoute::_("index.php?module=ground&view=cat&ccode=$item->alias&cid=$item->id") ?>">
                                    <?php echo $item->name ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                    <div class="ground-box" style="padding-left: 100px; padding-right: 100px;">
                        <?php foreach ($list as $item) { ?>
                            <div class="ground-item">
                                <div class="image-box" data-fancybox href="<?php echo $item->image  ?>">
                                    <img src="<?php echo format_image($item->image)  ?>" alt="<?php echo $item->image ?>">
                                </div>
                                <p class="text-center fw-semibold mt-3 mb-0"><?php echo $item->title ?></p>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if (IS_MOBILE) { ?>
        <div class="container">
            <div class="ground-wrapper">
                <div>
                    <div class="nav-tabs-mobile">
                        <p class="title-ground" href="<?php echo FSRoute::_("index.php?module=ground&view=home") ?>">
                            Tổng quan
                        </p>
                        <?php foreach ($ground_categories as $item) { ?>
                            <p class="title-ground" href="<?php echo FSRoute::_("index.php?module=ground&view=cat&ccode=$item->alias&cid=$item->id") ?>">
                                <?php echo $item->name ?>
                            </p>
                            <?php if (!empty($item->ground)) { ?>
                                <div class="owl-carousel owl-theme">
                                    <?php foreach ($item->ground as $val) { ?>
                                        <div class="ground-item-box" data-fancybox  href="<?php echo $val->image  ?>">
                                            <img style="width: 100%" src="<?php echo format_image($val->image) ?>" alt="<?php echo $val->alias ?>">
                                            <p style="color: #2C3A61;" class="text-center mb-0 mt-3 fw-semibold"><?php echo $val->title ?></p>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<script>
    window.onload = function() {
        $('[data-fancybox]').fancybox({
            buttons: [
                "zoom",
                //"share",
                "slideShow",
                "fullScreen",
                "download",
                "thumbs",
                "close"
            ],
            protect: true,
            preventCaptionOverlap: true,
        });
    }
</script>