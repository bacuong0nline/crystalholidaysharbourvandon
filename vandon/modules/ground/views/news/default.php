<?php
global $tmpl, $config;

// $tmpl->addStylesheet('home', 'modules/news/assets/css');
$tmpl->addStylesheet('detail', 'modules/news/assets/css');
// $tmpl->addStylesheet('tintucchitiet', 'modules/news/assets/css');
// $tmpl->addStylesheet('tintuc', 'modules/news/assets/css');

$tmpl->addScript('jquery.toc', 'modules/news/assets/js');
$tmpl->addScript('home', 'modules/news/assets/js');
$tmpl->addScript('detail', 'modules/news/assets/js');


$tmpl->addStyleSheet("flickity", "templates/default/css");
$tmpl->addScript("flickity", "templates/default/js");


?>
<main class="news-detail-wrapper" style="background: #F8F9F9;  padding-bottom: 100px">
    <div class="container desktop">
        <div class="d-flex justify-content-center pt-5 pb-4"></div>
    </div>
    <div class="container d-flex justify-content-between">
        <div>
            <p class="fw-bold" style="font-size: 18px">Tin tức</p>
        </div>
        <div class="ml-auto d-flex">
            <div>
                <a onclick="share_click(600, 600, 'fb')" class="ml-3 me-3">
                    <svg width="9" height="16" viewBox="0 0 9 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M8.30769 0.115385V2.65385H6.79808C6.2468 2.65385 5.875 2.76923 5.68269 3C5.49039 3.23077 5.39423 3.57692 5.39423 4.03846V5.85577H8.21154L7.83654 8.70192H5.39423V16H2.45192V8.70192H0V5.85577H2.45192V3.75962C2.45192 2.56731 2.78526 1.64263 3.45192 0.985577C4.11859 0.328526 5.00641 0 6.11539 0C7.05769 0 7.78846 0.0384615 8.30769 0.115385Z" fill="#ABABAB" />
                    </svg>
                </a>

                <a onclick="share_click(600, 600, 'tw')">
                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M16 3.54036C15.41 3.80104 14.7794 3.97708 14.1149 4.05833C14.793 3.65208 15.3151 3.00885 15.5592 2.24375C14.9252 2.61953 14.2234 2.89375 13.474 3.03932C12.8739 2.39948 12.0195 2 11.0769 2C9.26298 2 7.79487 3.46927 7.79487 5.28047C7.79487 5.53776 7.822 5.78828 7.87963 6.02865C5.15024 5.89323 2.72939 4.58646 1.1121 2.59922C0.830685 3.08333 0.667938 3.6487 0.667938 4.24792C0.667938 5.38542 1.25111 6.39089 2.13266 6.97995C1.59017 6.96641 1.08159 6.81745 0.640814 6.57031V6.61094C0.640814 8.20208 1.77326 9.52578 3.27527 9.82708C3.00064 9.90156 2.70905 9.94219 2.41068 9.94219C2.20047 9.94219 1.99364 9.92188 1.7936 9.88125C2.21064 11.1846 3.42445 12.1326 4.86205 12.1596C3.73978 13.0398 2.32253 13.5646 0.783217 13.5646C0.518754 13.5646 0.257682 13.5477 0 13.5172C1.44776 14.4583 3.17355 15 5.02479 15C11.0701 15 14.3725 9.99974 14.3725 5.66302C14.3725 5.52083 14.3691 5.37865 14.3624 5.23984C15.0032 4.77604 15.5592 4.20052 16 3.54036Z" fill="#ABABAB" />
                    </svg>
                </a>
            </div>
        </div>
    </div>
    <div class="container goodstudy-box-detail">
        <h1><?php echo $data->title ?></h1>
        <div class="d-flex mb-4 mt-4">
            <span style="font-size: 14px;" class="me-2"><?php echo format_date4($data->created_time) ?></span>
            |
            <span class="ms-2"><?php echo $data->hits . ' ' .  FSText::_("lượt xem") ?></span>
        </div>
        <div class="toc-box d-none" style="position: relative">

            <p class="font-weight-bold mb-0 fw-bold" style="color: #333">
                <?php echo FSText::_("Mục lục bài chia sẻ") ?>
                <a type="button" data-bs-toggle="collapse" href="#toc" role="button" aria-expanded="false" aria-controls="toc">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-down" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-up" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M7.646 4.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1-.708.708L8 5.707l-5.646 5.647a.5.5 0 0 1-.708-.708l6-6z" />
                    </svg>
                </a>
            </p>
            <ul id="toc" class="collapse show" style="margin-bottom: 0px">
            </ul>
        </div>

        <?php if (!empty($images)) { ?>
            <div class="container pt-3 pb-3" style="padding-bottom: 20px;">
                <div id="animated-thumbnails" class="carousel carousel-main" data-flickity='{"pageDots": false}'>
                    <?php foreach ($images as $item) { ?>
                        <a data-lg-size="1600-1067" data-src="<?php echo str_replace('original', 'original', $item->image) ?>" data-sub-html="" class="carousel-cell">
                            <img src="<?php echo str_replace('original', 'resized', $item->image) ?>" alt="<?php echo $item->image ?>">
                        </a>
                    <?php } ?>
                </div>
                <div class="carousel carousel-nav" data-flickity='{ "asNavFor": ".carousel-main", "contain": true, "pageDots": false, "cellAlign": "left", "prevNextButtons": false }'>
                    <?php foreach ($images as $item) { ?>
                        <div class="carousel-cell">
                            <img src="<?php echo str_replace('original', 'resized', $item->image) ?>" alt="<?php echo $item->image ?>">
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
        <div class="detail-content" id="detail-content">
            <?php echo  preg_replace('/(width|height|srcset|sizes)="[^"]*"/', '', $data->content)  ?>
        </div>

        <?php if (!empty($data->customer)) { ?>
            <div class="customer">
                <p class="fw-bold"><?php echo $data->customer ?></p>
                <p>
                    <svg width="23" height="21" viewBox="0 0 23 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M8.48 0.639997L4.96 15.44L4.32 12C5.6 12 6.64 12.4 7.44 13.2C8.24 13.9467 8.64 14.96 8.64 16.24C8.64 17.52 8.24 18.56 7.44 19.36C6.64 20.16 5.65333 20.56 4.48 20.56C3.2 20.56 2.16 20.16 1.36 19.36C0.613334 18.5067 0.24 17.4667 0.24 16.24C0.24 15.76 0.266667 15.3333 0.32 14.96C0.373334 14.5867 0.48 14.1333 0.64 13.6C0.8 13.0667 1.01333 12.4267 1.28 11.68L4.64 0.639997H8.48ZM22.08 0.639997L18.56 15.44L17.92 12C19.2 12 20.24 12.4 21.04 13.2C21.84 13.9467 22.24 14.96 22.24 16.24C22.24 17.52 21.84 18.56 21.04 19.36C20.24 20.16 19.2533 20.56 18.08 20.56C16.8 20.56 15.76 20.16 14.96 19.36C14.2133 18.5067 13.84 17.4667 13.84 16.24C13.84 15.76 13.8667 15.3333 13.92 14.96C13.9733 14.5867 14.08 14.1333 14.24 13.6C14.4 13.0667 14.6133 12.4267 14.88 11.68L18.24 0.639997H22.08Z" fill="#0E0E0E" />
                    </svg>
                </p>
                <p><?php echo $data->customer_review ?></p>
            </div>
        <?php } ?>

        <?php if (!empty($questions)) { ?>
            <?php echo $tmpl->list_question($questions) ?>
        <?php } ?>

        <div class="advice mt-5">
            <p style="font-size: 30px" class="fw-bold"><?php echo FSText::_("Bạn cần tư vấn") ?></p>
            <?php echo $config['advice'] ?>
        </div>


        <div class="container d-flex mt-5 mb-3">
            <?php if (!empty($list_tag)) { ?>
                <div class="d-flex col-md-9 flex-wrap">
                    <p class="ml-1"><?php echo FSText::_("Chủ đề") ?>:</p>
                    <div>
                        <?php foreach (@$list_tag as $item) { ?>
                            <a href="<?php echo FSText::_("index.php?module=news&view=home&task=tag&Itemid=4&tag=$item") ?>" class="tags">
                                <?php echo $item ?>
                            </a>
                        <?php } ?>
                    </div>
                </div>
            <?php  } ?>

            <!-- <div class="col-md-3 ml-auto d-flex">
                <span class="grey--text"><?php echo FSText::_("Chia sẻ:") ?></span>
                <div>
                    <a onclick="share_click(600, 600, 'fb')" class="ml-3 mr-3">
                        <svg width="9" height="16" viewBox="0 0 9 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8.30769 0.115385V2.65385H6.79808C6.2468 2.65385 5.875 2.76923 5.68269 3C5.49039 3.23077 5.39423 3.57692 5.39423 4.03846V5.85577H8.21154L7.83654 8.70192H5.39423V16H2.45192V8.70192H0V5.85577H2.45192V3.75962C2.45192 2.56731 2.78526 1.64263 3.45192 0.985577C4.11859 0.328526 5.00641 0 6.11539 0C7.05769 0 7.78846 0.0384615 8.30769 0.115385Z" fill="#ABABAB" />
                        </svg>
                    </a>

                    <a onclick="share_click(600, 600, 'tw')">
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M16 3.54036C15.41 3.80104 14.7794 3.97708 14.1149 4.05833C14.793 3.65208 15.3151 3.00885 15.5592 2.24375C14.9252 2.61953 14.2234 2.89375 13.474 3.03932C12.8739 2.39948 12.0195 2 11.0769 2C9.26298 2 7.79487 3.46927 7.79487 5.28047C7.79487 5.53776 7.822 5.78828 7.87963 6.02865C5.15024 5.89323 2.72939 4.58646 1.1121 2.59922C0.830685 3.08333 0.667938 3.6487 0.667938 4.24792C0.667938 5.38542 1.25111 6.39089 2.13266 6.97995C1.59017 6.96641 1.08159 6.81745 0.640814 6.57031V6.61094C0.640814 8.20208 1.77326 9.52578 3.27527 9.82708C3.00064 9.90156 2.70905 9.94219 2.41068 9.94219C2.20047 9.94219 1.99364 9.92188 1.7936 9.88125C2.21064 11.1846 3.42445 12.1326 4.86205 12.1596C3.73978 13.0398 2.32253 13.5646 0.783217 13.5646C0.518754 13.5646 0.257682 13.5477 0 13.5172C1.44776 14.4583 3.17355 15 5.02479 15C11.0701 15 14.3725 9.99974 14.3725 5.66302C14.3725 5.52083 14.3691 5.37865 14.3624 5.23984C15.0032 4.77604 15.5592 4.20052 16 3.54036Z" fill="#ABABAB" />
                        </svg>
                    </a>
                </div>
            </div> -->
        </div>

        <div class="binhluan">
            <form action="index.php?module=news&view=news&task=set_comment" method="post" id="form-comment" name="form-comment" enctype="multipart/form-data">
                <div class="col-12 mb-4">
                    <input type="text" placeholder="<?php echo FSText::_("Nhập tên của bạn") ?>" id="name-guest" name="name-guest" class="name-guest"></textarea>
                </div>
                <div class="col-12">
                    <textarea placeholder="<?php echo FSText::_("Nội dung") ?>" id="comment" name="comment" class="your-comment"></textarea>
                </div>
                <input type="hidden" id="id-news" name="id-news" value="<?php echo $data->id ?>" />

                <div class="w-100">
                    <div class="col no-gutters d-flex justify-content-center justify-content-md-end align-items-center send-comment">
                        <a id="send-comment" class="font-weight-bold"><?php echo FSText::_("Gửi bình luận") ?></a>
                    </div>
                </div>
            </form>

            <?php foreach ($list_cmt as $item) { ?>
                <div style="background-color: white" class="col-12 pl-4 pr-4 pt-4 ms-5">
                    <div class="col ">
                        <div class="d-flex">
                            <p class="mr-3 user-comment font-weight-bold mb-0 d-flex align-items-center">
                                <img class="mr-1" style="width: 20px!important" src="<?php echo $config['logo'] ?>" alt="logo" />
                                <span class="fw-bold ms-3" style="font-size: 24px"><?php echo $item->name ?></span>
                            </p>
                        </div>
                        <div class="mt-3 ">
                            <p class="mb-0 mt-2 mb-3"> <?php echo $item->comment ?></p>
                            <p class="mb-0">
                                <span class="me-2">
                                    <svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M9.23503 0C7.40851 0 5.62302 0.469192 4.10432 1.34824C2.58563 2.22729 1.40196 3.47672 0.702978 4.93853C0.00400136 6.40034 -0.178883 8.00887 0.177453 9.56072C0.533788 11.1126 1.41334 12.538 2.70488 13.6569C3.99642 14.7757 5.64195 15.5376 7.43336 15.8463C9.22478 16.155 11.0816 15.9965 12.7691 15.391C14.4566 14.7855 15.8989 13.7602 16.9137 12.4446C17.9284 11.129 18.4701 9.58225 18.4701 8C18.4627 5.88022 17.4874 3.84906 15.7571 2.35014C14.0268 0.851219 11.6821 0.00633091 9.23503 0V0ZM9.23503 14C7.86514 14 6.52602 13.6481 5.387 12.9888C4.24798 12.3295 3.36022 11.3925 2.83599 10.2961C2.31176 9.19974 2.17459 7.99334 2.44185 6.82946C2.7091 5.66557 3.36876 4.59647 4.33742 3.75736C5.30607 2.91824 6.54022 2.3468 7.88378 2.11529C9.22734 1.88378 10.62 2.0026 11.8856 2.45672C13.1512 2.91085 14.2329 3.67988 14.994 4.66658C15.7551 5.65327 16.1613 6.81331 16.1613 8C16.1558 9.58984 15.4243 11.1132 14.1266 12.2374C12.8288 13.3616 11.0703 13.9953 9.23503 14ZM10.3894 7.6L13.5062 10.3L11.8901 11.7L8.08065 8.4V3H10.3894V7.6Z" fill="#CBD0D3" />
                                    </svg>
                                </span>
                                <?php echo format_time_ago(strtotime($item->created_time)) ?>
                            </p>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
        <div class="d-flex mt-5 justify-content-center" style="width: 100%">
            <!-- <a class="facebook-button mt-4" onclick="share_click(600, 600, 'fb')">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                    <path d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z" />
                </svg>
                <span class="ms-2"><?php echo FSText::_("Chia sẻ") ?></span>
            </a>
            <a class="tweet-button mt-4 ms-3" onclick="share_click(600, 600, 'tw')">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                    <path d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z" />
                </svg>
                <span class="ms-2"><?php echo FSText::_("Chia sẻ") ?></span>
            </a>
            <a target="_blank" class="pinterest-button mt-4 ms-3" href="//pinterest.com/pin/create/link/?url=<?php echo FSRoute::_("index.php?module=news&view=news&code=$data->alias&id=$data->id") ?>">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
                    <path d="M204 6.5C101.4 6.5 0 74.9 0 185.6 0 256 39.6 296 63.6 296c9.9 0 15.6-27.6 15.6-35.4 0-9.3-23.7-29.1-23.7-67.8 0-80.4 61.2-137.4 140.4-137.4 68.1 0 118.5 38.7 118.5 109.8 0 53.1-21.3 152.7-90.3 152.7-24.9 0-46.2-18-46.2-43.8 0-37.8 26.4-74.4 26.4-113.4 0-66.2-93.9-54.2-93.9 25.8 0 16.8 2.1 35.4 9.6 50.7-13.8 59.4-42 147.9-42 209.1 0 18.9 2.7 37.5 4.5 56.4 3.4 3.8 1.7 3.4 6.9 1.5 50.4-69 48.6-82.5 71.4-172.8 12.3 23.4 44.1 36 69.3 36 106.2 0 153.9-103.5 153.9-196.8C384 71.3 298.2 6.5 204 6.5z" />
                </svg>
                <span class="ms-2"><?php echo FSText::_("Chia sẻ") ?></span>
            </a> -->
            <div class="addthis_inline_share_toolbox_439y"></div>
        </div>

        <div class="advice mt-5">
            <p style="font-size: 30px" class="fw-bold"><?php echo FSText::_("Nhận cập nhật") ?></p>
            <p><?php echo FSText::_("Bài viết mới sẽ được gửi tới email mà bạn đăng ký") ?></p>
            <form>
                <div class="d-flex">
                    <div style="width: 80%">
                        <input class="email-register" type="text" name="email-register" placeholder="<?php echo FSText::_("Địa chỉ email") ?>" />
                    </div>
                    <a class="email-register-btn"><?php echo FSText::_("Đăng ký") ?></a>
                </div>
            </form>
        </div>
    </div>

</main>

<div class="related-news">
    <p class="fw-bold title-relate"><?php echo FSText::_("Nội dung liên quan") ?></p>
    <div class="grid-relate">
        <?php foreach ($relate_news_list as $val2) {
            $tag = $this->model->get_record_by_id($val2->category_id, FSTable::_('fs_news_categories', 1))->tag;
        ?>
            <?php echo $tmpl->news_item($val2, $tag) ?>
        <?php } ?>
    </div>
</div>

<div class="related-news">
    <p class="fw-bold title-relate"><?php echo FSText::_("Chủ đề khác") ?></p>
    <div class="grid-relate">
        <?php foreach ($relate_news_list_2 as $val2) {
            $tag = $this->model->get_record_by_id($val2->category_id, FSTable::_('fs_news_categories', 1))->tag;
        ?>
            <?php echo $tmpl->news_item($val2, $tag) ?>
        <?php } ?>
    </div>
</div>


<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-6365db9bc04aa38b"></script>


<script>
    function share_click(width, height, type) {
        var leftPosition, topPosition;
        //Allow for borders.
        leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
        //Allow for title and status bars.
        topPosition = (window.screen.height / 2) - ((height / 2) + 50);
        var windowFeatures = "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";
        u = location.href;
        t = document.title;
        if (type === 'fb') {
            window.open('http://www.facebook.com/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t), 'sharer', windowFeatures);
        }
        if (type === 'tw') {
            window.open('http://twitter.com/intent/tweet?url=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t), 'sharer', windowFeatures);
        }
        return false;
    }
</script>