<div style="width: 65%" class="m-left pl-0 pr-0 comment-news">
  <h5 class="font-weight-bold"><?php echo FSText::_("Ý kiến bạn đọc")?> <?php echo !empty($list_comment) ? '('.count($total_comment).')' : null?></h5>
  <form action="index.php?module=news&view=news&task=save_comment&id=<?php echo FSInput::get("id")?>" method="post" id="comments-news">
    <div class="d-flex align-items-start form-comment" style="width: 100%">
      <img class="mr-3" src="/images/no-avatar.png" alt="no-avatar" />
      <div class="row justify-content-between" style="width: 100%; margin-left: 20px">
        <div class="input-feild" style="width: 48%">
          <input id="fullname" name="fullname" class="form-control" style="width: 100%" type="text" placeholder="Họ và tên" />
        </div>
        <div class="input-feild" style="width: 48%">
          <input id="email" name="email" class="form-control" style="width: 100%" type="text" placeholder="Email của bạn" />
        </div>
        <div class="w-100"></div>
        <textarea id="content" class="form-control mt-4" placeholder="Cảm nghĩ của bạn" name="content" style="width: 100%"></textarea>
        <div class="w-100"></div>
        <div class="d-flex mt-2" style="width: 100%">
          <div style="width: 50%">
            <a class="send-comment-news remove-link"><?php echo FSText::_("Gửi bình luận")?></a>
          </div>
          <div style="width: 50%; text-align: right; color: #AAAAAA">
            <p class="mb-0 note-comment"><?php echo FSText::_("Ý kiến của bạn sẽ được biên tập trước khi đăng.<br />Xin vui lòng gõ tiếng Việt có dấu.")?></p>
          </div>
        </div>
      </div>
    </div>
    <input id="link" type="hidden" name="link" value="<?php echo FSRoute::_("index.php?module=news&view=news&id=$data->id&code=$data->alias")?>">
  </form>
</div>
<input type="hidden" id="news_id" value="<?php echo FSInput::get("id")?>" />