<?php
global $tmpl;

$tmpl->addStylesheet('fancybox', 'templates/default/css');
$tmpl->addScript("fancybox", 'templates/default/js');
$tmpl->addStylesheet('home', 'modules/ground/assets/css');
$tmpl->addTitle($cat->name);

?>


<div>
  <?php echo $tmpl->load_direct_blocks('banners', array('style' => 'style4', 'category_id_image' => 3)) ?>
  <div class="container">
    <div class="ground-wrapper">
      <div>
        <ul class="nav-tabs">
          <li class="<?php echo FSInput::get("view") == "home" ? "active" : '' ?>">
            <a href="<?php echo FSRoute::_("index.php?module=ground&view=home") ?>">
              Tổng quan
            </a>
          </li>
          <?php foreach ($ground_categories as $item) { ?>
            <li>
              <a class="<?php echo FSInput::get("cid") == $item->id ? "active" : '' ?>" href="<?php echo FSRoute::_("index.php?module=ground&view=cat&ccode=$item->alias&cid=$item->id") ?>">
                <?php echo $item->name ?>
              </a>
            </li>
          <?php } ?>
        </ul>
        <div class="ground-box" style="padding-left: 100px; padding-right: 100px;">
          <?php foreach ($list as $item) { ?>
            <div class="ground-item">
              <div class="image-box" data-fancybox href="<?php echo $item->image ?>">
                <img src="<?php echo $item->image ?>" alt="<?php echo $item->image ?>">
              </div>
              <p class="text-center fw-semibold mt-3 mb-0"><?php echo $item->title ?></p>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  window.onload = function() {
    $('[data-fancybox]').fancybox({
      buttons: [
        "zoom",
        //"share",
        "slideShow",
        "fullScreen",
        "download",
        "thumbs",
        "close"
      ],
      protect: true,
      preventCaptionOverlap: true,
    });
  }
</script>