<?php

class GroundModelsCat extends FSModels {

    function __construct() {
        parent::__construct();
        global $module_config;
        $this->limit = 12;
        $fstable = FSFactory::getClass('fstable');
        $this->table_news = $fstable->_('fs_ground',1);
        $this->table_cat_news = $fstable->_('fs_ground_categories',1);
    }

    function set_query_body($cid) {
        $query_ordering = '';
        $where = "";
        $query = ' FROM ' . $this->table_news . '
						  WHERE ( category_id = ' . $cid . ' 
						  	OR category_id_wrapper like "%,' . $cid . ',%" )
						  	AND published = 1
						    '.$where.' ';
        return $query;
    }
    /*
     * get Category current
     * By Id or By code
     */
    function get_list_categories($id,$parent) {
        global $db;
        if($parent == 0){
            $query = "SELECT id, name, alias
                  FROM ".$this->table_cat_news."
                  WHERE published = 1 and parent_id = $id 
                  ORDER BY id ASC";
        } else
            $query = "SELECT id, name, alias
                  FROM ".$this->table_cat_news."
                  WHERE published = 1 and parent_id = $parent 
                  ORDER BY id ASC";

        $sql = $db->query($query);
        $result = $db->getObjectList();
        // print_r($result);die;
        return $result;
    }
    function getCategory() {
        $code = FSInput::get('ccode');
        if ($code) {
            $where = " AND alias = '$code' ";
        } else {
            $id = FSInput::get('cid', 0, 'int');
            if (!$id)
                die('Not exist this url');
            $where = " AND id = '$id' ";
        }
        $query = "  SELECT *
				    FROM " . $this->table_cat_news . " 
					WHERE published = 1 " . $where;
        global $db;
        $sql = $db->query($query);
        $result = $db->getObject();
        return $result;
    }

    function getNewsList($query_body) {
        if (!$query_body)
            return;

        global $db;
        $query = " SELECT id, alias, title, image, created_time, summary";
        $query .= $query_body;
        $query .= " order by ordering desc, created_time desc";
        $sql = $db->query_limit($query, $this->limit, $this->page);
        $result = $db->getObjectList();

        return $result;
    }
    function get_list_news($cat_id){
        global $db;
        $query = "  SELECT *
                    FROM " . $this->table_news . "
                    WHERE published = 1 AND category_id_wrapper like '%," . $cat_id . ",%' order by created_time desc ";
        $sql = $db->query_limit($query, $this->limit, $this->page);
        $result = $db->getObjectList();
        // print_r($db);
        return $result;
    }
    function getTotal($query_body)
    {
        if (!$query_body)
            return;
        global $db;
        $query = "SELECT count(*)";
        $query .= $query_body;
        $sql = $db->query($query);
        $total = $db->getResult();
        return $total;
    }

    function getPagination($total)
    {
        FSFactory::include_class('Pagination');
        $pagination = new Pagination($this->limit, $total, $this->page);
        return $pagination;
    }

    function get_local_add($id){
        $where = '';
        if ($id) {
            $where .= ' document_id =' . $id;
        } else{
            return false;
        }
        global $db;
        $query = ' SELECT name,size,id
					FROM ' . $this->table_add . ' 
					WHERE ' . $where . '
					ORDER BY id
							';
        $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }


    function get_list_all(){

        global $db;
        $query = ' SELECT id,rating_sum,rating_count,type_id
                    FROM ' . $this->table_name . ' 
                    WHERE published = 1';
        $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }
    function get_list_hot($id){
        global $db;
        $query = "SELECT id,alias,title,image,created_time,author
                  FROM ".$this->table_news."
                  WHERE published = 1 and category_id_wrapper like ',%".$id."%,' and is_hot = 1 order by id desc, created_time desc";
        $sql = $db->query_limit($query,10);
        $result = $db->getObjectList();
        return $result;
    }
    function get_list_showhome($id){
        global $db;
        $query = "SELECT id,alias,title,image,created_time,author,category_id,category_name,category_alias
                  FROM ".$this->table_news."
                  WHERE published = 1 and category_id_wrapper like ',%".$id."%,' and show_in_homepage = 1 ORDER by RAND(), id desc, created_time desc";
        $sql = $db->query_limit($query,4);
        $result = $db->getObjectList();
        return $result;
    }
}

?>