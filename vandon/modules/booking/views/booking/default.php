<?php
global $config, $tmpl, $device;
$tmpl->addStylesheet('booking', 'modules/booking/assets/css');
$tmpl->addScript('booking', 'modules/booking/assets/js');

$alert_info = array(
	0 => FSText::_('Nhập Từ Khóa'),
	1 => FSText::_('Bạn chưa nhập họ và tên'),
	2 => FSText::_('Bạn chưa nhập nội dung'),
	3 => FSText::_('Bạn chưa nhập email'),
	4 => FSText::_('Email không hợp lệ'),
	5 => FSText::_('Bạn chưa nhập số điện thoại'),
	6 => FSText::_('Số điện thoại không hợp lệ'),
	7 => FSText::_('Vui lòng nhập từ'),
	8 => FSText::_('số'),
	9 => FSText::_('đến'),
	10 => FSText::_('Bạn chưa nhập địa chỉ'),
);

?>



<input type="hidden" id="alert_info" value='<?php echo json_encode($alert_info) ?>'>
<main>
  <div class="d-flex flex-column justify-content-center align-items-center title">
    <h1 class="fw-bold"><?php echo FSText::_("Đặt lịch thiết kế") ?></h1>
    <p class="pb-4 text-center summary-booking" style="width: 25%"><?php echo FSText::_("Chúng tôi có mặt trực tiếp, trò chuyện trực tiếp, qua email và điện thoại.") ?></p>
  </div>
  <div class="d-flex ">
    <form action="index.php?module=booking&view=booking&task=save_contact2" id="booking-form" method="post" class="m-auto">
      <div class="input-form">
        <label for="name-booking">
          <?php echo FSText::_("Họ và tên") ?>
        </label>
        <br>
        <input type="text" id="name-booking" name="name-booking" placeholder="<?php echo FSText::_("Họ và tên") ?>" />
      </div>
      <div class="input-form">
        <label for="phone-booking">
          <?php echo FSText::_("Số điện thoại") ?>
        </label>
        <br>
        <input type="text" id="phone-booking" name="phone-booking" placeholder="<?php echo FSText::_("Số điện thoại") ?>" />
      </div>
      <div class="input-form">
        <label for="content">
          <?php echo FSText::_("Nội dung") ?>
        </label>
        <br>
        <textarea name="content" id="content" cols="30" rows="5"></textarea>
      </div>
      <a class="send-contact"><?php echo FSText::_("Gửi") ?></a>
    </form>
  </div>

</main>