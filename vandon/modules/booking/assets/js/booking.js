$(document).ready(function () {
  $('.send-contact').click(function () {
    var submit = '#contact-form';
    if (checkFormsubmit2()) {
      $("#booking-form").submit();
      $(this).css("pointer-events", "none");
    }
  })
})


var alert_info = $('#alert_info').val();
let alert_info1 = alert_info ? JSON.parse(alert_info) : [];

function checkFormsubmit2() {
    $('label.label_error').prev().remove();
    $('label.label_error').remove();

    if (!notEmpty("name-booking", alert_info1[1])) {
        return false;
    }

    if (!notEmpty("phone-booking", alert_info1[5])) {
        return false;
    }
    if (!isPhone("phone-booking", alert_info1[6])) {
        return false;
    }
    if (!lengthRestriction("phone-booking", "10", "12", alert_info1[7] + ' 10 ' + alert_info1[9] + ' 12 ' + alert_info1[8])) {
        return false;
    }

    if (!notEmpty("content", alert_info1[2])) {
        return false;
    }
    else {

    }
    // return false;
    return true;
}