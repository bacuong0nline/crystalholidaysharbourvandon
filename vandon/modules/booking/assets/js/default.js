$(document).ready(function () {
    var flkty = new Flickity('.carousel-project', {
        cellAlign: 'left',
        autoPlay: true,
        // fade: true
        // prevNextButtons: false

    });

    if ($(window).width() < 768) {
        var flkty = new Flickity('.carousel-project-mobile', {
            cellAlign: 'center',
            autoPlay: true,

        });
    }


    $('#news-box').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            1300: {
                items: 3
            },
            1900: {
                items: 4
            },
        }
    })
})
