<?php
global $config, $tmpl;

$tmpl->addStylesheet('home', 'modules/notfound/assets/css');

?>

<div class="error-page row-item" style="line-height: 24px;margin-bottom: 30px;">
	<?php echo $config['info_404']; ?>
</div><!-- END: .error-page -->