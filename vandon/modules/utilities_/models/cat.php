<?php

class ProjectModelsCat extends FSModels {

    function __construct() {
        parent::__construct();
        global $module_config;
        $fstable = FSFactory::getClass('fstable');
        $this->table_name = $fstable->_('fs_project',1);
        $this->table_category = $fstable->_('fs_introduce_categories',1);
        $this->limit = 12;
    }

    function set_query_body() {
        $date1 = FSInput::get("date_search");
        $where = "and category_id_wrapper like '%,".FSInput::get("cid").",%'";
        $fs_table = FSFactory::getClass('fstable');
        $query = " FROM " . $this->table_name. "
						  WHERE 
						  	 published = 1
						  	" . $where .
                " ORDER BY created_time DESC, id DESC 
						 ";

        return $query;
    }
    function get_list($query_body) {
        if (!$query_body)
            return;

        global $db;
        $query = " SELECT *";
        $query .= $query_body;
        $sql = $db->query_limit($query, $this->limit, $this->page);
        $result = $db->getObjectList();

        return $result;
    }
}

?>