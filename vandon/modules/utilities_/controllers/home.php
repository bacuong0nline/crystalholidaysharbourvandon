<?php

	
	class UtilitiesControllersHome extends FSControllers
	{
		function display()
		{			
			// call models
			$model = $this -> model;

			$where = '';
			$list = $this->model->get_records('published = 1', 'fs_utilities');

			foreach($list as $item) {
				$item->images = $this->model->get_records('record_id = '.$item->id.'', 'fs_utilities_images');
			}
			include 'modules/'.$this->module.'/views/'.$this->view.'/default.php';
		}
		
	}
	
?>