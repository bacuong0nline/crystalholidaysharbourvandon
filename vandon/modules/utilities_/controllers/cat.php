<?php


	class ProjectControllersCat extends FSControllers
	{
		function display()
		{			
			// call models
			$model = $this -> model;

			$id = FSInput::get("cid");
			$cat = $model->get_record_by_id($id, FSTable::_('fs_project_categories', 1));

			if(!$cat) {
				setRedirect(URL_ROOT, FSText::_("Không tồn tại"));
			}
			$body = $model->set_query_body();
			$cat->child = $model->get_list($body);

			include 'modules/'.$this->module.'/views/'.$this->view.'/default.php';
		}
		
	}
	
?>