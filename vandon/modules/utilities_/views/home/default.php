<?php
global $tmpl;
$tmpl->addStylesheet('utilities', 'modules/utilities/assets/css');
$tmpl->addStylesheet('slick', "templates/default/css");

$tmpl->addScript("slick", "templates/default/js");
$tmpl->addScript("home", 'modules/utilities/assets/js');

?>

<main>
  <?php echo $tmpl->load_direct_blocks('banners', array('style' => 'style6', 'category_id_image' => 2)) ?>

  <?php foreach ($list as $key => $item) { ?>
    <div class="container content">
      <div class="d-flex align-items-center justify-content-center" style="position: relative">
        <span class="line me-2"></span>
        <h4 class="title-american-2 text-cyan text-center title-content">
          <?php echo $item->title ?>
        </h4>
        <span class="line me-2"></span>
      </div>
      <div class="description text-white">
        <?php echo $item->content ?>
      </div>
      <div class="carousel image-box" style="width: max-content">
        <?php foreach ($item->images as $val) { ?>
          <div class="item">
            <img src="<?php echo format_image($val->image) ?>" alt="<?php echo format_image($val->image) ?>">
            <p class="fw-bold mt-3 ms-4"><?php echo $val->title ?></p>
          </div>
        <?php } ?>
      </div>
      <input type="hidden" id="all-image-<?php echo $key ?>" value='<?php echo json_encode($item->images) ?>'>

    </div>
  <?php } ?>

</main>

<script>
  window.onload = function() {
    // let elements = document.getElementsByClassName('image-box');

    // let key = 0;
    // let arr = JSON.parse($("#all-image-" + key).val()).map(item => obj = {
    //   "src": item.image
    // })
    // let dynamicGallery = []
    // for (let item of elements) {
    //   dynamicGallery[key] = lightGallery(item, {
    //     thumbnail: true,
    //     animateThumb: true,
    //     // zoomFromOrigin: false,
    //     // allowMediaOverlap: true,
    //     dynamic: true,
    //     toggleThumb: true,
    //     dynamicEl: JSON.parse($("#all-image-" + key).val()).map(item => obj = {
    //       src: item.image,
    //       thumb: item.image,

    //     }),
    //     plugins: [lgThumbnail]
    //   })
    //   // item.addEventListener("click", () => {
    //   //   dynamicGallery[key].openGallery(0);
    //   // });
    //   $(item).click(function() {
    //     dynamicGallery[key].openGallery(0);
    //   })
    //   key = key + 1;

    // }
    $('.image-box').slick({
      slidesToShow: 4,
      infinite: true,

    });

  }
</script>