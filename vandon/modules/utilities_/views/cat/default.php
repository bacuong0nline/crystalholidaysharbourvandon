<?php
global $tmpl;
$tmpl->addStylesheet('project', 'modules/project/assets/css');
$tmpl->addStyleSheet("flickity", "templates/default/css");
$tmpl->addScript("flickity", "templates/default/js");
$tmpl->addScript("home", 'modules/project/assets/js');
?>

<div class="container">
  <div class="d-flex justify-content-center align-items-center flex-column title-project">
    <h6 class="text-center title-project-1"><?php echo $cat->name ?></h6>
  </div>
</div>
<div class="container">
  <div class="project-wrapper">
    <?php foreach ($cat->child as $val2) { ?>
      <?php echo $tmpl->project_item($val2, $this->model)?>
    <?php } ?>
  </div>
</div>