$(document).on('click', '.detail-btn', function() { 
  let id = $(this).attr("data-id");
  $.ajax({
    type: 'get',
    url: '/index.php?module=members&view=history&raw=1&task=ajax_load_list_order',
    // contentType: "application/json",
    dataType: "html",
    data: {id: id},
    success: function (data) {
        $("#v-pills-tabContent").html(data)
        // selectCommune(JSON.parse(data))
        return true;
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
        console.log(textStatus)
    }
  })
})

$(".back").click(function() {
  location.reload()
})

$(document).on('click', '.other-page, .next-page', function(e) {
  let page = $(this).attr("data-page")
  $.ajax({
    type: 'GET',
    dataType: 'html',
    url: `/index.php?module=members&view=history&raw=1&task=getAjaxOrder`,
    data: `page=${page}`,
    success: function (data) {
        // let res = JSON.parse(data);
      // console.log(data);
      $(".list-order").html(data)
    }
  })
});