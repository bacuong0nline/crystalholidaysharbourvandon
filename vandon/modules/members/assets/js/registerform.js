let validate = { mail: false, phone: false, password: false, confirmpassword: false }

$(document).ready(function () {

	var alert_info = $('#alert_info').val();
	alert_info1 = alert_info ? JSON.parse(alert_info) : [];

	$('#request_register').click(function () {

		var submit = '#register-form';

		if (checkFormsubmit()) {
			$(submit).submit()
		}
	})
	checkFormsubmit()



	// update hits
	// setTimeout(function () {
	//     var product_id = $('#product_id').val();
	//     $.get(root + "index.php?module=contact&view=home&task=update_hits&raw=1", {
	//         id: product_id
	//     }, function (status) {
	//     });
	// }, 3000);

});
function btnAcceptRegister() {
	let formRegisterValid = validate.mail && validate.phone && validate.password && validate.confirmpassword;
	formRegisterValid ? $('#request_register').addClass("accept-btn").removeClass("disable-btn") : $('#request_register').removeClass("accept-btn").addClass("disable-btn")
}

function checkFormsubmit() {
	$("#mail-register").keyup(function () {
		let count = $("#mail-register").val().length;
		if (!notEmpty("mail-register", alert_info1[0]) || !emailValidator("mail-register", alert_info1[1])) {
			validate.mail = false;
			btnAcceptRegister()
			return false
		} else {
			validate.mail = true;
			btnAcceptRegister()
		}
	})
	$("#phone-register").keyup(function () {
		if (!lengthRestriction("phone-register", "10", "12", alert_info1[3] + ' 10 ' + alert_info1[4] + ' 12 ' + alert_info1[5])) {
			validate.phone = false;
			btnAcceptRegister()
			return false
		} else {
			validate.phone = true;
			btnAcceptRegister()
		}

	})
	$("#password-register").keyup(function () {
		if (!lengthMin("password-register", 6, alert_info1[2])) {
			validate.password = false;
			btnAcceptRegister()
			return false
		} else {
			validate.password = true;
			btnAcceptRegister()
		}
	})
	$("#password-register-confirm").keyup(function () {
		if (!lengthMin("password-register-confirm", 6, alert_info1[2])) {
			validate.confirmpassword = false;
			btnAcceptRegister()
			return false
		}
		if ($("#password-register").val() !== $("#password-register-confirm").val()) {
			invalid("password-register-confirm", alert_info1[3]);
			validate.confirmpassword = false;
			btnAcceptRegister()
			return false
		}
		else {
			console.log("true")
				validate.confirmpassword = true;
				btnAcceptRegister()
		}
	})

	return true
}
