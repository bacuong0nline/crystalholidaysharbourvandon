
$(document).ready(function () {

	var alert_info = $('#alert_info').val();
	alert_info1 = alert_info ? JSON.parse(alert_info) : [];

	$('#request_register').click(function () {

		var submit = '#register-form';

		if (checkFormsubmitRegister()) {
			$(submit).submit()
		}
	})
  $('#register-form').bind("keypress", function(e) {
    if (e.keyCode == 13) {
      var submit = '#register-form';

      if (checkFormsubmitRegister()) {
          $(submit).submit()
      }
    }
  });


	// update hits
	// setTimeout(function () {
	//     var product_id = $('#product_id').val();
	//     $.get(root + "index.php?module=contact&view=home&task=update_hits&raw=1", {
	//         id: product_id
	//     }, function (status) {
	//     });
	// }, 3000);

});


function checkFormsubmitRegister() {
		let count = $("#mail-register").val().length;
		if (!notEmpty("mail-register", alert_info1[0])) {
			return false
		}
		if (!emailValidator("mail-register", alert_info1[1])) {
			return false
		}
		if (!lengthRestriction("phone-register", "10", "12", alert_info1[3] + ' 10 ' + alert_info1[4] + ' 12 ' + alert_info1[5])) {
			return false
		}

		if (!lengthMin("password-register", 6, alert_info1[2])) {
			return false
		}
		if (!lengthMin("password-register-confirm", 6, alert_info1[2])) {
			return false
		}
		if ($("#password-register").val() !== $("#password-register-confirm").val()) {
			invalid("password-register-confirm", alert_info1[3]);
			return false
		}
	return true
}
