let validateSignin = {email: false, phone: false, password: false}

$(document).ready(function() {
  var alert_info = $('#alert_info').val();
  alert_info1 = alert_info ? JSON.parse(alert_info) : [];

  $('#request_signin').click(function () {

    var submit = '#signin-form';

    if (checkFormsubmit()) {
        $(submit).submit()
    }
  })

  $("#mail-phone-signin").keyup(function () {
    let userInput =  $("#mail-phone-signin").val()
    let isCharacter = isNaN(userInput)
    if (isCharacter || userInput.length === 0) {
      if (!notEmpty("mail-phone-signin", alert_info1[7]) || !emailValidator("mail-phone-signin", alert_info1[1])) {
        validateSignin.mail = false;
        btnAcceptSignIn()
      }   else {
        validateSignin.phone = true;
        validateSignin.mail = true;
        btnAcceptSignIn()
      }
    } else {
      if (!lengthRestriction("mail-phone-signin", "10", "12", alert_info1[3] + ' 10 ' + alert_info1[4] + ' 12 ' + alert_info1[5])) {
        validateSignin.phone = false;
        btnAcceptSignIn()
      } else {
        validateSignin.phone = true;
        validateSignin.mail = true;
        btnAcceptSignIn()
      }
    }


  })
  $("#password-signin").keyup(function () {
      if (!lengthMin("password-signin", 6, alert_info1[6])) {
        validateSignin.password = false;
        btnAcceptSignIn()
      } else {
        validateSignin.password = true;
        btnAcceptSignIn()
      }
  })
})

function btnAcceptSignIn() {
  let formSignInValid = validateSignin.mail && validateSignin.phone && validateSignin.password;
  formSignInValid ? $('#request_signin').addClass("accept-btn").removeClass("disable-btn") :  $('#request_signin').removeClass("accept-btn").addClass("disable-btn")
}