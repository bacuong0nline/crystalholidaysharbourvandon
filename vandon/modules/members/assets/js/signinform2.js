
$(document).ready(function() {
  var alert_info = $('#alert_info').val();
  alert_info1 = alert_info ? JSON.parse(alert_info) : [];

  $('#request_signin').click(function () {

    var submit = '#signin-form';

    if (checkFormsubmitSignin()) {
        $(submit).submit()
    }
  })
  $('#signin-form').bind("keypress", function(e) {
    if (e.keyCode == 13) {
      var submit = '#signin-form';

      if (checkFormsubmitSignin()) {
          $(submit).submit()
      }
    }
  });
})

function checkFormsubmitSignin() {
  let count = $("#mail-register").val().length;
  if (!notEmpty("mail-phone-signin", alert_info1[4])) {
    return false
  }
  if (!notEmpty("password-signin", alert_info1[5])) {
    return false
  }
  
  return true
}
