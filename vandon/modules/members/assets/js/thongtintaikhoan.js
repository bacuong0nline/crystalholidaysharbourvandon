$('#changepassword').click(function () {
  $('.resetpassword').slideToggle('medium')
})
$("#signout").click(function(e) {
  e.preventDefault()
    $("#fsignout").submit()
})

$("#update-info").click(function(e) {
  e.preventDefault()
  // console.log($("#changepassword").is(":checked"));
  if ($("#changepassword").is(":checked")) {
    if (checkFormsubmit2()) {
      $("#finfor").submit()
    }
  } else {
    $("#finfor").submit()
  }
})

let alert_info = $("#alert_info").val()
alert_info1 = alert_info ? JSON.parse(alert_info) : [];

function checkFormsubmit2() {
  if (!notEmpty("foldpassword", alert_info1[0])) {
      return false;
  }
  if (!notEmpty("fnewpassword", alert_info1[1])) {
      return false;
  }
  if (!notEmpty("fconfirmpassword", alert_info1[2])) {
      return false;
  }
  if ($("#fnewpassword").val() !== $("#fconfirmpassword").val()) {
    invalid("fconfirmpassword", alert_info1[3])
    return false;
  }
  else {

  }
  // return false;
  return true;
}


//So dia chi


$(".add-address").click(function() {
  $('.so-dia-chi').hide();
  $('.add-new').show()
})

$(".edit-address-btn").click(function() {
  $('.so-dia-chi').hide();
  let id = $(this).attr('data-id');
  $.ajax({
    type: 'get',
    url: '/index.php?module=members&view=address&raw=1&task=edit_address',
    // contentType: "application/json",
    data: {id: id},
    dataType: "html",
    success: function (data) {
        $('.form-address').html(data);
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
        console.log(textStatus)
    }
  })
})



$('#fprovince').select2({
  templateSelection: function (data, container) {
    let idProvince = $(data.element).attr('data-id');
    loadDistrict(idProvince)
    return data.text;
  }
});
$('#fdistrict').select2({
  templateSelection: function (data, container) {
    let idDistrict = $(data.element).attr('data-id');
  //   console.log(idDistrict);
    // loadCommunes(idDistrict)
    return data.text;
  }
});


function loadDistrict(idProvince) {
  $.ajax({
      type: 'get',
      url: '/index.php?module=members&view=members&raw=1&task=ajax_load_district',
      // contentType: "application/json",
      dataType: "text",
      data: {idCity: idProvince},
      success: function (data) {
          // console.log(data);
          selectDistrict(JSON.parse(data))
          return true;
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
          console.log(textStatus)
      }
  });
}
// function loadCommunes(idDistrict) {
//   $.ajax({
//       type: 'get',
//       url: '/index.php?module=members&view=members&raw=1&task=ajax_load_ward',
//       // contentType: "application/json",
//       dataType: "text",
//       data: {idDistrict: idDistrict},
//       success: function (data) {
//           selectCommune(JSON.parse(data))
//           return true;
//       },
//       error: function (XMLHttpRequest, textStatus, errorThrown) {
//           console.log(textStatus)
//       }
//   });
// }

function selectDistrict(data) {
  let option = '' 
  data.forEach(item => {
      option += `<option data-id="${item.id}" value="${item.id}">${item.name}</option>`
  })
  $("#fdistrict").html(option);
}
function selectCommune(data) {
  let option = '' 
  data.forEach(item => {
      option += `<option data-id="${item.id}" value="${item.id}" >${item.name}</option>`
  })
  $("#fcommune").html(option);
}

$("#address-book-btn").click(function(e) {
  e.preventDefault()

  if (checkFormsubmitAddress()) {
    
    $("#faddressbook").submit()
  }
})

$(".delete-address-btn").click(function() {
  let selectedId = $(this).attr("data-id");
  $.ajax({
    type: 'get',
    url: '/index.php?module=members&view=address&raw=1&task=delete_address',
    // contentType: "application/json",
    dataType: "html",
    data: {id: selectedId},
    success: function (data) {
        location.reload();
        return true;
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
        console.log(textStatus)
    }
  });
})

let alert_info2 = $("#alert_info2").val()
alert_info2 = alert_info2 ? JSON.parse(alert_info2) : [];
function checkFormsubmitAddress() {

  $('label.label_error').prev().remove();
  $('label.label_error').remove();

  if (!notEmpty("ffirstname", 'Bạn chưa nhập họ')) {
      return false;
  }
  if (!notEmpty("flastname", 'Bạn chưa nhập tên')) {
    return false;
  }
  if (!notEmpty("fphone", alert_info2[1])) {
      return false;
  }
  if (!isPhone("fphone", alert_info2[2])) {
      return false;
  }
  if (!lengthRestriction("fphone", "10", "12", alert_info2[4] + ' 10 ' + alert_info2[5] + ' 12 ' + alert_info2[6])) {
    return false;
  }
  if (!notEmpty("faddress", alert_info2[3])) {
    return false;
  }

  else {

  }
  // return false;
  return true;
}