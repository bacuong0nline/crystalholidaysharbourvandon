<?php
/**
 * @author ndson
 * @category controller
 */
class MembersControllersGoogle extends FSControllers{
    function __construct(){
    	$model = $this -> model;
        parent::__construct();
	}
    
    function google_login(){ 
    	$model = $this -> model;
        $strHTML = '';
        require(PATH_BASE.'libraries'.DS.'google-api-php'.DS.'config.php');
        //$redirect_uri = URL_ROOT.'oauth2callback1';
        $redirect_uri = URL_ROOT.'index.php?module=members&view=google&raw=1&task=google_login';
        $client_id = '937899157417-akcodh7j66csa6r3b52k4n39biu6ejg5.apps.googleusercontent.com';
        $client_secret = 'JnyFQD5q7m9b13HyAe49i9Ei';
        $client = new Google_Client();
        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);
        $client->setRedirectUri($redirect_uri);
        $client->setScopes('email');
        // print_r($client);

        if (isset($_GET['code'])) {
            $client->authenticate($_GET['code']);
            $_SESSION['access_token']  = $client->getAccessToken();
            $access_token = json_decode($_SESSION['access_token']);
            //$client->refreshToken($access_token->refresh_token);
            $token_url = 'https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token='.$access_token->access_token;
            $token_data = file_get_contents($token_url);
            $guser = json_decode($token_data);
			// var_dump($guser);die;
			
            if (!empty($guser)){
                 $data = $model->check_exits_email($guser->email);
				if ($data){
                    $persistent ='on';  
                    // if ($persistent == 'on') {
                        /* Set cookie to last 1 month */
                        // $time =time()+60*60*24*30;
                        // print_r($data);die;
                    //     setcookie('id',$data->id,$time,'/');
                    //     setcookie('name',$data->name,$time,'/');
                    //     setcookie('user_id',$data->id,$time,'/');
                    //     setcookie('username',$data->username,$time,'/');
                    //     setcookie('user_email',$data->email,$time,'/');
                    //     setcookie('avatar',$data->avatar,$time,'/');
                    //     setcookie('persistent',1,$time,'/');
                    // } else {
                    //     /* Cookie expires when browser closes */
                    //     setcookie('id',$data->id,false,'/');
                    //     setcookie('full_name',$data->name,false,'/');
                    //     setcookie('user_id',$data->id,false,'/');
                    //     setcookie('username',$data->username,false,'/');
                    //     setcookie('user_email',$data->email,false,'/');
                    //     setcookie('avatar',$data->avatar,false,'/');
                    //     setcookie('persistent',0,false,'/');
                    // }

				    $model->loginMailOnly($guser->email);
					$return['error'] 	= true;
					$return['url']		=  URL_ROOT;                           
                    $return['msg']		=  "Bạn đã đăng nhập thành công";
					// $strHTML  = '<script type="text/javascript">';
					// $strHTML .= '	window.opener.login_facebook('.json_encode($return).');';
					// $strHTML .= '	window.close();';
					// $strHTML .= '</script>';

                    $link = FSRoute::_('index.php?module=members&view=info');
				//    $link = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                    // $link = URL_ROOT;
                    $msg = FSText::_("Bạn đã đăng nhập thành công");
                    setRedirect($link, $msg);

				}else{
                    $row['email']       = $guser->email;
                    $username = explode( '@', $guser->email);
                    $row['username']    = $username[0];
                    $row['name']   = $guser->name;
                    $row['sex']         = $guser->gender ;
                    $row['avatar']      = $guser->picture ;
                    $fstring = FSFactory::getClass('FSString', '', '../');
					$row['activated_code'] =  $fstring->generateRandomString(32);
                    $row['type']        = 'google';
                    $row['is_type']     = '1';
                    $row['published']   = '1';
                    $row['block']   = '0';
                    $row['created_time'] = date("Y-m-d H:i:s");
						
					$id = $model->insertUser($row);
					// var_dump($id);die;
					if($id){
						$member = $this->model->get_record_by_id($id, 'fs_members');
						
						//var_dump($member);die;
                        $persistent ='on';  
                        if ($persistent == 'on') {
							
							
                            /* Set cookie to last 1 month */
                            $time =time()+60*60*24*30;

                            setcookie('id',$member->id,$time,'/');
                            setcookie('full_name',$member->name,$time,'/');
                            setcookie('user_id',$id,$time,'/');
                            setcookie('username',$member->username,$time,'/');
                            setcookie('user_email',$member->email,$time,'/');
                            setcookie('avatar',$member->avatar,$time,'/');
                            setcookie('persistent',1,$time,'/');
                        } else {
                            /* Cookie expires when browser closes */
                            setcookie('id',$member->id,false,'/');
                            setcookie('full_name',$member->name,false,'/');
                            setcookie('user_id',$id,false,'/');
                            setcookie('username',$member->username,false,'/');
                            setcookie('user_email',$member->email,false,'/');
                            setcookie('avatar',$member->avatar,false,'/');
                            setcookie('persistent',0,false,'/');
                        }
							 
							 
                        //$model->loginMailOnly($guser->email);
						// $return['error'] 	= true;
						// $return['url']		=  URL_ROOT;
						// $return['msg']		=  "Lưu Thành viên thành công";
						// $strHTML  = '<script type="text/javascript">';
						// $strHTML .= '	window.opener.login_facebook('.json_encode($return).');';
						// $strHTML .= '	window.close();';
						// $strHTML .= '</script>';
                        
					}//end: if($id)

                    $link = FSRoute::_('index.php?module=members&view=info');
					//$link = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                    // $link = URL_ROOT;
                    $msg = FSText::_("Bạn đã đăng nhập thành công");
                    setRedirect($link, $msg);

				}//end: if ($data)
            }else{
                unset($_SESSION['access_token']);
            }//end: if (!empty($guser))
        }//end: if (isset($_GET['code']))
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            /* $client->setAccessToken($_SESSION['access_token']);
            unset($_SESSION['access_token']);
            $this->google_login(); */
            unset($_SESSION['access_token']);
        } else {
            $authUrl = $client->createAuthUrl();
            $strHTML  = '<script type="text/javascript">';
			$strHTML .= '	top.location.href="'.$authUrl.'"';
            $strHTML .= '</script>';
        }
        echo $strHTML;
    }
    
} 
?>