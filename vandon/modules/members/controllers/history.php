<?php
	class MembersControllersHistory extends FSControllers
	{
		function display()
		{			
			$model = $this -> model;
			global $user;
      //global $tags_group;
			$fssecurity = FSFactory::getClass('fssecurity');
			if(!$user->is_loaded()) {
				$url = FSRoute::_("index.php?module=home&view=home");
				$msg = FSText :: _("Bạn phải đăng nhập để sử dụng tính năng này");
				setRedirect($url,$msg,'error');
			}
      //$categories = $model->get_records(' published = 1 ','fs_news_categories','id,name,alias');
			$breadcrumbs = array();
			$breadcrumbs[] = array(0 =>FSText::_('Thành viên'), 1 => '');

			global $tmpl, $user;
			$tmpl -> set_seo_special();
			// $tmpl -> assign('breadcrumbs', $breadcrumbs);

			$userInfo = $user->userInfo;
	
			$uid = $user->userID;
			$name = $userInfo->name;
			$first_name = $userInfo->first_name;
			$last_name = $userInfo->last_name;
			$email = $userInfo->email;
			if (!empty($userInfo->avatar)) {
				$avatar = $userInfo->avatar;
			}

			
			$array_status = array(
				11 => FSText::_('Đặt hàng'),
				4 => FSText::_('Đã giao hàng'),
				5 => FSText::_('Hủy đơn hàng'),
			);
		
			$listOrder = $model->getListOrder($uid);
			foreach($listOrder as $item) {
				$item->created_time = $this->format_date($item->created_time);
				$data = $model->get_data_order($item->id);
				$item->product_name = $data;
				switch($item->status) {
					case 11:
						$item->status = $array_status[11];
					break;
					case 4:
						$item->status = $array_status[4];
					break;
					case 11:
						$item->status = $array_status[5];
					break;
				}
			}
			// print_r($listOrder);die;
			// call views
			$query_body = $this->model->set_query_body($uid);
			$total = $this->model->getTotal($query_body);
	
			$pagination = $this->model->getAjaxPagination($total);
			// print_r($pagination);die;
			include 'modules/'.$this->module.'/views/'.$this->view.'/default.php';
		}
		function format_date($str_time){
			$time = strtotime($str_time);
			$hour = date('H',$time);
			$minute = date('i',$time);
			$date = date('d/m/Y',$time);
			return $date;
		 }
		 function ajax_load_list_order() {
				$id = FSInput::get("id");
				$order = $this->model->get_record_by_id($id, 'fs_order');
				$detail = $this->model->get_data_order($id);
				$product_detail = $this->model->get_data_order($id);
				switch($order->ord_payment_type) {
					case 1:
						$order->ord_payment_type =  "Thanh toán khi nhận hàng COD";
					break;
					case 2:
						$order->ord_payment_type =  "Chuyển khoản ngân hàng";
					break;
				}
				$array_status = array(
					11 => FSText::_('Đặt hàng'),
					4 => FSText::_('Đã giao hàng'),
					5 => FSText::_('Hủy đơn hàng'),
				);
				switch($order->status) {
					case 11:
						$order->status = $array_status[11];
					break;
					case 4:
						$order->status = $array_status[4];
					break;
					case 11:
						$order->status = $array_status[5];
					break;
				}
				$order->created_time = $this->format_date($order->created_time);
				$total = 0;
				foreach($product_detail as $item) {
					$total += $item->count*$item->price_has_options;
				}
				if ($order->vat == 1) {
					$total_vat = $total + $total*0.1;
				}
				// print_r($product_detail);die;
			 	include 'modules/'.$this->module.'/views/'.$this->view.'/detail.php';
			}
			function getAjaxOrder(){
				$page = FSInput::get("page");
				$uid = $_COOKIE['user_id'];

				$query_body = $this->model->set_query_body($uid);
				$total = $this->model->getTotal($query_body);
				$pagination = $this->model->getAjaxPagination($total, $page);
				$array_status = array(
					11 => FSText::_('Đặt hàng'),
					4 => FSText::_('Đã giao hàng'),
					5 => FSText::_('Hủy đơn hàng'),
				);
				$listOrder = $this->model->getListOrder($uid, $this->model->limit, $page);
				foreach($listOrder as $item) {
					$item->created_time = $this->format_date($item->created_time);
					$data = $this->model->get_data_order($item->id);
					$item->product_name = $data;
					switch($item->status) {
						case 11:
							$item->status = $array_status[11];
						break;
						case 4:
							$item->status = $array_status[4];
						break;
						case 11:
							$item->status = $array_status[5];
						break;
					}
				}
				include 'modules/' . $this->module . '/views/' . $this->view . '/product_list.php';

			}
  }