<?php
class MembersControllersAddress extends FSControllers
{
	function display()
	{
		$model = $this->model;

		global $user;

		//$categories = $model->get_records(' published = 1 ','fs_news_categories','id,name,alias');
		$breadcrumbs = array();
		$breadcrumbs[] = array(0 => FSText::_('Thành viên'), 1 => '');

		global $tmpl, $user;
		$tmpl->set_seo_special();
		// $tmpl->assign('breadcrumbs', $breadcrumbs);


		$userInfo = $user->userInfo;
		$id = $user->userID;
		$first_name = $userInfo->first_name;
		$last_name = $userInfo->last_name;
		// $name = $userInfo->name;
		$email = $userInfo->email;
		if (!empty($userInfo->avatar)) {
			$avatar = $userInfo->avatar;
		}

		$fssecurity = FSFactory::getClass('fssecurity');
		if(!$user->is_loaded()) {
			$url = FSRoute::_("index.php?module=home&view=home");
			$msg = FSText :: _("Bạn phải đăng nhập để sử dụng tính năng này");
			setRedirect($url,$msg,'error');
		}

		$model = $this->model;
		$mba = $model->getAllMemberAddress();

		foreach($mba as $item) {
			$item->province = $model->get_record_by_id($item->province_id, 'fs_cities')->name;
			$item->district = $model->get_record_by_id($item->district_id, 'fs_districts')->name;
			// $item->ward = $model->get_record_by_id($item->ward_id, 'fs_wards')->name;
		}
		$province = $model->getProvince();

		//var_dump($mba);
		// $data = $model->getMember();
		//$data_default = $model->getMemberAddress();
		// $cities = $model->getProvince($data_default->province_id);
		// $config_person_edit = $model->getConfig('person_edit');
		// call views			
		include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
	}

	function address_book() {
		global $user;
		$row = array();
		$row['first_name'] = FSInput::get('ffirstname');
		$row['last_name'] = FSInput::get('flastname');
		$row['province_id'] = FSInput::get('fprovince');
		$row['district_id'] = FSInput::get('fdistrict');
		// $row['ward_id'] = FSInput::get('fwards');
		$row['telephone'] = FSInput::get('fphone');
		$row['content'] = FSInput::get('faddress');
		$row['default'] = 0;
		if (FSInput::get('fdefault') == 'on') {
			$row['default'] = 1;
		}
		$row['member_id'] = $user->userID;
		$row['username'] = $user->userInfo->username;

		if ($row['default'] == 1) {
			$mba = $this->model->getAllMemberAddress();
			foreach($mba as $item) {
				$this->model->_update(array('default' => 0), 'fs_members_address', 'member_id= '.$user->userID);
			}
		}
		// var_dump($row);die;
		$id = $this->model->_add($row, 'fs_members_address');
		$url  = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		setRedirect($url, 'Cập nhật thành công');
	}
	function edit_address_book() {
		global $user;

		$row = array();
		$id_members_add = FSInput::get('id');
		$row['first_name'] = FSInput::get('ffirstname');
		$row['last_name'] = FSInput::get('flastname');
		$row['province_id'] = FSInput::get('fprovince');
		$row['district_id'] = FSInput::get('fdistrict');
		$row['telephone'] = FSInput::get('fphone');
		$row['address'] = FSInput::get('faddress');
		$row['default'] = 0;
		if (FSInput::get('fdefault') == 'on') {
			$row['default'] = 1;
		}
		$row['member_id'] = $user->userID;
		$row['username'] = $user->userInfo->username;

		if ($row['default'] == 1) {
			$mba = $this->model->getAllMemberAddress();
			foreach($mba as $item) {
				$this->model->_update(array('default' => 0), 'fs_members_address', 'member_id= '.$row['member_id']);
			}
		}
		$id = $this->model->_update($row, 'fs_members_address', 'id= '.$id_members_add);

		$url  = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		setRedirect($url, 'Cập nhật thành công');
	}
	function edit_address(){
		$model = $this->model;
		// $data = $model->getMember();
		//var_dump($data);
		$id_members = FSInput::get('id');
		$info_address = $model->get_record_by_id($id_members, 'fs_members_address');
		// print_r($info_address);die;
		// $data_address = $model->getAddress($info_address->province_id);
		$province = $model->getProvince();
		$district = $model->getDistrict($info_address->province_id);
		// $wards = $model->getWard($info_address->district_id);
		include 'modules/' . $this->module . '/views/' . $this->view . '/edit_address.php';
	}
	function delete_address() {
		$id=FSInput::get('id');
		$this->model->_remove('id='.$id,'fs_members_address');
	}
}
