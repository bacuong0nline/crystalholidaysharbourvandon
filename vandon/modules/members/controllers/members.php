<?php
class MembersControllersMembers extends FSControllers
{
	function display()
	{
		// call models
		$model = $this->model;
		//
		//			global $tags_group;
		//
		//			$categories = $model->get_records(' published = 1 ','fs_news_categories','id,name,alias');
		$breadcrumbs = array();
		$breadcrumbs[] = array(0 => FSText::_('Thành viên'), 1 => '');

		global $tmpl;
		$tmpl->set_seo_special();
		// $tmpl->assign('breadcrumbs', $breadcrumbs);

		// call views			
		include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
	}
	function check_register_save()
	{
		$model = $this->model;
		if (!$model->check_exits_email()) {
			return false;
		}
		if (!$model->check_exits_phone()) {
			return false;
		}
		return true;
	}
	function register_save()
	{
		$model = $this->model;
		if (!$this->check_register_save()) {
			return;
		}
		$id = $model->save();

		$user = $model->get_record_by_id($id, 'fs_members', '*');

		if ($id) {
			$url = URL_ROOT;
			//$link = FSRoute::_("index.php?module=users&view=users&task=user_info");
			$link = $url;

			$msg = "Bạn đã đăng ký tài khoản thành công! Vui lòng đăng nhập vào email để xác thực";

			$this->registerNewsletter($user->activated_code, $user->email, $user->username, $id);

			setRedirect($link, $msg);
		} else {
			$link = URL_ROOT;
			$msg = FSText::_("Xin lỗi. Bạn chưa đăng ký thành công.");
			setRedirect($link, $msg, 'error');
		}
	}
	function login_save()
	{
		global $user;
		$model = $this->model;
		$return = FSInput::get('return');

		if ($return) {
			$link = base64_decode($return);
		} else
			$link = FSRoute::_("index.php?module=members&view=info");

		$user_info = FSInput::get('log_username');
		$password = FSInput::get('log_password');

		$rs = $user->login($user_info, $password);
		if ($rs){
			setRedirect(URL_ROOT . $_SESSION['current_uri'], 'Đăng nhập thành công!', 'success');

		}
		else
			setRedirect(URL_ROOT, 'Tên đăng nhập hoặc mật khẩu không đúng!', 'error');
	}
	function forget()
	{
		if (isset($_COOKIE['username'])) {
			if ($_COOKIE['username']) {
				$link = FSRoute::_(URL_ROOT);
				setRedirect($link);
			}
		}
		$model = $this->model;
		$config_person_forget = $model->getConfig('person_forget');

		//breadcrumbs
		$breadcrumbs = array();
		$breadcrumbs[] = array(0 => 'Quên mật khẩu', 1 => '');
		global $tmpl;
		$tmpl->assign('breadcrumbs', $breadcrumbs);

		include 'modules/' . $this->module . '/views/' . $this->view . '/forget.php';
	}

	function user_info()
	{
		$fssecurity = FSFactory::getClass('fssecurity');
		$fssecurity->checkLogin();
		$model = $this->model;
		$data = $model->getMember();
		$breadcrumbs = array();
		$breadcrumbs[] = array(0 => 'Thông tin tài khoản', 1 => '');
		global $tmpl;
		$tmpl->assign('breadcrumbs', $breadcrumbs);

		include 'modules/' . $this->module . '/views/' . $this->view . '/user_info.php';
	}

	function registerNewsletter($activated_code, $email, $username, $id)
	{
		require(PATH_BASE . 'libraries/PHPMailer_v5.1/class.phpmailer.php');

		$global_class = FSFactory::getClass('FsGlobal');

		$admin_name = $global_class->getConfig('admin_name');
		$mail_register = $global_class->getConfig('mail_register');

		$nFrom = $admin_name;
		$mFrom = 'mail.finalstyle.noreply@gmail.com';
		$mPass = 'fs123456!@#$%^';
		$mTo = $email;
		$link_account = FSRoute::_('index.php?module=members&task=activated_account') . '&activated_code=' . $activated_code . '';

		$mail = new PHPMailer();

		$mail->Username   = $mFrom;
		$mail->Password   = $mPass;


		// print_r($mail);die;
		$mail->IsSMTP();
		$mail->CharSet  = "utf-8";
		$mail->SMTPDebug  = 0;   // enables SMTP debug information (for testing)
		$mail->SMTPAuth   = true;    // enable SMTP authentication
		$mail->SMTPSecure = "ssl";   // sets the prefix to the servier
		$mail->Host       = "smtp.gmail.com";    // sever gui mail.
		$mail->Port       = 465;         // cong gui mail de nguyen

		$mail->SetFrom($mFrom, $nFrom);
		// $mail->Subject('Khôi phục mật khẩu - '.$config['site_name']);
		$mail->Subject    = 'Kích hoạt tài khoản'; // tieu de email
		$body = '';
		$body .= $mail_register;
		$body = str_replace('{link}', $link_account, $body);

		$mail->MsgHTML($body); // noi dung chinh cua mail se nam o day.
		$mail->AddAddress($mTo, '');


		if (!$mail->Send()) {
			return false;
		}
		return true;
	}

	function activated_account()
	{
		$row = $this->model->activated();
		$url = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

		$link = $url;
		$link1 = URL_ROOT;
		if ($row) {
			$msg = "Kích hoạt tài khoản thành công, mời bạn đăng nhập!";
			setRedirect($link1, $msg);
		}
	}
	function ajax_load_province()
	{
		$list_province = $this->model->getProvince();
		echo json_encode($list_province);
	}
	function ajax_load_district()
	{
		$idProvince = FSInput::get("idCity");
		$list_district = $this->model->getDistrict($idProvince);
		echo json_encode($list_district);
	}
	function ajax_load_ward()
	{
		$idDistrict = FSInput::get("idDistrict");
		$list_commune = $this->model->getWard($idDistrict);
		echo json_encode($list_commune);
	}
	function logout() {
		global $user;
		$user->logout(URL_ROOT);
	}
}
