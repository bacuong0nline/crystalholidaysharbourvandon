<?php

class MembersControllersFacebook extends FSControllers
{

    function __construct(){
    	$model = $this -> model;
        parent::__construct();
	}
    function face_login(){
        global $user;
        $app_id = "301489711868004";
        $app_secret = "62495ffcad88492c53dc66e95ae2fea4";
        $permission = "email,public_profile";
        $my_url = FSRoute::_('index.php?module=members&view=facebook&task=face_login');
        $strHTML = '';
        $code = isset($_REQUEST['code'])?$_REQUEST['code']:'';
        if(empty($code)) {
            $strHTML = '';
            $_SESSION['state'] = md5(uniqid(rand(), TRUE));
            $dialog_url = "http://www.facebook.com/dialog/oauth?client_id=".$app_id."&redirect_uri=".urlencode($my_url) ."&scope=".$permission."&state=" . $_SESSION['state'];
            // $strHTML  = '<script type="text/javascript">';
            // $strHTML .= '	top.location.href="'.$dialog_url.'"';
            // $strHTML .= '</script>';
        }
        $S_State =isset($_SESSION['state'])?$_SESSION['state']:'';
        $R_State = isset($_REQUEST['state'])?$_REQUEST['state']:'';
        if( $S_State && ($S_State == $R_State)) {
            $token_url = 'https://graph.facebook.com/oauth/access_token?client_id='.$app_id.'&redirect_uri='.urlencode($my_url).'&client_secret='.$app_secret.'&code='.$code;
            $response = file_get_contents($token_url);
            $response = json_decode($response);
            $params = null;
            $_SESSION['access_token'] = $response->access_token;
            $graph_url = "https://graph.facebook.com/me?fields=email,id,name,gender,picture&access_token=" . $response->access_token;
            $data = file_get_contents($graph_url);
            $fuser = json_decode($data);
            if (!empty($fuser)){
                $data = $user->checkExitsFacebookID($fuser->id);
                if ($data){
                    $user->loginFacebookID($fuser->id);
                    $return['error'] 	= true;
                    $return['url']		=  URL_ROOT;
                    $return['msg']		=  "Bạn đã đăng nhập thành công";
                    // $strHTML  = '<script type="text/javascript">';
                    // $strHTML .= '	self.opener.location.reload();';
                    // $strHTML .= '	window.close();';
                    // $strHTML .= '</script>';
                    $link = FSRoute::_('index.php?module=members&view=info');
                    $msg = FSText::_("Bạn đã đăng nhập thành công");
                    setRedirect($link, $msg);

                }else{
                    $row['name'] = $fuser->name;
                    $row['email'] = $fuser->email;
                    $row['facebook_id'] = $fuser->id;
                    $row['password'] = '123456';
                    $row['type'] = 'facebook';
                    $row['avatar'] = 'http://graph.facebook.com/'.$fuser->id.'/picture?type=large';
                    $id = $user->insertUser($row);
                    if($id){
                        $user->updateUser(array('code' => 'CVN'.str_pad($id, 6, "0", STR_PAD_LEFT)), $id);
                        $user->loginFacebookID($fuser->id);
                        $return['error'] 	= true;
                        $return['url']		=  URL_ROOT;
                        $return['msg']		=  "Lưu Thành viên thành công";
                        // $strHTML  = '<script type="text/javascript">';
                        // $strHTML .= '	self.opener.location.reload();';
                        // $strHTML .= '	window.close();';
                        // $strHTML .= '</script>';
                        $link = FSRoute::_('index.php?module=members&view=info');
                        $msg = FSText::_("Bạn đã đăng nhập thành công");
                        setRedirect($link, $msg);

                    }
                }
            }else {
                $strHTML = '';
                $_SESSION['state'] = md5(uniqid(rand(), TRUE)); // CSRF protection
                $dialog_url = "http://www.facebook.com/dialog/oauth?client_id=".$app_id."&redirect_uri=".urlencode($my_url) ."&scope=".$permission."&state=" . $_SESSION['state'];
                $strHTML  = '<script type="text/javascript">';
                $strHTML .= 'top.location.href="'.$dialog_url.'"';
                $strHTML .= '</script>';
            }
        }else {
            $strHTML = '';
            $_SESSION['state'] = md5(uniqid(rand(), TRUE)); // CSRF protection
            $dialog_url = "http://www.facebook.com/dialog/oauth?client_id=".$app_id."&redirect_uri=".urlencode($my_url) ."&scope=".$permission."&state=" . $_SESSION['state'];
            $strHTML  .= '<script type="text/javascript">';
            $strHTML .= 'top.location.href="'.$dialog_url.'"';
            $strHTML .= '</script>';
        }
        echo $strHTML;
    }
}
