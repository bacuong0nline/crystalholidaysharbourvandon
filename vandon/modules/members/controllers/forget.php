<?php
class MembersControllersForget extends FSControllers
{
	function display()
	{
		$model = $this->model;

		//global $tags_group;

		//$categories = $model->get_records(' published = 1 ','fs_news_categories','id,name,alias');
		$breadcrumbs = array();
		$breadcrumbs[] = array(0 => FSText::_('Quên mật khẩu'), 1 => '');

		global $tmpl;
		$tmpl->set_seo_special();
		$tmpl->assign('breadcrumbs', $breadcrumbs);

		$model = $this->model;

		include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
  }

  function forget_save()
  {
      if(!$this->check_captcha())
    {
      $msg = "Mã hiển thị không đúng";
      setRedirect("index.php?module=users&task=forget&Itemid=38",$msg,'error');
    }

    $model = $this -> model;

    $user = $model->forget();
    // print_r($user);die;
    if(@$user->email)
    {
      $resetPass = $model->resetPass($user->id);
      $link = FSRoute::_('index.php?module=members&view=forget&task=forget');

      if(!$resetPass)
      {
        $msg = "Lỗi hệ thống khi reset Password";
        setRedirect($link,$msg,'error');
      }
        
      if(!$this -> sendMailForget($user,$resetPass))
      {
        $msg = "Lỗi hệ thống khi send mail";
        setRedirect($link,$msg,'error');
      }
      
      $msg = "Mật khẩu của bạn đã được thay đổi. Vui lòng kiểm tra email của bạn";
      setRedirect(URL_ROOT,$msg);
    }
    else{
              $link = FSRoute::_('index.php?module=users&view=users&task=forget');
      $msg = "Email của bạn không tồn tại trong hệ thống. Vui lòng kiểm tra lại!";
      setRedirect($link,$msg,'error');
    }
  }
  function sendMailForget($user,$resetPass)
  {
      require(PATH_BASE . 'libraries/PHPMailer_v5.1/class.phpmailer.php');

      $global_class = FSFactory::getClass('FsGlobal');

      $admin_name = $global_class->getConfig('admin_name');
      
      $nFrom = $admin_name;
      $mFrom = 'mail.finalstyle.noreply@gmail.com';
      $mPass = 'fs123456!@#$%^';
      $mTo = $user->email;

      $mail = new PHPMailer();

      $mail->Username   = $mFrom;
      $mail->Password   = $mPass;


      // print_r($mail);die;
      $mail->IsSMTP();
      $mail->CharSet  = "utf-8";
      $mail->SMTPDebug  = 0;   // enables SMTP debug information (for testing)
      $mail->SMTPAuth   = true;    // enable SMTP authentication
      $mail->SMTPSecure = "ssl";   // sets the prefix to the servier
      $mail->Host       = "smtp.gmail.com";    // sever gui mail.
      $mail->Port       = 465;         // cong gui mail de nguyen

      $mail->SetFrom($mFrom, $nFrom);
      // $mail->Subject('Khôi phục mật khẩu - '.$config['site_name']);
      $mail->Subject    = 'Khôi phục mật khẩu'; // tieu de email
      $body = '';
      $body .= '<div>Chào bạn!</div>';
      $body .= '<div>Bạn đã sử dụng chức năng lấy lại mật khẩu</div>';
      $body .= '<div>Mật khẩu đăng nhập mới của bạn là: <strong>'.$resetPass.'</strong> (Vui lòng thay đổi mật khẩu ngay sau khi đăng nhập).</div>';
      $body .= '<div>Ch&acirc;n th&agrave;nh c&#7843;m &#417;n!</div>';

      $mail->MsgHTML($body); // noi dung chinh cua mail se nam o day.
      $mail->AddAddress($mTo, '');


      if(!$mail ->Send()){
        return false;
      }
      return true;
  }

}
