<?php
class MembersControllersInfo extends FSControllers
{
	function display()
	{
		global $user;
		$model = $this->model;

		//global $tags_group;
		$fssecurity = FSFactory::getClass('fssecurity');
		if(!$user->is_loaded()) {
			$url = FSRoute::_("index.php?module=home&view=home");
			$msg = FSText :: _("Bạn phải đăng nhập để sử dụng tính năng này");
			setRedirect($url,$msg,'error');
		}
		//$categories = $model->get_records(' published = 1 ','fs_news_categories','id,name,alias');
		$breadcrumbs = array();
		$breadcrumbs[] = array(0 => FSText::_('Thành viên'), 1 => '');

		global $tmpl, $user;

		$userInfo = $user->userInfo;
		$tmpl->set_seo_special();
		// $tmpl->assign('breadcrumbs', $breadcrumbs);
		$id = $user->userID;
		$first_name = $userInfo->first_name;
		$last_name = $userInfo->last_name;
		$name = $userInfo->name;
		$email = $userInfo->email;
		if (!empty($userInfo->avatar)) {
			$avatar = $userInfo->avatar;
		}

		$info = $userInfo;
		// call views			
		include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
	}


	function updateInfo()
	{
		global $user;
		$row = array();
		$row['sex'] = FSInput::get('fgender');
		// $id = $_COOKIE["id"];
		$old_pass = FSInput::get('oldpassword');
		$new_pass = FSInput::get('newpassword');
		$confirm_pass = FSInput::get('confirmpassword');

		if (FSInput::get('fbirthday')) {
			$row['birthday'] = FSInput::get('fbirthday');
		}
		if (FSInput::get('fphone')) {
			$row['telephone'] = FSInput::get('fphone');
		}
		if (FSInput::get('ffirstname')) {
			$row['first_name'] = FSInput::get('ffirstname');
		}
		if (FSInput::get('flastname')) {
			$row['last_name'] = FSInput::get('flastname');
		}
		if (FSInput::get('newpassword')) {
			$row['password'] = $new_pass;
		}
		$rs = $user->updateUser($row, $user->userID);

		if ($rs) {
			$url  = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			setRedirect($url, 'Cập nhật thành công');
		}
		// if ($_COOKIE["persistent"]) {
		// 	$url  = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		// 	if ($old_pass=='' && $new_pass=='' && $confirm_pass=='') {
		// 		$exist_phone = $this->model->check_exist($row['telephone'], '', 'telephone', 'fs_members');
		// 		$phone_user = $this->model->get_record_by_id($id, 'fs_members')->telephone;

		// 		if ($exist_phone && $row['telephone'] != $phone_user){
		// 			setRedirect($url, 'Không thể lưu được. Số điện thoại đã được sử dụng!', 'error');
		// 		} else {
		// 			$this->model->_update($row, 'fs_members', ' id='.$id);
		// 		}
		// 	} else {
		// 		if ($this->model->check_exist(md5($old_pass), '', 'password', 'fs_members')){
		// 			$this->model->_update($row, 'fs_members', ' id='.$id);
		// 		}
		// 	}
		// 	setRedirect($url, 'Cập nhật thành công');

		// }
	}
}
