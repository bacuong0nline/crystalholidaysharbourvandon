<div class="detail-order" id="detail-order">
  <div class="row no-gutters">
    <a href="" class="d-flex align-items-center back" style="width: 20px; height: 30px">
      <i class="fa fa-chevron-left" aria-hidden="true"></i>
    </a>
    <div class="col" style="min-width: 730px">
      <p class="text--title"><?php echo FSText::_("Chi tiết đơn hàng") ?> <span style="color: #81954E; font-weight: 500"><?php echo 'DH' . str_pad($order->id, 8, "0", STR_PAD_LEFT) ?></span> - <span class="bold"><?php echo $order->status ?></span></p>
    </div>
    <div class="col"><?php echo FSText::_("Ngày đặt:") ?> <?php echo $order->created_time ?></div>
  </div>
  <div class="row no-gutters">
    <div class="col-md-4 no-gutters block pr-15px">
      <h4><?php echo FSText::_("Địa chỉ người nhận") ?></h4>
      <div class="block__div p-3">
        <p class="text-capitalize bold"><?php echo $order->name ?></p>
        <p><?php echo FSText::_("Địa chỉ:") ?> <?php echo $order->address . ', ' . $order->district . ', ' . $order->province ?></p>
        <p><?php echo FSText::_("Điện thoại:") ?> <?php echo $order->telephone ?></p>
      </div>
    </div>
    <div class="col-md-4 no-gutters block pr-15px">
      <h4><?php echo FSText::_("Hình thức giao hàng") ?></h4>
      <div class="block__div p-3">
        <?php
        if ($order->shipping == 1)
          echo 'Giao hàng';
        else if ($order->shipping == 2)
          echo "Khách hàng đến nhận hàng";
        ?>
      </div>
    </div>
    <div class="col-md-4 no-gutters block">
      <h4><?php echo FSText::_("Trạng thái đơn hàng") ?></h4>
      <div class="block__div p-3">
        <p class="bold"><?php echo $order->ord_payment_type ?></p>
        <p class="red--text"><?php echo $order->status ?></p>
      </div>
    </div>
    <div class="col no-gutters">
      <table class="table-detail">
        <thead>
          <tr>
            <td scope="col"><?php echo FSText::_("Sản phẩm") ?></td>
            <td scope="col"><?php echo FSText::_("Tên sản phẩm") ?></td>
            <td scope="col"><?php echo FSText::_("Giá") ?></td>
            <td scope="col"><?php echo FSText::_("Số lượng") ?></td>
            <!-- <td scope="col"><?php echo FSText::_("Tạm tính") ?></td> -->
          </tr>
        </thead>
        <tbody>
          <?php 
            //foreach ($product_detail as $item) {
            // $option_str = '';
            // $options = explode(',', $item->options);
            // $options = $this->cleanArray($options);
            // if (!empty($options))
            //   foreach ($options as $val) {
            //     $option_str .= ' - ';
            //     $rs = $this->model->get_record_by_id($val, 'fs_products_values');
            //     $option_str .= $this->model->get_record_by_id($rs->parent_id, 'fs_products_options')->name;
            //     $option_str .= ': ';
            //     $option_str .= $rs ? $rs->name . ' ' . format_money($rs->price) : 'Mặc định';
            //   }
          ?>
				<?php
        $total_money = 0;
        $total_discount = 0;
        for ($i = 0; $i < count($product_detail); $i++) {

				$item = $product_detail[$i];
				$link_view_product = FSRoute::_('index.php?module=products&view=product&code=' . $item->product_alias . '&id=' . $item->product_id . '&ccode=' . $item->category_alias . '&Itemid=6');

				$total_price_options = 0;
				$total_money += $item->price * $item->count;

				foreach (unserialize($item->options) as $item2) {
					if (!is_array(@$item2[0])) {
					  $total_price_options += $item2['price'] * $item2['quantity'];
					} else {
					  foreach ($item2 as $val) {
						$total_price_options += $val['price'] * $val['quantity'];
					  }
					}
				}
				$total_money += $total_price_options;
				$total_discount += $item->discount * $item->count;

				$options_display = '';

				$options_array = @unserialize($item->options);
				if (!empty($options_array)) {
					foreach ($options_array as $key => $val) {
						if (empty($val['id'])) {
							foreach ($val as $val2) {
								$price = @$val2['price'] != "0.00" ? format_money_0(@$val2['quantity'] * @$val2['price']) : null;
								$quantity_option = @$val2['quantity'] > 1 ? 'x ' . @$val2['quantity'] . '' : '';
								$options_display .= '<p class="mb-0" style="font-size: 12px; color: #6e6e6e; margin-bottom: 0px">' . @$val2['name_display'] . ' ' . $quantity_option . ' ' . $price . ' </p>';
								$_SESSION['price_options_value'] += $val2['price'] * @$val2['quantity'];
							};
						} else {
							$price = @$val['price'] != "0.00" ? format_money_0(@$val['quantity'] * @$val['price']) : null;
							$quantity_option = @$val['quantity'] > 1 ? 'x ' . @$val['quantity'] . '' : '';
							$options_display .= '<p class="mb-0" style="font-size: 12px; color: #6e6e6e; margin-bottom: 0px">' . @$val['name_display'] . ' ' . $quantity_option . ' ' . $price . ' </p>';
							$_SESSION['price_options_value'] += $val['price'] * @$val['quantity'];
						};
					};
				}
				?>
            <tr class="product-row">
              <td>
                <img class="thumbnail-detail" src="<?php echo ($item->old_id > 0 && strpos($item->image, 'images/products/products') !== false) ? URL_ROOT . str_replace('images/products/products/', 'images/products/products/medium_', $item->image) : URL_ROOT . str_replace('original', 'resized', $item->image) ?>" alt="<?php echo $item->alias ?>">
              </td>
              <td style="width: 50%"><span><?php echo $item->product_name ?> <?php echo $options_display ?></span></td>
              <td><?php echo format_money($item->price) ?></td>
              <td><?php echo $item->count ?></td>
              <td><?php //echo format_money($item->price_has_options * $item->count) ?></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="row no-gutters pay">
    <div class="col-md-9 no-gutters" style="max-width: 720px; text-align: end">
      <p><?php // echo FSText::_("Tạm tính:") ?></p>
      <p><?php echo FSText::_("Tổng thanh toán:") ?></p>
    </div>
    <div class="col-md-3 no-gutters" style="max-width: 720px; text-align: end; padding-right: 12px">
      <p class="bold"><?php // echo format_money($total) ?></p>
      <p class="bold red--text size--text-pay"><?php echo $order->vat ? format_money($total_vat) : format_money($total_money) ?></p>
    </div>
  </div>
</div>

<div class="detail-mobile">
  <div class="row no-gutters">
    <div class="col no-gutters">
      <a href="" class="d-flex align-items-center" style="height: 30px">
        <i class="fa fa-chevron-left" aria-hidden="true"></i>
        <span class="pl-3"> <?php echo FSText::_("Trở lại") ?><span>
      </a>
    </div>
  </div>
  <div class="row no-gutters">
    <div class="col-1 no-gutters pt-2">
      <i style="color: #C73536" class="fa fa-file-text-o" aria-hidden="true"></i>
    </div>
    <div class="col-11 no-gutters pt-2">
      <p><?php echo FSText::_("Mã đơn hàng:") ?> <span style="color: #0B1315"><?php echo 'DH' . str_pad($order->id, 8, "0", STR_PAD_LEFT) ?></span></p>
      <p><?php echo FSText::_("Ngày đặt hàng:") ?> <?php echo $order->created_time ?></p>
      <p><?php echo FSText::_("Trạng thái:") ?> <?php echo $order->status ?></p>
    </div>
  </div>
  <div class="row no-gutters">
    <div class="col-1 no-gutters pt-2">
      <i style="color: #C73536" class="fa fa-map-marker" aria-hidden="true"></i>
    </div>
    <div class="col-11 no-gutters pt-2">
      <p style="font-weight: 500"><?php echo FSText::_("Địa chỉ người nhận: ") ?></p>
      <p class="text-capitalize bold"><?php echo $order->name ?></p>
      <p><?php echo FSText::_("Địa chỉ:") ?> <?php echo $order->address . ', ' . $order->district . ', ' . $order->province ?></p>
    </div>
  </div>
  <div class="row no-gutters">
    <div class="col-1 no-gutters pt-3">
      <i style="color: #C73536" class="fa fa-shopping-basket" aria-hidden="true"></i>
    </div>
    <div class="col-11 no-gutters pt-2">
      <div class="row no-gutters">
        <div class="col no-gutters">
          <p><?php echo FSText::_("Thông tin kiện hàng") ?></p>
        </div>
      </div>
      <?php foreach ($product_detail as $item) {
        $option_str = '';
        $options = explode(',', $item->options);
        $options = $this->cleanArray($options);
        if (!empty($options))
          foreach ($options as $val) {
            $option_str .= ' - ';
            $rs = $this->model->get_record_by_id($val, 'fs_products_values');
            $option_str .= $this->model->get_record_by_id($rs->parent_id, 'fs_products_options')->name;
            $option_str .= ': ' . $rs->name;
          }
      ?>
        <div class="row no-gutters">
          <div class="col no-gutters">
            <img class="thumbnail-detail" src="<?php echo ($item->old_id > 0 && strpos($item->image, 'images/products/products') !== false) ? URL_ROOT . str_replace('images/products/products/', 'images/products/products/medium_', $item->image) : URL_ROOT . str_replace('original', 'resized', $item->image) ?>" alt="<?php echo $item->alias ?>">
          </div>
          <div class="col no-gutters">
            <?php echo $item->product_name ?> <?php echo $option_str ?>
            <p><?php echo format_money($item->price) ?> x <?php echo $item->count ?></p>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
  <div class="row no-gutters">
    <div class="col-1 no-gutters pt-3">
      <i style="color: #C73536" class="fa fa-truck" aria-hidden="true"></i>
    </div>
    <div class="col-11 no-gutters pt-2">
      <p><?php echo FSText::_("Hình thức giao hàng") ?></p>
      <p><?php echo FSText::_("Miễn phí giao hàng") ?></p>
    </div>
  </div>
  <div class="row no-gutters">
    <div class="col-1 no-gutters pt-3">
      <i style="color: #C73536" class="fa fa-money" aria-hidden="true"></i>
    </div>
    <div class="col-11 no-gutters pt-2">
      <p class="bold"><?php echo FSText::_("Hình thức thanh toán") ?></p>
      <p><?php echo $order->payment_method ?></p>
    </div>
  </div>
  <div class="row no-gutters">
    <div class="col no-gutters">
      <div class="row no-gutters">
        <div class="col no-gutters">
          <p><?php echo FSText::_("Tạm tính:") ?> <span style="position: absolute; right: 0" class="bold"><?php echo format_money($total) ?></span></p>
        </div>
      </div>
      <div class="row no-gutters">
        <div class="col no-gutters">
          <!-- <p><?php echo FSText::_("VAT(10%):") ?> <span style="position: absolute; right: 0"><?php echo $order->vat ? format_money($total * 0.1) : "Không áp dụng" ?></span></p> -->
        </div>
      </div>
      <div class="row no-gutters">
        <div class="col no-gutters">
          <p><?php echo FSText::_("Tổng thanh toán:") ?> <span style="position: absolute; right: 0" class="bold red--text size--text-pay"><?php echo $order->vat ? format_money($total_vat) : format_money($total) ?></span></p>
        </div>
      </div>
    </div>
  </div>
</div>