<h1>Quản lý đơn hàng</h1>
<table class="table">
  <thead>
    <tr>
      <th scope="col">Mã đơn hàng</th>
      <th scope="col">Ngày mua</th>
      <th scope="col">Sản phẩm</th>
      <th scope="col">Tổng tiền</th>
      <th class="text-right" scope="col">Trạng thái đơn hàng</th>
    </tr>
  </thead>
  <tbody id="product_list">
    <?php foreach ($listOrder as $item) { ?>
      <?php if (!empty($item->product_name[0]->product_name)) { ?>
        <tr>
          <td><a href="javscript:void(0)" class="detail-btn" data-id="<?php echo $item->id ?>"><?php echo 'DH' . str_pad($item->id, 8, "0", STR_PAD_LEFT) ?></a></td>
          <td><?php echo $item->created_time ?></td>
          <td><?php echo $item->product_name[0]->product_name ?><?php echo count($item->product_name) > 1 ? " ..." : null ?></td>
          <td class="bold"><?php echo format_money($item->total_end) ?></td>
          <td><?php echo $item->status ?></td>
        </tr>
      <?php } ?>
    <?php } ?>
  </tbody>
</table>
<div class="mobile-list-item">
  <?php foreach ($listOrder as $item) { ?>
    <?php if (isset($item->product_name[0]->product_name)) { ?>
      <div class="item-order">
        <a href="javscript:void(0)" class="detail-btn" data-id="<?php echo $item->id ?>">
          <p class="item-name">Tên sản phẩm: <?php echo $item->product_name[0]->product_name ?><?php echo count($item->product_name) > 1 ? " ..." : null ?></p>
          <div class="description">
            <p>Mã sản phẩm: <?php echo 'DH' . str_pad($item->id, 8, "0", STR_PAD_LEFT) ?></p>
            <p>Giá: <?php echo format_money($item->total_end) ?></p>
            <p>Ngày đặt hàng: <?php echo $item->created_time ?></p>
            <p>Trạng thái: <?php echo $item->status ?></p>
          </div>
        </a>
      </div>
    <?php } ?>
  <?php } ?>
</div>
<?php if ($pagination) {
  echo $pagination->showPagination(5, "v-pills-tabContent");
} ?>