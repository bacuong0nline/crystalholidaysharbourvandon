<?php
global $tmpl;
$tmpl->addStylesheet('thanhvien', 'modules/members/assets/css');
$tmpl->addScript('registerform2', 'modules/members/assets/js');
$tmpl->addScript('signinform2', 'modules/members/assets/js');

$alert_info = array(
  0 => FSText::_('Địa chỉ email không được bỏ trống'),
  1 => FSText::_('Địa chỉ email không hợp lệ'),
  2 => FSText::_('Mật khẩu phải nhiều hơn 6 ký tự'),
  3 => FSText::_('Mật khẩu nhập lại không trùng khớp'),
  4 => FSText::_('Vui lòng điền địa chỉ email hoặc số điện thoại'),
  5 => FSText::_('Vui lòng nhập mật khẩu'),

);
?>

<?php echo $tmpl->load_direct_blocks('breadcrumbs', array('style' => 'simple')); ?>

<main>
  <input type="hidden" id="alert_info" value='<?php echo json_encode($alert_info)?>' />
  <div class="container">
    <div class="row mb-5 no-gutters">
      <div class="col-md-6 divide no-gutters">
        <div class="divide-text">Hoặc</div>
        <div class="col no-gutters account mb-40px">
          <h4>Bạn đã có tài khoản SUNTEK</h4>
        </div>
        <div class="w-100"></div>
        <div class="col no-gutters mb-40px">
          <p>Quý khách vui lòng đăng nhập để thuận tiện trong việc thanh toán, xem chi tiết</p>
        </div>
        <div class="col no-gutters">
          <form action="" method="post" class="form-style" name="signin-form" id="signin-form">
            <div class="mb-30px">
              <label for="mail-phone-signin">Email hoặc số điện thoại <span class="red--text required">(*)</span>:</label><br>
              <input type="text" id="mail-phone-signin" name="mail-phone-signin" placeholder="Email hoặc số điện thoại đã đăng kí tài khoản">
            </div>
            <div class="mb-30px">
              <label for="password-signin">Mật khẩu <span class="red--text required">(*)</span>:</label><br>
              <input type="password" id="password-signin" name="password-signin" placeholder="********">
            </div>
            <a href="javascript:void(0)" class="btn-submit accept-btn" id="request_signin">Đăng nhập</a>
            <input type="hidden" name="module" value="members" />
						<input type="hidden" name="view" value="members" />
						<input type="hidden" name="task" value="login_save" />
          </form>
        </div>
        <div class="col no-gutters forget-password mt-2">
          <a href="<?php echo FSRoute::_("index.php?module=members&view=forget")?>">Quên mật khẩu?</a>
        </div>
        <div class="col no-gutters sign-in-facebook mt-2">
          <a href="<?php echo FSRoute::_('index.php?module=members&view=facebook&task=face_login') ?>">
            <img src="./img/trangchu/fb.png" />
            Đăng nhập bằng Facebook
          </a>
        </div>
        <div class="col no-gutters sign-in-google mt-2">
          <a style="width:100%; display: block" href="<?php echo FSRoute::_('index.php?module=members&view=google&raw=1&task=google_login') ?>">
            <img src="./img/trangchu/google.png" />
            Đăng nhập bằng Google
          </a>
        </div>
      </div>
      <div class="col-md-6 right-col no-gutters">
        <div class="col no-gutters account mb-40px">
          <h4>Bạn chưa có tài khoản SUNTEK</h4>
        </div>
        <div class="w-100"></div>
        <div class="col no-gutters mb-40px">
          <p>Quý khách vui lòng đăng ký để thuận tiện trong việc thanh toán, xem chi tiết đơn hàng và nhận nhiều ưu đãi hấp đẫn.</p>
        </div>
        <div class="col no-gutters ">
          <form action="" class="form-style" method="post" name="register-form" id="register-form">
            <div class="mb-30px">
              <label for="mail-register">Email <span class="red--text required">(*)</span>:</label>
              <input type="text" id="mail-register" name="mail-register" autocomplete="off" placeholder="Email của bạn">
            </div>
            <div class="mb-30px">
              <label for="phone-register">Số điện thoại <span class="red--text required">(*)</span>:</label>
              <input type="text" id="phone-register" name="phone-register" autocomplete="off" placeholder="Số điện thoại của bạn">
            </div>
            <!-- <div class="mb-30px">
              <label for="address-register">Địa chỉ:</label>
              <input type="text" id="address-register" name="address-register" autocomplete="off" placeholder="Địa chỉ của bạn">
            </div> -->
            <div class="mb-30px">
              <label for="password-register">Mật khẩu <span class="red--text required">(*)</span>:</label>
              <input type="password" id="password-register" name="password-register" placeholder="********">
            </div>
            <div class="mb-30px">
              <label for="password-register-confirm">Nhập lại mật khẩu <span class="red--text required">(*)</span>:</label>
              <input type="password" id="password-register-confirm" name="password-register-confirm" placeholder="********">
            </div>

            <a href="javascript:void(0)" class="btn-submit accept-btn ripple-btn" id="request_register">Đăng ký</a>
            <input type="hidden" name="module" value="members" />
						<input type="hidden" name="view" value="members" />
						<input type="hidden" name="task" value="register_save" />
          </form>
        </div>
        <!-- <div class="col no-gutters sign-in-facebook mt-3">
          <a href="#">
            <img src="./img/trangchu/fb.png" />
            Đăng ký bằng Facebook
          </a>
        </div>
        <div class="col no-gutters sign-in-google mt-3">
          <a href="#">
            <img src="./img/trangchu/google.png" />
            Đăng ký bằng Google
          </a>
        </div> -->
      </div>
    </div>
  </div>
</main>