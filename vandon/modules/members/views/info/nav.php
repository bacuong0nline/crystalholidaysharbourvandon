<?php
global $tmpl;
$tmpl->addStylesheet('thongtintaikhoan', 'modules/members/assets/css');
$tmpl->addScript('thongtintaikhoan', 'modules/members/assets/js');

$link_info = FSRoute::_("index.php?module=members&view=info");
$link_order = FSRoute::_("index.php?module=members&view=history");
$link_address = FSRoute::_("index.php?module=members&view=address")
?>

<div class="d-flex align-items-start user-tab">
  <img class="avatar" src="<?php echo !empty($avatar) ? $avatar : "/images/avatar.png" ?>" alt="avatar" />
  <div>
    <p class="small--text">Tài khoản của</p>
    <p class="bold medium--text">
      <?php
      if ($user->userInfo->first_name && $user->userInfo->last_name) {
        $name = $user->userInfo->first_name . ' ' . $user->userInfo->last_name;
      }
      if ($user->userInfo->facebook_id) {
        $name = $user->userInfo->name;
      } else {
        $name = $user->userInfo->email;
      }
      echo $name;
      ?>
    </p>
  </div>
</div>
<div class="nav flex-column nav-pills tab-user" id="v-pills-tab" role="tablist" aria-orientation="vertical">
  <a class="nav-link active" href="<?php echo $link_info ?>">
    <div class="block-icon-tab">
      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
        <path id="Path_645" data-name="Path 645" d="M72,72a4,4,0,1,0-4-4A4.012,4.012,0,0,0,72,72Zm0,2c-2.65,0-8,1.35-8,4v2H80V78C80,75.35,74.65,74,72,74Z" transform="translate(-64 -64)" fill="#999" />
      </svg>
    </div>
    Thông tin tài khoản
  </a>
  <a class="nav-link" href="<?php echo $link_order ?>">
    <div class="block-icon-tab">
      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
        <g id="checklist" transform="translate(0 -0.007)">
          <g id="Group_659" data-name="Group 659" transform="translate(3.003 0.007)">
            <path id="Path_647" data-name="Path 647" d="M201.956,15.639a.734.734,0,0,0,.535-.24.752.752,0,0,0,.189-.493V.745a.739.739,0,0,0-.739-.738H192.7a1.664,1.664,0,0,1,.409.65h.545a.741.741,0,0,1,.738.683h1.817A1.189,1.189,0,0,1,197.4,2.527V15.644l4.554-.005Z" transform="translate(-189.684 -0.007)" fill="#999" />
            <path id="Path_648" data-name="Path 648" d="M98.554,18.083a.647.647,0,0,0-1.294,0,.514.514,0,0,1-.514.5h-.683c0,.115,0,.452,0,.562h3.688c0-.151,0-.417,0-.562h-.683A.514.514,0,0,1,98.554,18.083Z" transform="translate(-96.064 -16.909)" fill="#999" />
          </g>
          <path id="Path_649" data-name="Path 649" d="M9.532,75.559H7.718v.108a.8.8,0,0,1-.8.8H2.776a.8.8,0,0,1-.8-.8v-.108H.161A.16.16,0,0,0,0,75.717V89.038a.16.16,0,0,0,.161.158H9.532a.16.16,0,0,0,.161-.158V75.717A.16.16,0,0,0,9.532,75.559Zm-8.4,3.006a.517.517,0,0,1,.731-.037l.254.23.949-1.084a.517.517,0,1,1,.778.681l-1.3,1.48a.532.532,0,0,1-.736.043L1.166,79.3a.517.517,0,0,1-.037-.731Zm7.045,9.469h-6.7a.517.517,0,1,1,0-1.034h6.7a.517.517,0,1,1,0,1.034ZM1.13,84.495a.517.517,0,0,1,.731-.037l.254.23L3.064,83.6a.517.517,0,0,1,.778.681l-1.3,1.48a.532.532,0,0,1-.736.043l-.644-.583a.517.517,0,0,1-.037-.731ZM8.175,85.92H5.267a.517.517,0,1,1,0-1.034H8.175a.517.517,0,1,1,0,1.034Zm0-3.782h-6.7a.517.517,0,1,1,0-1.034h6.7a.517.517,0,1,1,0,1.034Zm0-2.145H5.267a.517.517,0,1,1,0-1.034H8.175a.517.517,0,1,1,0,1.034Z" transform="translate(0 -73.19)" fill="#999" />
        </g>
      </svg>
    </div>
    Quản lý đơn hàng
  </a>
  <a class="nav-link" href="<?php echo $link_address ?>">
    <div class="block-icon-tab">
      <svg xmlns="http://www.w3.org/2000/svg" width="11.589" height="16" viewBox="0 0 11.589 16">
        <g id="maps-and-flags_2_" data-name="maps-and-flags (2)" transform="translate(-70.573)">
          <g id="Group_661" data-name="Group 661" transform="translate(70.573)">
            <path id="Path_653" data-name="Path 653" d="M76.368,0a5.8,5.8,0,0,0-5.795,5.795c0,3.965,5.186,9.786,5.406,10.032a.522.522,0,0,0,.776,0c.221-.246,5.406-6.067,5.406-10.032A5.8,5.8,0,0,0,76.368,0Zm0,8.71a2.915,2.915,0,1,1,2.915-2.915A2.919,2.919,0,0,1,76.368,8.71Z" transform="translate(-70.573)" fill="#999" />
          </g>
        </g>
      </svg>
    </div>
    Sổ địa chỉ
  </a>
  <form method="post" id="fsignout" name="fsignout" action="">
    <a class="nav-link" id="signout" href>
      <div class="block-icon-tab">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
          <g id="Icon_ionic-md-log-out" data-name="Icon ionic-md-log-out" transform="translate(-1.5 -1.5)">
            <path id="Path_650" data-name="Path 650" d="M6,9.515h7.3l-1.677,1.719,1.15,1.15,3.692-3.692L12.769,5,11.577,6.15,13.3,7.869H6Z" transform="translate(1.038 0.808)" fill="#999" />
            <g id="Group_660" data-name="Group 660" transform="translate(1.5 1.5)">
              <path id="Path_651" data-name="Path 651" d="M9.488,15.858A6.361,6.361,0,0,1,5,5,6.342,6.342,0,0,1,13.969,5l1.165-1.165a8.5,8.5,0,0,0-1.065-.892,8.006,8.006,0,1,0,1.062,12.227L13.969,14A6.3,6.3,0,0,1,9.488,15.858Z" transform="translate(-1.5 -1.5)" fill="#999" />
              <path id="Path_652" data-name="Path 652" d="M13.981,7.981h.046v.046h-.046Z" transform="translate(1.38 -0.004)" fill="#999" />
            </g>
          </g>
        </svg>
      </div>
      Đăng xuất
    </a>
    <input type="hidden" name="module" value="members" />
    <input type="hidden" name="view" value="members" />
    <input type="hidden" name="task" value="logout" />
  </form>
</div>