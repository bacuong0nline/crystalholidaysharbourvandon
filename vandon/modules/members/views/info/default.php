<?php
global $tmpl;
$tmpl->addStylesheet('thongtintaikhoan', 'modules/members/assets/css');
$tmpl->addScript('thongtintaikhoan', 'modules/members/assets/js');

$alert_info = array(
  0 => FSText::_('Mật khẩu cũ không được bỏ trống'),
  1 => FSText::_('Mật khẩu mới không được bỏ trống'),
  2 => FSText::_('Kiểm tra mật khẩu không được bỏ trống'),
  3 => FSText::_('Mật khẩu không trùng khớp')
);
?>

<main class="mt-4 mb-4">
  <input type="hidden" id="alert_info" value='<?php echo json_encode($alert_info) ?>' />
  <div class="container">
    <div class="row no-gutters">
      <div class="col-12 col-lg-3 left-col no-gutters">
        <?php include 'nav.php' ?>
      </div>
      <div class="col-12 col-lg-9 right-col no-gutters">
        <div class="tab-content info-tab" id="v-pills-tabContent">
          <div class="tab-pane fade show active" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
            <h1><?php echo FSText::_("Thông tin tài khoản")?></h1>
            <form action="" method="post" id="finfor" name="finfor">
              <div class="row no-gutters d-flex justify-content-center">
                <div class="col no-gutters">
                  <div class="input-field">
                    <label for="fname"><?php echo FSText::_("Họ và tên")?></label><br />
                    <input type="text" id="ffirstname" name="ffirstname" value="<?php echo $first_name ? $first_name : "" ?>"><br />
                  </div>
                  <div class="col-md-5 no-gutters gender">
                    <label for="fgender"><?php echo FSText::_("Giới tính")?></label><br />
                    <div class="radio-container">
                      <label for="male" style="margin-left: 25px;">
                        <?php echo FSText::_("Nam")?>
                        <input type="radio" name="fgender" id="male" value="0" <?php echo $info->sex ? null : 'checked' ?> />
                        <span class="style-radio"></span>
                      </label>
                    </div>
                    <div class="radio-container">
                      <label for="female" style="margin-left: 25px">
                        <?php echo FSText::_("Nữ")?>
                        <input type="radio" name="fgender" id="female" value="1" <?php echo $info->sex ? 'checked' : null ?> />
                        <span class="style-radio"></span>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="col no-gutters phone">
                  <div class="input-field">
                    <label style="opacity: 0" for="fname"><?php echo FSText::_("Họ và tên")?></label><br />
                    <input type="text" id="flastname" name="flastname" value="<?php echo $last_name ? $last_name : "" ?>"><br />
                  </div>
                  <div class="input-field">
                    <label for="fphone"><?php echo FSText::_("Số điện thoại")?></label><br />
                    <input disabled type="text" id="fphone" name="fphone" value="<?php echo $info->telephone ?>"><br />
                  </div>
                </div>
                <div class="col no-gutters d-flex justify-content-end phone">
                  <div>
                    <div class="input-field">
                      <label for="femail">Email</label><br />
                      <input type="text" id="email" name="femail" value="<?php echo $info->email ?>" disabled /><br />
                    </div>
                    <div class="row no-gutters">
                      <div class="col-md-7 birthday input-field">
                        <label for="fbirthday"><?php echo FSText::_("Ngày sinh")?></label>
                        <input type="date" name="fbirthday" id="fbirthday" value="<?php echo $info->birthday ? explode(" ", $info->birthday)[0] : "" ?>" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <label for="changepassword" class="check-box change-password ml-2">
                <input type="checkbox" id="changepassword" />
                <?php echo FSText::_("Thay đổi mật khẩu")?>
                <span class="checkmark checkmark-changepassword"></span>
              </label>
              <div class="row resetpassword">
                <div class="d-md-flex">
                  <div class="col-12 col-md-4">
                    <label for="foldpassword"><?php echo FSText::_("Mật khẩu cũ")?></label><br />
                    <input class="password-input" type="password" id="foldpassword" name="oldpassword"><br />
                  </div>
                  <div class="col-12 col-md-4">
                    <label for="fnewpassword"><?php echo FSText::_("Mật khẩu mới")?></label><br />
                    <input class="password-input" type="password" id="fnewpassword" name="newpassword"><br />
                  </div>
                  <div class="col-12 col-md-4">
                    <label for="fconfirmpassword"><?php echo FSText::_("Xác nhận mật khẩu")?></label><br />
                    <input class="password-input" type="password" id="fconfirmpassword" name="confirmpassword"><br />
                  </div>
                </div>
              </div>
              <div style="margin-top: 40px; margin-bottom: 20px">
                <a  class="text-uppercase" style="font-weight: 600" href id="update-info"> <?php echo FSText::_("Cập nhật")?> </a>
              </div>
              <input type="hidden" name="module" value="members" />
              <input type="hidden" name="view" value="info" />
              <input type="hidden" name="task" value="updateInfo" />
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>