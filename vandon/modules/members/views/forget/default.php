<?php
global $tmpl;
$tmpl->addTitle("Quên mật khẩu");
$tmpl->addStylesheet("users_forget", "modules/members/assets/css");
$Itemid = FSInput::get('Itemid', 1);
// $redirect = FSInput::get('redirect');
?>

<div id="login-form" class="frame_large">
  <div class="frame_auto_head">
    <div class="frame_auto_head_l text-center">
      <h4><?php echo FSText::_("Quên mật khẩu")?></h4>
    </div>
  </div>
  <div class="frame_auto_body">

    <form action="" name="forget_form" class="forget_form" method="post">

      <span><?php echo FSText::_("Chúng tôi sẽ tiến hành kiểm tra và gửi lại mật khẩu vào email của bạn.")?></span>
      <ul>
        <li>
          <span><?php echo FSText::_("Email/Số điện thoại:")?> </span><br>
          <input class="txtinput" type="text" name="email" />
        </li>
        <li>
          <span> <?php echo FSText::_("Mã xác nhận:")?></span><br>
          <input type="text" id="txtCaptcha" value="" name="txtCaptcha" maxlength="10" size="7" />
          <a href="javascript:changeCaptcha();" title="<?php echo FSText::_("Click để thay đổi mã xác nhận")?>" class="code-view">
          <img id="imgCaptcha" src="<?php echo URL_ROOT ?>libraries/jquery/ajax_captcha/create_image.php" />
          </a>
        </li>
        <li class="d-flex justify-content-center">
          <input type="submit" class='submitbt' name="submitbt" value="<?php echo FSText::_("Đồng ý"); ?>" />
        </li>
      </ul>

      <input type="hidden" name="module" value="members" />
      <input type="hidden" name="view" value="forget" />
      <input type="hidden" name="task" value="forget_save" />
    </form>

  </div>

</div>