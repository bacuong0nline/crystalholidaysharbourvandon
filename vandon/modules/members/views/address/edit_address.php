<?php
$alert_info2 = array(
  0 => FSText::_('Bạn chưa nhập họ và tên'),
  1 => FSText::_('Bạn chưa nhập số điện thoại'),
  2 => FSText::_('Số điện thoại không hợp lệ'),
  3 => FSText::_('Vui lòng nhập địa chỉ chi tiết'),
  4 => FSText::_('Số điện thoại từ'),
  5 => FSText::_('đến'),
  6 => FSText::_('số'),
);

?>

<div class="edit-address">
  <input type="hidden" id="alert_info2" value='<?php echo json_encode($alert_info2) ?>' />
  <h1>Sửa địa chỉ</h1>
  <form action="" method="post" id="faddressbook">
    <div class="row no-gutters justify-content-between">
      <div class="input-field" style="width: 32%">
        <label for="fname"><?php echo FSText::_("Họ") ?></label><br />
        <input type="text" id="ffirstname" name="ffirstname" value="<?php echo $info_address->first_name ?>"><br />
      </div>
      <div class="input-field" style="width: 32%">
        <label for="fname"><?php echo FSText::_("Tên") ?></label><br />
        <input type="text" id="flastname" name="flastname" value="<?php echo $info_address->last_name ?>"><br />
      </div>
      <div class="input-field" style="width: 32%">
        <label for="fphone"><?php echo FSText::_("Số điện thoại") ?><span class="bold red--text">*</span></label><br />
        <input type="text" id="fphone" name="fphone" value="<?php echo $info_address->telephone ?>"><br />
      </div>
    </div>
    <div class="row no-gutters justify-content-between" style="width: 100%">
      <div class="col-6">
        <div>
          <label for="fprovince">Tỉnh thành phố <span class="bold red--text">*</span></label><br />
          <select name="fprovince" class="select2 select2-box" id="fprovince">
            <?php foreach ($province as $item) { ?>
              <option <?php echo $info_address->province_id == $item->id ? 'selected' : null ?> data-id=<?php echo $item->id ?> value=<?php echo $item->id ?>><?php echo $item->name ?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="col-6">
        <div>
          <label for="fdistrict">Quận / Huyện <span class="red--text bold">*</span></label><br />
          <select name="fdistrict" class="select2 select2-box" id="fdistrict">
            <?php foreach ($district as $item) { ?>
              <option <?php echo $item->id == $info_address->district_id ? 'selected' : null ?> data-id=<?php echo $item->id ?> value=<?php echo $item->id ?>>
                <?php echo $item->name ?>
              </option>
            <?php } ?>
          </select>
        </div>
      </div>
    </div>
    <div class="row no-gutters justify-content-between">
      <div class="input-field col-12">
        <label for="faddress">Địa chỉ <span class="bold red--text">*</span></label><br />
        <input type="text" id="faddress" name="faddress" value="<?php echo $info_address->address ?>" placeholder="Số nhà, tên ngõ, tên đường"><br />
      </div>
    </div>
    <label for="default-address" class="check-box default-address ml-2">
      <input name="fdefault" type="checkbox" id="default-address" />
      Đặt địa chỉ này làm địa chỉ nhận hàng mặc định
      <span class="checkmark checkmark-address"></span>
    </label>
    <div style="margin-top: 40px; margin-bottom: 20px">
      <a onlick="updateAddress" style="font-weight: 600" id="address-book-btn"> Cập nhật </a>
    </div>
    <input type="hidden" name="module" value="members" />
    <input type="hidden" name="view" value="address" />
    <input type="hidden" name="task" value="edit_address_book" />
    <input type="hidden" name="id" value="<?php echo $info_address->id ?>" />

  </form>
</div>
<script>
  $('#fprovince').select2({
    templateSelection: function(data, container) {
      let idProvince = $(data.element).attr('data-id');
      loadDistrict(idProvince)
      return data.text;
    }
  });

  $('#fdistrict').select2({
    templateSelection: function(data, container) {
      let idDistrict = $(data.element).attr('data-id');
      //   console.log(idDistrict);
      // loadCommunes(idDistrict)
      return data.text;
    }
  });

  function loadDistrict(idProvince) {
    $.ajax({
      type: 'get',
      url: '/index.php?module=members&view=members&raw=1&task=ajax_load_district',
      // contentType: "application/json",
      dataType: "html",
      data: {
        idCity: idProvince
      },
      success: function(data) {
        // console.log(data);
        selectDistrict(JSON.parse(data))
        $("#fdistrict").val(<?php echo (int)$info_address->district_id ?>).trigger('change');

        return true;
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        console.log(textStatus)
      }
    });
  }
  $('#fdistrict').val(<?php echo $info_address->district_id ?>);

  function loadCommunes(idDistrict) {
    $.ajax({
      type: 'get',
      url: '/index.php?module=members&view=members&raw=1&task=ajax_load_ward',
      // contentType: "application/json",
      dataType: "text",
      data: {
        idDistrict: idDistrict
      },
      success: function(data) {
        selectCommune(JSON.parse(data))
        return true;
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        console.log(textStatus)
      }
    });
  }
  $("#address-book-btn").on('click',function(e) {
    $("#faddressbook").submit()
  })
</script>

<style>
  .select2 {
    width: 100% !important;
  }
</style>