<?php
global $tmpl;
$tmpl->addStylesheet('select2.min', 'modules/payment/assets/css');
$tmpl->addStylesheet('thongtintaikhoan', 'modules/members/assets/css');
$tmpl->addScript('select2.min', 'modules/payment/assets/js');
$tmpl->addScript('thongtintaikhoan', 'modules/members/assets/js');
?>


<main class="mt-4 mb-4">
  <div class="container">
    <div class="row no-gutters">
      <div class="col-12 col-lg-3 left-col no-gutters">
        <?php include 'nav.php' ?>
      </div>
      <div class="col-12 col-lg-9 right-col no-gutters">
        <div class="tab-content" id="v-pills-tabContent">
          <div class="so-dia-chi">
            <h1><?php echo FSText::_("Sổ địa chỉ")?></h1>
            <div class="row no-gutters">
              <a class="col d-flex no-gutters justify-content-center align-items-center add-address">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 16 16">
                  <path id="Path_923" data-name="Path 923" d="M10,2a8,8,0,1,0,8,8A8,8,0,0,0,10,2Zm4.267,8.533H10.533v3.733a.533.533,0,1,1-1.067,0V10.533H5.733a.533.533,0,1,1,0-1.067H9.467V5.733a.533.533,0,1,1,1.067,0V9.467h3.733a.533.533,0,1,1,0,1.067Z" transform="translate(-2 -2)" fill="#81954e" />
                </svg>
                <p class="ml-3" style="color: #81954E; text-transform: uppercase; font-weight: 600"><?php echo FSText::_("Thêm địa chỉ mới")?></p>
              </a>
            </div>
            <div class="list-address style-3">
              <?php foreach ($mba as $item) { ?>
                <div class="row no-gutters address-1">
                  <div class="col-md-10 no-gutters address-1__div-info">
                    <p style="font-weight: 600" class="text-uppercase"><?php echo $item->first_name . ' ' . $item->last_name ?></p>
                    <p><span class="grey--text">Địa chỉ:</span> <?php echo $item->content . ', ' . $item->district . ', ' . $item->province ?></p>
                    <p><span class="grey--text"> Điện thoại:</span> <?php echo $item->telephone ?></p>
                  </div>
                  <div class="col d-flex flex-column align-items-end">
                    <?php if ($item->default) { ?>
                      <div class="d-flex green--text">
                        <i class="fa fa-check-circle mr-1" aria-hidden="true"></i>
                        <p>Địa chỉ mặc định</p>
                      </div>
                    <?php } ?>
                    <a class="edit-address-btn" data-id="<?php echo $item->id ?>">Chỉnh sửa</a>
                    <?php if (!$item->default) { ?>
                      <a class="delete-address-btn red--text" data-id="<?php echo $item->id ?>">Xóa</a>
                    <?php } ?>
                  </div>
                </div>
              <?php } ?>
            </div>

          </div>
          <div class="form-address">
            <?php include 'add_address.php' ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>