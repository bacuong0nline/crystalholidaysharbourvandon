<?php
$alert_info2 = array(
  0 => FSText::_('Bạn chưa nhập họ và tên'),
  1 => FSText::_('Bạn chưa nhập số điện thoại'),
  2 => FSText::_('Số điện thoại không hợp lệ'),
  3 => FSText::_('Vui lòng nhập địa chỉ chi tiết'),
  4 => FSText::_('Số điện thoại từ'),
  5 => FSText::_('đến'),
  6 => FSText::_('số'),
);

?>

<div class="add-new">
  <input type="hidden" id="alert_info2" value='<?php echo json_encode($alert_info2) ?>' />
  <h1>Thêm địa chỉ mới</h1>
  <form action="" method="post" id="faddressbook">
    <div class="row no-gutters justify-content-between">
      <div class="input-field" style="width: 32%">
        <label for="fname">Họ</label><br />
        <input type="text" id="ffirstname" name="ffirstname" value=""><br />
      </div>
      <div class="input-field" style="width: 32%">
        <label for="fname">Tên</label><br />
        <input type="text" id="flastname" name="flastname" value=""><br />
      </div>
      <div class="input-field" style="width: 32%">
        <label for="fphone">Số điện thoại <span class="bold red--text">*</span></label><br />
        <input type="text" id="fphone" name="fphone" value=""><br />
      </div>
    </div>

    <div class="d-flex justify-content-between pb-3">
      <div style="width: 48%">
        <label for="fprovince">Tỉnh thành phố <span class="bold red--text">*</span></label><br />
        <select style="width: 100%" name="fprovince" class="" id="fprovince">
          <?php foreach ($province as $item) { ?>
            <option data-id=<?php echo $item->id ?> value=<?php echo $item->id ?>><?php echo $item->name ?></option>
          <?php } ?>
        </select>
      </div>
      <div style="width: 48%">
        <label for="fdistrict">Quận / Huyện <span class="red--text bold">*</span></label><br />
        <select  style="width: 100%" name="fdistrict" class="select2-box" id="fdistrict">
        </select>
      </div>
    </div>
    <div class="row no-gutters justify-content-between">
      <div class="input-field col-12">
        <label for="faddress">Địa chỉ <span class="bold red--text">*</span></label><br />
        <input type="text" id="faddress" name="faddress" placeholder="Số nhà, tên ngõ, tên đường"><br />
      </div>
    </div>
    <label for="default-address" class="check-box default-address ml-2">
      <input name="fdefault" type="checkbox" id="default-address" />
      Đặt địa chỉ này làm địa chỉ nhận hàng mặc định
      <span class="checkmark checkmark-address"></span>
    </label>
    <div style="margin-top: 40px; margin-bottom: 20px">
      <a style="font-weight: 600" href id="address-book-btn"> Cập nhật </a>
    </div>
    <input type="hidden" name="module" value="members" />
    <input type="hidden" name="view" value="address" />
    <input type="hidden" name="task" value="address_book" />
  </form>
</div>