<?php

class MembersModelsAddress extends FSModels
{

  function __construct()
  {
    parent::__construct();
    global $module_config;
    $errors = array();
    $fstable = FSFactory::getClass('fstable');
    $this->table_province = $fstable->_('fs_cities', 1);
    $this->table_address = $fstable->_('fs_members_address');
  }

  function getAddress($id)
  {

    global $db;
    $where = ' id = ' . $id;
    $query = 'SELECT * FROM ' . $this->table_address . '
              WHERE' . $where;
    //echo $query;
    $sql = $db->query($query);
    $result = $db->getObject();

    return $result;
  }
  // function getProvince($provinceid)
  // {
  //   global $db;
  //   $sql = " SELECT id,name,alias
  //     FROM fs_districts WHERE city_id = '" . $provinceid . "' ORDER BY ordering";
  //   $db->query($sql);
  //   return $db->getObjectList();
  // }
  function getDistrict($provinceid)
  {
    global $db;
    $sql = " SELECT id,name,alias
      FROM fs_districts WHERE city_id = '" . $provinceid . "' ORDER BY ordering";
    $db->query($sql);
    return $db->getObjectList();
  }

  function getWard($districts_id)
  {
    global $db;
    $sql = " SELECT id,name,alias
      FROM fs_wards WHERE districts_id = '" . $districts_id . "' ORDER BY ordering";
    $db->query($sql);
    return $db->getObjectList();
  }
  function getAllMemberAddress()
  {
    global $db, $user;
    $user_id = '';
    if ($user->userID) {
      $user_id = $user->userID;
    }
    $sql = " SELECT *
      FROM fs_members_address
      WHERE member_id  = '$user_id'";
    // $db->query($sql);
    //echo $sql;die;
    return $db->getObjectList($sql);
  }

  function getProvince()
  {
    global $db;
    $query = "SELECT *
              FROM " . $this->table_province . "
              where published = 1
              ORDER BY ordering ASC
    ";
    $db->query($query);
    $result = $db->getObjectList();
    return $result;
  }
}
