<?php

class MembersModelsHistory extends FSModels
{

  function __construct()
  {
    parent::__construct();
    global $module_config;
    $this->limit = 5;
    $errors = array();
    $fstable = FSFactory::getClass('fstable');
    $this->table_name = $fstable->_('fs_order', 1);
  }
  function set_query_body($uid)
  {
    $where = "";
    $query = "FROM " . $this->table_name ." where user_id = $uid";
    return $query;
  }

  function getListOrder($id)
  {
    if (!$id) {
      return;
    }
    global $db;


    $sql = " SELECT *
        FROM " . $this->table_name . " WHERE user_id = '" . $id . "' ORDER BY created_time DESC ";
    $db->query_limit($sql, $this->limit, $this->page);
    return $db->getObjectList();
  }

  function get_data_order($id)
  {
    global $db;
    $query = "  SELECT a.*,b.name as product_name, b.alias as product_alias,b.category_alias,b.weight, b.image, b.alias, b.old_id
					FROM fs_order_items AS a
					INNER JOIN fs_products AS b on a.product_id = b.id
					WHERE
						a.order_id = $id
					";
    $db->query($query);
    $result = $db->getObjectList();
    return $result;
  }

  function getTotal($query_body = null)
  {
    if (!$query_body)
      return;
    global $db;
    $query = "SELECT count(*)";
    $query .= $query_body;
    $sql = $db->query($query);
    $total = $db->getResult();
    return $total;
  }

  function getAjaxPagination($total, $current_page = 0) {
    FSFactory::include_class('AjaxPagination');
    $total_page = round($total / $this->limit);
    $pagination = new AjaxPagination($this->limit, $current_page, $total, $total_page);
    return $pagination;
  }
}
