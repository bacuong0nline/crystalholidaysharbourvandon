<?php

class MembersModelsForget extends FSModels
{

  function __construct()
  {
    parent::__construct();
    global $module_config;
    $errors = array();
    $fstable = FSFactory::getClass('fstable');
    $this->table_province = $fstable->_('fs_cities', 1);
    $this->table_address = $fstable->_('fs_members_address');
  }

  function forget()
  {
    global $db;
    $email = FSInput::get('email');	
    if(!$email)
      return false;
    $sql = " SELECT email, username, id ,name
        FROM fs_members
        WHERE email = '$email' or telephone = '$email'
         ";
    $db -> query($sql);
    return $db -> getObject();
  }

  function resetPass($userid)
  {
    $fstring = FSFactory::getClass('FSString','','../');
    $newpass =  $fstring->generateRandomString(8);
    $newpass_encode = md5($newpass);
    global $db;
    $sql = " UPDATE  fs_members SET 
          password = '$newpass_encode'
          WHERE 
          id = $userid
      ";
    // $db->query($sql);
    $rows = $db->affected_rows($sql);
    if(!$rows)
    {
      return false;
    }
    return $newpass;
  }
}
