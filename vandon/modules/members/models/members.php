<?php

class MembersModelsMembers extends FSModels {

    function __construct() {
        parent::__construct();
        global $module_config;
//        FSFactory::include_class('parameters');
//        $current_parameters = new Parameters($module_config->params);
//        $limit = $current_parameters->getParams('limit');
        //$limit = 6;
        $errors = array();
        $fstable = FSFactory::getClass('fstable');
        $this->table_districts = $fstable->_('fs_districts', 1);
        $this->table_communes = $fstable->_('fs_communes', 1);
        $this->table_name = $fstable->_('fs_members',1);

        $this->limit = 9;
    }


    // function set_query_body() {
    //     $date1 = FSInput::get("date_search");
    //     $where = "";
    //     $fs_table = FSFactory::getClass('fstable');
    //     $query = " FROM " . $fs_table->getTable('fs_faq',1) . "
	// 					  WHERE 
	// 					  	 published = 1
	// 					  	" . $where .
    //             " ORDER BY created_time DESC, id DESC 
	// 					 ";

    //     return $query;
    // }
    
    function set_query_body($email, $phone){
        $where = "";
        $query = "FROM ".$this->table_name." WHERE email='".$email."' OR phone = '".$phone."' ".$where."";
        return $query;
    }
    function form_empty($rows) {
        if(empty($rows['email'])) {
            return true;
        }
        if(empty($rows['phone'])) {
            return true;
        }
        if(empty($rows['password'])) {
            return true;
        }
        return false;
    }

    function user_check($query_body) {
        if(!$query_body) {
            return;
        }
        global $db;
        $query = "SELECT * ";
        $query .= $query_body;

        $sql = $db->query($query);

        $result = $db->getResult();

        return $result;
    }
    function check_exits_email()
    {
        global $db ;
        $email = FSInput::get("regis_email");

        if(!$email){
            return false;
        }

        $sql = " SELECT count(*) 
                FROM fs_members 
                WHERE 
                    email = '$email' 
                ";
        $db -> query($sql);
        $count = $db->getResult();
        if($count){
            $msg = "Email này đã có người sử dụng";
            $url = URL_ROOT;
            setRedirect($url, $msg, 'error');
            return false;
        }
        return true;
    }
    function check_exits_phone()
    {
        global $db ;
        $telephone = FSInput::get("regis_phone");

        if(!$telephone){
            return false;
        }

        $sql = " SELECT count(*) 
                FROM fs_members 
                WHERE 
                    telephone = '$telephone' 
                ";
        $db -> query($sql);
        $count = $db->getResult();
        if($count){
            $msg = "Số điện thoại này đã có người sử dụng";
            $url = URL_ROOT;
            setRedirect($url, $msg, 'error');
            return false;
        }
        return true;
    }
    function save()
    {
        global $db, $user;
        $row = array();
        $reg_first_name = FSInput::get("regis_first_name");
        $reg_last_name = FSInput::get("regis_last_name");

        $reg_email = FSInput::get("regis_email");
        $arr_email = explode( '@', $reg_email);
        $reg_phone = FSInput::get("regis_phone");
        $reg_password = FSInput::get("regis_password");

        if($this -> check_exist($arr_email[0], '', 'username', 'fs_members')){
            $row['username'] = $this -> genarate_username_news($arr_email[0],'','fs_members');
        }else{
            $row['username'] = $arr_email[0];
        }
        
        if(!$row['username'] && !$reg_password)
            return;

        $row['first_name'] = $reg_first_name;
        $row['last_name'] = $reg_last_name;
        $row['password'] = $reg_password;
        $row['telephone'] = $reg_phone;
        $row['email'] =  $reg_email;
        $row['block'] =  1;
        $row['published'] =  1;


        $fstring = FSFactory::getClass('FSString','','../');
        $row['activated_code'] =  $fstring->generateRandomString(32);

        $time = date("Y-m-d H:i:s");	
        $row['created_time']  = $time;
        $id = $user->insertUser($row);

        // $id = $this -> _add($row, 'fs_members');
        return $id;	
    }
    function activated(){
        $activated_code = FSInput::get('activated_code');
        //echo $activated_code;die;
        global $db;
        $sql = " UPDATE fs_members 
            SET block = 0
            WHERE activated_code = '$activated_code' ";
        //echo $sql;die;
        $rows = $db->affected_rows ($sql );
        return $rows;
    }
    // function getPagination($total) {
    //     FSFactory::include_class('Pagination');
    //     $pagination = new Pagination($this->limit, $total, $this->page);
    //     return $pagination;
    // }
    function genarate_username_news($value,$id = '',$table_name = ''){
        if(!$value)
            return false;
        if(!$table_name)
            $table_name = $this -> table_name;
        $i = 1;
        while(true){
            $value_news = $value.'_'.$i; 
            if(!$this -> check_exist($value_news,$id,'username',$table_name)){
                return $value_news;
            }
            $i ++;
        }
    }
    function login()
    {
        global $db;
        $user_info = FSInput::get('mail-phone-signin');	
        $password = md5(FSInput::get('password-signin'));

        $sql = " SELECT id, username,block, published,email, telephone
                FROM ".$this->table_name."
                WHERE (email = '$user_info' OR telephone = '$user_info')
                AND password = '$password' 
                AND block <> 1
                 "; //echo $sql; die();
        $db -> query($sql);
        $result = $db -> getObject();
        return $result;
    }
    function getFaq($id) {
        if ($id) {
            $where = " id = '$id' ";
        } else {
            $code = FSInput::get('code');
            if (!$code)
                die('Not exist this url');
            $where = " alias = '$code' ";
        }
        $fs_table = FSFactory::getClass('fstable');
        $query = " SELECT id,title,content,category_id,category_alias, summary,hits, video, is_video,
                        alias, tags, created_time, updated_time,seo_title,image,optimal_seo,
                        seo_keyword,seo_description,news_related,author_id,products_related, is_hot
						FROM " . $fs_table->getTable('fs_news',1) . " 
						WHERE 
						" . $where . " ";
        //print_r($query) ;   
        global $db;
        $sql = $db->query($query);
        $result = $db->getObject();
        return $result;
    }
    function getProvince() {
        global $db;
        $sql = "    SELECT *
                    FROM fs_cities
                ";
        $db->query($sql);
        $result = $db->getObjectList();
        return $result;
    }
    function getDistrict($id_Province) {
        global $db;
        $sql = "    SELECT *
                    FROM fs_districts
                    WHERE city_id = ".$id_Province."
                ";
        $db->query($sql);
        $result = $db->getObjectList();
        return $result;
    }
    function getWard($districts_id) {
        global $db;
        $sql = " SELECT id,name,alias
				    FROM fs_wards WHERE districts_id = '".$districts_id."' ORDER BY ordering";
        $db->query($sql);
        return $db->getObjectList();
    }

}

?>