$(function () {
	listproductpage2.init();
});
listproductpage2 = (function () {
	  function init() {
	        handler();
	  }
	  function handler() {
		  $.when($('.news_list').masonry({
	          gutterWidth: 3,
	          columnWidth: 1,
	          itemSelector: '.news-item'
	      })).done(function (v) {
	          ReloadListUI();
	      });
		  
	      $('.load_more').click(function () {
	  		// var cid =  $( "#category_id" ).val();
	  		
	    	// Start Masonry
	    	  jQuery('.news_list').masonry({
	    		  gutterWidth: 3,
		          columnWidth: 1,
		          itemSelector: '.news-item'
	    	  });
	            var _self = $(this);
	            _self.hide();
	            _self.parent().addClass("loading");
	            $.get("/index.php?module=news&view=home&task=fetch_pages&raw=1", { pagecurrent: $(this).attr('data-pagecurrent')}, function (data) {	             
	            	data = JSON.parse(data);
	            	$element = $(data.content);
	                console.log(data.next);
	                $('.news_list').append($element).masonry('appended', $element);
	                if (data.next) {
	                    _self.attr('data-pagecurrent', parseInt(data.totalCurrent));
	                    _self.attr('data-nextpage', parseInt(data.nextpage));
	                    _self.show();
	                    _self.parent().removeClass("loading");
	                } else {
	                    _self.parent().remove();
	                }

	            });
	        });
	  }
    return {
        init: init,
    };
})();

function ReloadListUI() {
    $('.news_list').each(function (i, e) {
        if ($(e).position().top == 0) {
            $(e).addClass('border-top');
        }
    });
}
