<?php
global $tmpl, $config;
$tmpl->addStylesheet('search', 'modules/search/assets/css');
$tmpl->addScript('search', 'modules/search/assets/js');
$tmpl->addTitle('Tìm kiếm');

$keyword = FSInput::get('keyword');
$total = count($list);
?>

<?php echo $tmpl->load_direct_blocks('breadcrumbs', array('style' => 'simple')); ?>

<?php
global $tmpl, $config;

?>
<div class="row products common no-gutters fetch-data-ajax" id="<?php echo $cid ?>">
  <div class="container pl-0 pr-0" style="padding-bottom: 150px">
    <?php if ($total > 0) { ?>
      <div class="row" style="width: 100%; margin-bottom: 20px">
        <div class="col">
          <span class="result" style="font-size: 15px;"><?php echo FSText::_('Kết quả tìm kiếm cho từ khóa:') ?></span>
          <strong style="font-size: 15px; display: inline-block;"><?php echo $keyword ?></strong>
        </div>
      </div>
      <div class=" mt-2 no-gutters news-box">
        <?php foreach ($list as $item) {
          $tag = $this->model->get_record_by_id($item->category_id, FSTable::_('fs_news_categories', 1))->tag;
        ?>
          <?php echo $tmpl->news_item($item, $tag) ?>
        <?php } ?>
      </div>
      <div class="row no-gutters" style="margin-bottom: 60px">
        <div class="col no-gutters d-flex justify-content-center">
          <?php if ($pagination) {
            echo $pagination->showPagination(1);
          }
          ?>
        </div>
      </div>
    <?php } else { ?>
      <span style="font-size: 15px;"><?php echo FSText::_('Không có kết quả nào cho từ khóa: ') ?>
      </span> &nbsp <strong style="font-size: 15px; display: inline-block; margin-bottom: 20px"> <?php echo $keyword ?></strong>
    <?php } ?>
  </div>
</div>