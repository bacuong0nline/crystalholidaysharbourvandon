<?php

	class SearchModelsSearch extends FSModels
	{
  function __construct()
  {
    parent::__construct();
    global $module_config;
    $limit = '';
    FSFactory::include_class('parameters');
      if(!empty($module_config->params)){
        $current_parameters = new Parameters($module_config->params);
        $limit   = $current_parameters->getParams('limit'); 
    }
    // $this->limit = $limit? $limit:10;
    $this->limit = 12;
    // $url = $_SERVER['REQUEST_URI'];
    // // $url = trim(preg_replace('/-page[0-9]+/i', '', $url));
    // if (strpos($url, '-page') !== false) {
    //   $this->page = substr($url, strpos($url, '.') - 1, 1);
    // }
   

    $fs_table = FSFactory::getClass('fstable');
    $this->table_name = $fs_table -> getTable('fs_news',1);
    $this->table_name_categories = $fs_table -> getTable('fs_products_categories',1);
  }
    function set_query_body()
    {
        $keyword = FSInput::get('keyword');
        if(!$keyword)
            return ;
        $fs_table = FSFactory::getClass('fstable');
        $where = "";
        $where .= " AND (name like '%".$keyword."%') ";
        $sql   = "	 FROM ".$this->table_name."
        WHERE published =1 $where"
        ;
        //print_r($sql);
        return $sql;
    }

      function getTotal($query_body)
      {
          $keyword = FSInput::get('keyword');
          if(!$keyword)
              return ;
          global $db;
          $where = "";
          $where .= " (name like '%".$keyword."%')";
          $fs_table = FSFactory::getClass('fstable');
          $query = "SELECT count(*) FROM ".$fs_table->getTable('fs_products',1)."
                    WHERE $where ";
          //echo $query;die;
          $sql = $db->query($query);
          $total = $db->getResult();
          return $total;
      }

      function getPagination($total = null) {
        FSFactory::include_class('Pagination');
        $pagination = new Pagination($this->limit, $total, $this->page);
        return $pagination;
      }

    function get_ajax_search(){
			global $db;
			$where = '';
			$query = FSInput::get('query', '');
			$arr_tags = explode ( ' ', $query );
			$total_tags = count ( $arr_tags );
			if ($total_tags) {
					$where .= ' AND (';
					$j = 0;
					for($i = 0; $i < $total_tags; $i ++) {
							$item = addslashes(trim ( $arr_tags [$i] ));
							if ($item) {
									if ($j > 0)
											$where .= ' AND ';
									$where .= " `name` like '%" . $item . "%'";
									$j ++;
							}
					}
					$where .= ' )';
			}
			$query = '  SELECT *
									FROM fs_products_categories 
									WHERE published = 1 AND level>0  '.$where.'
									ORDER BY ordering DESC
									LIMIT 20
									';

			$sql = $db->query($query);
			$result = $db->getObjectList();
			return $result;
    }

    function search_keyword(){
			global $db;
			$where = '';
			$query = FSInput::get('keyword', '');
      if(!$query)
          return ;
      $fs_table = FSFactory::getClass('fstable');
      $where = "";
      $where .= " AND (title like '%".$query."%') ";
      $sql   = "	 FROM ".$this->table_name."
      WHERE published = 1 $where";
			$query = '  SELECT *
									FROM '.$this->table_name.' 
									WHERE published = 1 '.$where.'
									ORDER BY ordering DESC
									';

      // $sql = $db->query($query);
      $sql = $db->query_limit($query, $this->limit, $this->page);
			$result = $db->getObjectList();
			return $result;
		}
	}
	
?>