<?php
global $tmpl;
$tmpl->addStylesheet('cat', 'modules/news/assets/css');
// $tmpl->addScript('home', 'modules/news/assets/js');
$cr = date_create(date('Y-m-d H:i:s'));

?>

<div>
    <?php echo $tmpl->load_direct_blocks('banners', array('style' => 'style4', 'category_id_image' => 4)) ?>
    <div>
        <div class="block-news">
            <div class="container">
                <div class="d-flex flex-wrap category-item" style="grid-gap: 10px">
                    <p class="mb-0 d-flex align-items-center"><?php echo FSText::_("Chủ đề") ?></p>
                    <?php foreach ($list_cat as $item) { ?>
                        <a class="cat-news" href="<?php echo FSRoute::_("index.php?module=knowledge&view=cat&ccode=$item->alias&cid=$item->id") ?>"><?php echo $item->name ?></a>
                    <?php } ?>
                </div>
                <?php foreach ($list_cat as $item) { ?>
                    <div>
                        <div class="title-cat-news">
                            <h2><?php echo $item->name ?></h2>
                        </div>
                    </div>
                    <?php if (!empty($item->news_child)) { ?>
                        <div class="grid-box">
                            <?php foreach ($item->news_child as $val3) {
                                $tag = $this->model->get_record_by_id($val3->category_id, FSTable::_('fs_knowledge_categories', 1))->tag;
                            ?>
                                <?php echo $tmpl->knowledge_item($val3, $tag) ?>
                            <?php } ?>
                        </div>
                        <a href="<?php echo FSRoute::_("index.php?module=knowledge&view=cat&ccode=$item->alias&cid=$item->id") ?>" class="btn-style-3 m-auto mb-5"><?php echo FSText::_("Xem thêm")?></a>
                    <?php } ?>
                    <?php if (!empty($item->cat_child)) { ?>
                        <div>
                            <div class="d-flex align-items-center mt-5 mb-5">
                                <p class="mb-0"><?php echo FSText::_("Chủ đề") ?></p>
                                <div class="d-flex ms-3 nav nav-tabs" id="nav-tab" role="tablist" style="grid-gap: 10px">
                                    <?php foreach ($item->cat_child as $key => $val) { ?>
                                        <a class="tag-news <?php echo $key == 0 ? 'active' : '' ?>" id="<?php echo $val->alias ?>-tab" data-bs-toggle="tab" data-bs-target="#<?php echo $val->alias ?>-tab-pane" type="button" role="tab" aria-controls="<?php echo $val->alias ?>-tab-pane" aria-selected="<?php echo $key == 0 ? true : false ?>"><?php echo $val->name ?></a>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="tab-content" id="nav-tabContent">
                                <?php foreach ($item->cat_child as $key1 => $val) { ?>
                                    <div class="tab-pane fade <?php echo $key1 == 0 ? 'active show' : '' ?>" id="<?php echo $val->alias ?>-tab-pane" role="tabpanel" aria-labelledby="<?php echo $val->alias ?>-tab" tabindex="0">
                                        <div class="grid-box">
                                            <?php foreach ($val->news as $val2) {
                                                $tag = $this->model->get_record_by_id($val->category_id, FSTable::_('fs_knowledge_categories', 1))->tag;
                                            ?>
                                                <?php echo $tmpl->knowledge_item($val2, $tag) ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>

                <div>

                </div>
            </div>

        </div>
    </div>
</div>