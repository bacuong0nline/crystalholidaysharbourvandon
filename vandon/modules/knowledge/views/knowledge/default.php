<?php
global $tmpl, $config;

$tmpl->addStylesheet('home', 'modules/news/assets/css');
$tmpl->addStylesheet('detail', 'modules/news/assets/css');
// $tmpl->addStylesheet('tintucchitiet', 'modules/news/assets/css');
$tmpl->addStylesheet('tintuc', 'modules/news/assets/css');

$tmpl->addScript('jquery.toc', 'modules/news/assets/js');
$tmpl->addScript('home', 'modules/news/assets/js');

?>
<main class="news-detail-wrapper" style="background: #F8F9F9;  padding-bottom: 100px">
    <div class="container desktop">
        <div class="d-flex justify-content-center pt-5 pb-4"></div>
    </div>
    <div class="container d-flex justify-content-between">
        <div>
            <p class="fw-bold" style="font-size: 18px">Tin tức</p>
        </div>
        <div class="ml-auto d-flex">
            <div>
                <a onclick="share_click(600, 600, 'fb')" class="ml-3 me-3">
                    <svg width="9" height="16" viewBox="0 0 9 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M8.30769 0.115385V2.65385H6.79808C6.2468 2.65385 5.875 2.76923 5.68269 3C5.49039 3.23077 5.39423 3.57692 5.39423 4.03846V5.85577H8.21154L7.83654 8.70192H5.39423V16H2.45192V8.70192H0V5.85577H2.45192V3.75962C2.45192 2.56731 2.78526 1.64263 3.45192 0.985577C4.11859 0.328526 5.00641 0 6.11539 0C7.05769 0 7.78846 0.0384615 8.30769 0.115385Z" fill="#ABABAB" />
                    </svg>
                </a>
                <!-- <a href="" class="mr-3">
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M15.6657 4.28654C15.4816 3.59382 14.9395 3.04814 14.2511 2.86304C13.0033 2.52637 8 2.52637 8 2.52637C8 2.52637 2.99672 2.52637 1.74905 2.86287C1.06055 3.04797 0.5184 3.59365 0.334484 4.28637C2.98023e-08 5.54228 0 8.16207 0 8.16207C0 8.16207 2.98023e-08 10.782 0.334316 12.0378C0.518232 12.7305 1.06038 13.276 1.74888 13.4613C2.99672 13.7978 8 13.7978 8 13.7978C8 13.7978 13.0035 13.7978 14.2511 13.4613C14.9396 13.276 15.4816 12.7305 15.6657 12.0378C16 10.7819 16 8.16207 16 8.16207C16 8.16207 16 5.54228 15.6657 4.28654ZM6.36362 10.5407V5.78346L10.5455 8.16224L6.36362 10.5407Z" fill="#ABABAB" />
                        </svg>
                    </a> -->
                <a onclick="share_click(600, 600, 'tw')">
                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M16 3.54036C15.41 3.80104 14.7794 3.97708 14.1149 4.05833C14.793 3.65208 15.3151 3.00885 15.5592 2.24375C14.9252 2.61953 14.2234 2.89375 13.474 3.03932C12.8739 2.39948 12.0195 2 11.0769 2C9.26298 2 7.79487 3.46927 7.79487 5.28047C7.79487 5.53776 7.822 5.78828 7.87963 6.02865C5.15024 5.89323 2.72939 4.58646 1.1121 2.59922C0.830685 3.08333 0.667938 3.6487 0.667938 4.24792C0.667938 5.38542 1.25111 6.39089 2.13266 6.97995C1.59017 6.96641 1.08159 6.81745 0.640814 6.57031V6.61094C0.640814 8.20208 1.77326 9.52578 3.27527 9.82708C3.00064 9.90156 2.70905 9.94219 2.41068 9.94219C2.20047 9.94219 1.99364 9.92188 1.7936 9.88125C2.21064 11.1846 3.42445 12.1326 4.86205 12.1596C3.73978 13.0398 2.32253 13.5646 0.783217 13.5646C0.518754 13.5646 0.257682 13.5477 0 13.5172C1.44776 14.4583 3.17355 15 5.02479 15C11.0701 15 14.3725 9.99974 14.3725 5.66302C14.3725 5.52083 14.3691 5.37865 14.3624 5.23984C15.0032 4.77604 15.5592 4.20052 16 3.54036Z" fill="#ABABAB" />
                    </svg>
                </a>
            </div>
        </div>
    </div>
    <div class="container goodstudy-box-detail">
        <h1><?php echo $data->title ?></h1>
        <div class="d-flex mb-4 mt-4">



        </div>
        <div class="detail-content">
            <?php echo preg_replace( '/(width|height|srcset|sizes)="[^"]*"/', '', $data->content ) ?>
        </div>
        <div class="container d-flex mt-3 mb-3">
            <?php if (!empty($list_tag)) { ?>
                <div class="d-flex col-md-9 flex-wrap">
                    <svg width="19" height="19" viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M17.1577 11.1751L11.1827 17.1501C11.0279 17.305 10.8441 17.428 10.6417 17.5118C10.4394 17.5957 10.2225 17.6389 10.0035 17.6389C9.78449 17.6389 9.56761 17.5957 9.36528 17.5118C9.16295 17.428 8.97914 17.305 8.82435 17.1501L1.66602 10.0001V1.66675H9.99935L17.1577 8.82508C17.4681 9.13735 17.6423 9.55977 17.6423 10.0001C17.6423 10.4404 17.4681 10.8628 17.1577 11.1751V11.1751Z" stroke="black" stroke-opacity="0.87" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                    <p class="ml-1">Tag:</p>
                    <div>
                        <?php foreach (@$list_tag as $item) { ?>
                            <a href="<?php echo FSText::_("index.php?module=news&view=home&task=tag&tag=$item") ?>" class="tags">
                                <?php echo $item ?>
                            </a>
                        <?php } ?>
                    </div>
                </div>
            <?php  } ?>

            <div class="col-md-3 ml-auto d-flex">
                <span class="grey--text"><?php echo FSText::_("Chia sẻ:") ?></span>
                <div>
                    <a onclick="share_click(600, 600, 'fb')" class="ml-3 mr-3">
                        <svg width="9" height="16" viewBox="0 0 9 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8.30769 0.115385V2.65385H6.79808C6.2468 2.65385 5.875 2.76923 5.68269 3C5.49039 3.23077 5.39423 3.57692 5.39423 4.03846V5.85577H8.21154L7.83654 8.70192H5.39423V16H2.45192V8.70192H0V5.85577H2.45192V3.75962C2.45192 2.56731 2.78526 1.64263 3.45192 0.985577C4.11859 0.328526 5.00641 0 6.11539 0C7.05769 0 7.78846 0.0384615 8.30769 0.115385Z" fill="#ABABAB" />
                        </svg>
                    </a>
                    <!-- <a href="" class="mr-3">
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M15.6657 4.28654C15.4816 3.59382 14.9395 3.04814 14.2511 2.86304C13.0033 2.52637 8 2.52637 8 2.52637C8 2.52637 2.99672 2.52637 1.74905 2.86287C1.06055 3.04797 0.5184 3.59365 0.334484 4.28637C2.98023e-08 5.54228 0 8.16207 0 8.16207C0 8.16207 2.98023e-08 10.782 0.334316 12.0378C0.518232 12.7305 1.06038 13.276 1.74888 13.4613C2.99672 13.7978 8 13.7978 8 13.7978C8 13.7978 13.0035 13.7978 14.2511 13.4613C14.9396 13.276 15.4816 12.7305 15.6657 12.0378C16 10.7819 16 8.16207 16 8.16207C16 8.16207 16 5.54228 15.6657 4.28654ZM6.36362 10.5407V5.78346L10.5455 8.16224L6.36362 10.5407Z" fill="#ABABAB" />
                        </svg>
                    </a> -->
                    <a onclick="share_click(600, 600, 'tw')">
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M16 3.54036C15.41 3.80104 14.7794 3.97708 14.1149 4.05833C14.793 3.65208 15.3151 3.00885 15.5592 2.24375C14.9252 2.61953 14.2234 2.89375 13.474 3.03932C12.8739 2.39948 12.0195 2 11.0769 2C9.26298 2 7.79487 3.46927 7.79487 5.28047C7.79487 5.53776 7.822 5.78828 7.87963 6.02865C5.15024 5.89323 2.72939 4.58646 1.1121 2.59922C0.830685 3.08333 0.667938 3.6487 0.667938 4.24792C0.667938 5.38542 1.25111 6.39089 2.13266 6.97995C1.59017 6.96641 1.08159 6.81745 0.640814 6.57031V6.61094C0.640814 8.20208 1.77326 9.52578 3.27527 9.82708C3.00064 9.90156 2.70905 9.94219 2.41068 9.94219C2.20047 9.94219 1.99364 9.92188 1.7936 9.88125C2.21064 11.1846 3.42445 12.1326 4.86205 12.1596C3.73978 13.0398 2.32253 13.5646 0.783217 13.5646C0.518754 13.5646 0.257682 13.5477 0 13.5172C1.44776 14.4583 3.17355 15 5.02479 15C11.0701 15 14.3725 9.99974 14.3725 5.66302C14.3725 5.52083 14.3691 5.37865 14.3624 5.23984C15.0032 4.77604 15.5592 4.20052 16 3.54036Z" fill="#ABABAB" />
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>

</main>

<script>
    function share_click(width, height, type) {
        var leftPosition, topPosition;
        //Allow for borders.
        leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
        //Allow for title and status bars.
        topPosition = (window.screen.height / 2) - ((height / 2) + 50);
        var windowFeatures = "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";
        u = location.href;
        t = document.title;
        if (type === 'fb') {
            window.open('http://www.facebook.com/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t), 'sharer', windowFeatures);
        }
        if (type === 'tw') {
            window.open('http://twitter.com/intent/tweet?url=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t), 'sharer', windowFeatures);
        }
        return false;
    }
</script>