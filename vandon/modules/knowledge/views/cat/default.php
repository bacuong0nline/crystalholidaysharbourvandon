<?php
global $tmpl;
$tmpl->addStylesheet('cat', 'modules/news/assets/css');
// $tmpl->addScript('home', 'modules/news/assets/js');
$cr = date_create(date('Y-m-d H:i:s'));

?>

<div>
    <?php echo $tmpl->load_direct_blocks('banners', array('style' => 'style4', 'category_id_image' => 4)) ?>
    <div>
        <div class="block-news">
            <div class="container">
                <?php if (!empty($list)) { ?>
                    <div>
                        <div class="title-cat-news">
                            <h2><?php echo $cat->name ?></h2>
                        </div>
                        <div class="grid-box">

                            <?php foreach ($list as $key1 => $val) {
                                $tag = $this->model->get_record_by_id($val->category_id, FSTable::_('fs_knowledge_categories', 1))->tag;

                            ?>
                                <?php echo $tmpl->knowledge_item($val, $tag) ?>
                            <?php } ?>
                        </div>

                    </div>
                <?php } ?>
            </div>

        </div>
    </div>
</div>