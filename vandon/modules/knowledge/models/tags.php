<?php

class NewsModelsTags extends FSModels
{
    function __construct()
    {
        $limit = 11;
        $page = FSInput::get('page');
        $this->limit = $limit;
        $this->page = $page;
        $fstable = FSFactory::getClass('fstable');
        $this->table_name = $fstable->_('fs_news', 1);
        $this->table_category = $fstable->_('fs_news_categories', 1);
        $this->table_comment = $fstable->_('fs_news_comments', 1);
    }

    /*
     * get Category current
     */
    function get_category_by_id($category_id)
    {
        if (!$category_id)
            return "";
        global $db;
        $builder = $db->table($this->table_category);
        $builder->where('id', $category_id);
        $query = $builder->get();
        $result = $query->getResult();
        return $result;
    }

    /*
     * get Article
     */

    function getNews($tag_name)
    {
        global $db;

        $query = "SELECT id,title,alias,image,created_time,summary
                  FROM $this->table_name
                  WHERE published = 1 and (tag_alias = '$tag_name' OR tag_alias LIKE '%,".$tag_name.",%')
                  ORDER BY created_time desc
                  ";
//        $page = FSInput::get('page');
//
//        if ($page) {
//            $builder->limit($this->limit, ($this->page - 1) * $this->limit);
//        } else{
//            $builder->limit($this->limit, 0);
//        }
        $sql = $db->query_limit($query, $this->limit, $this->page);
        $result = $db->getObjectList();
        return $result;
    }

    function getTotal($tag_name)
    {
        global $db;
        $query = "SELECT count(*)
                  FROM $this->table_name
                  WHERE published = 1 and (tag_alias = '$tag_name' OR tag_alias LIKE '%,".$tag_name.",%')
                  ";
        $sql = $db->query($query);
        $total = $db->getResult();
        return $total;
    }

    function getPagination($total = null)
    {
        FSFactory::include_class('Pagination');
        $pagination = new Pagination($this->limit, $total, $this->page);
        return $pagination;
    }
}

?>