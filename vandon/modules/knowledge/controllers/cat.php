<?php


class KnowledgeControllersCat extends FSControllers
{
	function display()
	{
		// call models
		$model = $this->model;
		// $list_cat = $model->get_records('published = 1 and level = 0', FSTable::_("fs_knowledge_categories", 1));
		$cat = $model->get_record('published = 1 and id = "'.FSInput::get("cid").'" ', FSTable::_("fs_knowledge_categories", 1));

		$list = $model->get_list_news($cat->id);

		global $tmpl;
		$tmpl->set_seo_special();

		// call views			
		include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
	}

	function load_more()
	{
		// call models
		$model = $this->model;
		$list = $model->load_more();
		$limit = FSInput::get('data-limit');
		if ($list) {
			include 'modules/' . $this->module . '/views/' . $this->view . '/loadmore.php';
			if (count($list) < $limit) {
				echo '
                        <script type="text/javascript">
                            $(document).ready(function () {
                                 $(".col-btn-show-more").fadeOut();
                            })
                        </script>
                ';
			}
		} else echo 0;
	}
	function tag() {
		$model = $this->model;
		$where = '';
		if (FSInput::get('tag')) {
            $where .= 'AND tags like "%'.FSInput::get('tag').'%"';
        }
        $fs_table = FSFactory::getClass('fstable');
		// $query_body = $model->set_query_body();
		$query = " FROM " . $fs_table->getTable('fs_news', 1) . "
		WHERE 
					published = 1
					" . $where .
		" ORDER BY created_time ASC, id DESC 
			";
		// echo $where;die;
		$list = $model->get_list($query);

		$total = $model->getTotal($query);
		$pagination = $model->getPagination($total);
		
		include 'modules/' . $this->module . '/views/' . $this->view . '/tag.php';

	}
	function load_right()
	{
		//            $page = FSInput::get('page');
		$next_page = FSInput::get('next_page');
		//            $list_name = FSInput::get('list');
		$cat = FSInput::get('cat');
		$list_news_pro = $this->model->get_list_right($next_page, $cat);
		if ($list_news_pro) {
			include 'modules/' . $this->module . '/views/' . $this->view . '/loadright.php';
		} else echo 0;
	}
}
