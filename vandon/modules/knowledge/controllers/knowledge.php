<?php
/*
 * Huy write
 */
// controller

class KnowledgeControllersKnowledge extends FSControllers
{
    var $module;
    var $view;
    function display()
    {
        // call models
        $model = $this->model;

        $data = $model->getNews();
        $categories = $model->get_records(' published = 1 ', 'fs_knowledge_categories', 'id,name,alias');
        if (!$data)
            setRedirect(URL_ROOT, 'Không tồn tại bài viết này', 'Error');
        $ccode = FSInput::get('ccode');
        $list = $model->get_list($data->category_id);

        $list_tag = explode(',', $data->tags);

        foreach($list_tag as $key => $item) {
            $list_tag[$key] = trim($item, ' ');
        };
        $list_tag = $this->cleanArray($list_tag);


        $fstable = FSFactory::getClass('fstable');
        $cat_parent = $this->model->get_record('id in (0' . $data->category_id_wrapper . '0) and level = 0 ', $fstable->_('fs_knowledge_categories', 1));

        

        $arr_bread = substr($data->category_id_wrapper, 1, -1);
        $arr_bread = explode(',', $arr_bread);
        $arr_bread = array_reverse($arr_bread);
        foreach ($arr_bread as $br) {
            $cat_bread = $this->model->get_record('id = ' . $br, 'fs_knowledge_categories', 'id,alias,name');
            $breadcrumbs[] = array(0 => $cat_bread->name, 1 => FSRoute::_('index.php?module=news&view=cat&ccode=' . $cat_bread->alias . '&cid=' . $cat_bread->id));
        }
        $breadcrumbs[] = array(0 => FSText::_($data->title), 1 => '');

        global $tmpl, $module_config;
        $tmpl->assign('title', $data->title);
        $tmpl->assign('tags', $data->tags);
        $tmpl->assign('description', $data->content);
        $tmpl->assign('og_image', URL_ROOT . $data->image);

        // seo
        $tmpl->set_data_seo($data);
        $this->update_hits();
        // call views			
        include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
    }

    // check captcha
    function check_captcha()
    {
        $captcha = FSInput::get('txtCaptcha');

        if ($captcha == $_SESSION["security_code"]) {
            return true;
        } else {
        }
        return false;
    }

    function rating()
    {
        $model = $this->model;
        if (!$model->save_rating()) {
            echo '0';
            return;
        } else {
            echo '1';
            return;
        }
    }
    function count_views()
    {
        $model = $this->model;
        if (!$model->count_views()) {
            echo 'hello';
            return;
        } else {
            echo '1';
            return;
        }
    }
    // update hits
    function update_hits()
    {
        $model = $this->model;
        $news_id = FSInput::get('id');
        $model->update_hits($news_id);
    }

    function save_comment()
    {
        $model = $this->model;

        $row = array();
        $row['name'] = addslashes(FSInput::get('fullname'));
        $row['email'] = addslashes(FSInput::get('email'));
        $row['comment'] = addslashes(FSInput::get('content'));
        $row['news_id'] = addslashes(FSInput::get('id'));

        if (FSInput::get('comment_id')) {
            $row['comment_id'] = addslashes(FSInput::get('comment_id'));
        }
        $_SESSION['comment_guest'] = $row;

        $id = $model->save_comment2();
        if (!$id) {
            setRedirect(URL_ROOT, 'Có lỗi xảy ra, bạn vui lòng thử lại', 'error');
        }
        if ($id) {
            $link = FSInput::get('link');
            setRedirect($link, 'Bình luận thành công, vui lòng chờ xét duyệt');
        }

        unset($_SESSION['comment_guest']);
    }

    function replyComment()
    {
        global $user;
        $userInfo = $user->userInfo;
        $name = $userInfo->full_name;
        $text = FSInput::get('text');
        $record_id = FSInput::get('record_id', 0, 'int');
        $parent_id = FSInput::get('parent_id', 0, 'int');
        $user_id = FSInput::get('user_id', 0, 'int');

        if (!$name ||  !$text || !$record_id) {
            echo 0;
            return false;
        }

        $time = date('Y-m-d H:i:s');
        $row['name'] = $name;
        $row['user_id'] = $user_id;
        $row['comment'] = $text;
        $row['record_id'] = $record_id;
        $row['parent_id'] = $parent_id;
        // $row ['published'] = 1;
        $row['readed'] = 1;
        $row['created_time'] = $time;
        $row['edited_time'] = $time;
        $row['replied'] = 1;
        $rs = $this->model->_add($row, 'fs_products_comments');
        echo $rs ? 1 : 0;
        if ($rs) {
            $row3['replied'] = 1;
            $this->model->_update($row3, 'fs_products_comments', ' id = ' . $parent_id);
        }
        if ($rs)
            $this->recal_comments($record_id);
        setRedirect(URL_ROOT, 'Cảm ơn bạn đã nhận xét', 'success');
        // return $rs;
    }
}
