<?php


class ProjectControllersHome extends FSControllers
{
	function display()
	{
		// call models
		$model = $this->model;

		$where = '';

		$banner = $this->model->get_record('category_id = 1', 'fs_banners');
		$list = $this->model->get_records('published = 1 and category_id = 1', 'fs_project');
		$projects_hot = $this->model->get_records('published = 1 and is_hot = 1 and category_id != 1', 'fs_project', '*', 'ordering asc');
		// print_r($project_hot);die;

		include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
	}
}
