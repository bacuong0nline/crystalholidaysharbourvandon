<?php

use Guzzle\Http\Url;

	class ProjectControllersProject extends FSControllers
	{
		function display()
		{			
			$model = $this -> model;
      $id = FSInput::get('id');
      $data = $model->get_record('published = 1 and id = '.$id.'', FSTable::_('fs_project', 1));
      if(!$data) {
        setRedirect(URL_ROOT, 'Không tồn tại bài viết này', 'error');
      }
			// $data_city = $model->get_record_by_id($data->city_id, 'fs_local_cities');
			// $cat = $model->get_record_by_id($data->category_id, FSTable::_("fs_project_categories", 1));
			// $cat_parent = $model->get_record('id in (0'.$cat->list_parents.'0) and level = 0', FSTable::_("fs_project_categories", 1));
			$images = $model->get_records('record_id = '.$id.'', FSTable::_('fs_project_images',1), '*', 'ordering asc, id asc', '5');
			$specials = $model->get_records('id in (0'.$data->special_point.'0)', FSTable::_("fs_complex_special_point", 1));


			// $list_cmt = $this->model->get_comments($data->id, $this->model->limit);
			
      // $this->update_hits();

      // if(!$data) {
      //   setRedirect(URL_ROOT, FSText::_('Sản phẩm chưa được kích hoạt'));
      // }

      if($data->category_id != 1) {
        $relate_project_list = $model->get_products_related($data->products_related);
      } else {
        $relate_project_list = $model->getRelateProjectList($data->id);
      }

        
      // $relate_news_list_2 = $model->getRelateNewsList2($data->category_id);
      
			include 'modules/'.$this->module.'/views/'.$this->view.'/default.php';
		}
    function update_hits()
    {
        $model = $this->model;
        $news_id = FSInput::get('id');
        $model->update_hits($news_id);
    }

		function set_comment()
    {
        $model = $this->model;

        $row = array();
        $row['name'] = addslashes(FSInput::get('name-guest'));

        $row['comment'] = addslashes(FSInput::get('comment'));
        $row['project_id'] = addslashes(FSInput::get('id-news'));
        $row['record_id'] = addslashes(FSInput::get('id-news'));

        $time = date("Y-m-d H:i:s");
        $row['created_time'] = $time;
  
        $data_prod = $this->model->get_record_by_id($row['project_id'], FSTable::_('fs_project',1));
        $row2['comments_total'] = $data_prod->comments_total + 1;
        $this->model->_update($row2, FSTable::_('fs_project',1), 'id = '.$row['project_id'].'');

        $id = $this->model->_add($row, FSTable::_('fs_project_comments', 1));

        if($id) {
            setRedirect(URL_ROOT, FSText::_("Cảm ơn bạn đã gửi bình luận"));
        }
    }
	}
