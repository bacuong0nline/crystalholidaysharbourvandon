<?php
global $tmpl, $config;
$tmpl->addStylesheet('default', 'modules/project/assets/css');
$tmpl->addStyleSheet("flickity", "templates/default/css");
$tmpl->addStylesheet('fancybox', 'templates/default/css');

$tmpl->addScript("fancybox", 'templates/default/js');
$tmpl->addScript("flickity", "templates/default/js");
$tmpl->addScript('jquery.toc', 'modules/news/assets/js');

$tmpl->addScript('project', 'modules/project/assets/js');
$tmpl->addScript('home', 'modules/project/assets/js');

// print_r($data);die;

?>

<div>
  <div class="banner-project" style="background-size: cover !important; background: url(<?php echo format_image($data->image2, 'banner') ?>), linear-gradient(106.76deg, #005176 -21.91%, #024d75 -2.34%, #094271 15.87%, #142f6b 33.53%, #241562 50.77%, #280f60 54.07%, #2b0f63 64.22%, #350d6e 72.98%, #450b80 81.23%, #5c0899 89.17%, #7905b9 96.88%, #9c00e0 104.31%, #9f00e3 104.81%, #5d0091 125.19%); ">
    <div class="container container-2">
      <div class="title-project-detail">
        <?php if ($data->category_id == 1) { ?>
          <h1 class="title-american-2 text-white text-uppercase"><?php echo $data->title ?></h1>
        <?php } ?>
        <?php if ($data->summary2) { ?>
          <div class="text-white"><?php echo $data->summary2 ?></div>
        <?php } ?>
        <!-- <p class="text-white text-left">Căn hộ siêu cao cấp dành cho KH VIP(có bể bơi hoặc bể sục)</p> -->
        <!-- <a class="btn-style-1 text-uppercase m-auto ms-md-0 btn-register-mobile">Đăng ký ngay</a> -->
      </div>
    </div>
  </div>
</div>
<div class="content-wrapper">
  <div class="container container-2">
    <div class="content d-flex flex-column flex-md-row">
      <div class="left text-white">
        <h2 class="title-american-2 text-uppercase pt-3 pt-0">Thông tin chi tiết</h2>
        <?php if ($data->category_id != 1) { ?>
          <h1 class="title-american-3 text-white text-uppercase"><?php echo $data->title ?></h1>
        <?php } ?>
        <div class="content-project">
          <?php echo $data->content ?>
        </div>
        <div class="more-icons">
          <?php foreach ($specials as $item) { ?>
            <div class="d-flex align-items-center">
              <img src="<?php echo $item->image ?>" alt="<?php echo $item->image ?>">
              <span class="ms-3"><?php echo $item->title ?></span>
            </div>
          <?php } ?>
        </div>
      </div>
      <div class="right" data-fancybox href="<?php echo $data->image3  ?>">
        <img src="<?php echo format_image($data->image3, 'banner') ?>" alt="<?php echo format_image($data->image) ?>">
      </div>
    </div>
  </div>
  <?php if (!empty($images)) { ?>
    <div class="container container-2 mt-5 pb-5">
      <h3 class="title-american text-white text-center title-library">Thư viện</h3>
      <div class="desktop">
        <div class="grid-container gallery-box central image-box">
          <?php foreach ($images as $key => $item) {  ?>
            <div class="central-item item<?php echo $key ?> specialized-training-item" href="<?php echo $item->image ?>">
              <img src="<?php echo format_image($item->image) ?>" alt="<?php echo format_image($item->image) ?>">
            </div>
          <?php } ?>
        </div>
      </div>
      <div class="mobile">
        <div class="d-flex justify-content-between image-box">
          <?php foreach ($images as $key => $item) {  ?>
            <?php if ($key < 2) { ?>
              <div style="width: 49%" class="item<?php echo $key ?> " href="<?php echo $item->image ?>">
                <img style="width: 100%" src="<?php echo format_image($item->image) ?>" alt="<?php echo format_image($item->image) ?>">
              </div>
            <?php } ?>
          <?php } ?>
        </div>
        <div style="width: 100%; margin-top: 10px; margin-bottom: 10px" class="image-box item<?php echo $key ?> " href="<?php echo $item->image ?>">
          <img style="width: 100%" src="<?php echo format_image($item->image) ?>" alt="<?php echo format_image($item->image) ?>">
        </div>
        <div class="d-flex justify-content-between image-box">
          <?php foreach ($images as $key => $item) {  ?>
            <?php if ($key > 2 && $key < 5) { ?>
              <div style="width: 49%" class="item<?php echo $key ?> " href="<?php echo $item->image ?>">
                <img style="width: 100%" src="<?php echo format_image($item->image) ?>" alt="<?php echo format_image($item->image) ?>">
              </div>
            <?php } ?>
          <?php } ?>
        </div>
      </div>
    </div>
  <?php } ?>
  <?php if (!empty($relate_project_list)) { ?>
    <div class="section section-6 padding-section-6 pb-5 mt-3">
      <div class="container">
        <div style="position: relative; z-index: 1">
          <h2 class="title-american text-white text-uppercase text-center">Các loại căn hộ</h2>
        </div>
      </div>
      <div class="container-6 mt-5">
        <div class="owl-carousel owl-theme" id="ground-box">
          <?php foreach ($relate_project_list as $item) { ?>
            <div>
              <a title="<?php echo $item->title ?>" style="display: block" class="ground-item" href="<?php echo FSRoute::_("index.php?module=project&view=project&code=$item->alias&id=$item->id") ?>">
                <img src="<?php echo format_image($item->image, 'banner') ?>" alt="<?php echo format_image($item->image) ?>">
                <div class="description-summary text-white">
                </div>
                <span class="seemore">Xem thêm</span>
              </a>
              <h3 style="color: white; text-align: center; padding-top: 30px" class="title"><?php echo $item->title ?></h3>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  <?php } ?>
</div>

<script>
  window.onload = function() {
    let elements = document.getElementsByClassName('image-box');

    for (let item of elements) {
      lightGallery(item, {
        thumbnail: true,
        animateThumb: true,
        // zoomFromOrigin: false,
        // allowMediaOverlap: true,
        toggleThumb: true,
        plugins: [lgThumbnail]
      })
    }

    $('[data-fancybox]').fancybox({
      buttons: [
        "zoom",
        //"share",
        "slideShow",
        "fullScreen",
        "download",
        "thumbs",
        "close"
      ],
      protect: true,
      preventCaptionOverlap: true,
    });
  }
</script>