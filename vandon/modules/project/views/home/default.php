<?php
global $tmpl;
$tmpl->addStylesheet('project', 'modules/project/assets/css');
// $tmpl->addStyleSheet("flickity", "templates/default/css");
// $tmpl->addScript("flickity", "templates/default/js");
// $tmpl->addScript("project", 'modules/project/assets/js');
$tmpl->addScript("home", 'modules/project/assets/js');

?>


<div>
  <div class="banner-project">
    <img src="<?php echo URL_ROOT . format_image($banner->image) ?>" alt="<?php echo format_image($banner->image) ?>" />
    <div class="title-project desktop">
      <h1 class="title-american text-white text-center"><?php echo $banner->title_banner ?></h1>
      <p class="text-white"><?php echo $banner->content_banner ?></p>
    </div>
  </div>
  <div class="wrapper">
    <div class="container">
      <div class="mobile">
        <h2 class="title-american project-highlight m-auto pt-3 pb-3" style="color: #1affff">SẢN PHẨM</h2>
      </div>
      <div class="desktop">
        <div class="title-box d-flex align-items-center justify-content-center">
          <span class="line me-2"></span>
          <h2 class="title-american project-highlight text-white">SẢN PHẨM MỞ BÁN</h2>
          <span class="line ms-2"></span>
        </div>
      </div>
      <div class="container">
        <div class="owl-carousel owl-theme" id="highlight-wrapper">
          <?php foreach ($projects_hot as $item) {  ?>
            <div class="highlight-box" style="width: 100%;">
              <div class="d-flex flex-column flex-md-row flex-column-reverse">
                <div class="left">
                  <div class="summary text-white d-flex align-items-center justify-content-center">
                    <div class="summary-box" style="width: 70%">
                      <h3 class="text-white title-project-highlight text-uppercase"><?php echo $item->title ?></h3>
                      <!-- <p class="fw-semibold title-2-project-highlight">Căn hộ siêu cao cấp dành cho KH VIP </p> -->
                      <div>
                        <?php echo $item->summary ?>
                      </div>
                      <a href="<?php echo FSRoute::_("index.php?module=project&view=project&code=$item->alias&id=$item->id") ?>" class="m-auto m-md-0 mt-4 text-uppercase <?php echo IS_MOBILE ? 'btn-style-1' : 'btn-style-2' ?>">Xem chi tiết</a>
                    </div>
                  </div>
                </div>
                <a class="right" style="display: block;" href="<?php echo FSRoute::_("index.php?module=project&view=project&code=$item->alias&id=$item->id") ?>">
                  <img src="<?php echo $item->image ?>" alt="<?php echo $item->image ?>">
                </a>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>

      <div class="desktop">
        <div class="title-box d-flex align-items-center justify-content-center">
          <span class="line me-2"></span>
          <h2 class="title-american project-highlight text-white text-uppercase">Các loại căn hộ</h2>
          <span class="line ms-2"></span>
        </div>
      </div>
      <div class="mobile">
        <div class="mt-3 mb-3 d-flex align-items-center justify-content-center">
          <h2 class="title-american project-highlight text-white text-uppercase">Các loại căn hộ</h2>
        </div>
      </div>
      <div class="grid-project-home">
        <?php foreach ($list as $item) { ?>
          <a href="<?php echo FSRoute::_("index.php?module=project&view=project&code=$item->alias&id=$item->id") ?>" class="project-item">
            <span class="view-detail">Xem chi tiết</span>
            <img onerror="this.src='/images/no-image.png'" src="<?php echo format_image($item->image); ?>" alt="<?php echo format_image($item->image); ?>" />
            <p class="text-uppercase fw-bold text-center title-project" style="letter-spacing: 0.1em; margin-top: 10px;"><?php echo $item->title ?></p>
          </a>
        <?php } ?>
      </div>
    </div>
  </div>
</div>