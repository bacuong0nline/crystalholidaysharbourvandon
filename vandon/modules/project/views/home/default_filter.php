<?php
global $tmpl;
$tmpl->addStylesheet('project', 'modules/project/assets/css');
$tmpl->addStyleSheet("flickity", "templates/default/css");
$tmpl->addScript("flickity", "templates/default/js");
$tmpl->addScript("home", 'modules/project/assets/js');

?>


<div class="container pb-5">
  <div class="d-flex justify-content-center align-items-center flex-column title-project">
    <h6 class="text-center title-project-1"><?php echo FSText::_("Xem Các Dự Án IKay Đã Thực Hiện") ?></h6>
    <p class="text-center summary-project"><?php echo FSText::_("Ngày nay tại IKAY, Chúng  tôi đang là đơn vị xây dựng, thiết kế và thi công nội thất, quy hoạch dự án có uy tín trên toàn quốc. Nhưng mọi thứ mới chỉ là bắt đầu!") ?></p>
  </div>
  <div class="d-flex justify-content-md-center align-items-center" style="grid-column-gap: 20px;">
    <div class="dropdown">
      <button class="btn dropdown-toggle all-project" type="button" data-bs-toggle="dropdown" aria-expanded="false">
        <?php
        if (FSInput::get("type")) {
          $type = $model->get_record('alias = "' . FSInput::get("type") . '"', FSTable::_('fs_project_categories', 1))->name;
          echo $type;
        } else {
          echo FSText::_("Tất cả loại hình");
        }

        ?>
      </button>
      <ul class="dropdown-menu">
        <?php
        $link1 = preg_replace("/&type=.*/", "", $_SERVER['REQUEST_URI']);
        ?>
        <li><a href="<?php echo $link1 ?>" class="dropdown-item active"><?php echo FSText::_("Tất cả dự án") ?></a></li>
        <?php foreach ($list_prj_cat as $item) { ?>
          <li><a class="dropdown-item" href="<?php echo FSRoute::addParameters('type', $item->alias); ?>"><?php echo $item->name ?></a></li>
        <?php } ?>
      </ul>
    </div>
    <div class="dropdown">
      <button class="btn dropdown-toggle all-project" type="button" data-bs-toggle="dropdown" aria-expanded="false">
        <?php
        if (FSInput::get("style")) {
          $style = $model->get_record('alias = "' . FSInput::get("style") . '"', FSTable::_('fs_project_style', 1))->title;
          echo $style;
        } else {
          echo FSText::_("Tất cả loại hình");
        }
        ?>
      </button>
      <ul class="dropdown-menu">
        <?php
        $link2 = preg_replace("/&style=.*/", "", $_SERVER['REQUEST_URI']);
        ?>
        <li><a href="<?php echo $link2 ?>" class="dropdown-item active"><?php echo FSText::_("Tất cả loại hình") ?></a></li>
        <?php foreach ($styles as $item) { ?>
          <li><a class="dropdown-item" href="<?php echo FSRoute::addParameters('style', $item->alias); ?>"><?php echo $item->title ?></a></li>
        <?php } ?>
      </ul>
    </div>
    <div class="dropdown mt-md-0">
      <button class="btn dropdown-toggle all-project" type="button" data-bs-toggle="dropdown" aria-expanded="false">
        <?php
        if (FSInput::get("cost")) {
          $cost_text = $model->get_record('alias = "' . FSInput::get("cost") . '"', FSTable::_('fs_project_cost', 1))->title;
          echo $cost_text;
        } else {
          echo FSText::_("Tất cả giá");
        }
        ?>
      </button>
      <ul class="dropdown-menu">
        <?php
        $link = preg_replace("/&cost=.*/", "", $_SERVER['REQUEST_URI']);
        ?>
        <li><a href="<?php echo $link ?>" class="dropdown-item active"><?php echo FSText::_("Tất cả giá") ?></a></li>
        <?php foreach ($cost as $item) { ?>
          <li><a class="dropdown-item" href="<?php echo FSRoute::addParameters('cost', $item->alias); ?>"><?php echo $item->title ?></a></li>
        <?php } ?>

      </ul>
    </div>
  </div>
</div>
<div class="container">
  <?php foreach ($list_prj_cat as $key => $item) { ?>
    <?php if (!empty($item->rand)) { ?>
      <div class="pt-5 pb-md-5">
        <h2><?php echo $item->name ?></h2>
        <div class="project-wrapper project-wrapper-<?php echo $key ?>">
          <?php foreach ($item->rand as $val2) { ?>
            <?php $tmpl->project_item($val2, $this->model) ?>
          <?php } ?>
        </div>
      </div>
    <?php } ?>
  <?php } ?>
</div>