<?php

class ProjectModelsProject extends FSModels {

    function __construct() {
        parent::__construct();
        global $module_config;
        $fstable = FSFactory::getClass('fstable');
        $this->table_name = $fstable->_('fs_project',1);
        $this->table_category = $fstable->_('fs_introduce_categories',1);
        $this->table_comment = $fstable->_('fs_project_comments', 1);

        $this->limit = 12;
    }



    function set_query_body() {
        $date1 = FSInput::get("date_search");
        $where = "";
        $fs_table = FSFactory::getClass('fstable');
        $query = " FROM " . $fs_table->getTable('fs_introduce',1) . "
						  WHERE 
						  	 published = 1
						  	" . $where .
                " ORDER BY created_time DESC, id DESC 
						 ";

        return $query;
    }

    function get_products_related($products_related)
	{
		if (!$products_related)
			return;
		$query   = " SELECT id, title,image, alias 
    					FROM " . $this->table_name . "
    					WHERE id IN (0" . $products_related . "0)
    					 ORDER BY POSITION(','+id+',' IN '0" . $products_related . "0')
    					";
		global $db;
		$sql = $db->query($query);
		$result = $db->getObjectList();
		return $result;
	}

    function update_hits($news_id)
    {
        if (USE_MEMCACHE) {
            $fsmemcache = FSFactory::getClass('fsmemcache');
            $mem_key = 'array_hits';

            $data_in_memcache = $fsmemcache->get($mem_key);
            if (!isset($data_in_memcache))
                $data_in_memcache = array();
            if (isset($data_in_memcache[$news_id])) {
                $data_in_memcache[$news_id]++;
            } else {
                $data_in_memcache[$news_id] = 1;
            }
            $fsmemcache->set($mem_key, $data_in_memcache, 10000);
        } else {
            if (!$news_id)
                return;

            // count
            global $db, $econfig;
            $sql = " UPDATE ".FSTable::_('fs_project')." 
						SET hits = hits + 1 
						WHERE  id = '$news_id' 
					 ";
            $db->query($sql);
            $rows = $db->affected_rows();
            return $rows;
        }
    }
    function getRelateProjectList($id)
    {
        if (!$id)
            die;

        global $db;
        $limit = 4;
        $query = ' SELECT *
                FROM fs_project
                WHERE published = 1 AND id != ' . $id . ' and category_id = 1
                ORDER BY  RAND()
				LIMIT ' . $limit;
        $db->query($query);
        $result = $db->getObjectList();

        return $result;
    }

    function getRelateNewsList2($cid)
    {
        if (!$cid)
            die;

        global $db;
        $limit = 4;
        $fs_table = FSFactory::getClass('fstable');
        $id = FSInput::get2('id', 0, 'int');
        $query = ' SELECT *
						FROM ' . $fs_table->getTable('fs_project', 1) . '
						WHERE category_id != ' . $cid . '
							AND published = 1 AND id != ' . $id . '
						ORDER BY  RAND()
						LIMIT ' . $limit;
        $db->query($query);
        $result = $db->getObjectList();

        return $result;
    }
    function get_comments($product_id, $limit = '', $page = 0)
    {
        global $db;
        if (!$product_id)
            return;

        if ($page) {
            $limit = (0 + $limit * ($page - 1)) . ',' . $limit;
        }

        if ($limit) {
            $limit = " LIMIT " . $limit;
        }
        $where = "";

        $query = " SELECT *
                    FROM " . $this->table_comment . "
                    WHERE record_id = $product_id 
                    AND published = 1 " . $where . "
                    ORDER BY created_time DESC
                    ";
        // echo $query;die;
        $db->query_limit($query, $this->limit, $page);
        $result = $db->getObjectList();

        $tree = FSFactory::getClass('tree', 'tree/');
        $list = $tree->indentRows2($result);
        // print_r($list);die;
        return $list;
    }
}

?>