$(document).ready(function () {

  $(".cat-project").click(function () {
    var ScrollPos = ($(this).children().closest('li').offset().left) + ($(this).parent().scrollLeft()) + ($(this).children().closest('li').outerWidth(true) / 2) - ($(this).parent().width() / 2);
    console.log(ScrollPos)
    $(this).parent().animate({
      scrollLeft: ScrollPos - 30
    }, 400);

    let pos = $(this).attr("data-pos");

    $(`.project-wrapper-${pos}`).hide();
  })

  $("#send-comment").click(function () {
    var submit = '#form-comment';
    if (checkFormsubmit2()) {
      $(submit).submit()
    }
  })
  $("#toc").toc({ content: ".description", headings: "h1,h2,h3,h4" });

  $('#toc a').on('click', function (e) {
    e.preventDefault();
    let target = $(this).attr('href');
    target = target.replace(/\./g, '\\.');
    $('html, body').animate({
      'scrollTop': $(target).offset().top - 50
    }, 800);
  });

  lightGallery(document.getElementById('animated-thumbnails'), {
    autoplayFirstVideo: false,
    // animateThumb: false,
    zoomFromOrigin: false,
    pager: false,
    galleryId: "nature",
    plugins: [lgZoom, lgThumbnail],
    getCaptionFromTitleOrAlt: false,
    mobileSettings: {
      controls: false,
      showCloseIcon: false,
      download: false,
      rotate: false
    }
  });


  $('img').each(function () {
    $(this).attr('data-src', $(this).attr("src"))
    $(this).attr('data-thumb-src', $(this).attr("src"))

    //    $(this).wrap(`<a data-src='${$(this).attr("src")}' ></a>`)
  })

  let elements = document.getElementsByTagName('img');

  lightGallery(document.getElementById('description'), {
    selector: 'img',
    autoplayFirstVideo: false,
    // animateThumb: false,
    zoomFromOrigin: false,
    pager: false,
    galleryId: "nature",
    plugins: [lgZoom, lgThumbnail],
    exThumbImage: 'data-src',
    getCaptionFromTitleOrAlt: false,
    mobileSettings: {
      controls: false,
      showCloseIcon: false,
      download: false,
      rotate: false
    }
  })

  let $carousel = $('.carousel-main').flickity({
    "prevNextButtons": false,
    "pageDots": false
  })

  $(".next-slide").click(function() {
    $carousel.flickity('next')
  })
  $(".prev-slide").click(function() {
    $carousel.flickity('previous')
  })
})

function checkFormsubmit2() {

  $('label.label_error').prev().remove();
  $('label.label_error').remove();

  if (!notEmpty("name-guest", "Bạn chưa nhập tên")) {
    return false;
  }


  if (!notEmpty("comment", "Bạn chưa nhập nội dung bình luận")) {
    return false;
  }

  else {

  }
  return true;
}