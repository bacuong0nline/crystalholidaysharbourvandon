$(document).ready(function () {
  // $(".carousel-project-item").flickity({
  //   prevNextButtons: false,
  //   pageDots: false
  // })

  // $(".next-slide-project-home").click(function () {
  //   $(this).next().flickity('next')
  // })
  // $(".cat-project").on("hidden.bs.tab", function () {
  //   $(".carousel-project-item").flickity('destroy')
  // });
  // $('.cat-project').on('shown.bs.tab', function (event) {
  //   $(".carousel-project-item").flickity({
  //     prevNextButtons: false,
  //     pageDots: false
  //   })
  // })

  // $(".cat-project").click(function () {
  //   var ScrollPos = ($(this).children().closest('li').offset().left) + ($(this).parent().scrollLeft()) + ($(this).children().closest('li').outerWidth(true) / 2) - ($(this).parent().width() / 2);
  //   console.log(ScrollPos)
  //   $(this).parent().animate({
  //     scrollLeft: ScrollPos - 30
  //   }, 400);

  //   let pos = $(this).attr("data-pos");

  //   $(`.project-wrapper-${pos}`).hide();
  // })


  $('#ground-box').owlCarousel({
    loop: true,
    margin: 10,

    nav: true,
    dots: false,
    center: true,
    responsive: {
      0: {
        navText: [`<svg width="18" height="22" viewBox="0 0 18 22" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M17.1113 0L0.000217438 11L17.1113 22V0Z" fill="white"/>
        </svg>        
        `, `<svg width="18" height="22" viewBox="0 0 18 22" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M0.111328 0L17.2224 11L0.111328 22V0Z" fill="white"/>
        </svg>        
        `],
        items: 1
      },
      600: {
        navText: [`<svg width="56" height="56" viewBox="0 0 56 56" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M27 38L17 27.5L27 17" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M39 28L18 28" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
        <circle r="27" transform="matrix(-1 0 0 1 28 28)" stroke="white" stroke-width="2"/>
        </svg>
        `, `<svg width="56" height="56" viewBox="0 0 56 56" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M29 38L39 27.5L29 17" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M17 28L38 28" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
        <circle cx="28" cy="28" r="27" stroke="white" stroke-width="2"/>
        </svg>
        `],
        items: 2
      }
    }
  })

  $('#highlight-wrapper').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    dots: false,
    autoplay: true,
    autoPlaySpeed: 5000,
    autoPlayTimeout: 5000,
    smartSpeed: 2000,
    responsive:{
        0:{
            items:1
        },
    }
  })

})