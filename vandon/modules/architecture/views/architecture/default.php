<?php
global $tmpl;
$tmpl->addStylesheet('detail', 'modules/architecture/assets/css');
$tmpl->addStylesheet('project', 'modules/project/assets/css');

$tmpl->addScript('select2.min', 'modules/address/assets/js', 'bottom');
$tmpl->addScript('cat', 'modules/architecture/assets/js', 'bottom');
// $tmpl->addScript('project', 'modules/project/assets/js');
$tmpl->addScript("home", 'modules/project/assets/js');

// $total = count($list);
$i = 0;

if ($data) {
  $seo_title = $data->seo_title ? $data->seo_title : $data->title;
  $seo_keyword = $data->seo_keyword ? $data->seo_keyword : $seo_title;
  $seo_description = $data->seo_description;
  $tmpl->addTitle($seo_title);
  $tmpl->addMetakey($seo_keyword);
  $tmpl->addMetades($seo_description);
}

?>
<?php echo $tmpl->load_direct_blocks('banners', array('style' => 'style3', 'category_id_image' => 2)) ?>

<div class="architecture">
  <div class="container d-flex justify-content-center align-items-center flex-column intro-partner">
    <div class="title-intro-partner">
      <h4 class="title text-center title-intro-2"><?php echo FSText::_("Được tin cậy bởi các đối tác") ?></h4>
    </div>
    <div style="width: 80%; margin: auto">
      <div class="owl-carousel owl-theme carousel-partner carousel-nav-partner">
        <?php foreach ($partners as $item) { ?>
          <div class="item">
            <img src="<?php echo str_replace('original', 'resized', $item->image) ?>" alt="<?php echo str_replace('original', 'resized', $item->image) ?>" />
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
  <div class="container home partner-wrapper">
    <div>
      <div class="d-flex flex-column-reverse flex-md-row" style="position: relative">
        <div class="left">
          <div class="container">
            <?php echo $data->content ?>
          </div>
        </div>
        <div>
          <div class="mobile pe-2 ps-2">
            <div class="title d-flex justify-content-start flex-column align-items-start">
              <h4 class="title-intro-3 text-center text-md-start"><?php echo FSText::_('Quy trình triển khai dự án') ?></h4>
              <p><?php echo FSText::_("Quy trình làm việc bài bản và đem đến những giải pháp thiết thực cho các khách hàng của IKAY") ?></p>
            </div>
          </div>
          <div class="right">
            <div class="video-box">
              <div class="video-item" data-lg-size="1280-720" data-video='{"source": [{"src":"<?php echo $data->file_upload ?>", "type":"video/mp4"}], "attributes": {"preload": false, "controls": true}}' data-pinterest-text="Pin it3" data-tweet-text="lightGallery slide  4">
                <div class="play-btn"></div>
                <img src="<?php echo str_replace('original', 'original', $data->image);  ?>" >
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="why-choose-ikay container">
    <?php echo $data->content2 ?>
  </div>
</div>


<div class="container">
  <div class="carousel-architecture" data-flickity='{"pageDots": false, "prevNextButtons": false}'>

    <?php foreach ($comments as $item) { ?>
      <div class="carousel-cell-architecture">
        <div class="comment-customer-box">
          <div class=" comment-customer">
            <div class="comment-customer-div" style="width: 70%">
              <div>
                <img class="avatar" src="<?php echo URL_ROOT .  $item->image ?>" alt="<?php echo $item->image ?>" />
              </div>
              <p class="comment"><?php echo $item->comment ?></p>
              <p class="customer"><?php echo $item->name ?></p>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
  </div>
</div>


<div class="container pb-5">
  <h2 class="text-center fw-bold"><?php echo FSText::_("Các Mẫu Nhà IKay Thiết Kế") ?></h2>
  <?php foreach ($list_prj_cat as $key => $item) { ?>
    <div class="pt-5">
      <h2><?php echo $item->name ?></h2>
      <ul class="d-flex nav nav-tabs" id="myTab-<?php echo $key ?>" style="grid-gap: 10px; padding-top: 30px; padding-bottom: 30px">
        <li class="cat-project nav-item" role="presentation">
          <span class="nav-link-2 active" id="all-project-tab-<?php echo $key ?>" data-bs-toggle="tab" data-bs-target="#all-project-<?php echo $key ?>" type="button" role="tab" aria-controls="all-project-<?php echo $key ?>" aria-selected="false">
            <?php echo FSText::_("Tất cả dự án") ?>
          </span>
        </li>
        <?php foreach ($item->child as $key2 => $val) { ?>
          <li data-pos="<?php echo $key ?>" class="cat-project nav-item" role="presentation">
            <span class="nav-link-2" id="<?php echo $val->alias ?>-tab" data-bs-toggle="tab" data-bs-target="#<?php echo $val->alias ?>" type="button" role="tab" aria-controls="<?php echo $val->alias ?>" aria-selected="false"><?php echo $val->name ?></span>
          </li>
        <?php } ?>
      </ul>
      <div class="tab-content" id="myTabContent-<?php echo $key ?>">
        <div class="tab-pane fade active show" id="all-project-<?php echo $key ?>" role="tabpanel" aria-labelledby="all-project-tab-<?php echo $key ?>">
          <div class="project-wrapper">
            <?php foreach ($item->rand as $val2) { ?>
              <?php $tmpl->project_item($val2, $this->model) ?>
            <?php } ?>
          </div>
          <a href="<?php echo FSRoute::_("index.php?module=project&view=cat&ccode=$item->alias&cid=$item->id&Itemid=78") ?>" class="btn-style-3 m-auto"><?php echo FSText::_("Xem thêm") ?></a>
        </div>
        <?php foreach ($item->child as $key => $val) { ?>
          <div class="tab-pane fade" id="<?php echo $val->alias ?>" role="tabpanel" aria-labelledby="<?php echo $val->alias ?>-tab">
            <div class="project-wrapper">
              <?php foreach ($val->prjs as $val2) { ?>
                <?php $tmpl->project_item($val2, $this->model) ?>
              <?php } ?>
            </div>
            <?php if (!empty($val->prjs)) { ?>
              <a href="<?php echo FSRoute::_("index.php?module=project&view=cat&ccode=$val->alias&cid=$val->id&Itemid=78") ?>" class="btn-style-3 m-auto"><?php echo FSText::_("Xem thêm") ?></a>
            <?php } ?>
          </div>
        <?php } ?>
      </div>

    </div>
  <?php } ?>
</div>