<?php

class AddressControllersAddress extends FSControllers
{
    var $module;
    var $view;

    function display()
    {
        $query_body = $this->model->set_query_body();
        $data = false;
        $list = $this->model->get_list($query_body);
        $dataCity = $this->model->get_District();
//        $district = $this->model->get_categories_tree();

        $central_system = $this->model->get_central_system();
        // print_r($central_system);die; 
        if (empty($_SESSION['city'])) {
            $query_body = $this->model->set_query_body(1);//ha noi
            $location_capital = $this->model->get_list($query_body);
        }
        unset($_SESSION["selected_central"]);

        include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
    }

    function detial()
    {
        $data = $this->model->getData();
        $query_body = $this->model->set_query_body();
        $list = $this->model->get_list($query_body);
        $dataCity = $this->model->get_District();
        $district = $this->model->get_categories_tree();
        $breadcrumbs = array();
        $breadcrumbs[] = array(0 => 'Showroom', 1 => '');
        global $tmpl;
        $tmpl->assign('breadcrumbs', $breadcrumbs);
        include 'modules/' . $this->module . '/views/' . $this->view . '/default.php';
    }

    function get_agency()
    {
        $city_id = FSInput::get('district_id');
        if ($city_id == 0) {
            unset($_SESSION["district"]);
        } else {
            $_SESSION["district"] = $city_id;
        }
    }

    function loadDistricts()
    {
        $city_id = FSInput::get('city_id');
        $_SESSION["city"] = $city_id;
        global $config;
        $listDistricts = $this->model->getListDistricts($city_id);
        $html = '';
        foreach ($listDistricts as $item) {
            $html .= '<option  value="' . $item->id . '">' . $item->name . '</option>';
        }
        unset($_SESSION["selected_central"]);
        echo $html;
    }
    function loadSelectedCentral()
    {
        $id = FSInput::get('id');
        $_SESSION["selected_central"] = $id;
        $city_id = FSInput::get('city_id');
        $_SESSION["city"] = $city_id;
        global $config;
        $listDistricts = $this->model->getListDistricts($city_id);
        $html = '';
        foreach ($listDistricts as $item) {
            $html .= '<option  value="' . $item->id . '">' . $item->name . '</option>';
        }
        echo $html;
    }
    function loadADDStore()
    {
        $id = FSInput::get('id');

        global $config;
        $infoStore = $this->model->get_record('id=' . $id, 'fs_address');
        $html = '';

        $html .= '<p>' . $infoStore->address . '<p>';

        echo $html;
        return true;
    }
}