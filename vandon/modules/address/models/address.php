<?php

class AddressModelsAddress extends FSModels
{
    function __construct()
    {
        parent::__construct();
        $this->limit = 200;
    }

    function set_query_body($city_id = 0)
    {
        $id = FSInput::get("city_id");
        $where = "";
        if ($id) {
            $_SESSION["city"] = $id;
        }
        if (!empty($_SESSION["city"])) {
            $where .= " and district=" . $_SESSION["city"];
        }
        if (!empty($_SESSION["district"])) {
            $where .= " and district=" . $_SESSION["district"];
        }
        if (!empty($_SESSION["selected_central"])) {
            $where .= " and id=" . $_SESSION["selected_central"];
        }
        if ($city_id) {
            $where .= " and province=" . $city_id;
        }
        $fs_table = FSFactory::getClass('fstable');
        $query = " FROM " . $fs_table->getTable('fs_address') . "
        WHERE  published = 1 " . $where . " ORDER BY  ordering ASC, id DESC";
        // echo $query;die;
        return $query;
    }


    /*
    * get Category current
    * By Id or By code
    */
    function get_category()
    {
        $fs_table = FSFactory::getClass('fstable');
        $code = FSInput::get('ccode');
        if ($code) {
            $where = " AND alias = '$code' ";
        } else {
            $id = FSInput::get('id', 0, 'int');
            if (!$id)
                die('Not exist this url');
            $where = " AND id = '$id' ";
        }
        $query = " SELECT *
        FROM " . $fs_table->getTable('fs_faq_categories') . " 
        WHERE published = 1 " . $where;
        global $db;
        $sql = $db->query($query);
        $result = $db->getObject();
        return $result;
    }

    function get_info_other()
    {
        $fs_table = FSFactory::getClass('fstable');
        $query = " SELECT *
        FROM " . $fs_table->getTable('fs_address_other') . " 
        ";
        global $db;
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    function get_list($query_body)
    {
        if (!$query_body)
            return;

        global $db;
        $query = " SELECT *";
        $query .= $query_body;
        $sql = $db->query_limit($query, $this->limit, $this->page);
        $result = $db->getObjectList();

        return $result;
    }
    function get_central_system()
    {
        global $db;
        $query = " SELECT id, title, image, alias FROM fs_central";
        $sql = $db->query_limit($query, 5, $this->page);
        $result = $db->getObjectList();

        return $result;
    }
    function getTotal($query_body)
    {
        if (!$query_body)
            return;
        global $db;
        $query = "SELECT count(*)";
        $query .= $query_body;
        $sql = $db->query($query);
        $total = $db->getResult();
        return $total;
    }

    function getPagination($total)
    {
        FSFactory::include_class('Pagination');
        $pagination = new Pagination($this->limit, $total, $this->page);
        return $pagination;
    }

    function get_categories_tree()
    {
        global $db;
        $id = FSInput::get("city_id");
        $where = "";
        if (!empty($_SESSION["city"])) {
            $where .= " and city_id=" . $_SESSION["city"];
        }
        $query = " SELECT DISTINCT p.* 
                     from fs_address a 
                     INNER JOIN fs_districts p on a.district=p.id  
                     where 1=1 $where 
                     order by a.ordering asc";
        $sql = $db->query($query);
        $result = $db->getObjectList();
        $tree = FSFactory::getClass('tree', 'tree/');
        $list = $tree->indentRows2($result);
        return $result;
    }

    function get_District()
    {
        global $db;
        $query = " SELECT DISTINCT p.* from fs_address a  INNER JOIN fs_districts p on a.district=p.id
            where a.published = 1
          ORDER BY ordering asc ";
        $sql = $db->query($query);
        $list = $db->getObjectList();
        return $list;
    }

    function getListDistricts($city_id = 0)
    {
        global $db;
        $sqlWhere = '';
        if ($_SESSION["city"]) {
            $sqlWhere .= " and city_id=" . $_SESSION["city"];
        }
        if ($city_id)
            $sqlWhere = ' AND city_id = "' . $city_id . '"';
        $query = '  SELECT id, name,alias
        FROM fs_districts 
        WHERE published = 1 ' . $sqlWhere . '
        ORDER BY ordering ASC';
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    function getData()
    {
        global $db;
        $code = FSInput::get('code');
        if ($code) {
            $where = ' AND alias = \'' . $code . '\'';
        } else {
            $id = FSInput::get('id', 0, 'int');
            $where = ' AND id = ' . $id;
        }
        $query = '  SELECT *
                    FROM fs_address
                    WHERE published = 1 ' . $where;
        $sql = $db->query($query);
        $result = $db->getObject();
        return $result;
    }
}