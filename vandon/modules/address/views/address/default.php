<?php
global $tmpl;
// $tmpl->addStylesheet('select2.min.css', 'modules/address/assets/css');
$tmpl->addStylesheet('default', 'modules/address/assets/css');
//$tmpl->addStylesheet('jquery.mCustomScrollbar', 'modules/address/assets/css');
//$tmpl->addScript('jquery.mCustomScrollbar.concat.min', 'modules/address/assets/js');
// $tmpl->addScript('select2.min', 'modules/address/assets/js', 'bottom');
$tmpl->addScript('cat', 'modules/address/assets/js', 'bottom');
$total = count($list);
$i = 0;

if ($data) {
    $seo_title = $data->seo_title ? $data->seo_title : $data->name;
    $seo_keyword = $data->seo_keyword ? $data->seo_keyword : $seo_title;
    $seo_description = $data->seo_description ? $data->seo_description : $data->address;
    $tmpl->addTitle($seo_title);
    $tmpl->addMetakey($seo_keyword);
    $tmpl->addMetades($seo_description);
}

?>
<style>
    .mapContainer,
    #myMap {
        position: relative;
        width: 100%;
        /* height: 400px; */
    }

    .customNavBar {
        position: absolute;
        top: 10px;
        left: 10px;
    }
</style>


<?php if ($tmpl->count_block('default-position')) { ?>
    <?php $tmpl->load_position('default-position'); ?>
<?php } ?>


<!-- Breadcrumb -->
<?php //$tmpl->load_direct_blocks('breadcrumbs'); 
?>

<?php if ($tmpl->count_block('top_default')) { ?>
    <?php $tmpl->load_position('top_default'); ?>
<?php } ?>
<div class="c-box_store">
    <div class="snowflake1" style="width: 100%; background-size: 100%; position: relative; padding-bottom: 200px;">
        <div class="container pb-3">
            <p class="text-uppercase text-center grey--text"><?php echo FSText::_("Hệ thống trung tâm") ?></p>
            <h5 class="title text-center text-uppercase mb-4"><?php echo FSText::_("hệ thống trung tâm vmg") ?></h5>
            <div class="d-flex justify-content-center">
                <p class="text-center summary-address" style="width: 70%"><?php echo FSText::_("Click chọn và khám phá ngay các trung tâm VMG English gần khu vực bạn nhất. Chúng tôi sẵn sàng chờ đón và phục vụ cho mọi nhu cầu của người học.") ?></p>
            </div>
        </div>
        <div class="container">
            <div class="block block-news block-news-default">
                <div class="block-content">
                    <div class="row c-box_map flex-column flex-md-row">
                        <div class="col-md-8 c-col_map">
                            <div class="mapContainer">
                                <div style="text-align: center; margin: 10px auto;">
                                    <div id="myMap" style="position:relative; width:100%; border: 1px solid #fff;"></div>
                                </div>
                                <?php
                                $latitude = 15.90306;
                                $longitude = 105.80669;
                                $json2 = '[';
                                $json_names_2 = array();

                                if (!empty($list))
                                    foreach (@$list as $item) {
                                        $json_names_2[] = "['" . $item->name . "'," . $item->latitude . "," . $item->longitude . ",13,'" . $item->address . "']";
                                        $latitude = $list[0]->latitude;
                                        $longitude = $list[0]->longitude;
                                    }
                                //$img = URL_ROOT.'modules/contact/assets/images/icon_gd.png';
                                // echo $img;die;
                                $json2 .= implode(',', $json_names_2);
                                $json2 .= ']';
                                ?>
                                <script type='text/javascript'>
                                    function GetMap() {
                                        var locations = <?php echo $json2 ?>;
                                        // console.log(locations.length);
                                        var pinInfoBox; //the pop up info box
                                        var infoboxLayer = new Microsoft.Maps.EntityCollection();
                                        var pinLayer = new Microsoft.Maps.EntityCollection();

                                        map = new Microsoft.Maps.Map(document.getElementById("myMap"), {
                                            credentials: "AnzmdBqhzTg091I0F_DxiNxtKM2BVXcgOACtIhT_0VMMhQmMTzK0lK9ORmYIfKH-",
                                            disableScrollWheelZoom: true
                                        });

                                        // Create the info box for the pushpin
                                        pinInfobox = new Microsoft.Maps.Infobox(new Microsoft.Maps.Location(0, 0), {
                                            visible: false
                                        });
                                        infoboxLayer.push(pinInfobox);


                                        for (var i = 0; i < locations.length; i++) {
                                            //add pushpins
                                            var latLon = new Microsoft.Maps.Location(locations[i][1], locations[i][2]);
                                            var pin = new Microsoft.Maps.Pushpin(latLon, {
                                                color: '#C3002F',
                                                // title: locations[i][0],
                                                // subTitle: locations[i][4],
                                                icon: './images/point.png'
                                            });
                                            pin.Title = locations[i][0]; //usually title of the infobox
                                            pin.Description = locations[i][4]; //information you want to display in the infobox
                                            pinLayer.push(pin); //add pushpin to pinLayer
                                            Microsoft.Maps.Events.addHandler(pin, 'click', displayInfobox);
                                        }

                                        map.entities.push(pinLayer);
                                        map.entities.push(infoboxLayer);
                                        map.setView({
                                            zoom: <?php echo @$_SESSION["city"] ? 12 : 9 ?>,
                                            center: new Microsoft.Maps.Location(<?php echo  $latitude ?>, <?php echo  $longitude ?>)
                                        });

                                    }

                                    function displayInfobox(e) {
                                        pinInfobox.setOptions({
                                            title: e.target.Title,
                                            description: e.target.Description,
                                            visible: true,
                                            offset: new Microsoft.Maps.Point(0, 25)
                                        });
                                        pinInfobox.setLocation(e.target.getLocation());
                                    }

                                    function hideInfobox(e) {
                                        pinInfobox.setOptions({
                                            visible: false
                                        });
                                    }
                                </script>
                                <script type='text/javascript' src='https://www.bing.com/api/maps/mapcontrol?callback=GetMap&key=AnzmdBqhzTg091I0F_DxiNxtKM2BVXcgOACtIhT_0VMMhQmMTzK0lK9ORmYIfKH-&CountryRegion=VN' async defer></script>

                            </div>
                        </div>
                        <div class="col-md-4 c-col_store">
                            <div class="c-box_selection">
                                <div class="select-aria">
                                    <select name="province" id="province" class="select_box" onchange="changeCity22(this.value,'district');">
                                        <option value="">--<?php echo FSText::_('Tất cả trung tâm') ?>--</option>
                                        <?php foreach ($dataCity as $province) { ?>
                                            <option <?php if (@$_SESSION["city"] == $province->id) echo 'selected="selected"'; ?> value="<?php echo $province->id; ?>">
                                                <?php echo $province->name; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="c-list_map">
                                <div class="list">
                                    <?php
                                    $html = "";
                                    foreach ($list as $value) {
                                        $link = FSRoute::_('index.php?module=address&view=address&task=detail&id=' . $value->id . '&code=' . $value->alias);
                                    ?>
                                        <div onclick="changeSelectedCentral(<?php echo $value->district?>, <?php echo $value->id ?>)" class="item_map selectAddress">
                                            <div class="content">
                                                <p class="title-address mb-0"><strong><?php echo $value->name; ?></strong></p>
                                                <p class="mb-0">
                                                    <?php echo $value->address; ?>
                                                </p>
                                                <?php if (!empty($value->phone)) { ?>
                                                    <p class="mb-0">
                                                        <?php echo $value->phone; ?>
                                                    </p>
                                                <?php } ?>
                                                <!--                                        <p><span><img src="/images/traffic-right-turn.png" alt="find" class="img-responsive"></span><a href="#">Tìm-->
                                                <!--                                                đường đi</a></p>-->
                                            </div>
                                        </div>

                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img style="position: absolute; bottom: 0px; width: 100%" src="./images/wave2.png" alt="wave2" />
    </div>
    <?php if ($tmpl->count_block('middle')) { ?>
        <?php $tmpl->load_position('middle'); ?>
    <?php } ?>
</div>