<?php
global $tmpl;
$tmpl->addStylesheet('default', 'plugins/comments/css');
$tmpl->addStylesheet('ratings', 'plugins/comments/css');
// $tmpl->addScript('rating', 'plugins/comments/js');
// $tmpl->addScript('default3', 'plugins/comments/js');

$url = $_SERVER['REQUEST_URI'];
$module = FSInput::get('module');
$view = FSInput::get('view');
$rid = FSInput::get('id');

$return = base64_encode($url);

?>
<script>
	$(function() { // Start when document ready
		$('#star-rating').rating(); // Call the rating plugin
		console.log('helloss')
	});
</script>

<div class='comments'>
	<form action="javascript:void(0);" method="post" name="comment_add_form" id='comment_add_form' class='form_comment' class="form_comment" onsubmit="javascript: submit_comment();return false;">
			<div class="col d-flex binhluan__title mt-3">
				<h4>Đánh giá của bạn về sản phẩm:</h4>
				<section class='rating-widget'>
					<!-- Rating Stars Box -->
					<div class='rating-stars text-center'>
						<ul id='stars'>
							<li class='star' title='Poor' data-value='1'>
								<i class='fa fa-star fa-fw'></i>
							</li>
							<li class='star' title='Fair' data-value='2'>
								<i class='fa fa-star fa-fw'></i>
							</li>
							<li class='star' title='Good' data-value='3'>
								<i class='fa fa-star fa-fw'></i>
							</li>
							<li class='star' title='Excellent' data-value='4'>
								<i class='fa fa-star fa-fw'></i>
							</li>
							<li class='star' title='WOW!!!' data-value='5'>
								<i class='fa fa-star fa-fw'></i>
							</li>
						</ul>
					</div>
				</section>
			</div>
			<div class="w-100"></div>
			<div class="col">
				<textarea placeholder="Nội dung đánh giá của bạn" id="your-comment" name="your-comment"></textarea>
			</div>
			<div class="w-100">
				<div class="col no-gutters d-flex justify-content-end align-items-center send-comment">
					<a href="#">Gửi bình luận</a>
				</div>
			</div>

			<input type="hidden" value="<?php echo $module; ?>" name='module' id="_cmt_module" />
			<input type="hidden" value="<?php echo $view; ?>" name='view' id="_cmt_view" />
			<input type="hidden" value="save_comment" name='task' />
			<input type="hidden" value="<?php echo $rid; ?>" name='record_id' id="_cmt_record_id" />
			<input type="hidden" value="<?php echo $return; ?>" name='return' id="_cmt_return" />
			<input type="hidden" value="<?php echo '/index.php?module=' . $module . '&view=' . $view . '&task=save_comment&raw=1'; ?>" name="return" id="link_reply_form" />
	</form>
</div>
<script type="text/javascript">
	$(function() {
		$('.point-rating').rating();
	});
</script>