<?php
/*
 * Huy write
 */

class AjaxPagination
{
    var $limit;
    var $total;
    var $page;
    var $url;
    public $id;
    public $cat;

    function __construct($limit, $page, $total, $url = '', $id = '', $cat ='')
    {
        $this->limit = $limit;
        $this->total = $total;
        $this->page = $page;
        $this->id = $id;
        $this->cat = $cat;
        if ($url)
            $this->url = $url;
        else {
            $url = $_SERVER['REQUEST_URI'];
            $this->url = $url;
        }

    }

    function create_link_with_page($url, $page)
    {

        if (!IS_REWRITE) {
            $url = trim(preg_replace('/&page=[0-9]+/i', '', $url));

            if (!$page || $page == 1) {
                return $url;
            } else {
                return $url . '&page=' . $page;
            }
        } else {
            if ($url == '' || $url == '/')
                $url = '/sim.html';
            if (!$page || $page == 1) {
                $url = trim(preg_replace('/-page[0-9]+/i', '', $url));
                if ($url == '/sim.html')
                    return URL_ROOT;
                return $url;
            } else {
                $search = preg_match('#-page([0-9]+)#is', $url, $main);
                if ($search) {
                    $url = preg_replace('/-page[0-9]+/i', '-page' . $page, $url);
                } else {
                    $url = preg_replace('/.html/i', '-page' . $page . '.html', $url);
                }
                return $url;
            }
        }
    }

    function create_link_with_page_ajax($url, $page)
    {

        return $this->url;

    }

    /*
     * maxpage is max page is show. But It is not last pageg.
     * ex: 1,2,3,4..100.=> 4 is max page
     */
    function showPagination($maxpage = 5)
    {
        $previous = '<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M15.1875 9H2.8125" stroke="#6D737A" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M7.875 3.9375L2.8125 9L7.875 14.0625" stroke="#6D737A" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>
        
        ';
        //$next = "Tiếp";
        $next = '<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M2.8125 9H15.1875" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M10.125 3.9375L15.1875 9L10.125 14.0625" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>
        ';
        $first_page = '<i class="fa left fa-angle-double-left " aria-hidden="true"></i>';
        $last_page = '<i class="fa right fa-angle-double-right " aria-hidden="true"></i>';

        $current_page = FSInput::get('page');
        if (!$current_page || $current_page < 0)
            $current_page = 1;
        $html = "";
        if ($this->limit < $this->total) {
            $num_of_page = ceil($this->total / $this->limit);

            $start_page = $current_page - $maxpage;
            if ($start_page <= 0)
                $start_page = 1;

            $end_page = $current_page + $maxpage;

            if ($end_page > $num_of_page)
                $end_page = $num_of_page;

            //WRITE prefix on screen
            $html .= "<div class='pagination row-item' style='text-align: right;'>";
            //Write Previous
            if (($current_page > 1) && ($num_of_page > 1)) {
//                $html .= "<a class='first-page' href='javascript:void(0)' title='first_page' data-page='" . (0) . "' onclick='load_ajax_pagination(0, \"" . AjaxPagination::create_link_with_page_ajax($this->url, 0) . "\")'  >" . $first_page . "Trang đầu</a>";
                $html .= "<li class='page-item'>
                                <a class='page-link pre_page' href='javascript:void(0)' title='pre_page' data-page='" . ($current_page - 1) . "'  data-url='" . $this->url . "' data-id='" . $this->id . "' data-cat='" . $this->cat . "' onclick='load_ajax_pagination(this)'  >" . $previous . "
                                </a>
                           </li>";
                if ($start_page != 1)
                    $html .= "<b>..</b>";
            }
            for ($i = $start_page; $i <= $end_page; $i++) {
                if ($i != $current_page) {
                    if ($i == 1)
                        $html .= "<li class='page-item'><a class='page-link' title='Page " . $i . "'  data-page='0' data-url='" . $this->url . "' data-id='" . $this->id . "' data-cat='" . $this->cat . "' href='javascript:void(0)'  onclick='load_ajax_pagination(this)' >" . $i . "</a></li>";
                    else
                        $html .= "<li class='page-item'><a class='page-link' title='Page " . $i . "' data-page='" . $i . "' data-url='" . $this->url . "' data-id='" . $this->id . "' data-cat='" . $this->cat . "' href='javascript:void(0)'  onclick='load_ajax_pagination(this)' >" . $i . "</a></li>";
                } else {
                    $html .= "<li class='page-item active '><a class='page-link' title='Page " . $i . "' class='current'>" . $i . "</a></li>";
                }
            }
            //Write Next
            if (($current_page < $num_of_page) && ($num_of_page > 1)) {
                if ($end_page < $num_of_page)
                    $html .= "<b>..</b>";
                $html .= "<li class='page-item'>
                            <a class='page-link next-page' title='Next page' href='javascript:void(0)' data-page='" . ($current_page + 1) . "'  data-url='" . $this->url . "' data-id='" . $this->id . "' data-cat='" . $this->cat . "' onclick='load_ajax_pagination(this)' >
                                " . $next . "</a>
                            </li>";
//                $html .= "<a class='last-page'  href='javascript:void(0)' data-page='" . ($num_of_page) . "' onclick='load_ajax_pagination(" . $num_of_page . ")'  >" . $last_page . "Trang cuối</a>";
            }
            $html .= "</div>";
        }

        return $html;
    }
}

?>
<script>
    function load_ajax_pagination(v) {
        let id = $(v).data('id');
        let page = $(v).data('page');
        let link = $(v).data('url');
        let cat = $(v).data('cat');
        let link_ajax = link + '&raw=1&page=' + page;
        if(cat != 0){
            link_ajax +='&cat='+cat;
        }

        // $.get($value,{}, function(html){
        // 	$('#list_item').html(html);
        // 	$('html, body').animate({scrollTop:$('#list_item').position().top}, 'slow');
        // });
        $.ajax({
            url: link_ajax,
            success: function (html) {
                $(`#${id}`).html(html);
                $('html, body').animate({scrollTop: $(`#${id}`).position().top - 150}, 'slow');
            }
        })
    }
</script>
