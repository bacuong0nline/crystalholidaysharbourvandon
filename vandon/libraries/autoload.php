<?php
/**
 * An example of a project-specific implementation.
 *
 * After registering this autoload function with SPL, the following line
 * would cause the function to attempt to load the \Foo\Bar\Baz\Qux class
 * from /path/to/project/src/Baz/Qux.php:
 *
 *      new \Foo\Bar\Baz\Qux;
 *
 * @param string $class The fully-qualified class name.
 * @return void
 */
spl_autoload_register(function ($class) {
    // base directory for the namespace prefix
    $base_dir = PATH_BASE . 'libraries/';

    // replace the namespace prefix with the base directory, replace namespace
    // separators with directory separators in the relative class name, append
    // with .php
    if (strpos($class, 'CodeIgniter') !== false){
        $class = str_replace(array('\CodeIgniter\\'), 'FS_Check', $class);
        $class = str_replace(array('CodeIgniter\\'), 'CI\\', $class);
        goto label_require;
    }
    if (strpos($class, 'Config') !== false){
        $class = str_replace(array('Config'), 'Finalstyle', $class);
        goto label_require;
    }

    label_require:
    $file = $base_dir . str_replace('\\', '/', $class) . '.php';
    // if the file exists, require it
    if (file_exists($file)) {
        require_once $file;
    }
});