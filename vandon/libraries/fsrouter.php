<?php

use FSRoute as GlobalFSRoute;

class FSRoute
{

    var $url;

    function __construct($url)
    {
    }

    static function _($url)
    {
        return FSRoute::enURL($url);
    }

    /*
     * Trả lại tên mã hóa trên URL
     */

    static function get_name_encode($name, $lang)
    {
        $lang_url = array();
        $lang_url = array(
            'hoc-phi' => 'tuition',
            'he-thong-ung-dung' => 'app',
            's' => 'se',
            'cpj' => 'cpje',
            'cl' => 'cle',
            'fl' => 'fle',
            'lien-he' => 'contact',
            'download-tai-lieu' => 'download-file',
            'san-pham' => 'products',
            'dp' => 'dpe',
            'bao-tri' => 'services',
            'cau-hoi-thuong-gap' => 'frequently-asked-questions',
            'tin-tuc' => 'news',
            'n' => 'ne',
            'cn' => 'cne',
            'c' => 'ce',
            'tim-kiem' => 'search',
            'ch' => 'che',
            'csn' => 'csne',
            'ht' => 'hte',
            'r' => 're',
            'tuyen-dung' => 'recruitment',
            'tim-kiem-cong-viec' => 'job',
            'gioi-thieu' => 'introduction',
            'xay-dung-nha-tron-goi' => 'construction-work-packaging',
            'gioi-thieu' => 'introduction',
            'quy-hoach-du-an' => 'project-planning',
            'thi-cong-noi-that' => 'furniture',
            'du-an' => 'project',
            'ung-dung' => 'application',
            'kien-thuc' => 'knowledge',
            'tinh-chi-phi-xay-dung-buoc-1' => 'step-1',
            'tinh-chi-phi-xay-dung-buoc-2' => 'step-2',
            'tinh-chi-phi-xay-dung-buoc-3' => 'step-3',
            'dat-lich-thiet-ke' => 'booking',
            'ket-qua' => 'final',
            'pj' => 'pje',

            'bq' => 'bqe',
        );

        if ($lang == 'vi')
            return $name;
        else
            return $lang_url[$name];
    }

    static function addParameters($params, $value, $module = '', $view = '')
    {
        // only filter
        if (!$module) {
            $module = FSInput::get('module');
            //$view = FSInput::get('view');
            if (!$view) {
                //$module = FSInput::get('module');
                $view = FSInput::get('view');
            }
        }
        if ($module == 'project' & $view == 'home') {
            $array_params_need_get = array(
                'type',
                'style',
                'cost'
            );

            $url =  'index.php?module=' . $module . '&view=' . $view;
            foreach ($array_params_need_get as $item) {
                if ($item != $params) {
                    $value_of_param = FSInput::get($item);
                    if ($value_of_param) {
                        if (strpos($url, $item) !== false) {
                        }
                        $url .= "&" . $item . "=" . $value_of_param;
                    }
                } else {
                    if ($value)
                        $url .= "&" . $item . "=" . $value;
                }
            }
            return FSRoute::_($url);
        }
        if ($module == 'products' && ($view == 'cat' || $view == 'home')) {
            $array_paras_need_get = array('type_car', 'time_tour', 'price_tour', 'rating_tour', 'cid', 'ccode', 'filter');
            $url = 'index.php?module=' . $module . '&view=' . $view;
            foreach ($array_paras_need_get as $item) {
                if ($item != $params) {
                    $value_of_param = FSInput::get($item);
                    if ($value_of_param) {
                        $url .= "&" . $item . "=" . $value_of_param;
                    }
                } else {
                    if ($value)
                        $url .= "&" . $item . "=" . $value;
                }
            }

            return FSRoute::_($url);
        }
        if ($module == 'products' && $view == 'cat_child') {
            $array_paras_need_get = array('ccode', 'sort', 'Itemid', 'cid');
            $url = 'index.php?module=' . $module . '&view=' . $view;
            foreach ($array_paras_need_get as $item) {
                if ($item != $params) {
                    $value_of_param = FSInput::get($item);
                    if ($value_of_param) {
                        $url .= "&" . $item . "=" . $value_of_param;
                    }
                } else {
                    if ($value)
                        $url .= "&" . $item . "=" . $value;
                }
            }
            return FSRoute::_($url);
        }
        return FSRoute::_($_SERVER['REQUEST_URI']);
    }

    function removeParameters($params)
    {
        // only filter
        $module = FSInput::get('module');
        $view = FSInput::get('view');
        $ccode = FSInput::get('ccode');
        $filter = FSInput::get('filter');
        $all_filter = FSInput::get('all_filter');
        $manu = FSInput::get('manu');
        $Itemid = FSInput::get('Itemid');

        $url = 'index.php?module=' . $module . '&view=' . $view;
        if ($ccode) {
            $url .= '&ccode=' . $ccode;
        }
        if ($filter) {
            $url .= '&filter=' . $filter;
        }
        $url .= '&Itemid=' . $Itemid;
        $url = trim(preg_replace('/&' . $params . '=[0-9a-zA-Z_-]+/i', '', $url));
    }

    /*
     * rewrite
     */

    static function enURL($url)
    {
        if (!$url)
            $url = $_SERVER['REQUEST_URI'];

        if (!IS_REWRITE)
            return URL_ROOT . $url;
        if (strpos($url, 'http://') !== false || strpos($url, 'https://') !== false)
            return $url;

        $url_reduced = substr($url, 10); // width : index.php
        $array_buffer = explode('&', $url_reduced, 10);
        $array_params = array();
        for ($i = 0; $i < count($array_buffer); $i++) {
            $item = $array_buffer[$i];
            $pos_sepa = strpos($item, '=');
            $array_params[substr($item, 0, $pos_sepa)] = substr($item, $pos_sepa + 1);
        }

        $module = isset($array_params['module']) ? $array_params['module'] : '';
        $view = isset($array_params['view']) ? $array_params['view'] : $module;
        $task = isset($array_params['task']) ? $array_params['task'] : 'display';
        $Itemid = isset($array_params['Itemid']) ? $array_params['Itemid'] : 0;
        //$location  = isset($array_params['location'])?$array_params['location']: CITY;

        $lang = isset($_SESSION['lang']) ? $_SESSION['lang'] : 'vi';
        $url_first = URL_ROOT;
        $url1 = '';
        switch ($module) {
            case 'project':
                switch ($view) {
                    case 'home':
                        foreach ($array_params as $key => $value) {
                            if ($key == 'module' || $key == 'view' || $key == 'task' || $key == 'Itemid' || $key == 'ccode' || $key == 'cid')
                                continue;
                            $url1 .= '&' . $key . '=' . $value;
                        }
                        if ($url1) {
                            return $url_first . FSRoute::get_name_encode('san-pham', $lang) . '.html?' . $url1;
                        } else {
                            return $url_first . FSRoute::get_name_encode('san-pham', $lang) . '.html';
                        }
                    case 'cat':
                        $ccode = isset($array_params['ccode']) ? $array_params['ccode'] : '';
                        $cid = isset($array_params['cid']) ? $array_params['cid'] : '';
                        $Itemid = isset($array_params['Itemid']) ? $array_params['Itemid'] : '';

                        return $url_first . $ccode . '-' . FSRoute::get_name_encode('cpj', $lang) . $cid . '.html';
                    case 'project':
                        $ccode = isset($array_params['code']) ? $array_params['code'] : '';
                        $id = isset($array_params['id']) ? $array_params['id'] : '';
                        $Itemid = isset($array_params['Itemid']) ? $array_params['Itemid'] : '';

                        return $url_first . $ccode . '-' . FSRoute::get_name_encode('pj', $lang) . $id . '.html';
                }
                break;
            case 'projectpartner':
                switch ($view) {
                    case 'cat':
                        $ccode = isset($array_params['ccode']) ? $array_params['ccode'] : '';
                        $id = isset($array_params['cid']) ? $array_params['cid'] : '';
                        return $url_first . $ccode . '-' . FSRoute::get_name_encode('cpp', $lang) . $id . '.html';
                    case 'home':
                        $code = isset($array_params['code']) ? $array_params['code'] : '';
                        $ccode = isset($array_params['ccode']) ? $array_params['ccode'] : '';
                        $id = isset($array_params['id']) ? $array_params['id'] : '';
                        return $url_first . $code . '-' . FSRoute::get_name_encode('pp', $lang) . $id . '.html';
                    case 'list':
                        return $url_first . FSRoute::get_name_encode('du-an-doi-tac', $lang) . '.html';
                }
                break;
            case 'central':
                switch ($view) {
                    case 'central':
                        $code = isset($array_params['code']) ? $array_params['code'] : '';
                        $id = isset($array_params['id']) ? $array_params['id'] : '';
                        $task = isset($array_params['task']) ? $array_params['task'] : '';
                        $Itemid = isset($array_params['Itemid']) ? $array_params['Itemid'] : '';

                        if ($task)
                            return $url_first . $code . '-' . FSRoute::get_name_encode('csn', $lang) . $id . '.html';
                        else
                            return $url_first . 'he-thong-' . $code . '-' . FSRoute::get_name_encode('ht', $lang) . $id . '-' . $Itemid . '.html';
                    default:
                        return $url_first . $url;
                }
                break;
            case 'architecture':
                switch ($view) {
                    case 'architecture':
                        return $url_first . FSRoute::get_name_encode('xay-dung-nha-tron-goi', $lang) . '.html';
                }
                break;
            case 'construction':
                switch ($view) {
                    case 'home':
                        return $url_first . FSRoute::get_name_encode('thi-cong-noi-that', $lang) . '.html';
                }
                break;
            case 'planning':
                switch ($view) {
                    case 'home':
                        return $url_first . FSRoute::get_name_encode('quy-hoach-du-an', $lang) . '.html';
                }
                break;
            case 'gallery':
                switch ($view) {
                    case 'home':
                        return $url_first . FSRoute::get_name_encode('thu-vien', $lang) . '.html';
                    case 'video':
                        return $url_first . FSRoute::get_name_encode('thu-vien-video', $lang) . '.html';
                }
                break;
            case 'application':
                switch ($view) {
                    case 'home':
                        return $url_first . FSRoute::get_name_encode('ung-dung', $lang) . '.html';
                    case 'step1':
                        return $url_first . FSRoute::get_name_encode('tinh-chi-phi-xay-dung-buoc-1', $lang) . '.html';
                    case 'step2':
                        return $url_first . FSRoute::get_name_encode('tinh-chi-phi-xay-dung-buoc-2', $lang) . '.html';
                    case 'step3':
                        return $url_first . FSRoute::get_name_encode('tinh-chi-phi-xay-dung-buoc-3', $lang) . '.html';
                    case 'final':
                        return $url_first . FSRoute::get_name_encode('ket-qua', $lang) . '.html';
                }
                break;
            case 'introduce':
                switch ($view) {
                    case 'home':
                        return $url_first . FSRoute::get_name_encode('gioi-thieu', $lang) . '.html';
                }
                break;
            case 'booking':
                switch ($view) {
                    case 'booking':
                        return $url_first . FSRoute::get_name_encode('dat-lich-thiet-ke', $lang) . '.html';
                }
                break;
            case 'experience':
                switch ($view) {
                    case 'experience':
                        return $url_first . FSRoute::get_name_encode('trai-nghiem-hoc-vien', $lang) . '.html';
                    default:
                        return $url_first . $url;
                }
                break;
            case 'news':
                switch ($view) {
                    case 'news':
                        $code = isset($array_params['code']) ? $array_params['code'] : '';
                        $ccode = isset($array_params['ccode']) ? $array_params['ccode'] : '';
                        $id = isset($array_params['id']) ? $array_params['id'] : '';
                        $tag = isset($array_params['tag']) ? $array_params['tag'] : '';
                        if ($tag)
                            return $url_first . $code . '-' . FSRoute::get_name_encode('n', $lang) . $id . '/tag=' . $tag . '.html';
                        else
                            return $url_first . $code . '-' . FSRoute::get_name_encode('n', $lang) . $id . '.html';
                    case 'cat':
                        $ccode = isset($array_params['ccode']) ? $array_params['ccode'] : '';
                        $id = isset($array_params['cid']) ? $array_params['cid'] : '';
                        return $url_first . $ccode . '-' . FSRoute::get_name_encode('cn', $lang) . $id . '.html';
                    case 'home':
                        return $url_first . FSRoute::get_name_encode('tin-tuc', $lang) . '.html';
                    default:
                        return $url_first . $url;
                }
                break;
            case 'knowledge':
                switch ($view) {
                    case 'knowledge':
                        $code = isset($array_params['code']) ? $array_params['code'] : '';
                        $id = isset($array_params['id']) ? $array_params['id'] : '';

                        return $url_first . $code . '-' . FSRoute::get_name_encode('k', $lang) . $id . '.html';
                    case 'home':
                        return $url_first . FSRoute::get_name_encode('kien-thuc', $lang) . '.html';
                    default:
                        return $url_first . $url;
                }
                break;
            case 'download':
                switch ($view) {
                        //                    case 'download':
                        //                        $code = isset($array_params['code']) ? $array_params['code'] : '';
                        //                        $ccode = isset($array_params['ccode']) ? $array_params['ccode'] : '';
                        //                        $id = isset($array_params['id']) ? $array_params['id'] : '';
                        //                        return $url_first . $code . '-' . FSRoute::get_name_encode('n', $lang) . $id . '.html';
                    case 'home':
                        return $url_first . FSRoute::get_name_encode('download-tai-lieu', $lang) . '.html';
                    default:
                        return $url_first . $url;
                }
                break;
            case 'projects':
                switch ($view) {
                    case 'filter_list':
                        $year = isset($array_params['year']) ? $array_params['year'] : '';
                        $month = isset($array_params['month']) ? $array_params['month'] : '';
                        return $url_first . 'month' . $month . '-year' . $year . '-' . FSRoute::get_name_encode('fl', $lang) . '.html';
                    case 'cat_list':
                        $id = isset($array_params['id']) ? $array_params['id'] : '';
                        $alias = isset($array_params['alias']) ? $array_params['alias'] : '';
                        return $url_first . $alias . '-' . FSRoute::get_name_encode('cl', $lang) . $id . '.html';
                    case 'filter':
                        $month = isset($array_params['month']) ? $array_params['month'] : '';
                        $year = isset($array_params['year']) ? $array_params['year'] : '';
                        return $url_first . 'month' . $month . '-year' . $year . '.html';
                    case 'news':
                        $code = isset($array_params['code']) ? $array_params['code'] : '';
                        //                        $ccode = isset($array_params['ccode']) ? $array_params['ccode'] : '';
                        $id = isset($array_params['id']) ? $array_params['id'] : '';
                        return $url_first . $code . '-' . FSRoute::get_name_encode('pj', $lang) . $id . '.html';
                    case 'cat':
                        $ccode = isset($array_params['ccode']) ? $array_params['ccode'] : '';
                        $id = isset($array_params['id']) ? $array_params['id'] : '';
                        switch ($task) {
                            case 'detail':
                                return $url_first . $ccode . '-' . FSRoute::get_name_encode('cpjm', $lang) . $id . '.html';
                        }
                        return $url_first . $ccode . '-' . FSRoute::get_name_encode('cpj', $lang) . $id . '.html';
                    case 'home':
                        switch ($task) {
                            case 'view_more':
                                return $url_first . 'xem-them.html';
                        }
                        return $url_first . FSRoute::get_name_encode('du-an-tieu-bieu', $lang) . '.html';
                    default:
                        return $url_first . $url;
                }
                break;
            case 'utilities':
                switch ($view) {
                    case 'home':
                        return $url_first . FSRoute::get_name_encode('tien-ich', $lang) . '.html';
                    default:
                        return $url_first . $url;
                }
                break;
            case 'ground':
                switch ($view) {
                    case 'home':
                        return $url_first . FSRoute::get_name_encode('mat-bang', $lang) . '.html';
                    default:
                        return $url_first . $url;
                }
                break;
            case 'forms':
                switch ($view) {
                    case 'home':
                        return $url_first . FSRoute::get_name_encode('mau-thiet-ke-gara-dep', $lang) . '.html';
                    default:
                        return $url_first . $url;
                }
                break;
                // case 'products':
                //     switch ($view) {
                //         case 'cart':
                //             return $url_first . FSRoute::get_name_encode('gio-hang', $lang) . '.html';
                //             break;
                //         case 'cat_child':
                //             $url_first .= '';
                //             foreach ($array_params as $key => $value) {
                //                 if ($key == 'module' || $key == 'view' || $key == 'task' || $key == 'Itemid' || $key == 'ccode' || $key == 'cid')
                //                     continue;
                //                 $url1 .= '&' . $key . '=' . $value;
                //             }
                //             $ccode = isset($array_params['ccode']) ? $array_params['ccode'] : '';
                //             $id = isset($array_params['cid']) ? $array_params['cid'] : '';
                //             $url_first .= $ccode . '-' . FSRoute::get_name_encode('pcc', $lang) . $id;
                //             return $url_first . '.html' . $url1;
                //         case 'home':
                //             $url_first .= FSRoute::get_name_encode('san-pham', $lang);
                //             switch ($task) {
                //                 case 'clear_filter':
                //                     return $url_first . '/clear_filter.html';
                //             }
                //             return $url_first . '.html';
                //         case 'product':
                //             switch ($task) {
                //                 case 'request':
                //                     $id = isset($array_params['id']) ? $array_params['id'] : '';
                //                     return $url_first . 'dang-ky-nhan-bao-gia-' . $id . '.html';
                //                 case 'edel':
                //                     $id =  isset($array_params['id']) ? $array_params['id'] : '';
                //                     return $url_first . 'del' . $id . '.html';
                //                 default:
                //                     $code = isset($array_params['code']) ? $array_params['code'] : '';
                //                     //$ccode = isset($array_params['ccode']) ? $array_params['ccode'] : '';
                //                     $id = isset($array_params['id']) ? $array_params['id'] : '';
                //                     return $url_first . $code . '-' . FSRoute::get_name_encode('dp', $lang) . $id . '.html';
                //             }
                //         case 'cat':
                //             $ccode = isset($array_params['ccode']) ? $array_params['ccode'] : '';
                //             $id = isset($array_params['cid']) ? $array_params['cid'] : '';
                //             $filter = isset($array_params['filter']) ? $array_params['filter'] : '';
                //             $task = isset($array_params['task']) ? $array_params['task'] : '';
                //             if ($filter) {
                //                 return $url_first . $ccode . '-pc' . $id . '/' . $filter . '-filter.html' . $url1;
                //             } else if ($ccode && $id) {
                //                 $url_first .= $ccode . '-' . FSRoute::get_name_encode('pc', $lang) . $id;
                //             }
                //             if ($task == "cat2") {
                //                 $url_first .= 'dat-hang-online';
                //             }
                //             // $table_name = isset($array_params['tablename']) ? $array_params['tablename'] : '';
                //             return $url_first . '.html';

                //         case 'search':
                //             $keyword = isset($array_params['keyword']) ? $array_params['keyword'] : '';
                //             $url = $url_first . FSRoute::get_name_encode('tim-kiem', $lang);
                //             if ($keyword) {
                //                 $url .= '/' . $keyword . '.html';
                //             }
                //             return $url;
                //         default:
                //             return $url_first . $url;
                //     }
                //     break;
            case 'search':
                switch ($view) {
                    case 'search':
                        $keyword  = isset($array_params['keyword']) ? $array_params['keyword'] : '';
                        $url = URL_ROOT . FSRoute::get_name_encode('tim-kiem', $lang);
                        if ($keyword) {
                            $url .= '/' . $keyword . '.html';
                        }
                        return $url;
                }
                break;
            case 'recruitment':
                switch ($view) {
                    case 'cat':
                        $task  = isset($array_params['search_job']) ? $array_params['search_job'] : '';
                        return $url = URL_ROOT . FSRoute::get_name_encode('tim-kiem-cong-viec', $lang) . '.html';

                    case 'home':
                        return $url = URL_ROOT . FSRoute::get_name_encode('tuyen-dung', $lang) . '.html';
                    case 'recruitment':
                        $code  = isset($array_params['code']) ? $array_params['code'] : '';
                        $id  = isset($array_params['id']) ? $array_params['id'] : '';

                        return $url_first . $code . '-' . FSRoute::get_name_encode('r', $lang) . $id . '.html';
                }
                break;
            case 'address':
                switch ($view) {
                    case 'address':
                        return $url_first . FSRoute::get_name_encode('he-thong-trung-tam', $lang) . '.html';
                }
                break;
            case 'schedule':
                switch ($view) {
                    case 'home':
                        return $url_first . FSRoute::get_name_encode('thi-chung-chi', $lang) . '.html';
                        $code  = isset($array_params['code']) ? $array_params['code'] : '';
                    case 'cat':
                        $cid  = isset($array_params['cid']) ? $array_params['cid'] : '';
                        $ccode  = isset($array_params['ccode']) ? $array_params['ccode'] : '';
                        $Itemid  = isset($array_params['Itemid']) ? $array_params['Itemid'] : '';

                        return $url_first . $ccode . '-' . FSRoute::get_name_encode('s', $lang) . $cid . '-' . $Itemid . '.html';
                }
                break;
            case 'tuition':
                switch ($view) {
                    case 'tuition':
                        return $url_first . FSRoute::get_name_encode('hoc-phi', $lang) . '.html';
                }
                break;
            case 'app':
                switch ($view) {
                    case 'app':
                        return $url_first . FSRoute::get_name_encode('he-thong-ung-dung', $lang) . '.html';
                }
                break;
            case 'goodstudy':
                switch ($view) {
                    case 'home':
                        return $url_first . FSRoute::get_name_encode('bi-quyet-hoc-tot', $lang) . '.html';
                    case 'goodstudy':
                        $id  = isset($array_params['id']) ? $array_params['id'] : '';
                        $code  = isset($array_params['code']) ? $array_params['code'] : '';
                        $Itemid  = isset($array_params['Itemid']) ? $array_params['Itemid'] : '';
                        return $url_first . $code . '-' . FSRoute::get_name_encode('bq', $lang) . $id . '.html';
                }
                break;
            case 'video':
                $code = isset($array_params['code']) ? $array_params['code'] : '';
                $id = isset($array_params['id']) ? $array_params['id'] : '';
                return $url_first . $code . '-' . FSRoute::get_name_encode('v', $lang) . $id . '.html';
            case 'service':
                switch ($view) {
                    case 'home':
                        return $url_first . FSRoute::get_name_encode('bao-tri', $lang) . '.html';
                    case 'service':
                        $code = isset($array_params['code']) ? $array_params['code'] : '';
                        $id = isset($array_params['id']) ? $array_params['id'] : '';
                        //return $url_first.FSRoute::get_name_encode('ct',$lang).'-'.$code.'.html';
                        return $url_first . $code . '-' . FSRoute::get_name_encode('pm', $lang) . $id . '.html';
                }
                break;
            case 'contents':
                switch ($view) {
                    case 'cat': {
                            return $url_first . FSRoute::get_name_encode('gioi-thieu', $lang) . '.html';
                        }
                    case 'new': {
                            $code = isset($array_params['code']) ? $array_params['code'] : '';
                            $id = isset($array_params['id']) ? $array_params['id'] : '';
                            $Itemid = isset($array_params['Itemid']) ? $array_params['Itemid'] : '';
                            return $url_first . $code . '-' . FSRoute::get_name_encode('c', $lang) . $Itemid . '.html';
                        }
                }
                break;

            case 'contact':
                return $url_first . FSRoute::get_name_encode('lien-he', $lang) . '.html';
                break;
            case 'apps':
                switch ($view) {
                    case 'apps': {
                            return $url_first . FSRoute::get_name_encode('he-thong-ung-dung', $lang) . '.html';
                        }
                }
                break;
            case 'payment':
                return $url_first . FSRoute::get_name_encode('thanh-toan', $lang) . '.html';
                break;
            case 'installment':
                return $url_first . 'tra-gop.html';
                break;

            case 'faq':
                switch ($view) {
                    case 'faq':
                        return $url_first . 'hoi-dap.html';
                }
                break;

            case 'home':
                switch ($view) {
                    case 'home':
                        return $url_first;
                }
                break;

            case 'notfound':
                switch ($view) {
                    case 'notfound':
                        return $url_first . 'notfound.html';
                    default:
                        return $url_first . $url;
                }
                break;

            case 'question':
                switch ($view) {
                    case 'default':
                        return $url_first . FSRoute::get_name_encode('cau-hoi-thuong-gap', $lang) . '.html';
                }
                break;

            case 'promotion':
                switch ($view) {
                    case 'blog':
                        $code = isset($array_params['ccode']) ? $array_params['ccode'] : '';
                        $id = isset($array_params['cid']) ? $array_params['cid'] : '';
                        $task = isset($array_params['task']) ? $array_params['task'] : '';
                        if ($task == 'detail') {
                            $code = isset($array_params['code']) ? $array_params['code'] : '';
                            $id = isset($array_params['id']) ? $array_params['id'] : '';
                            return $url_first . 'bai-viet/' . $code . '-' . FSRoute::get_name_encode('pm', $lang) . $id . '.html';
                        } else {
                            return $url_first . 'khuyen-mai/' . $code . '-' . FSRoute::get_name_encode('pm', $lang) . $id . '.html';
                        }
                        break;
                    case 'cat':
                        $code = isset($array_params['ccode']) ? $array_params['ccode'] : '';
                        $id = isset($array_params['cid']) ? $array_params['cid'] : '';
                        //return $url_first.FSRoute::get_name_encode('ct',$lang).'-'.$code.'.html';
                        return $url_first . 'khuyen-mai-thuc-don/' . $code . '-' . FSRoute::get_name_encode('pm', $lang) . $id . '.html';
                }
                break;

            case 'coupon':
                switch ($view) {
                    case 'home':
                        return $url_first . 'ma-giam-gia.html';

                    case 'cat':
                        $id = isset($array_params['id']) ? $array_params['id'] : '';
                        $ccode = isset($array_params['ccode']) ? $array_params['ccode'] : '';
                        return $url_first . 'ma-giam-gia-' . $ccode . '.html';
                }
                break;

            case 'department':
                switch ($view) {
                    case 'department':
                        return $url_first . 'he-thong-cua-hang.html';
                }
                break;

            case 'autoget':
                switch ($view) {
                    case 'autoget':
                        $code = isset($array_params['code']) ? $array_params['code'] : '';
                        $id = isset($array_params['id']) ? $array_params['id'] : '';
                        return $url_first . 'auto-get-product-gd' . $id . '.html';
                }
                break;

            case 'cache':
                return $url_first . 'delete-cache.html';

            case 'sitemap':
                return $url_first . 'site-map.html';

            case 'users':
                switch ($view) {
                    case 'users':
                        switch ($task) {
                            case 'login':
                                $url1 = '';
                                foreach ($array_params as $key => $value) {
                                    if ($key == 'module' || $key == 'view' || $key == 'Itemid' || $key == 'task')
                                        continue;
                                    $url1 .= '&' . $key . '=' . $value;
                                }

                                return URL_ROOT . 'dang-nhap.html' . $url1;
                            case 'register':
                                $url1 = '';
                                foreach ($array_params as $key => $value) {
                                    if ($key == 'module' || $key == 'view' || $key == 'Itemid' || $key == 'task')
                                        continue;
                                    $url1 .= '&' . $key . '=' . $value;
                                }
                                return URL_ROOT . 'dang-ky.html' . $url1;
                            case 'forget':
                                return URL_ROOT . 'quen-mat-khau.html';
                            case 'changepass':
                                return URL_ROOT . 'doi-mat-khau.html';
                            case 'logout':
                                return URL_ROOT . 'dang-xuat.html';

                            default:
                                return URL_ROOT . 'thong-tin-ca-nhan.html';
                        }
                    default:
                        return URL_ROOT . $url;
                    case 'order':
                        switch ($task) {
                            case 'show_order':
                                $id = isset($array_params['id']) ? $array_params['id'] : '';
                                return URL_ROOT . 'chi-tiet-don-hang-' . $id . '.html';
                            default:
                                return URL_ROOT . 'quan-ly-don-hang.html';
                        }
                        break;

                    case 'address':
                        switch ($task) {
                            case 'add_address':
                                return URL_ROOT . 'them-so-dia-chi.html';
                            case 'edit_address':
                                $id = isset($array_params['id']) ? $array_params['id'] : '';
                                return URL_ROOT . 'sua-so-dia-chi-' . $id . '.html';
                            default:
                                return URL_ROOT . 'so-dia-chi.html';
                        }
                        break;
                    case 'level':
                        return URL_ROOT . 'cap-tai-khoan.html';
                }
                break;
            case "projectpartner":
                switch ($view) {
                    case "cat":
                        $url_first .= '';
                        $ccode = isset($array_params['ccode']) ? $array_params['ccode'] : '';
                        $cid = isset($array_params['cid']) ? $array_params['cid'] : '';
                        $url_first .= $ccode . '-' . FSRoute::get_name_encode('pp', $lang) . $cid;
                        return $url_first . '.html';
                        break;
                    case "home":
                        $url_first .= '';
                        $code = isset($array_params['code']) ? $array_params['code'] : '';
                        $id = isset($array_params['id']) ? $array_params['id'] : '';
                        $url_first .= $code . '-' . FSRoute::get_name_encode('dpp', $lang) . $id;
                        return $url_first . '.html';
                        break;
                }

                break;
            default:
                return URL_ROOT . $url;
        }
    }

    /*
     * get real url from virtual url
     */

    function deURL($url)
    {
        if (!IS_REWRITE)
            return $url;
        return $url;
        if (strpos($url, URL_ROOT_REDUCE) !== false) {
            $url = substr($url, strlen(URL_ROOT_REDUCE));
        }
        if ($url == 'news.html')
            return 'index.php?module=news&view=home&Itemid=1';
        if (strpos($url, 'news-page') !== false) {
            $f = strpos($url, 'news-page') + 9;
            $l = strpos($url, '.html');
            $page = intval(substr($url, $f, ($l - $f)));
            return "index.php?module=news&view=home&page=$page&Itemid=1";
        }
        $array_url = explode('/', $url);
        $module = isset($array_url[0]) ? $array_url[0] : '';
        switch ($module) {
            case 'news':
                // if cat
                if (preg_match('#news/([^/]*)-c([0-9]*)-it([0-9]*)(-page([0-9]*))?.html#s', $url, $arr)) {
                    return "index.php?module=news&view=cat&id=" . @$arr[2] . "&Itemid=" . @$arr[3] . '&page=' . @$arr[5];
                }
                // if article
                if (preg_match('#news/detail/([^/]*)-i([0-9]*)-it([0-9]*).html#s', $url, $arr)) {
                    return "index.php?module=news&view=news&id=" . @$arr[2] . "&Itemid=" . @$arr[3];
                }
            case 'companies':
                $str_continue = ($module = isset($array_url[1])) ? $array_url[1] : '';
                if ($str_continue == 'register.html')
                    return "index.php?module=companies&view=company&task=register&Itemid=5";
                if (preg_match('#category-id([0-9]*)-city([0-9]*)-it([0-9]*)(-page([0-9]*))?.html#s', $str_continue, $arr)) {
                    if (isset($arr[5]))
                        return "index.php?module=companies&view=category&id=" . @$arr[1] . "&city=" . @$arr[2] . "&Itemid=" . @$arr[3] . "&page=" . @$arr[5];
                    else
                        return "index.php?module=companies&view=category&id=" . @$arr[1] . "&city=" . @$arr[2] . "&Itemid=" . @$arr[3];
                }
            default:
                return $url;
        }
    }

    function get_home_link()
    {
        $lang = isset($_SESSION['lang']) ? $_SESSION['lang'] : 'vi';
        if ($lang == 'vi') {
            return URL_ROOT;
        } else {
            return URL_ROOT . 'en';
        }
    }

    /*
     * Dịch ngang
     */

    static function change_link_by_lang($lang, $link = '')
    {
        $module = FSRoute::get_param('module', $link);
        $view = FSRoute::get_param('view', $link);
        if (!$view)
            $view = $module;
        if (!$module || ($module == 'home' && $view == 'home')) {
            if ($lang == 'en') {
                //				return URL_ROOT;
            } else {
                return URL_ROOT . 'vi';
            }
        }
        switch ($module) {

            case 'contents':
                switch ($view) {
                    case 'content':
                        $code = FSRoute::get_param('code', $link);
                        $record = FSRoute::trans_record_by_field($code, 'alias', 'fs_contents', $lang, 'id,alias,category_alias');
                        if (!$record)
                            return;
                        $url = URL_ROOT . FSRoute::get_name_encode('ct', $lang) . '-' . $record->alias;
                        return $url . '.html';
                        return $url;
                }
                break;
            default:
                $url = URL_ROOT . 'ce-information';
                return $url . '.html';
        }
    }

    /*
     * Hàm trả lại tham số: có thể từ biến $_REQUEST hay từ phân tích URL truyền vào
     */

    static function get_param($param_name, $link = '')
    {
        if (!$link)
            return FSInput::get($param_name);
        $url = str_replace('&amp;', '&', $link);
        $url_reduced = substr($url, 10); // width : index.php
        $array_buffer = explode('&', $url_reduced, 10);
        $array_params = array();
        for ($i = 0; $i < count($array_buffer); $i++) {
            $item = $array_buffer[$i];
            $pos_sepa = strpos($item, '=');
            $array_params[substr($item, 0, $pos_sepa)] = substr($item, $pos_sepa + 1);
        }
        return @$array_params[$param_name];
    }

    function get_record_by_id($id, $table_name, $lang, $select)
    {
        if (!$id)
            return;
        if (!$table_name)
            return;
        $fs_table = FSFactory::getClass('fstable');
        $table_name = $fs_table->getTable($table_name);

        $query = " SELECT " . $select . "
					  FROM " . $table_name . "
					  WHERE id = $id ";

        global $db;
        $sql = $db->query($query);
        $result = $db->getObject();
        return $result;
    }

    /*
     * Lấy bản ghi dịch ngôn ngữ
     */

    static function trans_record_by_field($value, $field = 'alias', $table_name, $lang, $select = '*')
    {
        if (!$value)
            return;
        if (!$table_name)
            return;
        $fs_table = FSFactory::getClass('fstable');
        $table_name_old = $fs_table->getTable($table_name);

        $query = " SELECT id
					  FROM " . $table_name_old . "
					  WHERE " . $field . " = '" . $value . "' ";

        global $db;
        $sql = $db->query($query);
        $id = $db->getResult();
        if (!$id)
            return;
        $query = " SELECT " . $select . "
					  FROM " . $fs_table->translate_table($table_name) . "
					  WHERE id = '" . $id . "' ";
        global $db;
        $sql = $db->query($query);
        $rs = $db->getObject();
        return $rs;
    }

    /*
     * Dịch từ field -> field ( tìm lại id rồi dịch ngược)
     */

    function translate_field($value, $table_name, $field = 'alias')
    {

        if (!$value)
            return;
        if (!$table_name)
            return;
        $fs_table = FSFactory::getClass('fstable');
        $table_name_old = $fs_table->getTable($table_name);

        $query = " SELECT id
					  FROM " . $table_name_old . "
					  WHERE $field = '" . $value . "' ";
        global $db;
        $sql = $db->query($query);
        $id = $db->getResult();
        if (!$id)
            return;
        $query = " SELECT " . $field . "
					  FROM " . $fs_table->translate_table($table_name) . "
					  WHERE id = '" . $id . "' ";
        global $db;
        $sql = $db->query($query);
        $rs = $db->getResult();
        return $rs;
    }
}
