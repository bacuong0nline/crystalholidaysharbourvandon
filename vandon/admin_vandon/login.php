<?php 


function login(){
	$db = new FS_PDO();
	$password = md5(FSInput::get('password'));
	$username = FSInput::get2('username');
    
	$query = 'SELECT u.id, u.username, u.email
                FROM fs_users AS u
                WHERE published = 1 AND  u.username = \''.$username.'\' AND u.password = \''.$password.'\'
                LIMIT 1';
                
	$user = $db->getObject($query);
	if(!$user){
		return false;
	}
	$_SESSION['ad_logged']     = 1;
	$_SESSION['ad_userid']     = $user->id;
//    $_SESSION['ad_groupid']    = $user->groupid;
 
    $_SESSION['ad_username']   = $user->username;
    $_SESSION['ad_useremail']  = $user->email;
	return true;
}

session_start();
if(isset($_SESSION['ad_logged']) && $_SESSION['ad_logged']==1)
    header("Location: index.php");
    
require_once("../includes/config.php");
require_once ("includes/defines.php");
require_once('../libraries/database/pdo.php');
require_once("../libraries/fsinput.php");

require_once ("../includes/functions.php");
$action		= FSInput::get('action');

if($action == "login"){
	if(!login()){
        echo '<script type="text/javascript">alert(\'Thông tin đăng nhập không đúng hoặc tài khoản của bạn chưa được kích hoạt!\')</script>';
	}	
	else{
		header( "Location: index.php" );
	}
}
?>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Login - Thiết kế web Finalstyle</title>
    <link type='image/x-icon' href='templates/default/images/favicon.ico' rel='icon' />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="copyright" content="© 2013 FinalStyle, Thiết kế website Phong Cách Số" /> 
    <meta name="robots" content="noindex, nofollow"/>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo URL_ROOT ?>libraries/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo URL_ROOT ?>libraries/bower_components/font-awesome/css/font-awesome.min.css" />
    <link href="templates/default/css/login.css" rel="stylesheet" />
</head>
<body>
    <div class="limiter">
        <div class="container-login100" style="">
            <div class="wrap-login100 p-l-50 p-r-50 p-t-40 p-b-20">
                <div class="text-center">
                    <img style="object-fit:contain" src="/images/favicon.jpg">
                </div>
                <form action="login.php" method="post" class="login100-form validate-form flex-sb flex-w" autocomplete="off">
                    <div class="p-t-31 p-b-9">
                        <span class="txt1">
                            Tên đăng nhập
                        </span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate = "Vui lòng nhập tên đăng nhập.">
                        <input class="input100" type="text" name="username" autocomplete="off">
                        <span class="focus-input100"></span>
                    </div>

                    <div class="p-t-13 p-b-9">
                        <span class="txt1">
                            Mật khẩu
                        </span>
                        <!-- <a href="https://finalstyle.com/" target="_blank" class="txt2 bo1 m-l-5">
                            Quên mật khẩu?
                        </a> -->
                    </div>

                    <div class="wrap-input100 validate-input" data-validate = "Vui lòng nhập mật khẩu.">
                        <input class="input100" type="password" name="password" autocomplete="off">
                        <span class="focus-input100"></span>
                    </div>

                    <div class="container-login100-form-btn m-t-17">
                        <button class="login100-form-btn">
                            ĐĂNG NHẬP
                        </button>
                    </div>

                    <div class="w-full text-center p-t-30">
                        <span class="txt2">
                            Thiết kế website
                        </span>
                        <a href="https://finalstyle.com/" target="_blank" class="txt2 bo1">
                            Finalstyle
                        </a>
                    </div>
                    <input name="action" type="hidden" value="login"/>
                </form>
            </div>
        </div>
    </div>
    <script src="<?php echo URL_ROOT . 'libraries/jquery/jquery-1.12.4.min.js' ?>"></script>
    <script type="text/javascript" src="templates/default/js/login.js"></script>
</body>
</html>