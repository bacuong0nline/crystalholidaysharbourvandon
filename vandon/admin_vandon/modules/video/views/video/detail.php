<?php
$title = @$data ? FSText :: _('Edit'): FSText :: _('Add'); 
global $toolbar;
$toolbar->setTitle($title);
$toolbar->addButton('apply',FSText :: _('Apply'),'','apply.png'); 
$toolbar->addButton('Save',FSText :: _('Save'),'','save.png'); 
$toolbar->addButton('back',FSText :: _('Cancel'),'','back.png');   

	$this -> dt_form_begin();
	
	TemplateHelper::dt_edit_text(FSText :: _('Title'),'title',@$data -> title);
	TemplateHelper::dt_edit_text(FSText :: _('Alias'),'alias',@$data -> alias,'',60,1,0,FSText::_("Can auto generate"));
	TemplateHelper::dt_edit_text(FSText :: _('link youtube'),'link_video',@$data -> link_video,'',100);
	TemplateHelper::dt_edit_image(FSText :: _('Thumbnail'),'image',str_replace('/original/','/small/',URL_ROOT.@$data->image), '', '', 'Nếu không sử dụng sẽ lấy mặc định');
	// TemplateHelper::dt_edit_file(FSText :: _('Video'),'file_upload',@$data -> file_upload, );
	TemplateHelper::dt_edit_choose_file(FSText::_('Video'), 'file_upload', @$data->file_upload, 'Video upload sẽ được ưu tiên xuất hiện');

	TemplateHelper::dt_checkbox(FSText::_('Published'),'published',@$data -> published,1);
	TemplateHelper::dt_edit_text(FSText :: _('Ordering'),'ordering',@$data -> ordering,@$maxOrdering,'20');
//	TemplateHelper::dt_edit_text(FSText :: _('Summary'),'summary',@$data -> summary,'',100,9);
	$this -> dt_form_end(@$data,1,0);

?>
<script>
	$(".upload").change(function() {
		console.log(1);
	})
</script>