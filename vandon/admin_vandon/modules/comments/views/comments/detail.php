<link type="text/css" rel="stylesheet" media="all" href="../libraries/jquery/jquery.ui/jquery-ui.css" />
<script type="text/javascript" src="../libraries/jquery/jquery.ui/jquery-ui.js"></script>
<?php
$title = @$data ? FSText::_('Edit') : FSText::_('Add');
global $toolbar;
$toolbar->setTitle($title);
$toolbar->addButton('save_add', FSText::_('Save and new'), '', 'save_add.png', 1);
$toolbar->addButton('apply', FSText::_('Apply'), '', 'apply.png', 1);
$toolbar->addButton('Save', FSText::_('Save'), '', 'save.png', 1);
$toolbar->addButton('back', FSText::_('Cancel'), '', 'cancel.png');

echo ' 	<div class="alert alert-danger" style="display:none" >
                    <span id="msg_error"></span>
            </div>';
//$this -> dt_form_begin(1,4,$title.' '.FSText::_('News'));
$this->dt_form_begin(1, 4, $title . ' ' . FSText::_('News'), 'fa-edit', 1, 'col-md-8', 1);
// TemplateHelper::dt_edit_selectbox(FSText::_('Danh xưng'), 'title', @$data->title, 0, array(0=> "--Chọn danh xưng--" ,1 => 'Ông', 2 => 'Bà'), $id);
TemplateHelper::dt_edit_text(FSText::_('Tên'), 'name', @$data->name);
// TemplateHelper::dt_edit_text(FSText::_('Địa chỉ'), 'address', @$data->address);

TemplateHelper::dt_edit_text(FSText::_('Alias'), 'alias', @$data->alias, '', 60, 1, 0, FSText::_("Can auto generate"));
TemplateHelper::dt_edit_image(FSText::_('Hình ảnh'), 'image', str_replace('/original/', '/small/', URL_ROOT . @$data->image));
TemplateHelper::dt_edit_selectbox(FSText::_('Categories'),'category_id',@$data -> category_id,0,$categories,$field_value = 'id', $field_label='treename',$size = 10,0,1);

TemplateHelper::datetimepicke(FSText::_('Published time'), 'created_time', @$data->created_time ? @$data->created_time : date('Y-m-d H:i:s'), FSText::_('Bạn vui lòng chọn thời gian hiển thị'), 20, '', 'col-md-3', 'col-md-4');
$this->dt_form_end_col(); 


$this->dt_form_begin(1, 2, FSText::_('Kích hoạt'), 'fa-unlock', 1, 'col-md-4 fl-right');
TemplateHelper::dt_checkbox(FSText::_('Published'), 'published', @$data->published, 1, '', '', '', 'col-sm-4', 'col-sm-8');
TemplateHelper::dt_edit_text(FSText::_('Ordering'), 'ordering', @$data->ordering, @$maxOrdering, '', '', 0, '', '', 'col-sm-4', 'col-sm-8');
$this->dt_form_end_col(); // END: col-2


$this->dt_form_begin(1, 4, FSText::_('Content'), 'fa-info', 1, 'col-md-8');
TemplateHelper::dt_edit_text(FSText::_(''), 'comment', @$data->comment, '', 650, 5, 0, '', '', 'col-sm-2', 'col-sm-12');
$this->dt_form_end_col(); // END: col-4


$this->dt_form_end(@$data, 1, 0, 2, 'Cấu hình seo', '', 1, 'col-sm-4');
?>
<script type="text/javascript">
    $('.form-horizontal').keypress(function(e) {
        if (e.which == 13) {
            formValidator();
            return false;
        }
    });

    function formValidator() {
        $('.alert-danger').show();



        $('.alert-danger').hide();
        return true;
    }
</script>
<?php //include 'detail_seo.php'; 
?>