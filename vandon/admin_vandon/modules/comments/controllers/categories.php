<?php
	class CommentsControllersCategories extends Controllers{
	
		function __construct()
		{
			$this->view = 'comments' ; 
			parent::__construct(); 
		}
		function display()
		{
			parent::display();
			$sort_field = $this -> sort_field;
			$sort_direct = $this -> sort_direct;
			
			$model  = $this -> model;
			$list = $model->get_data('');
			$pagination = $model->getPagination('');
			$categories = $model->get_categories_tree();
			// $education_categories = $model->get_education_categories_tree();

			// print_r($categories);die;
			include 'modules/'.$this->module.'/views/'.$this->view.'/list.php';
		}

		function add() {
			$model  = $this -> model;
			$maxOrdering = $model->getMaxOrdering();
			// $education_categories = $model->get_education_categories_tree();
			$categories = $model->get_categories_tree();
			include 'modules/'.$this->module.'/views/'.$this->view.'/detail.php';

		}

		function edit() {
			$model  = $this -> model;
			$maxOrdering = $model->getMaxOrdering();

			$categories = $model->get_categories_tree();
			// $education_categories = $model->get_education_categories_tree();

			$ids = FSInput::get('id',array(),'array');
			$id = $ids[0];
			$data = $model->get_record_by_id($id);

			include 'modules/'.$this->module.'/views/'.$this->view.'/detail.php';
		}
	}
?>
