<?php
class CommentsModelsComments extends FSModels
{

	var $limit;
	var $prefix;
	function __construct()
	{
		$this->limit = 20;
		$this->view = 'comments';
		$this->table_name = FSTable_ad::_('fs_comments',1);;
		$this->table_name_categories = FSTable_ad::_('fs_comments_categories',1);;

		$this -> arr_img_paths = array(
			array('lange',632,423,'resize_image'),
			array('resized',359,240,'resize_image'),
			array('small',112,75,'cut_image')
		);
		$cyear = date('Y');
		$cmonth = date('m');
		$this -> img_folder = 'images/comments/'.$cyear.'/'.$cmonth;

		parent::__construct();
	}
	function setQuery()
	{
		$ordering = "";
		$where = "  ";
		if (isset($_SESSION[$this->prefix . 'sort_field'])) {
			$sort_field = $_SESSION[$this->prefix . 'sort_field'];
			$sort_direct = $_SESSION[$this->prefix . 'sort_direct'];
			$sort_direct = $sort_direct ? $sort_direct : 'asc';
			$ordering = '';
			if ($sort_field)
				$ordering .= " ORDER BY $sort_field $sort_direct,a.parent_id, created_time DESC, id DESC ";
		}
		if (!$ordering)
			$ordering .= " ORDER BY created_time DESC , id DESC ";

		if (isset($_SESSION[$this->prefix . 'keysearch'])) {
			if ($_SESSION[$this->prefix . 'keysearch']) {
				$keysearch = $_SESSION[$this->prefix . 'keysearch'];
				$where .= " AND a.name LIKE '%" . $keysearch . "%' ";
			}
		}
		// estore
		if (isset($_SESSION[$this->prefix . 'filter0'])) {
			$filter = $_SESSION[$this->prefix . 'filter0'];
			if ($filter) {
				$where .= ' AND a.category_id =  ' . $filter . ' ';
			}
		}

		$gid = FSInput::get('gid');
		//			$where = " WHERE  show_admin = 1 OR group_id IS NOT NULL ";
		//			if($gid)
		//				$where .= "  AND = $gid ";
		$query = " SELECT a.*
						  FROM " . $this->table_name . " as a WHERE 1=1" .
			$where .
			$ordering . " ";
		return $query;
	}
	function save($row = array(), $use = 1) {
		$category_id = FSInput::get('category_id',0,'int');
		$cat =  $this->get_record_by_id($category_id,$this -> table_name_categories);
		$row['category_id_wrapper'] = $cat -> list_parents;
		$row['category_alias_wrapper'] = $cat -> alias_wrapper;
		$row['category_name'] = $cat -> name;
		$row['category_id'] = $category_id;
		$row['category_alias'] = $cat -> alias;


		$rs = parent::save($row, 1);
		return $rs;
	}

	function get_categories_tree()
	{
		global $db;
		$query = " SELECT a.*
					  FROM 
						  ".$this -> table_name_categories." AS a
						  WHERE published = 1 ORDER BY ordering ";         
		$sql = $db->query($query);
		$result = $db->getObjectList();
		$tree  = FSFactory::getClass('tree','tree/');
		$list = $tree -> indentRows2($result);
		return $list;
	}
}
