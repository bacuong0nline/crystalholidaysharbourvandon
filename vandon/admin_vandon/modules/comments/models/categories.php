<?php
class CommentsModelsCategories extends FSModels
{

	var $limit;
	var $prefix;
	function __construct()
	{
		$this->limit = 20;
		$this->view = 'comments';
		$this->table_name = FSTable_ad::_('fs_comments_categories',1);
		$this->table_education_name = FSTable_ad::_('fs_education_categories',1);;

		$this -> arr_img_paths = array(
			array('lange',632,423,'resize_image'),
			array('resized',359,240,'resize_image'),
			array('small',112,75,'cut_image')
		);
		parent::__construct();
	}

	function save($row = array(), $use = 1) {
		$category_id = FSInput::get('category_id',0,'int');
		$cat =  $this->get_record_by_id($category_id,$this -> table_name);
		$row['category_id_wrapper'] = $cat -> list_parents;
		$row['category_alias_wrapper'] = $cat -> alias_wrapper;
		$row['category_name'] = $cat -> name;
		$row['category_id'] = $category_id;
		$row['category_alias'] = $cat -> alias;

		$products_related = $color = FSInput::get('education_categories', array(), 'array');
        $str_products_related = implode(',', $products_related);
        if ($str_products_related) {
            $str_products_related = ',' . $str_products_related . ',';
        }
        $row['education_categories'] = $str_products_related;

		$rs = parent::save($row, 1);
		return $rs;
	}

	function get_categories_tree()
	{
		global $db;
		$query = " SELECT a.*
					  FROM 
						  ".$this -> table_name." AS a
						  WHERE published = 1 ORDER BY ordering ";         
		$sql = $db->query($query);
		$result = $db->getObjectList();
		$tree  = FSFactory::getClass('tree','tree/');
		$list = $tree -> indentRows2($result);
		return $list;
	}

	function get_education_categories_tree()
	{
		global $db;
		$query = " SELECT a.*
					  FROM 
						  ".$this -> table_education_name." AS a
						  WHERE published = 1 ORDER BY ordering ";         
		$sql = $db->query($query);
		$result = $db->getObjectList();
		$tree  = FSFactory::getClass('tree','tree/');
		$list = $tree -> indentRows2($result);
		return $list;
	}
}
