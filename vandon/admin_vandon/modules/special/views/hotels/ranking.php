<?php
TemplateHelper::dt_edit_text(FSText::_('Số lượt reviews'), 'reviews', @$data->reviews);
TemplateHelper::dt_edit_text(FSText::_('Tổng điểm'), 'ranking', @$data->ranking);
TemplateHelper::dt_edit_text(FSText::_('Vị trí'), 'pos_ranking', @$data->pos_ranking);
TemplateHelper::dt_edit_text(FSText::_('Sạch sẽ'), 'clean_ranking', @$data->clean_ranking);
TemplateHelper::dt_edit_text(FSText::_('Dịch vụ'), 'service_ranking', @$data->service_ranking);
TemplateHelper::dt_edit_text(FSText::_('Giá tiền'), 'price_ranking', @$data->price_ranking);
