<div id="qna-box">
    <?php if (!empty($list_qna)) { ?>
        <?php foreach ($list_qna as $item) { ?>
            <div class="panel panel-default qna-wrap" style="display: flex; flex-direction: column; padding: 15px">
                <button data-id="<?php echo $item->id ?>" type="button" class="btn btn-outline delete-item-qna" style="position: absolute; right: 0px; top: 0px; color: red">
                    <i class="fa fa-times"></i>
                </button>
                <div>
                    <label for="name_question"><?php echo FSText::_("Tiêu đề câu hỏi") ?></label>
                    <input maxlength="105" type="text" name="name_question[]" class="form-control title-qna" value="<?php echo $item->name ?>" />
                    <p class="count-text" style="margin-bottom: 4px; color: red; font-size: 12px">Đã nhập <?php echo strlen($item->name) . '/' . '105' ?> kí tự</p>
                </div>
                <div style="padding-top: 10px">
                    <label for="description_question"><?php echo FSText::_("Nội dung") ?></label>
                    <textarea name="description_question[]" id="description_question" class="form-control" id="" cols="30" rows="5"><?php echo $item->description ?></textarea>
                </div>
                <input value="<?php echo $item->id ?>" type="hidden" name="id_question[]" placeholder="<?php echo FSText::_($title) ?>" />
            </div>
        <?php } ?>
    <?php } ?>
</div>
<button type="button" class="btn btn-primary add-question"><?php echo FSText::_("Thêm câu hỏi") ?></button>

<div class="alert alert-success" style="position: fixed; bottom: 0px; right: 40px; display: none">
    <strong>Success!</strong> Indicates a successful or positive action.
</div>

<div class="alert alert-danger" style="position: fixed; bottom: 0px; right: 40px; display: none">
    <strong>Success!</strong> Indicates a successful or positive action.
</div>

<script>
    let html_qna = `
    <div class="panel panel-default qna-wrap" style="display: flex; flex-direction: column; padding: 15px">
        <button data-id="" type="button" class="btn btn-outline delete-item-qna" style="position: absolute; right: 0px; top: 0px; color: red">
            <i class="fa fa-times"></i>
        </button>
        <div>
            <label for="name_question"><?php echo FSText::_("Tiêu đề câu hỏi") ?></label>
            <input maxlength="105" type="text" name="name_question[]" class="form-control title-qna" />
            <p class="count-text" style="margin-bottom: 4px; color: red; font-size: 12px">Đã nhập 0/104 kí tự</p>
        </div>
        <div style="padding-top: 10px">
            <label for="description_question"><?php echo FSText::_("Nội dung") ?></label>
            <textarea name="description_question[]" class="form-control" id="" cols="30" rows="5"></textarea>
        </div>
    </div>
    `
    $(".add-question").click(function() {
        $("#qna-box").append(html_qna);
    })
    let time_out_qna;
    $(document).on("click", ".delete-item-qna", function() {
        clearTimeout(time_out_qna);
        $(this).parent().remove();
        id_qna = $(this).attr("data-id");
        if (id_qna)
            $.ajax({
                url: `index.php?module=hotels&view=hotels&task=deleteItemQna&raw=1&id=${id_qna}`,
                success: function(res) {
                    let response = JSON.parse(res);
                    if (!response.error) {
                        $(".alert-success").text(response.message);
                        $(".alert-success").css("display", "block");
                        time_out = setTimeout(function() {
                            $(".alert-success").css("display", "none");
                        }, 1000)
                    } else {
                        $(".alert-danger").text(response.message);
                        $(".alert-danger").css("display", "block");
                        time_out = setTimeout(function() {
                            $(".alert-danger").css("display", "none");
                        }, 1000)
                    }
                }
            })
    })

    function debounce(func, timeout = 300) {
        let timer;
        return (...args) => {
            clearTimeout(timer);
            timer = setTimeout(() => {
                func.apply(this, args);
            }, timeout);
        };
    }
    function textLength(value) {
        var maxLength = 144;
        if (value < maxLength) return false;
        return true;
    }
    $(document).on('keyup', '.title-qna', function() {
        if (!textLength($(this).val().length))
            $(this).next().text('Đã nhập ' + $(this).val().length + '/' + 105 + ' ký tự');
    })

</script>

<style>
    body {
        counter-reset: section;
    }

    .qna-wrap::before {
        counter-increment: section;
        content: ""counter(section) "";
        position: absolute;
        bottom: 0px;
        right: 0px;
        font-size: 180px;
        font-weight: bold;
        color: #eeeeee7a;
        line-height: 130px;
        z-index: 0;
    }

    .qna-wrap {
        position: relative;
        transition: .3s;
    }

    .qna-wrap:hover {
        background-color: #eeeeee40;
        transition: .3s;
    }
</style>