<?php
TemplateHelper::dt_edit_text(FSText::_('Tên'), 'name', @$data->name);
TemplateHelper::dt_edit_text(FSText::_('Alias'), 'alias', @$data->alias, '', 60, 1, 0, FSText::_("Can auto generate"));
TemplateHelper::dt_edit_text(FSText::_('Mã Khách sạn'), 'code', @$data->code, '', '', 1, 0, FSText::_("Can auto generate"));

TemplateHelper::dt_edit_image(FSText::_('Hình ảnh'), 'image', str_replace('/original/', '/original/', URL_ROOT . @$data->image), '30', '30');
TemplateHelper::dt_edit_image(FSText::_('Ảnh chia sẻ'), 'og_image', str_replace('/original/', '/original/', URL_ROOT . @$data->og_image), '30', '30');
TemplateHelper::dt_edit_selectbox(FSText::_('Thành phố'), 'city_id', @$data->city_id, 0, $cities, $field_value = 'id', $field_label = 'treename', $size = 1, 0, 1, '', '');
TemplateHelper::dt_edit_selectbox(FSText::_('Quận huyện'), 'district_id', @$data->district_id, 0, $district, $field_value = 'id', $field_label = 'treename', $size = 1, 0, 1, '', '');
TemplateHelper::dt_edit_selectbox(FSText::_('Loại Khách sạn'), 'complex_id', @$data->complex_id, 0, $complex, $field_value = 'id', $field_label = 'treename', $size = 1, 0, 1, '', '');
TemplateHelper::dt_edit_selectbox(FSText::_('Tiện ích ưa chuộng nhất'), 'favourite_service', @$data->favourite_service, 0, $services, $field_value = 'id', $field_label = 'title', $size = 1, 1, 1, '', '');

TemplateHelper::dt_edit_text(FSText::_('Hotline'), 'telephone', @$data->telephone, '', '', '', 0, '', '', 'col-sm-3', 'col-sm-9');
TemplateHelper::dt_edit_text(FSText::_('Email'), 'email', @$data->email, '', '', '', 0, '', '', 'col-sm-3', 'col-sm-9');
TemplateHelper::dt_edit_text(FSText::_('Địa chỉ'), 'address', @$data->address, '', '', '', 0, '', '', 'col-sm-3', 'col-sm-9');
TemplateHelper::dt_checkbox(FSText::_('Published'), 'published', @$data->published, 1);
TemplateHelper::datetimepicke(FSText::_('Published time'), 'created_time', @$data->created_time ? @$data->created_time : date('Y-m-d H:i:s'), FSText::_('Bạn vui lòng chọn thời gian hiển thị'), 20, '', 'col-sm-3', 'col-sm-5');
TemplateHelper::dt_edit_text(FSText::_('Ordering'), 'ordering', @$data->ordering, @$maxOrdering, '', '', 0, '', '', 'col-sm-3', 'col-sm-5');

TemplateHelper::dt_edit_text(FSText::_('email_cc'), 'email_cc', @$data->email_cc,'','','10','','');
?>

<?php
echo '<div style="border: 1px solid #d1d1d1; margin-bottom: 15px">';
echo '<label class="col-md-12 left col-xs-12 control-label">Ảnh khác</label>';
TemplateHelper::dt_edit_image2(FSText::_('Ảnh khác'), 'image', str_replace('/original/', '/resized/', URL_ROOT . @$data->image), '', '', '', 1, '', 'col-md-12', 'col-md-12');
echo '</div>';
?>
