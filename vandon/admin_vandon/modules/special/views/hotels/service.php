<link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">

<div class="services-wrapper" style="display: flex; flex-wrap: wrap; grid-gap: 10px 15px">
    <?php if (!empty($hotel_service)) { ?>
        <?php foreach ($hotel_service as $key => $val) { ?>
            <div class="panel panel-default" style="width: 49%; padding-top: 20px">
                <div class="panel-body" style="position: relative">
                    <button data-hotel=<?php echo $data->id ?> data-id="<?php echo $key ?>" type="button" class="btn btn-outline delete-item-service" style="position: absolute; right: 0px; top: -20px; color: red">
                        <i class="fa fa-times"></i>
                    </button>
                    <select data-service-item="<?php echo $val->service_item ?>" data-classify="<?php echo "select-service-item-" . $key ?>" name="service[]" id="" data-placeholder="Tiện ích" class="form-control chosen-select chosen-select-deselect select-service <?php echo "service_item_" . $key ?>">
                        <?php foreach ($services as $item) { ?>
                            <option <?php echo $val->category_id == $item->id ? 'selected' : null ?> value="<?php echo $item->id ?>"><?php echo $item->title ?></option>
                        <?php } ?>
                    </select>
                    <div style="height: 20px;"></div>
                    <select name="<?php echo "service_item_" . $key ?>[]" id="" multiple data-placeholder="Chi tiết tiện ích" class="form-control chosen-select chosen-select-deselect <?php echo "select-service-item-" . $key ?>">

                    </select>
                </div>
                <input type="hidden" name="position-service[]" value="<?php echo $key ?>" />
            </div>
        <?php } ?>
    <?php } ?>
</div>


<input type="hidden" id="count-service" value="<?php echo !empty($hotel_service) ? count($hotel_service) : 0 ?>" />

<button data-classify="select-service-item-<?php echo !empty($hotel_service) ? count($hotel_service) : 0 ?>" type="button" class="btn btn-primary add-more-service"><?php echo FSText::_("Thêm tiện ích") ?></button>

<script>
    $(document).ready(function() {
        let count = $("#count-service").val();
        let html1 = '';
        let classify = '';
        let select_id;
        let html = '';
        let select_item = '';
        $(document).on("click", ".add-more-service", function() {
            classify = $(this).attr("data-classify")
            html1 = `
    <div class="panel panel-default" style="width: 49%; padding-top: 20px">
        <div class="panel-body" style="position: relative">
            <button type="button" class="btn btn-outline delete-item-service" style="position: absolute; right: 0px; top: -20px; color: red">
                <i class="fa fa-times"></i>
            </button>
            <select data-classify="select-service-item-${count}" name="service[]" id="" data-placeholder="Tiện ích" class="form-control chosen-select chosen-select-deselect select-service">
                <option value="" disabled selected>Chọn tiện ích</option>
                <?php foreach ($services as $item) { ?>
                    <option value="<?php echo $item->id ?>"><?php echo $item->title ?></option>
                <?php } ?>
            </select>
            <div style="height: 20px;"></div>
            <select name="service_item_${count}[]" id="" multiple data-placeholder="Chi tiết tiện ích" class="form-control chosen-select chosen-select-deselect select-service-item-${count}">

            </select>
        </div>
    </div>
    `;
            $(".services-wrapper").append(html1);
            $(`.select-service`).chosen();
            $(`.${classify}`).chosen();
            count++;
            $(this).attr("data-classify", `select-service-item-${count}`)
        })


        $(".select-service").each(function(i, item) {

            // console.log(classify)
            $.ajax({
                url: `index.php?module=hotels&view=hotels&task=services_hotel&id=${$(item).val()}&raw=1`,
                success: function(res) {
                    html = '';
                    select_item = $(item).attr("data-service-item")
                    let data = JSON.parse(res);
                    data.map(item =>
                        html += `<option ${select_item.includes(item.id) ? 'selected' : null} value="${item.id}">${item.title}</option>`
                    )
                    classify = $(item).attr("data-classify")
                    $(`.${classify}`).html(html)
                    $(`.${classify}`).trigger("chosen:updated");
                }
            })
        })
        $(document).on("change", ".select-service", function() {
            classify = $(this).attr("data-classify")
            select_id = $(this).val();
            $.ajax({
                url: `index.php?module=hotels&view=hotels&task=services_hotel&id=${select_id}&raw=1`,
                success: function(res) {
                    html = '';
                    let data = JSON.parse(res);
                    data.map(item =>
                        html += `<option value="${item.id}">${item.title}</option>`
                    )
                    $(`.${classify}`).html(html)
                    $(`.${classify}`).find('option').prop('selected', true);
                    $(`.${classify}`).trigger("chosen:updated");
                }
            })
        })

        $(document).on("click", ".delete-item-service", function() {
            clearTimeout(time_out_qna);
            $(this).parent().parent().remove();
            pos = $(this).attr("data-id");
            hotel_id = $(this).attr("data-hotel");
            if (pos)
                $.ajax({
                    url: `index.php?module=hotels&view=hotels&task=deleteItemService&raw=1&id=${pos}&hotel_id=${hotel_id}`,
                    success: function(res) {
                        let response = JSON.parse(res);
                        if (!response.error) {
                            $(".alert-success").text(response.message);
                            $(".alert-success").css("display", "block");
                            time_out = setTimeout(function() {
                                $(".alert-success").css("display", "none");
                            }, 1000)
                        } else {
                            $(".alert-danger").text(response.message);
                            $(".alert-danger").css("display", "block");
                            time_out = setTimeout(function() {
                                $(".alert-danger").css("display", "none");
                            }, 1000)
                        }
                    }
                })
        })
    })
</script>