<link type="text/css" rel="stylesheet" media="all" href="../libraries/jquery/jquery.ui/jquery-ui.css" />
<script type="text/javascript" src="../libraries/jquery/jquery.ui/jquery-ui.js"></script>
<style>
    .table>tbody>tr>td {
        vertical-align: middle;
        padding-top: 10px;
        padding-bottom: 10px;
    }

    .table>tbody>tr:nth-child(even) {
        background: #f1f1f1;
    }

    .table>tbody>tr:hover {
        background: #ccc;
    }

    .table .a_table {
        white-space: nowrap;
    }
</style>


<?php
$title = @$data ? FSText::_('Edit') : FSText::_('Add');
global $toolbar;
$toolbar->setTitle($title);
$toolbar->addButton('save_add', FSText::_('Save and new'), '', 'save_add.png', 1);
$toolbar->addButton('apply', FSText::_('Apply'), '', 'apply.png', 1);
$toolbar->addButton('Save', FSText::_('Save'), '', 'save.png', 1);
$toolbar->addButton('back', FSText::_('Cancel'), '', 'cancel.png');

echo ' 	<div class="alert alert-danger" style="display:none" >
                    <span id="msg_error"></span>
            </div>';

$this->dt_form_begin(1, 4, $title . ' ' . FSText::_('Khách sạn'), 'fa-edit', 1, 'col-md-12', 1);
?>
<div id="tabs">
    <ul>
        <li><a href="#tabs-1"><?php echo FSText::_("Thông tin khách sạn")?></a></li>
        <li><a href="#tabs-2"><?php echo FSText::_("Tổng quan")?></a></li>
        <!-- <li><a href="#tabs-3"><?php echo FSText::_("Ưu đãi")?></a></li> -->
        <li><a href="#tabs-4"><?php echo FSText::_("Dịch vụ tiện ích")?></a></li>
        <li><a href="#tabs-5"><?php echo FSText::_("Vị trí")?></a></li>
        <li><a href="#tabs-6"><?php echo FSText::_("Hỏi đáp")?></a></li>
        <li><a href="#tabs-7"><?php echo FSText::_("Quy định")?></a></li>
        <li><a href="#tabs-8"><?php echo FSText::_("Đánh giá")?></a></li>
        <li><a href="#tabs-9"><?php echo FSText::_("Thông tin Booking")?></a></li>
        <li><a href="#tabs-10"><?php echo FSText::_("SEO")?></a></li>
    </ul>
    <div id="tabs-1">
        <?php include_once('detail_base.php') ?>
    </div>
    <div id="tabs-2">
        <?php include_once('overview.php') ?>
    </div>
    <!-- <div id="tabs-3">
        <p>Mauris eleifend est et turpis. Duis id erat. Suspendisse potenti. Aliquam vulputate, pede vel vehicula accumsan, mi neque rutrum erat, eu congue orci lorem eget lorem. Vestibulum non ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sodales. Quisque eu urna vel enim commodo pellentesque. Praesent eu risus hendrerit ligula tempus pretium. Curabitur lorem enim, pretium nec, feugiat nec, luctus a, lacus.</p>
        <p>Duis cursus. Maecenas ligula eros, blandit nec, pharetra at, semper at, magna. Nullam ac lacus. Nulla facilisi. Praesent viverra justo vitae neque. Praesent blandit adipiscing velit. Suspendisse potenti. Donec mattis, pede vel pharetra blandit, magna ligula faucibus eros, id euismod lacus dolor eget odio. Nam scelerisque. Donec non libero sed nulla mattis commodo. Ut sagittis. Donec nisi lectus, feugiat porttitor, tempor ac, tempor vitae, pede. Aenean vehicula velit eu tellus interdum rutrum. Maecenas commodo. Pellentesque nec elit. Fusce in lacus. Vivamus a libero vitae lectus hendrerit hendrerit.</p>
    </div> -->
    <div id="tabs-4">
        <?php include_once('service.php') ?>
    </div>
    <div id="tabs-5">
        <?php include_once('address.php') ?>
    </div>
    <div id="tabs-6">
        <?php include_once('qna.php') ?>
    </div>
    <div id="tabs-7">
        <?php include_once('regulation.php') ?>
    </div>
    <div id="tabs-8">
        <?php include_once('ranking.php') ?>
    </div>
    <div id="tabs-9">
        <?php 
        TemplateHelper::dt_edit_text(FSText::_('Tuổi trẻ em'), 'children', @$data->children);
        TemplateHelper::dt_edit_text(FSText::_('Tuổi em bé'), 'infant', @$data->infant);
        TemplateHelper::dt_edit_text(FSText::_('Check In'), 'check_in', @$data->check_in);
        TemplateHelper::dt_edit_text(FSText::_('Check Out'), 'check_out', @$data->check_out);
        ?>
    </div>
    <div id="tabs-10">
        <?php include_once('detail_seo.php') ?>
    </div>
</div>

<?php
$this->dt_form_end_col(); // END: col-1
?>
<input type="hidden" value="" name="field_remove" id="field_remove" />
<input type="hidden" value="<?php echo isset($data) ? count($list_filter) : 0; ?>" name="field_extend_exist_total" id="field_extend_exist_total" />
<input type="hidden" value="" name="new_field_total" id="new_field_total" />
<?php
$this->dt_form_end(@$data, 1, 0, 2, 'Cấu hình seo', '', 1, 'col-sm-4');
?>
<script type="text/javascript">
    $(function() {
        $("#tabs").tabs();
    });
    // $('.form-horizontal').keypress(function(e) {
    //     if (e.which == 13) {
    //         formValidator();
    //         return false;
    //     }
    // });

    function formValidator() {
        $('.alert-danger').show();

        if (!notEmpty('name', 'Bạn phải nhập tiêu đề'))
            return false;
        
        if ($('#ranking').val() > 5) {
            document.getElementById('msg_error').innerHTML = 'Tổng điểm phải nhỏ hơn hoặc bằng 5'
            return false;
        }
        if ($('#pos_ranking').val() > 5) {
            document.getElementById('msg_error').innerHTML = 'Điểm vị trí nhỏ hơn hoặc bằng 5'
            return false;
        }
        if ($('#clean_ranking').val() > 5) {
            document.getElementById('msg_error').innerHTML = 'Điểm sạch sẽ nhỏ hơn hoặc bằng 5'
            return false;
        }
        if ($('#price_ranking').val() > 5) {
            document.getElementById('msg_error').innerHTML = 'Điểm giá tiền nhỏ hơn hoặc bằng 5'
            return false;
        }
        $('.alert-danger').hide();
        return true;
    }
</script>
<?php //include 'detail_seo.php'; 
?>

<script>
    $(document).ready(function() {

    });

    var i = 0;

    function addField() {
        area_id = "#new_record_" + i;

        let html = `
        <td>
            <input type="text" class="form-control" name="new_name${i}" id="new_name${i}" placeholder="Tên">
        </td>
        <td>
            <input type="text" class="form-control" name="new_order${i}" id="new_order${i}" placeholder="Thứ tự" value="0">
        </td>
        <td>
            <a class="a_table" href="javascript: void(0)" onclick="javascript: remove_new_field(${i})">
                <i class="fa fa-trash"></i> Xóa
            </a>
        </td>
        `;

        // $(area_id).html(htmlString);
        $(area_id).append(html).css('display', 'table-row');
        i++;
        $("#new_field_total").val(i);
    }

    //remove extend field exits
    function remove_extend_field(area, fieldid) {
        if (confirm("You certain want remove this field")) {
            remove_field = "";
            remove_field = $('#field_remove').val();
            remove_field += "," + fieldid;
            $('#field_remove').val(remove_field);
            $('#extend_field_exist_' + area).css("display", 'none');
        }
        return false;
    }

    //remove new extend field
    function remove_new_field(area) {
        if (confirm("You certain want remove this field")) {
            area_id = "#new_record_" + area;
            $(area_id).remove();
        }
        return false;
    }
</script>