<link type="text/css" rel="stylesheet" media="all" href="../libraries/jquery/jquery.ui/jquery-ui.css"/>
<script type="text/javascript" src="../libraries/jquery/jquery.ui/jquery-ui.js"></script>
<style>
    .table > tbody > tr > td {
        vertical-align: middle;
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .table > tbody > tr:nth-child(even) {
        background: #f1f1f1;
    }
    .table > tbody > tr:hover{
        background: #ccc;
    }
    .table .a_table{
        white-space: nowrap;
    }
    .table .check-box{
        width: 20px;
        height: 20px;
    }
</style>
<?php
$title = @$data ? FSText:: _('Edit') : FSText:: _('Add');
global $toolbar;
$toolbar->setTitle($title);
$toolbar->addButton('save_add', FSText:: _('Save and new'), '', 'save_add.png', 1);
$toolbar->addButton('apply', FSText:: _('Apply'), '', 'apply.png', 1);
$toolbar->addButton('Save', FSText:: _('Save'), '', 'save.png', 1);
$toolbar->addButton('back', FSText:: _('Cancel'), '', 'cancel.png');

echo ' 	<div class="alert alert-danger" style="display:none" >
                    <span id="msg_error"></span>
            </div>';
//$this -> dt_form_begin(1,4,$title.' '.FSText::_('News'));
$this->dt_form_begin(1, 4, $title . ' ' . FSText::_('Dịch vụ đi kèm'), 'fa-edit', 1, 'col-md-7', 1);
TemplateHelper::dt_edit_text(FSText:: _('Tên'), 'title', @$data->title);
TemplateHelper::dt_edit_text(FSText:: _('Alias'), 'alias', @$data->alias, '', 60, 1, 0, FSText::_("Can auto generate"));
TemplateHelper::dt_edit_image(FSText:: _('Hình ảnh'), 'image', str_replace('/original/', '/original/', URL_ROOT . @$data->image), '30', '30');
//TemplateHelper::dt_edit_selectbox(FSText::_('Categories'),'category_id',@$data -> category_id,0,$categories,$field_value = 'id', $field_label='treename',$size = 10,0,1);
// TemplateHelper::dt_checkbox(FSText::_('Loại'),'type',@$data -> type,'0',array('0'=>FSText :: _("Người"),'1'=>FSText :: _("Gói")));
TemplateHelper::dt_edit_text(FSText :: _('Giá'),'price',@$data -> price,'','',1,0,'','','col-sm-3','col-sm-9');
TemplateHelper::dt_edit_text(FSText :: _('Đơn vị'),'unit',@$data -> unit,'','',1,0,'','','col-sm-3','col-sm-9');

TemplateHelper::dt_edit_selectbox(FSText::_('Khách sạn'), 'hotel_id', @$data->hotel_id, 0, $hotel, $field_value = 'id', $field_label = 'treename', $size = 1, 1, 0, '', '', '', 'col-md-3', 'col-md-9');
TemplateHelper::dt_edit_text(FSText:: _('Mô tả'), 'summary', @$data->summary, '', '', '3', 0, '', '', 'col-sm-3', 'col-sm-9');

//TemplateHelper::datetimepicke( FSText :: _('Ngày bắt đầu' ), 'start_time', @$data->start_time?@$data->start_time:date('Y-m-d H:i:s'), FSText :: _('Bạn vui lòng chọn thời gian hiển thị'), 20,'','col-md-3','col-md-5');
//TemplateHelper::datetimepicke( FSText :: _('Ngày kết thúc' ), 'end_time', @$data->end_time?@$data->end_time:date('Y-m-d H:i:s'), FSText :: _('Bạn vui lòng chọn thời gian hiển thị'), 20,'','col-md-3','col-md-5');
//TemplateHelper::dt_checkbox(FSText::_('Published'),'published',@$data -> published,1,'','','','col-sm-3','col-sm-9');
//TemplateHelper::dt_checkbox(FSText::_('Lọc'),'is_filter',@$data -> is_filter,0,'','','','col-sm-3','col-sm-9');

//TemplateHelper::datetimepicke( FSText :: _('Published time' ), 'created_time', @$data->created_time?@$data->created_time:date('Y-m-d H:i:s'), FSText :: _('Bạn vui lòng chọn thời gian hiển thị'), 20,'','col-md-3','col-md-5');
//TemplateHelper::dt_edit_text(FSText :: _('Link video'),'video',@$data -> video,'',100,1,0,'','','col-sm-3','col-sm-9');
$this->dt_form_end_col(); // END: col-1

$this->dt_form_begin(1, 2, FSText::_('Quản trị'), 'fa-user', 1, 'col-md-5 fl-right');
TemplateHelper::dt_edit_text(FSText:: _('Ordering'), 'ordering', @$data->ordering, 0, '', '', 0, '', '', 'col-sm-3', 'col-sm-9');
TemplateHelper::dt_checkbox(FSText::_('Published'), 'published', @$data->published, 1, '', '', '', 'col-sm-3', 'col-sm-9');
TemplateHelper::datetimepicke(FSText:: _('Published time'), 'created_time', @$data->created_time ? @$data->created_time : date('Y-m-d H:i:s'), FSText:: _('Bạn vui lòng chọn thời gian hiển thị'), 20, '', 'col-md-3', 'col-md-9');

//        TemplateHelper::dt_text(FSText :: _('Người tạo'),@$data -> author,'','','','col-md-6','col-md-6');
//TemplateHelper::dt_text(FSText :: _('Thời gian tạo'),date('H:i:s d/m/Y',strtotime(@$data -> start_time)));
//        TemplateHelper::dt_text(FSText :: _('Người sửa cuối'),@$data -> author_last,'','','','col-md-6','col-md-6');
//TemplateHelper::dt_text(FSText :: _('Thời gian sửa'),date('H:i:s d/m/Y',strtotime(@$data -> end_time)));
$this->dt_form_end_col(); // END: col-4
$this->dt_form_begin(1, 2, FSText::_('Điều kiện đặt phòng'), 'fa-user', 1, 'col-md-12 fl-right');
TemplateHelper::dt_edit_text(FSText:: _(''), 'policy', @$data->policy, '', 650, 450, 1, '', '', 'col-sm-12', 'col-sm-12');
//TemplateHelper::dt_edit_text(FSText :: _(''),'content',@$data -> content,'',650,450,1,'','','col-sm-2','col-sm-12');

$this->dt_form_end_col(); // END: col-4
// include 'detail_price.php';
$this->dt_form_end(@$data, 1, 0, 2, 'Cấu hình seo', '', 1, 'col-sm-4');
?>
<script type="text/javascript">
    $('.form-horizontal').keypress(function (e) {
        if (e.which == 13) {
            formValidator();
            return false;
        }
    });

    function formValidator() {
        $('.alert-danger').show();
        if (!notEmpty('title', 'Bạn phải nhập tiêu đề'))
            return false;
        // if(!notEmpty('image','bạn phải nhập hình ảnh'))
		// 	return false;
        // if(!notEmpty('category_id','Bạn phải chọn danh mục'))
        //   return false;
        // if(!notEmpty('summary','Bạn phải nhập nội dung mô tả'))
        //   return false;
        // if (CKEDITOR.instances.content.getData() == '') {
        //     invalid("content", 'Bạn phải nhập nội dung chi tiết');
        //     return false;
        // }
        $('.alert-danger').hide();
        return true;
    }
</script>
<?php //include 'detail_seo.php'; ?>

<script>
    $(document).ready(function () {

    });

    var i = 0;
    function addField() {
        area_id = "#new_record_" + i;

        let html = `
        <td width="30%">
            <input type="text" class="form-control" name="new_name${i}" id="new_name${i}" placeholder="Tên dịch vụ">
        </td>
        <td>
            <input type="text" class="form-control" name="new_per_price${i}" id="new_per_price${i}" placeholder="Giá">
        </td>
        <td>
            <input type="text" class="form-control" name="new_unit${i}" id="new_unit${i}" placeholder="Đơn vị">
        </td>
        <td>
            <input type="text" class="form-control" name="new_order${i}" id="new_order${i}" placeholder="Thứ tự" value="0">
        </td>
        <td>
            <input type="checkbox" class="check-box" name="new_free${i}" id="new_free${i}">
        </td>
        <td width="">
            <a class="a_table" href="javascript: void(0)" onclick="javascript: remove_new_field(${i})">
                <i class="fa fa-trash"></i> Xóa
            </a>
        </td>
        `;

        // $(area_id).html(htmlString);
        $(area_id).append(html).css('display', 'table-row');
        i++;
        $("#new_field_total").val(i);
    }

    //remove extend field exits
    function remove_extend_field(area, fieldid) {
        if (confirm("You certain want remove this field")) {
            remove_field = "";
            remove_field = $('#field_remove').val();
            remove_field += "," + fieldid;
            $('#field_remove').val(remove_field);
            $('#extend_field_exist_' + area).css("display",'none');
        }
        return false;
    }

    //remove new extend field
    function remove_new_field(area) {
        if (confirm("You certain want remove this field")) {
            area_id = "#new_record_" + area;
            $(area_id).remove();
        }
        return false;
    }
</script>
