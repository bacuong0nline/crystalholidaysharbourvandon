<div class="col-md-12 col-xs-12">
    <div class="panel panel-info">
        <div class="panel-heading">
            <i class="fa fa-edit"></i> Bảng Giá dịch vụ
        </div>
        <div class="panel-body">
            <table class="table field_tbl">
                <thead>
                <tr>
                    <th>Tên</th>
                    <th>Giá VND</th>
                    <th>Đơn vị</th>
                    <th>Thứ tự</th>
                    <th>Miễn phí</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php if(!empty($list_price)) {?>
                    <?php foreach ($list_price as $j=>$item) {?>
                        <tr id="extend_field_exist_<?php echo $j ?>">
                            <td width="30%">
                                <input type="text" class="form-control" name="ex_name<?php echo $j ?>" id="ex_name<?php echo $j ?>" placeholder="Tên dịch vụ" value="<?php echo $item->name ?>">
                                <input type="hidden" name="be_name<?php echo $j ?>" value="<?php echo $item->name ?>">
                                <input type="hidden" name="id_exist_<?php echo $j ?>" value="<?php echo $item->id ?>">
                            </td>
                            <td>
                                <input type="text" class="form-control" name="ex_per_price<?php echo $j ?>" id="ex_per_price<?php echo $j ?>" placeholder="Giá" value="<?php echo format_money($item->per_price,'',0) ?>">
                                <input type="hidden" name="be_per_price<?php echo $j ?>" value="<?php echo $item->per_price ?>">
                            </td>
                            <td>
                                <input type="text" class="form-control" name="ex_unit<?php echo $j ?>" id="ex_unit<?php echo $j ?>" placeholder="Đơn vị" value="<?php echo $item->unit ?>">
                                <input type="hidden" name="be_unit<?php echo $j ?>" value="<?php echo $item->unit ?>">
                            </td>
                            <td>
                                <input type="text" class="form-control" name="ex_order<?php echo $j ?>" id="ex_order<?php echo $j ?>" placeholder="Thứ tự" value="<?php echo $item->ordering ?>">
                                <input type="hidden" name="be_order<?php echo $j ?>" value="<?php echo $item->ordering ?>">
                            </td>
                            <td>
                                <input class="check-box" type="checkbox" name="ex_free<?php echo $j ?>" <?php echo $item->is_free == 1 ? 'checked' : '' ?>>
                                <input type="hidden" name="be_free<?php echo $j ?>" value="<?php echo $item->is_free ?>">
                            </td>
                            <td>
                                <a class="a_table" href="javascript: void(0)" onclick="javascript: remove_extend_field(<?php echo $j ?>,'<?php echo $item->id; ?>')">
                                    <i class="fa fa-trash"></i> Xóa
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                <?php for ($i = 0; $i < 100; $i++) { ?>
                    <tr id="new_record_<?php echo $i; ?>" style="display: none"></tr>
                <?php } ?>
                </tbody>
            </table>
            <a style="color: #73c5fa;" href="javascript:void(0);" onclick="addField()">
                <?php echo FSText:: _("Thêm dịch vụ"); ?> <i class="fa fa-plus-square" aria-hidden="true"></i>
            </a>
        </div>
    </div>
</div>
<input type="hidden" value="" name="field_remove" id="field_remove"/>
<input type="hidden" value="<?php echo isset($data) ? count($list_price) : 0; ?>" name="field_extend_exist_total" id="field_extend_exist_total"/>
<input type="hidden" value="" name="new_field_total" id="new_field_total"/>
