<?php

class HotelsModelsHotels extends FSModels
{
    var $limit;
    var $prefix;

    function __construct()
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $this->limit = 30;
        $this->view = 'hotels';

        //$this -> table_types = 'fs_news_types';
        $this->arr_img_paths = array(
            array('large', 850, 480, 'resize_image_fix_height'),
            array('resized', 0, 0, 'resize_image'),
            array('small', 250, 200, 'resize_image_fix_height')
        );

        $this->arr_img_paths_other = array(
            array('large', 850, 480, 'resize_image_fix_height'),
            array('resize', 500, 350, 'resize_image_fix_height'),
            array('small', 250, 200, 'resize_image_fix_height')
        );

        $this->arr_image_og_paths = array(
            array('resized', 0, 0, 'resize_image'),
            array('small', 250, 200, 'resize_image_fix_height')
        );

        $this->table_category_name = FSTable_ad::_('fs_complex', 1);
        $this->table_category_news_name = FSTable_ad::_('fs_news_categories', 1);
        $this->table_services = FSTable_ad::_('fs_hotels_service', 1);
        $this->table_service_items = FSTable_ad::_('fs_hotels_service_items', 1);

        $this->table_name = FSTable_ad::_('fs_hotels', 1);
        $this->table_news = FSTable_ad::_('fs_news', 1);

        $this->table_filter = FSTable_ad::_('fs_hotels_filter', 1);
        $this->table_city = FSTable_ad::_('fs_cities', 1);
        $this->table_district = FSTable_ad::_('fs_districts', 1);
        $this->table_link = 'fs_menus_createlink';
        //            $this -> table_tags = FSTable_ad::_('fs_tags',1);
        $limit_created_link = 30;
        $this->limit_created_link = $limit_created_link;
        // config for save
        $cyear = date('Y');
        $cmonth = date('m');
        //$cday = date('d');
        $this->img_folder = 'images/hotels/hotels';
        $this->check_alias = 0;
        $this->field_img = 'image';

        parent::__construct();
    }

    function setQuery()
    {

        // ordering
        $ordering = "";
        $where = "  ";
        if (isset($_SESSION[$this->prefix . 'sort_field'])) {
            $sort_field = $_SESSION[$this->prefix . 'sort_field'];
            $sort_direct = $_SESSION[$this->prefix . 'sort_direct'];
            $sort_direct = $sort_direct ? $sort_direct : 'asc';
            $ordering = '';
            if ($sort_field)
                $ordering .= " ORDER BY $sort_field $sort_direct, created_time DESC, id DESC ";
        }

        // from
        if (isset($_SESSION[$this->prefix . 'text0'])) {
            $date_from = $_SESSION[$this->prefix . 'text0'];
            if ($date_from) {
                $date_from = strtotime($date_from);
                $date_new = date('Y-m-d H:i:s', $date_from);
                $where .= ' AND a.created_time >=  "' . $date_new . '" ';
            }
        }

        // to
        if (isset($_SESSION[$this->prefix . 'text1'])) {
            $date_to = $_SESSION[$this->prefix . 'text1'];
            if ($date_to) {
                $date_to = $date_to . ' 23:59:59';
                $date_to = strtotime($date_to);
                $date_new = date('Y-m-d H:i:s', $date_to);
                $where .= ' AND a.created_time <=  "' . $date_new . '" ';
            }
        }

        // estore
        if (isset($_SESSION[$this->prefix . 'filter0'])) {
            $filter = $_SESSION[$this->prefix . 'filter0'];
            if ($filter) {
                $where .= " AND a.complex_id = $filter ";
            }
        }

        if (isset($_SESSION[$this->prefix . 'filter1'])) {
            $filter = $_SESSION[$this->prefix . 'filter1'];
            if ($filter) {
                $where .= " AND a.city_id = $filter ";
            }
        }

        if (!$ordering)
            $ordering .= " ORDER BY created_time DESC , id DESC ";


        if (isset($_SESSION[$this->prefix . 'keysearch'])) {
            if ($_SESSION[$this->prefix . 'keysearch']) {
                $keysearch = $_SESSION[$this->prefix . 'keysearch'];
                $where .= " AND a.name LIKE '%" . $keysearch . "%' ";
            }
        }
        if($_SESSION['ad_userid'] != 1 && $_SESSION['ad_userid'] != 9){
            $rs = $this->get_record('id='.$_SESSION['ad_userid'],'fs_users');
            $where .= ' and a.id in (0'.$rs->hotels_categories.'0) ';
        }

            $query = " SELECT a.*
						  FROM 
						  	" . $this->table_name . " AS a
						  	WHERE 1=1 " .
            $where .
            $ordering . " ";
        return $query;
    }

    function save($row = array(), $use_mysql_real_escape_string = 1)
    {
        $title = FSInput::get('name');
        if (!$title)
            return false;
        $id = FSInput::get('id', 0, 'int');

        $complex_id = FSInput::get('complex_id', 0, 'int');
        if (!$complex_id) {
            Errors::_('Bạn phải chọn loại khách sạn');
            return;
        }

        $comp = $this->get_record_by_id($complex_id, $this->table_category_name);

        $row['complex_name'] = $comp->name;
        $row['complex_alias'] = $comp->alias;

        $fsstring = FSFactory::getClass('FSString', '', '../');
        
        $district_id = FSInput::get('district_id', 0, 'int');
        $district = $this->get_record_by_id($district_id, $this->table_district);

        $row['district_name'] = $district->name;
        $row['district_alias'] = $fsstring->stringStandart($district->name);

        $row['description'] = htmlspecialchars_decode(FSInput::get('description'));

        // image
        $image = $_FILES["image"]["name"];
        if ($image) {
            $image = $this->upload_image('image', '_' . time(), 3000000, $this->arr_img_paths);
            if ($image) {
                $row['image'] = $image;
            }
        }

        $image_og = $_FILES["og_image"]["name"];
        if ($image_og) {
            $og_image = $this->upload_image('og_image', '_' . time(), 3000000, $this->arr_image_og_paths);
            if ($og_image) {
                $row['og_image'] = $og_image;
            }
        }

        //        $row['content'] = htmlspecialchars_decode(FSInput::get('content'));
        $time = date('Y-m-d H:i:s');
        $row['published'] = FSInput::get('published');

        $min_price = FSInput::get('min_price');
        $max_price = FSInput::get('max_price');
        $row['min_price'] = $this->standart_money($min_price, 0);
        $row['max_price'] = $this->standart_money($max_price, 0);

        $multi_categories = FSInput::get('favourite_service', array(), 'array');
        $str_multi_categories = implode(',', $multi_categories);

        if ($str_multi_categories) {
            $str_multi_categories = ',' . $str_multi_categories . ',';
        }
        $row['favourite_service'] = $str_multi_categories;

        $user_id = isset($_SESSION['ad_userid']) ? $_SESSION['ad_userid'] : '';
        if (!$user_id)
            return false;

        $user = $this->get_record_by_id($user_id, 'fs_users', 'username');
        if ($id) {
            $row['updated_time'] = $time;
        } else {
            $row['created_time'] = $time;
            $row['updated_time'] = $time;
        }

        $multi_categories = FSInput::get('news_record_related', array(), 'array');
        $str_multi_categories = implode(',', $multi_categories);

        if ($str_multi_categories) {
            $str_multi_categories = ',' . $str_multi_categories . ',';
        }
        $row['news_related'] = $str_multi_categories;

       
        $alias = $fsstring->stringStandart($title);
        $row['services'] = $this->save_service();

        $rs = parent::save($row);
        $this->save_extend_around($rs);
        $this->save_qna($rs);
        $rs2 = $this->save_hotel_filter($rs);
        $this->save_products_images($rs);
        session_regenerate_id();
        return $rs;
    }

    function save_products_images($record_id)
    {
        $this->_update(array(
            'record_id' => $record_id
        ), 'fs_hotels_images', 'session_id=\'' . session_id() . '\'');
    }

    function standart_money($money, $method)
    {
        $money = str_replace(',', '', trim($money));
        $money = str_replace(' ', '', $money);
        $money = str_replace('.', '', $money);
        //		$money = intval($money);
        $money = (float)($money);
        if (!$method)
            return $money;
        if ($method == 1) {
            $money = $money * 1000;
            return $money;
        }
        if ($method == 2) {
            $money = $money * 1000000;
            return $money;
        }
    }

    function save_hotel_filter($id)
    {
        global $db;

        $tablename = $this->table_filter;

        // save exist field
        if (!$this->save_exist_field($tablename)) {
            return false;
        }

        // save new field
        if (!$this->save_new_field($tablename, $id)) {
            return false;
        }

        // remove field
        if (!$this->remove_exist_field($tablename)) {
            return false;
        }
        return true;
    }
    function get_services()
    {
        $query = " SELECT *
                    FROM " . $this->table_services . " order by title asc";
        global $db;
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }
    function get_service_items()
    {
        $query = " SELECT *
                    FROM " . $this->table_service_items . "";
        global $db;
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }
    function remove_exist_field($tablename)
    {
        global $db;
        $field_remove = trim(FSInput::get('field_remove'));
        if ($field_remove) {
            $array_field_remove = explode(",", $field_remove);
            if (count($array_field_remove) > 0) {
                foreach ($array_field_remove as $item) {
                    $this->_remove('id = ' . $item, $tablename);
                }
            }
        }
        return true;
    }

    function save_exist_field($table_name)
    {
        global $db;
        //        print_r($_REQUEST);die;
        $field_extend_exist_total = FSInput::get('field_extend_exist_total');
        for ($i = 0; $i < $field_extend_exist_total; $i++) {
            $sql_update = " UPDATE " . $table_name . "
							SET ";
            $id_exist = FSInput::get('id_exist_' . $i);

            $ex_name = FSInput::get('ex_name' . $i);
            $be_name = FSInput::get('be_name' . $i);

            $ex_order = FSInput::get('ex_order' . $i);
            $be_order = FSInput::get('be_order' . $i);

            if (($ex_name != $be_name) || ($ex_order != $be_order)) {
                $sql_update .= "title = '$ex_name',
								ordering = '$ex_order',
								updated_time = '" . date('Y-m-d H:i:s') . "'
								";
                $sql_update .= " WHERE id = $id_exist ";
                $db->query($sql_update);
                $rows = $db->affected_rows();
            }
        }
        return true;
    }

    function save_new_field($table_name, $id)
    {
        global $db;
        $new_field_total = FSInput::get('new_field_total');
        if ($new_field_total) {
            $row = array();
            for ($i = 0; $i < $new_field_total; $i++) {
                $row['hotel_id'] = $id;
                $row['hotel_code'] = $this->get_record_by_id($id, $this->table_name)->code;
                $row['title'] = FSInput::get('new_name' . $i);
                $row['ordering'] = FSInput::get('new_order' . $i);
                $row['created_time'] = date('Y-m-d H:i:s');
                $row['updated_time'] = date('Y-m-d H:i:s');
                $row['published'] = 1;

                if ($row['title'] != '')
                    $id_sub = $this->_add($row, $table_name, 0);
            }
            if (!$id_sub) {
                Errors::setError("Không thể thêm mới dịch vụ!");
                return false;
            }
        }
        return true;
    }

    function get_news_related($news_related)
    {
        if (!$news_related)
            return;
        $query = " SELECT * 
    					FROM " . $this->table_news . "
    					WHERE id IN (0" . $news_related . "0) 
    					 ORDER BY POSITION(','+id+',' IN '0" . $news_related . "0')
    					";
        global $db;
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    /*
     *==================== AJAX RELATED news==============================
     */

    function ajax_get_news_related()
    {
        $news_id = FSInput::get('new_id', 0, 'int');
        // category_id danh muc
        $category_id = FSInput::get('category_id', 0, 'int');
        // tim kiem keyword
        $keyword = FSInput::get('keyword');
        $keyword_tag = FSInput::get('keyword_tag');
        // chuoi id tin lien quan keyword tag
        $str_related = FSInput::get('str_related');
        // id khi click vao xoa tin lien quan
        $id = FSInput::get('id', 0, 'int');

        $where = ' WHERE published = 1 ';

        if ($category_id) {
            $where .= ' AND (category_id_wrapper LIKE "%,' . $category_id . ',%"	) ';
        }
        if ($keyword) {
            $where .= " AND ( title LIKE '%" . $keyword . "%' OR alias LIKE '%" . $keyword . "%' OR content LIKE '%" . $keyword . "%' )";
        }
        if ($keyword_tag) {
            $keyword_tag = explode(',', $keyword_tag);
            //$keyword_tag = str_replace(',','',$keyword_tag);
            $total = count($keyword_tag);
            $where .= ' AND ( ';
            for ($i = 0; $i < $total; $i++) {
                if ($i == 0) {
                    $where .= " title LIKE '%" . $keyword_tag[$i] . "%' OR alias LIKE '%" . $keyword_tag[$i] . "%' OR content LIKE '%" . $keyword_tag[$i] . "%' ";
                } else {
                    $where .= " OR title LIKE '%" . $keyword_tag[$i] . "%' OR alias LIKE '%" . $keyword_tag[$i] . "%' OR content LIKE '%" . $keyword_tag[$i] . "%' ";
                }
            }
            $where .= ' ) ';
        }
        if ($str_related) {
            if ($id) {
                $str_related = str_replace(',' . $id, '', $str_related);
            }
            $where .= ' AND id NOT IN(0' . $str_related . '0) ';
        }

        $query_body = ' FROM ' . $this->table_name . ' ' . $where;
        $ordering = " ORDER BY created_time DESC , id DESC ";
        $query = ' SELECT id,category_id,title,category_name,image' . $query_body . $ordering . ' LIMIT 100 ';
        //print_r($query);
        global $db;
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    /*
     * select in category of home
     */
    function get_categories_news_tree()
    {
        global $db;
        $query = " SELECT a.*
						  FROM
						  	" . $this->table_category_news_name . " AS a
						  	WHERE published = 1 ORDER BY ordering ";
        $sql = $db->query($query);
        $result = $db->getObjectList();
        $tree = FSFactory::getClass('tree', 'tree/');
        $list = $tree->indentRows2($result);
        return $list;
    }

    /*
     * Save all record for list form
     */
    function save_all()
    {
        $total = FSInput::get('total', 0, 'int');
        if (!$total)
            return true;
        $field_change = FSInput::get('field_change');
        if (!$field_change)
            return false;
        $field_change_arr = explode(',', $field_change);
        $total_field_change = count($field_change_arr);
        $record_change_success = 0;
        for ($i = 0; $i < $total; $i++) {
            //	        	$str_update = '';
            $row = array();
            $update = 0;
            foreach ($field_change_arr as $field_item) {
                $field_value_original = FSInput::get($field_item . '_' . $i . '_original');
                $field_value_new = FSInput::get($field_item . '_' . $i);
                if (is_array($field_value_new)) {
                    $field_value_new = count($field_value_new) ? ',' . implode(',', $field_value_new) . ',' : '';
                }

                if ($field_value_original != $field_value_new) {
                    $update = 1;
                    // category
                    if ($field_item == 'category_id') {
                        $cat = $this->get_record_by_id($field_value_new, $this->table_category_name);
                        $row['category_id_wrapper'] = $cat->list_parents;
                        $row['category_alias_wrapper'] = $cat->alias_wrapper;
                        $row['category_name'] = $cat->name;
                        $row['category_alias'] = $cat->alias;
                        $row['category_id'] = $field_value_new;
                    } else {
                        $row[$field_item] = $field_value_new;
                    }
                }
            }
            if ($update) {
                $id = FSInput::get('id_' . $i, 0, 'int');
                $str_update = '';
                global $db;
                $j = 0;
                foreach ($row as $key => $value) {
                    if ($j > 0)
                        $str_update .= ',';
                    $str_update .= "`" . $key . "` = '" . $value . "'";
                    $j++;
                }

                $sql = ' UPDATE  ' . $this->table_name . ' SET ';
                $sql .= $str_update;
                $sql .= ' WHERE id =    ' . $id . ' ';
                $db->query($sql);
                $rows = $db->affected_rows();
                if (!$rows)
                    return false;
                $record_change_success++;
            }
        }
        return $record_change_success;
    }

    function get_linked_id()
    {
        $id = FSInput::get('id', 0, 'int');
        if (!$id)
            return;
        global $db;
        $query = " SELECT *
						FROM  " . $this->table_link . "
						WHERE published = 1
						AND id = $id 
						 ";
        $result = $db->getObject($query);

        return $result;
    }

    /*
     * get List data from table
     * for create link
     */
    function get_data_from_table($add_table, $add_field_display, $add_field_value, $add_field_distinct)
    {
        $query = $this->set_query_create_link($add_table, $add_field_display, $add_field_value, $add_field_distinct);
        if (!$query)
            return;
        global $db;
        $sql = $db->query_limit($query, $this->limit_created_link, $this->page);
        $result = $db->getObjectList();

        return $result;
    }

    function get_total_create_link($add_table, $add_field_display, $add_field_value, $add_field_distinct)
    {
        global $db;
        $query = $this->set_query_create_link($add_table, $add_field_display, $add_field_value, $add_field_distinct);

        $total = $db->getTotal($query);
        return $total;
    }

    function get_pagination_create_link($add_table, $add_field_display, $add_field_value, $add_field_distinct)
    {
        $total = $this->get_total_create_link($add_table, $add_field_display, $add_field_value, $add_field_distinct);
        $pagination = new Pagination($this->limit_created_link, $total, $this->page);
        return $pagination;
    }

    function set_query_create_link($add_table, $add_field_display, $add_field_value, $add_field_distinct)
    {
        $query = '';
        if ($add_field_distinct) {
            if ($add_field_display != $add_field_value) {
                echo "Khi đã chọn distinct, duy nhất chỉ xét một trường. Bạn hãy check lại trường hiển thị và trường dữ liệu";
                return false;
            }
            $query .= ' SELECT DISTINCT ' . $add_field_display . ' ';
        } else {
            $query .= ' SELECT ' . $add_field_display . ' ,' . $add_field_value . '  ';
        }
        $query .= ' FROM ' . $add_table;
        $query .= '	WHERE published = 1 ';
        return $query;
    }

    function get_list_complex()
    {
        global $db;
        $sql = "SELECT *
                    FROM $this->table_category_name
                    WHERE published = 1 ORDER BY ordering ASC, id DESC";
        $db->query($sql);
        $result = $db->getObjectList();
        $tree = FSFactory::getClass('tree', 'tree');
        $rs = $tree->indentRows2($result, 3);
        return $rs;
    }

    function get_list_cities()
    {
        global $db;
        $sql = "SELECT *
                    FROM $this->table_city
                    WHERE 1 = 1 ORDER BY ordering ASC, id DESC";
        $db->query($sql);
        $result = $db->getObjectList();
        $tree = FSFactory::getClass('tree', 'tree');
        $rs = $tree->indentRows2($result, 3);
        return $rs;
    }

    function get_list_district()
    {
        global $db;
        $sql = "SELECT *
                    FROM $this->table_district
                    WHERE 1 = 1 ORDER BY ordering ASC, id DESC";
        $db->query($sql);
        $result = $db->getObjectList();
        $tree = FSFactory::getClass('tree', 'tree');
        $rs = $tree->indentRows2($result, 3);
        return $rs;
    }

    function get_list_filter($id)
    {
        global $db;
        $sql = "SELECT *
                    FROM $this->table_filter
                    WHERE hotel_id = $id ORDER BY ordering ASC, id DESC";
        $db->query($sql);
        return $db->getObjectList();
    }

    function save_extend_around($id)
    {
        $fs_table = FSFactory::getClass('fstable');
        $name_around = FSInput::get("name_around", array(), 'array');
        $around_distance = FSInput::get("around_distance", array(), 'array');
        $id_around = FSInput::get("id_around", array(), 'array');

        $id_hotel = FSInput::get('id');

        for ($i = 0; $i < count($name_around); $i++) {
            if ($name_around[$i] != '' && $around_distance[$i] != '') {
                $row2['name'] = $name_around[$i];
                $row2['distance'] = $around_distance[$i];
                $row2['hotel_id'] = $id;
                $row2['created_time'] = date("Y-m-d H:i:s");
                $row2['published'] = 1;
                $row2['category_id'] = 1;
                $row2['category_name'] = "Xung quanh có gì";
                if (!@$id_around[$i])
                    $this->_add($row2, $fs_table->getTable('fs_hotels_around', 1));
                else
                    $this->_update($row2, $fs_table->getTable('fs_hotels_around', 1), "id = " . $id_around[$i] . "");
            }
        }

        $name_location = FSInput::get("name_location", array(), 'array');
        $location_distance = FSInput::get("location_distance", array(), 'array');
        $id_location = FSInput::get("id_location", array(), 'array');

        for ($i = 0; $i < count($name_location); $i++) {
            if ($name_location[$i] != '' && $location_distance[$i] != '') {
                $row3['name'] = $name_location[$i];
                $row3['distance'] = $location_distance[$i];
                $row3['hotel_id'] = $id;
                $row3['created_time'] = date("Y-m-d H:i:s");
                $row3['published'] = 1;
                $row3['category_id'] = 2;
                $row3['category_name'] = "Địa điểm thăm quan hàng đầu";
                if (!$id_location[$i])
                    $this->_add($row3, $fs_table->getTable('fs_hotels_around', 1));
                else
                    $this->_update($row3, $fs_table->getTable('fs_hotels_around', 1), "id = " . $id_location[$i] . "");
            }
        }

        $name_restaurant = FSInput::get("name_restaurant", array(), 'array');
        $restaurant_distance = FSInput::get("restaurant_distance", array(), 'array');
        $id_restaurant = FSInput::get("id_restaurant", array(), 'array');

        for ($i = 0; $i < count($name_restaurant); $i++) {
            if ($name_restaurant[$i] != '' && $restaurant_distance[$i] != '') {
                $row4['name'] = $name_restaurant[$i];
                $row4['distance'] = $restaurant_distance[$i];
                $row4['hotel_id'] = $id;
                $row4['created_time'] = date("Y-m-d H:i:s");
                $row4['published'] = 1;
                $row4['category_id'] = 3;
                $row4['category_name'] = "Nhà hàng & quán cà phê";
                if (!$id_restaurant[$i])
                    $this->_add($row4, $fs_table->getTable('fs_hotels_around', 1));
                else
                    $this->_update($row4, $fs_table->getTable('fs_hotels_around', 1), "id = " . $id_restaurant[$i] . "");
            }
        }

        $name_view = FSInput::get("name_view", array(), 'array');
        $view_distance = FSInput::get("view_distance", array(), 'array');
        $id_view = FSInput::get("id_view", array(), 'array');

        for ($i = 0; $i < count($name_view); $i++) {
            if ($name_view[$i] != '' && $view_distance[$i] != '') {
                $row5['name'] = $name_view[$i];
                $row5['distance'] = $view_distance[$i];
                $row5['hotel_id'] = $id;
                $row5['created_time'] = date("Y-m-d H:i:s");
                $row5['published'] = 1;
                $row5['category_id'] = 4;
                $row5['category_name'] = "Cảnh đẹp thiên nhiên";
                if (!$id_view[$i])
                    $this->_add($row5, $fs_table->getTable('fs_hotels_around', 1));
                else
                    $this->_update($row5, $fs_table->getTable('fs_hotels_around', 1), "id = " . $id_view[$i] . "");
            }
        }

        $name_market = FSInput::get("name_beach", array(), 'array');
        $market_distance = FSInput::get("beach_distance", array(), 'array');
        $id_market = FSInput::get("id_beach", array(), 'array');

        for ($i = 0; $i < count($name_market); $i++) {
            if ($name_market[$i] != '' && $market_distance[$i] != '') {

                $row6['name'] = $name_market[$i];
                $row6['distance'] = $market_distance[$i];
                $row6['hotel_id'] = $id;
                $row6['created_time'] = date("Y-m-d H:i:s");
                $row6['published'] = 1;
                $row6['category_id'] = 5;
                $row6['category_name'] = "Các bãi biển trong khu vực";
                if (!$id_market[$i])
                    $this->_add($row6, $fs_table->getTable('fs_hotels_around', 1));
                else
                    $this->_update($row6, $fs_table->getTable('fs_hotels_around', 1), "id = " . $id_market[$i] . "");
            }
        }

        $name_airpot = FSInput::get("name_airpot", array(), 'array');
        $airpot_distance = FSInput::get("airpot_distance", array(), 'array');
        $id_airpot = FSInput::get("id_airpot", array(), 'array');

        for ($i = 0; $i < count($name_airpot); $i++) {
            if ($name_airpot[$i] != '' && $airpot_distance[$i] != '') {
                $row7['name'] = $name_airpot[$i];
                $row7['distance'] = $airpot_distance[$i];
                $row7['hotel_id'] = $id;
                $row7['created_time'] = date("Y-m-d H:i:s");
                $row7['published'] = 1;
                $row7['category_id'] = 6;
                $row7['category_name'] = "Sân bay gần nhất";
                if (!$id_airpot[$i])
                    $this->_add($row7, $fs_table->getTable('fs_hotels_around', 1));
                else
                    $this->_update($row7, $fs_table->getTable('fs_hotels_around', 1), "id = " . $id_airpot[$i] . "");
            }
        }
    }

    function save_qna($id)
    {
        $fs_table = FSFactory::getClass('fstable');
        $name_question = FSInput::get("name_question", array(), 'array');
        $description_question = FSInput::get("description_question", array(), 'array');
        $id_question = FSInput::get("id_question", array(), 'array');
        for ($i = 0; $i < count($name_question); $i++) {
            if ($name_question[$i] != '' && $name_question[$i] != '') {
                $row8['name'] = $name_question[$i];
                $row8['description'] = $description_question[$i];
                $row8['hotel_id'] = $id;

                if (!@$id_question[$i])
                    $this->_add($row8, $fs_table->getTable('fs_hotels_qna', 1));
                else
                    $this->_update($row8, $fs_table->getTable('fs_hotels_qna', 1), "id = " . $id_question[$i] . "");
            }
        }
    }

    function save_service()
    {
        $fs_table = FSFactory::getClass('fstable');
        $service = FSInput::get("service", array(), 'array');
        $position_service = FSInput::get("position-service", array(), 'array');

        $temp = array();

        foreach ($service as $key => $item) {
            $pos = $position_service[$key];
            if ($pos != '' && $pos != '') {
                $service_item = FSInput::get("service_item_" . $pos, array(), 'array');
            } else {
                $service_item = FSInput::get("service_item_" . $key, array(), 'array');
            }
            $temp[$key] = array(
                'category_id' => $item,
                'service_item' => implode(',', $service_item)
            );
        }
        return json_encode($temp);
    }
}
