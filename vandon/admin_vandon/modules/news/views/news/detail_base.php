
<?php
TemplateHelper::dt_edit_text(FSText::_('Tiêu đề tin'), 'title', @$data->title);
TemplateHelper::dt_edit_text(FSText::_('Alias'), 'alias', @$data->alias, '', 60, 1, 0, FSText::_("Can auto generate"));
TemplateHelper::dt_edit_image(FSText::_('Thumbnail'), 'image', str_replace('/original/', '/small/', URL_ROOT . @$data->image));
TemplateHelper::dt_edit_image(FSText::_('Ảnh bìa'), 'banner', str_replace('/original/', '/small/', URL_ROOT . @$data->banner));

TemplateHelper::dt_edit_selectbox(FSText::_('Categories'),'category_id',@$data -> category_id,0,$categories,$field_value = 'id', $field_label='treename',$size = 10,0,1);
TemplateHelper::datetimepicke(FSText::_('Published time'), 'created_time', @$data->created_time ? @$data->created_time : date('Y-m-d H:i:s'), FSText::_('Bạn vui lòng chọn thời gian hiển thị'), 20, '', 'col-md-3', 'col-md-4');

TemplateHelper::dt_text(FSText::_('Người tạo'), @$data->author, '', '', 1, 'col-md-3', 'col-md-9', 'right');
TemplateHelper::dt_text(FSText::_('Người sửa cuối'), @$data->author_last, '', '', 1, 'col-md-3', 'col-md-9', 'right');

TemplateHelper::dt_edit_text(FSText::_('Tag'), 'tags', @$data->tags, '', 100, 5, 0, '', '', 'col-sm-3', 'col-sm-9');


TemplateHelper::dt_checkbox(FSText::_('Published'), 'published', @$data->published, 1, '', '', '', 'col-sm-4', 'col-sm-8');
TemplateHelper::dt_checkbox(FSText::_('Hiển thị trang chủ'), 'show_in_homepage', @$data->show_in_homepage, 0, '', '', '', 'col-sm-4', 'col-sm-8');
TemplateHelper::dt_edit_text(FSText::_('Ordering'), 'ordering', @$data->ordering, @$maxOrdering, '', '', 0, '', '', 'col-sm-4', 'col-sm-8');


TemplateHelper::dt_edit_image2(FSText::_('Image'), 'image', str_replace('/original/', '/resized/', URL_ROOT . @$data->image), '', '', '', 1);
TemplateHelper::dt_edit_text(FSText::_('Tóm tắt'), 'summary', @$data->summary, '', 100, 5, 0, '', '', 'col-sm-3', 'col-sm-9');
TemplateHelper::dt_edit_text(FSText::_('Mô tả chi tiết'), 'content', @$data->content, '', 650, 450, 1, '', '', 'col-sm-3', 'col-sm-9');

TemplateHelper::dt_edit_text(FSText::_('Tên khách hàng'), 'customer', @$data->customer, '', 60, 1, 0);
TemplateHelper::dt_edit_text(FSText::_('Khách hàng nhận xét'), 'customer_review', @$data->customer_review, '', 100, 5, 0, '', '', 'col-sm-3', 'col-sm-9');

TemplateHelper::dt_edit_text(FSText::_('Mô tả khác'), 'content2', @$data->content2, '', 650, 450, 1, '', '', 'col-sm-3', 'col-sm-9');



//$this -> dt_form_end(@$data,1,0,2,'Cấu hình seo');
?>
