<?php

class PromotionModelsManual_sale extends FSModels
{
    var $limit;
    var $prefix;

    function __construct()
    {
        $this->limit = 20;
        $this->view = 'manual_sale';
        $this->table_name = 'fs_promotion_manual';
        $this -> arr_img_paths = array(array('large',1280,378,'resized_not_crop'),array('small',80,27,'resized_not_crop'));
        $this->img_folder = 'images/manual_sale/' . date('Y/m/d');
        $this->check_alias = 0;
        $this -> field_img = 'image';
        parent::__construct();
    }

    function get_products_related($product_related)
    {
        if (!$product_related)
            return;
        $query = " SELECT id, name,image, created_time, price, model, quantity 
					FROM fs_products
					WHERE id IN (0" . $product_related . "0) 
					 ORDER BY POSITION(','+id+',' IN '0" . $product_related . "0')
					";
        global $db;
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    function ajax_get_products_related()
    {
        $news_id = FSInput::get('product_id', 0, 'int');
        $category_id = FSInput::get('category_id', 0, 'int');
        $keyword_from = FSInput::get('keyword_from');
        $keyword_from = substr($keyword_from,2,7);
        $keyword_to = FSInput::get('keyword_to');
        $keyword_to = substr($keyword_to,2,7);
        $price_from = FSInput::get('price_from');
        $price_to = FSInput::get('price_to');
        $time_from = FSInput::get('time_from');
        $time_to = FSInput::get('time_to');
        $where = ' WHERE published = 1 ';
        if ($category_id) {
            $where .= ' AND (category_id_wrapper LIKE "%,' . $category_id . ',%"	) ';
        }
//        $where .= " AND ( name LIKE '%" . $keyword . "%' OR alias LIKE '%" . $keyword . "%' OR id = '$keyword' )";
        if ($keyword_from){
            $where .= " AND ( model_compare >= " . $keyword_from . " )";
        }
        if ($keyword_to){
            $where .= " AND ( model_compare <= " . $keyword_to . " )";
        }
        if ($price_from) {
            $where .= " AND ( price >= " . $price_from . " )";
        }
        if ($price_to) {
            $where .= " AND ( price <= " . $price_to . " )";
        }
        if ($time_from) {
            $time_from = date('Y-m-d H:i:s', strtotime($time_from));
            $where .= " AND ( created_time >= '" . $time_from . "' )";
        }
        if ($time_to) {
            $time_to = date('Y-m-d H:i:s', strtotime($time_to));
            $where .= " AND ( created_time <= '" . $time_to . "' )";
        }

        $query_body = ' FROM fs_products ' . $where;
        $ordering = " ORDER BY created_time DESC , id DESC ";
        $query = ' SELECT id,category_id,name,category_name,image,created_time, price, model, quantity' . $query_body . $ordering;
//            echo $query;die;
        global $db;
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    function get_promotion()
    {
        $time = date('Y-m-d H:i:s');
        $query = " SELECT id, products_related, date_end, date_start
					FROM fs_promotion 
					WHERE published = 1  ORDER BY date_end desc
					";
//            echo $query;die;
        global $db;
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    function get_cur_promotion($promotion_id)
    {
        $time = date('Y-m-d H:i:s');
        $query = " SELECT id, products_related, date_end, date_start
					FROM fs_promotion 
					WHERE published = 1 AND id = $promotion_id AND '$time' < date_end
					";
//            echo $query;die;
        global $db;
        $sql = $db->query($query);
        $result = $db->getObject();
        return $result;
    }

    function get_exist_promotion($promotion_id)
    {
        $time = date('Y-m-d H:i:s');
        $query = " SELECT id, products_related, date_end
					FROM fs_promotion 
					WHERE published = 1 AND id = $promotion_id AND '$time' < date_end
					";
//            echo $query;die;
        global $db;
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    function get_manual()
    {
        $time = date('Y-m-d H:i:s');
        $query = " SELECT id, products_related
					FROM fs_promotion_manual 
					WHERE published = 1 ORDER BY date_end desc
					";
//            echo $query;die;
        global $db;
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    function get_cur_manual($promotion_id)
    {
        $time = date('Y-m-d H:i:s');
        $query = " SELECT id, products_related, date_end, date_start
					FROM fs_promotion_manual 
					WHERE published = 1 AND id = $promotion_id AND '$time' < date_end
					";
//            echo $query;die;
        global $db;
        $sql = $db->query($query);
        $result = $db->getObject();
        return $result;
    }

    function get_exist_manual($promotion_id)
    {
        $time = date('Y-m-d H:i:s');
        $query = " SELECT id, products_related, date_end
					FROM fs_promotion_manual 
					WHERE published = 1 AND id = $promotion_id AND '$time' < date_end
					";
//            echo $query;die;
        global $db;
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }


    function get_gba()
    {
        $time = date('Y-m-d H:i:s');
        $query = " SELECT id, products_related, date_end, date_start
					FROM fs_promotion_gba 
					WHERE published = 1 ORDER BY date_end desc
					";
//            echo $query;die;
        global $db;
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    function get_cur_gba($promotion_id)
    {
        $time = date('Y-m-d H:i:s');
        $query = " SELECT id, products_related, date_end, date_start
					FROM fs_promotion_gba 
					WHERE published = 1 AND id = $promotion_id AND '$time' < date_end
					";
//            echo $query;die;
        global $db;
        $sql = $db->query($query);
        $result = $db->getObject();
        return $result;
    }

    function get_exist_gba($promotion_id)
    {
        $time = date('Y-m-d H:i:s');
        $query = " SELECT id, products_related, date_end
					FROM fs_promotion_gba 
					WHERE published = 1 AND id = $promotion_id AND '$time' < date_end
					";
//            echo $query;die;
        global $db;
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    function get_all_products()
    {

        $query = " SELECT name,id
						FROM fs_products
						WHERE published = 1
						ORDER BY ordering DESC";
        global $db;
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    function get_categories_tree()
    {
        global $db;
        $query = " SELECT a.*
					   FROM fs_products_categories AS a
					   ORDER BY ordering ";
        $sql = $db->query($query);
        $result = $db->getObjectList();
        $tree = FSFactory::getClass('tree', 'tree/');
        $list = $tree->indentRows2($result);
        return $list;
    }

    function convertDateTime($strDate = "", $strTime = "")
    {
        //Break string and create array date time
        $strDate = str_replace("/", "-", $strDate);
        $strDateArray = explode("-", $strDate);
        $countDateArr = count($strDateArray);
        $strTime = str_replace("-", ":", $strTime);
        $strTimeArray = explode(":", $strTime);
        $countTimeArr = count($strTimeArray);
        //Get Current date time
        $today = getdate();
        $day = $today["mday"];
        $mon = $today["mon"];
        $year = $today["year"];
        $hour = $today["hours"];
        $min = $today["minutes"];
        $sec = $today["seconds"];
        //Get date array
        switch ($countDateArr) {
            case 2:
                $day = intval($strDateArray[0]);
                $mon = intval($strDateArray[1]);
                break;
            case $countDateArr >= 3:
                $day = intval($strDateArray[0]);
                $mon = intval($strDateArray[1]);
                $year = intval($strDateArray[2]);
                break;
        }
        //Get time array
        switch ($countTimeArr) {
            case 2:
                $hour = intval($strTimeArray[0]);
                $min = intval($strTimeArray[1]);
                break;
            case $countTimeArr >= 3:
                $hour = intval($strTimeArray[0]);
                $min = intval($strTimeArray[1]);
                $sec = intval($strTimeArray[2]);
                break;
        }
        //Return date time integer
        if (@mktime($hour, $min, $sec, $mon, $day, $year) == -1) return $today[0];
        else return mktime($hour, $min, $sec, $mon, $day, $year);
    }

    function save($row = array(), $use_mysql_real_escape_string = 1)
    {
        $id = FSInput::get('id', 0, 'int');
        $row['name'] = FSInput::get('name');
        $row['code'] = trim(FSInput::get('code'));
//        $row['values_apply']= FSInput::get('values_apply');
        $row['summary'] = FSInput::get('summary');
        $row['published'] = FSInput::get('published');
        $row['ordering'] = FSInput::get('ordering');
        $row['discount'] = FSInput::get('discount');
        $row['discount_unit'] = FSInput::get('discount_unit');
        $products_related = $color = FSInput::get('products_record_related', array(), 'array');
        $str_products_related = implode(',', $products_related);
        if ($str_products_related) {
            $str_products_related = ',' . $str_products_related . ',';
        }

        $row ['products_related'] = $str_products_related;
        $date_start = $this->convertDateTime(FSInput::get('date_start'), '00:00');
        $date_end = $this->convertDateTime(FSInput::get('date_end'), '23:59');
        $row['date_start'] = date('Y-m-d H:i:s', $date_start);
        $row['date_end'] = date('Y-m-d H:i:s', $date_end);
        $this->save_sale_items($id, $str_products_related);
        return parent::save($row);
    }

    function save_sale_items($item_id, $products_related)
    {
        if (!$item_id)
            return false;

        global $db;

        // remove before update or inser
        $sql = " DELETE FROM fs_sale_items
					WHERE sale_id = '$item_id'";
        $db->query($sql);
        $rows = $db->affected_rows();
        // insert data

        // Repeat estores

        $sql = " INSERT INTO fs_sale_items (`sale_id`,`product_id`,`discount_percent`,`amount_discount`,`amount_sold`)
					VALUES ";
        $array_insert = array();

        $arr_id = explode(',', $products_related);
        for ($i = 1; $i < count($arr_id) - 1; $i++) {
            $id = $arr_id[$i];
            $discount_id = 'discount_' . $id;
            $discount_percent = 'discount_percent_' . $id;
            $discount_amount = 'amount_discount_' . $id;
            $discount_sold = 'amount_sold_' . $id;
            $discount_id = FSInput::get($discount_id);
            $discount_percent = FSInput::get($discount_percent);
            $discount_amount = FSInput::get($discount_amount);
            $discount_sold = FSInput::get($discount_sold);
            if (!empty($discount_percent) or !empty($discount_amount) or !empty($discount_sold)) {
                $array_insert[] = "('$item_id','$id','$discount_percent','$discount_amount','$discount_sold') ";
            }
        }
        // Repeat products

        if (count($array_insert)) {
            $sql_insert = implode(',', $array_insert);
            $sql .= $sql_insert;
            $db->query($sql);
            $rows = $db->affected_rows();
            return true;
        } else {
            return;
        }
    }

    function get_sale_items($prod_id,$manual_id){
        global  $db;
        $query = " SELECT * from fs_sale_items WHERE sale_id = $manual_id AND product_id = $prod_id";
        $sql = $db->query($query);
        $result = $db->getObject();
//        var_dump($result);die;
        return $result;
    }
}

?>