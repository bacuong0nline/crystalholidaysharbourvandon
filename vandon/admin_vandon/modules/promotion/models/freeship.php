<?php
class PromotionModelsFreeship extends FSModels
{
    var $limit;
    var $prefix;
    
    function __construct()
    {
        $this->limit = 20;
        $this->view = 'freeship';
        $this->table_name = 'fs_promotion_freeship';
        $this->img_folder = 'images/freeship/' . date('Y/m/d');
        $this->check_alias = 0;
        $this->field_img = '';
        parent::__construct();
    }

    function convertDateTime($strDate = "", $strTime = "")
    {
        //Break string and create array date time
        $strDate = str_replace("/", "-", $strDate);
        $strDateArray = explode("-", $strDate);
        $countDateArr = count($strDateArray);
        $strTime = str_replace("-", ":", $strTime);
        $strTimeArray = explode(":", $strTime);
        $countTimeArr = count($strTimeArray);
        //Get Current date time
        $today = getdate();
        $day = $today["mday"];
        $mon = $today["mon"];
        $year = $today["year"];
        $hour = $today["hours"];
        $min = $today["minutes"];
        $sec = $today["seconds"];
        //Get date array
        switch ($countDateArr) {
            case 2:
                $day = intval($strDateArray[0]);
                $mon = intval($strDateArray[1]);
                break;
            case $countDateArr >= 3:
                $day = intval($strDateArray[0]);
                $mon = intval($strDateArray[1]);
                $year = intval($strDateArray[2]);
                break;
        }
        //Get time array
        switch ($countTimeArr) {
            case 2:
                $hour = intval($strTimeArray[0]);
                $min = intval($strTimeArray[1]);
                break;
            case $countTimeArr >= 3:
                $hour = intval($strTimeArray[0]);
                $min = intval($strTimeArray[1]);
                $sec = intval($strTimeArray[2]);
                break;
        }
        //Return date time integer
        if (@mktime($hour, $min, $sec, $mon, $day, $year) == -1) return $today[0];
        else return mktime($hour, $min, $sec, $mon, $day, $year);
    }

    function save($row = array(), $use_mysql_real_escape_string = 1){
        $row['name']       = FSInput::get('name');
        $row['code']        = trim(FSInput::get('code'));
        $row['number_apply']= FSInput::get('number_apply');
        $row['number_from']= FSInput::get('number_from');
        $row['summary']     = FSInput::get('summary');
        $row['published']   = FSInput::get('published');
        $row['ordering']    = FSInput::get('ordering');
        $row['discount']    = FSInput::get('discount');
        $row['discount_unit']= FSInput::get('discount_unit');
        $date_start = $this->convertDateTime(FSInput::get('start_date'), '00:00');
        $date_end = $this->convertDateTime(FSInput::get('expiration_date'), '23:59');
        $row['start_date']      = date('Y-m-d H:i:s', $date_start);
        $row['expiration_date'] = date('Y-m-d H:i:s', $date_end);
        return  parent::save($row);
    }
}
?>