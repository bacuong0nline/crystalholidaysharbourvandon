<?php
	class PromotionModelsPromotion extends FSModels
	{
		var $limit;
		var $prefix ;
		function __construct()
		{
			$this -> limit = 20;
			$this -> view = 'promotion';

			$this -> arr_img_paths = array(array('large',1280,378,'resized_not_crop'),array('small',80,27,'resized_not_crop'));
			$this -> table_name = 'fs_promotion';

			// config for save
			$cyear = date('Y');
			$cmonth = date('m');
			$this -> img_folder = 'images/promotion/'.$cyear.'/'.$cmonth;
			$this -> check_alias = 0;
			$this -> field_img = 'image';

			parent::__construct();
		}
		function setQuery(){

			// ordering
			$ordering = "";
			$where = "  ";
			if(isset($_SESSION[$this -> prefix.'sort_field']))
			{
				$sort_field = $_SESSION[$this -> prefix.'sort_field'];
				$sort_direct = $_SESSION[$this -> prefix.'sort_direct'];
				$sort_direct = $sort_direct?$sort_direct:'asc';
				$ordering = '';
				if($sort_field)
					$ordering .= " ORDER BY $sort_field $sort_direct, created_time DESC, id DESC ";
			}

			// estore
			if(isset($_SESSION[$this -> prefix.'filter0'])){
				$filter = $_SESSION[$this -> prefix.'filter0'];
				if($filter){
					$where .= ' AND a.category_id_wrapper like  "%,'.$filter.',%" ';
				}
			}

			if(!$ordering)
				$ordering .= " ORDER BY created_time DESC , id DESC ";


			if(isset($_SESSION[$this -> prefix.'keysearch'] ))
			{
				if($_SESSION[$this -> prefix.'keysearch'] )
				{
					$keysearch = $_SESSION[$this -> prefix.'keysearch'];
					$where .= " AND a.title LIKE '%".$keysearch."%' ";
				}
			}

			$query = " SELECT a.*
						  FROM 
						  	".$this -> table_name." AS a
						  	WHERE 1=1 ".
						 $where.
						 $ordering. " ";
			return $query;
		}
		/*
		 * select in category of home
		 */
		function get_categories_tree()
		{
			global $db;
			$query = " SELECT a.*
					   FROM fs_products_categories AS a
					   ORDER BY ordering ";
			$sql = $db->query($query);
			$result = $db->getObjectList();
			$tree  = FSFactory::getClass('tree','tree/');
			$list = $tree -> indentRows2($result);
			return $list;
		}

        function convertDateTime($strDate = "", $strTime = "")
        {
            //Break string and create array date time
            $strDate = str_replace("/", "-", $strDate);
            $strDateArray = explode("-", $strDate);
            $countDateArr = count($strDateArray);
            $strTime = str_replace("-", ":", $strTime);
            $strTimeArray = explode(":", $strTime);
            $countTimeArr = count($strTimeArray);
            //Get Current date time
            $today = getdate();
            $day = $today["mday"];
            $mon = $today["mon"];
            $year = $today["year"];
            $hour = $today["hours"];
            $min = $today["minutes"];
            $sec = $today["seconds"];
            //Get date array
            switch ($countDateArr) {
                case 2:
                    $day = intval($strDateArray[0]);
                    $mon = intval($strDateArray[1]);
                    break;
                case $countDateArr >= 3:
                    $day = intval($strDateArray[0]);
                    $mon = intval($strDateArray[1]);
                    $year = intval($strDateArray[2]);
                    break;
            }
            //Get time array
            switch ($countTimeArr) {
                case 2:
                    $hour = intval($strTimeArray[0]);
                    $min = intval($strTimeArray[1]);
                    break;
                case $countTimeArr >= 3:
                    $hour = intval($strTimeArray[0]);
                    $min = intval($strTimeArray[1]);
                    $sec = intval($strTimeArray[2]);
                    break;
            }
            //Return date time integer
            if (@mktime($hour, $min, $sec, $mon, $day, $year) == -1) return $today[0];
            else return mktime($hour, $min, $sec, $mon, $day, $year);
        }

        function get_products_related($product_related)
        {
            if (!$product_related)
                return;
            $query = " SELECT id, name,image, created_time, price, model, quantity 
					FROM fs_products
					WHERE id IN (0" . $product_related . "0) 
					 ORDER BY POSITION(','+id+',' IN '0" . $product_related . "0')
					";
            global $db;
            $sql = $db->query($query);
            $result = $db->getObjectList();
            return $result;
        }

        function ajax_get_products_related()
        {
            $id = FSInput::get('product_id', 0, 'int');
            $category_id = FSInput::get('category_id', 0, 'int');
            $keyword = FSInput::get('keyword');
            $keyword_from = FSInput::get('keyword_from');
            $keyword_from = substr($keyword_from,2,7);
            $keyword_to = FSInput::get('keyword_to');
            $keyword_to = substr($keyword_to,2,7);
            $price_from = FSInput::get('price_from');
            $price_to = FSInput::get('price_to');
            $time_from = FSInput::get('time_from');
            $time_to = FSInput::get('time_to');
            $where = ' WHERE published = 1 ';
            if ($category_id) {
                $where .= ' AND (category_id_wrapper LIKE "%,' . $category_id . ',%"	) ';
            }
//            $where .= " AND ( name LIKE '%" . $keyword . "%' OR alias LIKE '%" . $keyword . "%' OR id = '$keyword' )";
            if ($keyword_from){
                $where .= " AND ( model_compare >= " . $keyword_from . " )";
            }
            if ($keyword){
                $where .= " AND ( name like '%" . $keyword . "%' )";
            }
            if ($keyword_to){
                $where .= " AND ( model_compare <= " . $keyword_to . " )";
            }
            if ($price_from){
            $where .= " AND ( price >= " . $price_from . " )";
            }
            if ($price_to){
                $where .= " AND ( price <= " . $price_to . " )";
            }
            if ($time_from){
                $time_from = date('Y-m-d H:i:s',strtotime($time_from));
                $where .= " AND ( created_time >= '" . $time_from . "' )";
            }
            if ($time_to){
                $time_to = date('Y-m-d H:i:s',strtotime($time_to));
                $where .= " AND ( created_time <= '" . $time_to . "' )";
            }

            $query_body = ' FROM fs_products ' . $where;
            $ordering = " ORDER BY created_time DESC , id DESC ";
            $query = ' SELECT id,category_id,name,category_name,image,created_time, price, model, quantity' . $query_body . $ordering ;
            global $db;
            $sql = $db->query($query);
            $result = $db->getObjectList();
            return $result;
        }

		function save($row = array(),$use_mysql_real_escape_string = 0) {
			$id = FSInput::get ( 'id', 0, 'int' );
            $name = FSInput::get('title');
//			//time promotion
//			$date_start = FSInput::get('date_start');
//			$published_hour_start = FSInput::get('published_hour_start',date('H:i'));
//			$date_end = FSInput::get('date_end');
//			$published_hour_end = FSInput::get('published_hour_end',date('H:i'));

            $arr_promotion_products = FSInput::get('promotion_products', array(), 'array');
            if (count($arr_promotion_products)) {
                $str_values = implode(',', $arr_promotion_products);
                $row['promotion_products'] = count($arr_promotion_products) ? ',' . $str_values . ',' : '';
            }
            $products_related = $color = FSInput::get('products_record_related', array(), 'array');
            $str_products_related = implode(',', $products_related);
            if ($str_products_related) {
                $str_products_related = ',' . $str_products_related . ',';
            }
            $row ['products_related'] = $str_products_related;
            $row['name'] = $name;
            $discount = FSInput::get('discount');
            $row ['discount'] = $discount = $this->standart_money($discount, 0);
            $discount_unit = FSInput::get('discount_unit',1,'int');
//            if ($discount_unit == 3){
//                $row ['price'] = $discount ;
//            }

            $date_start = $this->convertDateTime(FSInput::get('date_start'), '00:00');
            $date_end = $this->convertDateTime(FSInput::get('date_end'), '23:59');
            $row['date_start']      =  date('Y-m-d H:i:s',$date_start);
            $row['date_end'] = date('Y-m-d H:i:s', $date_end);
            $row['discount_unit'] = $discount_unit;



			if( date('Y-m-d H:i:s') > $row['date_end']){
				Errors::_( 'Thời gian khuyến mại đã quá hạn','alert' );
			}
			if($row['date_start']  > $row['date_end']){
				Errors::_( 'Thời gian bắt đầu phải nhỏ hơn thời gian kết thúc','alert' );
			}
			$id = parent::save ( $row );
//			echo $id;die;
			if (! $id) {
				Errors::setError ( 'Not save' );
				return false;
			}
//			if($id){
//				$this -> save_promotion_products($id);
//			}
			return $id;
		}

        function get_promotion(){
            $time = date('Y-m-d H:i:s');
            $query = " SELECT id, products_related, date_end, date_start
					FROM fs_promotion 
					WHERE published = 1  ORDER BY date_end desc
					";
//            echo $query;die;
            global $db;
            $sql = $db->query($query);
            $result = $db->getObjectList();
            return $result;
        }

        function get_cur_promotion($promotion_id){
            $time = date('Y-m-d H:i:s');
            $query = " SELECT id, products_related, date_end, date_start
					FROM fs_promotion 
					WHERE published = 1 AND id = $promotion_id AND '$time' < date_end
					";
//            echo $query;die;
            global $db;
            $sql = $db->query($query);
            $result = $db->getObject();
            return $result;
        }

        function get_exist_promotion($promotion_id){
            $time = date('Y-m-d H:i:s');
            $query = " SELECT id, products_related, date_end
					FROM fs_promotion 
					WHERE published = 1 AND id = $promotion_id AND '$time' < date_end
					";
//            echo $query;die;
            global $db;
            $sql = $db->query($query);
            $result = $db->getObjectList();
            return $result;
        }

        function get_manual(){
            $time = date('Y-m-d H:i:s');
            $query = " SELECT id, products_related
					FROM fs_promotion_manual 
					WHERE published = 1 ORDER BY date_end desc
					";
//            echo $query;die;
            global $db;
            $sql = $db->query($query);
            $result = $db->getObjectList();
            return $result;
        }

        function get_cur_manual($promotion_id){
            $time = date('Y-m-d H:i:s');
            $query = " SELECT id, products_related, date_end, date_start
					FROM fs_promotion_manual 
					WHERE published = 1 AND id = $promotion_id AND '$time' < date_end
					";
//            echo $query;die;
            global $db;
            $sql = $db->query($query);
            $result = $db->getObject();
            return $result;
        }

        function get_exist_manual($promotion_id){
            $time = date('Y-m-d H:i:s');
            $query = " SELECT id, products_related, date_end
					FROM fs_promotion_manual 
					WHERE published = 1 AND id = $promotion_id AND '$time' < date_end
					";
//            echo $query;die;
            global $db;
            $sql = $db->query($query);
            $result = $db->getObjectList();
            return $result;
        }

        function get_gba(){
            $time = date('Y-m-d H:i:s');
            $query = " SELECT id, products_related, date_end, date_start
					FROM fs_promotion_gba 
					WHERE published = 1 ORDER BY date_end desc
					";
//            echo $query;die;
            global $db;
            $sql = $db->query($query);
            $result = $db->getObjectList();
            return $result;
        }

        function get_cur_gba($promotion_id){
            $time = date('Y-m-d H:i:s');
            $query = " SELECT id, products_related, date_end, date_start
					FROM fs_promotion_gba 
					WHERE published = 1 AND id = $promotion_id AND '$time' < date_end
					";
//            echo $query;die;
            global $db;
            $sql = $db->query($query);
            $result = $db->getObject();
            return $result;
        }

        function get_exist_gba($promotion_id){
            $time = date('Y-m-d H:i:s');
            $query = " SELECT id, products_related, date_end
					FROM fs_promotion_gba 
					WHERE published = 1 AND id = $promotion_id AND '$time' < date_end
					";
//            echo $query;die;
            global $db;
            $sql = $db->query($query);
            $result = $db->getObjectList();
            return $result;
        }

        function standart_money($money, $method)
        {
            $money = str_replace(',', '', trim($money));
            $money = str_replace(' ', '', $money);
            $money = str_replace('.', '', $money);
//		$money = intval($money);
            $money = (double)($money);
            if (!$method)
                return $money;
            if ($method == 1) {
                $money = $money * 1000;
                return $money;
            }
            if ($method == 2) {
                $money = $money * 1000000;
                return $money;
            }
        }


		function get_all_products(){

			$query   = " SELECT name,id
						FROM fs_products
						WHERE published = 1
						ORDER BY ordering DESC";
			global $db;
			$sql = $db->query($query);
			$result = $db->getObjectList();
			return $result;
		}
//		function get_promotion_products($product_id){
//
//			$query   = " SELECT b.name,b.id, a.price_old,a.price_new,a.inf_promotion,a.product_incenty_id ,b.image
//						FROM fs_promotion_products AS a
//						LEFT JOIN fs_products AS b ON a.product_incenty_id = b.id
//						WHERE a.product_id = $product_id";
//			global $db;
//			$sql = $db->query($query);
//			$result = $db->getObjectList();
//			return $result;
//		}
//		function remove_promotion_product(){
//
//			$id = FSInput::get('id',0,'int');
//			$promotion_product_id = FSInput::get('promotion_product_id',0,'int');
//			if(!$id || !$promotion_product_id)
//				return;
//
//			$sql = " SELECT promotion_products
//				FROM fs_promotion
//				WHERE id = $id
//					";
//			global $db ;
//			$db->query($sql);
//			$rs =  $db->getResult();
//			if(!$rs)
//				return;
//
//			$arr = explode( ',',$rs);
//			if(!count($arr))
//				return;
//			$str = '';
//			$i  = 0;
//			foreach($arr as $item){
//				if($item != $promotion_product_id){
//					if($i > 0)
//						$str .= ',';
//					$str .= $item;
//					$i ++;
//				}
//			}
//			$row['promotion_products'] = $str;
//			print_r($row);
//
//			// remove from fs_products_incentives
//			$this -> remove_from_promotion_products($id ,$promotion_product_id);
//			return $this -> _update($row,'fs_promotion','id = '.$id .'');
//		}
//		function remove_from_promotion_products($id ,$promotion_product_id){
//			$sql = " DELETE FROM fs_promotion_products
//						WHERE product_id = $id
//							AND product_incenty_id = $promotion_product_id " ;
//			global $db;
//			$db->query($sql);
//			$rows = $db->affected_rows();
//		}
//		function update_product($id){
//			global $db;
//			$rs = 0;
//			$promotion_products = $this->get_records('product_id = '.$id,'fs_promotion_products');
//			$promotion = $this->get_record_by_id($id,'fs_promotion');
//			if(count($promotion_products)){
//				foreach($promotion_products as $item){
//					$product_incenty_id = $item -> product_incenty_id;
//					$product = $this->get_record_by_id($product_incenty_id,'fs_products','tablename');
//
//					$sql = ' UPDATE fs_products SET ';
//		            $sql .=  ' `promotion_price` =  "'.$item->price_new.'",';
//		            $sql .=  ' `date_start` =  "'.$promotion->date_start.'",';
//		            $sql .=  ' `date_end` =  "'.$promotion->date_end.'",';
//		            $sql .=  ' `promotion_title` =  "'.$promotion->title.'",';
//		            $sql .=  ' `promotion_info` =  "'.$item->inf_promotion.'",';
//		            $sql .=  ' `promotion_published` =  "'.$promotion->published.'"';
//		            $sql .=  ' WHERE id =    '.$product_incenty_id.' ';
//		            $db->query($sql);
//		            $result = $db->affected_rows();
//		            if($result){
//		            	$rs++;
//		            }
//
//	             	$sql_ext = ' UPDATE '.$product->tablename.' SET ';
//		            $sql_ext .=  ' `promotion_price` =  "'.$item->price_new.'",';
//		            $sql_ext .=  ' `date_start` =  "'.$promotion->date_start.'",';
//		            $sql_ext .=  ' `date_end` =  "'.$promotion->date_end.'",';
//		            $sql_ext .=  ' `promotion_title` =  "'.$promotion->title.'",';
//		            $sql_ext .=  ' `promotion_info` =  "'.$item->inf_promotion.'",';
//		            $sql_ext .=  ' `promotion_published` =  "'.$promotion->published.'"';
//		            $sql_ext .=  ' WHERE record_id =    '.$product_incenty_id.' ';
//					//echo $sql_ext; die;
//		            $db->query($sql_ext);
//		            $db->affected_rows();
//				}
//			}
//			return $rs;
//		}

	}
?>