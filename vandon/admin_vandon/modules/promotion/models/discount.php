<?php
class PromotionModelsDiscount extends FSModels
{
    var $limit;
    var $prefix;
    
    function __construct()
    {
        $this->limit = 20;
        $this->view = 'discount';
        $this->table_name = 'fs_promotion_discount';
        $this->img_folder = 'images/discount/' . date('Y/m/d');
        $this->check_alias = 0;
        $this->field_img = '';
        parent::__construct();
    }
    
    function save($row = array(), $use_mysql_real_escape_string = 1){
        $row['title']       = FSInput::get('title');
        $row['code']        = trim(FSInput::get('code'));
//        $row['values_apply']= FSInput::get('values_apply');
        $row['summary']     = FSInput::get('summary');
        $row['published']   = FSInput::get('published');
        $row['ordering']    = FSInput::get('ordering');
        $row['discount']    = FSInput::get('discount');
        $row['discount_unit']= FSInput::get('discount_unit');
        $row['start_date']      = strtotime(FSInput::get('start_date'));
//        echo strtotime(FSInput::get('start_date'));die;
        $row['expiration_date'] = strtotime(FSInput::get('expiration_date'));
        return  parent::save($row);
    }
}
?>