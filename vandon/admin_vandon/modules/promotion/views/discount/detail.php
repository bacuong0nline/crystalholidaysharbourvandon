<?php
$title = @$data ? FSText::_('Edit') : FSText::_('Add');
global $toolbar;
$toolbar->setTitle($title);
$toolbar->addButton('save_add', FSText:: _('Save and new'), '', 'save_add.png');
$toolbar->addButton('apply', FSText:: _('Apply'), '', 'apply.png');
$toolbar->addButton('Save', FSText:: _('Save'), '', 'save.png');
$toolbar->addButton('back', FSText:: _('Cancel'), '', 'back.png');
$arr_unitDiscount = array('1' => 'USD', '2' => '%');
$arr_objectUser = array('1' => 'Khách chưa có đơn', '2' => 'Khách đã có đơn', '3' => 'Khách có đơn từ');
?>
<!--<script language="javascript" type="text/javascript" src="../libraries/jquery/jquery.ui/jquery-ui.js"></script>-->
<!--<link rel="stylesheet" type="text/css" media="screen" href="../libraries/jquery/jquery.ui/jquery-ui.css" />-->
<?php
$this->dt_col_start('col-xs-12 col-md-12 connectedSortable', 1);
echo ' 	<div class="alert alert-danger alert-dismissible" style="display:none" >
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <span id="msg_error"></span>
        </div>';
$this->dt_form_begin();
TemplateHelper::dt_edit_text('Tên chương trình', 'title', @$data->title);
TemplateHelper::dt_edit_text('Mã giảm giá', 'code', @$data->code, '', 20);
TemplateHelper::dt_edit_text('Giảm', 'discount', @$data->discount, '', 20);
TemplateHelper::dt_edit_selectbox(FSText::_('Đơn vị tính'), 'discount_unit', (int)@$data->discount_unit, 0, $arr_unitDiscount, $field_value = 'id', $field_label = 'name', $size = 1, 0);
TemplateHelper::datetimepicke('Áp dụng từ ngày', 'start_date', date('Y-m-d', @$data->start_date), '', 20, $comment = '', $class_col1 = 'col-md-2', $class_col2 = 'col-md-3', 'YYYY-MM-DD');
TemplateHelper::datetimepicke('Áp dụng đến ngày', 'expiration_date', date('Y-m-d', @$data->expiration_date), '', 20, $comment = '', $class_col1 = 'col-md-2', $class_col2 = 'col-md-3', 'YYYY-MM-DD');
TemplateHelper::dt_edit_selectbox(FSText::_('Áp dụng cho đối tượng'), 'object_user', (int)@$data->object_user, 0, $arr_objectUser, $field_value = 'id', $field_label = 'name', $size = 1, 0);
TemplateHelper::dt_edit_text('Đơn từ ... USD trở lên  (Chỉ điền nếu mục "Khách có đơn từ")', 'values_apply', @$data->values_apply, 0, 12, 1, 0, '');
TemplateHelper::dt_edit_text('Số lượng áp dụng', 'number_apply', @$data->number_apply, 0, 12, 1, 0, 'Giá trị = 0 là không giới hạn');
if (isset($data->number_usered)) {
    TemplateHelper::dt_edit_text('Số lượng đã sử dụng', '', @$data->number_usered, 0, 20);
}
//TemplateHelper::dt_edit_text('Giới thiệu', 'summary', @$data->summary, '', 100, 7);
TemplateHelper::dt_checkbox('Kích hoạt', 'published', @$data->published, 1);
TemplateHelper::dt_edit_text('Thứ tự', 'ordering', @$data->ordering, @$maxOrdering, '20');
$this->dt_form_end(@$data, 1, 0);
$this->dt_col_end();
?>
<script type="text/javascript">
    // $("#start_date").datepicker({ dateFormat: "dd/mm/yy" });
    // $("#expiration_date").datepicker({ dateFormat: "dd/mm/yy" });

</script>