
<?php
$title = @$data ? FSText::_('Edit') : FSText::_('Add');
global $toolbar;
$toolbar->setTitle($title);
$toolbar->addButton('save_add', FSText:: _('Save and new'), '', 'save_add.png');
$toolbar->addButton('apply', FSText:: _('Apply'), '', 'apply.png');
$toolbar->addButton('Save', FSText:: _('Save'), '', 'save.png');
$toolbar->addButton('back', FSText:: _('Cancel'), '', 'back.png');
?>
<script language="javascript" type="text/javascript" src="../libraries/jquery/jquery.ui/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="../libraries/jquery/jquery.ui/jquery-ui.css" />
<link type="text/css" rel="stylesheet" media="all" href="../libraries/jquery/colorpicker/css/colorpicker.css"/>
<script type="text/javascript" src="../libraries/jquery/colorpicker/js/colorpicker.js"></script>
<script type="text/javascript" src="../libraries/jquery/colorpicker/js/eye.js"></script>
<?php
$this->dt_col_start('col-xs-12 col-md-12 connectedSortable', 1);
echo ' 	<div class="alert alert-danger alert-dismissible" style="display:none" >
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <span id="msg_error"></span>
        </div>';
$this->dt_form_begin();
TemplateHelper::dt_edit_text('Tên chương trình', 'name', @$data->name);
TemplateHelper::datetimepicke('Ngày bắt đầu', 'start_date', !empty($data->start_date) ? date('d/m/Y', strtotime($data->start_date)) : '', '', 20, $comment = '', $class_col1 = 'col-md-2', $class_col2 = 'col-md-3', 'DD/MM/Y');
TemplateHelper::datetimepicke('Ngày hết hạn', 'expiration_date', !empty($data->expiration_date) ? date('d/m/Y', strtotime($data->expiration_date)) : '', '', 20, $comment = '', $class_col1 = 'col-md-2', $class_col2 = 'col-md-3', 'DD/MM/Y');
TemplateHelper::dt_edit_text('Số lượng áp dụng', 'number_apply', @$data->number_apply, 0, 12, 1, 0, 'Giá trị = 0 là không giới hạn');
TemplateHelper::dt_edit_text('Áp dụng cho đơn từ ... sản phẩm', 'number_from', @$data->number_from, 0, 12, 1, 0, '');
TemplateHelper::dt_checkbox('Kích hoạt', 'published', @$data->published, 1);
TemplateHelper::dt_edit_text('Thứ tự', 'ordering', @$data->ordering, @$maxOrdering, '20');
if (isset($data->number_usered)) {
//    TemplateHelper::dt_edit_text('Số lượng đã sử dụng', '', @$data->number_usered, 0, 20);
}
$this->dt_form_end(@$data, 1, 0);
$this->dt_col_end();
?>
<script type="text/javascript" language="javascript">
    $(function () {
        $("#products_related_from_time").datepicker({
            clickInput: true,
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });

        //         $("#products_related_from_time").change(function () {
//             document.formSearch.submit();
//         });
        $("#products_related_to_time").datepicker({
            clickInput: true,
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });
//         $("#products_related_to_time").change(function () {
//             document.formSearch.submit();
//         });
    });

</script>