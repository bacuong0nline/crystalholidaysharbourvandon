<link type="text/css" rel="stylesheet" media="all" href="../libraries/jquery/jquery.ui/jquery-ui.css"/>
<script type="text/javascript" src="../libraries/jquery/jquery.ui/jquery-ui.js"></script>

<link type="text/css" rel="stylesheet" media="all" href="../libraries/jquery/colorpicker/css/colorpicker.css"/>
<script type="text/javascript" src="../libraries/jquery/colorpicker/js/colorpicker.js"></script>
<script type="text/javascript" src="../libraries/jquery/colorpicker/js/eye.js"></script>

<!-- FOR TAB -->
<script>
    $(document).ready(function () {
        $("#tabs").tabs();
    });
</script>
<?php
$title = @$data ? FSText:: _('Edit') : FSText:: _('Add');
global $toolbar;
$toolbar->setTitle($title);
$toolbar->addButton('save_add', FSText:: _('Save and new'), '', 'save_add.png');
$toolbar->addButton('apply', FSText:: _('Apply'), '', 'apply.png');
$toolbar->addButton('Save', FSText:: _('Save'), '', 'save.png');
$toolbar->addButton('back', FSText:: _('Cancel'), '', 'back.png');
$this->dt_col_start('col-xs-12 col-md-12 connectedSortable', 1);
echo ' 	<div class="alert alert-danger alert-dismissible" style="display:none" >
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <span id="msg_error"></span>
        </div>';
$this->dt_form_begin(0);
?>
<div id="tabs">
    <ul>
        <li><a href="#fragment-1"><span><?php echo FSText::_("Thông tin cơ bản"); ?></span></a></li>
        <?php if (isset($data)) { ?>
<!--            <li><a href="#fragment-2"><span>--><?php //echo FSText::_("Thêm sản phẩm"); ?><!--</span></a></li>-->
        <?php } ?>
    </ul>

    <!--	BASE FIELDS    -->
    <div id="fragment-1">
        <?php include_once 'detail_base.php'; ?>
    </div>
</div>
<?php
$this->dt_form_end(@$data, 0);
$this -> dt_col_end();
?>