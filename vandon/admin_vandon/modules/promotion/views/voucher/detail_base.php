<table cellspacing="1" class="admintable">
    <?php
    $arr_unitDiscount = array('1' => 'USD', '2' => '%', '3' => 'SamePrice');
    TemplateHelper::dt_edit_text(FSText:: _('Title'), 'title', @$data->name);
    TemplateHelper::dt_edit_text(FSText:: _('Alias'), 'alias', @$data->alias, '', 60, 1, 0, FSText::_("Can auto generate"));
    TemplateHelper::datetimepicke('Ngày bắt đầu', 'date_start', !empty($data->date_start) ? date('d/m/Y', strtotime($data->date_start)) : '', '', 20, $comment = '', $class_col1 = 'col-md-2', $class_col2 = 'col-md-3', 'DD/MM/Y');
    TemplateHelper::datetimepicke('Ngày hết hạn', 'date_end', !empty($data->date_end) ? date('d/m/Y', strtotime($data->date_end)) : '', '', 20, $comment = '', $class_col1 = 'col-md-2', $class_col2 = 'col-md-3', 'DD/MM/Y');
    TemplateHelper::dt_edit_selectbox(FSText::_('Đơn vị tính'), 'discount_unit', (int)@$data->discount_unit, 0, $arr_unitDiscount, $field_value = 'id', $field_label = 'name', $size = 1, 0);
    TemplateHelper::dt_edit_text('Giá trị giảm', 'discount', @$data->discount, '', 20);
    //    TemplateHelper::dt_edit_selectbox(FSText::_('Sản phẩm KM'), 'promotion_products', @$data->promotion_products, 0, $products, $field_value = 'id', $field_label = 'name', $size = 1, 1);
    TemplateHelper::dt_checkbox(FSText::_('Published'), 'published', @$data->published, 1);

    ?>
</table>

<div class="products_related">
    <div class="row">
        <div class="col-xs-12">
            <div class='products_related_search row'>
                <div class="row-item col-xs-2" style="margin-bottom: 20px;">
                    <select class="form-control chosen-select" name="products_related_category_id"
                            id="products_related_category_id">
                        <option value="">Danh mục</option>
                        <?php
                        foreach ($categories as $item) {
                            ?>
                            <option value="<?php echo $item->id; ?>"><?php echo $item->treename; ?> </option>
                        <?php } ?>
                    </select>
                </div>

                <!--                Khoảng giá-->
                <div class="row-item col-xs-2">
                    <div class="input-group custom-search-form">
                        <input type="text" placeholder="Giá từ" name='products_related_from' class="form-control"
                               value='' id='products_related_from'/>
                    </div>
                </div>
                <div class="row-item col-xs-2">
                    <div class="input-group custom-search-form">
                        <input type="text" placeholder="Giá đến" name='products_related_to' class="form-control"
                               value='' id='products_related_to'/>
                    </div>
                </div>
                <!--                Khoảng giá-->

                <!--                Khoảng thời gian up-->
                <div class="row-item col-xs-2">
                    <div class="input-group custom-search-form">
                        <input type="datetime" placeholder="Time From" name='products_related_from_time'
                               class="form-control" value='' id='products_related_from_time'/>
                    </div>
                </div>
                <div class="row-item col-xs-2">
                    <div class="input-group custom-search-form">
                        <input type="text" placeholder="Time to" name='products_related_to_time' class="form-control"
                               value='' id='products_related_to_time'/>
                    </div>
                </div>
                <!--                Khoảng thời gian up-->
                <div class="row-item col-xs-2">
                    <div class="input-group custom-search-form">
                        <input type="text" placeholder="Tên/Mã" name='products_related_keyword' class="form-control"
                               value='' id='products_related_keyword'/>
                        <span class="input-group-btn">
							<a id='products_related_search' class="btn btn-default">
								<i class="fa fa-search"></i>
							</a>
						</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class='title-related'><?php echo FSText::_('Danh sách sản phẩm') ?></div>
            <div id='products_related_l'>
                <div id='products_related_search_list'></div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6">
            <div id='products_related_r'>
                <!--	LIST RELATE			-->
                <div class='title-related'><?php echo FSText::_('Sản phẩm đã chọn') ?></div>
                <ul id='products_sortable_related'>
                    <?php
                    $i = 0;
                    if (isset($products_related))
                        foreach ($products_related as $item) {
                            ?>
                            <li id='products_record_related_<?php echo $item->id ?>'>
                                <div class="left">
                                    <img width="80"
                                         src="<?php echo URL_ROOT . str_replace('/original/', '/resized/', $item->image); ?>">
                                </div>
                                <div class="right">
                                    <?php
                                    $sub = $this->model->get_records('product_id="' . $item->id . '" AND published = 1 ', 'fs_products_sub');
                                    $price = 0;
                                    $price_total = 0;
                                    foreach ($sub as $item_sub) {
                                        if ($item_sub->price_h != 0) {
                                            $price = $item_sub->price_h;
                                            $price_total += $price;
                                        } else {
                                            $price = $item_sub->price;
                                            $price_total += $price;
                                        }
                                    }
                                    ?>
                                    <p class="name"><?php echo $item->name; ?></p>
                                    <div>
                                        <span class="border-right">Mã sp:<?php echo $item->id ?></span><span
                                                class="border-right">Giá Lot:<?php echo '$' . $price_total ?></span>
                                        <p class="">Ngày
                                            đăng:<?php echo date('Y/m/d', strtotime($item->created_time)) ?></p>
                                    </div>

                                </div>
                                <div>
                                    <a class='products_remove_relate_bt'
                                       onclick="javascript: remove_products_related(<?php echo $item->id ?>)"
                                       href="javascript: void(0)" title='Xóa'>
                                        <img border="0" alt="Remove" src="templates/default/images/toolbar/remove.png">
                                    </a>
                                </div>

                                <!--                                <img width="80" src="-->
                                <?php //echo URL_ROOT . str_replace('/original/', '/resized/', $item->image); ?><!--">-->

                                <input type="hidden" name='products_record_related[]' value="<?php echo $item->id; ?>"/>
                            </li>
                        <?php } ?>
                </ul>
                <!--	end LIST RELATE			-->
                <div id='products_record_related_continue'></div>
            </div>
        </div>

        <!--<div class='products_close_related col-xs-12' style="display:none">
                <a href="javascript:products_close_related()"><strong class='red'>Đóng</strong></a>
            </div>
            <div class='products_add_related col-xs-12'>
                <a href="javascript:products_add_related()"><strong class='red'>Thêm sản phẩm liên quan</strong></a>
            </div> -->
    </div>
</div>
<script type="text/javascript">
    //    search_products_related();
    //     $("#products_sortable_related").sortable();

    function products_add_related() {
        $('#products_related_l').show();
        $('#products_related_l').attr('width', '50%');
        $('#products_related_r').attr('width', '50%');
        $('.products_close_related').show();
        $('.products_add_related').hide();
    }

    function products_close_related() {
        $('#products_related_l').hide();
        $('#products_related_l').attr('width', '0%');
        $('#products_related_r').attr('width', '100%');
        $('.products_add_related').show();
        $('.products_close_related').hide();
    }
    //function search_products_related(){
    //    $("#products_related_category_id").change(function() {
    //        var category_id = $('#products_related_category_id').val();
    //        var product_id = <?php //echo @$data->id ? $data->id : 0 ?>//;
    //        var str_exist = '';
    //        products_related(keyword = null, category_id, product_id, str_exist)
    //    })
    $('#products_related_search').on('click', function () {
        var keyword = $('#products_related_keyword').val();
        var price_from = $('#products_related_from').val();
        var price_to = $('#products_related_to').val();
        var time_from = $('#products_related_from_time').val();
        var time_to = $('#products_related_to_time').val();
        var category_id = $('#products_related_category_id').val();
        var product_id = <?php echo @$data->id ? $data->id : 0 ?>;
        var str_exist = '';
        products_related(keyword, category_id, product_id, str_exist, price_from, price_to, time_from, time_to);
    });

    $('#products_related_keyword').on('keyup', function (e) {
        if (e.key === 'Enter' || e.keyCode === 13) {
            var keyword = $('#products_related_keyword').val();
            if (keyword == '' || keyword == null) {
                alert('Bạn chưa nhập từ khóa tìm kiếm');
                return
            }
            var category_id = $('#products_related_category_id').val();
            var product_id = <?php echo @$data->id ? $data->id : 0 ?>;
            var str_exist = '';
            products_related(keyword, category_id = null, product_id, str_exist);
        }
    });
    //}
    function products_related(keyword, category_id, product_id, str_exist, price_from, price_to, time_from, time_to) {

        $("#products_sortable_related li input").each(function (index) {
            if (str_exist != '')
                str_exist += ',';
            str_exist += $(this).val();
        });
        $.get("index2.php?module=promotion&view=promotion&task=ajax_get_products_related&raw=1", {
            product_id: product_id,
            keyword: keyword,
            price_from: price_from,
            price_to: price_to,
            time_from: time_from,
            time_to: time_to,
            category_id: category_id,
            str_exist: str_exist
        }, function (html) {
            $('#products_related_search_list').html(html);
        });
    }
    function set_products_related(id) {
        var max_related = 1000;
        var length_children = $("#products_sortable_related li").length;
        if (length_children >= max_related) {
            alert('Tối đa chỉ có ' + max_related + ' sản phẩm liên quan');
            return;
        }
        var title = $('.products_related_item_' + id).html();
        var html = '<li id="products_record_related_' + id + '">' + title + '<input type="hidden" name="products_record_related[]" value="' + id + '" />';
        html += '<a class="products_remove_relate_bt"  onclick="javascript: remove_products_related(' + id + ')" href="javascript: void(0)" title="Xóa"><img border="0" alt="Remove" src="templates/default/images/toolbar/remove.png"></a>';
        html += '</li>';
        $('#products_sortable_related').append(html);
        $('.products_related_item_' + id).hide();
    }

    function set_products_related_disable(id) {
        alert('Không thể thêm, Sản phẩm thuộc chương trình khuyến mại khác');
    }

    function remove_products_related(id) {
        $('#products_record_related_' + id).remove();
        $('.products_related_item_' + id).show().addClass('red');
    }
</script>
<style>
    .title-related {
        background: none repeat scroll 0 0 #F0F1F5;
        font-weight: bold;
        margin-bottom: 4px;
        padding: 2px 0 4px;
        text-align: center;
        width: 100%;
    }

    #products_related_search_list {
        height: 400px;
        overflow: scroll;
    }

    .products_related_item {
        background: url("/admin/images/page_next.gif") no-repeat scroll right center transparent;
        border-bottom: 1px solid #EEEEEE;
        cursor: pointer;
        margin: 2px 10px;
        padding: 5px;
    }

    .products_related_item_disable {
        background: url("/admin/images/page_next.gif") no-repeat scroll right center transparent;
        border-bottom: 1px solid #EEEEEE;
        cursor: pointer;
        margin: 2px 10px;
        padding: 5px;
        opacity: 0.5;
    }

    #products_sortable_related li {
        cursor: move;
        list-style: decimal outside none;
        margin-bottom: 8px;
        display: grid;
        grid-template-columns: 20% 70% 10%;
        padding: 10px 0px;
        border-bottom: 1px solid #ddd;
    }

    #products_sortable_related li .right .border-right {
        padding-right: 5px;
        padding-left: 5px;
        border-right: 1px solid #ccc
    }

    #products_sortable_related li .right .name {
        font-weight: 600;
        color: #737373;
    }

    .products_remove_relate_bt {
        padding-left: 10px;
    }

    .products_related table {
        margin-bottom: 5px;
    }

    #products_related_l .products_related_item {
        display: grid;
        grid-template-columns: 20% 70% 10%;
        padding: 10px 0px;
        border-bottom: 1px solid #ddd;
    }
</style>

<script type="text/javascript" language="javascript">
    $(function () {
        $("#products_related_from_time").datepicker({
            clickInput: true,
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });

        //         $("#products_related_from_time").change(function () {
//             document.formSearch.submit();
//         });
        $("#products_related_to_time").datepicker({
            clickInput: true,
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });
//         $("#products_related_to_time").change(function () {
//             document.formSearch.submit();
//         });
    });

</script>

