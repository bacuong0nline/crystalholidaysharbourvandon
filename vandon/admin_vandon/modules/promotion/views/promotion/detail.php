<link type="text/css" rel="stylesheet" media="all" href="../libraries/jquery/jquery.ui/jquery-ui.css"/>
<script type="text/javascript" src="../libraries/jquery/jquery.ui/jquery-ui.js"></script>

<link type="text/css" rel="stylesheet" media="all" href="../libraries/jquery/colorpicker/css/colorpicker.css"/>
<script type="text/javascript" src="../libraries/jquery/colorpicker/js/colorpicker.js"></script>
<script type="text/javascript" src="../libraries/jquery/colorpicker/js/eye.js"></script>

<!-- FOR TAB -->
<script>
    $(document).ready(function () {
        $("#tabs").tabs();
    });
</script>
<?php
$title = @$data ? FSText:: _('Edit') : FSText:: _('Add');
global $toolbar;
$toolbar->setTitle($title);
$toolbar->addButton('save_add', FSText:: _('Save and new'), '', 'save_add.png');
$toolbar->addButton('apply', FSText:: _('Apply'), '', 'apply.png');
$toolbar->addButton('Save', FSText:: _('Save'), '', 'save.png');
$toolbar->addButton('back', FSText:: _('Cancel'), '', 'back.png');
echo ' 	<div class="alert alert-danger alert-dismissible" style="display:none" >
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <span id="msg_error"></span>
        </div>';
$this->dt_form_begin(0);
?>
<div id="tabs">
    <ul>
        <li><a href="#fragment-1"><span><?php echo FSText::_("Thông tin cơ bản"); ?></span></a></li>
        <?php if (isset($data)) { ?>
        <?php } ?>
    </ul>

    <!--	BASE FIELDS    -->
    <div id="fragment-1">
        <?php include_once 'detail_base.php'; ?>
    </div>
</div>
<?php
$this->dt_form_end(@$data, 0);
?>

<style>
    #gmap {
        height: 400px;
        margin: 20px 0px;
        width: 100% !important;
    }

    .controls {
        margin-top: 16px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 15px;
        text-overflow: ellipsis;
        width: 400px;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    }

    .pac-container {
        font-family: Roboto;
    }

    #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
    }

    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }
    table th, td {text-align: center;}
    .ui-widget-header {
        background:none;
        border:none;
    }
    .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
        border: none;
    }
    .ui-state-active a, .ui-state-active a:link, .ui-state-active a:visited {
        color: #C73536 !important;
    }
    .ui-state-default a, .ui-state-default a:link, .ui-state-default a:visited {
        color: #333;
    }
    body .panel a:focus, body .panel a:hover {
        color: #C73536 !important;
        background-color: transparent !important;
    }
    .ui-widget-content {
        border: none;
        background: white;
    }
    .ui-state-default:focus {
        outline: none !important;
    }
    .panel-info>.panel-heading {
    color: #ffffff;
    background-color: #333;
    border-color: #333;
    }
    .panel-info {
        border: none;
    }
</style>