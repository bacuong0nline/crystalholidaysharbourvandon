<?php

class PromotionControllersVoucher extends Controllers
{
    function __construct()
    {
        $this->view = 'voucher';
        parent::__construct();
    }

    function display()
    {
        parent::display();
        $sort_field = $this->sort_field;
        $sort_direct = $this->sort_direct;

        $model = $this->model;
        $list = $model->get_data('');
        $pagination = $model->getPagination();
        include 'modules/' . $this->module . '/views/' . $this->view . '/list.php';
    }

    function add()
    {
        $ids = FSInput::get('id', array(), 'array');
//        $id = $ids[0];
        $model = $this->model;
        $products = $model->get_all_products();
//        $categories = $model->get_categories_tree();
//			$tags_categories = $model->get_tags_categories();
//			$data = $model->get_record_by_id($id);
        // data from fs_news_categories

//			$promotion_products = $model -> get_promotion_products($data -> id);

        include 'modules/' . $this->module . '/views/' . $this->view . '/detail.php';
    }

    function edit()
    {
        $ids = FSInput::get('id', array(), 'array');
        $id = $ids[0];
        $model = $this->model;
        $categories = $model->get_categories_tree();
//			$tags_categories = $model->get_tags_categories();
        $data = $model->get_record_by_id($id);
        @$products_related = $model->get_products_related($data->products_related);
        $products = $model->get_all_products();
        // data from fs_news_categories

//			$promotion_products = $model -> get_promotion_products($data -> id);

        include 'modules/' . $this->module . '/views/' . $this->view . '/detail.php';
    }

    // remove products_together
    function remove_product()
    {
        $model = $this->model;
        if ($model->remove_promotion_product()) {
            echo '1';
            return;
        } else {
            echo '0';
            return;
        }
    }

    function update_product($id)
    {

        $model = $this->model;
        $promotion = $model->get_record_by_id($id, 'fs_promotion');
//			if($promotion -> date_start < date('Y-m-d H:i:s') && $promotion -> date_end > date('Y-m-d H:i:s')){
        $link = 'index.php?module=' . $this->module . '&view=promotion&task=update&id=' . $id;
//			}else{
//				$link = 'index.php?module='.$this -> module.'&view=promotion&task=no_update';
//			}
        return '<a href="' . $link . '" title="Update lại toàn bộ danh sách  sản phẩm" ><img src="templates/default/images/toolbar/icon-update.png" /> </a>';

    }

    /*
     *
     */
    function update()
    {
        $id = FSInput::get('id', 0, 'int');
        $link = 'index.php?module=' . $this->module . '&view=' . $this->view;
        $model = $this->model;
        $rs = $model->update_product($id);
        if ($this->page)
            $link .= '&page=' . $this->page;
        if ($rs) {
            setRedirect($link, $rs . ' ' . FSText:: _('Sản phẩm  đã update'));
        } else {
            setRedirect($link, FSText:: _('Không có sản phẩm  nào được upadte'));
        }
    }

    function no_update()
    {
        $link = 'index.php?module=' . $this->module . '&view=' . $this->view;
        if ($this->page)
            $link .= '&page=' . $this->page;
        setRedirect($link, FSText:: _('Không thể update. Bạn hãy kiểm tra lại thời gian k/m'), 'error');

    }

    function ajax_get_products_related()
    {
        $model = $this->model;
        $data = $model->ajax_get_products_related();
        $html = $this->products_genarate_related($data);
        echo $html;
        return;
    }

    function products_genarate_related($data)
    {
        $str_exist = FSInput::get('str_exist');
        $html = '';
        $html .= '<div class="products_related">';
        foreach ($data as $item) {
            $arr_check = $this->get_all_promotion($item->id);

            if ($str_exist && strpos(',' . $str_exist . ',', ',' . $item->id . ',') !== false) {
                if ($arr_check[0] == 1 || $arr_check[1] == 1) {
                    $html .= '<div class="red products_related_item_disable  products_related_item_' . $item->id . '" onclick="javascript: set_products_related_disable(' . $item->id . ')" style="display:none" >';
                }else{
                    $html .= '<div class="red products_related_item  products_related_item_' . $item->id . '" onclick="javascript: set_products_related(' . $item->id . ')" style="display:none" >';
                }
                $html .= '<div>';
                $html .= '<img src="' . str_replace('/original/', '/resized/', URL_ROOT . @$item->image) . '" width="80">';
//                $html .= '<img src="' .  URL_ROOT . @$item->image . '" width="80">';
                $html .= '</div>';
                $html .= '<div>';
                $html .= $item->name;
                $html .= '<div>';
                $html .= '<span style="border-right: 1px solid #ccc; padding: 0px 5px">';
                $html .= 'Mã sp:' . $item->id;
                $html .= '</span>';
                $html .= '<span style="border-right: 1px solid #ccc; padding: 0px 5px">';
                $html .= 'Giá Lot' . $item->price;
                $html .= '</span>';
                $html .= '<span style="border-right: 1px solid #ccc; padding: 0px 5px">';
                $html .= 'Ngày đăng' . date('Y/m/d',strtotime($item->created_time));
                $html .= '</div>';
                $html .= '</div>';
                $html .= '</div>';
            } else {
                if ($arr_check[0] == 1 || $arr_check[1] == 1){
                    $html .= '<div class="products_related_item_disable  products_related_item_' . $item->id . '" onclick="javascript: set_products_related_disable(' . $item->id . ')">';
                }else{
                    $html .= '<div class="products_related_item  products_related_item_' . $item->id . '" onclick="javascript: set_products_related(' . $item->id . ')">';
                }
                $html .= '<div>';
                $html .= '<img src="' . str_replace('/original/', '/resized/', URL_ROOT . @$item->image) . '" width="80">';
//                $html .= '<img src="' .  URL_ROOT . @$item->image . '" width="80">';
                $html .= '</div>';
                $html .= '<div>';
                $html .= $item->name;
                $html .= '<div>';
                $html .= '<span style="border-right: 1px solid #ccc; padding: 0px 5px">';
                $html .= 'Mã sp:' . $item->id;
                $html .= '</span>';
                $html .= '<span style="border-right: 1px solid #ccc; padding: 0px 5px">';
                $html .= 'Giá Lot:' . $item->price;
                $html .= '</span>';
                $html .= '<p style="border-right: 1px solid #ccc; padding: 0px 5px">';
                $html .= 'Ngày đăng:' . date('Y/m/d',strtotime($item->created_time));
                $html .= '</p>';
                $html .= '</div>';
                $html .= '</div>';
                $html .= '</div>';
            }
        }
        $html .= '</div>';
        return $html;
    }

    function get_all_promotion($id){
        $list_promotion = $this->model->get_promotion();
        $check_promotion = 0;
        foreach ($list_promotion as $item) {
            $list = $item->products_related;
            $arr  = explode(',',$list);
            for ($i = 0; $i< count($arr); $i++){
                if ($id == $arr[$i]){
                    $check_promotion = 1;
                    break;
                }
            }
        }

        $list_manual = $this->model->get_manual();
        $check_manual = 0;
        foreach ($list_manual as $item) {
            $list = $item->products_related;
            $arr  = explode(',',$list);
            for ($i = 0; $i< count($arr); $i++){
                if ($id == $arr[$i]){
                    $check_manual = 1;
                    break;
                }
            }
        }
        $arr_check = [$check_promotion, $check_manual];
        return $arr_check;
    }
}

?>