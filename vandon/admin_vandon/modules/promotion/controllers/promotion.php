<?php

class PromotionControllersPromotion extends Controllers
{
    function __construct()
    {
        $this->view = 'promotion';
        parent::__construct();
    }

    function display()
    {
        parent::display();
        $sort_field = $this->sort_field;
        $sort_direct = $this->sort_direct;

        $model = $this->model;
        $list = $model->get_data('');
        $pagination = $model->getPagination();
        include 'modules/' . $this->module . '/views/' . $this->view . '/list.php';
    }

    function add()
    {
        $ids = FSInput::get('id', array(), 'array');
//        $id = $ids[0];
        $model = $this->model;
        $products = $model->get_all_products();
        $categories = $model->get_categories_tree();
//			$tags_categories = $model->get_tags_categories();
//			$data = $model->get_record_by_id($id);
        // data from fs_news_categories

//			$promotion_products = $model -> get_promotion_products($data -> id);

        include 'modules/' . $this->module . '/views/' . $this->view . '/detail.php';
    }

    function edit()
    {
        $ids = FSInput::get('id', array(), 'array');
//        var_dump($ids);die;
        $id = $ids[0];
        $model = $this->model;
        $categories = $model->get_categories_tree();
//			$tags_categories = $model->get_tags_categories();
        $data = $model->get_record_by_id($id);
        @$products_related = $model->get_products_related($data->products_related);
        $products = $model->get_all_products();
        // data from fs_news_categories

//			$promotion_products = $model -> get_promotion_products($data -> id);

        include 'modules/' . $this->module . '/views/' . $this->view . '/detail.php';
    }

    // remove products_together
    function remove_product()
    {
        $model = $this->model;
        if ($model->remove_promotion_product()) {
            echo '1';
            return;
        } else {
            echo '0';
            return;
        }
    }

    function update_product($id)
    {

        $model = $this->model;
        $promotion = $model->get_record_by_id($id, 'fs_promotion');
//			if($promotion -> date_start < date('Y-m-d H:i:s') && $promotion -> date_end > date('Y-m-d H:i:s')){
        $link = 'index.php?module=' . $this->module . '&view=promotion&task=update&id=' . $id;
//			}else{
//				$link = 'index.php?module='.$this -> module.'&view=promotion&task=no_update';
//			}
        return '<a href="' . $link . '" title="Update lại toàn bộ danh sách  sản phẩm" ><img src="templates/default/images/toolbar/icon-update.png" /> </a>';

    }

    /*
     *
     */
    function update()
    {
        $id = FSInput::get('id', 0, 'int');
        $link = 'index.php?module=' . $this->module . '&view=' . $this->view;
        $model = $this->model;
        $rs = $model->update_product($id);
        if ($this->page)
            $link .= '&page=' . $this->page;
        if ($rs) {
            setRedirect($link, $rs . ' ' . FSText:: _('Sản phẩm  đã update'));
        } else {
            setRedirect($link, FSText:: _('Không có sản phẩm  nào được upadte'));
        }
    }

    function no_update()
    {
        $link = 'index.php?module=' . $this->module . '&view=' . $this->view;
        if ($this->page)
            $link .= '&page=' . $this->page;
        setRedirect($link, FSText:: _('Không thể update. Bạn hãy kiểm tra lại thời gian k/m'), 'error');

    }

    function ajax_get_products_related()
    {
        $id = FSInput::get('product_id');
        $model = $this->model;
        $data = $model->ajax_get_products_related();
        echo $html = $this->products_genarate_related($data);
        return;
    }

    function products_genarate_related($data)
    {
        $str_exist = FSInput::get('str_exist');
        $html = '';
        $html .= '<div class="products_related">';
        foreach ($data as $item) {
            if ($str_exist && strpos(',' . $str_exist . ',', ',' . $item->id . ',') !== false) {
                $html .= '<div class="red products_related_item  products_related_item_' . $item->id . '" onclick="javascript: set_products_related(' . $item->id . ')" style="display:none" >';
                $html .= $item->name . '<br/>';
                $html .= '<img src="' . str_replace('/original/', '/resized/', URL_ROOT . @$item->image) . '" width="80">';
                $html .= '</div>';
            } else {
                $html .= '<div class="products_related_item  products_related_item_' . $item->id . '" onclick="javascript: set_products_related(' . $item->id . ')">';
                $html .= $item->name . '<br/>';
                $html .= '<img src="' . str_replace('/original/', '/resized/', URL_ROOT . @$item->image) . '" width="80">';
                $html .= '</div>';
            }
        }
        $html .= '</div>';
        return $html;
    }

    function get_all_promotion($id,$promotion_id){
        $time = date('Y-m-d H:i:s');
        $cur_promotion = $this->model->get_cur_promotion($promotion_id);
        @$cur_pro_date_end = $cur_promotion->date_end;
        @$cur_pro_date_start = $cur_promotion->date_start;
        $list_promotion = $this->model->get_promotion();
        $check_promotion = 0;
        foreach ($list_promotion as $item) {
            $list = $item->products_related;
            $arr  = explode(',',$list);
            if (in_array($id,$arr)){
                $exist_id = $item->id;
                $exist_promotion = $this->model->get_exist_promotion($exist_id);
                foreach ($exist_promotion as $item_exist){
                    if (($cur_pro_date_start < $cur_pro_date_end)&&(($cur_pro_date_start > $item_exist->date_end ) || ($cur_pro_date_end < @$item_exist->date_start ))){
                        $check_promotion = 0;
                    }else{
                        $check_promotion = $item->id;
                    }
                }

            }
        }

        $cur_manual = $this->model->get_cur_manual($promotion_id);
        @$cur_man_date_end = $cur_manual->date_end;
        @$cur_man_date_start = $cur_manual->date_start;
        $list_manual = $this->model->get_manual();
        $check_manual = 0;
        foreach ($list_manual as $item_manual) {
            $list = $item_manual->products_related;
            $arr  = explode(',',$list);
            if (in_array($id,$arr)){
                $exist_id = $item_manual->id;
                $exist_manual = $this->model->get_exist_manual($exist_id);
                foreach ($exist_manual as $item_exist){
                    if (($cur_man_date_start < $cur_man_date_end) && (($cur_man_date_start > $item_exist->date_end ) || ($cur_man_date_end < @$item_exist->date_start ))){
                        $check_manual = 0;

                    }else{
                        $check_manual = $item_manual->id;
                    }
                }
            }
        }

        $cur_gba = $this->model->get_cur_gba($promotion_id);
        @$cur_gba_date_end = $cur_gba->date_end;
        @$cur_gba_date_start = $cur_gba->date_start;
        $list_gba = $this->model->get_gba();
        $check_gba = 0;
        foreach ($list_gba as $item_gba) {
            $list = $item_gba->products_related;
            $arr  = explode(',',$list);
            if (in_array($id,$arr)){
                $exist_id = $item_gba->id;
                $exist_gba = $this->model->get_exist_gba($exist_id);
                foreach ($exist_gba as $item_exist){
                    if (($cur_gba_date_start < $cur_gba_date_end) && (($cur_gba_date_start > $item_exist->date_end ) || ($cur_gba_date_end < @$item_exist->date_start ))){
                        $check_gba = 0;

                    }else{
                        $check_gba = $item_gba->id;
                    }
                }
            }
        }

        $arr_check = [$check_promotion, $check_manual, $check_gba];
        return $arr_check;
    }
}

?>