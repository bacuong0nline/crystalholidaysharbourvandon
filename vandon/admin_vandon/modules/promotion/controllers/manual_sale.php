<?php
class PromotionControllersManual_sale extends Controllers
{
    function __construct()
    {
        $this->view = 'manual_sale';
        parent::__construct();
    }
    function display()
    {
        parent::display();
        $sort_field = $this->sort_field;
        $sort_direct = $this->sort_direct;
        $model = $this->model;
        $list = $model->get_data('');
        $pagination = $model->getPagination();
        include 'modules/' . $this->module . '/views/' . $this->view . '/list.php';
    }
    function add()
    {
        $model = $this->model;
        $maxOrdering = $model->getMaxOrdering();
        $products = $model->get_all_products();
        include 'modules/' . $this->module . '/views/' . $this->view . '/detail.php';
    }
    function edit()
    {
        $ids = FSInput::get('id', array(), 'array');
        $id = $ids[0];
        $model = $this->model;
        $categories = $model->get_categories_tree();
        $data = $model->get_record_by_id($id);
        @$products_related = $model->get_products_related($data->products_related);
        $products = $model->get_all_products();
        include 'modules/' . $this->module . '/views/' . $this->view . '/detail.php';
    }
    function ajax_get_products_related()
    {
        $id = FSInput::get('product_id');
        $model = $this->model;
        $data = $model->ajax_get_products_related();
        $html = $this->products_genarate_related($data,$id);
        echo $html;
        return;
    }

    function products_genarate_related($data,$promotion_id)
    {
        $str_exist = FSInput::get('str_exist');
        $html = '';
        $html .= '<div class="products_related">';
        foreach ($data as $item) {
            $arr_check = $this->get_all_promotion($item->id, $promotion_id);

            if ($str_exist && strpos(',' . $str_exist . ',', ',' . $item->id . ',') !== false) {
                if ($arr_check[0] != 0 || $arr_check[1] != 0 || $arr_check[2]!=0) {
                    $html .= '<div class="red products_related_item_disable  products_related_item_' . $item->id . '" onclick="javascript: set_products_related_disable(' . $item->id . ')" style="display:none" >';
                }else{
                    $html .= '<div class="red products_related_item  products_related_item_' . $item->id . '" onclick="javascript: set_products_related(' . $item->id . ')" style="display:none" >';
                }
//                $html .= '<div>';
//                $html .= '<img src="' . str_replace('/original/', '/resized/', URL_ROOT . @$item->image) . '" width="80">';
//                $html .= '<img src="' .  URL_ROOT . @$item->image . '" width="80">';
//                $html .= '</div>';
                $html .= '<div>';
                $html .= '<p class="name">';
                $html .= $item->name;
                $html .= '</p>';
                $html .= '<div>';
                $html .= '<span style="border-right: 1px solid #ccc; padding: 0px 5px">';
                $html .= 'Mã sp:' . $item->model;
                $html .= '</span>';
                $html .= '<span style="border-right: 1px solid #ccc; padding: 0px 5px">';
                $html .= 'Giá Lot' . $item->price;
                $html .= '</span>';
                $html .= '<span style="border-right: 1px solid #ccc; padding: 0px 5px">';
                $html .= 'Ngày đăng' . date('Y/m/d',strtotime($item->created_time));
                $html .= '<span class="max_quant" style="border-right: 1px solid #ccc; padding: 0px 5px">';
                $html .= 'Số lượng Lot: ' . $item->quantity;
                $html .= '</span>';
                $html .= '</div>';
                $html .= '</div>';
                $html .= '</div>';
            } else {
                if ($arr_check[0] != 0 || $arr_check[1] != 0 || $arr_check[2] != 0){
                    $html .= '<div class="products_related_item_disable  products_related_item_' . $item->id . '" onclick="javascript: set_products_related_disable(' . $item->id . ')">';
                }else{
                    $html .= '<div class="products_related_item  products_related_item_' . $item->id . '" onclick="javascript: set_products_related(' . $item->id . ')">';
                }
//                $html .= '<div>';
//                $html .= '<img src="' . str_replace('/original/', '/resized/', URL_ROOT . @$item->image) . '" width="80">';
//                $html .= '<img src="' .  URL_ROOT . @$item->image . '" width="80">';
//                $html .= '</div>';
                $html .= '<div>';
                $html .= '<p class="name">';
                $html .= $item->name;
                $html .= '</p>';
                $html .= '<div>';
                $html .= '<span style="border-right: 1px solid #ccc; padding: 0px 5px">';
                $html .= 'Mã sp:' . $item->model;
                $html .= '</span>';
                $html .= '<span style="border-right: 1px solid #ccc; padding: 0px 5px">';
                $html .= 'Giá Lot:' . $item->price;
                $html .= '</span>';
                $html .= '<p style="border-right: 1px solid #ccc; padding: 0px 5px">';
                $html .= 'Ngày đăng:' . date('Y/m/d',strtotime($item->created_time));
                $html .= '<span class="max_quant" style="border-right: 1px solid #ccc; padding: 0px 5px">';
                $html .= 'Số lượng Lot: ' . $item->quantity;
                $html .= '</span>';
                $html .= '</p>';
                $html .= '</div>';
                $html .= '</div>';
                $html .= '</div>';
            }
        }
        $html .= '</div>';
        return $html;
    }

    function get_all_promotion($id,$promotion_id){
        $time = date('Y-m-d H:i:s');
        $cur_promotion = $this->model->get_cur_promotion($promotion_id);
        @$cur_pro_date_end = $cur_promotion->date_end;
        @$cur_pro_date_start = $cur_promotion->date_start;
        $list_promotion = $this->model->get_promotion();
        $check_promotion = 0;
        foreach ($list_promotion as $item) {
            $list = $item->products_related;
            $arr  = explode(',',$list);
            if (in_array($id,$arr)){
                $exist_id = $item->id;
                $exist_promotion = $this->model->get_exist_promotion($exist_id);
                foreach ($exist_promotion as $item_exist){
                    if (($cur_pro_date_start < $cur_pro_date_end)&&(($cur_pro_date_start > $item_exist->date_end ) || ($cur_pro_date_end < @$item_exist->date_start ))){
                        $check_promotion = 0;
                    }else{
                        $check_promotion = $item->id;
                    }
                }

            }
        }

        $cur_manual = $this->model->get_cur_manual($promotion_id);
        @$cur_man_date_end = $cur_manual->date_end;
        @$cur_man_date_start = $cur_manual->date_start;
        $list_manual = $this->model->get_manual();
        $check_manual = 0;
        foreach ($list_manual as $item_manual) {
            $list = $item_manual->products_related;
            $arr  = explode(',',$list);
            if (in_array($id,$arr)){
                $exist_id = $item_manual->id;
                $exist_manual = $this->model->get_exist_manual($exist_id);
                foreach ($exist_manual as $item_exist){
                    if (($cur_man_date_start < $cur_man_date_end) && (($cur_man_date_start > $item_exist->date_end ) || ($cur_man_date_end < @$item_exist->date_start ))){
                        $check_manual = 0;

                    }else{
                        $check_manual = $item_manual->id;
                    }
                }
            }
        }

        $cur_gba = $this->model->get_cur_gba($promotion_id);
        @$cur_gba_date_end = $cur_gba->date_end;
        @$cur_gba_date_start = $cur_gba->date_start;
        $list_gba = $this->model->get_gba();
        $check_gba = 0;
        foreach ($list_gba as $item_gba) {
            $list = $item_gba->products_related;
            $arr  = explode(',',$list);
            if (in_array($id,$arr)){
                $exist_id = $item_gba->id;
                $exist_gba = $this->model->get_exist_gba($exist_id);
                foreach ($exist_gba as $item_exist){
                    if (($cur_gba_date_start < $cur_gba_date_end) && (($cur_gba_date_start > $item_exist->date_end ) || ($cur_gba_date_end < @$item_exist->date_start ))){
                        $check_gba = 0;

                    }else{
                        $check_gba = $item_gba->id;
                    }
                }
            }
        }

        $arr_check = [$check_promotion, $check_manual, $check_gba];
        return $arr_check;
    }
}
?>