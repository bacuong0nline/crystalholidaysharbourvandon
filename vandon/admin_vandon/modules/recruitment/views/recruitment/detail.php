<link type="text/css" rel="stylesheet" media="all" href="../libraries/jquery/jquery.ui/jquery-ui.css" />
<script type="text/javascript" src="../libraries/jquery/jquery.ui/jquery-ui.js"></script>
<!-- HEAD -->
<?php

$title = @$data ? FSText::_('Edit') : FSText::_('Add');
global $toolbar;
$toolbar->setTitle($title);
$toolbar->addButton('apply', FSText::_('Apply'), '', 'apply.png', 1);
$toolbar->addButton('Save', FSText::_('Save'), '', 'save.png', 1);
$toolbar->addButton('cancel', FSText::_('Cancel'), '', 'cancel.png');

echo ' 	<div class="alert alert-danger" style="display:none" >
                    <span id="msg_error"></span>
            </div>';

$this->dt_form_begin(1, 4, $title . ' ' . FSText::_('Chi tiết'), 'fa-edit', 1, 'col-md-12', 1);
TemplateHelper::dt_edit_text(FSText::_('Title'), 'name', @$data->name, '', 255,1,0,'','','col-md-3', 'col-md-9', $disabled = 0);
TemplateHelper::dt_edit_text(FSText::_('Alias'), 'alias', @$data->alias, '', 60, 1, 0, FSText::_("Can auto generate"));
TemplateHelper::dt_edit_text(FSText::_('Nơi làm việc'), 'address', @$data->address, '', 255,1,0, $comment = 'Địa chỉ chi tiết','','col-md-3', 'col-md-9', $disabled = 0);
// TemplateHelper::dt_edit_selectbox(FSText::_("Tỉnh thành"), 'province_id', @$data->province_id, $default = '', $dataCity, $field_value = 'id', $field_label = 'name', $size = 1, $multi = 0, $add_fisrt_option = 0, $comment = '');
// TemplateHelper::dt_edit_text(FSText::_('Vị trí làm việc'), 'position', @$data->position, '', 255,1,0,'','','col-md-3', 'col-md-9', $disabled = 0);
// TemplateHelper::dt_edit_text(FSText::_('Ordering'), 'ordering', @$data->ordering, @$maxOrdering, '20');
// TemplateHelper::dt_edit_text(FSText::_('Bằng cấp'), 'degree', @$data->degree, '', 100, 1, 0, '', '', 'col-md-3', 'col-md-9', $disabled = 0);
// TemplateHelper::dt_edit_text(FSText::_('Kinh nghiệm'), 'experience', @$data->experience, '', 100, 1, 0, '', '', 'col-md-3', 'col-md-9', $disabled = 0);
// TemplateHelper::dt_edit_text(FSText::_('Vị trí ứng tuyển'), 'position', $this->model->get_record_by_id(@$data->position, 'fs_recruitment_position')->name, '', 100, 1, 0, '', '', 'col-md-3', 'col-md-9', $disabled = 0);
// TemplateHelper::dt_edit_text(FSText::_('Mức lương mong muốn'), 'salary', @$data->salary, '', 100, 1, 0, '', '', 'col-md-3', 'col-md-9', $disabled = 0);
TemplateHelper::dt_edit_selectbox(FSText::_('Loại công việc'), 'working_day', @$data->working_day, '', array(1 => 'Hợp đồng', 2 => 'Toàn thời gian', 3 => 'Thực tập'), 1, 0, '', '', 'col-md-3', $comment = '', $disabled = 0);
// TemplateHelper::dt_edit_text(FSText::_('Kinh nghiệm làm việc'), 'experience', @$data->experience, '', 100, 1, 0, '', '', 'col-md-3', 'col-md-9', $disabled = 0);
// TemplateHelper::datetimepicke(FSText::_('Hạn nộp hồ sơ'), 'published_date', @$data->published_date ? @$data->published_date : date('Y-m-d H:i:s'), FSText::_('Bạn vui lòng chọn thời gian nộp hồ sơ'), 20, '', 'col-md-3', 'col-md-4');
TemplateHelper::dt_edit_selectbox(FSText::_('Danh mục vị trí'), 'list_position', @$data->list_position , 0, $list_position, $field_value = 'id', $field_label = 'treename', $size = 10, 0, 1, '', '', '', 'col-md-3', 'col-md-9');
// TemplateHelper::dt_edit_selectbox(FSText::_('Categories'), 'category_id', @$data->category_id, 0, $categories, $field_value = 'id', $field_label = 'treename', $size = 10, 0, 1);

// TemplateHelper::dt_edit_text(FSText::_('Trình độ chuyên môn'), 'qualification', @$data->qualification, '', 100, 1, 0, '', '', 'col-md-3', 'col-md-9', $disabled = 0);
// TemplateHelper::dt_edit_text(FSText::_('Mô tả công việc'), 'summary', @$data->summary, '', 650, 450, 1, '', '', 'col-sm-2', 'col-sm-12');
TemplateHelper::dt_edit_text(FSText::_('Mô tả'), 'description', @$data->description, '', 650, 450, 1, '', '', 'col-sm-2', 'col-sm-12');


$this->dt_form_end(@$data, 1, 0, 2, 'Cấu hình seo', '', 1, 'col-sm-4'); // END: col-1


?>

<script type="text/javascript">
    $('.form-horizontal').keypress(function(e) {
        if (e.which == 13) {
            formValidator();
            return false;
        }
    });

    function formValidator() {
        // $('.alert-danger').show();
        return true;
    }
</script>