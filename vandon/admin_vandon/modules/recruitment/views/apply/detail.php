<link type="text/css" rel="stylesheet" media="all" href="../libraries/jquery/jquery.ui/jquery-ui.css" />
<script type="text/javascript" src="../libraries/jquery/jquery.ui/jquery-ui.js"></script>
<!-- HEAD -->
<?php

$title = @$data ? FSText::_('Edit') : FSText::_('Add');
global $toolbar;
$toolbar->setTitle($title);
// $toolbar->addButton('apply', FSText::_('Apply'), '', 'apply.png', 1);
// $toolbar->addButton('Save', FSText::_('Save'), '', 'save.png', 1);
$toolbar->addButton('cancel', FSText::_('Cancel'), '', 'cancel.png');

echo ' 	<div class="alert alert-danger" style="display:none" >
                    <span id="msg_error"></span>
            </div>';

$this->dt_form_begin(1, 4, $title . ' ' . FSText::_('Chi tiết'), 'fa-edit', 1, 'col-md-12', 1);
TemplateHelper::dt_edit_text(FSText::_('Họ và tên'), 'name', @$data->name, '', 255, 1, 0, '', '', 'col-md-3', 'col-md-9', $disabled = 1);
// TemplateHelper::dt_edit_text(FSText::_('Alias'), 'alias', @$data->alias, '', 60, 1, 0, FSText::_("Can auto generate"));
TemplateHelper::dt_edit_text(FSText::_('Số điện thoại'), 'phone', @$data->phone, '', 255, 1, 0, $comment = '', '', 'col-md-3', 'col-md-9', $disabled = 1);
TemplateHelper::dt_edit_text(FSText::_('Email'), 'email', @$data->email, '', 255, 1, 0, $comment = '', '', 'col-md-3', 'col-md-9', $disabled = 1);
TemplateHelper::dt_edit_text(FSText::_('Thư xin việc'), 'content', @$data->content, '', 100, 5, 0, '', '', 'col-sm-3', 'col-sm-9');


$this->dt_form_end(@$data, 1, 0, 2, 'Cấu hình seo', '', 1, 'col-sm-4'); // END: col-1


?>
<div class="row">
    <div class="col-md-3">
        <p style="font-weight: 700; text-align: right"><?php echo FSText::_("Hồ sơ tải lên") ?></p>
    </div>
    <div class="col-md-9">
        <a download href="<?php echo URL_ROOT .  $data->file ?>"><?php echo $data->file ?></a>
    </div>
</div>


<div class="row">
    <div class="col-md-3">
        <p style="font-weight: 700; text-align: right"><?php echo FSText::_("Ứng tuyển vị trí") ?></p>
    </div>
    <div class="col-md-9">
        <a href="<?php echo FSRoute::_("index.php?module=recruitment&view=recruitment&code=$data->alias&id=$data->job_id") ?>"><?php echo $this->model->get_record_by_id($data->job_id, FSTable::_('fs_recruitments', 1))->name ?></a>
    </div>
</div>

<script type="text/javascript">
    $('.form-horizontal').keypress(function(e) {
        if (e.which == 13) {
            formValidator();
            return false;
        }
    });

    function formValidator() {
        // $('.alert-danger').show();
        return true;
    }
</script>