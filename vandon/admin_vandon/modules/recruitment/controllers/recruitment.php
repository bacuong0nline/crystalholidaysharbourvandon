<?php
class RecruitmentControllersRecruitment extends ControllersCategories
{
	function __construct()
	{
		$this->view = 'categories';
		parent::__construct();
	}

	function display()
	{
		parent::display();
		$sort_field = $this->sort_field;
		$sort_direct = $this->sort_direct;
		$list = $this->model->get_data('');


	}
	function add() {
		$model =  $this->model;
		$dataCity = $model->get_city();
		$list_position = $model->get_list_position();
		include 'modules/' . $this->module . '/views/' . $this->view . '/detail.php';
	}
	function edit()
	{
		$model =  $this->model;
		$ids = FSInput::get('id', array(), 'array');
		$id = $ids[0];
		$data = $model->get_record_by_id($id);
		$list_position = $model->get_list_position();

		$dataCity = $model->get_city();
		$categories = $model->get_categories_tree();

		include 'modules/' . $this->module . '/views/' . $this->view . '/detail.php';
	}

}
