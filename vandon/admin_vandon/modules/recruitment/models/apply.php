<?php
class RecruitmentModelsApply extends FSModels
{
	function __construct()
	{

		$this->table_name = FSTable_ad::_('fs_recruitment_apply');
		// $this->table_name_position = FSTable_ad::_('fs_recruitment_position');

		$this->check_alias = 1;
		$this->call_update_sitemap = 1;
		// exception: key (field need change) => name ( key change follow this field)
		$this->field_except_when_duplicate = array(array('list_parents', 'id'), array('alias_wrapper', 'alias'));
		// config for save
		$cyear = date('Y');
		$cmonth = date('m');
		//$cday = date('d');
		$this->img_folder = 'images/recruitments/cat/' . $cyear . '/' . $cmonth;
		$this->field_img = 'image';
		parent::__construct();
		$this->limit = 100;

		// $this -> array_synchronize = array($this -> table_items=>array('id'=>'category_id','alias'=>'category_alias','name'=>'category_name'
		//                                                                            ,'published'=>'published_cate','alias_wrapper'=>'category_alias_wrapper'));
	}

	function get_city()
	{
		global $db;
		$query = "SELECT a.*
                      FROM fs_cities
                      AS a 
                      ORDER BY ordering asc ";
		$sql = $db->query($query);
		$list = $db->getObjectList();
		return $list;
	}
	function get_recruitment($product_related)
	{
		if (!$product_related)
			return;
		$query   = ' SELECT id, name,phone, address 
    					FROM ' . $this->table_name . '';
		global $db;
		$sql = $db->query($query);
		$result = $db->getObjectList();
		return $result;
	}
	// function get_list_position()
	// {
	// 	$query   = ' SELECT id, name
	// 	FROM ' . $this->table_name_position . '';
	// 	global $db;
	// 	$sql = $db->query($query);
	// 	$result = $db->getObjectList();
	// 	return $result;
	// }
}
