<?php
class RecruitmentModelsSalary extends FSModels
{
	function __construct()
	{

		$this->table_name = FSTable_ad::_('fs_salary');
		$this->check_alias = 1;
		$this->call_update_sitemap = 1;
		// config for save
		$cyear = date('Y');
		$cmonth = date('m');
		//$cday = date('d');
		parent::__construct();
		$this->limit = 100;
	}

	function save($row = array(), $use_mysql_real_escape_string = 1) {
		$row['name'] = FSInput::get('name');
		// $row['created_time'] = date('Y-m-d H:i:s');
		$row['published'] = FSInput::get('published');


		$rs = parent::save($row, 1);
		return $rs;
	}

}
