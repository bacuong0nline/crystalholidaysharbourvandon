<?php 
	class EducationModelsCategories extends ModelsCategories
	{
		function __construct()
		{
			
			$this -> table_items = FSTable_ad::_('fs_education', 1);
			$this -> table_name = FSTable_ad::_('fs_education_categories', 1);
			$this -> check_alias = 1;
			$this -> call_update_sitemap = 1;
			$this -> table_products = FSTable_ad::_('fs_products');
			// exception: key (field need change) => name ( key change follow this field)
			$this -> field_except_when_duplicate = array(array('list_parents','id'),array('alias_wrapper','alias'));
			// config for save
			$cyear = date('Y');
			$cmonth = date('m');
			//$cday = date('d');
			$this -> arr_img_paths = array(
				array('lange',632,423,'cut_image'),
				array('resized',0,0,'resize_image'),
				array('small',112,75,'cut_image')
			);
			$this -> img_folder = 'images/education/cat/'.$cyear.'/'.$cmonth;
			$this -> field_img = 'image';
			parent::__construct();
			$this -> limit = 100;
            
           // $this -> array_synchronize = array($this -> table_items=>array('id'=>'category_id','alias'=>'category_alias','name'=>'category_name'
//                                                                            ,'published'=>'published_cate','alias_wrapper'=>'category_alias_wrapper'));
		}
        
        function get_products_related($product_related){
    		if(!$product_related)
    				return;
    		$query   = ' SELECT id, name,image 
    					FROM '.$this -> table_products.'
    					WHERE id IN (0'.$product_related.'0) 
    					 ORDER BY POSITION(","+id+"," IN "0'.$product_related.'0")
    					';
    		global $db;
    		$sql = $db->query($query);
    		$result = $db->getObjectList();
    		return $result;
    	}

		function save($row = array(), $e = 1) {
			$id = FSInput::get('id','','int');
			if (!isset($row['hover'])) {
				$image = $_FILES['hover']["name"];
				if ($image) {
					$this->remove_old_image($id, 'hover');
	
					$image = $this->upload_image('hover', '_' . time(), 10000000);
					// print_r($image);die;
					$row['hover'] = 	$image;
					if (!isset($row['hover'])) {
						// tính chiều rộng để thêm vào admin
						list($root_width, $root_height) = getimagesize(URL_ROOT . $image);
						$arr_img_paths = $this->arr_img_paths;
						$get_height = $arr_img_paths[0][2];
						$new_width  = ceil($root_width * $get_height / $root_height);
					}
				}
			}
			$id = parent::save ( $row );
			return $id;
		}
	}
	
?>