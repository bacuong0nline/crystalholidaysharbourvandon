<?php 
	class ContentsModelsNew extends FSModels
	{
		var $limit;
		var $prefix ;
		function __construct()
		{
			$limit = FSInput::get('limit',20,'int');
			$this -> limit = $limit;
			$this -> view = 'contents';
			
			$this -> table_category_name = FSTable_ad::_('fs_contents_categories');
			$this -> arr_img_paths = array(
                                            array('resized',467,547,'resize_image'),
//                                            array('small',282,170,'cut_image')
                                        );
			$this -> table_name = FSTable_ad::_('fs_contents_new');
			
			// config for save
			$cyear = date('Y');
			$cmonth = date('m');
			$cday = date('d');
			$this -> img_folder = 'images/contents/'.$cyear.'/'.$cmonth.'/'.$cday;
			$this -> check_alias = 0;
			$this -> field_img = 'image1';
			
			parent::__construct();
		}
		
		function setQuery(){
			
			// ordering
			$ordering = "";
			$where = "  ";
			if(isset($_SESSION[$this -> prefix.'sort_field']))
			{
				$sort_field = $_SESSION[$this -> prefix.'sort_field'];
				$sort_direct = $_SESSION[$this -> prefix.'sort_direct'];
				$sort_direct = $sort_direct?$sort_direct:'asc';
				$ordering = '';
				if($sort_field)
					$ordering .= " ORDER BY $sort_field $sort_direct, created_time DESC, id DESC ";
			}
			
			// filter category
			if(isset($_SESSION[$this -> prefix.'filter0'])){
				$filter = $_SESSION[$this -> prefix.'filter0'];
				if($filter){
					$where .= ' AND a.category_id_wrapper like  "%,'.$filter.',%" ';
				}
			}	
			
			if(!$ordering)
				$ordering .= " ORDER BY created_time DESC , id DESC ";
			
			
			if(isset($_SESSION[$this -> prefix.'keysearch'] ))
			{
				if($_SESSION[$this -> prefix.'keysearch'] )
				{
					$keysearch = $_SESSION[$this -> prefix.'keysearch'];
					$where .= " AND a.title LIKE '%".$keysearch."%' ";
				}
			}
			
			$query = " SELECT a.*
						  FROM 
						  	".$this -> table_name." AS a
						  	WHERE 1=1 ".
						 $where.
						 $ordering. " ";
			return $query;
		}
		
		function save($row = array(), $use_mysql_real_escape_string = 1){
			$title = FSInput::get('title');
			// $show_in_homepage = FSInput::get('show_in_homepage');
			if(!$title)
				return false;
			$id = FSInput::get('id',0,'int');	
			$category_id = FSInput::get('category_id','int',0);
			if(!$category_id){
				Errors::_('Bạn phải chọn danh mục');
				return;
			}
            
            $user_id = isset($_SESSION['ad_userid'])? $_SESSION['ad_userid']:'';
            if(!$user_id)
                return false;
                
            $user = $this->get_record_by_id($user_id,'fs_users','username');
            if($id){
                $row['author_last'] = $user->username;
            }else{
                $row['author'] = $user->username;
            }
			
			$cat =  $this->get_record_by_id($category_id,'fs_contents_categories');
			$row['category_id_wrapper'] = $cat -> list_parents;
			$row['category_alias_wrapper'] = $cat -> alias_wrapper;
			$row['category_name'] = $cat -> name;
			$row['category_alias'] = $cat -> alias;
			$row['category_published'] = $cat -> published;
			
			$row['content'] = htmlspecialchars_decode(FSInput::get('content'));

			$image1 = $_FILES["image1"]["name"];
			if($image1){
				$this -> remove_old_image($id,'image1');
				$image = $this -> upload_image('image1','_'.time(),10000000);
				if($image){
					$row['image1'] = $image;	
				}
			}
			$image2 = $_FILES["image2"]["name"];
			if($image2){
				$this -> remove_old_image($id,'image2');
				$image = $this -> upload_image('image2','_'.time(),10000000);
				if($image){
					$row['image2'] = $image;	
				}
			}
			$image3 = $_FILES["image3"]["name"];
			if($image3){
				$this -> remove_old_image($id,'image3');
				$image = $this -> upload_image('image3','_'.time(),10000000);
				if($image){
					$row['image3'] = $image;	
				}
			}

			$image4 = $_FILES["image4"]["name"];
			if($image4){
				$this -> remove_old_image($id,'image4');
				$image = $this -> upload_image('image4','_'.time(),10000000);
				if($image){
					$row['image4'] = $image;	
				}
			}


			$image5 = $_FILES["image5"]["name"];
			if($image5){
				$this -> remove_old_image($id,'image5');
				$image = $this -> upload_image('image5','_'.time(),10000000);
				if($image){
					$row['image5'] = $image;	
				}
			}

			$image6 = $_FILES["image6"]["name"];
			if($image6){
				$this -> remove_old_image($id,'image6');
				$image = $this -> upload_image('image6','_'.time(),10000000);
				if($image){
					$row['image6'] = $image;	
				}
			}
			$image7 = $_FILES["image7"]["name"];
			if($image7){
				$this -> remove_old_image($id,'image7');
				$image = $this -> upload_image('image7','_'.time(),10000000);
				if($image){
					$row['image7'] = $image;	
				}
			}

			// print_r($row);die;
			return parent::save($row);
		}

		/*
		 * select in category of home
		 */
		function get_categories_tree()
		{
			global $db;
			$query = " SELECT a.*
						  FROM 
						  	".$this -> table_category_name." AS a
						  	ORDER BY ordering ";
			$sql = $db->query($query);
			$result = $db->getObjectList();
			$tree  = FSFactory::getClass('tree','tree/');
			$list = $tree -> indentRows2($result);
			return $list;
		}
		
		/*
	     * Save all record for list form
	     */
	    function save_all(){
	        $total = FSInput::get('total',0,'int');
	        if(!$total)
	           return true;
	        $field_change = FSInput::get('field_change');
	        if(!$field_change)
	           return false;
	        $field_change_arr = explode(',',$field_change);
	        $total_field_change = count($field_change_arr);
	        $record_change_success = 0;
	        for($i = 0; $i < $total; $i ++){
//	        	$str_update = '';
	        	$row = array();
	        	$update = 0;
	        	foreach($field_change_arr as $field_item){
	        	      $field_value_original = FSInput::get($field_item.'_'.$i.'_original')	;
	        	      $field_value_new = FSInput::get($field_item.'_'.$i)	;
	        		  if(is_array($field_value_new)){
        	      		$field_value_new = count($field_value_new)?','.implode(',',$field_value_new).',':'';
	        	      }
	        	      
	        	      if($field_value_original != $field_value_new){
	        	          $update =1;
	        	       		// category
	        	          if($field_item == 'category_id'){
	        	          		$cat =  $this->get_record_by_id($field_value_new,'fs_contents_categories');
								$row['category_id_wrapper'] = $cat -> list_parents;
								$row['category_alias_wrapper'] = $cat -> alias_wrapper;
								$row['category_name'] = $cat -> name;
								$row['category_alias'] = $cat -> alias;
								$row['category_published'] = $cat -> published;
								$row['category_id'] = $field_value_new;
	        	          }else{
								$row[$field_item] = $field_value_new;
	        	          }
	        	      }    
	        	}
	        	if($update){
	        		$id = FSInput::get('id_'.$i, 0, 'int'); 
	        		$str_update = '';
	        		global $db;
	        		$j = 0;
	        		foreach($row as $key => $value){
	        			if($j > 0)
	        				$str_update .= ',';
	        			$str_update .= "`".$key."` = '".$value."'";
	        			$j++;
	        		}
            
		            $sql = ' UPDATE  '.$this ->  table_name . ' SET ';
		            $sql .=  $str_update;
		            $sql .=  ' WHERE id =    '.$id.' ';
		            $db->query($sql);
		            $rows = $db->affected_rows();
		            if(!$rows)
		                return false;
		            $record_change_success ++;
	        	}
	        }
	        return $record_change_success;  
	           
	        
	    }
	}
	
?>