<?php
$title = @$data ? FSText::_('Edit') : FSText::_('Add');
global $toolbar;
$toolbar->setTitle($title);
$toolbar->addButton('save_add', FSText::_('Save and new'), '', '', 1);
$toolbar->addButton('apply', FSText::_('Apply'), '', 'apply.png', 1);
$toolbar->addButton('Save', FSText::_('Save'), '', 'save.png', 1);
$toolbar->addButton('back', FSText::_('Cancel'), '', 'cancel.png');

echo ' 	<div class="alert alert-danger" style="display:none" >
                    <span id="msg_error"></span>
            </div>';

$this->dt_form_begin(1, 4, $title . ' ' . FSText::_('Contents'), 'fa-edit', 1, 'col-md-12', 1);
TemplateHelper::dt_edit_text(FSText::_('Alias'), 'alias', @$data->alias, '', 60, 1, 0, FSText::_("Can auto generate"));
TemplateHelper::dt_edit_selectbox(FSText::_('Categories'), 'category_id', @$data->category_id, 1, $categories, $field_value = 'id', $field_label = 'treename', $size = 1, 0, 1);
TemplateHelper::dt_edit_text(FSText::_('Ordering'), 'ordering', @$data->ordering, @$maxOrdering, '20');
TemplateHelper::dt_checkbox(FSText::_('Published'), 'published', @$data->published, 1, '', '', '', 'col-sm-3', 'col-sm-9');
$this->dt_form_begin(1, 2, FSText::_('Block 1'), 'fa-star', 1, 'col-md-12');

?>
<div class="d-flex">
    <?php
    TemplateHelper::dt_edit_text(FSText::_('Chủ đề 1'), 'title', @$data->title);
    ?>
    <div class="d-flex"></div>
    <?php
    TemplateHelper::dt_edit_image('', 'image1', URL_ROOT . str_replace('/original/', '/resized/', @$data->image1));
    TemplateHelper::dt_edit_image('', 'image2', URL_ROOT . str_replace('/original/', '/resized/', @$data->image2));
    TemplateHelper::dt_edit_image('', 'image3', URL_ROOT . str_replace('/original/', '/resized/', @$data->image3));
    ?>
    </div>
    <div style="width: 100%"></div>
    <?php
    ?>
</div>
<?php
TemplateHelper::dt_edit_text(FSText::_(''), 'summary1', @$data->summary1, '', 650, 450, 1, '', '', '', 'col-sm-12');
$this->dt_form_end_col();


$this->dt_form_begin(1, 2, FSText::_('Block 2'), 'fa-star', 1, 'col-md-12');
TemplateHelper::dt_edit_text(FSText::_('Chủ đề 2'), 'title2', @$data->title2);
TemplateHelper::dt_edit_image('', 'image4', URL_ROOT . str_replace('/original/', '/resized/', @$data->image4));
TemplateHelper::dt_edit_text(FSText::_(''), 'summary2', @$data->summary2, '', 650, 450, 1, '', '', '', 'col-sm-12');
$this->dt_form_end_col();

$this->dt_form_begin(1, 2, FSText::_('Block 3'), 'fa-star', 1, 'col-md-12');

?>
<div class="d-flex">
    <?php
    TemplateHelper::dt_edit_text(FSText::_('Chủ đề 3'), 'title3', @$data->title3);
    ?>
    <div class="d-flex"></div>
    <?php
    TemplateHelper::dt_edit_image('', 'image5', URL_ROOT . str_replace('/original/', '/resized/', @$data->image5));
    TemplateHelper::dt_edit_image('', 'image6', URL_ROOT . str_replace('/original/', '/resized/', @$data->image6));
    TemplateHelper::dt_edit_image('', 'image7', URL_ROOT . str_replace('/original/', '/resized/', @$data->image7));
    ?>
</div>
<?php
TemplateHelper::dt_edit_text(FSText::_(''), 'summary3', @$data->summary3, '', 650, 450, 1, '', '', '', 'col-sm-12');
$this->dt_form_end_col();

$this->dt_form_end_col();



$this->dt_form_end(@$data, 0, 0, 2, 'Cấu hình seo', '', 1, 'col-sm-12');

?>
<style>
    .d-flex {
        display: flex;
        flex-direction: row;
    }
</style>
<script type="text/javascript">
    $('.form-horizontal').keypress(function(e) {
        if (e.which == 13) {
            formValidator();
            return false;
        }
    });

    function formValidator() {
        $('.alert-danger').show();

        if (!notEmpty('title', 'Bạn phải nhập tiêu đề'))
            return false;

        $('.alert-danger').hide();
        return true;
    }
</script>