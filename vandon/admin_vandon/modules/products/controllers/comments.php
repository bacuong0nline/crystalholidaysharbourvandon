<?php
	class ProductsControllersComments extends Controllers{
	
		function __construct()
		{
			$this->view = 'comments' ; 
			parent::__construct(); 
			$array_type_comment = array( 0 => 'Đã trả lời',1 => 'Chưa trả lời');
			$this -> arr_type_comment = $array_type_comment;
		}
		function display()
		{
			parent::display();
			$sort_field = $this -> sort_field;
			$sort_direct = $this -> sort_direct;
			
			$model  = $this -> model;
			$list_comments =   $model->get_data_by_comments();
			
			// print_r($list_comments);die;
			$str_key = '';

			$arr_comments_by_key_record_id  = array();
			foreach ($list_comments as $item) {
			  $id = $item->record_id;
			  if (isset($arr_comments_by_key_record_id[$id])) {
			     $arr_comments_by_key_record_id[$id][] = $item;
			  } else {
			     $arr_comments_by_key_record_id[$id] = array($item);
			  }
			}

			$str_key =  implode(", ", array_keys($arr_comments_by_key_record_id));



			$arr_comments_by_key_parent_id  = array();
			foreach ($list_comments as $item) {
			  $id = $item->parent_id;
			  if (isset($arr_comments_by_key_parent_id[$id])) {
			     $arr_comments_by_key_parent_id[$id][] = $item;
			  } else {
			     $arr_comments_by_key_parent_id[$id] = array($item);
			  }
			}
			$str_key_parent_id = '';
			$str_key_parent_id =  implode(", ", array_keys($arr_comments_by_key_parent_id));

			$list = $model->get_data($str_key);
		
		
			$array_type_comment = $this -> arr_type_comment;
			$array_obj_type_comment = array();
			foreach($array_type_comment as $key => $name){
				$array_obj_type_comment[] = (object)array('id'=>($key+1),'name'=>$name);
			}
			

			$categories = $model->get_categories_tree_all();
			$total = $model->getTotal($str_key);	
			$pagination = $model->getPagination($str_key );
			include 'modules/'.$this->module.'/views/'.$this->view.'/list.php';
		}
		function add()
		{
			return;
		}
		
		/*
		 * Group user > 3 => Have permission
		 */
		function check_comments(){
			$user_group = $_SESSION['cms_group'];
			$link = 'index.php';
			if($user_group < 3){
				setRedirect($link,FSText :: _('Không có quyền truy cập vào link này'),'error');
				return false;
			}
				
			return true;
		}
		
		
		function ajax_get_comments_by_product()
		{
			$record_id = FSInput::get('record_id',0,'int');
			if(!$record_id){
				echo 'record_id = null';
				return;
			}
			$str_key_parent_id = FSInput::get('str_key_parent_id');
			$model = $this -> model;
			$data = $model->get_comments_by_product($record_id,$str_key_parent_id);
			$html = $this -> genarate_comments($data);
			echo $html;
			if(count($data)){
				$model->update_unread_for_comments($record_id);	
			}
			return;
		}
		
		function genarate_comments($data){
			$model = $this -> model;
			
			$arr_level = $model -> get_level();
			$all_user = $model -> get_all_user();
		

			$html = '';
			$html .= '<div class="comments">';
			foreach ($data as $item){
				$rating = $item->rating * 20;
				@$user_commemt = $all_user[$item -> user_id];
				@$user_level = $arr_level[$user_commemt->level];
				$html .= '<div class="comment-item comment-item-'.$item -> id.' '.($item -> parent_id? "comment-child":"") .'">';	
					$html .= '<div class="comment_info">';
						$html .= '<div class="comment_head">';
						$html .= '<span class="name" >'. $item -> name.'</span> ';	
						$html .= '<span class="email" >('. $item -> email.')</span>';
						if(@$user_level->level == 1 || $item -> is_admin == 1){
							$html .= '<span class="level">Quản trị viên</span>';
						}
						$html .= '</div>';	
						$html .= '<div><span class="comment" id="comment_content_'.$item->id.'"  onclick="javascript: open_form_edit('.$item -> id.')">'. $item -> comment.'</span></div>';	
						
						//edit
						$html .= '<div class="edit_area hide" id="edit_area_'.$item->id.'">	';	
						$html .= '<div class="text_area_ct">';		
						$html .= '<textarea id="text_edit_'.$item -> id.'" cols="142" rows="4" name="text" >'.$item -> comment.'</textarea>';
						$html .= '</div>';
						$html .= '	<div class="reply_button_area">	';
						$html .= '		<a class="button" href="javascript: void(0);" onclick="javascript: submit_edit('.$item -> id.','.$item -> record_id.')"';
						$html .= '			<span>Gửi</span>	';
						$html .= '		</a>&nbsp;&nbsp;	';
						$html .= '		<a class="button_edit_close" href="javascript: void(0)" onclick="javascript: close_form_edit('.$item -> id.')" >';
						$html .= '			<span>Đóng lại</span>	';
						$html .= '		</a>&nbsp;&nbsp;	';
						$html .= '		<div class="clear"></div>	';
						$html .= '	</div>	';
						$html .= '</div>';
						//end edit
						
						$html .= '<div class="actions"><span class="datetime" >'. date('d/m/Y H:i',strtotime($item -> created_time)).'</span><span id="_rating" ><span style="width:'.$rating.'%">&nbsp;</span></span>';	
						$html .= '<a class="button bt_reply" id="bt_reply_'.$item -> id.'" href="javascript: void(0);" onclick="javascript: call_form_reply('.$item -> id.'); ">Trả lời</a>';
						//$html .= '<span class="q_a">Q&A:&nbsp;&nbsp;';
//							if($item -> is_q_a){
//								$html .= '<a href="javascript:void(0);" onclick="return ajax_un_question_answer('.$item -> id.','.$item -> record_id .')" title="Hỏi đáp">';
//									$html .= '<img border="0" src="templates/default/images/qa.png" alt="Question answer">';
//								$html .= '</a>';
//							}else{
//								$html .= '<a href="javascript:void(0);" onclick="return ajax_question_answer('.$item -> id.','.$item -> record_id .')" title="Hỏi đáp">';
//									$html .= '<img border="0" src="templates/default/images/un_qa.png" alt="Disable question answer">';
//								$html .= '</a>';
//							}
						$html .= '</span>';
						$html .= '<span class="status">Duyệt:';
						if($item -> published){
							$html .= '<a href="javascript:void(0);" onclick="return ajax_unpublished('.$item -> id.','.$item -> record_id .')" title="Click vào để ngừng xuất bản">';
							$html .= '<img border="0" src="templates/default/images/published.png" alt="Enabled status"></a>';
						}else{
							$html .= '<a href="javascript:void(0);" onclick="return ajax_published('.$item -> id.','.$item -> record_id .')" title="Click vào để xuất bản">';
							$html .= '<img border="0" src="templates/default/images/unpublished.png" alt="Disable status"></a>';
						}
						$html .= '</span><span class="remove_button">Xóa:';
						$html .= '&nbsp;&nbsp;<a href="javascript:void(0);" onclick="return ajax_del('.$item -> id.','.$item -> record_id.','.$item -> published .')" title="Xóa">';
						$html .= '<img border="0" src="templates/default/images/toolbar/remove_2.png" alt="Remove" ></a></span></div>';
					$html .= '</div>';
					$html .= '<div class="reply_area hide" id="reply_area_'.$item->id.'">	';	
					$html .= '<div class="text_area_ct">';		
					$html .= '<input type="text" id="name_'.$item -> id.'" onfocus="if(this.value==\'Họ tên\') this.value=\'\'" onblur="if(this.value==\'\') this.value=\'Họ tên\'" value="'.$_SESSION['ad_username'].'" size="63" /><br/>';
					$html .= '<textarea id="text_'.$item -> id.'" cols="72" rows="4" name="text" onfocus="if(this.value==\'Nội dung\') this.value=\'\'" onblur="if(this.value==\'\') this.value=\'Nội dung\'">Nội dung</textarea>';
					$html .= '</div>';
					$html .= '	<div class="reply_button_area">	';
					$html .= '		<a class="button" href="javascript: void(0);" onclick="javascript: submit_reply('.$item -> id.','.$item -> record_id.')"';
					$html .= '			<span>Gửi</span>	';
					$html .= '		</a>	';
					$html .= '		<a class="button_reply_close" href="javascript: void(0)" onclick="javascript: close_form_reply('.$item -> id.')" >';
					$html .= '			<span>Đóng lại</span>	';
					$html .= '		</a>	';
					$html .= '		<div class="clear"></div>	';
					$html .= '	</div>	';
					$html .= '</div>	';
				$html .= '</div>';				
			}
			$html .= '</div>';
			return $html;
		}
		function ajax_question_answer(){
			$model = $this -> model;
			$rs = $model->ajax_question_answer(1);
			return;
		}
		function ajax_un_question_answer(){
			$model = $this -> model;
			$rs = $model->ajax_question_answer(0);
			return;
		}
		function ajax_published(){
			$model = $this -> model;
			$rs = $model->ajax_published(1);
			return;
		}
		function ajax_unpublished(){
			$model = $this -> model;
			$rs = $model->ajax_published(0);
			return;
		}
		function ajax_del(){
			$model = $this -> model;
			$rs = $model->ajax_del();
			return;
		}
		
		function ajax_save_comment(){
			$model = $this -> model;
			$model -> save_comment();
			return;
		}
		function ajax_edit_comment(){
			$model = $this -> model;
			$model -> edit_comment();
			return;
		}
		
//		function edit()
//		{
//			$ids = FSInput::get('id',array(),'array');
//			$id = $ids[0];
//			$model = $this -> model;
////			$categories  = $model->get_categories_tree();
//			$categories = $model->get_categories_tree_all();
////			$tags_categories = $model->get_tags_categories();
//			$data = $model->get_product_by_id($id);
//			if(!$data){
//				echo "<br/>Không tồn tại hoặc không có quyền truy cập";
//				return;
//			}
//			$arr_status = $this ->  arr_status;
//			$group_id = $_SESSION['cms_group'];
//			$members = $model -> get_records('published = 1','fs_members');
//			// owner
//			$this -> create_status_for_user( $group_id,0,$data -> status);
//			include 'modules/'.$this->module.'/views/'.$this->view.'/detail.php';	
//		}
		
		/*
		 * Tạo ra mảng status cho người dùng
		 */
		function create_status_for_user($group_id, $is_owner,$status_current){
//			$this -> arr_status = array(1=>'Lưu nháp',2=>'BTV từ chối',3=>'Chờ BTV duyệt',4=>'Đã biên tập',5=>'Hạ bài (kéo về)',6=>'Xuất bản');
			switch($group_id){
				case '1':
					if($status_current == 2){
						$this -> arr_status_edit = array(1=>'Lưu nháp',2=>'BTV từ chối',3=>'Chờ BTV duyệt');
						return;
					}else{
						$this -> arr_status_edit = array(1=>'Lưu nháp',3=>'Chờ BTV duyệt');
						return;
					}
				case '2':
					if($is_owner){
						if($status_current == 5){
							$this -> arr_status_edit = array(1=>'Lưu nháp',2=>'BTV từ chối',3=>'Chờ BTV duyệt',4=>'Đã biên tập',5=>'Hạ bài (kéo về)');
							return;
						}else{
							$this -> arr_status_edit = array(1=>'Lưu nháp',2=>'BTV từ chối',3=>'Chờ BTV duyệt',4=>'Đã biên tập');
							return;
						}
					}else{
						if($status_current == 5){
							$this -> arr_status_edit = array(2=>'BTV từ chối',3=>'Chờ BTV duyệt',4=>'Đã biên tập',5=>'Hạ bài (kéo về)');
							return;
						}else{
							$this -> arr_status_edit = array(2=>'BTV từ chối',3=>'Chờ BTV duyệt',4=>'Đã biên tập');
							return;
						}
					}
				case '3':
				case '4':
					if($is_owner){
						$this -> arr_status_edit = array(1=>'Lưu nháp',2=>'BTV từ chối',3=>'Chờ BTV duyệt',4=>'Đã biên tập',5=>'Hạ bài (kéo về)',6=>'Xuất bản');
						return;
					}else{
						$this -> arr_status_edit = array(2=>'BTV từ chối',3=>'Chờ BTV duyệt',4=>'Đã biên tập',5=>'Hạ bài (kéo về)',6=>'Xuất bản');
						return;
					}
			}
		}
		
		function view_comment($product){
			$link = 'index.php?module=products&view=comments&keysearch=&text_count=1&text0='.$news ->id.'&filter_count=1&filter0=0';
			$html = '';
			if($product -> comments_unread){
				$html .= '<strong>'.$product -> comments_unread.'/'.$product -> comments_total.'</strong>';
			}else{
				$html .= $product -> comments_unread.'/'.$product -> comments_total;
			}
			$html .=  '<br/><a href="'.$link.'" ><img border="0" src="templates/default/images/comment.png" alt="Comment"></a>';
			return $html; 
		}
		
		function view_status($status){
			$arr_status = $this -> arr_status;
			return $arr_status[$status]; 
		}
		
	
	}
?>
