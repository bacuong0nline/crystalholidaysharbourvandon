<?php
	// models 
//	include 'modules/'.$module.'/models/'.$view.'.php';
		  
	class ProductsControllersPrice extends Controllers
	{
		function __construct()
		{
			$this->view = 'products' ; 
			parent::__construct(); 
		}
		function display()
		{
			parent::display();
			$sort_field = $this -> sort_field;
			$sort_direct = $this -> sort_direct;
			
			$list = $this -> model->get_data("");
			$pagination = $this -> model->getPagination("");
			include 'modules/'.$this->module.'/views/'.$this->view.'/list.php';
		}
        function do_import(){
            include 'modules/'.$this->module.'/views/'.$this -> view.'/import.php';
        }

        function import(){
            if(empty($_FILES['import']["name"])) {
                setRedirect('index.php?module=products&view=origin&task=import', FSText :: _('Bạn vui lòng chọn file excel.'));
                return false;
            }

            require(PATH_BASE.'libraries/PHPExcel/PHPExcel.php');
            $objPHPExcel = PHPExcel_IOFactory::load($_FILES['import']['tmp_name']);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $numberRow = $sheet->getHighestRow();
            $i = 0;
            for($row = 2; $row <= $numberRow; $row++){
                $id = $sheet->getCell('A'.$row)->getValue();
                $data = array(
                    'name' => $sheet->getCell('A'.$row)->getValue(),
                    'created_time' => date('Y-m-d H:i:s'),
                    'updated_time' => date('Y-m-d H:i:s'),
                    'published' => 1,
                );

                $this->table_products = FSTable_ad::_('fs_products_origin');

                $this->model->_add($data, $this->table_products);

                $i++;
            }
            setRedirect('index.php?module=products&view=origin', FSText :: _('Bạn đã cập nhật <b>'.$i.'</b> bản ghi'));
        }
	}
	
?>