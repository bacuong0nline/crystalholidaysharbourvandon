<link type="text/css" rel="stylesheet" media="all" href="../libraries/jquery/jquery.ui/jquery-ui.css" />

<style>
    #gmap {
        height: 400px;
        margin: 20px 0px;
        width: 100% !important;
    }

    .controls {
        margin-top: 16px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 15px;
        text-overflow: ellipsis;
        width: 400px;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    }

    .pac-container {
        font-family: Roboto;
    }

    #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
    }

    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

    table th,
    td {
        text-align: center;
    }

    .ui-widget-header {
        background: none;
        border: none;
    }

    .ui-state-default,
    .ui-widget-content .ui-state-default,
    .ui-widget-header .ui-state-default {
        border: none;
    }

    .ui-state-active a,
    .ui-state-active a:link,
    .ui-state-active a:visited {
        color: #C73536 !important;
    }

    .ui-state-default a,
    .ui-state-default a:link,
    .ui-state-default a:visited {
        color: #333;
    }

    body .panel a:focus,
    body .panel a:hover {
        color: #C73536 !important;
        background-color: transparent !important;
    }

    .ui-widget-content {
        border: none;
        background: white;
    }

    .ui-state-default:focus {
        outline: none !important;
    }

    .panel-info>.panel-heading {
        color: #ffffff;
        background-color: #333;
        border-color: #333;
    }

    .panel-info {
        border: none;
    }
    .form-horizontal .link-product {
        color: #337ab7 !important
    }
</style>
<script>
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<?php
$title = @$data ? FSText::_('Edit') : FSText::_('Add');

$postLevel = array(
    0 => FSText::_('Thường'),
    1 => FSText::_('Vip'),
    2 => FSText::_('Siêu Vip'),
);

global $toolbar;
$toolbar->setTitle($title);
$toolbar->addButton('save_add', FSText::_('Save and new'), '', 'save_add.png');
$toolbar->addButton('apply', FSText::_('Apply'), '', 'apply.png');
$toolbar->addButton('Save', FSText::_('Save'), '', 'save.png');
$toolbar->addButton('back', FSText::_('Cancel'), '', 'cancel.png');

if (@$data) {
    $link = FSRoute::_("index.php?module=products&view=product&ccode=ccode&code=$data->alias&id=$data->id&Itemid=10");
}

if (@$link)
$this->dt_form_begin(1, 4, '<a target="_blank" class="link-product" href="'.$link.'">'.$link.'</a>', 'fa-link', 1, 'col-md-12', 1);
else
$this->dt_form_begin(1, 4, 'Sản phẩm mới', 'fa-link', 1, 'col-md-12', 1);

$category_id = isset($data->category_id) ? $data->category_id : $cid;
?>
<div id="tabs">
    <ul>
        <li><a href="#fragment-1"><span><?php echo FSText::_("Trường cơ bản"); ?></span></a></li>
        <!-- <li><a href="#fragment-2"><span><?php echo FSText::_("Thuộc tính trong menu"); ?></span></a></li> -->
        <li><a href="#fragment-3"><span><?php echo FSText::_("SEO"); ?></span></a></li>
        <li><a href="#fragment-4"><span><?php echo FSText::_("Sp liên quan"); ?></span></a></li>
        <!-- <li><a href="#fragment-8"><span><?php //echo FSText::_("Các p/k"); 
                                                ?></span></a></li>
	    <li><a href="#fragment-13"><span><?php //echo FSText::_("D/v sửa chữa"); 
                                            ?></span></a></li>-->
        <!-- <li><a href="#fragment-7"><span><?php echo FSText::_("Màu sắc"); ?></span></a></li> -->
        <!-- <li><a href="#fragment-8"><span><?php echo FSText::_("Kích thước"); ?></span></a></li> -->
        <!-- <li><a href="#fragment-9"><span><?php echo FSText::_("Giá"); ?></span></a></li> -->
        <!-- <li><a href="#fragment-8"><span><?php echo FSText::_("Size"); ?></span></a></li> -->
        <?php if (isset($extend_fields) && $extend_fields) { ?>

            <!-- <li><a href="#fragment-14"><span><?php //echo FSText::_("Đặc điểm nổi bật"); 
                                                    ?></span></a></li> -->
            <li><a href="#fragment-9"><span><?php echo FSText::_("Thuộc tính riêng"); ?></span></a></li>


            <!-- <li><a href="#fragment-11"><span><?php //echo FSText::_("Tin liên quan"); 
                                                    ?></span></a></li>
					<li><a href="#fragment-12"><span><?php //echo FSText::_("Hướng đẫn sd"); 
                                                        ?></span></a></li> -->
        <?php } ?>

        <?php if (isset($extend_fields_new) && !empty($extend_fields_new)) { ?>
            <li><a href="#fragment-10"><span><?php echo FSText::_("Thuộc tính riêng"); ?></span></a></li>
        <?php } ?>
    </ul>

    <!--	BASE FIELDS    -->
    <div id="fragment-1">
        <?php include_once 'detail_base.php'; ?>
    </div>
    <!--	END BASE FIELDS    -->

    <!--	IMAGE FIELDS    -->
    <div id="fragment-3">
        <?php include_once 'detail_seo.php'; ?>
    </div>

    <!-- <div id="fragment-2">
        <?php include_once 'detail_specs.php'; ?>
    </div> -->
    <!--	IMAGE FIELDS    -->

    <div id="fragment-4">
        <?php include_once 'detail_related.php'; ?>
    </div>
    <!-- <div id="fragment-8">
					<?php //include_once 'detail_compatable.php';
                    ?>
				</div>
				<div id="fragment-13">
					<?php //include_once 'detail_service.php';
                    ?>
				</div>-->
    <!-- <div id="fragment-7">
        <?php // include_once 'detail_color.php'; ?>
    </div>
    <div id="fragment-8">
        <?php // include_once 'detail_sizes.php'; ?>
    </div>
    <div id="fragment-9">
        <?php // include_once 'detail_prices.php'; ?>
    </div> -->
    <!-- <div id="fragment-8">
    </div> -->
    <?php if (isset($extend_fields) && $extend_fields) { ?>

        <!--	end IMAGE FIELDS    -->
        <!-- <div id="fragment-14">
			    	<?php //include_once 'detail_images_plus.php';
                    ?>
			    </div> -->
        <div id="fragment-9">
            <?php include_once 'detail_extend.php'; ?>
        </div>


        <!-- <div id="fragment-11">
			    	<?php //include_once 'detail_news_related.php';
                    ?>
			    </div>
			    <div id="fragment-12">
			    	<?php //include_once 'detail_guide.php';
                    ?>
			    </div> -->

    <?php } ?>

    <?php if (isset($extend_fields_new) && !empty($extend_fields_new)) { ?>
        <div id="fragment-10">
            <?php include_once 'detail_extend_new.php'; ?>
        </div>
    <?php } ?>
</div>

<?php
//$this->dt_form_end_col(); // END: col-4
$this->dt_form_end(@$data, 1, 0, 2, 'Cấu hình seo', '', 1);
?>

<script type="text/javascript" src="../libraries/jquery/jquery.ui/jquery-ui.js"></script>