<link type="text/css" rel="stylesheet" media="all" href="templates/default/css/jquery-ui.css" />
<script type="text/javascript" src="../libraries/jquery/jquery.ui/jquery-ui.js"></script>
<?php  
	global $toolbar;
	$toolbar->setTitle(FSText :: _('Products') );
	$toolbar->addButton('duplicate',FSText :: _('Duplicate'),FSText :: _('You must select at least one record'),'duplicate.png');
	$toolbar->addButton('save_all',FSText :: _('Save'),'','save.png'); 
	$toolbar->addButton('add',FSText :: _('Add'),'','add.png'); 
	$toolbar->addButton('edit',FSText :: _('Edit'),FSText :: _('You must select at least one record'),'edit.png'); 
	$toolbar->addButton('remove',FSText :: _('Remove'),FSText :: _('You must select at least one record'),'remove.png'); 
	$toolbar->addButton('published',FSText :: _('Published'),FSText :: _('You must select at least one record'),'published.png');
	$toolbar->addButton('unpublished',FSText :: _('Unpublished'),FSText :: _('You must select at least one record'),'unpublished.png');
		// $toolbar->addButton('update',FSText :: _('Update'),FSText :: _('You must select at least one record'),'update.png');
	//$toolbar->addButton('export',FSText :: _('Export'),'','Excel-icon.png');

    $array_status = array(
        2=>FSText::_('Tạm dừng'),
        1=>FSText::_('Kích hoạt')
    );
    $array_status_pro = array(
        2=>FSText::_('Hết hàng'),
        1=>FSText::_('còn hàng')
    );
	//	FILTER

    $filter_config  = array();
	$fitler_config['search'] = 1; 
	$fitler_config['filter_count'] = 4;
	$fitler_config['text_count'] = 2;

	
	$filter_categories = array();
	$filter_categories['title'] = FSText::_('Categories'); 
	$filter_categories['list'] = @$categories; 
	$filter_categories['field'] = 'treename';

    $filter_brands = array();
    $filter_brands['title'] = FSText::_('Brand');
    $filter_brands['list'] = @$brand;

    $filter_status = array();
    $filter_status['title'] = FSText::_('Trạng thái kích hoạt');
    $filter_status['list'] = @$array_status;

    $filter_status_pro = array();
    $filter_status_pro['title'] = FSText::_('Trạng thái');
    $filter_status_pro['list'] = @$array_status_pro;

    $text_from_date = array();
    $text_from_date['title'] = FSText::_('Từ ngày');

    $text_to_date = array();
    $text_to_date['title'] = FSText::_('Đến ngày');

	$fitler_config['filter'][] = $filter_categories;
    $fitler_config['filter'][] = $filter_status;
    $fitler_config['filter'][] = $filter_brands;
    $fitler_config['filter'][] = $filter_status_pro;
    $fitler_config['text'][] = $text_from_date;
    $fitler_config['text'][] = $text_to_date;

	//	CONFIG
	$list_config = array();
    $list_config[] = array('title'=>'Image','field'=>'image','type'=>'image','arr_params'=>array('width'=> 60,'search'=>'/original/','replace'=>'/resized/'));
	$list_config[] = array('title'=>'Tên','field'=>'name','ordering'=> 1, 'type'=>'text_link','col_width' => '20%','link'=>'index.php?module=products&view=product&ccode=ccode&code=code&id=id&Itemid=10','arr_params'=>array('size'=> 30));
    $list_config[] = array('title'=>'Price','field'=>'price','ordering'=> 1, 'type'=>'format_money','col_width' => '100');
	$list_config[] = array('title'=>'Category','field'=>'category_name','ordering'=> 1, 'type'=>'text','col_width' => '15%');
    $list_config[] = array('title' =>'Mới', 'field' => 'is_new', 'ordering' => 1, 'type' => 'change_status_aj_new', 'arr_params' => array('function' => 'is_new'));
    $list_config[] = array('title'=>'Hot','field'=>'is_hot','ordering'=> 1, 'type'=>'change_status_aj_hot','arr_params'=>array('function'=>'is_hot'));
    $list_config[] = array('title' =>'Hiển thị trang chủ', 'field' => 'show_in_home', 'ordering' => 1, 'type' => 'change_status_aj_show_in_home', 'arr_params' => array('function' => 'show_in_home'));

    // $list_config[] = array('title'=>'Hàng mới','field'=>'is_new','ordering'=> 1, 'type'=>'change_status_aj_new','arr_params'=>array('function'=>'is_new'));
    $list_config[] = array('title'=>'Ordering','field'=>'ordering','ordering'=> 1, 'type'=>'edit_text','arr_params'=>array('size'=>3));
	$list_config[] = array('title'=>'Published','field'=>'published','ordering'=> 1, 'type'=>'published');

	$list_config[] = array('title'=>'Edit','type'=>'edit');
	$list_config[] = array('title'=>'Edited time','field'=>'edited_time','ordering'=> 1, 'type'=>'datetime');
	$list_config[] = array('title'=>'Id','field'=>'id','ordering'=> 1, 'type'=>'text');
    
	TemplateHelper::genarate_form_liting($this->module,$this -> view,$list,$fitler_config,$list_config,$sort_field,$sort_direct,$pagination);
?>
<script>
	$(function() {
		$( "#text0" ).datepicker({clickInput:true,dateFormat: 'dd-mm-yy',changeMonth: true, changeYear: true} );
		$( "#text1" ).datepicker({clickInput:true,dateFormat: 'dd-mm-yy',changeMonth: true, changeYear: true} );
	});

    function ajax_unstt(id,type){

        $.get("index2.php?module=products&view=products&task=ajax_unstt&raw=1",{id:id,type:type}, function(status){
            if(status != 0){
                // $(name).remove();
                var name='';
                var st='';
                if(type=='unis_sell') {
                    name = '#au_' + id + '_sell';
                    type = 'is_sell';
                    st='sell';
                }
                else if(type=='unis_hot'){
                    name = '#au_' + id + '_hot';
                    type='is_hot';
                    st='hot';
                }
                else if(type=='unis_new'){
                    name = '#au_' + id + '_new';
                    type='is_new';
                    st='new';
                }
                else if(type=='unshow_in_home'){
                    name = '#au_' + id + '_show_in_home';
                    type='show_in_home';
                    st='show_in_home';
                }
                // var $html='<a title="Enable item" id="as_'+id';
                // $html +='" onclick="ajax_stt(\''+id+'\',\''+type+'\')" href="javascript:void(0)"> <img border="0" alt="Disable status" src="templates/default/images/unpublished.png"></a>';
                // alert(status);

                $(name+' img').attr('src','templates/default/images/unpublished.png');
                $(name).attr('onclick','ajax_stt(\''+id+'\',\''+type+'\')');
                $(name).attr('id','as_'+id+'_'+st);

                // $(name).parent().append('<a>adb</a>');
            }
        });
    }

    function ajax_stt(id,type){
        $.get("index2.php?module=products&view=products&task=ajax_stt&raw=1",{id:id,type:type}, function(status){
            if(status != 0){
                var name='';
                if(type=='is_sell') {
                    name = '#as_' + id + '_sell';
                    type = 'unis_sell';
                    st='sell';
                }
                else if(type=='is_hot'){
                    name = '#as_' + id + '_hot';
                    type='unis_hot';
                    st='hot';
                }
                else if(type=='is_new'){
                    name = '#as_' + id + '_new';
                    type='unis_new';
                    st='new';
                }
                else if(type=='show_in_home'){
                    name = '#as_' + id + '_show_in_home';
                    type='unshow_in_home';
                    st='show_in_home';
                }
                // var $html='<a title="Enable item" id="as_'+id';
                // $html +='" onclick="ajax_stt(\''+id+'\',\''+type+'\')" href="javascript:void(0)"> <img border="0" alt="Disable status" src="templates/default/images/unpublished.png"></a>';
                // alert(status);

                $(name+' img').attr('src','templates/default/images/published.png');
                $(name).attr('onclick','ajax_unstt(\''+id+'\',\''+type+'\')');
                $(name).attr('id','au_'+id+'_'+st);
            }
        });
    }
</script>
