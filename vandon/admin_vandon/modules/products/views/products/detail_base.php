<?php
  $this->dt_form_begin(1, 2, FSText::_(''), 'fa-user', 1, 'col-md-12 fl-left border-none');
  echo '<div class="wrap-colpro">';
  echo '<div class="colpro">';
  TemplateHelper::dt_edit_selectbox(FSText::_('Categories'), 'category_id', $category_id, 0, $relate_categories, $field_value = 'id', $field_label = 'treename', $size = 1, 0, 0, '', '', '', 'col-md-3', 'col-md-9');
  // TemplateHelper::dt_edit_text(FSText:: _('Ordering'), 'ordering', @$data->ordering, @$maxOrdering, '', '', 0, '', '');
  TemplateHelper::dt_edit_text(FSText::_('Name'), 'name', @$data->name, '', 255, 1, 0, '', '', 'col-md-3', 'col-md-9');
  TemplateHelper::dt_edit_text(FSText::_('Alias'), 'alias', @$data->alias, '', 40, 1, 0, FSText::_("Để trống sẽ tự động lấy tiêu đề sản phẩm"), '', 'col-md-3', 'col-md-9');
  // echo '<div style="display: flex;">';
  // TemplateHelper::dt_edit_text(FSText::_('Đơn vị'), 'unit', @$data->unit, 1, 255, 1, 0, '', '', 'col-md-3', 'col-md-8');
  // TemplateHelper::dt_edit_selectbox('', 'measure', @$data->measure, 0, @$list_measure, $field_value = 'id', $field_label = 'name', $size = 1, 0, 'không', '', '', '', '', 'col-md-12', '', 0);
  // echo '</div>';
  // TemplateHelper::dt_edit_selectbox(FSText::_('Xuất xứ'), 'origin_id', @$data->origin_id, 0, @$origin, $field_value = 'id', $field_label = 'name', $size = 1, 0, 0, '', '', '', 'col-md-3', 'col-md-9');
  // TemplateHelper::dt_edit_text(FSText::_('Bảo hành'), 'guarantee', @$data->guarantee, '', 255, 1, 0, '', '', 'col-md-3', 'col-md-9');

  TemplateHelper::dt_edit_image(FSText::_('Ảnh đại diện'), 'image', URL_ROOT . @$data->image);

  echo '</div>';
  echo '<div class="colpro">';
  // TemplateHelper::dt_edit_selectbox(FSText::_('Chất liệu'), 'materials_id', @$data->materials_id, 0, @$materials, $field_value = 'id', $field_label = 'name', $size = 1, 1,0,'','','','col-md-5','col-md-7');

  TemplateHelper::dt_edit_text(FSText::_('Giá bán'), 'price', @$data->price, '', 60, 1, 0, '', '', 'col-md-5', 'col-md-7');
  TemplateHelper::dt_edit_text(FSText::_('Giá gốc'), 'price_old', @$data->price_old, '', 60, 1, 0, '', '', 'col-md-5', 'col-md-7');
  // TemplateHelper::dt_edit_text(FSText::_('Chi tiết khuyến mãi'), 'discount_unit', @$data->discount_unit, '', 60, 1, 0, '', '', 'col-md-5', 'col-md-7');
  // TemplateHelper::dt_edit_selectbox(FSText::_('Phong cách'), 'style_id', @$data->style_id, 0, $styles, $field_value = 'id', $field_label = 'name', $size = 1, 0, 0, '', '', '', 'col-md-5', 'col-md-7');
  TemplateHelper::dt_edit_text(FSText::_('Ordering'), 'ordering', @$data->ordering, @$maxOrdering, 255, 1, 0, '', '', 'col-md-5', 'col-md-7');

  TemplateHelper::dt_edit_selectbox(FSText::_('Danh mục khuyến mại'), 'promotion_type', @$data->promotion_type , 0, $list_promotion_type, $field_value = 'id', $field_label = 'name', $size = 1, 0, 0, '', '', '', 'col-md-5', 'col-md-7');

  echo '</div>';
  echo '</div>';

  echo '<div class="col-checkbox">';
  TemplateHelper::dt_checkbox(FSText::_('Published'), 'published', @$data->published, 1, '', '', '','col-md-5','col-md-7');
  TemplateHelper::dt_checkbox(FSText::_('Còn hàng'), 'is_stock', @$data->is_stock, 0, '', '', '','col-md-5','col-md-7');

  TemplateHelper::dt_checkbox(FSText::_('Nổi bật'), 'is_hot', @$data->is_hot, 0, '', '', '','col-md-5','col-md-7');
  TemplateHelper::dt_checkbox(FSText::_('Mới'), 'is_new', @$data->is_new, 0, '', '', '','col-md-5','col-md-7');
  TemplateHelper::dt_checkbox(FSText::_('Trang chủ'), 'show_in_home', @$data->show_in_home, 0, '', '', '','col-md-5','col-md-7');

  // TemplateHelper::dt_edit_text(FSText:: _('Số lượng còn lại'), 'count', @$data->count, 0);
  // TemplateHelper::datetimepicke(FSText:: _('Ngày hết hạn'), 'date_end', @$data->date_end ? @$data->date_end : date('Y-m-d H:i:s'), FSText:: _('Bạn vui lòng chọn thời gian hiển thị'), 20, '', 'col-md-3', 'col-md-4');
  echo '</div>';
  TemplateHelper::dt_edit_image2(FSText::_('Image'), 'image', str_replace('/original/', '/resized/', URL_ROOT . @$data->image), '', '', '', 1);

  // TemplateHelper::dt_edit_text(FSText:: _('Điểm thưởng'), 'diem_thuong', @$data->diem_thuong, '', 60, 1, 0, '', '');
  // TemplateHelper::dt_edit_text(FSText:: _('Điểm đóng góp'), 'diem_gop', @$data->diem_gop, '', 60, 1, 0, '', '');
  // TemplateHelper::dt_edit_text(FSText:: _('Link Video'), 'video', @$data->video);
  //TemplateHelper::dt_edit_selectbox('Loại giảm giá','discount_unit',@$data -> discount_unit,0,array('percent'=>'Phần trăm'),$field_value = '', $field_label='','','','','','','');
  //TemplateHelper::dt_edit_text(FSText :: _('Giảm giá'),'discount',@$data -> discount,'',60,1,0,'','');

  // TemplateHelper::dt_edit_text(FSText:: _('Khối lượng'), 'weight', @$data->weight, 0);
  // TemplateHelper::dt_edit_text(FSText:: _('Kích thước'), 'size', @$data->size, 0);

  // TemplateHelper::dt_checkbox(FSText::_('Tag giảm giá'),'discount_tag',@$data -> discount_tag,0);
  echo '<div class="colpro-editor">';
  TemplateHelper::dt_edit_text(FSText::_('Sơ lược sản phẩm'), 'summary', @$data->summary, '', 100, 6, 1, '', '', 'col-md-2', 'col-md-10');
  TemplateHelper::dt_edit_text(FSText::_('Mô tả chi tiết'), 'description', @$data->description, '', 100, 6, 1, '', '', 'col-md-2', 'col-md-10');
  // TemplateHelper::dt_edit_text(FSText::_('Bộ sản phẩm'), 'accessories', @$data->accessories, '', 100, 6, 1, '', '', 'col-md-2', 'col-md-10');
  echo '</div>';

$this->dt_form_end_col(); // END: col-4
?>

