<div class="dataTable_wrapper">
	<table id="table-prices" border="0" class="tbl_form_contents table table-hover table-striped table-bordered" width="100%" cellspacing="4" cellpadding="4" bordercolor="#CCC">
		<thead>
			<tr>
				<th width="20%" align="center">
					<?php echo FSText::_('Chất liệu'); ?>
				</th>
				<th width="20%" align="center">
					<?php echo FSText::_('Kích thước'); ?>
				</th>
				<th width="20%" align="center">
					<?php echo FSText::_('Màu sắc'); ?>
				</th>
				<th width="20%" align="center">
					<?php echo FSText::_('Giá'); ?>
				</th>
				<th width="20%" align="center">
					<?php echo FSText::_('Hiển thị'); ?>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php
			if (isset($prices) && !empty($prices)) {
				foreach ($prices as $item) { ?>
					<?php
					$color_data = $this->model->get_record_by_id($item->color_id, 'fs_products_colors');
					$material_data = $this->model->get_record_by_id($item->material_id, 'fs_products_materials');
					$size_data = $this->model->get_record_by_id($item->size_id, 'fs_products_sizes');
					?>
					<tr>
						<td>
							<?php echo @$material_data->name; ?><br />
						</td>
						<td>
							<?php echo @$size_data->name; ?><br />
						</td>
						<td>
							<?php echo @$color_data->name; ?><br />
						</td>
						<td>
							<?php echo format_money($item->price); ?><br />
						</td>
						<td>
							<input type="checkbox" value="<?php echo $item->id; ?>" name="other_price_exist[]" id="other_price_exit<?php echo $item->id; ?>" checked />
						</td>
					</tr>
			<?php }
			} ?>
		</tbody>
	</table>
</div>

<span data-bs-toggle="tooltip" data-bs-placement="bottom" data-id="<?php echo FSInput::get("id") ?>" class="btn add-more-prices btn-primary" style="margin-top: 10px">
	Thêm giá
	<i class="fa fa-plus-square" aria-hidden="true"></i>
</span>
<div class="tip">Bạn chưa chọn chất liệu, kích thước hoặc màu sắc</div>
<style>
	input[type=checkbox] {
		transform: scale(1.5);
	}

	.sizes-picture {
		height: 60px !important;
	}
	.tip {
		display: none;
		color: #dc2c2c;
		font-weight: bold;
	}
</style>
<script>
	$(document).ready(function() {
		// let arr_checkbox = $(".select-checkbox").attr("disabled", true)
		let selected_checkbox
		$(".select-file").change(function(e) {
			let fileId = $(this).attr('data-id');
			let id = $(this).attr('data-id');
			if ($(`#other_image_${fileId}`).val()) {
				// $(`#other_size${id}`).attr("disabled", false);
				$(`#other_size${id}`).attr("checked", true);
			} else {
				// $(`#other_size${id}`).attr("disabled", true);
			}
		})
	})

	let dom_color, dom_material, dom_sizes;
	<?php if (!empty($sizes) || !empty($materials_products) || !empty($colors_products)) { ?>
		$(".add-more-prices").click(function() {
			$(".tip").css('display', 'none')
			dom_sizes =
				`
				<select class="select2 form-control" name="size_id_price${order}">
					<?php if (!empty($sizes)) { ?>
						<?php foreach ($sizes as $item) { ?>
							<option value="<?php echo $item->id ?>"><?php echo $item->name ?></option>
						<?php } ?>
					<?php } else { ?>
						<option value="">Chưa có kích thước</option>
					<?php } ?>
				</select>
				`
			dom_material =
				`
				<select class="select2 form-control" name="material_id_price${order}">
					<?php if (!empty($materials_products)) { ?>
						<?php foreach ($materials_products as $item) { ?>
							<option value="<?php echo $item->id ?>"><?php echo $item->name ?></option>
						<?php } ?>
					<?php } else { ?>
						<option value="">Chưa chọn chất liệu</option>
					<?php } ?>
				</select>
				`
			dom_color =
				`
				<select class="select2 form-control" name="color_id_price${order}">
					<?php if (!empty($colors_products)) { ?>
						<?php foreach ($colors_products as $item) { ?>
							<option value="<?php echo $item->color_id ?>"><?php echo $item->color_name ?></option>
						<?php } ?>
					<?php } else { ?>
						<option value="">Chưa chọn màu sắc</option>
					<?php } ?>
				</select>
				`


			let row =
				`
					<tr>
							<td style="width: 100%; height: 120px; display: flex; justify-content: center; align-items: center">
								${dom_material}
							</td>
							<td>
								<div style="width: 100%; height: 100px; display: flex; justify-content: center; align-items: center">
									${dom_sizes}
								</div>
							</td>
							<td>
								<div style="width: 100%; height: 100px; display: flex; justify-content: center; align-items: center">
									${dom_color}
								</div>
							</td>
							<td>
								<div style="width: 100%; height: 100px; display: flex; justify-content: center; align-items: center">
									<input style="width: 50%;" type="text" class="form-control name-color-${order}" placeholder="Giá" name="price_price${order}" aria-label="Username" aria-describedby="basic-addon1">
								</div>
							</td>
							<td>
									<div style="width: 100%; height: 100px; display: flex; justify-content: center; align-items: center">
										<input class="select-checkbox" id="" type="checkbox" value="${order}" name="other_price_new[]" id="other_price_new${order}" checked/>
									</div>
							</td>
					</tr>
				`
			$('#table-prices tr:last').after(row);
			++order;
		
		})
	<?php } else { ?>
		$(".add-more-prices").click(function() {
			$(".tip").css('display', 'block')
		})
	<?php } ?>
</script>