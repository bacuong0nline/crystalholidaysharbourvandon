<div class="dataTable_wrapper">
	<table id="table-sizes" border="0" class="tbl_form_contents table table-hover table-striped table-bordered" width="100%" cellspacing="4" cellpadding="4" bordercolor="#CCC">
		<thead>
			<tr>
				<th width="20%" align="center">
					<?php echo FSText::_('Kích thước'); ?>
				</th>
				<!-- <th width="20%" align="center">
					<?php echo FSText::_('Chất liệu'); ?>
				</th> -->
				<!-- <th width="20%" align="center">
					<?php echo FSText::_('Giá'); ?>
				</th> -->
				<th width="20%" align="center">
					<?php echo FSText::_('Hiển thị'); ?>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php
			if (isset($sizes) && !empty($sizes)) {
				foreach ($sizes as $item) {
					@$data_by_size = $array_data_by_size[$item->id];
			?>
					<?php if (@$data_by_size) { 
						$color_data = $this->model->get_record_by_id($data_by_size->color_id, 'fs_products_colors');
						?>
						<tr>
							<td>
								<?php echo $item->name; ?><br />
							</td>
							<!-- <td>
								<?php $link_img = str_replace('/original', '/small/', @$data_by_size->image); ?>
								<img style="margin-top: 10px" onerror="this.src='/images/not_picture.png'" class="sizes-picture img-responsive image-box box-image-<?php echo $item->id ?>" alt="" src="<?php echo URL_ROOT . $link_img; ?>" /><br />
								<div class="fileUpload btn btn-primary ">
									<span><i class="fa fa-cloud-upload"></i> Chọn ảnh</span>
									<input data-id="<?php echo $item->id ?>" data-class="box-image-<?php echo $item->id ?>" class="upload upload select-file" type="file" name="image_exit_size_<?php echo $item->id; ?>" />
									<input data-id="<?php echo $item->id ?>" data-class="size-box-image-<?php echo $item->id ?>" class="upload select-file" type="file" id="other_image_<?php echo $item->id; ?>" name="other_image_<?php echo $item->id; ?>" data-id=<?php echo $item->id; ?> />
								</div>
								<input type="hidden" value="<?php echo @$data_by_size->id; ?>" name="id_exist_<?php echo $item->id; ?>">
								<input type="hidden" value="<?php echo $item->id; ?>" name="size_exist_total[]" />
								<input type="hidden" id="name_image_exit_<?php echo $item->id; ?>" name="name_price_size_exist_<?php echo $item->id; ?>" value="<?php echo @$data_by_size->image; ?>">
							</td> -->

							<td>
								<input type="checkbox" value="<?php echo $item->id; ?>" name="other_size_exit[]" id="other_size_exit<?php echo $item->id; ?>" checked />
							</td>
						</tr>
					<?php } else { ?>
						<!-- <tr>
							<td>
								<?php echo $item->name; ?><br />
							</td>
							<td>
								<?php $link_img = str_replace('/original', '/small/', @$item->image); ?>
								<img style="margin-top: 10px" onerror="this.src='/images/not_picture.png'" class="sizes-picture img-responsive image-box size-box-image-<?php echo $item->id ?>" alt="" src="<?php echo URL_ROOT . $item->image; ?>" /><br />
								<div class="fileUpload btn btn-primary ">
									<span><i class="fa fa-cloud-upload"></i> Chọn ảnh</span>
									<input data-id="<?php echo $item->id ?>" data-class="size-box-image-<?php echo $item->id ?>" class="upload select-file" type="file" id="other_image_<?php echo $item->id; ?>" name="other_image_<?php echo $item->id; ?>" data-id=<?php echo $item->id; ?> />
								</div>
							</td>
							<td>
								<input class="select-checkbox" id="<?php echo "other_size" . $item->id ?>" type="checkbox" value="<?php echo $item->id; ?>" name="other_size[]" id="other_size<?php echo $item->id; ?>" />
							</td>
						</tr> -->
					<?php } ?>
			<?php }
			} ?>
		</tbody>
	</table>
</div>

<span data-id="<?php echo FSInput::get("id") ?>" class="btn add-more-sizes btn-primary" style="margin-top: 10px">
	Thêm kích thước
	<i class="fa fa-plus-square" aria-hidden="true"></i>
</span>
<style>
	input[type=checkbox] {
		transform: scale(1.5);
	}

	.sizes-picture {
		height: 60px !important;
	}
</style>
<script>
	$(document).ready(function() {
		// let arr_checkbox = $(".select-checkbox").attr("disabled", true)
		let selected_checkbox
		$(".select-file").change(function(e) {
			let fileId = $(this).attr('data-id');
			let id = $(this).attr('data-id');
			if ($(`#other_image_${fileId}`).val()) {
				// $(`#other_size${id}`).attr("disabled", false);
				$(`#other_size${id}`).attr("checked", true);
			} else {
				// $(`#other_size${id}`).attr("disabled", true);
			}
		})
	})
	$(".add-more-sizes").click(function() {
		<?php if (!empty($materials)) { ?>
			dom_material =
				`
			<select class="select2 form-control" name="material_id${order}">
				<?php foreach ($materials as $item) { ?>
					<option value="<?php echo $item->id ?>"><?php echo $item->name ?></option>
				<?php } ?>
			</select>
			`
		<?php } else { ?>
			dom_material = 'Không có chất liệu'
		<?php } ?>
		let row =
			`
        <tr>
            <td style="width: 100%; height: 120px; display: flex; justify-content: center; align-items: center">
                <input style="width: 100%; text-align: center" type="text" class="form-control name-color-${order}" placeholder="Tên kích thước" name="name_size${order}" aria-label="Username" aria-describedby="basic-addon1">
            </td>
						<td>
								<div style="width: 100%; height: 100px; display: flex; justify-content: center; align-items: center">
	            		<input class="select-checkbox" id="" type="checkbox" value="${order}" name="other_size_new[]" id="other_size_new${order}" />
								</div>
						</td>
        </tr>
    	`
		$('#table-sizes tr:last').after(row);
		++order;
	})
</script>