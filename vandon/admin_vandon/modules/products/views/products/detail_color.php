<input style="width: 30%; margin-bottom: 10px;" class="form-control" type="text" id="search-color" placeholder="Tìm kiếm màu ">

<table border="0" id="table-color" class="tbl_form_contents" width="100%" cellspacing="4" cellpadding="4" bordercolor="#3c8dbc">
    <thead>
        <tr>
            <th align="center" width="15%">
                <?php echo FSText::_('Màu'); ?>
            </th>
            <th align="center" width="25%">
                <?php echo FSText::_('Chọn ảnh'); ?>
            </th>
            <th align="center" width="15%">
                <?php echo FSText::_('Hiển thị'); ?>
            </th>
        </tr>
    </thead>
    <tbody>

        <?php
        if (isset($colors) && !empty($colors)) {
            foreach ($colors as $item) {
                @$data_by_color = $array_data_by_color[$item->id];
        ?>
                <?php if (@$data_by_color) { ?>
                    <tr style="background-color: <?php echo '#' . $item->code . '30' ?>">
                        <td>

                            <?php echo $item->name; ?><br />
                            <span id="colorSelector"><span style="background-color: <?php echo ($item->code) ? '#' . $item->code : '#0000ff'; ?>"></span></span>
                        </td>

                        <td>
                            <?php $link_img = str_replace('/original', '/small/', @$data_by_color->image); ?>
                            <img style="margin-top: 10px" onerror="this.src='/images/not_picture.png'" class="color-picture img-responsive image-box box-image-<?php echo $item->id ?>" alt="" src="<?php echo URL_ROOT . $link_img; ?>" /><br />
                            <div class="fileUpload btn btn-primary ">
                                <span><i class="fa fa-cloud-upload"></i> Chọn ảnh</span>
                                <input data-class="box-image-<?php echo $item->id ?>" class="upload" type="file" name="image_exit_color_<?php echo $item->id; ?>" />
                                <input type="hidden" id="name_image_exit_<?php echo $item->id; ?>" name="name_price_color_exist_<?php echo $item->id; ?>" value="<?php echo @$data_by_color->image; ?>">
                            </div>
                            <!--                        <input type="text" size="20" id="color_name_exit_--><?php //echo $item->id;
                                                                                                            ?>
                            <!--" name="color_name_exist_--><?php //echo $item->id;
                                                            ?>
                            <!--"  value="--><?php //echo @$data_by_color->name;
                                                ?>
                            <!--" placeholder="Alt ảnh màu" >-->
                        </td>
                        <td>
                            <input type="checkbox" value="<?php echo $item->id; ?>" name="other_color_exit[]" id="other_color_exit<?php echo $item->id; ?>" checked />
                            <input type="hidden" value="<?php echo @$data_by_color->id; ?>" name="id_exist_<?php echo $item->id; ?>">
                            <input type="hidden" value="<?php echo $item->id; ?>" name="color_exist_total[]" />
                        </td>
                    </tr>

                <?php } ?>
            <?php } ?>
            <?php
            foreach ($colors as $item) {
                @$data_by_color = $array_data_by_color[$item->id];
            ?>
                <?php if (!@$data_by_color) { ?>
                    <tr style="background-color: <?php echo '#' . $item->code . '30' ?>">
                        <td>

                            <?php echo $item->name; ?><br />
                            <span id="colorSelector"><span style="background-color: <?php echo ($item->code) ? '#' . $item->code : '#0000ff'; ?>"></span></span>
                        </td>

                        <td>
                            <?php $link_img = str_replace('/original', '/small/', @$data_by_color->image); ?>
                            <img style="margin-top: 10px" onerror="this.src='/images/not_picture.png'" class="box-image-<?php echo $item->id ?> img-responsive image-box color-picture" alt="" src="" />
                            <div class="fileUpload btn btn-primary ">
                                <span><i class="fa fa-cloud-upload"></i> Chọn ảnh</span>
                                <input data-class="box-image-<?php echo $item->id ?>" class="upload select-file" type="file" id="other_image_<?php echo $item->id; ?>" name="other_image_<?php echo $item->id; ?>" data-id=<?php echo $item->id; ?> />
                            </div>
                            <!--                        <input type="text" size="20" id="new_color_name_--><?php //echo $item->id;
                                                                                                            ?>
                            <!--" name="new_color_name_--><?php //echo $item->id;
                                                            ?>
                            <!--" placeholder="Alt ảnh màu" >-->
                        </td>
                        <td>
                            <input class="select-checkbox" type="checkbox" value="<?php echo $item->id; ?>" name="other_color[]" id="other_color<?php echo $item->id; ?>" />
                        </td>
                    </tr>
                <?php } ?>
        <?php
            }
        }
        ?>
    </tbody>
</table>
<span data-id="<?php echo FSInput::get("id") ?>" class="btn add-more-color btn-primary" style="margin-top: 10px">
    Thêm màu
    <i class="fa fa-plus-square" aria-hidden="true"></i>
</span>
<style>
    #colorSelector {
        border: 1px solid #9F9F9F;
        display: inline-block;
        height: 16px;
        position: relative;
        width: 16px;
    }

    #colorSelector span {
        height: 16px;
        left: 0;
        position: absolute;
        top: 0;
        width: 16px;
    }

    .img-responsive {
        margin: auto;
    }

    #table-color tr td {
        width: 30%;
    }

    #table-color tr {
        transition: all 0.3s;
        -webkit-transform: translateZ(0) scale(1, 1);
        -webkit-backface-visibility: hidden;
    }

    #table-color tr:hover {
        background-color: #33333310;
        /* transform: scale(1.02); */
        transition: all 0.3s;

        box-shadow: 0px 0px 11px 0px rgba(0, 0, 0, 0.2);
    }

    .save-color:hover {
        cursor: pointer
    }

    .color-picture {
        height: 60px !important;
    }

    input[type=checkbox] {
        transform: scale(1.5);
    }
</style>
<script>
    function nurZahlen(el) {
        var val = el.value.replace(/[^0-9+.-]/g, '');
        el.value = val;
    }
    $(document).ready(function() {
        // let arr_checkbox = $(".select-checkbox").attr("disabled", true)
        let selected_checkbox
        $(document).on("change", ".select-file", function() {
            let fileId = $(this).attr('data-id');
            let id = $(this).attr('data-id');
            if ($(`#other_image_${fileId}`).val()) {
                // $(`#other_color${id}`).attr("disabled", false);
                $(`#other_color${id}`).attr("checked", true);
            } else {
                // $(`#other_color${id}`).attr("disabled", true);
            }
        })
        var $rows = $('#table-color tr');
        $('#search-color').keyup(function() {
            var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

            $rows.show().filter(function() {
                var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
                return !~text.indexOf(val);
            }).hide();
        });
    })
    let order = 0;
    let record_id = $(".add-more-color").attr("data-id");
    $(".add-more-color").click(function() {
        let row =
            `
        <tr>
            <td style="border: none; width: 100%; height: 175px; display: flex; justify-content: center; align-items: center">
                <input style="border-radius: 5px; width: 50%; margin-right: 20px;" type="text" class="form-control name-color-${order}" placeholder="Tên màu" name="name_color" aria-label="Username" aria-describedby="basic-addon1">
                <input type="color" name="code_color" class="code-color-${order}"/>
            </td>
            <td>
                <img style="margin-top: 10px" onerror="this.src='/images/not_picture.png'" class="color-picture box-image-${order} img-responsive image-box" alt="" src="" /><br />
                <div class="fileUpload btn btn-primary ">
                    <span><i class="fa fa-cloud-upload"></i> Chọn ảnh</span>
                    <input data-class="box-image-${order}" class="upload select-file select-file-${order}" type="file" />
                </div>
            </td>
            <td class="select-color-box">
                <a data-order="${order}" class="save-color"><img src="templates/default/images/toolbar/save.png"></a>
            </td>
        </tr>
    `
        $('#table-color tr:last').after(row);
        ++order;
    })

    $(document).on('click', '.save-color', function() {
        let order = $(this).attr("data-order");
        let file_data = $(".select-file-" + order).prop('files')[0];
        let _this = this;
        let code = $(".code-color-" + order).val();
        var rgbaCol = 'rgba(' + parseInt(code.slice(-6, -4), 16) +
            ',' + parseInt(code.slice(-4, -2), 16) +
            ',' + parseInt(code.slice(-2), 16) +
            ',0.3)';
        let form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('name_color', $(".name-color-" + order).val());
        form_data.append('code_color', $(".code-color-" + order).val());
        form_data.append('record_id', record_id);
        $.ajax({
            method: 'POST',
            processData: false,
            contentType: false,
            url: "/admin_hcbc/index.php?module=products&view=products&task=save_color&raw=1",
            data: form_data,
            success: function(result) {
                let data = JSON.parse(result);
                $(_this).parent().parent().css("background-color", rgbaCol)
                $(_this).hide();
                alert(data.message);
                $(_this).parent().html(data.input)
                $(".select-file-" + order).attr("data-id", data.id)
                $(".select-file-" + order).attr("id", "other_image_" + data.id)
                $(".select-file-" + order).attr("name", "other_image_" + data.id)
            }
        })
    })
</script>