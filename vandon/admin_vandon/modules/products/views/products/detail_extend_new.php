<table>
<?php

$data_extends = unserialize(@$data->data_extends);
foreach ($extend_fields_new as $item){
    switch ($item->field_type){
        case 'select':
            $sub_item ='';
            TemplateHelper::dt_edit_selectbox($item->field_name, $item->id.'_extend', @$data_extends[$item->id]['value'],0, @$item->select_option,'id', 'name', $size = 1,0,0,'',$sub_item);
            break;
        case 'multi_select':
            $sub_item ='';
            TemplateHelper::dt_edit_selectbox($item->field_name, $item->id.'_extend', @$data_extends[$item->id]['value'],0, @$item->select_option,'id', 'name', $size = 10,1,0,'Giữ phím Ctrl để chọn nhiều item', $sub_item);
            break;
        case 'yesno':
            TemplateHelper::dt_checkbox($item->field_name, $item->id.'_extend', @$data_extends[$item->id]['value'], $default = 0, $array_value = array(1 => 'Có', 0 => 'Không'), $sub_item = '');
            break;
        default:
            TemplateHelper::dt_edit_text($item->field_name, $item->id.'_extend', @$data_extends[$item->id]['value']);
            break;
    }
} ?>
</table>