<?php
$title = @$data ? FSText::_('Edit'): FSText::_('Add');
global $toolbar;
$toolbar->setTitle($title);
$toolbar->addButton('save_add',FSText :: _('Save and new'),'','save_add.png');
$toolbar->addButton('apply',FSText::_('Apply'),'','apply.png');
$toolbar->addButton('save',FSText::_('Save'),'','save.png');
$toolbar->addButton('cancel',FSText::_('Cancel'),'','cancel.png');
$this -> dt_form_begin(1,4,$title.' '.FSText::_('Categories'),'',1,'col-md-8',1);
TemplateHelper::dt_edit_text(FSText :: _('Name'),'name',@$data -> name);
TemplateHelper::dt_edit_text(FSText :: _('Alias'),'alias',@$data -> alias,'',60,1,0,FSText::_("Can auto generate"));
TemplateHelper::dt_edit_selectbox(FSText::_('Parent'),'parent_id',@$data -> parent_id, 5,$categories,$field_value = 'id', $field_label='treename',$size = 1,0,1);
// TemplateHelper::dt_edit_selectbox(FSText::_('Tên bảng'),'tablename',@$data -> tablename,'fs_products',$tables,$field_value = 'table_name', $field_label='table_name',$size = 1,0,1);
TemplateHelper::dt_checkbox(FSText::_('Published'),'published',@$data -> published,1);
TemplateHelper::dt_edit_text(FSText :: _('Ordering'),'ordering',@$data -> ordering,@$maxOrdering,'20');
?>

<?php
// TemplateHelper::dt_edit_image(FSText :: _('Icon'),'icon',URL_ROOT.@$data->icon);
// TemplateHelper::dt_edit_image(FSText :: _('Icon 2'),'icon_home',URL_ROOT.@$data->icon_home);
?>
<?php
// TemplateHelper::dt_edit_image(FSText :: _('Icon hover'),'avatar',str_replace('/original/','/small/',URL_ROOT.@$data->avatar));
// TemplateHelper::dt_edit_image(FSText :: _('Image'),'image',str_replace('/original/','/small/',URL_ROOT.@$data->image));

// TemplateHelper::dt_edit_text(FSText :: _('Chú thích'),'summary',@$data -> summary);
// TemplateHelper::dt_edit_text(FSText :: _('Mô tả danh mục'),'description',@$data -> description,'',100,6);

// TemplateHelper::dt_edit_text(FSText :: _(''),'content',@$data -> content,'',650,450,1,'','','col-sm-2','col-sm-12');
// TemplateHelper::dt_edit_text(FSText :: _('Mô tả dưới'),'summary_small',@$data -> summary_small,'',100,6);
//TemplateHelper::dt_checkbox(FSText::_('Dữ liệu mở rộng'),'have_extend',@$data -> have_extend,1);
//require_once ('detail_extend.php');
?>


<?php
TemplateHelper::dt_edit_selectbox(FSText::_('Các lựa chọn'),'options',@$data -> options,'',$options,$field_value = 'id', $field_label='name',$size = 1,1,1);
//TemplateHelper::dt_edit_selectbox(FSText::_('Xuất xứ'),'origins',@$data -> origins,'',$origins,$field_value = 'id', $field_label='name',$size = 1,1,1);
//TemplateHelper::dt_edit_selectbox(FSText::_('Phong cách'),'styles',@$data -> styles,'',$styles,$field_value = 'id', $field_label='name',$size = 1,1,1);

?>
<?php 

$this->dt_form_end_col(); // END: col-1
$this -> dt_form_end(@$data,1,1,2,FSText::_('Cấu hình Seo'),'',1,'col-md-4');
?>

