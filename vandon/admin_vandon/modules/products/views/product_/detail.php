<link type="text/css" rel="stylesheet" media="all" href="../libraries/jquery/jquery.ui/jquery-ui.css" />
<script type="text/javascript" src="../libraries/jquery/jquery.ui/jquery-ui.js"></script>
<?php
	$title = @$data ? FSText::_('Edit'): FSText::_('Add'); 
	global $toolbar;
	$toolbar->setTitle($title);
	//$toolbar->addButton('save_add',FSText::_('Save and new'),'','save_add.png'); 
	$toolbar->addButton('apply',FSText::_('Apply'),'','apply.png'); 
	$toolbar->addButton('Save',FSText::_('Save'),'','save.png'); 
    $toolbar->addButton('back',FSText::_('Cancel'),'','cancel.png');   
    
	$this -> dt_form_begin(1,4,$title.' '.FSText::_('Sản phẩm'),'fa-edit',1,'col-md-8 col-xs-12',1);
    	TemplateHelper::dt_edit_text(FSText::_('Name'),'name',@$data -> name);
    	TemplateHelper::dt_edit_text(FSText::_('Alias'),'',@$data -> alias,'',40,1,0,FSText::_("Có thể sinh tự động"));
//        TemplateHelper::dt_edit_text(FSText::_('Code'),'code',@$data -> code);
        TemplateHelper::dt_edit_selectbox(FSText::_('Danh mục'),'category_id',@$data -> category_id,0,$categories,$field_value = 'id', $field_label='treename',$size = 0,0);
        TemplateHelper::dt_edit_selectbox(FSText::_('Hãng sản xuất'),'manufactor_id',@$data -> manufactor_id,0,$list_producer,$field_value = 'id', $field_label='treename',$size = 0,0);

        TemplateHelper::dt_edit_text(FSText::_('Mã sản phẩm'),'pro_code',@$data -> pro_code);
        TemplateHelper::dt_edit_text(FSText::_('Chất liệu'),'material',@$data -> material);
        TemplateHelper::dt_edit_text(FSText::_('Màu sắc'),'color',@$data -> color);
        TemplateHelper::dt_edit_text(FSText::_('Kiểu dáng'),'style',@$data -> style);
        TemplateHelper::dt_edit_text(FSText::_('Kích thước'),'size',@$data -> size);
        TemplateHelper::dt_edit_text(FSText::_('Thời gian giao hàng'),'delivery_time',@$data -> delivery_time);


       TemplateHelper::dt_edit_text(FSText::_('Giá'),'price',@$data -> price);
       TemplateHelper::dt_edit_text(FSText::_('Giá cũ'),'price_old',@$data -> price_old);
//        TemplateHelper::dt_edit_text(FSText::_('Ngày hết hạn'),'expiry_date',@$data -> expiry_date);
        //TemplateHelper::dt_edit_text(FSText::_('Danh sách số lượng'),'list_quantity',@$data -> list_quantity,'',100,5,0,FSText::_("danh sách số luọng phải viết liền, cách nhau bởi dấu phẩy"),'');
        //TemplateHelper::dt_edit_text(FSText::_('Danh sách giá'),'list_price',@$data -> list_price,'',100,5,0,FSText::_("danh sách giá phải viết liền, cách nhau bởi dấu phẩy"),'');

        TemplateHelper::dt_edit_image(FSText::_('Image'),'image',str_replace('/original/','/small/',URL_ROOT.@$data->image));
        // TemplateHelper::dt_edit_text(FSText::_('Tags'),'tags',@$data -> tags,'',100,3);   
       TemplateHelper::dt_edit_text(FSText :: _('Mô tả'),'summary',@$data -> summary,'',100,6);
//      TemplateHelper::dt_edit_text(FSText :: _('Loại quà tặng'),'gift_code',@$data -> gift_code,'',100,2);
    $this->dt_form_end_col(); // END: col-1
    
    $this -> dt_form_begin(1,2,FSText::_('Kích hoạt'),'fa-user',1,'col-md-4 fl-right');
        TemplateHelper::dt_checkbox(FSText::_('Published'),'published',@$data -> published,1);
        TemplateHelper::dt_checkbox(FSText::_('Nổi bật'),'is_hot',@$data -> is_hot,0);
        TemplateHelper::dt_checkbox(FSText::_('Khuyến mãi'),'is_sell',@$data -> is_sell,0);
        TemplateHelper::dt_checkbox(FSText::_('Tag giảm giá'),'discount_tag',@$data -> discount_tag,0);
        TemplateHelper::dt_checkbox(FSText::_('Còn hàng'),'is_stock',@$data -> is_stock,0);
        TemplateHelper::dt_edit_text(FSText::_('Ordering'),'ordering',@$data -> ordering,@$maxOrdering);
    $this->dt_form_end_col(); // END: col-2        
//    $this -> dt_form_begin(1,2,FSText::_('Tags'),'fa-tags',1,'col-md-4 fl-right');
//        TemplateHelper::dt_edit_text(FSText :: _(''),'tags',@$data -> tags,'',100,4,0,'','','col-sm-2','col-sm-12');
//    $this->dt_form_end_col();

    $this -> dt_form_begin(1,2,FSText::_('Image'),'fa-image',1,'col-md-12');
        TemplateHelper::dt_edit_image2(FSText::_('Image'),'image',str_replace('/original/','/resized/',URL_ROOT.@$data->image),'','','',1);
    $this->dt_form_end_col(); // END: col-3
    
    $this -> dt_form_begin(1,4,FSText::_('Giới thiệu'),'fa-info',1,'col-md-12');
        TemplateHelper::dt_edit_text(FSText::_(''),'content',@$data -> content,'',650,450,1,'','','col-sm-2','col-sm-12');
    $this->dt_form_end_col(); // END: col-4

    $this -> dt_form_begin(1,2,FSText::_('Cấu hình seo'),'',1,'col-md-12 fl-right');
    TemplateHelper::dt_edit_text(FSText::_('SEO title'),'seo_title',@$data -> seo_title? @$data -> seo_title:@$data -> name,'',100,1,0,'','','col-md-12','col-md-12');
    TemplateHelper::dt_edit_text(FSText::_('SEO meta keyword'),'seo_keyword',@$data -> seo_keyword? @$data -> seo_keyword:@$data -> name.'','',100,1,0,'','','col-md-12','col-md-12');
    $description = html_entity_decode(@$data -> description);
    TemplateHelper::dt_edit_text(FSText::_('SEO meta description'),'seo_description',@$data -> seo_description,'',100,9,0,'','','col-md-12','col-md-12');
    $this->dt_form_end_col(); // END: col-4
    
    $this -> dt_form_end(@$data,1,0,2,'Cấu hình seo','',1,'col-sm-4');
?>