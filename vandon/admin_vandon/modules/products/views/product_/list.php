<script language="javascript" type="text/javascript" src="../libraries/jquery/jquery.ui/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="../libraries/jquery/jquery.ui/jquery-ui.css"/>
<?php
global $toolbar;
$toolbar->setTitle(FSText:: _('Danh sách'));
$toolbar->addButton('duplicate', FSText:: _('Duplicate'), FSText:: _('You must select at least one record'), 'duplicate.png');
$toolbar->addButton('save_all', FSText:: _('Save'), '', 'save.png');
$toolbar->addButton('add', FSText:: _('Add'), '', 'add.png');
$toolbar->addButton('Xem chi tiết', FSText:: _('Edit'), FSText:: _('You must select at least one record'), 'edit.png');
$toolbar->addButton('remove', FSText:: _('Remove'), FSText:: _('You must select at least one record'), 'remove.png');
$toolbar->addButton('published', FSText:: _('Published'), FSText:: _('You must select at least one record'), 'published.png');
$toolbar->addButton('unpublished', FSText:: _('Unpublished'), FSText:: _('You must select at least one record'), 'unpublished.png');
//$toolbar->addButton('update',FSText :: _('Update'),FSText :: _('You must select at least one record'),'update.png');
//$toolbar->addButton('export',FSText :: _('Export'),'','Excel-icon.png');

//	FILTER
$filter_config = array();
$fitler_config['search'] = 1;
if ($_SESSION['ad_userid'] == 9) {
    $fitler_config['filter_count'] = 1;

    $filter_categories = array();
    $filter_categories['title'] = FSText::_('Categories');
    $filter_categories['list'] = @$categories;
    $filter_categories['field'] = 'treename';

    $fitler_config['filter'][] = $filter_categories;
}


//	CONFIG
$list_config = array();
$list_config[] = array('title' => 'Name', 'field' => 'name', 'ordering' => 1, 'type' => 'text_link', 'col_width' => '20%', 'link' => 'index.php?module=products&view=product&ccode=ccode&code=code&id=id&Itemid=10', 'arr_params' => array('size' => 20));

$list_config[] = array('title' => 'Image', 'field' => 'image', 'type' => 'image', 'arr_params' => array('width' => 60, 'search' => '/original/', 'replace' => '/small/'));
//$list_config[] = array('title' => 'Category', 'field' => 'category_name', 'ordering' => 1, 'type' => 'text', 'col_width' => '20%', 'arr_params' => array('size' => 20));
//$list_config[] = array('title' => 'Category', 'field' => 'category_id', 'ordering' => 1, 'type' => 'text', 'arr_params' => array('arry_select' => $categories, 'field_value' => 'id', 'field_label' => 'treename', 'size' => 5));
$list_config[] = array('title'=>'Category','field'=>'category_id','ordering'=> 1, 'type'=>'edit_selectbox','arr_params'=>array('arry_select'=>$categories,'field_value'=>'id','field_label'=>'treename','size'=>10));
$list_config[] = array('title' => 'Ordering', 'field' => 'ordering', 'ordering' => 1, 'type' => 'edit_text', 'arr_params' => array('size' => 3));
$list_config[] = array('title' => 'SP tiêu biểu', 'field' => 'is_hot', 'ordering' => 1, 'type' => 'change_status', 'arr_params' => array('function' => 'is_hot'));
$list_config[] = array('title' => 'Published', 'field' => 'published', 'ordering' => 1, 'type' => 'published');
$list_config[] = array('title' => 'Chi tiết', 'type' => 'edit');
$list_config[] = array('title' => 'Thời giạn tạo', 'field' => 'created_time', 'ordering' => 1, 'type' => 'datetime');
$list_config[] = array('title' => 'Id', 'field' => 'id', 'ordering' => 1, 'type' => 'text');
TemplateHelper::genarate_form_liting($this->module, $this->view, $list, $fitler_config, $list_config, $sort_field, $sort_direct, $pagination);
?>

