<?php 
class ProductsModelsCategories extends ModelsCategories
{
    var $limit;
    var $prefix;

    function __construct()
    {
        parent::__construct();
        $limit = FSInput::get('limit', 200, 'int');
        $this->limit = $limit;
        $this->type = 'products';
        $this->table_items = FSTable_ad::_('fs_' . $this->type);
        $this->table_name = FSTable_ad::_('fs_' . $this->type . '_categories');
        $this->table_name_table = FSTable_ad::_('fs_' . $this->type . '_tables');
        $this->check_alias = 1;
        $this->call_update_sitemap = 0;
        $this->img_folder = 'images/' . $this->type . '/cat';
        $this->field_img = 'image';
        // $this->arr_img_paths = array(array('resized', 48, 48, 'resize_image'), array('large', 150, 140, 'resize_image'));
        $this->arr_img_paths_banner = array(array('resized', 200, 180, 'resize_image'), array('large', 300, 380, 'resize_image_fix_height'));
        // $this->arr_img_paths_icon = array(array('resized', 24, 24, 'resized_not_crop'));
        $this->arr_img_paths_banner_menu = array(array('resized', 210, 290, 'resize_image'));
        $this->arr_banner_slide_paths_other = array(array('resized', 580, 300, 'resize_image'));
        //synchronize
        //$this -> array_synchronize = array('fs_products_filters_values' => array('id'=> 'category_id','alias'=>'category_alias')); // đồng bộ dữ liệu ngoài bảng extend. Viết dang  array(tablename => array(field1, field2,...))
        // exception: key (field need change) => name ( key change follow this field)
        $this->field_except_when_duplicate = array(array('list_parents', 'id'), array('alias_wrapper', 'alias'));


    }

    /*
     * Show list category of product follow page
     */
    function get_categories_tree()
    {
        global $db;
        $query = $this->setQuery();
        $sql = $db->query($query);
        $result = $db->getObjectList();
        $tree = FSFactory::getClass('tree', 'tree/');
        $list = $tree->indentRows2($result);
        $limit = $this->limit;
        $page = $this->page ? $this->page : 1;

        $start = $limit * ($page - 1);
        $end = $start + $limit;

        $list_new = array();
        $i = 0;
        foreach ($list as $row) {
            if ($i >= $start && $i < $end) {
                $list_new[] = $row;
            }
            $i++;
            if ($i > $end)
                break;
        }
        return $list_new;
    }

    /*
     * Select all list category of product
     */
    function get_categories_tree_all()
    {
        global $db;
        $query = $this->setQuery();
        $sql = $db->query($query);
        $result = $db->getObjectList();
        $tree = FSFactory::getClass('tree', 'tree/');
        $list = $tree->indentRows2($result);

        return $list;
    }

    function setQuery()
    {

        // ordering
        $ordering = "";
        $task = FSInput::get('task');
        if (isset($_SESSION[$this->prefix . 'sort_field'])) {
            $sort_field = $_SESSION[$this->prefix . 'sort_field'];
            $sort_direct = $_SESSION[$this->prefix . 'sort_direct'];
            $sort_direct = $sort_direct ? $sort_direct : 'asc';
            $ordering = '';
            if ($sort_field)
                $ordering .= " ORDER BY $sort_field $sort_direct, created_time DESC, id DESC ";

        }

        if (!$ordering)
            $ordering .= " ORDER BY created_time DESC , id DESC ";

        $where = "  ";
        if (isset($_SESSION[$this->prefix . 'filter0'])) {
            $filter = $_SESSION[$this->prefix . 'filter0'];
            if ($filter == 2) {
                $where .= ' AND a.published = 0 ';
            } else if ($filter == 0) {
                $where .= '';
            } else {
                $where .= ' AND a.published = ' . $filter . ' ';
            }
        }

        if (isset($_SESSION[$this->prefix . 'filter1'])) {
            $filter = $_SESSION[$this->prefix . 'filter1'];
            if ($filter) {
                $where .= ' AND a.list_parents like  "%,' . $filter . ',%" ';
            }
        }

        if (isset($_SESSION[$this->prefix . 'keysearch'])) {
            if ($_SESSION[$this->prefix . 'keysearch'] && $task != 'edit' && $task != 'add') {
                $keysearch = $_SESSION[$this->prefix . 'keysearch'];
                $where .= " AND name LIKE '%" . $keysearch . "%' ";
            }
        }

        $query = " SELECT a.*, a.parent_id as parent_id 
						  FROM 
						  	" . $this->table_name . " AS a
						  	WHERE 1=1" .
            $where .
            $ordering . " ";
        //echo $query;
        return $query;
    }

    function get_categories_level0()
    {
        global $db;
        $query = ' SELECT *
                        FROM ' . $this->table_name . ' 
                        WHERE published   = 1  AND level = 0 ';
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    function get_tablenames()
    {
        $query = " 	   SELECT DISTINCT(a.table_name) 
						  FROM $this->table_name_table AS a 
						 ";
        global $db;
        $db->query($query);
        $list = $db->getObjectList();
        $list = array_merge(array(0 => (object)array('table_name' => FSTable_ad::_('fs_products'))), $list);
        return $list;
    }
    function save($row = array(), $use_mysql_real_escape_string = 1) {
        $parent_id = FSInput::get('parent_id', '', 'int');
        $row['parent_id'] = 5;
        $options = FSInput::get ( 'options', array (), 'array' );
        $str_options = implode ( ',', $options );

        if ($str_options) {
        	$str_options = ',' . $str_options . ',';
        }
        $row['options'] = $str_options;
        return parent::save($row);
    }
    function update_table_extend($cid, $tablename)
    {

        $record = $this->get_record_by_id($cid, $this->table_name);
        $alias = $record->alias;
        if ($record->parent_id) {
            $parent = $this->get_record_by_id($record->parent_id, $this->table_name);
            $list_parents = ',' . $cid . $parent->list_parents;
            $alias_wrapper = ',' . $alias . $parent->alias_wrapper;
        } else {
            $list_parents = ',' . $cid . ',';
            $alias_wrapper = ',' . $alias . ',';
        }

        // update table items
        $id = FSInput::get('id', 0, 'int');
        if ($id) {
            $row2['category_id_wrapper'] = $list_parents;
            $row2['category_alias'] = $record->alias;
            $row2['category_alias_wrapper'] = $alias_wrapper;
            $row2['category_name'] = $record->name;
            $row2['category_published'] = $record->published;
            $this->_update($row2, $tablename, ' category_id = ' . $cid . ' ');
        }
    }
    /*
     * value: == 1 :new
     * value  == 0 :unnew
     * published record
     */
    function is_hot($value)
    {
        $ids = FSInput::get('id', array(), 'array');

        if (count($ids)) {
            global $db;
            $str_ids = implode(',', $ids);
            $sql = " UPDATE " . $this->table_name . "
							SET is_hot = $value
						WHERE id IN ( $str_ids ) ";
            $db->query($sql);
            $rows = $db->affected_rows();
            return $rows;
        }
        // 	update sitemap
        if ($this->call_update_sitemap) {
            $this->call_update_sitemap();
        }
        return 0;
    }

    /*
     * value: == 1 :new
     * value  == 0 :unnew
     * published record
     */
    function is_menu($value)
    {
        $ids = FSInput::get('id', array(), 'array');

        if (count($ids)) {
            global $db;
            $str_ids = implode(',', $ids);
            $sql = " UPDATE " . $this->table_name . "
							SET is_menu = $value
						WHERE id IN ( $str_ids ) ";
            $db->query($sql);
            $rows = $db->affected_rows();
            return $rows;
        }
        // 	update sitemap
        if ($this->call_update_sitemap) {
            $this->call_update_sitemap();
        }
        return 0;
    }

    function get_size()
    {
        $where = '';


        global $db;
        $query = ' SELECT id,name
							FROM fs_products_sizes 
							WHERE published   = 1 
							 ' . $where . '	OR tablenames="" ';
        $sql = $db->query($query);
        $alias = $db->getObjectList();

        return $alias;
    }

    function get_trademark()
    {
        global $db;
        $query = " SELECT a.*
						  FROM fs_products_thuong_hieu AS a
						  	WHERE published = 1 ORDER BY ordering ";
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }
    function get_material()
    {
        global $db;
        $query = " SELECT a.*
						  FROM fs_products_materials AS a
						  	WHERE published = 1 ORDER BY ordering ";
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }
    function upload_other_images()
    {
        $module = FSInput::get('module');
        global $db;
        $cyear = date('Y');
        $cmonth = date('m');
        $cday = date('d');

        $path = PATH_BASE . 'images' . DS . $module . '_categories' . DS . $cyear . DS . $cmonth . DS . $cday . DS . 'original' . DS;
        require_once(PATH_BASE . 'libraries' . DS . 'upload.php');
        $upload = new  Upload();
        $upload->create_folder($path);
        $file_name = $upload->uploadImage('file', $path, 10000000, '_' . time());
        if (is_string($file_name) and $file_name != '' and !empty($this->arr_banner_slide_paths_other)) {
            foreach ($this->arr_banner_slide_paths_other as $item) {
                $path_resize = str_replace(DS . 'original' . DS, DS . $item [0] . DS, $path);
                $upload->create_folder($path_resize);
                $method_resize = $item [3] ? $item [3] : 'resized_not_crop';
                $upload->$method_resize ($path . $file_name, $path_resize . $file_name, $item [1], $item [2]);
            }
        }


        // xoay ảnh trên IOS và save ghi đè lên ảnh cũ.
        //require_once($_SERVER['DOCUMENT_ROOT'].'/libraries/lib/WideImage.php'); // Gọi thư viện WideImage.php
//            $uploadedFileName = $path.$file_name;  // lấy ảnh từ  đã upload lên
//            $load_img = WideImage::load($uploadedFileName);
//            $exif = exif_read_data($uploadedFileName); //
//            $orientation = @$exif['Orientation'];
//            if(!empty($orientation)) {
//                switch($orientation) {
//                    case 8:
//                        $image_p = imagerotate($uploadedFileName,90,0);
//                        //echo 'It is 8';
//                        break;
//                    case 3:
//                        $image_p = imagerotate($uploadedFileName,180,0);
//
//                        //echo 'It is 3';
//                        break;
//                    case 6:
//                        $load_img->rotate(90)->saveToFile($uploadedFileName);
//                        //$image_p = imagerotate($uploadedFileName,-90,0);
//                        //echo 'It is 6';
//                        break;
//
//                }
//                //imagejpeg ( $image_p , $path.'test.jpg' ,  100 );
//            }
        // END save ảnh xoay trên IOS

//            if(is_string($file_name) and $file_name!='' and !empty($this->arr_banner_slide_paths_other)){
        //      	foreach ( $this->arr_img_paths_other as $item ) {
        // 	$path_resize = str_replace ( '/original/', '/'. $item [0].'/', $path );
        // 	$upload->create_folder ( $path_resize );
        // 	$method_resize = $item [3] ? $item [3] : 'resized_not_crop';
        // 	$upload->$method_resize ( $path . $file_name, $path_resize . $file_name, $item [1], $item [2] );
        // }
//                $fsFile = FSFactory::getClass('FsFiles');
//                foreach($this->arr_banner_slide_paths_other as $item){
//                    $path_resize = str_replace(DS.'original'.DS, DS.$item[0].DS, $path);
//                    $fsFile -> create_folder($path_resize);
//                    $method_resize = $item[3]?$item[3]:'resized_not_crop';
//                    if(!$fsFile ->$method_resize($path.$file_name, $path_resize.$file_name,$item[1], $item[2]))
//                        return false;
//
//                }
//            }
        $data = base64_decode(FSInput::get('data'));
        $data = explode('|', $data);
        $row = array();
        if ($data[0] == 'add')
            $row['session_id'] = $data[1];
        else
            $row['record_id'] = $data[1];
        $row['image'] = 'images/' . $module . '_categories/' . $cyear . '/' . $cmonth . '/' . $cday . '/' . 'original' . '/' . $file_name;
        $row['title'] = $_FILES['file']['name'];

        $fs_table = new FSTable_ad();
        $tablename = $fs_table->_('fs_' . $module . '_categories_images');

        $rs = $this->_add($row, $tablename);
        echo $rs;
        return $rs;
    }

    function delete_other_image($record_id = 0)
    {
        $reocord_id = FSInput::get('reocord_id', 0, 'int');
        $file_name = FSInput::get('name');
        $id = FSInput::get('id');

        $module = FSInput::get('module');
        global $db;

        $where = '';
        if ($file_name) {
            $where .= ' AND title = \'' . $file_name . '\'';
        } else {
            $where .= ' AND id = ' . $id;
        }

        if ($reocord_id) {
            $where .= ' AND record_id = ' . $reocord_id;
        }
        $fs_table = new FSTable_ad();
        $tablename = $fs_table->_('fs_' . $module . '_categories_images');

        $query = '  SELECT *
                        FROM ' . $tablename . '
                        WHERE  1 = 1 ' . $where;
        $db->query($query);
        $images = $db->getObject();
        if ($images) {
            echo $query = '  DELETE FROM ' . $tablename . '
                                WHERE id = \'' . $images->id . '\'';
            $db->query($query);
            $path = PATH_BASE . $images->image;
            @unlink($path);
            foreach ($this->arr_img_paths_other as $image) {
                @unlink(str_replace('/original/', '/' . $image[0] . '/', $path));
            }
        }
    }

    function sort_other_images()
    {
        $module = FSInput::get('module');
        $fs_table = new FSTable_ad();
        $tablename = $fs_table->_('fs_' . $module . '_categories_images');

        global $db;
        if (isset($_POST["sort"])) {
            if (is_array($_POST["sort"])) {
                foreach ($_POST["sort"] as $key => $value) {
                    $db->query("UPDATE " . $tablename . " SET ordering = $key WHERE id = $value");
                }
            }
        }
    }

}