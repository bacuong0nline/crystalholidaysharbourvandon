<?php 
	class ProductsModelsComments extends FSModels{
	
		var $limit;
		var $prefix ;
		function __construct() {
			$this -> limit = 20;
			$this -> view = 'comments';
			$this -> table_name = 'fs_products';
			$this -> table_category_name = 'fs_products_categories';
			parent::__construct();
		}
		
		function setQuery($str_key = ''){
			
			// ordering
			$ordering = "";
			$where = "  ";
			$tmp = 0;
			// id bài viết
			if (isset ( $_SESSION [$this->prefix . 'text0'] )) {
				$search_record_id = $_SESSION [$this->prefix . 'text0'];
				if ($search_record_id) {
					$where .= ' AND a.id =   "' . $search_record_id . '" ';
				}
			}
			
			if (isset ( $_SESSION [$this->prefix . 'sort_field'] )) {
					$sort_field = $_SESSION[$this -> prefix.'sort_field'];
					$sort_direct = $_SESSION[$this -> prefix.'sort_direct'];
					$sort_direct = $sort_direct?$sort_direct:'asc';
					$ordering = '';
					if($sort_field)
						$ordering .= " ORDER BY $sort_field $sort_direct, created_time DESC, id DESC ";
				}
				
			// category
			if (isset ( $_SESSION [$this->prefix . 'filter0'] )) {
				$filter = $_SESSION [$this->prefix . 'filter0'];
				if ($filter) {
					$where .= ' AND a.category_id_wrapper like  "%,' . $filter . ',%" ';
				}
			}

			// type comment: 1=>'Comment đã hiển thị',2=>'Comment chưa hiển thị',3=>'Comment chưa đọc'
			if (isset ( $_SESSION [$this->prefix . 'filter1'] )) {
				$filter = $_SESSION [$this->prefix . 'filter1'];
				if ($filter) {
					if($filter == 1){
						$where .= ' AND comments_published > 0';	
					}else if($filter == 2){
						$where .= ' AND (comments_total - comments_published) > 0';
					}else if($filter == 3){
						$where .= ' AND comments_unread > 0';
					}
				}
			}
					
			if (! $ordering)
				$ordering .= " ORDER BY a.comments_last_time DESC, a.created_time DESC , id DESC ";
			
			if (isset ( $_SESSION [$this->prefix . 'keysearch'] )) {
				if ($_SESSION [$this->prefix . 'keysearch']) {
					$keysearch = $_SESSION [$this->prefix . 'keysearch'];
					$where .= " AND a.name LIKE '%" . $keysearch . "%' ";
				}
			}
			
			// from
			// if(isset($_SESSION[$this -> prefix.'text1'])){
			// 	$tmp++;
			// }
			// // to
			// if(isset($_SESSION[$this -> prefix.'text2'])){
			// 	$tmp++;
			// }
				
			// if(isset($_SESSION[$this -> prefix.'text3'])){
			// 	$tmp++;
			// }

			if($tmp){
				if(!$str_key)
					$where .= " AND 1 = 0";
			}

			if($str_key){
				$where .= " AND id IN (" . $str_key . ")";
			}
			$query = " SELECT  a.* 
							  FROM 
							  	" . $this->table_name . " AS a
							  	WHERE 1=1 " . $where . $ordering . " ";
			return $query;
		}
		
		function setQuery2(){
				
				// ordering
				$ordering = "";
				$where = "";
		
			// from
			if(isset($_SESSION[$this -> prefix.'text1']))
			{
				$date_from = $_SESSION[$this -> prefix.'text1'];
				if($date_from){
					$date_from = strtotime($date_from);
					$date_new = date('Y-m-d H:i:s',$date_from);
					$where .= ' AND b.created_time >=  "'.$date_new.'" ';
				}
			}
			
			// to
			if(isset($_SESSION[$this -> prefix.'text2']))
			{
				$date_to = $_SESSION[$this -> prefix.'text2'];
				if($date_to){
					$date_to = $date_to . ' 23:59:59';
					$date_to = strtotime($date_to);
					$date_new = date('Y-m-d H:i:s',$date_to);
					$where .= ' AND b.created_time <=  "'.$date_new.'" ';
				}
			}


			if(isset($_SESSION[$this -> prefix.'text3']))
			{
				$text = $_SESSION[$this -> prefix.'text3'];
				if($text){
					$where .= ' AND b.name LIKE "%'.$text.'%" ';
				}
			}

			// category
			if (isset ( $_SESSION [$this->prefix . 'filter1'] )) {
				$filter = $_SESSION [$this->prefix . 'filter1'];
				if ($filter) {
					$where .= ' AND b.replied =' . $filter ;
				}
			}
			

			if (! $ordering)
				$ordering .= " ORDER BY  b.created_time DESC , id DESC ";
			
		
			if(!$where)
				return;

			 $query = " SELECT  record_id ,name,parent_id
							  FROM 
							  	fs_products_comments AS b
							  	WHERE 1=1  " . $where . " ";
			return $query;
		}
		


		function get_data_by_comments()
		{

			$where = "";
			$query = " SELECT  record_id ,name,parent_id
			FROM 
				fs_products_comments AS b
				WHERE 1=1  " . $where . " ";

			if(!$query)
				return array();
			// print_r($query);die;
			global $db;
			$sql = $db->query ( $query );
			$result = $db->getObjectList ();
			return $result;
		}
		

		function get_data($str_key)
		{
			global $db;
			$query = $this->setQuery($str_key);
			if(!$query)
				return array();
			$sql = $db->query_limit($query,$this->limit,$this->page);
			$result = $db->getObjectList();
			// print_r($sql);die;
			return $result;
		}

		/*
		 * show total of models
		 */
		function getTotal($str_key)
		{
			global $db;
			$query = $this->setQuery($str_key);
			$sql = $db->query($query);
			$total = $db->getTotal();
			return $total;
		}
		
		function getPagination($str_key = null)
		{
			$total = $this->getTotal($str_key);			
			$pagination = new Pagination($this->limit,$total,$this->page);
			return $pagination;
		}


		function save($row = array(),$use_mysql_real_escape_string = 0) {
			return;
		}
		
		function get_comments_by_product($record_id,$str_key_parent_id='') {
			global $db;
			if (! $record_id)
				return;
			$type = FSInput::get('type',0,'int');
			$where = 	' WHERE record_id = '.$record_id.' ';
			if($type == 1){
				$where .= ' AND published = 1';
			}else if($type == 2){
				$where .= ' AND published = 0';
			}
			
			// from
			if(isset($_SESSION[$this -> prefix.'text1']))
			{
				$date_from = $_SESSION[$this -> prefix.'text1'];
				if($date_from){
					$date_from = strtotime($date_from);
					$date_new = date('Y-m-d H:i:s',$date_from);
					$where .= ' AND b.created_time >=  "'.$date_new.'" ';
				}
			}
			
			// to
			if(isset($_SESSION[$this -> prefix.'text2']))
			{
				$date_to = $_SESSION[$this -> prefix.'text2'];
				if($date_to){
					$date_to = $date_to . ' 23:59:59';
					$date_to = strtotime($date_to);
					$date_new = date('Y-m-d H:i:s',$date_to);
					$where .= ' AND b.created_time <=  "'.$date_new.'" ';
				}
			}
				

			if(isset($_SESSION[$this -> prefix.'text3']))
			{
				$text = $_SESSION[$this -> prefix.'text3'];
				if($text){
					$where .= ' AND b.name LIKE "%'.$text.'%" ';
				}
			}
			
			
			// category
			if (isset ( $_SESSION [$this->prefix . 'filter1'] )) {
				$filter = $_SESSION [$this->prefix . 'filter1'];
				if ($filter) {
					$where .= ' AND b.replied =' . $filter ;
				}
			}
			if($str_key_parent_id){
				$where .= " OR (id IN (" . $str_key_parent_id . ") AND record_id = ".$record_id ." ) ";
			}

			 $query = " SELECT name,created_time,id,email,comment,parent_id,rating, published,record_id,user_id,is_admin,is_q_a
							FROM fs_products_comments as b
							$where
							ORDER BY  created_time  DESC
							LIMIT 120
							";
//			echo $query;die;
			$db->query ( $query );
			$result = $db->getObjectList ();
			$tree = FSFactory::getClass ( 'tree', 'tree/' );
			$list = $tree->indentRows2 ( $result );
			return $list;
		}
		function ajax_question_answer($value = 1) {
			$id = FSInput::get ( 'id', 0, 'int' );
			$record_id = FSInput::get ( 'record_id', 0, 'int' );
			if (! $id || ! $record_id) {
				echo 0;
				return;
			}
			
			$row ['is_q_a'] = $value;
			$rs = $this->_update ( $row, 'fs_products_comments', 'id = ' . $id . ' AND record_id =  ' . $record_id . ' ' );
			echo $rs ? 1 : 0;
			return;
		}
		function ajax_published($published = 1) {
			$id = FSInput::get ( 'id', 0, 'int' );
			$record_id = FSInput::get ( 'record_id', 0, 'int' );
			if (! $id || ! $record_id) {
				echo 0;
				return;
			}
			
			$row ['published'] = $published;
			$rs = $this->_update ( $row, 'fs_products_comments', 'id = ' . $id . ' AND record_id =  ' . $record_id . ' ' );
			echo $rs ? 1 : 0;
			if ($rs) {
				$this->recal_comments ( $record_id );
			}
			return;
		}
		function ajax_del() {
			$id = FSInput::get ( 'id', 0, 'int' );
			$record_id = FSInput::get ( 'record_id', 0, 'int' );
			if (! $id || ! $record_id) {
				echo 0;
				return;
			}
			
			$rs = $this->_remove ( 'id = ' . $id . ' AND record_id =  ' . $record_id . ' ', 'fs_products_comments' );
			if($rs){
				$this -> _remove('record_id = '.$id, 'fs_notify');
			}
			echo $rs ? 1 : 0;
			if ($rs) {
				$this->recal_comments ( $record_id );
			}
			return;
		}
		
		function recal_comments($record_id) {
			$list = $this->get_records ( 'record_id = ' . $record_id, 'fs_products_comments', 'id,published' );
			$total_published = 0;
			foreach ( $list as $item ) {
				if ($item->published == 1) {
					$total_published ++;
				}
			}
			$row ['comments_published'] = $total_published;
			$row ['comments_unread'] = 0;
			$row ['comments_total'] = count ( $list );
			$rs = $this->_update ( $row, 'fs_products', 'id = ' . $record_id . ' ' );
			return $rs;
		}
		function update_unread_for_comments($record_id) {
			$row ['readed'] = 1;
			$this->_update ( $row, 'fs_products_comments', 'record_id = ' . $record_id );
			
			$row2 ['comments_unread'] = 0;
			$this->_update ( $row2, 'fs_products', 'id = ' . $record_id . ' ' );
			return;
		}
		
		function save_comment() {
			$name = FSInput::get ( 'name' );
			$text = FSInput::get ( 'text' );
			$record_id = FSInput::get ( 'record_id', 0, 'int' );
			$parent_id = FSInput::get ( 'parent_id', 0, 'int' );
			if (! $name ||  ! $text || ! $record_id){
				echo 0;
				return false;
			}
				
			$time = date ( 'Y-m-d H:i:s' );	
			$row ['name'] = $name;
			$row ['email'] = $_COOKIE['ad_useremail'];
			$row ['comment'] = $text;
			$row ['record_id'] = $record_id;
			$row ['parent_id'] = $parent_id;
			$row ['published'] = 1;
			$row ['readed'] = 1;
			$row ['created_time'] = $time;
			$row ['edited_time'] = $time;
			$row ['is_admin'] = 1;
			$row ['replied'] = 1;
			
			$rs = $this -> _add($row, 'fs_products_comments');
			if($rs){

				$parent = $this -> get_record('id = '.$parent_id ,'fs_products_comments');
				$product = $this ->  get_record('id = '.$record_id ,'fs_products');

				// Insert vào notify
				$row2 = array();
				$row2['user_id'] = $parent ->user_id;
				$time = date("Y-m-d H:i:s");
				$row2['created_time'] = $time;
				$row2['readers_id'] = '';
				$link = FSRoute::_('index.php?module=products&view=product&code='.$product -> alias.'&id='.$product -> id.'&ccode='.$product->category_alias);
				$row2['message'] = 'Bạn nhận được 1 câu trả lời từ  <a href="'.$link.'" >'.$product -> name.'</a> ';
				$row2['image'] = $product -> image;
				$row2['is_read'] = 0;
				$row2['record_id'] = $rs;
				$row2['type'] = 'comment_reply';
				$row2['action_user_name'] = $_COOKIE['ad_username'];
				$this -> _add($row2, 'fs_notify');
				
				// $this -> mail_to_after_successful($rs);
			}
			echo $rs?1:0;
			if ($rs){
				$row3['replied'] = 1;
				$this -> _update($row3, 'fs_products_comments',' id = '.$parent_id);
			}
			if ($rs)
				$this->recal_comments ( $record_id );
			return $rs;
		}
		function mail_to_after_successful($id){
			if(!$id)
				return;
			global $db;
			
	         
			// get order
			$query = " SELECT * 
						FROM fs_products_comments
						WHERE  id = '$id' 
					";	
			$db -> query($query);
			$data = $db->getObject();
		
				
			if(!$data)
				return;
			
			$parent = $this -> get_record('id = '.$data ->parent_id ,'fs_products_comments');
			$product = $this ->  get_record('id = '.$data ->record_id ,'fs_products');

			// send Mail()
			$mailer = FSFactory::getClass('Email','mail');
			
			$select = 'SELECT * FROM fs_config WHERE published = 1';
			global $db;
			$db -> query($select);
			$config = $db->getObjectListByKey('name');
			$admin_name  = $config['admin_name']-> value;
			$admin_email  = $config['admin_email']-> value;
			
		

			$mailer->SMTPDebug  = 1;   
			$mailer -> isHTML(true);
			$mailer -> setSender(array($admin_email,$admin_name));
			$mailer -> AddBCC('ngocdv@finalstyle.com','dao ngoc');
			$mailer -> AddAddress($parent->email,$parent->name);
			$mailer -> setSubject('Skma.com.vn - Trả lời bình luận'); 
			

			$link = FSRoute::_('index.php?module=products&view=product&code='.$product -> alias.'&id='.$product -> id.'&ccode='.$product->category_alias);

			// body
			$body = '';
			
			

			// order common
			$body .= '<div style="padding: 10px" >Chào bạn '.$parent->name.' </div>';
			$body .= '	<div style="padding: 10px">';

			$body .= '		<div>Câu hỏi của bạn trên bài viết tại  <a href="'.$link.'" >'.$product -> name.'</a>, website <a href="'.URL_ROOT.'" >didongthongminh.vn</a> đã có 1 <span style="color:#ff8b00">Quản trị viên</span> trả lời nội dung như sau:';
			$body .= '	</div>';
			$body .= '<br/>';
			
			// table
			$body .= '<table width="100%" border="1" bordercolor="#DDD" style="border-collapse: collapse; margin-bottom:20px;" cellpadding="10">';
			$body .= 	'<thead style="height: 25px;">';
			$body .= 		'<tr>';
			$body .= 			'<td width="50%">';
			$body .= 				'<strong>Câu hỏi của bạn</strong>';
			$body .= 			'</td>';
			$body .= 			'<td >';
			$body .= 				'<strong>Câu trả lời</strong>';
			$body .= 			'</td>';
			
			$body .= 		'</tr>';
			$body .= 	'</thead>';
			
			$body .= 	'<tbody>';
			$body .= 		'<tr>';
			$body .= 			'<td>';
			$body .= 				$parent -> comment;
			$body .= 			'</td>';
			$body .= 			'<td>';
			$body .= 				$data -> comment;
			$body .= 			'</td>';
			$body .= 		'</tr>';
			$body .= 		'</tbody>';
			$body .= 		'</table>';
			$body .= '<div style="padding: 10px" >Cảm ơn bạn đã quan tâm đến sản phẩm</div>';
			// echo $body ;die;


			$mailer -> setBody($body);
			if(!$mailer ->Send())
				return false;
			return true;
		}
		function edit_comment() {
			$text = FSInput::get ( 'text' );
			$record_id = FSInput::get ( 'record_id', 0, 'int' );
			$comment_id = FSInput::get ( 'comment_id', 0, 'int' );
			$parent_id = FSInput::get ( 'comment_id', 0, 'int' );
			if (!$text || !$record_id){
				echo 0;
				return false;
			}
				
			$time = date ( 'Y-m-d H:i:s' );	
	//		$row ['name'] = $name;
	//		$row ['email'] = $_SESSION['cms_useremail'];
			$row ['comment'] = $text;
			$row ['record_id'] = $record_id;
	//		$row ['parent_id'] = $parent_id;
	//		$row ['published'] = 1;
	//		$row ['readed'] = 1;
	//		$row ['created_time'] = $time;
			$row ['edited_time'] = $time;
			
			$rs = $this -> _update($row, 'fs_products_comments',' id = '.$comment_id);
			echo $rs?1:0;
			if ($rs)
				$this->recal_comments ( $record_id );
			return $rs;
		}

		function get_level(){
			$sql = " SELECT * FROM fs_members_level ";
			global $db ;
			$db->query($sql);
			return $db->getObjectListByKey('level');
		}

		function get_all_user(){

			$query = " SELECT *
						FROM fs_members 
						 ";
			global $db;
			$db -> query($query);
			return $rs = $db->getObjectListByKey('id');
		}
	}
?>
