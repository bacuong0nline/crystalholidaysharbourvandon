<?php TemplateHelper::dt_edit_text(FSText::_('Name'), 'name', @$data->name);
TemplateHelper::dt_edit_selectbox(FSText::_('Chọn Tỉnh/Thành phố'), 'city_id', @$data->province, 0, $dataCity, $field_value = 'id', $field_label = 'name', $size = 10, 0, 1);
?>

<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label"><?php echo FSText::_("Quận/Huyện") ?></label>
    <div class="col-md-9 col-xs-12">
        <select id="district_id" name="district_id" class="form-control ">
            <?php foreach ($district as $item) { ?>
                <option <?php echo ($item->id == @$data->district) ? 'selected="selected"' : ''; ?> value="<?php echo $item->id ?>"><?php echo $item->name ?></option>
            <?php } ?>
        </select>
    </div>
</div>

<?php
TemplateHelper::dt_edit_text(FSText::_('Địa chỉ'), 'address', @$data->address, '', 600, 1, 0, '');
TemplateHelper::dt_edit_text(FSText::_('Code CRM'), 'code_crm', @$data->code_crm);
TemplateHelper::dt_edit_text(FSText::_('Điện thoại'), 'phone', @$data->phone);
TemplateHelper::dt_checkbox(FSText::_('Published'), 'published', @$data->published, 1);
TemplateHelper::dt_edit_text(FSText::_('Ordering'), 'ordering', @$data->ordering,  @$maxOrdering, '20');
TemplateHelper::dt_edit_text(FSText::_('Vĩ độ'), 'latitude', @$data->latitude,  '21.037834051277333');
TemplateHelper::dt_edit_text(FSText::_('Kinh độ'), 'longitude', @$data->longitude,  '105.8411953210175');
TemplateHelper::dt_edit_text(FSText::_('Link map'), 'link', @$data->link, '', 600, 1, 0, '');
?>
<div id="searchBoxContainer">
    <?php
    TemplateHelper::dt_edit_text(FSText::_('Tìm địa điểm'), 'searchBox', '', '', 600, 1, 0, '');

    // TemplateHelper::dt_edit_image(FSText::_('Hình ảnh'), 'image', str_replace('/original/', '/resized/', URL_ROOT . @$data->image));

    ?>
</div>


<div id='printoutPanel'></div>
<div class="form-group">
    <label class="col-md-2 col-xs-12 control-label"><?php echo FSText::_('Bản đồ'); ?></label>
    <div class="col-md-10 col-xs-12">
        <div id="myMap" style="position:relative; width:100%; height:750px; border: 1px solid #fff;"></div>
    </div>
</div>

<style>
    .MicrosoftMap {
        position: absolute;
        left: 25%;
    }
    .as_container_search {
        left: 25% !important;
    }
</style>

<script type="text/javascript" language="javascript">
    $(function() {
        $("select#city_id").change(function() {
            $.ajax({
                type: 'get',
                url: "index.php?module=address&view=address&task=district&raw=1",
                data: {
                    cid: $(this).val()
                },
                dataType: "text",

                success: function(text) {

                    if (text == '')
                        return;
                    j = eval("(" + text + ")");

                    var options = '';
                    for (var i = 0; i < j.length; i++) {
                        options += '<option value="' + j[i].id + '">' + j[i].name + '</option>';
                    }
                    $('#district_id').html(options);
                    elemnent_fisrt = $('#district_id option:first').val();
                }
            });
        });
    })
</script>