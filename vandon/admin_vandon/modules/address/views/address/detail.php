<?php
$latitude = @$data->latitude ? $data->latitude : '10.950166534718377';
$longitude = @$data->longitude ? $data->longitude : '106.83500459295963';
?>
<style>
    #gmap {
        height: 400px;
        margin: 20px 0px;
        width: 100% !important;
    }

    .controls {
        margin-top: 16px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 15px;
        text-overflow: ellipsis;
        width: 400px;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    }

    .pac-container {
        font-family: Roboto;
    }

    #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
    }

    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

    .mapContainer,
    #myMap {
        position: relative;
        width: 100%;
        height: 400px;
    }

    .customNavBar {
        position: absolute;
        top: 10px;
        left: 10px;
    }
</style>

<script type='text/javascript'>
    var map;
    var pinLayer;
    function suggestionSelected(result) {
        //Remove previously selected suggestions from the map.
        map.entities.clear();

        //Show the suggestion as a pushpin and center map over it.
        var pin = new Microsoft.Maps.Pushpin(result.location);
        map.entities.push(pin);

        map.setView({
            bounds: result.bestView
        });
    }

    function GetMap() {
        <?php
        $json2 = '[';
        $json_names_2 = array();

        $json_names_2[] = "['" . @$data->name . "'," . $latitude . "," . $longitude . ",13,'" . @$data->address . "']";

        $json2 .= implode(',', $json_names_2);
        $json2 .= ']';
        ?>
        var locations = <?php echo @$json2 ?>;
        var infoboxLayer = new Microsoft.Maps.EntityCollection();
        pinLayer = new Microsoft.Maps.EntityCollection();
        map = new Microsoft.Maps.Map(document.getElementById("myMap"), {
            credentials: 'AnzmdBqhzTg091I0F_DxiNxtKM2BVXcgOACtIhT_0VMMhQmMTzK0lK9ORmYIfKH-',
            showSearchBar: true
        });
        Microsoft.Maps.loadModule('Microsoft.Maps.AutoSuggest', function () {
            var manager = new Microsoft.Maps.AutosuggestManager({ map: map });
            manager.attachAutosuggest('#searchBox', '#searchBoxContainer', suggestionSelected);
        });
        pinInfobox = new Microsoft.Maps.Infobox(new Microsoft.Maps.Location(0, 0), {
            visible: false
        });
        infoboxLayer.push(pinInfobox);

        var latLon = new Microsoft.Maps.Location(<?php echo $latitude ?>, <?php echo $longitude ?>);

        var pin = new Microsoft.Maps.Pushpin(latLon, {
            // color: '#1D8843',
            title: locations[0][0],
            subTitle: locations[0][4],
            // icon: './images/arrow-up1.png'
        });


        pin.Title = locations[0][0];
        pin.Description = locations[0][4]; //information you want to display in the infobox
        pinLayer.push(pin); //add pushpin to pinLayer
        Microsoft.Maps.Events.addHandler(pin, 'click', displayInfobox);
        Microsoft.Maps.Events.addHandler(map, 'click', clickToMap);


        map.entities.push(pinLayer);
        map.entities.push(infoboxLayer);
        map.setView({
            zoom: 10,
            center: new Microsoft.Maps.Location(<?php echo $latitude ?>, <?php echo $longitude ?>)
        });

        function displayInfobox(e) {
            console.log(e);
            pinInfobox.setOptions({
                title: e.target.Title,
                description: e.target.Description,
                visible: true,
                offset: new Microsoft.Maps.Point(0, 25)
            });
            pinInfobox.setLocation(e.target.getLocation());
        }

        function hideInfobox(e) {
            pinInfobox.setOptions({
                visible: false
            });
        }

        function clickToMap(e) {
            $("#latitude").val(e.location.latitude);
            $("#longitude").val(e.location.longitude);
            var latLon2 = new Microsoft.Maps.Location(e.location.latitude, e.location.longitude);
            var pin2 = new Microsoft.Maps.Pushpin(latLon2, {
                // color: '#1D8843',
                // icon: './images/arrow-up1.png'
            });
            removePin()
            pinLayer.push(pin2); //add pushpin to pinLayer

        }

        function removePin() {
            for (var i = map.entities.getLength() - 1; i >= 0; i--) {
                var pushpin = map.entities.get(i);
                if (pushpin instanceof Microsoft.Maps.Pushpin) {
                    map.entities.removeAt(i);
                }
            }
        }

    }
</script>
<script type='text/javascript' src='https://www.bing.com/api/maps/mapcontrol?callback=GetMap&key=AnzmdBqhzTg091I0F_DxiNxtKM2BVXcgOACtIhT_0VMMhQmMTzK0lK9ORmYIfKH-&CountryRegion=VN' async defer></script>
<link type="text/css" rel="stylesheet" media="all" href="../libraries/jquery/jquery.ui/jquery-ui.css" />
<script type="text/javascript" src="../libraries/jquery/jquery.ui/jquery-ui.js"></script>
<script type="text/javascript" src="../libraries/jquery/jquery.ui/jquery-ui.js"></script>

<!-- FOR TAB -->
<script>
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<?php
$title = @$data ? FSText::_('Edit') : FSText::_('Add');
global $toolbar;
$toolbar->setTitle($title);
$toolbar->addButton('apply', FSText::_('Apply'), '', 'apply.png');
$toolbar->addButton('Save', FSText::_('Save'), '', 'save.png');
$toolbar->addButton('back', FSText::_('Cancel'), '', 'back.png');

$this->dt_form_begin(0);
?>

<?php include_once 'detail_base.php'; ?>

<?php

$this->dt_form_end(@$data, 0);

?>
<script type="text/javascript" src="<?php // echo URL_ROOT.'libraries/jquery/google_map/gg_map.js'
                                    ?>"></script>
<script>

</script>