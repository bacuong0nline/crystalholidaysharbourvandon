<?php
$title = @$data ? FSText::_('Edit') : FSText::_('Add');
global $toolbar;
$toolbar->setTitle($title);
// $toolbar->addButton('save_add',FSText :: _('Save and new'),'','save_add.png',1); 
$toolbar->addButton('apply', FSText::_('Apply'), '', 'apply.png', 1);
$toolbar->addButton('Save', FSText::_('Save'), '', 'save.png', 1);
$toolbar->addButton('back', FSText::_('Cancel'), '', 'cancel.png');

echo '  <div class="alert alert-danger" style="display:none" >
                    <span id="msg_error"></span>
            </div>';

$this->dt_form_begin(1, 4, $title . ' ' . FSText::_('Bài học video'), 'fa-edit', 1, 'col-md-12', 1);
?>
<?php
TemplateHelper::dt_edit_text(FSText::_('Title'), 'title', @$data->title, '', 255, 1, 0, '', '', 'col-sm-3', 'col-sm-9');
TemplateHelper::dt_edit_image(FSText::_('Image'), 'image', URL_ROOT . @$data->image);
TemplateHelper::dt_checkbox(FSText::_('Published'), 'published', @$data->published, 1, '', '', '');
// TemplateHelper::dt_edit_text(FSText::_('Title'), 'Tiêu đề', @$data->title, '', 650, 450, 0, '', '', 'col-sm-3', 'col-sm-9');
$this->dt_form_end_col();

$this->dt_form_end(@$data, 1, 0, 2, 'Cấu hình seo', '', 1, 'col-sm-4');
?>

<script type="text/javascript">
    $('.form-horizontal').keypress(function(e) {
        if (e.which == 13) {
            formValidator();
            return false;
        }
    });

    function formValidator() {
        var $i = $("#check_image").val();
        var $v = $("#check_video").val();

        if ($i == 0) {
            if (!notEmpty('image', 'Bạn phải nhập hình ảnh')) {
                return false;
            }
        }

        $('.alert-danger').hide();

        return true;
    }
</script>