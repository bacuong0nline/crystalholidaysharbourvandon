<?php
class FurnitureModelsCategories extends ModelsCategories
{
	function __construct()
	{

		$this->table_items = FSTable_ad::_('fs_furniture');
		$this->table_name = FSTable_ad::_('fs_furniture_categories');
		$this->check_alias = 1;
		$this->call_update_sitemap = 1;
		$this->table_products = FSTable_ad::_('fs_products');
		$this->field_except_when_duplicate = array(array('list_parents', 'id'), array('alias_wrapper', 'alias'));
		// config for save
		$cyear = date('Y');
		$cmonth = date('m');
		//$cday = date('d');
		$this->img_folder = 'images/planning/cat/' . $cyear . '/' . $cmonth;
		$this->field_img = 'image';
		parent::__construct();
		$this->limit = 100;

		// $this -> array_synchronize = array($this -> table_items=>array('id'=>'category_id','alias'=>'category_alias','name'=>'category_name'
		//                                                                            ,'published'=>'published_cate','alias_wrapper'=>'category_alias_wrapper'));
	}
	function categories($value)
	{
		$ids = FSInput::get('id', array(), 'array');

		if (count($ids)) {
			global $db;
			$str_ids = implode(',', $ids);
			$sql = " UPDATE " . $this->table_name . "
								SET display_category = $value
							WHERE id IN ( $str_ids ) ";
			$rows = $db->affected_rows($sql);
			return $rows;
		}
		// 	update sitemap
		if ($this->call_update_sitemap) {
			$this->call_update_sitemap();
		}

		return 0;
	}

}
