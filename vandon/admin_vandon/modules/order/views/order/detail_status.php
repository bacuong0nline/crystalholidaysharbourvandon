<?php
$array_type = array(
  2 => FSText::_('Chuyển khoản ngân hàng '),
  1 => FSText::_('Giao hàng - nhận tiền (COD)'),
  // 99 => FSText::_('Đơn hàng bị hủy'),
  // 3 => FSText::_('Thanh toán qua VNPAY')
);
$postStatus = array(
  11 => FSText::_('Đặt hàng'),
  // 1 => FSText::_('Xác nhận đặt hàng thành công'),
  // 2 => FSText::_('Hết hàng'),
  // 3 => FSText::_('Đang vận chuyển'),
  4 => FSText::_('Đã giao hàng và thanh toán thành công'),
  5 => FSText::_('Hủy đơn hàng'),
  // 6 => FSText::_('Đã thanh toán qua VNPAY'),
  // 7 => FSText::_('Giao dịch khởi tạo'),
  // 8 => FSText::_('Thanh toán không thành công qua VNPAY')
);

$id = FSInput::get('id');
?>
<div class="table-responsive" style="min-height: 350px">

  <table class="table table-striped">
    <tbody>
      <tr>
        <td>
          <?php
          TemplateHelper::dt_edit_selectbox(FSText::_('Trạng thái đơn hàng'), 'status', @$order->status, 0, $postStatus, $field_value = 'id', $field_label = 'title', $size = 1, 0, 1);
          ?>
        </td>
      </tr>
      <tr>
        <td>
          <?php
          TemplateHelper::dt_edit_text(FSText::_('Ghi chú'), 'note_adc', @$order->note_adc, '', '', 5);
          ?>
        </td>

      </tr>

      <!--		  --><?php //if(!$order->status  ){
                    ?>
      <!--			<tr>-->
      <!--				<td>Hủy đơn hàng: </td>-->
      <!--				<td>-->
      <!--					Bạn hãy click vào <a href="javascript: cancel_order(-->
      <?php //echo $order ->id; 
      ?>
      <!--)" ><strong class='red'> đây</strong></a> nếu bạn muốn <strong> hủy đơn hàng </strong>này-->
      <!--					<br/>-->
      <!--						Chú ý: nếu bạn hủy đơn hàng mà khách hàng đã thanh toán thì hệ thống sẽ trả lại tiền cho họ-->
      <!--				</td>-->
      <!--		  </tr>			  	-->
      <!--		  --><?php //}
                    ?>
      <!--		 --><?php //if($order->status < 1 || !$order->status  ){
                  ?>
      <!--		 	<tr>-->
      <!--				<td>Hoàn tất đơn hàng: </td>-->
      <!--				<td>-->
      <!--					Bạn hãy click vào <a href="javascript: finished_order(-->
      <?php //echo $order ->id; 
      ?>
      <!--)" ><strong class='red'> đây</strong></a> để <strong> hoàn tất</strong> đơn hàng này-->
      <!--					<br/>-->
      <!--						Chú ý: nếu bạn hoàn tất đơn hàng mà khách hàng đã thanh toán thì hệ thống sẽ trả lại tiền cho gian hàng-->
      <!--				</td>-->
      <!--		  </tr>-->
      <!--		 --><?php //}
                  ?>
      <tr>
        <td>
          <div class="col-md-3">
            <?php echo FSText::_('Hình thức thanh toán') ?>
          </div>
          <div class="col-md-9">
            <strong class="red"><?php echo $array_type[$order->payment_method]; ?></strong>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <div class="col-md-3">
            <?php echo FSText::_('Hình thức nhận hàng') ?>
          </div>
          <div class="col-md-9">
            <?php
            if ($order->shipping == 1)
              echo 'Giao hàng';
            else if ($order->shipping == 2)
              echo "Khách hàng đến nhận hàng";
            ?>
          </div>
        </td>
      </tr>
      <!--            <tr>-->
      <!--                <td>-->
      <!--                    <div class="col-md-3">-->
      <!---->
      <!--                    </div>-->
      <!--                    <div class="col-md-9">
                        <a class="btn btn-danger" style="color: #fff" href="javascript:void(0)" onclick="ghtk(<?= $id ?>)">Tạo vận đơn</a>
                    </div>-->
      <!--                </td>-->
      <!--            </tr>-->
      <?php
      if ($order->boc || $order->thi) {
      ?>
        <tr>
          <td>
            <div class="col-md-3">
              <p>Gói quà:</p>
            </div>
            <div class="col-md-9">
              <?php if ($order->boc) echo 'Có';
              else echo 'Không'; ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-3">
              <p>Thiệp mừng:</p>
            </div>
            <div class="col-md-9">
              <?php if ($order->thi) echo 'Có';
              else echo 'Không'; ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-2">
              <p>Gửi từ:</p>
            </div>
            <div class="col-md-4">
              <p><?= $order->from_send ?></p>
            </div>
            <div class="col-md-2">
              <p>Đến:</p>
            </div>
            <div class="col-md-4">
              <p><?= $order->to_send ?></p>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-3">
              <p>Nội dung:</p>
            </div>
            <div class="col-md-9">
              <?= $order->note_send; ?>
            </div>
            <div class="clearfix"></div>
          </td>
        </tr>
      <?php
      }
      ?>

    </tbody>
  </table>
  <!-- ENd TABLE 							-->

</div>
<script>
  function cancel_order(order_id) {
    if (confirm('Bạn có chắc chắn muốn hủy đơn hàng này?')) {
      window.location = 'index.php?module=order&view=order&id=' + order_id + '&task=cancel_order';
    }
  }

  function finished_order(order_id) {
    if (confirm('Bạn có chắc chắn muốn hoàn tất đơn hàng này?')) {
      window.location = 'index.php?module=order&view=order&id=' + order_id + '&task=finished_order';
    }
  }

  function pay_penalty(order_id) {
    if (confirm('Bạn có chắc chắn đã phạt thành viên này?')) {
      window.location = 'index.php?module=order&view=order&id=' + order_id + '&task=pay_penalty';
    }
  }

  function pay_compensation(order_id) {
    if (confirm('Bạn có chắc chắn đã bồi thường cho thành viên này?')) {
      window.location = 'index.php?module=order&view=order&id=' + order_id + '&task=pay_compensation';
    }
  }

  function ghtk(order_id) {
    if (confirm('Bạn có chắc chắn muốn tạo vận đơn?')) {
      window.location = 'index.php?module=order&view=order&id=' + order_id + '&task=ghtk';
    }
  }
</script>