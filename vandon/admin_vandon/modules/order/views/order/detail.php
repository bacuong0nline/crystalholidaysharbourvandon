<!-- HEAD -->
<?php
$title = @$order ? FSText::_('Xem đơn hàng ') . 'DH' . str_pad($order->id, 8, "0", STR_PAD_LEFT) : FSText::_('Add');
global $toolbar;
$toolbar->setTitle($title);
$toolbar->addButton('save', FSText::_('Save'), '', 'save.png');
$toolbar->addButton('apply', FSText::_('Apply'), '', 'apply.png');
$toolbar->addButton('', FSText::_('Print'), '', 'print.png', 0, 1);
$toolbar->addButton('back', FSText::_('Cancel'), '', 'cancel.png');
?>
<!-- END HEAD-->
<style>
	.table-order>td>a {
		color: #333;
	}
</style>
<!-- BODY-->
<form action="index.php?module=<?php echo $this->module; ?>&view=<?php echo $this->view; ?>" name="adminForm" method="post" enctype="multipart/form-data">
	<div class="row">
		<div class="col-lg-6 col-xs-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<i class="fa fa-cog"></i>
					Mã đơn hàng: <strong><?php echo 'DH' . str_pad($order->id, 8, "0", STR_PAD_LEFT); ?></strong>
				</div>
				<div class="panel-body">
					<?php $print = FSInput::get('print', 0, 'int'); ?>
					<?php if (!$print) { ?>
						<?php include_once 'detail_status.php'; ?>
					<?php } ?>
				</div>
			</div>
		</div>

		<!-- <div class="col-lg-6 col-xs-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-cog"></i>
                    <?php //echo FSText::_('Thông tin người nhận hàng') 
					?>
                </div>
                <div class="panel-body">
                    <?php //include_once 'detail_buyer.php'; 
					?>
                </div>
            </div>
        </div> -->
		<?php if ($print && $order->send_gift) {
		} else {
		?>
			<div class="col-lg-6 col-xs-12">
				<div class="panel panel-info">
					<div class="panel-heading">
						<i class="fa fa-cog"></i>
						<?php echo FSText::_('Thông tin người mua hàng') ?>
					</div>
					<div class="panel-body">
						<!--  SENDER INFO -->
						<?php include_once 'detail_recipient.php'; ?>
						<!--  end SENDER INFO -->
					</div>
				</div>
			</div>
		<?php
		}
		?>
	</div>

	<div class="form_body">

	</div>
	<?php
	?>

	<table class="table table-striped table-bordered table-hover table-order">
		<thead>
			<tr>
				<th width="30">STT</th>
				<th width="40%"><?php echo FSText::_('Tên sản phẩm') ?></th>
				<?php if ($print && $order->send_gift) {
				} else {
				?>
					<th><?php echo "Giá(VNĐ)"; ?></th>
				<?php
				}
				?>
				<th><?php echo "Số lượng"; ?></th>
				<?php if ($print && $order->send_gift) {
				} else {
				?>
					<th><?php echo "Tổng giá tiền"; ?></th>
				<?php
				}
				?>
			</tr>
		</thead>
		<tbody>
			<?php
			$total_money = 0;
			$total_discount = 0;
			for ($i = 0; $i < count($data); $i++) {
			?>
				<?php
				$item = $data[$i];
				$link_view_product = FSRoute::_('index.php?module=products&view=product&code=' . $item->product_alias . '&id=' . $item->product_id . '&ccode=' . $item->category_alias . '&Itemid=6');

				$total_price_options = 0;
				$total_money += $item->price * $item->count;

				foreach (unserialize($item->options) as $item2) {
					if (!is_array(@$item2[0])) {
					  $total_price_options += $item2['price'] * $item2['quantity'];
					} else {
					  foreach ($item2 as $val) {
						$total_price_options += $val['price'] * $val['quantity'];
					  }
					}
				}
				$total_money += $total_price_options;
				$total_discount += $item->discount * $item->count;

				$options_display = '';

				$options_array = @unserialize($item->options);
				if (!empty($options_array)) {
					foreach ($options_array as $key => $val) {
						if (empty($val['id'])) {
							foreach ($val as $val2) {
								$price = @$val2['price'] != "0.00" ? format_money_0(@$val2['quantity'] * @$val2['price']) : null;
								$quantity_option = @$val2['quantity'] > 1 ? 'x ' . @$val2['quantity'] . '' : '';
								$options_display .= '<p class="mb-0" style="font-size: 12px; color: #6e6e6e; margin-bottom: 0px">' . @$val2['name_display'] . ' ' . $quantity_option . ' ' . $price . ' </p>';
								$_SESSION['price_options_value'] += $val2['price'] * @$val2['quantity'];
							};
						} else {
							$price = @$val['price'] != "0.00" ? format_money_0(@$val['quantity'] * @$val['price']) : null;
							$quantity_option = @$val['quantity'] > 1 ? 'x ' . @$val['quantity'] . '' : '';
							$options_display .= '<p class="mb-0" style="font-size: 12px; color: #6e6e6e; margin-bottom: 0px">' . @$val['name_display'] . ' ' . $quantity_option . ' ' . $price . ' </p>';
							$_SESSION['price_options_value'] += $val['price'] * @$val['quantity'];
						};
					};
				}
				?>
				<tr class='row<?php echo ($i % 2); ?>'>
					<td align="center"><strong><?php echo ($i + 1); ?></strong><br /></td>
					<td>
						<a href="<?php echo $link_view_product; ?>" target="_blank">
							<?php echo $item->product_name; ?>
						</a>
						<?php echo $options_display ?>
					</td>
					<?php if ($print && $order->send_gift) {
					} else {
					?>
						<!--		PRICE 	-->
						<td>
							<?php if ($item->discount > 0) {
							?>
								<?php echo format_money($item->price, ' VNĐ'); ?>
							<?php } else { ?>
								<?php echo format_money($item->price, ' VNĐ'); ?>
							<?php } ?>
						</td>
					<?php
					}
					?>
					<td>
						<input class="form-control" id="disabledInput" type="text" placeholder="<?php echo FSText::_('Số lượng'); ?>" value="<?php echo $item->count; ?>" disabled="">
					</td>
					<?php if ($print && $order->send_gift) {
					} else {
					?>
						<td>
							<span class='red'>
								<?php if ($item->discount > 0) { ?>
									<?php echo format_money(($item->price * $item->count) + $total_price_options, ' VNĐ'); ?>
								<?php } else { ?>
									<?php echo format_money(($item->price * $item->count) + $total_price_options, ' VNĐ'); ?>
								<?php } ?>
							</span>
						</td>
					<?php
					}
					?>
				</tr>
			<?php } ?>
			<?php if ($print && $order->send_gift) {
			} else {
			?>
				<tr>
					<td colspan="4" align="right">
						<strong>T&#7893;ng ti&#7873;n:</strong>
					</td>
					<td>
						<strong class='red'><?php echo format_money($total_money, ' VNĐ'); ?>
					</td>
				</tr>
				<?php if ($order->discount_code) { ?>
				<tr>
					<td colspan="4" align="right">
						<strong>Mã giảm giá:</strong>
					</td>
						<td>
							<strong class='red'>
								<?php
									$voucher_detail = @$model->get_record_by_id($order->discount_code, 'fs_discount');
									if (@$voucher_detail && @$voucher_detail->unit == 2) {
										$discount = $total_money * @$voucher_detail->discount / 100;
										if (@$discount > @$voucher_detail->maximum) {
											$discount = @$voucher_detail->maximum;
										}
									  } else if (@$voucher_detail && @$voucher_detail->unit == 1) {
										 	$discount = @$voucher_detail->discount;
									  }
									  echo @format_money($discount);
								?>
							</strong>
						</td>
				</tr>
				<?php } ?>

				<tr>
					<td colspan="4" align="right">
						<strong>Phí ship:</strong>
					</td>
					<td>
						<strong class='red'>
							<?php
							if ($order->shipping == 1)
								echo format_money($model->get_record("name = '$order->district'", 'fs_districts')->fee_shipping);
							else if ($order->shipping == 2)
								echo "Miễn phí";
							?>
						</strong>
					</td>
				</tr>
				<tr>
					<td colspan="4" align="right">
						<strong>Thanh toán:</strong>
					</td>
					<td>
						<strong class='red'><?= format_money($order->total_end, ' VNĐ') ?>
						</strong>
					</td>
				</tr>
			<?php
			}
			?>
		</tbody>
	</table>
	<?php if (@$order->id) { ?>
		<input type="hidden" value="<?php echo $order->id; ?>" name="id">
	<?php } ?>
	<input type="hidden" value="<?php echo $this->module; ?>" name="module">
	<input type="hidden" value="<?php echo $this->view; ?>" name="view">
	<input type="hidden" value="" name="task">
	<input type="hidden" value="0" name="boxchecked">
</form>
<!-- end FORM	MAIN - ORDER						-->

<!--  ESTORE INFO -->
<?php // include_once 'detail_estore.php'; 
?>
<!--  end ESTORE INFO -->


<!--  RECIPIENT INFO -->
<?php //include_once 'detail_recipient.php'; 
?>
<!--  end RECIPIENT INFO -->

<?php // include_once 'detail_payment.php'; 
?>
<!-- END BODY-->

<script type="text/javascript" language="javascript">
	print_page();

	function print_page() {
		var width = 800;
		var centerWidth = (window.screen.width - width) / 2;
		//	    var centerHeight = (window.screen.height - windowHeight) / 2;
		$('.Print').click(function() {
			link = window.location.href;
			link += '&print=1';
			window.open(link, "", "width=" + width + ",menubar=0,resizable=1,scrollbars=1,statusbar=0,titlebar=0,toolbar=0',left=" + centerWidth + ",top=0");
		});
	}

	function submitbutton(pressbutton) {
		alert(pressbutton);
		if (pressbutton == 'remove') {
			if (confirm('Bạn có chắc chắn muốn xóa?'))
				submitform(pressbutton);
		} else {
			submitform(pressbutton);
		}
	}

	/**
	 * Submit the admin form
	 */
	function submitform(pressbutton) {
		if (pressbutton == 'export') {
			url_current = window.location.href;
			url_current = url_current.replace('#', '');
			window.open(url_current + '&task=export');
			return;
		}
		alert(pressbutton);
		if (pressbutton) {
			document.adminForm.task.value = pressbutton;
		}
		if (typeof document.adminForm.onsubmit == "function") {
			document.adminForm.onsubmit();
		}

		if (document.adminForm.task.value != 'cancel') {

		}

		document.adminForm.submit();
	}
</script>