<div class="panel panel-primary">
	<div class="panel-body">
		<table class="table table-hover table-striped table-bordered ">
			<thead>
				<tr>
					<th class="title" width="20%" rowspan="2">
						<?php echo FSText::_('Module'); ?>
					</th>
					<th class="title" width="30%" rowspan="2">
						<?php echo FSText::_('Nhóm task vụ'); ?>
					</th>
					<th class="title" width="" colspan="2" style="text-align: center;">
						<?php echo FSText::_('Chức năng'); ?>
					</th>
				</tr>
				<tr>
					<th><?php echo FSText::_('View'); ?> </th>
					<th style="text-align: center;"><?php echo FSText::_('Phân quyền chức năng'); ?></th>
					<!--
				<th><?php //echo FSText :: _('Remove'); 
						?> </th>
                <th><?php //echo FSText :: _('Published'); 
										?> </th> -->
				</tr>

			</thead>
			<tbody>
				<?php foreach ($arr_task as $module_name => $module) :
					//var_dump($module);
				?>
					<tr>
						<td align="left" rowspan="<?php echo (count($module)); ?>">
							<strong><?php echo FSText::_(ucfirst($module_name)); ?></strong>
						</td>
						<?php $k = 0; ?>
						<?php foreach ($module as $view_name => $view) : ?>
							<?php $perm = @$list_permission[$view->id]->permission ? @$list_permission[$view->id]->permission : 0; ?>
							<?php
							$list_permission_user = $model->get_record(' user_id = ' . $data->id . ' AND module = "' . $view->module . '" AND view = "' . $view->view . '" ', 'fs_users_permission_fun');

							//print_r($perm);
							$name_box = "per_";
							$name_box .= $view->id ?  ($view->id) : "0";
							$id_box = $name_box;
							$name_box .= "[]";
							?>

							<?php if ($k) { ?>
					<tr>
					<?php } ?>
					<td><?php echo $view->description ? FSText::_($view->description) : FSText::_(ucfirst($view_name)); ?></td>
					<td>
						<input type="checkbox" class="checkbox-custom" value="3" name="<?php echo $name_box; ?>" <?php echo @$perm >= 3 ? "checked=\"checked\"" : ""; ?> id="<?php echo $id_box . "_v1"; ?>" />
						<label for="<?php echo $id_box . "_v1"; ?>" class="checkbox-custom-label"></label>
					</td>
					<td>
						<span style=" font-size: 24px;text-align: center;color: #57baf9;">
							<a style="font-size: 24px;display: block;text-align: center;color: #57baf9;" href="javascript:void(0)" onclick="load_funciton('<?php echo $view->module ?>','<?php echo $view->view ?>',<?php echo $id; ?>)">
								<i class="fa fa-truck"></i>
								<span style="position: absolute; margin-left: 10px">
									<?php echo @$list_permission_user->list_field && @$view->list_function && !strpos(@$view->list_function, @$list_permission_user->list_field) !== false ?  '<i style="font-size: 13px" class="fa fa-check" aria-hidden="true"></i>' : '<i style="font-size: 13px" class="fa fa-spinner" aria-hidden="true"></i>' ?>
								</span>
							</a>
						</span>
					</td>
					</tr>
					<?php $k++; ?>
				<?php endforeach; ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	function load_content(n_module = '', n_view = '', id) {
		if (!n_module || !n_view || !id)
			return false;
		$("#sv_bt").load("index.php?module=users&view=users&raw=1&task=display_page", {
			"n_module": n_module,
			"n_view": n_view,
			"user_id": id
		}, function() { //get content from PHP page
			//$(".loading-div").hide(); //once done, hide loading element
			$("#myModal").modal();
		});
	}

	function load_funciton(n_module = '', n_view = '', id) {
		if (!n_module || !n_view || !id)
			return false;
		$("#sv_bt").load("index.php?module=users&view=users&raw=1&task=display_page_fun", {
			"n_module": n_module,
			"n_view": n_view,
			"user_id": id
		}, function() { //get content from PHP page
			//$(".loading-div").hide(); //once done, hide loading element
			$("#myModal").modal();
		});
	}
</script>