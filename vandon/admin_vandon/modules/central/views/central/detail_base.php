<?php TemplateHelper::dt_edit_text(FSText:: _('Name'), 'name', @$data->title);
TemplateHelper::dt_edit_selectbox(FSText::_('Chọn Tỉnh/Thành phố'), 'city_id', @$data->province, 0, $dataCity, $field_value = 'id', $field_label = 'name', $size = 10, 0, 1);
?>

<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label"><?php echo FSText::_("Quận/Huyện") ?></label>
    <div class="col-md-9 col-xs-12">
        <select id="district_id" name="district_id" class="form-control ">
            <?php foreach ($district as $item) { ?>
                <option <?php echo ($item->id == @$data->district) ? 'selected="selected"' : ''; ?>
                        value="<?php echo $item->id ?>"><?php echo $item->name ?></option>
            <?php } ?>
        </select>
    </div>
</div>

<?php
// TemplateHelper::dt_edit_text(FSText:: _('Địa chỉ'), 'address', @$data->address, '', 600, 1, 0, '');
// TemplateHelper::dt_edit_text(FSText:: _('Điện thoại'), 'phone', @$data->phone);
TemplateHelper::dt_checkbox(FSText::_('Published'), 'published', @$data->published, 1);
TemplateHelper::dt_edit_text(FSText:: _('Ordering'), 'ordering', @$data->ordering,  @$maxOrdering, '20');
TemplateHelper::dt_edit_image(FSText::_('Hình ảnh'), 'image', str_replace('/original/', '/resized/', URL_ROOT . @$data->image));
TemplateHelper::dt_edit_image2(FSText::_('Image'), 'image', str_replace('/original/', '/resized/', URL_ROOT . @$data->image), '', '', '', 1);
TemplateHelper::dt_edit_text(FSText::_('Giới thiệu trung tâm'), 'more_info', @$data->more_info, '', 650, 450, 1, '', '', 'col-sm-2', 'col-sm-12');

?>




<script type="text/javascript" language="javascript">
    $(function () {
        $("select#city_id").change(function () {
            $.ajax({
                type: 'get',
                url: "index.php?module=address&view=address&task=district&raw=1",
                data: {cid: $(this).val()},
                dataType: "text",

                success: function (text) {

                    if (text == '')
                        return;
                    j = eval("(" + text + ")");

                    var options = '';
                    for (var i = 0; i < j.length; i++) {
                        options += '<option value="' + j[i].id + '">' + j[i].name + '</option>';
                    }
                    $('#district_id').html(options);
                    elemnent_fisrt = $('#district_id option:first').val();
                }
            });
        });
    })

</script>