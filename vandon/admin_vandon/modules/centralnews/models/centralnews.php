<?php

class CentralNewsModelsCentralNews extends FSModels
{
    var $limit;
    var $prefix;

    function __construct()
    {
        $this->limit = 50;
        $this->view = 'centralnews';
        $this->table_name = FSTable_ad::_('fs_central_news');
        $this->table_name_categories = FSTable_ad::_('fs_central');

        $this->arr_img_paths = array(array('resized', 0, 0, 'resize_image'), array('large', 880, 487, 'cut_image'));

        // config for save
        $cyear = date('Y');
        $cmonth = date('m');
        $cday = date('d');
        $this->img_folder = 'images/central_news/' . $cyear . '/' . $cmonth . '/' . $cday;


        parent::__construct();
    }

    function setQuery()
    {

        // ordering
        $ordering = "";
        $where = "  ";
        if (isset($_SESSION[$this->prefix . 'sort_field'])) {
            $sort_field = $_SESSION[$this->prefix . 'sort_field'];
            $sort_direct = $_SESSION[$this->prefix . 'sort_direct'];
            $sort_direct = $sort_direct ? $sort_direct : 'asc';
            $ordering = '';
            if ($sort_field)
                $ordering .= " ORDER BY $sort_field $sort_direct, created_time DESC, id DESC ";
        }
        if (!$ordering)
            $ordering .= " ORDER BY created_time DESC , id DESC ";


        if (isset($_SESSION[$this->prefix . 'keysearch'])) {
            if ($_SESSION[$this->prefix . 'keysearch']) {
                $keysearch = $_SESSION[$this->prefix . 'keysearch'];
                $where .= " AND a.name LIKE '%" . $keysearch . "%' ";
            }
        }
        if(isset($_SESSION[$this -> prefix.'filter0'])){
            $filter = $_SESSION[$this -> prefix.'filter0'];
            if($filter){
                $where .= ' AND a.category_id = '.$filter.' ';
            }
        }
        $query = " SELECT a.*
						  FROM 
						  	" . $this->table_name . " AS a
						  	WHERE 1=1 " .
            $where .
            $ordering . " ";
        return $query;
    }

    function remove()
    {
        $img_paths = array();
        $path_original = PATH_IMG_ADDRESS . 'original' . DS;
        $path_resize = PATH_IMG_ADDRESS . 'resized' . DS; //142x100
        $path_large = PATH_IMG_ADDRESS . 'large' . DS; //309x219
        $img_paths[] = $path_original;
        $img_paths[] = $path_resize;
        $img_paths[] = $path_large;
        return parent::remove('image', $img_paths);
    }

    /*
     * value: == 1 :hot
     * value  == 0 :unhot
     * published record
     */
    function save($row = array(), $use = 1) {
        $category_id = FSInput::get('category_id',0,'int') ? FSInput::get('category_id',0,'int') : 30;
        if(!$category_id){
            Errors::_('Bạn phải chọn danh mục');
            return;
        }
        $cat =  $this->get_record_by_id($category_id,$this -> table_name_categories);
        $row['category_id_wrapper'] = $cat -> list_parents;
        $row['category_alias_wrapper'] = $cat -> alias_wrapper;
        $row['category_name'] = $cat -> title;
        $row['category_id'] = $category_id;
        $row['category_alias'] = $cat -> alias;
        $row['created_time'] = date('Y-m-d H:i:s');
        $rs = parent::save($row);

        return $rs;
    }
    function hot($value)
    {
        $ids = FSInput::get('id', array(), 'array');

        if (count($ids)) {
            global $db;
            $str_ids = implode(',', $ids);
            $sql = " UPDATE " . $this->table_name . "
							SET is_hot = $value
						WHERE id IN ( $str_ids ) ";
            $db->query($sql);
            $rows = $db->affected_rows();
            return $rows;
        }
        return 0;
    }
    function save_central_images($record_id)
    {
        $this->_update(array(
            'record_id' => $record_id
        ), 'fs_central_images', 'session_id=\'' . session_id() . '\'');
    }


    function get_showroom_images($address_id)
    {
        if (!$address_id)
            return;
        $query = " SELECT image,id 
						FROM fs_showroom_images
						WHERE address_id = $address_id";
        global $db;
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    function remove_other_images($add_id)
    {
        if (!$add_id)
            return true;
        $other_images_remove = FSInput::get('other_image', array(), 'array');
        $str_other_images = implode(',', $other_images_remove);
        if ($str_other_images) {
            global $db;

            // remove images in folder contain these images
            $query = " SELECT image 
						FROM fs_showroom_images
						WHERE address_id = $add_id
						AND id IN ($str_other_images)
						";
            $sql = $db->query($query);
            $images_need_remove = $db->getObjectList();

            $fsFile = FSFactory::getClass('FsFiles', '');
            foreach ($images_need_remove as $item) {
                if ($item->image) {
                    $fsFile->remove($item->image, PATH_IMG_ADDRESS . 'original' . DS);
                    $fsFile->remove($item->image, PATH_IMG_ADDRESS . 'resized' . DS);
                    $fsFile->remove($item->image, PATH_IMG_ADDRESS . 'large' . DS);
                    $fsFile->remove($item->image, PATH_IMG_ADDRESS . 'medium' . DS);
                    $fsFile->remove($item->image, PATH_IMG_ADDRESS . 'small' . DS);

                }
            }

            // remove in database
            $sql = " DELETE FROM fs_showroom_images
						WHERE address_id = $add_id
							AND id IN ($str_other_images)";
            $db->query($sql);
            $rows = $db->affected_rows();
            return $rows;
        }
        return true;
    }

    function getListDistricts($city_id = 0)
    {
        global $db;
        $sqlWhere = '';
        if ($city_id)
            $sqlWhere = ' AND city_id = "' . $city_id . '"';
        $query = '  SELECT id, name
						FROM fs_local_districts 
						WHERE published = 1 ' . $sqlWhere . '
						ORDER BY ordering ASC';
//                        echo $query;die;
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    function get_categories_tree2()
    {
        global $db;
        $query = " SELECT *
						  FROM 
						  	fs_local_cities AS a where 1=1 order by a.ordering asc
						  	 ";
        //	echo $query;
        $sql = $db->query($query);
        $result = $db->getObjectList();
        $tree = FSFactory::getClass('tree', 'tree/');
        $list = $tree->indentRows2($result);
        return $result;
    }

    function get_city()
    {
        global $db;
        $query = "SELECT a.*
                      FROM fs_cities
                      AS a 
                      ORDER BY ordering asc ";
        $sql = $db->query($query);
        $list = $db->getObjectList();
        return $list;
    }


    function getDistricts($cityid = '1473')
    {
        if (!$cityid)
            $cityid = '1473';
        global $db;
        $sql = " SELECT id, name FROM fs_districts
					WHERE city_id = $cityid ";
        $db->query($sql);
        return $db->getObjectList();
    }


    function get_categories_tree()
    {
        global $db;
        $query = " SELECT name,id,city_id
						  FROM 
						  	fs_districts AS a where 1=1 order by a.ordering asc
						  	 ";
        //	echo $query;
        $sql = $db->query($query);
        $result = $db->getObjectList();
        $tree = FSFactory::getClass('tree', 'tree/');
        $list = $tree->indentRows2($result);
        return $result;
    }

    function remove_days($record_id)
    {
        if (!$record_id)
            return true;
        $other_days_remove = FSInput::get('other_days', array(), 'array');
        $str_other_days = implode(',', $other_days_remove);
        if ($str_other_days) {
            global $db;

            // remove in database
            $sql = " DELETE FROM fs_address_other
    							WHERE record_id = $record_id AND id IN ($str_other_days)";
            $db->query($sql);
            $rows = $db->affected_rows();
            return $rows;
        }
        return true;
    }

    function save_exist_days($id)
    {
        global $db;
        // EXIST FIELD
        $days_exist_total = FSInput::get('days_exist_total');

        $sql_alter = "";
        $arr_sql_alter = array();
        $rs = 0;
        for ($i = 0; $i < $days_exist_total; $i++) {
            $id_days_exist = FSInput::get('id_days_exist_' . $i);
            $days_name = mysql_real_escape_string(FSInput::get('days_name_exist_' . $i));
            $row = array();
            $row ['source'] = $days_name;
//                    if($i==1){
//                        echo $id_days_exist;die;
//                    }
            $u = $this->_update($row, 'fs_address_other', ' id=' . $id_days_exist);
            if ($u)
                $rs++;
        }
        return $rs;
    }

    function save_new_days($record_id)
    {
        //    die;
        global $db;
        for ($i = 0; $i < 20; $i++) {
            $row = array();
            $row ['source'] = mysql_real_escape_string(FSInput::get('new_days_name_' . $i));

            if (!$row ['source']) {
                continue;
            }

            $row ['record_id'] = $record_id;
            $rs = $this->_add($row, 'fs_address_other', 1);
        }
        return true;
    }
    function get_news_categories_tree()
    {
        global $db;
        $sql = " SELECT id, title, parent_id AS parent_id 
                FROM $this->table_name_categories WHERE published = 1";
        $db->query($sql);
        $categories = $db->getObjectList();
        // print_r($sql);die;
        // $tree = FSFactory::getClass('tree', 'tree/');
        // $rs = $tree->indentRows($categories, 1);
        return $categories;
    }
    function get_days($tours_id)
    {
        return $this->get_records('record_id = ' . $tours_id, 'fs_address_other');
    }
}

?>