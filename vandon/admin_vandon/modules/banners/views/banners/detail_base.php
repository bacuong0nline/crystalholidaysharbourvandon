<?php
TemplateHelper::dt_edit_text(FSText::_('Tên Banner'), 'name', @$data->name);

?>
<!-- <div class="form-group">
        <label class="col-sm-3 col-xs-12 control-label"><?php echo FSText::_('Loại banner'); ?></label>
		<div class="col-sm-9 col-xs-12">
			<select class="form-control chosen-select" name="type" id="type" >
				<?php
				// selected category
				$cat_compare  = 0;
				if (@$data->type) {
					$cat_compare = $data->type;
				}
				$i = 0;
				foreach ($array_type as $key => $name) {
					$checked = "";
					if (!$cat_compare && !$i) {
						$checked = "selected=\"selected\"";
					} else {
						if ($cat_compare == $key)
							$checked = "selected=\"selected\"";
					}

				?>
					<option value="<?php echo $key; ?>" <?php echo $checked; ?> ><?php echo $name;  ?> </option>	
				<?php
					$i++;
				} ?> 
			</select>
		</div>
	</div> -->

<?php
TemplateHelper::dt_edit_selectbox(FSText::_('Danh mục banner'), 'category_id', @$data->category_id, 0, $categories, $field_value = 'id', $field_label = 'name', $size = 1, 0);
// TemplateHelper::dt_edit_image(FSText::_('Image'), 'image', URL_ROOT . @$data->image, '', '', FSText::_(''));
?>
<!-- <div class="col-md-3"></div>
<div class="col-md-9" style="margin-bottom: 10px">
	<img class="image" src="" alt="" style="width: 100px; height: 100px" />
</div> -->
<?php
TemplateHelper::dt_edit_choose_file(FSText::_('Image'), 'image', @$data->image, $comment = '', $class_col1 = 'col-md-3', $class_col2 = 'col-md-9');

?>
<div class="test">
	<?php
	TemplateHelper::dt_edit_text(FSText::_('Tiêu đề mô tả'), 'title_banner', @$data->title_banner);
	TemplateHelper::dt_edit_text(FSText::_('Mô tả'), 'content_banner', @$data->content_banner);
	?>
</div>

<div class="test2">
	<?php
	TemplateHelper::dt_edit_text(FSText::_('Mô tả chi tiết'), 'description', @$data->description, '', 650, 450, 1, '', '', 'col-sm-3', 'col-sm-9');
?>
</div>
<?php
TemplateHelper::dt_checkbox(FSText::_('Published'), 'published', @$data->published, 1);
TemplateHelper::dt_edit_text(FSText::_('Ordering'), 'ordering', @$data->ordering, @$maxOrdering, '20');

?>
<script type="text/javascript">
	$(document).ready(function() {
		if ($("#category_id").val() == 9) {
			$(".test").hide();
			$(".test2").show()
		}
		if ($("#category_id").val() != 9) {
			$(".test2").hide()

		}
		$("#category_id").change(function() {
			if ($(this).val() == 9) {
				$(".test").hide();
				$(".test2").show()
			} else {
				$(".test").show();
				$(".test2").hide()

			}
		})
		$("#image").change(function() {
			$(".image").attr("src", $(this).val())
		})

		$("#set-width").hide()
		$('#check_none').click(function() {
			$('.listItem option').each(function() {
				$(this).attr('selected', '');
			});
			$('.listItem').attr('disabled', 'disabled');
			$(".listItem").trigger("chosen:updated");
		});
		$('#check_all').click(function() {
			$('.listItem option').each(function() {
				$(this).attr('selected', 'selected');
			});
			$('.listItem').attr('disabled', 'disabled');
			$(".listItem").trigger("chosen:updated");
		});
		$('#check_select').click(function() {
			$('.listItem').removeAttr('disabled');
			$(".listItem").trigger("chosen:updated");
		});

	});
</script>

<style>
	.test {}
</style>