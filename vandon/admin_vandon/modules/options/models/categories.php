<?php
class OptionsModelsCategories extends ModelsCategories
{
	function __construct()
	{

		$this->table_name = FSTable_ad::_('fs_products_options');
		$this->table_values = FSTable_ad::_('fs_products_values');
		$this->check_alias = 1;
		$this->call_update_sitemap = 1;
		$this->field_except_when_duplicate = array(array('list_parents', 'id'), array('alias_wrapper', 'alias'));
		$cyear = date('Y');
		$cmonth = date('m');
		$this->img_folder = 'images/options/cat/' . $cyear . '/' . $cmonth;
		$this->field_img = 'image';
		parent::__construct();
		$this->limit = 100;
	}

	function get_options()
	{
		$query   = ' SELECT *
    					FROM ' . $this->table_name . '
    					';
		global $db;
		$sql = $db->query($query);
		$result = $db->getObjectList();
		return $result;
	}
	function get_value()
	{
		$id = FSInput::get('id');
		$query   = ' SELECT *
    					FROM ' . $this->table_values . ' WHERE parent_id = ' . $id . '';
		global $db;
		$sql = $db->query($query);
		$result = $db->getObjectList();
		return $result;
	}
	function save($row = array(), $use_mysql_real_escape_string = 1)
	{
		$count = FSInput::get("count-input");
		$id = FSInput::get('id', 0, 'int');

		$rs = parent::save($row, 1);

		if (!$rs) {
			Errors::setError('Not save');
			return false;
		} else {
			for ($i = 0; $i < $count; $i++) {
				$order_input = "value-" . $i . "";
				$price_input = "price-" . $i . "";

				$row['name'] = FSInput::get($order_input);
				$row['parent_id'] = $rs;
				$row['created_time'] = date('Y-m-d H:i:s');
				$row['price'] =  FSInput::get($price_input);

				$this->_add($row, 'fs_products_values');
			}
		}
		return $rs;
	}
}
