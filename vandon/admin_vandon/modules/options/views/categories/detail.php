<link type="text/css" rel="stylesheet" media="all" href="../libraries/jquery/jquery.ui/jquery-ui.css" />
<script type="text/javascript" src="../libraries/jquery/jquery.ui/jquery-ui.js"></script>
<!-- HEAD -->
<?php

$title = @$data ? FSText::_('Edit') : FSText::_('Add');
global $toolbar;
$toolbar->setTitle($title);
$toolbar->addButton('save_add', FSText::_('Save and new'), '', 'save_add.png', 1);
$toolbar->addButton('apply', FSText::_('Apply'), '', 'apply.png', 1);
$toolbar->addButton('save', FSText::_('Save'), '', 'save.png', 1);
$toolbar->addButton('cancel', FSText::_('Cancel'), '', 'cancel.png');

echo ' 	<div class="alert alert-danger" style="display:none" >
                    <span id="msg_error"></span>
            </div>';

$this->dt_form_begin(1, 4, $title . ' ' . FSText::_('Lựa chọn'), 'fa-edit', 1, 'col-md-8', 1);
TemplateHelper::dt_edit_text(FSText::_('Tên'), 'name', @$data->name);
TemplateHelper::dt_checkbox(FSText::_('Published'), 'published', @$data->published, 1);
TemplateHelper::dt_checkbox(FSText::_('Chọn nhiều'), 'multiple', @$data->multiple, 0);
TemplateHelper::dt_edit_text(FSText::_('Ordering'), 'ordering', @$data->ordering, @$maxOrdering, '20');

$this->dt_form_begin(1, 2, FSText::_('Lựa chọn'), 'fa-edit', 1, 'col-md-12 fl-left');

?>
<input type="hidden" name="count-input" id="count-input" value="" />
<input type="hidden" name="id" value="<?php echo FSInput::get("id") ?>" />

<table id="table-value">
	<tr>
		<th>Tên</th>
	</tr>
	<?php if (!empty($value)) { ?>
		<?php foreach ($value as $item) { ?>
			<tr>
				<td style="display: flex; justify-content: space-between">
					<span><?php echo $item->name ?></span> - <span><?php echo format_money($item->price) ?></span>
					<span data-id="<?php echo $item->id ?>" class="delete-btn btn-danger" style="margin-top: 10px; padding: 8px; line-height: 6px; border-radius: 5px">
						Xóa
						<i class="fa fa-times" aria-hidden="true"></i>
					</span>
				</td>
			</tr>
		<?php } ?>
	<?php } ?>
</table>
<span class="btn add-more btn-primary" style="margin-top: 10px">
	Thêm lựa chọn
	<i class="fa fa-plus-square" aria-hidden="true"></i>
</span>
<?php
$this->dt_form_end_col();

$this->dt_form_end(@$data, 0, 0);

?>

<script type="text/javascript">
	$('.form-horizontal').keypress(function(e) {
		if (e.which == 13) {
			formValidator();
			return false;
		}
	});

	function formValidator() {
		$('.alert-danger').show();

		if (!notEmpty('name', 'Bạn phải nhập tên danh mục'))
			return false;

		$('.alert-danger').hide();
		return true;
	}
	let order = 0;

	$(".add-more").click(function() {
		$row = `
		<tr> 
			<td>
				<input class="input-value" placeholder="Tên" name="value-${order}" value="" />
				<input class="input-value" placeholder="Giá" name="price-${order}" value="" />
			</td>
		</tr>
		`
		$("#table-value").append($row);
		order++;

		$("#count-input").val(order);
	})

	$(".delete-btn").click(function() {
		let id = $(this).attr('data-id');
		$.ajax({
			method: 'GET',
			processData: false,
			contentType: false,
			url: `index.php?module=options&view=categories&task=delete_option&raw=1&id=${id}`,
			success: function(result) {
				if (result) {
					window.location.reload();
				}
			}
		})
	})
</script>

<style>
	table {
		font-family: arial, sans-serif;
		border-collapse: collapse;
		width: 100%;
	}

	td,
	th {
		border: 1px solid #dddddd;
		text-align: left;
		padding: 8px;
	}

	tr:nth-child(even) {
		background-color: #dddddd;
	}

	.input-value {
		width: 100%;
		padding: 5px;
		border: 1px solid #ccc;
	}

	.input-value:focus {
		outline: none;
	}
</style>