<?php
class OptionsControllersCategories extends ControllersCategories
{
	function __construct()
	{
		$this->view = 'categories';
		parent::__construct();
	}
	function display()
	{

		$sort_field = @$this->sort_field;
		$sort_direct  = @$this->sort_direct;
		$pagination = $this->model->getPagination('');
		$list = $this->model->get_options();
		include 'modules/' . $this->module . '/views/' . $this->view . '/list.php';
	}

	function edit()
	{
		$model =  $this->model;
		$value = $this->model->get_value();
		$id = FSInput::get("id");
		$data = $model->get_record_by_id($id);
		include 'modules/' . $this->module . '/views/' . $this->view . '/detail.php';
	}
	function delete_option()
	{
		$id = FSInput::get('id');

		$sql = " DELETE FROM fs_products_values 
		WHERE id IN ( $id ) ";

		global $db;
		$rows = $db->affected_rows($sql);
		echo $rows;
	}
}
