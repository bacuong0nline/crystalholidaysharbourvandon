<!-- HEAD -->
<?php
$fstring = FSFactory::getClass('FSString', '', '../');
$code = $fstring->generateRandomString(8);
$title = @$data ? FSText::_('Edit') : FSText::_('Add');
global $toolbar;
$toolbar->setTitle($title);
$toolbar->addButton('apply', FSText::_('Apply'), '', 'apply.png');
$toolbar->addButton('save', FSText::_('Save'), '', 'save.png');
$toolbar->addButton('cancel', FSText::_('Cancel'), '', 'cancel.png');

$this->dt_form_begin();

TemplateHelper::dt_edit_text(FSText::_('Name'), 'name', @$data->name);
if (@$data)
	TemplateHelper::dt_edit_text(FSText::_('Mã giảm giá'), 'code', $data->code, '', 255, 1, 0, '', '', 'col-md-3', 'col-md-9', 1);
else
	TemplateHelper::dt_edit_text(FSText::_('Mã giảm giá'), 'code', $code);

TemplateHelper::dt_edit_image(FSText::_('Hình ảnh'), 'image', @$data->image);
TemplateHelper::dt_edit_text(FSText::_('Giới hạn'), 'limit_used', @$data->limit_used, 100, '20', 1, 0, 'Số lượt sử dụng còn lại');
TemplateHelper::dt_edit_text(FSText::_('Mức giảm'), 'discount', @$data->discount, 10, '20');
TemplateHelper::dt_edit_selectbox(FSText::_('Loại giảm giá'), 'unit', @$data->unit, 0, array(1 => 'VNĐ', 2 => 'Phần trăm'), $field_value = 'id', $field_label = '');
TemplateHelper::dt_edit_text(FSText::_('Giảm tối đa (VNĐ)'), 'maximum', @$data->maximum, 10, '20', 1, 0, 'Áp dụng cho loại giảm giá (%)');
TemplateHelper::dt_edit_text(FSText::_('Đơn hàng'), 'order_price', @$data->order_price, 0, '20', 1, 0, 'Số tiền đơn hàng tối thiểu để áp dụng mã giảm giá');

TemplateHelper::datetimepicke(FSText::_('Từ ngày'), 'created_time', @$data->created_time ? @$data->created_time : date('Y-m-d H:i:s'), 60, '20');
TemplateHelper::datetimepicke(FSText::_('Đến ngày'), 'expiration_date', @$data->expiration_date ? @$data->expiration_date : date('Y-m-d H:i:s'), 60, '20');
TemplateHelper::dt_edit_text('Mô tả', 'summary', @$data->summary, '', 100, 5, 0, '', '', 'col-sm-3', 'col-sm-9');

TemplateHelper::dt_edit_text(FSText::_('Ordering'), 'ordering', @$data->ordering ? @$data->ordering : '1', @$maxOrdering, '20');
TemplateHelper::dt_checkbox(FSText::_('Published'), 'published', @$data->published, 1);
TemplateHelper::dt_checkbox(FSText::_('Hiển thị'), 'show_in_homepage', @$data->show_in_homepage, 0);

TemplateHelper::dt_checkbox(FSText::_('Áp dụng cho'), 'choose_type', @$data->choose_type, 1, $array_value = array(1 => 'Danh mục khuyến mại', 2 => 'Sản phẩm'));

?>

<div class="category-promotion">
<?php
TemplateHelper::dt_edit_selectbox(FSText::_("Danh mục khuyến mại"), 'category_promotion', @$data->category_promotion, 0, @$category_promotion, 'id', 'name', 1, 0, 0, '', '');
?>
</div>

<?php
$this->dt_form_begin(0, 1, '', 'fa-edit', 1, 'col-md-12 products_related', 1);
?>


<div class="products_related">
	<p style="font-weight: bold">Chọn sản phẩm</p>
	<div class="row">
		<div class="col-xs-12">
			<div class='products_related_search row'>
				<div class="row-item col-xs-6" style="margin-bottom: 20px;">
					<select class="form-control chosen-select" name="products_related_category_id" id="products_related_category_id">
						<option value="">Danh mục</option>
						<?php
						foreach ($categories as $item) {
						?>
							<option value="<?php echo $item->id; ?>"><?php echo $item->treename;  ?> </option>
						<?php } ?>
					</select>
				</div>
				<div class="row-item col-xs-6">
					<div class="input-group custom-search-form">
						<input type="text" placeholder="Tìm kiếm tên sản phẩm" name='products_related_keyword' class="form-control" value='' id='products_related_keyword' />
						<span class="input-group-btn">
							<a id='products_related_search' class="btn btn-default">
								<i class="fa fa-search"></i>
							</a>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-6">
			<div class='title-related'>Danh sách sản phẩm</div>
			<div id='products_related_l'>
				<div id='products_related_search_list'></div>
			</div>
		</div>

		<div class="col-xs-12 col-md-6">
			<div id='products_related_r'>
				<!--	LIST RELATE			-->
				<div class='title-related'>Sản phẩm khuyến mại</div>
				<ul id='products_sortable_related'>
					<?php
					$i = 0;
					if (isset($products_related))
						foreach ($products_related as $item) {
					?>
						<li id='products_record_related_<?php echo $item->id ?>'><?php echo $item->name; ?>
							<a class='products_remove_relate_bt' onclick="javascript: remove_products_related(<?php echo $item->id ?>)" href="javascript: void(0)" title='Xóa'>
								<img border="0" alt="Remove" src="templates/default/images/toolbar/remove_2.png">
							</a>
							<br />
							<img width="80" src="<?php echo URL_ROOT . str_replace('/original/', '/resized/', $item->image); ?>">
							<input type="hidden" name='products_record_related[]' value="<?php echo $item->id; ?>" />
						</li>
					<?php } ?>
				</ul>
				<!--	end LIST RELATE			-->
				<div id='products_record_related_continue'></div>
			</div>
		</div>

	</div>
</div>

<?php
$this->dt_form_end_col();

$this->dt_form_end(@$data, 1, 0);

?>

<style>
	table {
		font-family: arial, sans-serif;
		border-collapse: collapse;
		width: 100%;
	}

	td,
	th {
		border: 1px solid #dddddd;
		text-align: left;
		padding: 8px;
	}

	tr:nth-child(even) {
		background-color: #dddddd;
	}

	.input-value {
		width: 100%;
		padding: 5px;
		border: 1px solid #ccc;
	}

	.input-value:focus {
		outline: none;
	}

	#products_related_r {
		max-height: 400px;
		overflow: auto;
	}
</style>

<script>
	$(document).ready(function() {
		if ($("#unit").val() == 2) {
			$("#maximum").css("display", "block")
		} else if ($("#unit").val() == 1) {
			$("#maximum").css("display", "none")
		}
		$("#unit").change(function() {
			$("#maximum").toggle()
			$("#maximum").val("");
		})
		if($("input[name=choose_type]:checked").val() == 1) {
			$(".category-promotion").show();
			$(".products_related").hide();
			// $("input[name=products_record_related]").val([])
		} else if($("input[name=choose_type]:checked").val() == 2) {
			$("#category_promotion").val(0);
			$(".category-promotion").hide();
			$(".products_related").show();
		}
		$("input[name=choose_type]").change(function() {
			if($(this).val() == 1) {
			$(".category-promotion").show();
			$(".products_related").hide();
		} else if($(this).val() == 2) {
			$(".category-promotion").hide();
			$(".products_related").show();
		}
		})
	})
</script>

<script type="text/javascript">
	//search_products_related();
	// $("#products_sortable_related").sortable();

	function products_add_related() {
		$('#products_related_l').show();
		$('#products_related_l').attr('width', '50%');
		$('#products_related_r').attr('width', '50%');
		$('.products_close_related').show();
		$('.products_add_related').hide();
	}

	function products_close_related() {
		$('#products_related_l').hide();
		$('#products_related_l').attr('width', '0%');
		$('#products_related_r').attr('width', '100%');
		$('.products_add_related').show();
		$('.products_close_related').hide();
	}
	//function search_products_related(){
	$("#products_related_category_id").change(function() {
		var category_id = $('#products_related_category_id').val();
		var product_id = <?php echo @$data->id ? $data->id : 0 ?>;
		var str_exist = '';
		products_related(keyword = null, category_id, product_id, str_exist)
	})
	$('#products_related_search').on('click', function() {
		var keyword = $('#products_related_keyword').val();
		if (keyword == '' || keyword == null) {
			alert('Bạn chưa nhập từ khóa tìm kiếm');
			return
		}
		var category_id = $('#products_related_category_id').val();
		var product_id = <?php echo @$data->id ? $data->id : 0 ?>;
		var str_exist = '';
		products_related(keyword, category_id = null, product_id, str_exist);
	});
	$('#products_related_keyword').on('keyup', function(e) {
		if (e.key === 'Enter' || e.keyCode === 13) {
			var keyword = $('#products_related_keyword').val();
			if (keyword == '' || keyword == null) {
				alert('Bạn chưa nhập từ khóa tìm kiếm');
				return
			}
			var category_id = $('#products_related_category_id').val();
			var product_id = <?php echo @$data->id ? $data->id : 0 ?>;
			var str_exist = '';
			products_related(keyword, category_id = null, product_id, str_exist);
		}
	});
	//}
	function products_related(keyword, category_id, product_id, str_exist) {

		$("#products_sortable_related li input").each(function(index) {
			if (str_exist != '')
				str_exist += ',';
			str_exist += $(this).val();
		});
		$.get("index2.php?module=products&view=products&task=ajax_get_products_related&raw=1", {
			product_id: product_id,
			keyword: keyword,
			category_id: category_id,
			str_exist: str_exist
		}, function(html) {
			$('#products_related_search_list').html(html);
		});
	}

	function set_products_related(id) {
		var max_related = 100;
		var length_children = $("#products_sortable_related li").length;
		if (length_children >= max_related) {
			alert('Tối đa chỉ có ' + max_related + ' sản phẩm liên quan');
			return;
		}
		var title = $('.products_related_item_' + id).html();
		var html = '<li id="products_record_related_' + id + '">' + title + '<input type="hidden" name="products_record_related[]" value="' + id + '" />';
		html += '<a class="products_remove_relate_bt"  onclick="javascript: remove_products_related(' + id + ')" href="javascript: void(0)" title="Xóa"><img border="0" alt="Remove" src="templates/default/images/toolbar/remove_2.png"></a>';
		html += '</li>';
		$('#products_sortable_related').append(html);
		$('.products_related_item_' + id).hide();
	}

	function remove_products_related(id) {
		$('#products_record_related_' + id).remove();
		$('.products_related_item_' + id).show().addClass('red');
	}
</script>

<style>
	.title-related {
		background: none repeat scroll 0 0 #F0F1F5;
		font-weight: bold;
		margin-bottom: 4px;
		padding: 2px 0 4px;
		text-align: center;
		width: 100%;
	}

	#products_related_search_list {
		height: 400px;
		overflow: scroll;
	}

	.products_related_item {
		background: url("/admin/images/page_next.gif") no-repeat scroll right center transparent;
		border-bottom: 1px solid #EEEEEE;
		cursor: pointer;
		margin: 2px 10px;
		padding: 5px;
	}

	#products_sortable_related li {
		cursor: move;
		list-style: decimal outside none;
		margin-bottom: 8px;
	}

	.products_remove_relate_bt {
		padding-left: 10px;
	}

	.products_related table {
		margin-bottom: 5px;
	}
</style>