<?php 
	class DiscountModelsDiscount extends FSModels
	{
		function __construct()
		{
			$this -> limit = 20;
			
			$this -> table_name = 'fs_discount';
			$this -> table_categories = 'fs_products_categories';
			$this -> category_promotion = 'fs_promotion_type_categories';

			$this -> check_alias = 0;
			$this -> arr_img_paths = array(
				array('lange',632,423,'cut_image'),
				array('resized',359,240,'cut_image'),
				array('small',112,75,'cut_image')
			);
			parent::__construct();
		}
		function setQuery(){
			
			// ordering
			$ordering = "";
			$where = "  ";
			if(isset($_SESSION[$this -> prefix.'sort_field']))
			{
				$sort_field = $_SESSION[$this -> prefix.'sort_field'];
				$sort_direct = $_SESSION[$this -> prefix.'sort_direct'];
				$sort_direct = $sort_direct?$sort_direct:'asc';
				$ordering = '';
				if($sort_field)
					$ordering .= " ORDER BY $sort_field $sort_direct, created_time DESC, id DESC ";
			}
			if(!$ordering)
				$ordering .= " ORDER BY ordering DESC , id DESC ";
			
			
			if(isset($_SESSION[$this -> prefix.'keysearch'] ))
			{
				if($_SESSION[$this -> prefix.'keysearch'] )
				{
					$keysearch = $_SESSION[$this -> prefix.'keysearch'];
					$where .= " AND ( a.name LIKE '%".$keysearch."%' )";
				}
			}
			$query = " SELECT a.*, a.alias as ccode
						  FROM 
						  ".$this -> table_name." AS a
						  	WHERE 1=1".
						 $where.
						 $ordering. " ";
						
			return $query;
		}
		function save($row = array(), $use_mysql_real_escape_string = 1) {
			$products_related = $color = FSInput::get('products_record_related', array(), 'array');
			$str_products_related = implode(',', $products_related);
			if ($str_products_related) {
				$str_products_related = ',' . $str_products_related . ',';
			}
			$row['apply_for'] = $str_products_related;

			$rs = parent::save($row, 1);
			return $rs;
		}
		function get_categories_tree()
		{
			global $db;
			$where = '';
			if (isset($_SESSION[$this->prefix . 'category_keysearch'])) {
				if ($_SESSION[$this->prefix . 'category_keysearch']) {
					$keysearch = $_SESSION[$this->prefix . 'category_keysearch'];
					$where .= " AND ( name LIKE '%" . $keysearch . "%' OR alias LIKE '%" . $keysearch . "%' OR id = '" . $keysearch . "')";
				}
			}
			$sql = " SELECT id, name, parent_id AS parent_id  ,level
					FROM " . $this->table_categories . "
					WHERE 1=1 " . $where;
			$db->query($sql);
			$categories = $db->getObjectList();
	
			$tree = FSFactory::getClass('tree', 'tree/');
			$list = $tree->indentRows2($categories);
			return $list;
		}
		function get_products_related($product_related)
		{
			if (!$product_related)
				return;
			$query = " SELECT id, name,image 
						FROM fs_products
						WHERE id IN (0" . $product_related . "0) 
						 ORDER BY POSITION(','+id+',' IN '0" . $product_related . "0')
						";
			global $db;
			$sql = $db->query($query);
			$result = $db->getObjectList();
			return $result;
		}
		function get_category_promotion() {
			global $db;
			$sql = " SELECT id, name, parent_id AS parent_id  ,level
					FROM " . $this->category_promotion . "
					WHERE 1=1";
			$db->query($sql);
			$categories = $db->getObjectList();
	
			$tree = FSFactory::getClass('tree', 'tree/');
			$list = $tree->indentRows2($categories);
			return $list;
		}
	}
	
?>