<?php
	class DiscountControllersDiscount extends Controllers{
		function display(){
			parent::display();
			$sort_field = $this -> sort_field;
			$sort_direct = $this -> sort_direct;
			
			$model  = $this -> model;
			$list = $this -> model->get_data('');
			$pagination = $model->getPagination('');
			$category_promotion = $model->get_category_promotion();
			include 'modules/'.$this->module.'/views/'.$this->view.'/list.php';
		}
		function add() {
			$id = FSInput::get("id");
			$categories = $this->model->get_categories_tree();
			$category_promotion = $this->model->get_category_promotion();
			$obj = new stdClass;
			$obj->id = 0;
			$obj->name = "-----------";
			$category_promotion = array_merge(array(0 => $obj), $category_promotion);
			include 'modules/'.$this->module.'/views/'.$this->view.'/detail.php';
		}
		function edit() {
			$id = FSInput::get("id");
			$categories = $this->model->get_categories_tree();
			$data = $this->model->get_record_by_id($id);
			@$products_related = $this->model->get_products_related($data->apply_for);
			$category_promotion = $this->model->get_category_promotion();
			$obj = new stdClass;
			$obj->id = 0;
			$obj->name = "-----------";
			$category_promotion = array_merge(array(0 => $obj), $category_promotion);
			include 'modules/'.$this->module.'/views/'.$this->view.'/detail.php';
		}
	}	
	
?>