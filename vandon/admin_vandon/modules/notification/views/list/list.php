<script language="javascript" type="text/javascript" src="../libraries/jquery/jquery.ui/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="../libraries/jquery/jquery.ui/jquery-ui.css" />
<?php  
	global $toolbar;
	$toolbar->setTitle(FSText :: _('News') );
	//$toolbar->addButton('save_all',FSText :: _('Save'),'','save.png');
    $toolbar->addButton('duplicate',FSText :: _('Duplicate'),FSText :: _('You must select at least one record'),'duplicate.png');
	$toolbar->addButton('add',FSText :: _('Add'),'','add.png');  
	$toolbar->addButton('remove',FSText :: _('Remove'),FSText :: _('You must select at least one record'),'remove.png'); 
	$toolbar->addButton('published',FSText :: _('Published'),FSText :: _('You must select at least one record'),'published.png');
	$toolbar->addButton('unpublished',FSText :: _('Unpublished'),FSText :: _('You must select at least one record'),'unpublished.png');
	
	//	FILTER
	$filter_config  = array();
	$fitler_config['search'] = 1;
	$fitler_config['filter_count'] = 0;
//    $fitler_config['text_count'] = 2;

	$filter_categories = array();
	$filter_categories['title'] = FSText::_('Categories');
	$filter_categories['list'] = @$categories;
	$filter_categories['field'] = 'treename';
    
//    $text_from_date = array();
//	$text_from_date['title'] =  FSText::_('Từ ngày');
//
//	$text_to_date = array();
//	$text_to_date['title'] =  FSText::_('Đến ngày');
	
	$fitler_config['filter'][] = $filter_categories;
//    $fitler_config['text'][] = $text_from_date;
//	$fitler_config['text'][] = $text_to_date;
	//	CONFIG	
	$list_config = array();
	$list_config[] = array('title'=>'Tiêu đề tin','field'=>'title','ordering'=> 1,'align'=>'left', 'type'=>'text_link','col_width' => '30%','link'=>'index.php?module=news&view=news&ccode=ccode&code=code&id=id&Itemid=4','arr_params'=>array('size'=> 30));
	$list_config[] = array('title'=>'Summary','field'=>'summary','type'=>'text','col_width' => '20%','arr_params'=>array('size'=>30,'rows'=>1));
	// $list_config[] = array('title'=>'Người đăng','field'=>'author','ordering'=> 1, 'type'=>'text');
    $list_config[] = array('title'=>'Thời gian gửi','field'=>'created_time','ordering'=> 1, 'type'=>'datetime');
    // $list_config[] = array('title'=>'Hoạt động','field'=>'published','ordering'=> 1, 'type'=>'published');
	$list_config[] = array('title'=>'Edit','type'=>'edit');
	// $list_config[] = array('title'=>'Gửi thông báo','type'=>'send');

	$list_config[] = array('title'=>'Id','field'=>'id','ordering'=> 1, 'type'=>'text');
	
	TemplateHelper::genarate_form_liting($this->module,$this -> view,$list,$fitler_config,$list_config,$sort_field,$sort_direct,$pagination);
?>
<script>
	$(function() {
		$( "#text0" ).datepicker({
		  clickInput:true,
          dateFormat: 'dd-mm-yy',
          changeMonth: true,
          numberOfMonths: 2,
          changeYear: true,
          maxDate:  " + d ",
          showMonthAfterYear: true
        });
		$( "#text1" ).datepicker({
		  clickInput:true,
          dateFormat: 'dd-mm-yy',
          changeMonth: true,
          numberOfMonths: 2,
          changeYear: true,
          maxDate:  " + d ",
          showMonthAfterYear: true
        });
	});
</script>