<link type="text/css" rel="stylesheet" media="all" href="../libraries/jquery/jquery.ui/jquery-ui.css" />
<script type="text/javascript" src="../libraries/jquery/jquery.ui/jquery-ui.js"></script>
<?php
$title = @$data ? FSText::_('Edit') : FSText::_('Add');
global $toolbar;
$toolbar->setTitle($title);
$toolbar->addButton('save_add', FSText::_('Save and new'), '', 'save_add.png', 1);
$toolbar->addButton('apply', FSText::_('Apply'), '', 'apply.png', 1);
$toolbar->addButton('Save', FSText::_('Save'), '', 'save.png', 1);
$toolbar->addButton('back', FSText::_('Cancel'), '', 'cancel.png');

echo ' 	<div class="alert alert-danger" style="display:none" >
                    <span id="msg_error"></span>
            </div>';
$this->dt_form_begin(1, 4, $title . ' ' . FSText::_('Notifications'), 'fa-edit', 1, 'col-md-8', 1);
TemplateHelper::dt_edit_text(FSText::_('Tiêu đề'), 'title', @$data->title);
TemplateHelper::dt_edit_text(FSText::_('Alias'), 'alias', @$data->alias, '', 60, 1, 0, FSText::_("Can auto generate"));
TemplateHelper::datetimepicke(FSText::_('Published time'), 'created_time', @$data->created_time ? @$data->created_time : date('Y-m-d H:i:s'), FSText::_('Bạn vui lòng chọn thời gian hiển thị'), 20, '', 'col-md-3', 'col-md-4');
TemplateHelper::dt_edit_text(FSText::_('Nội dung thông báo'), 'summary', @$data->summary, '', 100, 5, 0, '', '', 'col-sm-3', 'col-sm-9');

$this->dt_form_end_col(); // END: col-1

$this->dt_form_begin(1, 2, FSText::_('Kích hoạt'), 'fa-unlock', 1, 'col-md-4 fl-right');
TemplateHelper::dt_checkbox(FSText::_('Published'), 'published', @$data->published, 1, '', '', '', 'col-sm-4', 'col-sm-8');
TemplateHelper::dt_edit_text(FSText::_('Ordering'), 'ordering', @$data->ordering, @$maxOrdering, '', '', 0, '', '', 'col-sm-4', 'col-sm-8');
$this->dt_form_end_col(); // END: col-2

$this->dt_form_end(@$data, 1, 0, 2, 'Cấu hình seo', '', 1, 'col-sm-4');
?>

<script type="text/javascript">
    
	function formValidator()
	{
	    $('.alert-danger').show();	
        
		if(!notEmpty('title','Bạn phải nhập tiêu đề thông báo'))
			return false;
                
		$('.alert-danger').hide();
		return true;
	}
 
</script>