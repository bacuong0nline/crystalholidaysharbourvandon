<link type="text/css" rel="stylesheet" media="all" href="../libraries/jquery/jquery.ui/jquery-ui.css" />
<script type="text/javascript" src="../libraries/jquery/jquery.ui/jquery-ui.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<?php
$title = @$data ? FSText::_('Edit') : FSText::_('Add');
global $toolbar;
$toolbar->setTitle($title);
$toolbar->addButton('save_add', FSText::_('Save and new'), '', 'save_add.png', 1);
$toolbar->addButton('apply', FSText::_('Apply'), '', 'apply.png', 1);
$toolbar->addButton('Save', FSText::_('Save'), '', 'save.png', 1);
$toolbar->addButton('back', FSText::_('Cancel'), '', 'cancel.png');

echo ' 	<div class="alert alert-danger" style="display:none" >
                    <span id="msg_error"></span>
            </div>';
$this->dt_form_begin(1, 4, $title . ' ' . FSText::_('Notifications'), 'fa-edit', 1, 'col-md-8', 1);
TemplateHelper::dt_edit_text(FSText::_('Tiêu đề'), 'title', @$data->title);
TemplateHelper::dt_edit_text(FSText::_('Alias'), 'alias', @$data->alias, '', 60, 1, 0, FSText::_("Can auto generate"));
TemplateHelper::datetimepicke(FSText::_('Published time'), 'created_time', @$data->created_time ? @$data->created_time : date('Y-m-d H:i:s'), FSText::_('Bạn vui lòng chọn thời gian hiển thị'), 20, '', 'col-md-3', 'col-md-4');
?>

<?php
TemplateHelper::dt_edit_text(FSText::_('Nội dung thông báo'), 'summary', @$data->summary, '', 100, 5, 0, '', '', 'col-sm-3', 'col-sm-9');
// $this->dt_form_end_col(); // END: col-1

?>
<?php
$this->dt_form_begin(1, 4,FSText::_('Chọn người nhận thông báo (Nếu không chọn sẽ gửi toàn bộ thiết bị)'), 'fa-edit', 1, 'col-md-12', 1);

?>
<div style="margin-top: 10px" class="d-flex form-group">
	<div class="col-md-3 control-label">
	</div>
	<div class="col-md-9">
		<button type="button" class="btn btn-primary select-author-btn">Chọn tác giả <i class="fa fa-chevron-down"></i></button>
	</div>
</div>
<div style="margin-top: 10px; <?php echo empty(@$data->send_to) ? "display: none" : null?>" class="d-flex form-group select-author">
	<div class="col-md-3 control-label">
		<p><b>Chọn tác giả</b></p>
		<p style="font-size: 10px">Chỉ hiển thị những tác giả đã đăng nhập vào ứng dụng VCPMC. Và chỉ những tác giả được chọn sẽ nhận được thông báo này</p>
	</div>
	<div class="col-md-9">
		<select id="members" class="members" name="members[]" multiple="multiple">
		<?php
            foreach (@$members as $item) {
            ?>
                <option value="<?php echo $item->id ?>"><?php echo $item->name; ?></option>
            <?php } ?>
		</select>
		<!-- <p style="color: red; margin-bottom: 0">Giữ phím ctrl để chọn nhiều</p> -->
		<!-- <a class="btn btn-danger chosen-deselect deselect-member" style="cursor: pointer;margin-top: 10px;"><?php echo FSText::_('Xóa chọn') ?></a> -->
		<button type="button" class="btn btn-danger chosen-deselect deselect-member "><?php echo FSText::_('Xóa chọn') ?></button>

	</div>
</div>
<?php
$this->dt_form_end_col(); // END: col-2

?>
<?php
$this->dt_form_end(@$data, 1, 0, 2, 'Cấu hình seo', '', 1, 'col-sm-4');
?>

<script type="text/javascript">
	function formValidator() {
		$('.alert-danger').show();

		if (!notEmpty('title', 'Bạn phải nhập tiêu đề thông báo'))
			return false;

		$('.alert-danger').hide();
		return true;
	}
	$(document).ready(function() {
		$('.members').select2({
			placeholder: "Tác giả",
			ajax: {
				url: `/admin_vcp/index.php?module=notification&view=notification&task=get_ajax_members&raw=1`,
				data: function(params) {
					var query = {
						search: params.term,
						page: params.page || 1
					}
					return query;
				},
				processResults: function(data, params) {
					data = JSON.parse(data);
					return {
						results: data.results,
						pagination: {
							'more': data.pagination.more
						}
					};
				}
			}
		});
		$('#members').val([<?php echo @$data->send_to ?>]).trigger('change')

		$(".deselect-member").click(function() {
			$('.members').val(null).trigger("change");
		})
		$(".select-author-btn").click(function() {
			$(".select-author").toggle();
			$('.members').val(null).trigger("change");
		})
	})
</script>