<?php 
	class NotificationModelsNotification extends FSModels
	{
		var $limit;
		var $prefix ;
		function __construct()
		{
		    date_default_timezone_set('Asia/Ho_Chi_Minh');  
			$this -> limit = 30;
			$this -> view = 'notifications';
			
			//$this -> table_types = 'fs_news_types';
			$this -> arr_img_paths = array(
                                            array('lange',632,423,'cut_image'),
                                            array('resized',359,240,'cut_image'),
                                            array('small',112,75,'cut_image')
                                        );
			// $this -> table_category_name = FSTable_ad::_('fs_news_categories',1);
            $this -> table_name = FSTable_ad::_('fs_notifications',1);
            $this -> table_link = 'fs_menus_createlink';
            $this -> table_tags = FSTable_ad::_('fs_tags',1);
            $limit_created_link = 30;
			$this->limit_created_link = $limit_created_link;
			// config for save
			$cyear = date('Y');
			$cmonth = date('m');
			//$cday = date('d');
			$this -> img_folder = 'images/notifications/'.$cyear.'/'.$cmonth;
			$this -> check_alias = 0;
			$this -> field_img = 'image';
			
			parent::__construct();
		}
		function save($row = array(), $use_mysql_real_escape_string = 1) {
			$members = FSInput::get("members", array(), 'array');
			$string = '';
			foreach($members as $key=>$item) {
				$data_members = $this->get_record_by_id($item, 'fs_members');
				$string .= $data_members->device_token;
				if($key + 1 < count($members)) {
					$string .= ',';
				}
			}
			$rows['send_to'] = implode(',', $members);
			$rows['send_to_token'] = $string;

			$rs = parent::save($rows);

			return $rs;
		}
	}

?>