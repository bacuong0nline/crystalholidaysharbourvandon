<?php
class ScheduleModelsComments extends FSModels
{

	var $limit;
	var $prefix;
	function __construct()
	{
		$this->limit = 20;
		$this->view = 'comments';
		$this->table_name = 'fs_schedule_comments';
		$this->table_name_categories = 'fs_schedule_categories';

		$this -> arr_img_paths = array(
			array('lange',632,423,'resize_image'),
			array('resized',359,240,'resize_image'),
			array('small',112,75,'cut_image')
		);
		$cyear = date('Y');
		$cmonth = date('m');
		$this -> img_folder = 'images/comments/'.$cyear.'/'.$cmonth;

		parent::__construct();
	}

	function save($row = array(), $use = 1) {
		$category_id = FSInput::get('category_id',0,'int');
		$cat =  $this->get_record_by_id($category_id,$this -> table_name_categories);
		$row['category_id_wrapper'] = $cat -> list_parents;
		$row['category_alias_wrapper'] = $cat -> alias_wrapper;
		$row['category_name'] = $cat -> name;
		$row['category_id'] = $category_id;
		$row['category_alias'] = $cat -> alias;


		$rs = parent::save($row, 1);
		return $rs;
	}

	function get_categories_tree()
	{
		global $db;
		$query = " SELECT a.*
					  FROM 
						  ".$this -> table_name_categories." AS a
						  WHERE published = 1 ORDER BY ordering ";         
		$sql = $db->query($query);
		$result = $db->getObjectList();
		$tree  = FSFactory::getClass('tree','tree/');
		$list = $tree -> indentRows2($result);
		return $list;
	}
}
