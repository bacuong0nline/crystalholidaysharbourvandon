<?php
	class ScheduleControllersCategories extends ControllersCategories{
	   function __construct()
		{
			$this->view = 'categories' ; 
			parent::__construct(); 
			$this -> arr_img_paths = array(
				array('lange',632,423,'cut_image'),
				array('resized',359,240,'cut_image'),
				array('small',112,75,'cut_image')
			);
		}
        
        function edit()
		{
			$model =  $this -> model;
			$ids = FSInput::get('id',array(),'array');
			$id = $ids[0];
			$data = $model->get_record_by_id($id);
			// $categories = $model->get_categories_tree2();
			include 'modules/'.$this->module.'/views/'.$this->view.'/detail.php';
		}
	}
	
?>