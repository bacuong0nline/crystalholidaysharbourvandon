<link type="text/css" rel="stylesheet" media="all" href="../libraries/jquery/jquery.ui/jquery-ui.css" />
<script type="text/javascript" src="../libraries/jquery/jquery.ui/jquery-ui.js"></script>
<?php
$title = @$data ? FSText::_('Edit') : FSText::_('Add');
global $toolbar;
$toolbar->setTitle($title);
$toolbar->addButton('save_add', FSText::_('Save and new'), '', 'save_add.png', 1);
$toolbar->addButton('apply', FSText::_('Apply'), '', 'apply.png', 1);
$toolbar->addButton('Save', FSText::_('Save'), '', 'save.png', 1);
$toolbar->addButton('back', FSText::_('Cancel'), '', 'cancel.png');

echo ' 	<div class="alert alert-danger" style="display:none" >
                    <span id="msg_error"></span>
            </div>';
$this->dt_form_begin(1, 4, $title . ' ' . FSText::_('Sửa dự án'), 'fa-edit', 1, 'col-md-12', 1);

?>
<div id="tabs">
    <ul>
        <li><a href="#tabs-1"><?php echo FSText::_("Chi tiết bài viết") ?></a></li>
        <li><a href="#tabs-2"><?php echo FSText::_("Câu hỏi thường gặp") ?></a></li>
    </ul>
    <div id="tabs-1">
        <?php include_once('detail_base.php') ?>
    </div>
    <div id="tabs-2">
        <?php include_once('qna.php') ?>
    </div>
</div>
<?php


$this->dt_form_end(@$data, 1, 0, 2, 'Cấu hình seo', '', 1, 'col-sm-4');
?>
<script type="text/javascript">
    $("#tabs").tabs();

    $('.form-horizontal').keypress(function(e) {
        if (e.which == 13) {
            formValidator();
            return false;
        }
    });

    function formValidator() {
        $('.alert-danger').hide();

        return true;
    }
</script>
<?php //include 'detail_seo.php'; 
?>