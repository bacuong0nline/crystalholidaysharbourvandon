<!-- HEAD -->
<?php
$title = @$data ? FSText::_('S&#7917;a v&#7883; tr&#237; hi&#7875;n th&#7883;') : FSText::_('T&#7841;o m&#7899;i v&#7883; tr&#237; hi&#7875;n th&#7883;');
global $toolbar;
$toolbar->setTitle($title);
$toolbar->addButton('apply', FSText::_('Apply'), '', 'apply.png', 1);
$toolbar->addButton('Save', FSText::_('Save'), '', 'save.png', 1);
$toolbar->addButton('cancel', FSText::_('Cancel'), '', 'cancel.png');

echo ' 	<div class="alert alert-danger" style="display:none" >
                    <span id="msg_error"></span>
            </div>';
//$this -> dt_form_begin(1,4,$title);
global $position;
$this->dt_form_begin(1, 4, FSText::_('Cài đặt'), 'fa-edit', 1, 'col-md-8', 1);
TemplateHelper::dt_edit_text(FSText::_('Tiêu đề Block'), 'title', @$data->title);
$this->dt_form_end_col(); // END: col-1

$this->dt_form_begin(1, 9, FSText::_('Kích hoạt'), 'fa-unlock', 1, 'col-md-4 fl-right');
TemplateHelper::dt_checkbox(FSText::_('Published'), 'published', @$data->published, 1, '', '', '', 'col-sm-4', 'col-sm-8');
TemplateHelper::dt_checkbox(FSText::_('Hiển thị tiêu đề'), 'showTitle', @$data->showTitle, 0, '', '', '', 'col-sm-4', 'col-sm-8');
TemplateHelper::dt_edit_text(FSText::_('Ordering'), 'ordering', @$data->ordering, @$maxOrdering, '', '', 0, '', '', 'col-sm-4', 'col-sm-8');
?>
<div style="border: 1px solid #337ab7; padding: 20px">
	<p style="font-weight: bold"><?php echo FSText::_("Style: ") ?></p>
	<img class="type-image style-90" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/tieu-chi.png' ?>" alt="tieu-chi" />
	<p class="type-image style-90 text-center">Kiểu 1</p>
	<img class="type-image style-90" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/daotao1.png' ?>" alt="tieu-chi" />
	<p class="type-image style-90 text-center">Kiểu 2</p>
	<img class="type-image style-90" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/daotao2.png' ?>" alt="tieu-chi" />
	<p class="type-image style-90 text-center">Kiểu 3</p>
	<img class="type-image style-90" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/daotao3.png' ?>" alt="tieu-chi" />
	<p class="type-image style-90 text-center">Kiểu 4</p>

	<img class="type-image style-88" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/daotao.png' ?>" alt="daotao" />
	<p class="type-image style-88 text-center">Kiểu 1</p>
	<img class="type-image style-88" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/daotaochuyenbiet.png' ?>" alt="daotaochuyenbiet" />
	<p class=" type-image style-88 text-center">Kiểu 2</p>
	<img class="type-image style-89" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/form-lien-he.png' ?>" alt="lienhe" />
	<p class="type-image style-89 text-center">Kiểu 1</p>
	<img class="type-image style-89" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/form2.png' ?>" alt="form2" />
	<p class="type-image style-89 text-center">Kiểu 2</p>
	<img class="type-image style-89" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/form3.png' ?>" alt="form3" />
	<p class="type-image style-89 text-center">Kiểu 3</p>
	<img class="type-image style-89" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/form4.png' ?>" alt="form4" />
	<p class="type-image style-89 text-center">Kiểu 4</p>
	<img class="type-image style-91" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/trainghiemhocvien.png' ?>" alt="trainghiemhocvien" />
	<img class="type-image style-99" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/comment.png' ?>" alt="comment" />
	<p class="type-image style-99 text-center">Kiểu 1</p>
	<img class="type-image style-99" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/comment2.png' ?>" alt="comment" />
	<p class="type-image style-99 text-center">Kiểu 2</p>
	<img class="type-image style-93" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/hethongtrungtam.png' ?>" alt="hethongtrungtam" />
	<img class="type-image style-94" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/dangky.png' ?>" alt="dangky" />
	<img class="type-image style-98" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/tintuc.png' ?>" alt="tintuc" />
	<p class="type-image style-98 text-center">Kiểu 1 + Cho danh mục tin tức 1 cấp</p>
	<p class="type-image style-98 text-center">Kiểu 2 + Danh mục tin tức nhiều cấp con</p>

	<img class="type-image style-96" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/giangvien.png' ?>" alt="giangvien" />
	<img class="type-image style-100" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/lich-thi.png' ?>" alt="lich-thi" />
	<img class="type-image style-102" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/gallery1.png' ?>" alt="gallery" />
	<p class="type-image style-102 text-center">Kiểu 1</p>
	<img class="type-image style-102" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/gallery2.png' ?>" alt="gallery" />
	<p class="type-image style-102 text-center">Kiểu 2</p>
	<img class="type-image style-102" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/gallery3.png' ?>" alt="gallery" />
	<p class="type-image style-102 text-center">Kiểu 3</p>
	<img class="type-image style-34" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/banner1.png' ?>" alt="banner1" />
	<p class="type-image style-34 text-center">Kiểu 1: Mô tả + Video</p>
	<img class="type-image style-34" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/banner2.png' ?>" alt="banner2" />
	<p class="type-image style-34 text-center">Kiểu 2: Mô tả + Video</p>
	<img class="type-image style-34" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/banner3.png' ?>" alt="banner3" />
	<p class="type-image style-34 text-center">Kiểu 3: Mô tả + Slide ảnh</p>
	<p class="type-image style-34 text-center">Kiểu 4: Mô tả trên ảnh nền</p>
	<img class="type-image style-104" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/app1.png' ?>" alt="app1" />
	<p class="type-image style-104 text-center">Kiểu 1: Sử dụng Bulleted List trên editor sẽ tự động đếm số (Xem mẫu) </p>
	<img class="type-image style-105" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/tuyen-dung-1.png' ?>" alt="tuyen-dung-1" />
	<p class="type-image style-105 text-center">Kiểu 1: Chú ý thêm class title-ielts vào tiêu đề để có hiệu ứng </p>
	<img class="type-image style-105" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/tuyen-dung-6.png' ?>" alt="tuyen-dung-6" />
	<p class="type-image style-105 text-center">Kiểu 2: Chú ý thêm class title-ielts vào tiêu đề để có hiệu ứng </p>
	<img class="type-image style-105" style="width: 100%; height: auto; object-fit: contain" src="<?php echo URL_ROOT . 'admin/images/tuyen-dung-7.png' ?>" alt="tuyen-dung-7" />
	<p class="type-image style-105 text-center">Kiểu 3 </p>
</div>
<?php
$this->dt_form_end_col(); // END: col-2



$this->dt_form_begin(1, 4, $title . ' ' . FSText::_('Cài đặt'), 'fa-edit', 1, 'col-md-8');
?>
<div class="form-group">
	<label class="col-sm-2 col-xs-12 control-label"><?php echo FSText::_('N&#417;i xu&#7845;t hi&#7879;n'); ?></label>
	<div class="col-sm-10 col-xs-12">
		<div class="" style="margin-bottom: 10px;">
			<input class="radio-custom" type="radio" id='check_none' name='area_select' value='none' <?php echo (!@$data->listItemid || @$data->listItemid == 'none') ? 'checked="checked"' : ''; ?> />
			<label for="check_none" class="radio-custom-label"><?php echo FSText::_('Không xuất hiện') ?></label>

			<input class="radio-custom" type="radio" id='check_select' name='area_select' value='select' <?php echo (@$data->listItemid && @$data->listItemid != 'none' && @$data->listItemid != 'all') ? 'checked="checked"' : ''; ?> />
			<label for="check_select" class="radio-custom-label"><?php echo FSText::_('Lựa chọn') ?></label>

			<input class="radio-custom" type="radio" id='check_all' name='area_select' value='all' <?php echo (@$data->listItemid == 'all') ? 'checked="checked"' : ''; ?> />
			<label for="check_all" class="radio-custom-label"><?php echo FSText::_('Tất cả') ?></label>
		</div>
		<?php
		// print_r($menus_items_all);die;
		$listItemid = @$data->listItemid;
		$checked = 0;
		$checked_all = 0;

		if ((!@$data->listItemid) || @$data->listItemid === 'none' || @$data->listItemid === '0') {
			$checked = 0;
		} else if (@$data->listItemid === 'all') {
			$checked_all = 1;
		} else {
			$checked = 1;
			$checked_all = 0;
			$arr_menu_item = explode(',', @$data->listItemid);
		}
		?>
		<select data-placeholder="<?php echo FSText::_('Nơi xuất hiện') ?>" name="menus_items[]" size="8" multiple="multiple" class='form-control chosen-select-no-results listItem select2' <?php echo (!@$data->listItemid || @$data->listItemid == 'none' || @$data->listItemid == 'all') ? 'disabled="disabled"' : ''; ?>>
			<?php
			foreach ($menus_items_all as $item) {

				$html_check = "";
				if ($checked_all) {
					$html_check = "' selected='selected' ";
				} else {
					if ($checked) {
						if (in_array($item->id, $arr_menu_item)) {
							$html_check = "' selected='selected' ";
						}
					}
				}
			?>
				<option value="<?php echo $item->id ?>" <?php echo $html_check; ?>><?php echo $item->name; ?></option>
			<?php } ?>
		</select>
	</div>
</div><!-- END: type -->
<div class="form-group">
	<label class="col-sm-2 col-xs-12 control-label"><?php echo FSText::_('Kiểu'); ?></label>
	<div class="col-sm-10 col-xs-12">
		<select data-placeholder="<?php echo FSText::_('Kiểu') ?>" name="type" class="type form-control chosen-select">
			<?php
			$block_select = isset($data->module) ? $data->module : 'contents';
			foreach ($listmoduletype as $item) {
				if ($item->block == $block_select) {
					echo "<option value='" . $item->id . "' selected='selected'>" . $item->name . ' [' . $item->block . ']' . "</option>";
				} else {
					echo "<option value='" . $item->id . "'>" . $item->name . ' [' . $item->block . ']' . "</option>";
				}
			}
			?>
		</select>
	</div>
</div><!-- END: type -->
<div class="form-group">
	<label class="col-sm-2 col-xs-12 control-label"><?php echo FSText::_('V&#7883; tr&#237;'); ?></label>
	<div class="col-sm-10 col-xs-12">
		<select data-placeholder="<?php echo FSText::_('V&#7883; tr&#237;'); ?>" name="position" class="pos form-control chosen-select">
			<?php
			foreach ($positions as $key => $p) {
				if ((@$data->position) && $key == @$data->position) {
					echo "<option value='" . $key . "' selected='selected'>" . $p . "</option>";
				} else {
					echo "<option value='" . $key . "'>" . $p . '[' . $key . ']' . "</option>";
				}
			}
			?>
		</select>
	</div>
</div><!-- END: positions -->
<?php if (@$data->module == "form") {
	//TemplateHelper::dt_edit_image(FSText::_('Hình ảnh'), 'image', str_replace('/original/', '/original/', URL_ROOT . @$data->image));
}
if (@$data->module == 'banners') { 
	TemplateHelper::dt_edit_text(FSText::_('Summary'), 'summary', @$data->summary, '', 100, 5, 1);

} 
?>

<?php if (@$data->module == 'recruitment' || @$data->module == 'app' || @$data->module == 'contents' || @$data->module == 'criteria' || @$data->module == 'contents2' || @$data->module == 'experience') { ?>
	<div class="form-group">
		<label class="col-sm-2 col-xs-12 control-label"><?php echo FSText::_('Content'); ?></label>
		<div class="col-sm-10 col-xs-12">
			<?php
			//				echo $data->content;
			$oFCKeditor = new FCKeditor('content');
			$oFCKeditor->BasePath	=  '../libraries/wysiwyg_editor/';
			$oFCKeditor->Value		= @$data->content;
			$oFCKeditor->Width = 650;
			$oFCKeditor->Height = 450;
			$oFCKeditor->Create();
			?>
		</div>
	</div><!-- END: positions -->
<?php } ?>
<a target="_blank" href="<?php echo URL_ROOT . 'admin/images/giai-thich.png' ?>" style="color:red !important">
	<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-question-circle-fill" viewBox="0 0 16 16">
		<path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.496 6.033h.825c.138 0 .248-.113.266-.25.09-.656.54-1.134 1.342-1.134.686 0 1.314.343 1.314 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.003.217a.25.25 0 0 0 .25.246h.811a.25.25 0 0 0 .25-.25v-.105c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.267 0-2.655.59-2.75 2.286a.237.237 0 0 0 .241.247zm2.325 6.443c.61 0 1.029-.394 1.029-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94 0 .533.425.927 1.01.927z" />
	</svg>
	<?php echo FSText::_("Giải thích Parameters") ?>
</a>
<div class="form-group">
	<label class="col-sm-2 col-xs-12 control-label"><?php echo FSText::_('Parameters'); ?></label>
	<div class="col-sm-10 col-xs-12">
		<?php include_once 'detail_params.php'; ?>
	</div>
</div>
<?php
$this->dt_form_end_col(); // END: col-4
$this->dt_form_end(@$data, 1, 0, 2, '', '', 1);
?>
<style>
	.type-image {
		display: none;
	}
</style>
<script type="text/javascript">
	$('.form-horizontal').keypress(function(e) {
		if (e.which == 13) {
			formValidator();
			return false;
		}
	});

	function formValidator() {
		$('.alert-danger').show();

		if (!notEmpty('title', 'Bạn phải nhập tiêu đề'))
			return false;

		$('.alert-danger').hide();
		return true;
	}
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#check_none').click(function() {
			$('.listItem option').each(function() {
				$(this).attr('selected', '');
			});
			$('.listItem').attr('disabled', 'disabled');
			$(".listItem").trigger("chosen:updated");
		});
		$('#check_all').click(function() {
			$('.listItem option').each(function() {
				$(this).attr('selected', 'selected');
			});
			$('.listItem').attr('disabled', 'disabled');
			$(".listItem").trigger("chosen:updated");
		});
		$('#check_select').click(function() {
			$('.listItem').removeAttr('disabled');
			$(".listItem").trigger("chosen:updated");
		});

		$(".style-" + $(".type").val()).css("display", "block");

		$(".type").change(function() {
			$(".type-image").css("display", "none");
			$(".style-" + $(this).val()).css("display", "block");
		})
	});
</script>