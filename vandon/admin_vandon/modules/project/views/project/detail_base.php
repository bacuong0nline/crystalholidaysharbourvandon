
<?php
TemplateHelper::dt_edit_text(FSText::_('Tiêu đề'), 'title', @$data->title);
TemplateHelper::dt_edit_text(FSText::_('Alias'), 'alias', @$data->alias, '', 60, 1, 0, FSText::_("Can auto generate"));
// TemplateHelper::dt_edit_selectbox(FSText::_('Chi phí'), 'cost', @$data->cost, 0, $costs, $field_value = 'id', $field_label = 'title', $size = 10, 0, 1);
// TemplateHelper::dt_edit_selectbox(FSText::_('Phong cách'), 'style', @$data->style, 0, $styles, $field_value = 'id', $field_label = 'title', $size = 1, 1, 0, '', '', '', 'col-md-3', 'col-md-9');
// TemplateHelper::dt_edit_text(FSText::_('Diện tích'), 'area', @$data->area, '', 60, 1, 0, '');
// TemplateHelper::dt_edit_selectbox(FSText::_('Tỉnh thành phố'), 'city_id', @$data->city_id, 0, $dataCity, $field_value = 'id', $field_label = 'name', $size = 10, 0, 1);
// TemplateHelper::dt_edit_text(FSText::_('Địa điểm khác'), 'address2', @$data->address2, '', 60, 1, 0, '');
TemplateHelper::dt_edit_image(FSText::_('Thumbnail'), 'image', str_replace('/original/', '/small/', URL_ROOT . @$data->image));
TemplateHelper::dt_edit_image(FSText::_('Ảnh bìa'), 'image2', str_replace('/original/', '/small/', URL_ROOT . @$data->image2));
TemplateHelper::dt_edit_image(FSText::_('Ảnh thông tin chi tiết'), 'image3', str_replace('/original/', '/small/', URL_ROOT . @$data->image3));

TemplateHelper::dt_edit_selectbox(FSText::_('Categories'), 'category_id', @$data->category_id, 0, $categories, $field_value = 'id', $field_label = 'treename', $size = 10, 0, 1);
TemplateHelper::dt_edit_selectbox(FSText::_('Tiện ích'), 'special_point', @$data->special_point, 0, $special_points, $field_value = 'id', $field_label = 'title', $size = 10, 1, 1);

TemplateHelper::datetimepicke(FSText::_('Published time'), 'created_time', @$data->created_time ? @$data->created_time : date('Y-m-d H:i:s'), FSText::_('Bạn vui lòng chọn thời gian hiển thị'), 20, '', 'col-md-3', 'col-md-9');
TemplateHelper::dt_checkbox(FSText::_('Published'), 'published', @$data->published, 1, '', '', '', 'col-sm-3', 'col-sm-9');
TemplateHelper::dt_edit_text(FSText::_('Ordering'), 'ordering', @$data->ordering, @$maxOrdering, '', '', 0, '', '', 'col-sm-3', 'col-sm-9');
TemplateHelper::dt_edit_text(FSText::_('Tóm tắt trang chủ'), 'summary2', @$data->summary2, '', 650, 450, 1, 'Xuất hiện ở trang chủ', '', 'col-sm-3', 'col-sm-9');
TemplateHelper::dt_edit_image2(FSText::_('Image'), 'image', str_replace('/original/', '/resized/', URL_ROOT . @$data->image), '', '', '', 1);
TemplateHelper::dt_edit_text(FSText::_('Mô tả điểm nổi bật'), 'summary', @$data->summary, '', 650, 450, 1, '', '', 'col-sm-3', 'col-sm-9');
TemplateHelper::dt_edit_text(FSText::_('Tổng quan'), 'content', @$data->content, '', 650, 450, 1, '', '', 'col-sm-3', 'col-sm-9');
// TemplateHelper::dt_edit_text(FSText::_('Tên khách hàng'), 'customer', @$data->customer, '', 60, 1, 0, '', '', 'col-sm-3', 'col-sm-9');
// TemplateHelper::dt_edit_text(FSText::_('Khách hàng nhận xét'), 'customer_review', @$data->customer_review, '', 100, 5, 0, '', '', 'col-sm-3', 'col-sm-9');
// TemplateHelper::dt_edit_text(FSText::_('Mô tả khác'), 'content2', @$data->content2, '', 650, 450, 1, '', '', 'col-sm-3', 'col-sm-9');


?>
