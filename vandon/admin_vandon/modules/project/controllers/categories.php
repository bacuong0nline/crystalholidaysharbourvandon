<?php
	class ProjectControllersCategories extends ControllersCategories{
	   function __construct()
		{
			$this->view = 'categories' ; 
			parent::__construct(); 
		}
        
        function edit()
		{
			$model =  $this -> model;
			$ids = FSInput::get('id',array(),'array');
			$id = $ids[0];
			$data = $model->get_record_by_id($id);
			$categories = $model->get_categories_tree();
 
			include 'modules/'.$this->module.'/views/'.$this->view.'/detail.php';
		}
		function uncategories()
		{
			$model = $this -> model;
			$rows = $model->categories(0);
			$link = 'index.php?module='.$this -> module.'&view='.$this -> view;
			$page = FSInput::get('page',0);
			if($page > 1)
				$link .= '&page='.$page;
			if($rows)
			{
				setRedirect($link,$rows.' '.FSText :: _('Kích hoạt thành công'));
			}
			else
			{
				setRedirect($link,FSText :: _('Huỷ bỏ kích hoạt thành công'),'error');
			}
		}
		function categories()
		{
			$model = $this -> model;
			$rows = $model->categories(1);
			$link = 'index.php?module='.$this -> module.'&view='.$this -> view;
			$page = FSInput::get('page',0);
			if($page > 1)
				$link .= '&page='.$page;
			if($rows)
			{
				setRedirect($link,$rows.' '.FSText :: _('Kích hoạt thành công'));
			}
			else
			{
				setRedirect($link,FSText :: _('Huỷ bỏ kích hoạt thành công'),'error');
			}
		}
	}
	
?>