<?php
	class ServiceControllersService extends Controllers
	{
		function __construct()
		{
			$this->view = 'service' ;
			parent::__construct(); 
		}
		function display()
		{
			parent::display();
			$sort_field = $this -> sort_field;
			$sort_direct = $this -> sort_direct;
			
			$model  = $this -> model;
			$list = $model->get_data('');
			$pagination = $model->getPagination('');
			include 'modules/'.$this->module.'/views/'.$this->view.'/list.php';
		}
		function add()
		{
			$model = $this -> model;
			$maxOrdering = $model->getMaxOrdering();
			
			// products related
			//$products_categories = $model->get_products_categories_tree();
				
			include 'modules/'.$this->module.'/views/'.$this -> view.'/detail.php';
		}
		
		function edit()
		{
			$ids = FSInput::get('id',array(),'array');
			$id = $ids[0];
			$model = $this -> model;
			$data = $model->get_record_by_id($id);
			include 'modules/'.$this->module.'/views/'.$this->view.'/detail.php';
		}

        function show_in_homepage()
        {
            $this->is_check('show_in_homepage',1,'show home');
        }
        function unshow_in_homepage()
        {
            $this->unis_check('show_in_homepage',0,'un home');
        }
		
		
	}
?>