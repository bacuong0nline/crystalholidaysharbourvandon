<?php
	// models 
//	include 'modules/'.$module.'/models/'.$view.'.php';
		  
	class CountrysControllersDistricts extends Controllers
	{
		function __construct()
		{
			$this->view = 'districts' ; 
			parent::__construct(); 
		}
        
		function display()
		{
			parent::display();
			$sort_field = $this -> sort_field;
			$sort_direct = $this -> sort_direct;
			$model  = $this -> model;
			$list = $this -> model->get_data('');
			$city = $model->get_cities();
			$pagination = $this->model->getPagination();

			include 'modules/'.$this->module.'/views/'.$this->view.'/list.php';
		}
        
       	function add()
		{
			$model = $this -> model;
			$city = $model->get_cities();
			$maxOrdering = $model->getMaxOrdering();
			
			include 'modules/'.$this->module.'/views/'.$this -> view.'/detail.php';
		}
		
		function edit()
		{
			$ids = FSInput::get('id',array(),'array');
			$id = $ids[0];
			$model = $this -> model;
			$data = $model->get_record_by_id($id);
			$city = $model->get_cities();
			include 'modules/'.$this->module.'/views/'.$this->view.'/detail.php';
		}
	}
	
?>