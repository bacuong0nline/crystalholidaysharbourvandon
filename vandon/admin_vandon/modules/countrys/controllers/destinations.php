<?php
	// models 
//	include 'modules/'.$module.'/models/'.$view.'.php';
		  
	class CountrysControllersDestinations extends Controllers
	{
		function __construct()
		{
			$this->view = 'destinations' ; 
			parent::__construct(); 
		}
        
		function display()
		{
			parent::display();
			$sort_field = $this -> sort_field;
			$sort_direct = $this -> sort_direct;
			$model  = $this -> model;
			$list = $this -> model->get_data('');
			$cities = $model->get_cities();
			// $districts  = $model->getDistricts();

			$pagination = $this -> model->getPagination();
			include 'modules/'.$this->module.'/views/'.$this->view.'/list.php';
		}
        
       	function add()
		{
			$model = $this -> model;
			$cities = $model->get_cities();
			// $districts  = $model->getDistricts();
			$maxOrdering = $model->getMaxOrdering();
			
			include 'modules/'.$this->module.'/views/'.$this -> view.'/detail.php';
		}
		
		function edit()
		{
			$ids = FSInput::get('id',array(),'array');
			$id = $ids[0];
			$model = $this -> model;
			$data = $model->get_record_by_id($id);
			$cities = $model->get_cities();
			$districts  = $model->getDistricts($data->city_id);
			include 'modules/'.$this->module.'/views/'.$this->view.'/detail.php';
		}
	}
	
?>