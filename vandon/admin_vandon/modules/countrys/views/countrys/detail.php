<?php
    $title = @$data ? FSText :: _('Edit'): FSText :: _('Add'); 
    global $toolbar;
    $toolbar->setTitle($title);
    $toolbar->addButton('apply',FSText :: _('Apply'),'','apply.png'); 
    $toolbar->addButton('Save',FSText :: _('Save'),'','save.png'); 
    $toolbar->addButton('back',FSText :: _('Cancel'),'','back.png');   

	$this -> dt_form_begin();
	
//	TemplateHelper::dt_edit_text(FSText :: _('Title'),'title',@$data -> title);
    TemplateHelper::dt_edit_text(FSText :: _('Quốc gia'),'name',@$data -> name);
	TemplateHelper::dt_checkbox(FSText::_('Published'),'published',@$data -> published,1);
	TemplateHelper::dt_edit_text(FSText :: _('Ordering'),'ordering',@$data -> ordering,@$maxOrdering,'20');
//	TemplateHelper::dt_edit_text(FSText :: _('Description'),'description',@$data -> description,'',650,450,1);
	$this -> dt_form_end(@$data);

?>