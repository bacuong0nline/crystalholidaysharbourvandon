<?php
    $title = @$data ? FSText :: _('Edit'): FSText :: _('Add'); 
    global $toolbar;
    $toolbar->setTitle($title);
    $toolbar->addButton('apply',FSText :: _('Apply'),'','apply.png'); 
    $toolbar->addButton('Save',FSText :: _('Save'),'','save.png'); 
    $toolbar->addButton('back',FSText :: _('Cancel'),'','back.png');   

	$this -> dt_form_begin();
	
    TemplateHelper::dt_edit_selectbox(FSText::_('Thành phố'),'city_id',@$data -> city_id,0,$city,$field_value = 'id', $field_label='name',$size = 1,0);
    TemplateHelper::dt_edit_text(FSText :: _('Quận huyện'),'name',@$data -> name);
    TemplateHelper::dt_edit_text(FSText :: _('Phí ship'),'fee_shipping',@$data -> fee_shipping);

	TemplateHelper::dt_checkbox(FSText::_('Published'),'published',@$data -> published,1);
	TemplateHelper::dt_edit_text(FSText :: _('Ordering'),'ordering',@$data -> ordering,@$maxOrdering,'20');
	$this -> dt_form_end(@$data);

?>