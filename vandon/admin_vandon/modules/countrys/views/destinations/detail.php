<?php
$title = @$data ? FSText::_('Edit') : FSText::_('Add');
global $toolbar;
$toolbar->setTitle($title);
$toolbar->addButton('apply', FSText::_('Apply'), '', 'apply.png');
$toolbar->addButton('Save', FSText::_('Save'), '', 'save.png');
$toolbar->addButton('back', FSText::_('Cancel'), '', 'back.png');

$this->dt_form_begin();

TemplateHelper::dt_edit_text(FSText::_('Điểm đến'), 'name', @$data->name);
?>

<div class="form-group">
    <label class="col-md-2 col-xs-12 control-label"><?php echo FSText::_("Tỉnh thành phố") ?></label>
    <div class="col-md-10 col-xs-12">
        <select class="chosen-select chosen-select-deselect chosen-select chosen-select-deselect-deselect" name="city_id" id="city_id">
            <?php foreach ($cities as $city) { ?>
                <?php $checked =  (@$data->city_id == $city->id) ? " selected = 'selected'" : ""; ?>
                <option value="<?php echo $city->id; ?>" <?php echo $checked; ?>><?php echo $city->name; ?></option>
            <?php } ?>
        </select>
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 col-xs-12 control-label"><?php echo FSText::_("Quận huyện") ?></label>
    <div class="col-md-10 col-xs-12">
        <select class="chosen-select chosen-select-deselect chosen-select chosen-select-deselect-deselect" name="district_id" id="district_id">
            <?php foreach ($districts as $district) { ?>
                <?php $checked =  (@$data->district_id == $district->id) ?  " selected = 'selected'" : ""; ?>
                <option value="<?php echo $district->id; ?>" <?php echo $checked; ?>><?php echo $district->name; ?></option>
            <?php } ?>
        </select>
    </div>
</div>

<?php
TemplateHelper::dt_edit_text(FSText::_('Mô tả'), 'summary', @$data->summary);
TemplateHelper::dt_edit_image(FSText::_('Image'), 'image', str_replace('/original/', '/resized/', URL_ROOT . @$data->image));
TemplateHelper::dt_checkbox(FSText::_('Published'), 'published', @$data->published, 1);
TemplateHelper::dt_edit_text(FSText::_('Ordering'), 'ordering', @$data->ordering, @$maxOrdering, '20');
$this->dt_form_end(@$data);

?>
<script type="text/javascript" language="javascript">
    $(function() {
        $("select#city_id").change(function() {
            $.ajax({
                url: "index.php?module=hotels&task=district&raw=1",
                data: {
                    cid: $(this).val()
                },
                dataType: "text",

                success: function(text) {

                    if (text == '')
                        return;
                    j = eval("(" + text + ")");

                    var options = '';
                    for (var i = 0; i < j.length; i++) {
                        options += '<option value="' + j[i].id + '">' + j[i].name + '</option>';
                    }
                    $('#district_id').html(options);
                    // elemnent_fisrt = $('#district_id option:first').val();
                    $("#district_id").chosen()
                    $("#district_id").trigger("chosen:updated");
                }
            });
        });
    })
</script>