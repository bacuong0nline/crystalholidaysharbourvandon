<?php 
	class CountrysModelsDistricts extends FSModels
	{
		var $limit;
		var $prefix ;
		function __construct()
		{
			$this -> limit = 20;
			$this -> view = 'districts';
			$this -> table_name = 'fs_districts';
			$this -> table_name_cities = 'fs_cities';
			$this -> table_name_districts = 'fs_districts';

			// config for save
			$this -> check_alias = 0;
			parent::__construct();
		}
	
		
		function setQuery(){
			
			// ordering
			$ordering = "";
			if(isset($_SESSION[$this -> prefix.'sort_field']))
			{
				$sort_field = $_SESSION[$this -> prefix.'sort_field'];
				$sort_direct = $_SESSION[$this -> prefix.'sort_direct'];
				$sort_direct = $sort_direct?$sort_direct:'asc';
				$ordering = '';
				if($sort_field)
					$ordering .= " ORDER BY $sort_field $sort_direct, created_time DESC, id DESC ";
					
			}
		
			$where = "  ";
			
			if(isset($_SESSION[$this -> prefix.'keysearch'] ))
			{
				if($_SESSION[$this -> prefix.'keysearch'] )
				{
					$keysearch = $_SESSION[$this -> prefix.'keysearch'];
					$where .= " AND a.name LIKE '%".$keysearch."%' ";
				}
			}
			// manufactory_id
			if(isset($_SESSION[$this -> prefix.'filter0'])){
				$filter = $_SESSION[$this -> prefix.'filter0'];
				if($filter){
					$where .= ' AND a.city_id =  '.$filter.'';
				}
			}
			
			$query = " SELECT a.*
						  FROM 
						  	fs_districts AS a
						  	WHERE 1=1
                        ".$where.
						 $ordering. " ";
			return $query;
		}

		function save($row = array(), $use_mysql_real_escape_string = 1){
			$title = FSInput::get('name');
			if(!$title)
				return false;
			$id = FSInput::get('id',0,'int');	
			$city_id = FSInput::get('city_id','int',0);
			if(!$city_id){
				Errors::_('Bạn phải chọn thành phố');
				return;
			}
			            
            $city_id  = FSInput::get('city_id',0,'int');
//            $record =  $this->get_record('id = '.$city_id.' ' ,''.$this -> table_name_cities.'','name,id,alias');
//            $row['city_id'] = $record ->  id;
//            $row['city_name'] = $record ->  name;
//            $row['city_alias'] = $record ->  alias;
			
			return parent::save($row);
		}

		/*
         * select in category of home
         */
        function get_cities()
        {
            global $db;
            $query = " SELECT a.*
                          FROM 
                            ".$this -> table_name_cities." AS a
                            ORDER BY ordering ";
            $sql = $db->query($query);
            $result = $db->getObjectList();
            return $result;
        }
				function getDistricts2()
        {
            global $db;
            $query = " SELECT *
                          FROM 
                            ".$this -> table_name_districts."
                            ORDER BY ordering ";
            $sql = $db->query($query);
            $result = $db->getObjectList();
            return $result;
        }
	}
	
?>