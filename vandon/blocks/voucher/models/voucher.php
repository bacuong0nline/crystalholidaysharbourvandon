<?php
class VoucherBModelsVoucher extends FSModels
{
	function __construct()
	{
		$fstable = FSFactory::getClass('fstable');
		$this->table_name = $fstable->_('fs_products', 1);
    $this->table_units = $fstable->_('fs_products_units', 1);
    $this->table_discount = $fstable->_('fs_discount', 1);
	}
  function get_record_by_id($id, $table_name = '', $select = '*')
  {
    if (!$id)
      return;
    if (!$table_name)
      $table_name = $this->table_name;
    $query = " SELECT " . $select . "
            FROM " . $table_name . "
            WHERE id = $id ";

    global $db;
    $sql = $db->query($query);
    $result = $db->getObject();
    return $result;
  }
  function getAllVoucher() {
    $date = date("Y-m-d");
    $where = 'show_in_homepage = 1 and expiration_date > "'.$date.'"';
    $query = " SELECT *
          FROM " . $this->table_discount . " 
          WHERE published = 1 and " . $where . "";
    global $db;
    $sql = $db->query($query);
    $result = $db->getObjectList();
    return $result;
  }
  function get_records($where = '', $table_name = '', $select = '*', $ordering = '', $limit = '', $field_key = '')
	{
		$sql_where = " ";
		if ($where) {
			$sql_where .= ' WHERE ' . $where;
		}
		if (!$table_name)
			$table_name = $this->table_name;
		$query = " SELECT " . $select . "
					  FROM " . $table_name . $sql_where;

		if ($ordering)
			$query .= ' ORDER BY ' . $ordering;
		if ($limit)
			$query .= ' LIMIT ' . $limit;

		global $db;
		$sql = $db->query($query);
		if (!$field_key)
			$result = $db->getObjectList();
		else
			$result = $db->getObjectListByKey($field_key);
		return $result;
	}
  function get_record($where = '', $table_name = '', $select = '*')
	{
		if (!$where)
			return;
		if (!$table_name)
			$table_name = $this->table_name;
		$query = " SELECT " . $select . "
					  FROM " . $table_name . "
					  WHERE " . $where;

		global $db;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}
  function getProduct($id)
  {
    $where = "";
    $where .= " AND id = " . $id;
    $query = " SELECT *
          FROM " . $this->table_name . " 
          WHERE published = 1 " . $where . "";
    global $db;
    $sql = $db->query($query);
    $result = $db->getObject();
    return $result;
  }
  function getMeasure($id)
  {
    $where = "";
    $where .= " AND id = " . $id;
    $query = " SELECT *
          FROM " . $this->table_units . " 
          WHERE published = 1 " . $where . "";
    global $db;
    $sql = $db->query($query);
    $result = $db->getObject();
    return $result;
  }
  
}
