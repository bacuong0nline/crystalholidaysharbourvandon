<?php
include 'blocks/voucher/models/voucher.php';
class VoucherBControllersVoucher
{
  function __construct()
  {
  }
  function display($parameters, $title)
  {

    $model = new VoucherBModelsVoucher();

    $quantity = 0;
    $style = $parameters->getParams('style');
    $style = $style ? $style : 'default';

    $total_price = 0;
    $data = array();

    $list_cart = $_SESSION['cart'];
    if (!empty($list_cart))
      foreach ($list_cart as $key1 => $item) {
        $data[$item[0]] = $model->getProduct($item[0]);
        $products_info = $data[$item[0]];
        $total = 0;
        $total = $products_info->price * $item[1];
        $total_price += $total;
      }
    $vouchers = $model->getAllVoucher();
    include 'blocks/voucher/views/voucher/' . $style . '.php';
  }

}
