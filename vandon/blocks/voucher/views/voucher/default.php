<?php
global $tmpl;
$tmpl->addStylesheet('default', 'blocks/cart/assets/css');
$tmpl->addScript('default', 'blocks/cart/assets/js');
$tmpl->addScript('default', 'blocks/voucher/assets/js');

$Itemid = FSInput::get("Itemid");

?>
<div class="modal fade" id="voucher-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalVoucher" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="width: 400px">
      <div class="vouchers-box">
        <div class="title-voucher-modal p-3  border-bottom">
          <p class="text-center font-weight-bold mb-0"><?php echo FSText::_("Khuyến mãi") ?></p>
        </div>
        <div class="coupon m-2">
          <input type="text" placeholder="<?php echo FSText::_("Nhập mã tại đây") ?>" class="voucher" name="voucher">
          <a data-total=<?php echo $total_price ?> data-used="" class="use-voucher"><?php echo FSText::_("Sử dụng") ?></a>
        </div>
        <div class="d-flex align-items-center" style="background-color: #f7f7f7; height: 40px">
          <p class="mb-0 text-uppercas ml-2 font-weight-bold text-uppercase" style="color: #6e6e6e; font-size: 12px"><?php echo FSText::_("Sử dụng ngay") ?></p>
        </div>
        <div class="pl-4 pr-4">
          <?php foreach ($vouchers as $item) { ?>
            <div class="mt-3" style="
                  display: flex;
                  background-image: url(/images/voucher.png); width: 100%;
                  box-shadow: 0 10px 20px rgb(0 0 0 / 10%);
                  border-radius: 8px;
                  background-size: 100% 100%;
                  height: 110px">
              <div class="col-md-4 mt-1 d-flex justify-content-center ml-2">
                <?php if ($item->image) { ?>
                  <img style="width: 80%; height: 80px; object-fit: cover; margin: auto" src="<?php echo $item->image ?>" alt="voucher" />
                <?php } ?>
              </div>
              <div class="col-md-8">
                <p class="ml-2 mt-2 mb-0" style="font-weight: bold; cursor: pointer"><?php echo $item->name ?></p>
                <p class="ml-2 mb-0" style="font-size: 11px"><?php echo FSText::_("Mã: ") . $item->code ?></p>
                <p class="ml-2 mb-0" style="font-size: 11px"><?php echo FSText::_("HSD: ") .  format_date($item->created_time) ?></p>
                <p data-total=<?php echo $total_price ?> data-voucher="<?php echo $item->code ?>" class="use-voucher-modal ml-2 mb-1" style="color: #f68c59; font-size: 13px; cursor: pointer"><?php echo FSText::_("Sử dụng ngay") ?></p>
                <?php if($item->summary) { ?>
                  <p class="ml-2 name-voucher text-right" style="font-size: 11px; cursor: pointer; color: #ccc"><?php echo FSText::_("Xem chi tiết")?></p>
                <?php } ?>
              </div>
            </div>
            <div style="
                display: none;
                font-size: 12px;
                background: #fff5e5;
                border-radius: 10px;
                box-shadow: 0 10px 20px rgb(0 0 0 / 10%);
                min-height: 100px;
                margin-top: 10px;
                padding: 10px;
                max-height: 100px;
                overflow: auto;">
              <p><?php echo $item->summary ? $item->summary : 'Không có mô tả' ?></p>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>

  </div>