<?php global $tmpl;
?>
<div style="position: relative; padding-bottom: 100px;" class="exp-box <?php echo $background == 1 ? 'snowflake1' : 'snowflake2' ?>">
  <div class="container">
    <p class="text-uppercase text-center title-block-1" data-aos="fade-right"><?php echo @$params['title1']  ?></p>
    <h5 class="title text-center text-uppercase title-block" data-aos="fade-left"><?php echo @$params['title'] ?></h5>
    <div class="col-12 d-flex flex-column align-items-center">
      <?php if (@$params['title2']) { ?>
        <p class="text-center font-weight-strong title-block-2"><?php echo @$params['title2'] ?></p>
      <?php } ?>
    </div>
  </div>
  <div class="container">
    <div class="d-flex flex-column flex-md-row">
      <div class="col-md-6" data-aos="fade-right" data-aos-offset="300" data-aos-easing="ease-in-sine">
        <?php echo $data->content ?>
        <div class="d-flex justify-content-center">
          <a style="width: inherit; height: inherit" class="modal-register font-weight-strong btn-register-2 text-uppercase align-items-center p-3">
            <?php echo FSText::_("Đăng ký học thử") ?> &nbsp;&nbsp;&nbsp;
            <svg width="10" height="10" viewBox="0 0 10 10" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path d="M5 0L4.11562 0.884375L7.60625 4.375H0V5.625H7.60625L4.11562 9.11562L5 10L10 5L5 0Z" />
            </svg>
          </a>
        </div>
      </div>
      <div class="col-md-6 right mt-3 mt-md-0" data-aos="fade-left" data-aos-offset="300" data-aos-easing="ease-in-sine">
        <p style="color: #767676" class="font-italic">
          <?php echo @$params['summary'] ?>
        </p>
        <div>
          <?php
          $youtube = $videos[0]->link_video;
          if (!empty($youtube)) {
            $youtube = explode('?v=', $videos[0]->link_video);
            $id_youtube = $youtube[1];
          }
          ?>
          <div class="video-box p-0" style="display: block">
            <?php if ($videos[0]->file_upload) { ?>
              <div class="video-item" data-lg-size="1280-720" data-video='{"source": [{"src":"<?php echo $videos[0]->file_upload ?>", "type":"video/mp4"}], "attributes": {"preload": false, "controls": true}}' data-pinterest-text="Pin it3" data-tweet-text="lightGallery slide  4" data-sub-html="<h4><?php echo $videos[0]->title ?></h4>" title="<?php echo $videos[0]->title ?>">
                <div class="play-btn"></div>
                <img style="width: 100%; height: 338px; object-fit: cover" class="img-responsive first-image" src="<?php echo !$videos[0]->image ? 'https://img.youtube.com/vi/' . $id_youtube . '/0.jpg' : str_replace('original', 'resized', URL_ROOT . $videos[0]->image) ?>" />
              </div>
            <?php } else { ?>
              <div class="video-item" data-lg-size="1280-720" data-src="<?php echo $videos[0]->link_video . '&mute=0'?>" data-pinterest-text="Pin it3" data-tweet-text="lightGallery slide  4" data-sub-html="<h4><?php echo $videos[0]->title ?></h4>" title="<?php echo $videos[0]->title ?>">
                <div class="play-btn"></div>
                <img style="width: 100%; height: 338px; object-fit: cover" class="img-responsive first-image" src="<?php echo !$videos[0]->image ? 'https://img.youtube.com/vi/' . $id_youtube . '/0.jpg' : str_replace('original', 'resized', URL_ROOT . $videos[0]->image) ?>" />
              </div>
            <?php } ?>
          </div>
          <div class="mt-2 video-box" style="display: grid; grid-template-columns: 1fr 1fr 1fr 1fr; grid-gap: 10px">
            <?php foreach ($videos as $key => $item) { ?>
              <?php if ($key > 0) { ?>
                <?php
                $youtube = $item->link_video;
                if (!empty($youtube)) {
                  $youtube = explode('?v=', $item->link_video);
                  $id_youtube = $youtube[1];
                }
                ?>
                <?php if ($item->file_upload) { ?>
                  <div class="video-item <?php echo $key > 4 ? 'd-none' : null ?>" data-lg-size="1280-720" data-video='{"source": [{"src":"<?php echo $item->file_upload ?>", "type":"video/mp4"}], "attributes": {"preload": false, "controls": true}}' data-pinterest-text="Pin it3" data-tweet-text="lightGallery slide  4" data-sub-html="<h4><?php echo $item->title ?></h4>" title="<?php echo $item->title ?>">
                    <div class="play-btn-2"></div>
                    <img style="width: 100%; height: 65px; object-fit: cover" class="img-responsive" src="<?php echo !$item->image ? 'https://img.youtube.com/vi/' . $id_youtube . '/0.jpg' : str_replace('original', 'resized', URL_ROOT . $item->image) ?>" />
                  </div>
                <?php } else { ?>
                  <div class="video-item <?php echo $key > 4 ? 'd-none' : null ?>" data-lg-size="1280-720" data-src="<?php echo $item->link_video . '&mute=0' ?>" data-pinterest-text="Pin it3" data-tweet-text="lightGallery slide  4" data-sub-html="<h4><?php echo $item->title ?></h4>" title="<?php echo $item->title ?>">
                    <div class="play-btn-2"></div>
                    <img style="width: 100%; height: 65px; object-fit: cover" class="img-responsive" src="<?php echo !$item->image ? 'https://img.youtube.com/vi/' . $id_youtube . '/0.jpg' : str_replace('original', 'resized', URL_ROOT . $item->image) ?>" />
                  </div>
                <?php } ?>
              <?php } ?>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php if (@$background == 1) { ?>
    <img class="wave2" src="./images/wave2.png" alt="wave2" />
  <?php } else { ?>
    <img class="wave1" src="./images/wave1.png" alt="wave1" />
  <?php } ?>
</div>

<script>
  window.onload = function() {
    let e = document.getElementsByClassName('video-box');

    for (let item of e) {
      lightGallery(item, {
        thumbnail: true,
        animateThumb: true,
        zoomFromOrigin: false,
        allowMediaOverlap: true,
        toggleThumb: true,
        plugins: [lgThumbnail, lgVideo, lgZoom]
      })
    }
  }
</script>