
$(document).ready(function() {
    $('.history-wrapper').owlCarousel({
        loop: true,
        // margin: 10,
        // nav: true,
        autoplay: true,
        slideBy: 2,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    })
})