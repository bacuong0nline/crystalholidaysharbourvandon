<?php $tmpl;
$tmpl->addStyleSheet('default', 'blocks/central_history/assets/css');

$tmpl->addScript('slide', 'blocks/central_history/assets/js');
?>
<div class="<?php echo @$background ? 'snowflake1' : 'snowflake2' ?>" style="z-index:0; width: 100%; background-size: 100%; background-repeat: no-repeat; background-position: center center; position: relative; padding-bottom: 100px;">
    <div class="container pb-3 snowflake1-box">
        <p class="text-uppercase text-center"><?php echo $params['title1'] ?></p>
        <h5 class="title text-center text-uppercase mb-4"><?php echo $params['title'] ?></h5>
        <p class="text-uppercase text-center"><?php echo $params['title2'] ?></p>
        <div class="d-flex justify-content-center">
            <p style="width: 70%" class="summary-central text-center grey--text">
                <?php echo $params['summary'] ?>
            </p>
        </div>
    </div>
    <div class="d-flex history-wrapper owl-carousel owl-theme">
        <?php foreach ($list as $key => $item) { ?>
            <?php if ($key % 2 == 0) { ?>
                <div>
                    <h5 class="text-uppercase red--text font-weight-bold text-center"><?php echo $item->title ?></h5>
                    <div class="decor-up">
                        <?php echo htmlspecialchars_decode($item->content) ?>
                    </div>
                </div>
            <?php } else { ?>
                <div class="decor-down-wrapper">
                    <h5 class="text-uppercase red--text font-weight-bold text-center title-down"><?php echo $item->title ?></h5>
                    <div class="decor-down">
                        <?php echo htmlspecialchars_decode($item->content)  ?>
                    </div>
                    <img class="polygon-left" src="./images/polygon.svg" alt="polygon" />
                    <img class="polygon-right" src="./images/polygon.svg" alt="polygon" />
                </div>
            <?php } ?>
        <?php } ?>
    </div>

    <?php if (@$background == 1) { ?>
        <img class="wave2" src="./images/wave2.png" alt="wave2" />
    <?php } else { ?>
        <img class="wave1" src="./images/wave1.png" alt="wave1" />
    <?php } ?>
</div>