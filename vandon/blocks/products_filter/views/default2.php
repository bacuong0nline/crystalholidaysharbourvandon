<?php
global $tmpl;
$tmpl->addStylesheet($style, 'blocks/products_filter/assets/css');
$tmpl->addScript($style, 'blocks/products_filter/assets/js');

$filter = FSInput::get('filter');

$key_1 = substr($filter, 0, 1);
if ($key_1 == 'p') {
  if (strpos($filter, ',') == true) {
    $f = substr($filter, 0, strpos($filter, ','));
    $f = trim($f, 'phong-cach,');
  } else {
    $f = explode('-', $filter);
    $f1 = array_shift($f);
    $f2 = array_shift($f);

    $f = implode('-', $f);
  }
} else {
  $f = strstr($filter, ',');
}

$url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
?>
<?php $html_current = ''; ?>
<?php $html_filter = ''; ?>
<?php
// if (count($arr_fields_current)) {
//   // die;
//   foreach ($arr_fields_current as $item) {
//     $html_current .= '<div class="filter filter-' . $item->field_name . '">';
//     $html_current .= '<div class="filter-title">';
//     $html_current .= $item->field_show;
//     $html_current .= '</div>';
//     $buff_filter_id = array_diff($arr_filter_request, array($item->alias));
//     $str_filter_id = implode(",", $buff_filter_id);
//     $link = FSRoute::addParameters('filter', $str_filter_id);
//     $html_current .= '<ul>';
//     $html_current .= '<li class="' . ($filter->has_selected ? 'selected' : '') . '"><a href="' . $link . '" title="' . $item->filter_show . '" >' . $item->filter_show . '</a></li>';
//     $html_current .= '</ul>';
//     $html_current .= '</div>';
//   }
// }
foreach ($arr_filter_by_field as $field_show => $filters) {
  $html_filter .= '<div class="filter filter-' . $filters[0]->field_name . '">';
  $html_filter .= '<div class="filter-title">' . FSText::_($filters[0]->field_show) . '</div>';
  $html_filter .= '<ul>';

  foreach ($filters as $filter) {
    $field_name_alias = str_replace('_', '-', $filter->field_name);
    $str_filter_id = $filter_request ? $filter_request . "," . 'phong-cach' . '-' . $filter->alias : 'phong-cach' . '-' . $filter->alias;
    $str_filter_id = explode(',', $str_filter_id);
    foreach ($str_filter_id as $key => $val) {
      if (in_array($val, $arr_filter_request) && strpos($val, 'phong-cach') !== false)
        unset($str_filter_id[$key]);
    }
    $str_filter_id = implode(',', $str_filter_id);
    $link = FSRoute::addParameters('filter', $str_filter_id);

    $html_filter .= '<li class="check ' . ($filter->has_selected ? 'selected' : '') . '"><a href="' . $link . '" title="' . $filter->filter_show . '" >' . $filter->filter_show . '</a></li>';
  }
  $html_filter .= '</ul>';
  $html_filter .= '</div>';
}
?>
<div class="hidden-xs hidden-sm fs-filter">
  <div id="block-<?php echo $blockId ?>" class="block-products-filter block-products-filter-<?php echo $style ?>">
    <!-- <div class="span3-title">
            Tìm kiếm theo
        </div> -->
    <!--end: .span3-title-->
    <!-- <?php echo $html; ?> -->
    <?php echo $html_current . $html_filter; ?>
    <input id="root_id" type="hidden" value="<?php echo $cat->id ?>" />
  </div>
  <!--end: #block-<?php echo $blockId ?>-->

  <div id="filter-desktop">
    <?php if (!empty($styles)) { ?>
      <div class="row pt-3 m-3 no-gutters filter-item">
        <div class="col-md-2">
          <p class="primary-color font-weight-bold">Phong cách</p>
        </div>
        <div class="col d-flex" style="flex-wrap: wrap">
          <?php foreach ($styles as $item) {
            $str_filter_id = $filter_request ? $filter_request . "," . 'phong-cach' . '-' . $item->alias : 'phong-cach' . '-' . $item->alias;
            $str_filter_id = explode(',', $str_filter_id);

            if (!empty($arr_filter_request))
              foreach ($str_filter_id as $key => $val) {
                $checked = in_array($val, $arr_filter_request);
                if (in_array($val, $arr_filter_request) && strpos($val, 'phong-cach') !== false)
                  unset($str_filter_id[$key]);
              }

            $str_filter_id = implode(',', $str_filter_id);
            $link = FSRoute::addParameters('filter', $str_filter_id);
            // echo $link; die;
          ?>
            <label class="checkbox trademark">
              <a class="remove-link link-filter" href="<?php echo $link ?>">
                <?php echo $item->name ?> <input name="style[]" type="radio" <?php echo @$checked ? 'checked' : null ?>>
                <span class="checkmark"></span>
              </a>
            </label>
          <?php } ?>
        </div>
      </div>
    <?php } ?>
    <?php if (!empty($list_materials)) { ?>
      <div class="row pt-3 m-3 no-gutters filter-item">
        <div class="col-md-2">
          <p class="primary-color font-weight-bold">Chất liệu</p>
        </div>
        <div class="col d-flex" style="flex-wrap: wrap">
          <?php foreach ($list_materials as $item) {
            $str_filter_id = $filter_request ? $filter_request . "," . 'chat-lieu' . '-' . $item->alias : 'chat-lieu' . '-' . $item->alias;
            $str_filter_id = explode(',', $str_filter_id);
            if (!empty($arr_filter_request))
              foreach ($str_filter_id as $key => $val) {
                $checked = in_array($val, $arr_filter_request);
                if (in_array($val, $arr_filter_request) && strpos($val, 'chat-lieu') !== false)
                  unset($str_filter_id[$key]);
              }
            $str_filter_id = implode(',', $str_filter_id);
            $link = FSRoute::addParameters('filter', $str_filter_id);
          ?>
            <label class="checkbox trademark">
              <a class="remove-link link-filter" href="<?php echo $link  ?>">
                <?php echo $item->name ?> <input name="material[]" type="radio" <?php echo @$checked ? 'checked' : null ?>>
                <span class="checkmark"></span>
              </a>
            </label>
          <?php } ?>
        </div>
      </div>
    <?php } ?>
    <?php if (!empty($price_filter)) { ?>
      <div class="row pt-3 m-3 no-gutters filter-item">
        <div class="col-md-2">
          <p class="primary-color font-weight-bold">Khoảng giá</p>
        </div>
        <div class="col d-flex" style="flex-wrap: wrap">
          <?php foreach ($price_filter as $item) {
            $str_filter_id = $filter_request ? $filter_request . "," . 'khoang-gia' . '-' . $item->min . '-' . $item->max : 'khoang-gia' . '-' . $item->min . '-' . $item->max;
            $str_filter_id = explode(',', $str_filter_id);
            if (!empty($arr_filter_request))
              foreach ($str_filter_id as $key => $val) {
                $checked = in_array($val, $arr_filter_request);
                if (in_array($val, $arr_filter_request) && strpos($val, 'khoang-gia') !== false)
                  unset($str_filter_id[$key]);
              }
            $str_filter_id = implode(',', $str_filter_id);
            $link = FSRoute::addParameters('filter', $str_filter_id);
          ?>
            <label class="checkbox trademark">
              <a class="remove-link link-filter" href="<?php echo $link  ?>">
                <?php echo $item->title ?> <input name="price_filter[]" type="radio" <?php echo @$checked ? 'checked' : null ?>>
                <span class="checkmark"></span>
              </a>
            </label>
          <?php } ?>
        </div>
      </div>
    <?php } ?>
    <?php if (!empty($origins)) { ?>
      <div class="row pt-3 m-3 no-gutters filter-item">
        <div class="col-md-2">
          <p class="primary-color font-weight-bold">Xuất xứ</p>
        </div>
        <div class="col d-flex" style="flex-wrap: wrap">
          <?php foreach ($origins as $item) {
            $str_filter_id = $filter_request ? $filter_request . "," . 'xuat-xu' . '-' . $item->alias : 'xuat-xu' . '-' . $item->alias;
            $str_filter_id = explode(',', $str_filter_id);
            if (!empty($arr_filter_request))
              foreach ($str_filter_id as $key => $val) {
                $checked = in_array($val, $arr_filter_request);
                if (in_array($val, $arr_filter_request) && strpos($val, 'xuat-xu') !== false)
                  unset($str_filter_id[$key]);
              }
            $str_filter_id = implode(',', $str_filter_id);
            $link = FSRoute::addParameters('filter', $str_filter_id);
          ?>
            <label class="checkbox trademark">
              <a class="remove-link link-filter" href="<?php echo $link  ?>">
                <?php echo $item->name ?> <input name="origin[]" type="radio" <?php echo @$checked ? 'checked' : null ?>>
                <span class="checkmark"></span>
              </a>
            </label>
          <?php } ?>
        </div>
      </div>
    <?php } ?>
  </div>
  <div id="filter_mobile"></div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
  let dom = `
  <?php if (!empty($styles)) { ?>
  <div class="card">
    <div class="card-header" id="headingOne">
      <h2 class="mb-0">
        <a class="d-flex remove-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          <p class="primary-color font-weight-bold">Phong cách</p>
          <i class="fa fa-angle-down ml-auto style-icon" aria-hidden="true"></i>
        </a>
      </h2>
    </div>
    <div data-icon="style-icon" id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="">
        <div class="col d-flex" style="flex-wrap: wrap">
          <?php foreach ($styles as $item) {
            $str_filter_id = $filter_request ? $filter_request . "," . 'phong-cach' . '-' . $item->alias : 'phong-cach' . '-' . $item->alias;
            $str_filter_id = explode(',', $str_filter_id);

            if (!empty($arr_filter_request))
              foreach ($str_filter_id as $key => $val) {
                $checked = in_array($val, $arr_filter_request);
                if (in_array($val, $arr_filter_request) && strpos($val, 'phong-cach') !== false)
                  unset($str_filter_id[$key]);
              }

            $str_filter_id = implode(',', $str_filter_id);
            $link = FSRoute::addParameters('filter', $str_filter_id);
            // echo $link; die;
          ?>
            <label class="checkbox trademark">
              <a class="remove-link link-filter" href="<?php echo $link ?>">
                <?php echo $item->name ?> <input name="style[]" type="radio" <?php echo @$checked ? 'checked' : null ?>>
                <span class="checkmark"></span>
              </a>
            </label>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
  <?php if (!empty($list_materials)) { ?>
    <div class="card">
      <div class="card-header" id="headingTwo">
        <h2 class="mb-0">
          <a class="remove-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            <p class="primary-color font-weight-bold">Chất liệu</p>
            <i class="fa fa-angle-down ml-auto material-icon" aria-hidden="true"></i>
          </a>
        </h2>
      </div>
      <div data-icon="material-icon" id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
        <div class="">
        <div class="col d-flex" style="flex-wrap: wrap">

          <?php foreach ($list_materials as $item) {
            $str_filter_id = $filter_request ? $filter_request . "," . 'chat-lieu' . '-' . $item->alias : 'chat-lieu' . '-' . $item->alias;
            $str_filter_id = explode(',', $str_filter_id);

            if (!empty($arr_filter_request))
              foreach ($str_filter_id as $key => $val) {
                $checked = in_array($val, $arr_filter_request);
                if (in_array($val, $arr_filter_request) && strpos($val, 'chat-lieu') !== false)
                  unset($str_filter_id[$key]);
              }

            $str_filter_id = implode(',', $str_filter_id);
            $link = FSRoute::addParameters('filter', $str_filter_id);
            // echo $link; die;
          ?>
            <label class="checkbox trademark">
              <a class="remove-link link-filter" href="<?php echo $link ?>">
                <?php echo $item->name ?> <input name="material[]" type="radio" <?php echo @$checked ? 'checked' : null ?>>
                <span class="checkmark"></span>
              </a>
            </label>
          <?php } ?>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
  <?php if ($price_filter) { ?>
    <div class="card">
      <div class="card-header" id="headingThree">
        <h2 class="mb-0">
          <a class="remove-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
            <p class="primary-color font-weight-bold">Khoảng giá</p>
            <i class="fa fa-angle-down ml-auto price-icon" aria-hidden="true"></i>
          </a>
        </h2>
      </div>
      <div data-icon="price-icon" id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
        <div class="">
          <div class="col d-flex" style="flex-wrap: wrap">
            <?php foreach ($price_filter as $item) {
              $str_filter_id = $filter_request ? $filter_request . "," . 'khoang-gia' . '-' . $item->min . '-' . $item->max : 'khoang-gia' . '-' . $item->min . '-' . $item->max;
              $str_filter_id = explode(',', $str_filter_id);
              if (!empty($arr_filter_request))
                foreach ($str_filter_id as $key => $val) {
                  $checked = in_array($val, $arr_filter_request);
                  if (in_array($val, $arr_filter_request) && strpos($val, 'khoang-gia') !== false)
                    unset($str_filter_id[$key]);
                }
              $str_filter_id = implode(',', $str_filter_id);
              $link = FSRoute::addParameters('filter', $str_filter_id);
            ?>
              <label class="checkbox trademark">
                <a class="remove-link link-filter" href="<?php echo $link  ?>">
                  <?php echo $item->title ?> <input name="price_filter[]" type="radio" <?php echo @$checked ? 'checked' : null ?>>
                  <span class="checkmark"></span>
                </a>
              </label>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
  <?php if ($origins) { ?>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h2 class="mb-0">
        <a class="remove-link" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
          <p class="primary-color font-weight-bold">Xuất xứ</p>
          <i class="fa fa-angle-down ml-auto origin-icon" aria-hidden="true"></i>
        </a>
      </h2>
    </div>
    <div data-icon="origin" id="collapseFour" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="">
      <div class="col d-flex" style="flex-wrap: wrap">

        <?php foreach ($origins as $item) {
          $str_filter_id = $filter_request ? $filter_request . "," . 'xuat-xu' . '-' . $item->alias : 'xuat-xu' . '-' . $item->alias;
          $str_filter_id = explode(',', $str_filter_id);

          if (!empty($arr_filter_request))
            foreach ($str_filter_id as $key => $val) {
              $checked = in_array($val, $arr_filter_request);
              if (in_array($val, $arr_filter_request) && strpos($val, 'xuat-xu') !== false)
                unset($str_filter_id[$key]);
            }

          $str_filter_id = implode(',', $str_filter_id);
          $link = FSRoute::addParameters('filter', $str_filter_id);
          // echo $link; die;
        ?>
          <label class="checkbox trademark">
            <a class="remove-link link-filter" href="<?php echo $link ?>">
              <?php echo $item->name ?> <input name="origin[]" type="radio" <?php echo @$checked ? 'checked' : null ?>>
              <span class="checkmark"></span>
            </a>
          </label>
        <?php } ?>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
  `
  $(document).ready(function() {
    if ($(window).width() < 800) {
      $("#filter_mobile").html(dom)
    }


    $(".remove-link").click(function() {
      if ($(".fa", this).hasClass("fa-angle-down")) {
        $('> .fa', this).removeClass('fa-angle-down').addClass('fa-angle-up');
      } else if ($(".fa", this).hasClass("fa-angle-up")) {
        $('> .fa', this).removeClass('fa-angle-up').addClass('fa-angle-down');
      }
    })
    $('input:checked').parents('.collapse').addClass('show')

    $(".show").each((key, item) => {
      $("." + $(item).attr('data-icon')).removeClass('fa-angle-down').addClass('fa-angle-up')
    })
  })
</script>