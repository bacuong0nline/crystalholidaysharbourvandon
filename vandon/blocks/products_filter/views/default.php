<?php
global $tmpl;
$tmpl->addStylesheet($style, 'blocks/products_filter/assets/css');

$filter=FSInput::get('filter');
$key_1=substr($filter, 0, 1);
        if($key_1=='t'){
             if(strpos($filter,',')==true){
                $f=substr($filter,0, strpos($filter, ','));
                $f=trim($f,'thuong-hieu,');
            }else{
                $f=explode('-',$filter);
                $f1=array_shift($f);
                $f2=array_shift($f);

                $f=implode('-',$f);
            }

            
        }else{
            $f=strstr($filter,',');
        }



$url='http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
?>
<?php $html_current = '';?>
<?php $html_filter = '';?>
<?php
if(count($arr_fields_current)){
    // die;
    foreach( $arr_fields_current as $item){
        $html_current .= '<div class="filter filter-'.$item -> field_name.'">';
        $html_current .= '<div class="filter-title">';
        $html_current .= $item -> field_show;
        $html_current .= '</div>';
        $buff_filter_id = array_diff($arr_filter_request,array($item -> alias));
        $str_filter_id = implode(",",$buff_filter_id);
        $link = FSRoute::addParameters('filter',$str_filter_id);
        $html_current .= '<ul>';
        $html_current .= '<li class="'.($filter->has_selected?'selected':'').'"><a href="'.$link.'" title="'.$item ->filter_show.'" >'.$item ->filter_show.'</a></li>';
        $html_current .= '</ul>';
        $html_current .= '</div>';
    }
}
foreach($arr_filter_by_field as $field_show => $filters){
        $html_filter .= '<div class="filter filter-'.$filters[0]->field_name.'">';
        $html_filter .= '<div class="filter-title">'.FSText::_($filters[0]->field_show).'</div>';
        $html_filter .= '<ul>';

        foreach($filters as $filter){
            $field_name_alias = str_replace('_','-',$filter->field_name);
            $str_filter_id = $filter_request?$filter_request.",".$field_name_alias.'-'.$filter -> alias:$field_name_alias.'-'.$filter -> alias;
            $str_filter_id = explode(',', $str_filter_id);
            foreach($str_filter_id as $key=>$val){
                if(in_array($val, $arr_filter_request) && strpos($val, $field_name_alias) !== false)
                    unset($str_filter_id[$key]);
            }
            $str_filter_id = implode(',', $str_filter_id);
            $link = FSRoute::addParameters('filter',$str_filter_id);

            $html_filter .= '<li class="check '.($filter->has_selected?'selected':'').'"><a href="'.$link.'" title="'.$filter ->filter_show.'" >'.$filter ->filter_show.'</a></li>';
        }
        $html_filter .= '</ul>';
        $html_filter .= '</div>';
}
?>
<div class="hidden-xs hidden-sm fs-filter">
    <div id="block-<?php echo $blockId ?>" class="block-products-filter block-products-filter-<?php echo $style ?>">
        <!-- <div class="span3-title">
            Tìm kiếm theo
        </div> --><!--end: .span3-title-->
        <!-- <?php echo $html; ?> -->
        <?php echo $html_current.$html_filter; ?>
        <input id="root_id" type="hidden" value="<?php echo $cat->id?>" />
    </div><!--end: #block-<?php echo $blockId ?>-->

    <div class="filter_th">

        <p class="filter-title">Thương Hiệu</p>
        <ul>
            <?php foreach ($trademark as $item) {
                ?>
                <li class="<?php if($item->alias==$f) echo 'selected'?>">
                    <a href="<?php echo URL_ROOT.($cat?$cat->alias:'san-pham').'/loc-san-pham:thuong-hieu-'.$item->alias ?>"><?php echo $item->name ?></a>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>