<?php
global $tmpl;
$tmpl->addStylesheet('default', 'blocks/news_menu/assets/css');
$code = FSInput::get('ccode');
?>
<div class="list_menu_new">
    <h3 class="title-left-menu"> <i class="fas fa-bars fa-pull-left"></i> <?php echo $title; ?></h3>
    <ul class="tab-content">
        <?php foreach ($list as $item) { ?>
        <li class="<?php echo $code == $item->alias? 'active':'' ?>"><a href="<?php echo FSRoute::_('index.php?module=news&view=cat&ccode='.$item->alias.'&id='.$item->id); ?>"><i class="fas fa-caret-right"></i><?php echo $item->name; ?></a></li>
        <?php } ?>
    </ul>
</div>