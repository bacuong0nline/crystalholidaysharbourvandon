<?php
global $tmpl;
$tmpl->addStylesheet('default', 'blocks/slideshow/assets/css');
//    $tmpl -> addStylesheet('owl.theme.default.min','libraries/jquery/owl.carousel');
//    $tmpl -> addScript('owl.carousel.min','libraries/jquery/owl.carousel');
$tmpl -> addScript('default_home','blocks/slideshow/assets/js');
$i = 0;$j = 0;
?>

<div class="row-design row">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <?php foreach ($list_new2 as $item) { ?>
                <div class="item slick <?php echo ($j == 0) ? 'active' : '' ?>">
                    <a class="" href="<?php echo $item->url; ?>">
                        <img class="img-responsive" src="<?php echo $item->image ?>" alt="<?php echo $item->name; ?>"/>
                    </a>
                </div>
                <?php $j++; ?>
            <?php } ?>
        </div>
        <button class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </button>

        <button class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <i class="fa fa-chevron-right" aria-hidden="true"></i>
        </button>

        <div class="row">
            <?php foreach ($list_new2 as $item) { ?>
                <div class="col-md-5 col-5 <?php echo ($i == 0) ? 'active' : '' ?>" data-target="#myCarousel" data-slide-to="<?php echo $i ?>">
                    <a class="" href="<?php echo $item->url; ?>">
                        <img class="img-responsive" src="<?php echo $item->image ?>" alt="<?php echo $item->name; ?>"/>
                    </a>
                </div>
                <?php $i++; ?>
            <?php } ?>
        </div>
    </div>
</div>