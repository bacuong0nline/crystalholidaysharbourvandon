<?php
global $tmpl;
$tmpl->addStylesheet('style_default', 'blocks/slideshow/assets/css');
//$tmpl->addScript('default_home', 'blocks/slideshow/assets/js');
//$tmpl->addScript('default', 'blocks/slideshow/assets/js');
$i = 0;
$j = 0;
?>
<div class="content-slider">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php foreach ($list_new as $item) { ?>
                <li data-target="#myCarousel" data-slide-to="<?php echo $i ?>" class="<?php echo ($i == 0) ? 'active' : '' ?> " </li>
                <?php $i++ ?>
            <?php } ?>
        </ol>
        <div class="carousel-inner" role="listbox">
            <?php foreach ($list_new as $item) { ?>
                <div class="item <?php echo ($j == 0) ? 'active' : '' ?>">
                    <a class="hot-corner" href="<?php echo $item->url ?>">
                        <img class="img-responsive" src="<?php echo $item->image ?>" alt="<?php echo $item->name; ?>"/>
                    </a>
                </div>
                <?php $j++; ?>
            <?php } ?>
        </div>

    </div>
</div>
