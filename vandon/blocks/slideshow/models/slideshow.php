<?php 
	class SlideshowBModelsSlideshow
	{
		function __construct()
		{
		    $fstable = FSFactory::getClass('fstable');
            $this->table_name = $fstable->_('fs_slideshow',1);
//            $this->table_news = $fstable->_('fs_news');
            $this->table_categories = $fstable->_('fs_slideshow_categories',1);
		}
		
		function get_data($cat_id){
		    $where = '';
            //var_dump($cat_id);
            if($cat_id){
                $where .= ' AND category_id = '. $cat_id;
            }
			$query = '  SELECT id,name
					FROM '. $this->table_categories .'
					WHERE published = 1 '. $where .'
					ORDER BY ordering ';
                    
			global $db;
			$db->query($query);
			$result = $db->getObjectList();
			return $result;
		}

        function get_news(){
            $query = " SELECT id,category_id,image,name
						  FROM ". $this->table_name."
						  WHERE published = 1 AND category_id = 1
						  ";
            global $db;
            $db->query($query);
            $result = $db->getObjectList();
            return $result;
        }
        function get_news2(){
            $query = " SELECT id,category_id,image,name
						  FROM ". $this->table_name."
						  WHERE published = 1 AND category_id = 2
						  ";
            global $db;
            $db->query($query);
            $result = $db->getObjectList();
            return $result;
        }
	}
	
?>