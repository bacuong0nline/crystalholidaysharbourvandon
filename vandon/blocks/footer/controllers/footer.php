<?php
  include 'blocks/footer/models/footer.php';
  class FooterBControllersFooter {
    function __construct() {
    }
    function display($parameters,$title) {

      $model = new FooterBModelsFooter();

      $id = FSInput::get('cid');
      $limit = 4;

      $cat = $model->getContentCategory();
      $cat_product = $model->getCatProduct();
      foreach($cat_product as $item) {
        $item->child = $model->getProducts($item->id);
      }
      foreach($cat as $item) {
        $item->contents = $model->getContents($item->id);
      }

      $style = $parameters->getParams('style');
      $style = $style ? $style : 'default';


      include 'blocks/footer/views/footer/'.$style.'.php';
    }
  }
  function filterProjectPartner($cat, $model, $id, $limit) {
    $i = 0;
    foreach($cat as $item) {
      $project_partner_items = $model->getListProjectPartner($item->id);
      $list_project_partner = array();
      foreach($project_partner_items as $row ) {
        $arrdisplay = explode(',', $row->display_category_id);
        if (in_array($id, $arrdisplay)) {
          array_push($list_project_partner, $row);
        }
        $countItems = count($list_project_partner);
        if ($countItems > 4)  {
          $cat[$i]->pp = array_slice($list_project_partner, 0, $limit);
        } else {
          $cat[$i]->pp = $list_project_partner;
        }
      }
      $i++;
    }
    return $cat;
  }
?>