<?php
  class FooterBModelsFooter extends FSModels {
		function __construct()
		{
      $fstable = FSFactory::getClass('fstable');
      $table_filter = "";

      $this->table_name = $fstable->_('fs_contents',1);
      $this->table_catproduct = $fstable->_('fs_products_categories',1);
      $this->table_categories = $fstable->_('fs_contents_categories',1);
    }
    function getContentCategory() {
      $where = '';
      $query = " SELECT *
      FROM " . $this->table_categories . " 
      WHERE published = 1 ".$where." ORDER BY ordering ASC LIMIT 2";
      global $db;
      $sql = $db->query($query);
      $result = $db->getObjectList();
      return $result;
    }
    function getCatProduct() {
      $where = 'and level = 0 and show_in_homepage = 1';
      $query = " SELECT *
      FROM " . $this->table_catproduct . " 
      WHERE published = 1 ".$where." ORDER BY ordering ASC";
      global $db;
      $sql = $db->query($query);
      $result = $db->getObjectList();
      return $result;
    }
    function getContents($id) {
      $where = "AND category_id = ".$id." ";
      $query = " SELECT *
      FROM " . $this->table_name . " 
      WHERE published = 1 ".$where." ORDER BY created_time ASC LIMIT 7";
      global $db;
      $sql = $db->query($query);
      $result = $db->getObjectList();
      return $result;
    }
    function getProducts($id) {
      $where = "AND parent_id = ".$id." ";
      $query = " SELECT *
      FROM " . $this->table_catproduct . " 
      WHERE published = 1 ".$where." ORDER BY created_time ASC LIMIT 5";
      global $db;
      $sql = $db->query($query);
      $result = $db->getObjectList();
      return $result;
    }
  }

?>