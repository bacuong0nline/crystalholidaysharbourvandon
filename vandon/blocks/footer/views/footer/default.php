<?php
global $tmpl, $config;
$tmpl->addStylesheet('default', 'blocks/projectpartner/assets/css');
$tmpl->addScript('footer', 'blocks/footer/assets/js');

?>
<?php foreach ($cat as $key => $item) { ?>
  <div class="col about-footer">
    <p class="font-weight-bold"><?php echo $item->name ?></p>
    <ul class="pl-3">
      <?php foreach ($item->contents as $row) { ?>
        <li class="font-weight-normal pb-2">
          <a class="remove-link" href="<?php echo FSRoute::_("index.php?module=contents&view=content&id=$row->id&code=$row->alias") ?>"><?php echo $row->title ?></a>
        </li>
      <?php } ?>
    </ul>
    <?php if ($key == 0) { ?>
      <div>
        <?php if ($config['facebook']) { ?>
          <a href="<?php echo $config['facebook'] ?>">
            <i style="color: #999999" class="fa fa-facebook mr-3" aria-hidden="true"></i>
          </a>
        <?php } ?>
        <?php if ($config['youtube']) { ?>
          <a href="<?php echo $config['youtube'] ?>">
            <i style="color: #999999" class="fa fa-youtube-play mr-3" aria-hidden="true"></i>
          </a>
        <?php } ?>
        <?php if ($config['twitter']) { ?>
          <a href="<?php echo $config['twitter'] ?>">
            <i style="color: #999999" class="fa fa-twitter mr-3" aria-hidden="true"></i>
          </a>
        <?php } ?>
        <?php if ($config['instagram']) { ?>
          <a href="<?php echo $config['instagram'] ?>">
            <i style="color: #999999" class="fa fa-instagram mr-3" aria-hidden="true"></i>
          </a>
        <?php } ?>
        <?php if ($config['linkedin']) { ?>
          <a href="<?php echo $config['linkedin'] ?>">
            <i style="color: #999999" class="fa fa-linkedin mr-3" aria-hidden="true"></i>
          </a>
        <?php } ?>

      </div>
    <?php } ?>
  </div>

<?php } ?>