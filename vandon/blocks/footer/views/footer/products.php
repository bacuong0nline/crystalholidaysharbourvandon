<div class="row cat-footer-desktop mt-3" style="border-bottom: 1px solid #eee">
  <?php foreach ($cat_product as $item) { ?>
    <?php if (!empty($item->child)) { ?>
      <div class="col">
        <p class="title-cat-footer font-weight-bold"><?php echo $item->name ?></p>
        <ul class="pl-4">
          <?php foreach ($item->child as $row) { ?>
            <li><a href="<?php echo FSRoute::_("index.php?module=products&view=cat&cid=$row->id&ccode=$row->alias") ?>" class="remove-link"><?php echo $row->name ?></a></li>
          <?php } ?>
        </ul>
        <a href="<?php echo FSRoute::_("index.php?module=products&view=cat&cid=$item->id&ccode=$item->alias") ?>" class="remove-link primary-color">Xem thêm</a>
      </div>
    <?php } ?>
  <?php } ?>
  <div class="col">
    <?php foreach ($cat_product as $item) { ?>
      <?php if (empty($item->child)) { ?>
        <p class="title-cat-footer font-weight-bold"><?php echo $item->name ?></p>
        <a href="<?php echo FSRoute::_("index.php?module=products&view=cat&cid=$item->id&ccode=$item->alias") ?>" class="remove-link primary-color mb-3">Xem thêm</a>
      <?php } ?>
    <?php } ?>
  </div>
</div>

<div class="accordion" id="accordionExample">
  <?php foreach ($cat_product as $key => $item) { ?>
    <div class="card">
      <div class="p-3" id="heading-<?php echo $key ?>">
        <h2 class="mb-0">
          <button class="btn" type="button" data-toggle="collapse" data-target="#collapse-<?php echo $key ?>" aria-expanded="true" aria-controls="collapse-<?php echo $key ?>">
            <b><?php echo $item->name ?></b>
          </button>
          <div class="plus alt"></div>
        </h2>
      </div>
      <div id="collapse-<?php echo $key ?>" class="collapse" aria-labelledby="heading-<?php echo $key ?>" data-parent="#accordionExample">
        <ul>
          <?php foreach ($item->child as $row) { ?>
            <li>
              <a href="<?php echo FSRoute::_("index.php?module=products&view=cat&cid=$row->id&ccode=$row->alias") ?>" class="remove-link p-1"><?php echo $row->name ?></a>
            </li>
          <?php } ?>
        </ul>
      </div>
    </div>
  <?php } ?>
  <?php foreach ($cat as $key=>$val) { ?>
    <div class="card">
      <div class="p-3" id="heading-<?php echo $key ?>">
        <h2 class="mb-0">
          <button class="btn" type="button" data-toggle="collapse" data-target="#collapse-content-<?php echo $key ?>" aria-expanded="true" aria-controls="collapse-<?php echo $key ?>">
            <b><?php echo $val->name ?></b>
          </button>
          <div class="plus alt"></div>
        </h2>
      </div>
      <div id="collapse-content-<?php echo $key ?>" class="collapse" aria-labelledby="heading-<?php echo $key ?>" data-parent="#accordionExample">
        <ul>
          <?php foreach ($val->contents as $row) { ?>
            <li><a class="remove-link p-1" href="<?php echo FSRoute::_("index.php?module=contents&view=content&id=$row->id&code=$row->alias") ?>"><?php echo $row->title ?></a></li>
          <?php } ?>
        </ul>
      </div>
    </div>
  <?php } ?>
</div>