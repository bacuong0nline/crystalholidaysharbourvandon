<?php
class GalleryBModelsGallery
{
	function __construct()
	{
		$fstable = FSFactory::getClass('fstable');
		$this->table_name = $fstable->_('fs_banners', 1);
		$this->table_news = $fstable->_('fs_news', 1);
		$this->table_products_categories = $fstable->_('fs_products_categories', 1);
		$this->table_categories = $fstable->_('fs_banners_categories', 1);
		$this->table_menus = $fstable->_('fs_menus_items', 1);
		$this->table_block = $fstable->_('fs_blocks', 1);
		$this->table_gallery_image = $fstable->_('fs_gallery_images', 1);
		$this->table_video = $fstable->_('fs_videos', 1);

	}

	function get_list($record_id)
	{
		$query = " SELECT *
		FROM " . $this->table_gallery_image . " AS a
		WHERE record_id = '$record_id' order by ordering asc
		";
		global $db;
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}
	function get_list_video($cid)
	{
		$query = " SELECT *
		FROM " . $this->table_video . " AS a
		WHERE parent_id = '$cid' order by ordering asc limit 12
		";
		global $db;
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}
}
