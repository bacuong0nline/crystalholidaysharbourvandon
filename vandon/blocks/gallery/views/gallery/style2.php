<?php $tmpl;
?>

<div class="<?php echo @$background == 1 ? 'snowflake1' : 'snowflake2' ?>" style="width: 100%; background-size: 100%; background-repeat: no-repeat; background-position: center center; position: relative; padding-bottom: 100px;">
    <div class="container pb-3 snowflake1-box">
        <p class="text-uppercase text-center title-block-1"><?php echo $params['title1'] ?></p>
        <h5 class="title text-center text-uppercase mb-4"><?php echo $params['title'] ?></h5>
        <p class="text-uppercase text-center"><?php echo $params['title2'] ?></p>
        <div class="d-flex justify-content-center">
            <p style="width: 70%" class="summary-central text-center grey--text">
                <?php echo $params['summary'] ?>
            </p>
        </div>
    </div>
    <div class="container d-flex flex-wrap">
        <div class="col-12 col-md-6 mt-3 mt-md-0">
            <div class="d-flex justify-content-between videos-box">
                <div style="width: 100%">
                    <?php
                    $youtube = $videos[0]->link_video;
                    if (!empty($youtube)) {
                        $youtube = explode('?v=', $videos[0]->link_video);
                        $id_youtube = $youtube[1];
                    }
                    ?>
                    <div class="video-box">
                        <?php if ($videos[0]->file_upload) { ?>
                            <div class="video-item" data-lg-size="1280-720" data-video='{"source": [{"src":"<?php echo $videos[0]->file_upload ?>", "type":"video/mp4"}], "attributes": {"preload": false, "controls": true}}' data-pinterest-text="Pin it3" data-tweet-text="lightGallery slide  4" data-sub-html="<h4><?php echo $videos[0]->title ?></h4>" title="<?php echo $videos[0]->title ?>">
                                <div class="play-btn"></div>
                                <img style="width: 100%; height: 338px; object-fit: cover" class="img-responsive" src="<?php echo !$videos[0]->image ? 'https://img.youtube.com/vi/' . $id_youtube . '/0.jpg' : str_replace('original', 'resized', URL_ROOT . $videos[0]->image) ?>" />
                            </div>
                        <?php } else { ?>
                            <div class="video-item" data-lg-size="1280-720" data-src="<?php echo $videos[0]->link_video ?>" data-pinterest-text="Pin it3" data-tweet-text="lightGallery slide  4" data-sub-html="<h4><?php echo $videos[0]->title ?></h4>" title="<?php echo $videos[0]->title ?>">
                                <div class="play-btn"></div>
                                <img style="width: 100%; height: 338px; object-fit: cover" class="img-responsive" src="<?php echo !$videos[0]->image ? 'https://img.youtube.com/vi/' . $id_youtube . '/0.jpg' : str_replace('original', 'resized', URL_ROOT . $videos[0]->image) ?>" />
                            </div>
                        <?php } ?>
                    </div>
                    <div class="mt-2 video-box" style="display: grid; grid-template-columns: 1fr 1fr 1fr 1fr; grid-gap: 10px">
                        <?php foreach ($videos as $key => $item) { ?>
                            <?php if ($key > 0) { ?>
                                <?php
                                $youtube = $item->link_video;
                                if (!empty($youtube)) {
                                    $youtube = explode('?v=', $item->link_video);
                                    $id_youtube = $youtube[1];
                                }
                                ?>
                                <?php if ($item->file_upload) { ?>
                                    <div class="video-item <?php echo $key > 4 ? 'd-none' : null ?>" data-lg-size="1280-720" data-video='{"source": [{"src":"<?php echo $item->file_upload ?>", "type":"video/mp4"}], "attributes": {"preload": false, "controls": true}}' data-pinterest-text="Pin it3" data-tweet-text="lightGallery slide  4" data-sub-html="<h4><?php echo $item->title ?></h4>" title="<?php echo $item->title ?>">
                                        <div class="play-btn-2"></div>
                                        <img style="width: 100%; height: 65px; object-fit: cover" class="img-responsive" src="<?php echo !$item->image ? 'https://img.youtube.com/vi/' . $id_youtube . '/0.jpg' : str_replace('original', 'resized', URL_ROOT . $item->image) ?>" />
                                    </div>
                                <?php } else { ?>
                                    <div class="video-item <?php echo $key > 4 ? 'd-none' : null ?>" data-lg-size="1280-720" data-src="<?php echo $item->link_video . '&mute=0' ?>" data-pinterest-text="Pin it3" data-tweet-text="lightGallery slide  4" data-sub-html="<h4><?php echo $item->title ?></h4>" title="<?php echo $item->title ?>">
                                        <div class="play-btn-2"></div>
                                        <img style="width: 100%; height: 65px; object-fit: cover" class="img-responsive" src="<?php echo !$item->image ? 'https://img.youtube.com/vi/' . $id_youtube . '/0.jpg' : str_replace('original', 'resized', URL_ROOT . $item->image) ?>" />
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 mt-3 mt-md-0">
            <div class="grid-exp video-box" id="<?php echo 'gallery' . $params['category_id'] ?>">
                <?php foreach ($gallery as $key => $item) { ?>
                    <a data-lg-size="1600-1067" class="<?php echo $key > 5 ? 'd-none' : null ?> item<?php echo $key + 1 ?> gallery-item" data-src="<?php echo $item->image ?>" data-sub-html="<?php echo $item->title ?>">
                        <img style="width: 100%; object-fit: cover" src="<?php echo $item->image ?>" alt="<?php echo $item->image ?>" />
                    </a>
                <?php } ?>
            </div>
        </div>

    </div>
    <?php if (@$background == 1) { ?>
        <img class="wave2" src="./images/wave2.png" alt="wave2" />
    <?php } else { ?>
        <img class="wave1" src="./images/wave1.png" alt="wave1" />
    <?php } ?>
</div>

<script>
    window.onload = function() {
        let elements = document.getElementsByClassName('video-box');

        for (let item of elements) {
            lightGallery(item, {
                thumbnail: true,
                animateThumb: true,
                // zoomFromOrigin: false,
                // allowMediaOverlap: true,
                toggleThumb: true,
                plugins: [lgThumbnail, lgVideo, lgZoom]
            })
        }
    }
</script>