<?php $tmpl;
?>

<div class="<?php echo @$background == 1 ? 'snowflake1' : 'snowflake2' ?> gallery-3" style="width: 100%; background-size: 100%; background-repeat: no-repeat; background-position: center center; position: relative; padding-bottom: 100px;">
    <div class="container pb-3 snowflake1-box">
        <p class="text-uppercase text-center title-block-1"><?php echo $params['title1'] ?></p>
        <h5 class="title text-center text-uppercase mb-4"><?php echo $params['title'] ?></h5>
        <p class="text-uppercase text-center"><?php echo $params['title2'] ?></p>
        <div class="d-flex justify-content-center">
            <p style="width: 70%" class="summary-central text-center grey--text">
                <?php echo $params['summary'] ?>
            </p>
        </div>
    </div>
    <div class="container">
        <div class="video-box">
            <?php foreach ($videos as $item) { ?>
                <?php if ($item->file_upload) { ?>
                    <a data-lg-size="1280-620" class="video-item" data-video='{"source": [{"src":"<?php echo $item->file_upload ?>", "type":"video/mp4"}], "attributes": {"preload": false, "controls": true}}' data-poster="https://img.youtube.com/vi/egyIeygdS_E/maxresdefault.jpg" data-sub-html="">
                        <div class="play-btn" href="#"></div>
                        <img onerror="this.src='./images/no-image.png'" style="width: 100%; height: 234px; object-fit: cover" class="img-responsive" src="<?php echo $item->image ?>" />
                    </a>
                <?php } else { ?>
                    <a data-lg-size="1280-620" class="video-item" data-src="<?php echo $item->link_video . '&mute=0' ?>" data-sub-html="">
                        <div class="play-btn" href="#"></div>
                        <?php
                        $youtube = $item->link_video;
                        if (!empty($youtube)) {
                            $youtube = explode('?v=', $item->link_video);
                            $id_youtube = $youtube[1];
                        }
                        ?>
                        <img style="width: 100%; height: 234px; object-fit: cover" onerror="this.src='./images/no-image.png'" src="<?php echo !$item->image ? 'https://img.youtube.com/vi/' . $id_youtube . '/0.jpg' : str_replace('original', 'resized', URL_ROOT . $item->image) ?>" alt="<?php echo $item->image ?>" />
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <?php if (@$background == 1) { ?>
        <img class="wave2" src="./images/wave2.png" alt="wave2" />
    <?php } else { ?>
        <img class="wave1" src="./images/wave1.png" alt="wave1" />
    <?php } ?>
</div>

<script>
    window.onload = function() {
        $(".gallery-3").find(".video-box").lightSlider({
            item: $(window).width() < 768 ? 2 : 3,
            slideMargin: 10,
            enableDrag: false,
        });
        let elements = document.getElementsByClassName('video-box');

        for (let item of elements) {
            lightGallery(item, {
                thumbnail: true,
                animateThumb: true,
                // zoomFromOrigin: false,
                // allowMediaOverlap: true,
                toggleThumb: true,
                plugins: [lgThumbnail, lgVideo, lgZoom]
            })
        }
    }
</script>