<?php $tmpl;
?>
<div class="<?php echo @$background == 1 ? 'snowflake1' : 'snowflake2' ?>" style="z-index:0;background-image: url(./images/worldmap.png); width: 100%; background-size: 100%; background-repeat: no-repeat; background-position: center center; position: relative; padding-bottom: 100px;">
    <div class="container pb-3 snowflake1-box">
        <p class="text-uppercase text-center title-block-1" data-aos="fade-right"><?php echo $params['title1'] ?></p>
        <h5 class="title text-center text-uppercase mb-4" data-aos="fade-left"><?php echo $params['title'] ?></h5>
        <p class="text-uppercase text-center"><?php echo $params['title2'] ?></p>
        <div class="d-flex justify-content-center">
            <p style="width: 70%" class="summary-central text-center grey--text">
                <?php echo $params['summary'] ?>
            </p>
        </div>
    </div>
    <div class="container box-criterion">
        <div class="video-box central-box" id="<?php echo 'gallery' . $params['category_id']?>">
            <?php foreach ($gallery as $key => $item) { ?>
                <a 
                data-aos="fade-in"
                data-aos-delay="<?php echo $key * 300?>"
                data-aos-anchor-placement="top-bottom"
                data-lg-size="1600-1067" 
                class="<?php echo $key > 5 ? 'd-none' : null ?> central-<?php echo $key + 1 ?> gallery-item" 
                data-src="<?php echo $item->image?>"
                data-sub-html="<?php echo $item->title?>"
                >
                    <img src="<?php echo $item->image ?>" alt="<?php echo $item->image ?>" />
                </a>
            <?php } ?>
        </div>
        <script>
            window.onload = function(){
                let elements = document.getElementsByClassName('video-box');

                for (let item of elements) {
                    lightGallery(item, {
                    thumbnail: true,
                    animateThumb: true,
                    // zoomFromOrigin: false,
                    // allowMediaOverlap: true,
                    toggleThumb: true,
                    plugins: [lgThumbnail, lgVideo, lgZoom]
                    })
                }
            }

        </script>
    </div>
    <?php if (@$background == 1) { ?>
        <img class="wave2" src="./images/wave2.png" alt="wave2" />
    <?php } else { ?>
        <img class="wave1" src="./images/wave1.png" alt="wave1" />
    <?php } ?>
</div>