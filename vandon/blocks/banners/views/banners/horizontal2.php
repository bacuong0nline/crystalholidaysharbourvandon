<?php foreach ($list as $item) { ?>
  <?php if (@$item->image) { ?>
    <a style="position: relative; display: block" href="<?php echo $item->link ?>">
      <img src="<?php echo '/' . $item->image ?>" alt="" style="max-width: 100%;max-height: 100%;height: inherit !important;">
      <div class="<?php echo $item->content ? "center" : null ?>">
        <?php if($item->content) { ?>
        <div class="animate__animated animate__delay-1s <?php echo $item->effect?>">
          <?php echo $item->content?>
        </div>
        <?php } else { ?>
          <div class="menu-name">
            <p style="font-weight: 600" class="title"><?php echo $menu_name->name?></p>
          </div>
        <?php } ?>
      </div>
    </a>
  <?php } ?>
<?php } ?>
<style>
  .box-image {
    position: absolute
  }
  .menu-name {
    position: absolute;
    font-size: 32px;
    left: 11%;
    top: 50%;
    transform: translate(-50%, -50%);
  }
  .menu-name .title {
    color: white;
    font-weight: 500;

  }
  .center {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
  .center p {
    color: white;
  }
</style>