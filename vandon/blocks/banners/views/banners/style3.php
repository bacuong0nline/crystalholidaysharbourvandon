<?php global $tmpl;
$tmpl->addStyleSheet("flickity", "templates/default/css");
$tmpl->addStyleSheet("flickity-fade", "blocks/banners/assets/css");
$tmpl->addScript("flickity", "templates/default/js");
$tmpl->addScript("flickity-fade", "templates/default/js");
$tmpl->addStyleSheet("banner-home", "blocks/banners/assets/css");
if (!IS_MOBILE) {
    $tmpl->addScript("slide", "blocks/banners/assets/js");
} else {
    $tmpl->addScript("slide-mobile", "blocks/banners/assets/js");
}

// print_r($slide);die;

function link_see_more($link_see_more, $see_more)
{
?>
    <a href="<?php echo $link_see_more ?>" class="">
        <svg style="margin-right: 12px" width="53" height="54" viewBox="0 0 49 50" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M24.6799 49C37.9348 49 48.6799 38.2548 48.6799 25C48.6799 11.7452 37.9348 1 24.6799 1C11.4251 1 0.679932 11.7452 0.679932 25C0.679932 38.2548 11.4251 49 24.6799 49Z" fill="#D3A455" />
            <path d="M24.68 48.0944C37.4347 48.0944 47.7744 37.7548 47.7744 25.0001C47.7744 12.2455 37.4347 1.90576 24.68 1.90576C11.9254 1.90576 1.58569 12.2455 1.58569 25.0001C1.58569 37.7548 11.9254 48.0944 24.68 48.0944Z" stroke="#D3A455" stroke-width="2" />
            <path d="M18.9277 25.5H30.3477" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
            <path d="M24.6377 19.79L30.3477 25.5L24.6377 31.21" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        </svg>
        <span style="color: white"><?php echo @$see_more ?></span>
    </a>
<?php
}
?>



<?php if (!IS_MOBILE) { ?>
    <div class="carousel-wrapper carousel-home">
        <?php if (!empty($slide)) { ?>
            <div class="carousel carousel-main" id="flickity-home">
                <?php foreach (@$slide as $key => $item) { ?>
                    <?php if ($item->file_upload) { ?>
                        <div class="carousel-cell">
                            <div>
                                <div class="summary-banner">
                                    <p class="fw-bold title-banner" style="font-size: 20px; color: white"><span style="font-size:26px;"><?php echo $item->title_banner ?></span></p>
                                    <p class=" content-banner" style="width: 40%; color: white; font-size: 16px"><?php echo $item->content_banner ?></p>
                                    <div class="mt-3 d-flex content-banner">
                                        <?php if (@$item->text_play_video && @$item->see_more) { ?>
                                            <div class="video-box">
                                                <div data-lg-size="1280-720" data-video='{"source": [{"src":"<?php echo $item->link_play_video ?>", "type":"video/mp4"}], "attributes": {"preload": false, "controls": true}}' data-pinterest-text="Pin it3" data-tweet-text="lightGallery slide  4" class="<?php echo FSInput::get("Itemid", 1, 'int') == 1 ? 'btn-style-1' : 'btn-style-2'  ?> me-3">
                                                    <svg style="margin-right: 16px" class="bi bi-caret-right-fill" fill="currentColor" height="16" viewbox="0 0 16 16" width="16" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"></path>
                                                    </svg>
                                                    <?php echo $item->text_play_video ?>
                                                </div>
                                            </div>
                                        <?php } else if (@$item->text_play_video) { ?>
                                            <div class="video-box">
                                                <div data-lg-size="1280-720" data-video='{"source": [{"src":"<?php echo $item->link_play_video ?>", "type":"video/mp4"}], "attributes": {"preload": false, "controls": true}}' data-pinterest-text="Pin it3" data-tweet-text="lightGallery slide  4" class="me-3">
                                                    <svg width="49" height="49" viewBox="0 0 49 49" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M24.3232 48.7861C37.5781 48.7861 48.3232 38.041 48.3232 24.7861C48.3232 11.5313 37.5781 0.786133 24.3232 0.786133C11.0684 0.786133 0.323242 11.5313 0.323242 24.7861C0.323242 38.041 11.0684 48.7861 24.3232 48.7861Z" fill="#D3A455" />
                                                        <path d="M24.3229 47.8806C37.0775 47.8806 47.4172 37.5409 47.4172 24.7862C47.4172 12.0316 37.0775 1.69189 24.3229 1.69189C11.5682 1.69189 1.22852 12.0316 1.22852 24.7862C1.22852 37.5409 11.5682 47.8806 24.3229 47.8806Z" stroke="#D3A455" stroke-width="2" />
                                                        <path d="M29.127 24.7861L21.4127 30.7861V18.7861L29.127 24.7861Z" fill="white" />
                                                    </svg>
                                                    <span style="color: white; margin-left: 13px"><?php echo $item->text_play_video ?></span>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php if (@$item->see_more && $item->text_play_video) { ?>
                                            <a href="<?php echo $item->link_see_more ?>" class="<?php echo $item->text_play_video ? 'btn-style-2' : 'btn-style-1' ?>">
                                                <?php echo @$item->see_more ?>
                                            </a>
                                        <?php } else if (@$item->see_more && !$item->text_play_video) {
                                            link_see_more($item->link_see_more, $item->see_more);
                                        } ?>
                                    </div>
                                </div>
                            </div>
                            <video muted loop preload="metadata" poster="" class="video">
                                <source src="<?php echo $item->file_upload ?>" type="video/mp4" />
                            </video>
                            <div class="desktop remove-line-height">
                                <img src="/images/icon-scroll.svg" alt="Scroll down indicator">
                                <div class="down-arrow">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white" class="bi bi-chevron-down updown" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
                                    </svg>
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="carousel-cell">
                            <div>
                                <div class="summary-banner">
                                    <p class="fw-bold title-banner" style="font-size: 20px; color: white"><span style="font-size:26px;"><?php echo $item->title_banner ?></span></p>
                                    <p class=" content-banner content-banner-summary" style="width: 40%; color: white; font-size: 16px"><?php echo $item->content_banner ?></p>
                                    <div class="mt-3 d-flex content-banner">
                                        <?php if (@$item->text_play_video) { ?>
                                            <div class="video-box">
                                                <div data-lg-size="1280-720" data-video='{"source": [{"src":"<?php echo $item->link_play_video ?>", "type":"video/mp4"}], "attributes": {"preload": false, "controls": true}}' data-pinterest-text="Pin it3" data-tweet-text="lightGallery slide  4" class="<?php echo FSInput::get("Itemid", 1, 'int') == 1 ? 'btn-style-1' : 'btn-style-4'  ?> me-3">
                                                    <svg style="margin-right: 16px" class="bi bi-caret-right-fill" fill="currentColor" height="16" viewbox="0 0 16 16" width="16" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"></path>
                                                    </svg>
                                                    <?php echo $item->text_play_video ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php if (@$item->see_more) { ?>
                                            <!-- <a href="<?php echo $item->link_see_more ?>" class="<?php echo $item->text_play_video ? 'btn-style-2' : 'btn-style-1' ?>">
                                                <?php echo @$item->see_more ?>
                                            </a> -->
                                            <?php
                                            link_see_more($item->link_see_more, $item->see_more);
                                            ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $image = str_replace(['jpeg', 'png', 'jpg'], ['webp', 'webp', 'webp'], $item->image);
                            $image = str_replace('original', 'resized', $image);
                            ?>
                            <img src="<?php echo $image ?>" alt="banner" class="video" />
                            <div class="desktop remove-line-height">
                                <img src="/images/icon-scroll.svg" alt="Scroll down indicator">
                                <div class="down-arrow">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white" class="bi bi-chevron-down updown" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
                                    </svg>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        <?php } ?>
        <?php if (count($slide) > 1) { ?>
            <div class="progress-bar-wrapper">
                <div class="mb-1 prev-slide">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white" class="bi bi-chevron-left" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
                    </svg>
                    <span class="count-slide fw-bold" style="color: white; font-family: arial"></span>
                </div>
                <div class="progress-bar-box">
                    <div class="progress-bar"></div>
                </div>
                <div class="mt-1 d-flex justify-content-end next-slide">
                    <span style="color: white; font-family: arial" class="total-slide fw-bold"><?php echo count($slide) < 9 ? '0' . count($slide) : count($slide) ?></span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white" class="bi bi-chevron-right" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                    </svg>
                </div>
            </div>
        <?php } ?>
    </div>
<?php } else { ?>
    <div class="carousel-wrapper carousel-home">
        <?php if (!empty($slide)) { ?>
            <div class="carousel carousel-main" id="flickity-home-mobile">
                <?php foreach (@$slide as $key => $item) { ?>
                    <?php if ($item->file_upload_mobile) { ?>
                        <div class="carousel-cell">
                            <div>
                                <div class="summary-banner">
                                    <p class="fw-bold title-banner" style="font-size: 20px; color: white"><span style="font-size:26px;"><?php echo $item->title_banner ?></span></p>
                                    <p class=" content-banner" style="width: 80%; color: white; font-size: 16px"><?php echo $item->content_banner ?></p>
                                    <div class="mt-3 d-flex content-banner">
                                        <?php if (@$item->text_play_video && @$item->see_more) { ?>
                                            <div class="video-box">
                                                <div data-lg-size="1280-720" data-video='{"source": [{"src":"<?php echo $item->link_play_video ?>", "type":"video/mp4"}], "attributes": {"preload": false, "controls": true}}' data-pinterest-text="Pin it3" data-tweet-text="lightGallery slide  4" class="<?php echo FSInput::get("Itemid", 1, 'int') == 1 ? 'btn-style-1' : 'btn-style-2'  ?> me-3">
                                                    <svg style="margin-right: 16px" class="bi bi-caret-right-fill" fill="currentColor" height="16" viewbox="0 0 16 16" width="16" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"></path>
                                                    </svg>
                                                    <?php echo $item->text_play_video ?>
                                                </div>
                                            </div>
                                        <?php } else if (@$item->text_play_video) { ?>
                                            <div class="video-box">
                                                <div data-lg-size="1280-720" data-video='{"source": [{"src":"<?php echo $item->link_play_video ?>", "type":"video/mp4"}], "attributes": {"preload": false, "controls": true}}' data-pinterest-text="Pin it3" data-tweet-text="lightGallery slide  4" class="me-3">
                                                    <svg width="49" height="49" viewBox="0 0 49 49" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M24.3232 48.7861C37.5781 48.7861 48.3232 38.041 48.3232 24.7861C48.3232 11.5313 37.5781 0.786133 24.3232 0.786133C11.0684 0.786133 0.323242 11.5313 0.323242 24.7861C0.323242 38.041 11.0684 48.7861 24.3232 48.7861Z" fill="#D3A455" />
                                                        <path d="M24.3229 47.8806C37.0775 47.8806 47.4172 37.5409 47.4172 24.7862C47.4172 12.0316 37.0775 1.69189 24.3229 1.69189C11.5682 1.69189 1.22852 12.0316 1.22852 24.7862C1.22852 37.5409 11.5682 47.8806 24.3229 47.8806Z" stroke="#D3A455" stroke-width="2" />
                                                        <path d="M29.127 24.7861L21.4127 30.7861V18.7861L29.127 24.7861Z" fill="white" />
                                                    </svg>
                                                    <span style="color: white; margin-left: 13px"><?php echo $item->text_play_video ?></span>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php if (@$item->see_more && $item->text_play_video) { ?>
                                            <a href="<?php echo $item->link_see_more ?>" class="<?php echo $item->text_play_video ? 'btn-style-2' : 'btn-style-1' ?>">
                                                <?php echo @$item->see_more ?>
                                            </a>
                                        <?php } else if (@$item->see_more && !$item->text_play_video) {
                                            link_see_more($item->link_see_more, $item->see_more);
                                        } ?>
                                    </div>
                                </div>
                            </div>
                            <video muted="muted" autoplay loop playsinline preload="metadata" poster="" class="video">
                                <source src="<?php echo $item->file_upload_mobile ?>" type="video/mp4" />
                            </video>
                            <div class="desktop remove-line-height">
                                <img src="/images/icon-scroll.svg" alt="Scroll down indicator">
                                <div class="down-arrow">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white" class="bi bi-chevron-down updown" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
                                    </svg>
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="carousel-cell">
                            <div>
                                <div class="summary-banner">
                                    <p class="fw-bold title-banner" style="font-size: 20px; color: white"><span style="font-size:26px;"><?php echo $item->title_banner ?></span></p>
                                    <p class=" content-banner content-banner-summary" style="width: 40%; color: white; font-size: 16px"><?php echo $item->content_banner ?></p>
                                    <div class="mt-3 d-flex content-banner">
                                        <?php if (@$item->text_play_video && @$item->see_more) { ?>
                                            <div class="video-box">
                                                <div data-lg-size="1280-720" data-video='{"source": [{"src":"<?php echo $item->link_play_video ?>", "type":"video/mp4"}], "attributes": {"preload": false, "controls": true}}' data-pinterest-text="Pin it3" data-tweet-text="lightGallery slide  4" class="<?php echo FSInput::get("Itemid", 1, 'int') == 1 ? 'btn-style-1' : 'btn-style-2'  ?> me-3">
                                                    <svg style="margin-right: 16px" class="bi bi-caret-right-fill" fill="currentColor" height="16" viewbox="0 0 16 16" width="16" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"></path>
                                                    </svg>
                                                    <?php echo $item->text_play_video ?>
                                                </div>
                                            </div>
                                        <?php } else if (@$item->text_play_video) { ?>
                                            <div class="video-box">
                                                <div data-lg-size="1280-720" data-video='{"source": [{"src":"<?php echo $item->link_play_video ?>", "type":"video/mp4"}], "attributes": {"preload": false, "controls": true}}' data-pinterest-text="Pin it3" data-tweet-text="lightGallery slide  4" class="me-3">
                                                    <svg width="49" height="49" viewBox="0 0 49 49" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M24.3232 48.7861C37.5781 48.7861 48.3232 38.041 48.3232 24.7861C48.3232 11.5313 37.5781 0.786133 24.3232 0.786133C11.0684 0.786133 0.323242 11.5313 0.323242 24.7861C0.323242 38.041 11.0684 48.7861 24.3232 48.7861Z" fill="#D3A455" />
                                                        <path d="M24.3229 47.8806C37.0775 47.8806 47.4172 37.5409 47.4172 24.7862C47.4172 12.0316 37.0775 1.69189 24.3229 1.69189C11.5682 1.69189 1.22852 12.0316 1.22852 24.7862C1.22852 37.5409 11.5682 47.8806 24.3229 47.8806Z" stroke="#D3A455" stroke-width="2" />
                                                        <path d="M29.127 24.7861L21.4127 30.7861V18.7861L29.127 24.7861Z" fill="white" />
                                                    </svg>
                                                    <span style="color: white; margin-left: 13px"><?php echo $item->text_play_video ?></span>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php if (@$item->see_more) {
                                            link_see_more($item->link_see_more, $item->see_more);
                                        } ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $image = str_replace(['jpeg', 'png', 'jpg'], ['webp', 'webp', 'webp'], $item->image_mobile);
                            $image = str_replace('original', 'resized', $image);
                            ?>
                            <img src="<?php echo $image ?>" alt="banner" class="video" />
                            <div class="desktop remove-line-height">
                                <img src="/images/icon-scroll.svg" alt="Scroll down indicator">
                                <div class="down-arrow">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white" class="bi bi-chevron-down updown" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
                                    </svg>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        <?php } ?>
        <?php if (count($slide) > 1) { ?>
            <div class="progress-bar-wrapper">
                <div class="mb-1 prev-slide">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white" class="bi bi-chevron-left" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
                    </svg>
                    <span class="count-slide fw-bold" style="color: white; font-family: arial"></span>
                </div>
                <div class="progress-bar-box">
                    <div class="progress-bar"></div>
                </div>
                <div class="mt-1 d-flex justify-content-end next-slide">
                    <span style="color: white; font-family: arial" class="total-slide fw-bold"><?php echo count($slide) < 9 ? '0' . count($slide) : count($slide) ?></span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white" class="bi bi-chevron-right" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                    </svg>
                </div>
            </div>
        <?php } ?>
    </div>
<?php } ?>