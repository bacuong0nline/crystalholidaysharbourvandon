<?php global $tmpl;
$tmpl->addStyleSheet("style6", "blocks/banners/assets/css");

// print_r($slide);die;
?>

<div class="banner-style-5">
  <?php foreach ($slide as $item) { ?>
    <?php
    $image = str_replace(['jpeg', 'png', 'jpg'], ['webp', 'webp', 'webp'], $item->image);
    $image = str_replace('original', 'resized', $image);
    ?>
    <div class="d-md-flex container banner-mobile-utilities" style="height: 100%;">
      <div class="left">
        <div class="title-banner">
          <h2 style="" class="mt-4 mt-md-0 mb-md-5 title-american text-cyan text-center text-md-start"><?php echo $item->title_banner?></h2>
          <p class="text-white text-center text-md-start"><?php echo $item->content_banner?></p>
        </div>
      </div>
      <div class="right" style="background: url(<?php echo format_image($item->image)?>)" >
      </div>
    </div>
  <?php } ?>
</div>