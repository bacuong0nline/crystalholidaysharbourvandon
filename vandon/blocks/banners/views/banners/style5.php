<?php global $tmpl;
$tmpl->addStyleSheet("style5", "blocks/banners/assets/css");

// print_r($slide);die;
?>

<div class="banner-style-5">
  <?php foreach ($slide as $item) { ?>
    <?php
    $image = str_replace(['jpeg', 'png', 'jpg'], ['webp', 'webp', 'webp'], $item->image);
    $image = str_replace('original', 'resized', $image);
    ?>
    <div class="d-flex flex-column flex-md-row flex-column-reverse container banner-box" style="height: 100%;">
      <div class="left">
        <div class="title-banner">
          <h2 style="" class="mt-3 mb-3 mb-md-5 title-american text-cyan title-page text-center"><?php echo $item->title_banner?></h2>
          <p class="text-white text-center"><?php echo $item->content_banner ?></p>
        </div>
      </div>
      <div class="right" style="background: url(<?php echo $image?>)">
      </div>
    </div>
  <?php } ?>
</div>