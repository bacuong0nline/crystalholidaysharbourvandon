<div data-aos="fade-up" class="section-1 banner" style="position: relative; background-image: url(<?php echo URL_ROOT . str_replace('original', 'resized', $banner->image) ?>); height: 600px; width: 100%; background-size: cover; background-repeat: no-repeat; background-position: center center;">
    <div class="container d-flex align-items-start banner-box">
        <div class="col-md-6 text-center-mobile" style="color: white">
            <h5 class="text-uppercase mb-4 mt-4 mt-md-0 title-banner-style2" style="font-family: UTM Bebas, sans-serif; font-size: 40px"><?php echo $params['title'] ?></h5>
            <div class="summary-banner-1"><?php echo @$block->summary ?></div>
            <?php if ($params['button']) { ?>
                <a class="modal-register btn-register font-weight-strong">
                    <?php echo FSText::_($params['button']) . '&nbsp;&nbsp;&nbsp;&nbsp;' ?>
                    <svg width="10" height="10" viewBox="0 0 10 10" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M5 0L4.11562 0.884375L7.60625 4.375H0V5.625H7.60625L4.11562 9.11562L5 10L10 5L5 0Z" />
                    </svg>
                </a>
            <?php } ?>
        </div>
        <div class="col-md-6 mt-4 mt-md-0">
                <?php
                $youtube = @$data->link_video;
                if (!empty($youtube)) {
                    $youtube = explode('?v=', @$data->link_video);
                    @$id_youtube = $youtube[1];
                }
                ?>
                <div class="video-box">
                    <?php if (@$data->file_upload) { ?>
                        <div class="video-item" data-lg-size="1280-720" data-video='{"source": [{"src":"<?php echo @$data->file_upload ?>", "type":"video/mp4"}], "attributes": {"preload": false, "controls": true}}' data-pinterest-text="Pin it3" data-tweet-text="lightGallery slide  4">
                            <div class="play-btn"></div>
                            <img style="width: 100%; height: 338px; object-fit: cover" class="img-responsive image-video thumbnail-video" src="<?php echo !@$data->image ? 'https://img.youtube.com/vi/' . @$id_youtube . '/0.jpg' : str_replace('original', 'resized', URL_ROOT . @$data->image) ?>" />
                        </div>
                    <?php } else { ?>
                        <div class="video-item" data-lg-size="1280-720" data-src="<?php echo @$data->link_video . '&mute=0' ?>" data-pinterest-text="Pin it3" data-tweet-text="lightGallery slide  4">
                            <div class="play-btn"></div>
                            <img style="width: 100%; height: 315px; object-fit: cover" class="img-responsive image-video thumbnail-video" src="<?php echo !@$data->image ? 'https://img.youtube.com/vi/' . @$id_youtube . '/0.jpg' : str_replace('original', 'resized', URL_ROOT . @$data->image) ?>" />
                        </div>
                    <?php } ?>
                </div>
            </div>
    </div>
    <img style="position: absolute; bottom: 0px; width: 100%" src="./images/wave1.png" alt="wave1" />
</div>

<script>
    window.onload = function() {
        let elements = document.getElementsByClassName('video-box');

        for (let item of elements) {
            lightGallery(item, {
                thumbnail: true,
                animateThumb: true,
                zoomFromOrigin: false,
                allowMediaOverlap: true,
                toggleThumb: true,
                plugins: [lgThumbnail, lgVideo, lgZoom]
            })
        }
    }
</script>