<div class="row mt-4 justify-content-between banner-bottom-slide-home">
  <?php foreach ($list as $item) { ?>
    <a href="<?php echo $item->link?>" class="pl-0 pr-0">
      <img src="<?php echo URL_ROOT.$item->image?>" alt="<?php echo $item->image?>">
    </a>
  <?php } ?>
</div>