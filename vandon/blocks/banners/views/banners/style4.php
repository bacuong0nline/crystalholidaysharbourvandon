<?php global $tmpl;
$tmpl->addStyleSheet("style4", "blocks/banners/assets/css");

// print_r($slide);die;
?>

<div class="banner-style-4">
    <?php foreach ($slide as $item) { ?>
        <?php
        $image = str_replace(['jpeg', 'png', 'jpg'], ['webp', 'webp', 'webp'], $item->image);
        $image = str_replace('original', 'resized', $image);
        ?>
        <img src="<?php echo $image ?>" alt="<?php echo $item->image ?>">
        <div class="container">
            <div class="title-banner">
                <h2 class="title-american-2 text-uppercase" style="color: white">
                    <?php echo @$item->title_banner ?>
                </h2>
            </div>
        </div>
    <?php } ?>
</div>

<script>
    window.onload = function() {
        let elements = document.getElementsByClassName('video-box');

        for (let item of elements) {
            lightGallery(item, {
                thumbnail: true,
                animateThumb: true,
                zoomFromOrigin: false,
                allowMediaOverlap: true,
                toggleThumb: true,
                plugins: [lgThumbnail, lgVideo, lgZoom]
            })
        }
    }
</script>