

$(document).ready(function () {
    const target = document.getElementById('flickity-home-mobile');
    const videos = target.getElementsByTagName('video');
    const videosLength = videos.length;

    var flkty = new Flickity('.carousel', {
        pageDots: false,
        prevNextButtons: false,
        wrapAround: true,
        fade: true,
        autoPlay: 15000,
        draggable: false,
        on: {
            ready: function () {

                for (let i = 0; i < videosLength; i++) {
                    if (videos[i] !== undefined) {
                        videos[i].play();
                    }
                }
            }
        }
    });



    $(".count-slide").html(`0${flkty.selectedIndex + 1}`)
    $(".total-slide").html(`${flkty.selectedIndex < 9 ? "0" + (flkty.selectedIndex + 2) : flkty.selectedIndex + 2}`)

    flkty.on('select', function () {

        $(".count-slide").html(`${flkty.selectedIndex < 9 ? "0" + (flkty.selectedIndex + 1) : flkty.selectedIndex + 1}`);

        $(".total-slide").html(`${flkty.selectedIndex < 9 ? "0" + (flkty.selectedIndex + 2) : flkty.selectedIndex + 2}`)
        if ((flkty.selectedIndex + 2) > flkty.cells.length) {
            $(".total-slide").html(`01`)
        }
        setTimeout(() => {
            $(".title-banner").addClass("animate__fadeInUp animate__animated");
            $(".content-banner").addClass("animate__fadeInDown animate__animated");
        }, 300)


    });
    $(".title-banner").addClass("animate__fadeInUp animate__animated");
    $(".content-banner").addClass("animate__fadeInDown animate__animated");


    // flkty.on('change', changeSlide);
    // function changeSlide(index) {
    //     console.log(1);
    //     for (let i = 0; i < videosLength; i++) {
    //         videos[i].currentTime = 0;
    //         videos[index].play();
    //     }
    // }
    // flkty.on('scroll', function (progress) {
    //     progress = Math.max(0, Math.min(1, progress));
    //     progressBar.style.width = progress * 100 + '%';
    // });


    var time = 15;
    var $bar,
        $slider,
        isPause,
        tick,
        percentTime;

    $slider = $('.carousel');
    // $slider.flickity({
    //     wrapAround: true,
    // });

    $bar = $('.progress-bar-box .progress-bar');

    // $('.carousel-wrapper').on({
    //   mouseenter: function() {
    //     isPause = true;
    //   },
    //   mouseleave: function() {
    //     isPause = false;
    //   }
    // })


    function startProgressbar() {
        resetProgressbar();
        percentTime = 0;
        isPause = false;
        tick = setInterval(interval, 10);
    }

    function interval() {
        if (isPause === false) {
            percentTime += 1 / (time + 0.1);
            $bar.css({
                width: percentTime + "%"
            });
            if (percentTime >= 100) {
                // $slider.flickity('next', true);
                flkty.next();

                startProgressbar();
            }
        }
    }


    function resetProgressbar() {
        $bar.css({
            width: 0 + '%'
        });
        clearTimeout(tick);
    }


    startProgressbar();


    $(".prev-slide").click(function () {
        flkty.previous();

        $(".title-banner").removeClass("animate__fadeInUp animate__animated");
        $(".content-banner").removeClass("animate__fadeInDown animate__animated");


        startProgressbar();

    })
    $(".next-slide").click(function () {
        flkty.next();

        $(".title-banner").removeClass("animate__fadeInUp animate__animated");
        $(".content-banner").removeClass("animate__fadeInDown animate__animated");

        startProgressbar();
    })

})



window.onload = function() {
    let elements = document.getElementsByClassName('video-box');

    for (let item of elements) {
        lightGallery(item, {
            thumbnail: true,
            animateThumb: true,
            zoomFromOrigin: false,
            allowMediaOverlap: true,
            toggleThumb: true,
            plugins: [lgThumbnail, lgVideo, lgZoom]
        })
    }
}