<?php
$params = array(
	'suffix' => array(
		'name' => 'Hậu tố',
		'type' => 'text',
		'default' => '_banner',
	),
	//		'id' => array(
	//					'name' => 'Id (dùng cho qcáo bên ngoài trang)',
	//					'type' => 'text',
	//					'default' => 'divAdLeft',
	//					'comment' => 'divAdLeft dùng cho bên trái, divAdRight cho bên phải'
	//					),
	'title' => array(
		'name' => 'Tiêu đề',
		'type' => 'text',
		'default' => '',
	),
	// 'summary' => array(
	// 	'name' => 'Nội dung',
	// 	'type' => 'text',
	// 	'default' => '',
	// ),
	'button' => array(
		'name' => 'Nút ấn',
		'type' => 'text',
		'achtung' => 'Để trống sẽ không hiển thị',
		'default' => '',
	),
	'style' => array(
		'name' => 'Style',
		'type' => 'select',
		'achtung' => 'Kiểu 1, Kiểu 2 --> Chỉ cần chọn Video hiển thị + nhập tóm tắt ở trên <br /> Kiểu 3 --> Chỉ cần chọn danh mục banner (Sẽ hiển thị ảnh kèm theo mô tả trong danh mục đó)',
		'value' => array(
			'style1' => 'Kiểu 1',
			'style2' => 'Kiểu 2',
			'style3' => 'Kiểu 3',
			'style4' => 'Kiểu 4',
			// 'vertical-adv' => 'Quảng cáo dọc'
		)
	),
	'category_id_image' => array(
		'name' => 'Chọn danh mục banner',
		'class' => 'category-select',
		'type' => 'select',
		'value' => get_category(),
		// 'attr' => array('multiple' => 'multiple'),
	),
	'category_id' => array(
		'name' => 'Video',
		'class' => 'category-select',
		'type' => 'select',
		'value' => get_video(),
		// 'attr' => array('multiple' => 'multiple'),
	),
	// 'image' => array(
	// 	'name' => 'Ảnh',
	// 	'class' => 'image-select',
	// 	'type' => 'select',
	// 	'value' => get_image(),
	// 	// 'attr' => array('multiple' => 'multiple'),
	// ),
);
function get_category()
{
	global $db;
	$query = " SELECT name, id
						FROM fs_banners_categories
						";
	$sql = $db->query($query);
	$result = $db->getObjectList();
	if (!$result)
		return;
	$arr_group = array();
	foreach ($result as $item) {
		$arr_group[$item->id] = $item->name;
	}
	return $arr_group;
}
function get_category_video()
{
	global $db;
	$fstable = FSFactory::getClass('fstable');
	$table_name = $fstable->_('fs_videos_categories', 1);

	$query = " SELECT name, id
						FROM $table_name
						";
	$sql = $db->query($query);
	$result = $db->getObjectList();
	if (!$result)
		return;
	$arr_group = array();
	foreach ($result as $item) {
		$arr_group[$item->id] = $item->name;
	}
	return $arr_group;
}
function get_video()
{
	global $db;
	$fstable = FSFactory::getClass('fstable');
	$table_name = $fstable->_('fs_videos', 1);

	$query = " SELECT title, id, image, link_video, file_upload
						FROM $table_name where published = 1
						";
	$sql = $db->query($query);
	$result = $db->getObjectList();
	if (!$result)
		return;
	$arr_group = array();
	foreach ($result as $item) {
		$arr_group[$item->id] = $item->title;
	}
	return $arr_group;
}
?>
