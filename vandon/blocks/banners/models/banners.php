<?php
class BannersBModelsBanners
{
	function __construct()
	{
		$fstable = FSFactory::getClass('fstable');
		$this->table_name = $fstable->_('fs_banners', 1);
		$this->table_news = $fstable->_('fs_news', 1);
		$this->table_news_cat = $fstable->_('fs_news_categories', 1);

		$this->table_products_categories = $fstable->_('fs_products_categories', 1);
		$this->table_categories = $fstable->_('fs_banners_categories', 1);
		$this->table_menus = $fstable->_('fs_menus_items', 1);
		$this->table_videos = $fstable->_('fs_videos', 1);
		$this->fs_education_categories = $fstable->_('fs_education_categories', 1);
		$this->table_block = $fstable->_('fs_blocks', 1);

	}
	function getList($category_id, $cat_home = null)
	{
		$where = '';
		if ($category_id) {
			$where .= ' AND category_id = ' . $category_id . ' ';
		}
		// return;
		$module = FSInput::get('module');
		$view = FSInput::get('view');
		$ccode = FSInput::get('ccode');
		$cid = FSInput::get('cid');
		//			$cat = $this->get_cats($ccode);
		$filter = FSInput::get('filter');

		$where2 = '';
		$Itemid = FSInput::get('Itemid', 1, 'int');
		$where .= " AND (listItemid = 'all'
							OR listItemid like '%,$Itemid,%')" . $where2;

		$query = ' SELECT *
						  FROM ' . $this->table_name . ' AS a
						  WHERE published = 1
						 ' . $where . ' ORDER BY ordering, id';
		global $db;
		// print_r($query);die;

		$db->query($query);
		$list = $db->getObjectList();
		return $list;
	}
	function getListAdvertisment() {
		$query = " SELECT *
		FROM " . $this->table_name . " AS a
		WHERE category_id = 4
		";
		global $db;
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}
	function get_info_menu($itemId)
	{
		$query = " SELECT name
		FROM " . $this->table_menus . " AS a
		WHERE id = $itemId
		";
		global $db;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}
	function getList2($products_categories)
	{
		$where = '';
		if (!$products_categories)
			return;
		$where .= " AND products_categories like '%,$products_categories,%' ";
		$module = FSInput::get('module');
		$view = FSInput::get('view');
		$ccode = FSInput::get('ccode');
		$filter = FSInput::get('filter');

		// Itemid
		$Itemid = FSInput::get('Itemid', 1, 'int');
		$where .= " AND (listItemid = 'all'
							OR listItemid like '%,$Itemid,%')
							";

		$query = ' SELECT name,id,category_id,type,image,flash,content,link,height,width,is_news
						  FROM ' . $this->table_name . ' AS a
						  WHERE published = 1
						 ' . $where . ' ORDER BY ordering, id ';
		global $db;
		$db->query($query);
		$list = $db->getObjectList();
		// print_r($list);die;
		return $list;
	}
	function get_news()
	{
		$query = " SELECT id,title,alias,category_id,category_name,category_alias
						  FROM " . $this->table_news . " AS a
						  WHERE published = 1 LIMIT 2
						  ";
		global $db;
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}

	function get_info_categories($id_category)
	{
		if (!$id_category)
			return;
		$query = " SELECT id,name,alias,summary
							FROM " . $this->table_products_categories . " AS a
							WHERE published = 1 AND id = $id_category
							";
		global $db;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}
	function getBannerFooter()
	{
		$Itemid = FSInput::get('Itemid', 1, 'int');

		$where = '';
		$where .= " AND (listItemid = 'all'
					OR listItemid like '%,$Itemid,%')
					";
		$query = " SELECT id,name,alias, image, link
						FROM " . $this->table_name . " AS a
						WHERE published = 1 AND category_id = 16 " . $where . "";

		global $db;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}

	function get_news2()
	{
		$query = " SELECT id,title,alias,category_id,category_name,category_alias
						  FROM " . $this->table_news . " AS a
						  WHERE published = 1 AND show_in_homepage = 1 order by id desc LIMIT 6
						  ";
		global $db;
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}
	function get_data($id)
	{
		$query = " SELECT *
		FROM " . $this->table_videos . " AS a
		WHERE published = 1 AND id = $id ";

		global $db;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}
	function get_cat($id)
	{
		$query = " SELECT *
		FROM " . $this->table_news_cat . " AS a
		WHERE published = 1 AND id = $id ";

		global $db;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}
	function get_slide($cid)
	{
		$query = " SELECT *
		FROM " . $this->table_name . " AS a
		WHERE published = 1 AND category_id = $cid order by ordering ASC";

		global $db;
		// echo $query;die;
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}
	function get_banner()
	{
		$item_id = FSInput::get("Itemid");
		$query = " SELECT image, content
		FROM " . $this->table_name . " AS a
		WHERE published = 1 AND listItemid like '%,$item_id,%' ";

		global $db;
		// echo $query;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}
	function get_data_block($id) {
		$query = " SELECT summary
		FROM " . $this->table_block . " AS a
		WHERE published = 1 AND id = $id";

		global $db;
		// echo $query;die;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}
	function get_education()
	{
		$cid = FSInput::get("cid");
		$query = " SELECT image
		FROM " . $this->fs_education_categories . " AS a
		WHERE published = 1 AND id = $cid ";

		global $db;
		// echo $query;die;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}
}
