<?php
class FormBModelsForm
{
	function __construct()
	{
		$fstable = FSFactory::getClass('fstable');
		$this->table_name = $fstable->_('fs_comments', 1);
		$this->table_comments_categories = $fstable->_('fs_comments_categories', 1);
		$this->table_products_categories = $fstable->_('fs_products_categories', 1);
		// $this->table_categories = $fstable->_('fs_education_categories', 1);
		$this->table_menus = $fstable->_('fs_menus_items', 1);
		$this->table_video = $fstable->_('fs_videos', 1);
		$this->table_central = $fstable->_('fs_address', 1);
		$this->table_education_categories = $fstable->_('fs_education_categories',1);
	}
	function get_list_video($cid)
	{
		$query = " SELECT *
		FROM " . $this->table_video . " AS a
		WHERE parent_id = '$cid' order by ordering asc limit 12
		";
		global $db;
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}
	function get_education_categories() {
		$query = " SELECT *
		FROM " . $this->table_education_categories . " AS a
		WHERE level > 0
		";
		global $db;
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}
	function get_central_system()
    {
        global $db;
        $query = " SELECT * FROM $this->table_central where published = 1";
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }
	function getList($category_id, $cat_home = null)
	{
		$where = '';
		if ($category_id) {
			$where .= ' AND category_id = ' . $category_id . ' ';
		}
		// return;
		$module = FSInput::get('module');
		$view = FSInput::get('view');
		$ccode = FSInput::get('ccode');
		$cid = FSInput::get('cid');
		//			$cat = $this->get_cats($ccode);
		$filter = FSInput::get('filter');

		$where2 = '';

		$Itemid = FSInput::get('Itemid', 1, 'int');
		$where .= " AND (listItemid = 'all'
							OR listItemid like '%,$Itemid,%')" . $where2;

		$query = ' SELECT *
						  FROM ' . $this->table_name . ' AS a
						  WHERE published = 1
						 ' . $where . ' ORDER BY ordering, id';
		global $db;

		$db->query($query);
		$list = $db->getObjectList();
		return $list;
	}

	function get_list_comment($cid)
	{
		if (!$cid)
			return;
		$query = " SELECT *
						FROM " . $this->table_name . " AS a
						WHERE published = 1 AND category_id = $cid limit 12
						";
		// echo $query;die;
		global $db;
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}



}
