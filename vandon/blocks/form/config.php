<?php
$params = array(
	'suffix' => array(
		'name' => 'Hậu tố',
		'type' => 'text',
		'default' => '_form',
	),
	//		'id' => array(
	//					'name' => 'Id (dùng cho qcáo bên ngoài trang)',
	//					'type' => 'text',
	//					'default' => 'divAdLeft',
	//					'comment' => 'divAdLeft dùng cho bên trái, divAdRight cho bên phải'
	//					),
	'title1' => array(
		'name' => 'Tiêu đề phụ 1',
		'type' => 'text',
		'default' => '',
	),
	'title' => array(
		'name' => 'Tiêu đề chính',
		'type' => 'text',
		'default' => '',
	),
	'title2' => array(
		'name' => 'Tiêu đề phụ 2',
		'type' => 'text',
		'default' => '',
	),
	'summary' => array(
		'name' => 'Tóm tắt',
		'type' => 'text',
		'default' => '',
	),
	'background' => array(
		'name' => 'Màu nền',
		'type' => 'select',
		'value' => array(
			1 => 'Trắng',
			2 => 'Xám'
		)
	),
	'style' => array(
		'name' => 'Style',
		'type' => 'select',
		'achtung' => 'Kiểu 1, Kiểu 2 -> Không cần chọn nhóm cảm nghĩ và video <br /> Kiểu 3 -> Cần chọn nhóm cảm nghĩ chia sẻ <br /> Kiểu 4 -> Cần chọn nhóm video',

		'value' => array(
			'style1' => 'Kiểu 1',
			'style2' => 'Kiểu 2',
			'style3' => 'Kiểu 3',
			'style4' => 'Kiểu 4'
		)
	),

	'category_id' => array(
		'name' => 'Cảm nghĩ và chia sẻ',
		'type' => 'select',
		'value' => get_category(),
		// 'attr' => array('multiple' => 'multiple'),
	),
	'category_id2' => array(
		'name' => 'Nhóm thư viện video',
		'class' => 'category-select',
		'type' => 'select',
		'value' => get_category_video(),
	),
);
function get_category_video()
{
	global $db;
	$fstable = FSFactory::getClass('fstable');
	$table_name = $fstable->_('fs_videos_categories', 1);

	$query = " SELECT name, id
						FROM $table_name
						";
	$sql = $db->query($query);
	$result = $db->getObjectList();
	if (!$result)
		return;
	$arr_group = array();
	foreach ($result as $item) {
		$arr_group[$item->id] = $item->name;
	}
	return $arr_group;
}
function get_category()
{
	$fstable = FSFactory::getClass('fstable');
	$table_name = $fstable->_('fs_comments', 1);
	$table_comments_categories = $fstable->_('fs_comments_categories', 1);

	global $db;
	$query = " SELECT name, id
						FROM $table_comments_categories where level = 0
						";
	$sql = $db->query($query);
	$result = $db->getObjectList();
	if (!$result)
		return;
	$arr_group = array();
	foreach ($result as $item) {
		$arr_group[$item->id] = $item->name;
	}
	return $arr_group;
}
function get_image()
{
	global $db;
	$query = " SELECT name, id, image
						FROM fs_banners
						";
	$sql = $db->query($query);
	$result = $db->getObjectList();
	if (!$result)
		return;
	$arr_group = array();
	foreach ($result as $item) {
		$arr_group[$item->id] = $item->name;
	}
	return $arr_group;
}
?>
