$(document).ready(function() {
    $("#fedu2").change(function() {
        if($(this).val() == "ielts/toeic" || $(this).val() == "cambridge") {
            $(".other-option").addClass("d-none");
            $(".osir").removeClass("d-none");
            $(".other-option").first().removeAttr("selected");
            $(".osir").attr("selected", "true");
        } else {
            $(".other-option").removeClass("d-none");
            $(".other-option").first().attr("selected", "true");
            $(".osir").addClass("d-none");
            $(".osir").removeAttr("selected", "false");

        }
    })


    $(".select-edu").change(function() {
        if($(this).val() == "ielts/toeic" || $(this).val() == "cambridge") {
            $(".other-option").addClass("d-none");
            $(".osir").removeClass("d-none");
            $(".other-option").first().removeAttr("selected");
            $(".osir").attr("selected", "true");
        } else {
            $(".other-option").removeClass("d-none");
            $(".other-option").first().attr("selected", "true");
            $(".osir").addClass("d-none");
            $(".osir").removeAttr("selected", "false");

        }
    })
    // $("#fcentral2").change(function() {
    //     if($(this).val() == 19) {
    //         $(".other-edu").addClass("d-none");
    //     } else {
    //         $(".other-edu").removeClass("d-none");
    //     }
    // })
})