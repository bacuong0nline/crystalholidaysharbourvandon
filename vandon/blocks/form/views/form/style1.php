<?php
global $tmpl, $config;
$tmpl->addStylesheet('form', 'blocks/form/assets/css');
?>
<div class="<?php echo $background == 1 ? 'snowflake1' : 'snowflake2' ?> form-block-style1" style="width: 100%; background-size: 100%; position: relative; padding-bottom: 100px;">
    <div class="container pb-3">
        <p class="text-uppercase text-center title-block-1" data-aos="fade-right"><?php echo @$params['title1']  ?></p>
        <h5 class="title text-center text-uppercase title-block" data-aos="fade-left"><?php echo @$params['title'] ?></h5>
        <div class="col-12 d-flex flex-column align-items-center">
            <?php if (@$params['title2']) { ?>
                <p class="text-center font-weight-strong title-block-2"><?php echo @$params['title2'] ?></p>
            <?php } ?>

        </div>
        <p class="text-center">
            <svg style="color: #999999" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt-fill" viewBox="0 0 16 16">
                <path d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 1 0-6 3 3 0 0 1 0 6z" />
            </svg>
            <span class="font-weight-strong"><?php echo FSText::_("Trụ sở chính: ") ?></span>
            <span><?php echo $config['central'] ?></span>
        </p>
        <div class="d-flex justify-content-center">
            <p class="text-center mr-3">
                <svg style="color: #999999" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-phone-fill" viewBox="0 0 16 16">
                    <path d="M3 2a2 2 0 0 1 2-2h6a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V2zm6 11a1 1 0 1 0-2 0 1 1 0 0 0 2 0z" />
                </svg>
                <span class="font-weight-strong"><?php echo FSText::_("Điện thoại: ") ?></span>
                <span><?php echo $config['hotline'] ?></span>
            </p>
            <p class="text-center">
                <svg style="color: #999999" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-envelope-fill" viewBox="0 0 16 16">
                    <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z" />
                </svg>
                <span class="font-weight-strong"><?php echo  FSText::_("Email: ") ?></span>
                <span><?php echo $config['mail_support'] ?></span>
            </p>
        </div>

        <?php if (@$params['summary']) { ?>
            <div class="d-flex justify-content-center">
                <p class="text-center summary2" style="width: 76% !important"><?php echo @$params['summary'] ?></p>
            </div>
        <?php } ?>

    </div>
    <div class="container">
        <form class="form-register m-auto" id="form1-contact" method="POST" action="index.php?module=contact&view=contact&task=save_contact2" style="width: 700px;">
            <div class="d-flex mt-3">
                <div class="col-6 pr-0">
                    <div class="input-field">
                        <input type="text" name="form1-fullname" id="form1-fullname" placeholder="<?php echo FSText::_("Họ và tên") ?>" />
                    </div>
                    <div class="input-field">
                        <input type="text" name="form1-email" id="form1-email" placeholder="Email" />
                    </div>
                </div>
                <div class="col-6">
                    <div class="input-field">
                        <input type="text" name="form1-phone" id="form1-phone" placeholder="<?php echo FSText::_("Số điện thoại") ?>" />
                    </div>
                    <div class="input-field">
                        <input type="text" name="form1-address" id="form1-address" placeholder="<?php echo FSText::_("Địa chỉ") ?>" />
                    </div>
                </div>
            </div>
            <div class="col-12 mt-3">
                <div class="input-field">
                    <input type="text" name="form1-topic" id="form1-topic" placeholder="<?php echo FSText::_("Chủ đề") ?> " />
                </div>
            </div>
            <div class="col-12 mt-3">
                <textarea placeholder="<?php echo FSText::_("Nội dung")?>" name="form1-content" id="form1-content" cols="30" style="width: 100%; height: 100px"></textarea>
            </div>
            <a class="form1-contact btn-register-5 mt-2"><?php echo FSText::_("Gửi liên hệ") ?></a>
        </form>
    </div>
    <?php if (@$background == 1) { ?>
        <img class="wave2" src="./images/wave2.png" alt="wave2" />
    <?php } else { ?>
        <img class="wave1" src="./images/wave1.png" alt="wave1" />
    <?php } ?>
</div>