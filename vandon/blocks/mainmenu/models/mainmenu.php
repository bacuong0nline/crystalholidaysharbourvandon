<?php

class MainMenuBModelsMainMenu {

    function __construct() {
        $fstable = FSFactory::getClass('fstable');
        $this->table_name = $fstable->_('fs_products','1');
//        if($_SESSION['lang'] == 'vi'){
//            $this->table = 'fs_menus_items';
//        }else{
//            $this->table = 'fs_menus_items_en';
//        }
        $this->table = $fstable->_('fs_menus_items','1');
    }

    function getList($group) {
        if (!$group)
            return;
        global $db;

        $sql = " SELECT id,image,link, name, level, parent_id as parent_id, 
                            target, description,is_type,is_link,summary,bk_color,icon
					        FROM " . $this->table . "
					        WHERE published  = 1 and id != 1
						    AND group_id = $group 
					        ORDER BY ordering
                    ";
        $db->query($sql);
        $result = $db->getObjectList();
        $tree_class = FSFactory::getClass('tree', 'tree/');
        return $list = $tree_class->indentRows($result, 3);
    }

    function getListMobile2() {
        global $db;

        $sql = " SELECT id,image,link, name, level, parent_id as parent_id, 
                            target, description,is_type,is_link,summary,bk_color,icon
					        FROM " . $this->table . "
					        WHERE published  = 1 and id != 1
					        ORDER BY ordering
                    ";
        $db->query($sql);
        $result = $db->getObjectList();
        $tree_class = FSFactory::getClass('tree', 'tree/');
        return $list = $tree_class->indentRows($result, 3);
    }
    function get_child($parent) {
        if (!$parent)
            return;
        global $db;

        $sql = " SELECT *
					        FROM " . $this->table . "
					        WHERE published  = 1
						    AND parent_id = $parent 
					        ORDER BY ordering
                    ";
        $db->query($sql);
        $result = $db->getObjectList();
        $tree_class = FSFactory::getClass('tree', 'tree/');
        return $result;
    }
    function getListMobile($group) {
        if (!$group)
            return;
        global $db;

        $sql = " SELECT id,image,link, name, level, parent_id as parent_id, 
                            target, description,is_type,is_link,summary,bk_color,icon, alias
					        FROM " . $this->table . "
					        WHERE published  = 1 and level = 0 and id != 1
						    AND group_id = $group 
					        ORDER BY ordering
                    ";

        $db->query($sql);
        $result = $db->getObjectList();
        $tree_class = FSFactory::getClass('tree', 'tree/');
        return $list = $tree_class->indentRows($result, 3);
    }
    function getList2($group) {
        if (!$group)
            return;
        global $db;

        $sql = " SELECT id,image,link, name, level, parent_id as parent_id, 
                            target, description,is_type,is_link,summary,bk_color,icon, alias
					        FROM " . $this->table . "
					        WHERE published  = 1 and level = 0 and id != 1 and id != 77
						    AND group_id = $group 
					        ORDER BY ordering
                    ";

        $db->query($sql);
        $result = $db->getObjectList();
        $tree_class = FSFactory::getClass('tree', 'tree/');
        return $list = $tree_class->indentRows($result, 3);
    }
    function getList3($group) {
        if (!$group)
            return;
        global $db;

        $sql = " SELECT id,image,link, name, level, parent_id as parent_id, 
                            target, description,is_type,is_link,summary,bk_color,icon, alias
					        FROM " . $this->table . "
					        WHERE published  = 1 and list_parent in (0,77,0)
						    AND group_id = $group 
					        ORDER BY ordering
                    ";
        // echo $sql;die;
        $db->query($sql);
        $result = $db->getObjectList();
        $tree_class = FSFactory::getClass('tree', 'tree/');
        return $list = $tree_class->indentRows($result, 3);
    }
}

?>