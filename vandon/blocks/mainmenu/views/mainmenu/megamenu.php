<?php
global $tmpl;
$tmpl->addScript('styles', 'blocks/mainmenu/assets/js');
$tmpl->addStylesheet('styles', 'blocks/mainmenu/assets/css');
$lang = FSInput::get('lang');
$Itemid = FSInput::get('Itemid');
?>

<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="list_menu">
    <div class="list-menu-product-show">
        <ul id='megamenu' class="menu <?php echo ($Itemid != 1) ? 'selected' : ''; ?>">
            <?php $i = 0; ?>
            <?php foreach ($level_0 as $key => $item) : ?>
                <?php $link = $item->link ? FSRoute::_($item->link) : ''; ?>
                <?php $class = 'level_0'; ?>
                <?php $class .= $item->description ? ' long ' : ' sort' ?>
                <?php if ($arr_activated[$item->id]) $class .= ' activated '; ?>

                <?php
                $has_child = (isset($children[$item->id]) && count($children[$item->id]));
                if ($has_child) {
                    $class .= ' has-child';
                }
                ?>
                <li style="position: relative" class="<?php echo $class; ?> <?php echo $item->is_type ? 'mega-menu' : '' ?>">
                    <a style="width: 80%; display: block;" href="<?php echo $link; ?>" id="menu_item_<?php echo $item->id; ?>" class="menu_item_a" title="<?php echo htmlspecialchars($item->name); ?>">
                        <?php echo $item->name; ?>
                    </a>
                    <?php
                    if ($has_child) { ?>
                        <span style="position: absolute; right: 10px; top: 3px;">
                            <a data-bs-toggle="collapse" href="#collapseExample<?php echo $key ?>" role="button" aria-expanded="false" aria-controls="collapseExample<?php echo $key ?>">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-down" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
                                </svg>
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-up" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M7.646 4.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1-.708.708L8 5.707l-5.646 5.647a.5.5 0 0 1-.708-.708l6-6z" />
                                </svg>
                            </a>
                        </span>
                    <?php } ?>
                    <!--	LEVEL 1			-->
                    <?php if (isset($children[$item->id]) && count($children[$item->id])) { ?>
                        <ul class="collapse collapse-child" id="collapseExample<?php echo $key ?>">
                        <?php } ?>
                        <?php if (isset($children[$item->id]) && count($children[$item->id])) { ?>
                            <?php $j = 0; ?>
                            <?php foreach ($children[$item->id] as $key => $child) { ?>
                                <?php $link = $child->link ? FSRoute::_($child->link) : ''; ?>
                                <li class='sub-menu-level1 <?php if ($arr_activated[$child->id]) $class .= ' activated '; ?> '>
                                    <a href="<?php echo $link; ?>" class="sub-menu-item" id="menu_item_<?php echo $child->id; ?>" title="<?php echo htmlspecialchars($child->name); ?>">
                                        <?php echo $child->name; ?>
                                    </a>
                                </li>
                                <?php $j++; ?>
                            <?php } ?>
                        <?php } ?>
                        <?php if (isset($children[$item->id]) && count($children[$item->id])) { ?>
                        </ul>
                    <?php } ?>
                </li>
                <?php $i++; ?>
            <?php endforeach; ?>
            <!--	CHILDREN				-->
        </ul>
    </div>
</div>