<?php
global $tmpl, $config;
// $tmpl->addStylesheet('bottom', 'blocks/mainmenu/assets/css');

?>
<ul class="category-menu-footer d-flex justify-content-start mb-0 p-3 ps-0" style="list-style-type: none">
    <?php foreach ($list as $item) { ?>
        <li style="color: #1affff; margin-right: 30px;">
            <a title="<?php echo $item->name ?>" href="<?php echo $item->link?>"><?php echo $item->name ?></a>
        </li>
    <?php  } ?>
</ul>