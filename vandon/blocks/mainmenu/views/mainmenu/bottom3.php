<?php
global $tmpl, $config;

$tmpl->addStylesheet('bottom', 'blocks/mainmenu/assets/css');

?>
<div class="accordion" id="accordionExample">
  <?php foreach ($list_footer_mobile as $item) { ?>
    <?php if (!empty($item->child)) { ?>
      <div class="accordion-item">
        <h2 class="accordion-header" id="<?php echo 'heading-' . $item->alias ?>">
          <button class="accordion-button collapsed <?php echo empty($item->child) ? 'no-child' : '' ?>" type="button" data-bs-toggle="collapse" data-bs-target="#<?php echo $item->alias ?>" aria-expanded="false" aria-controls="<?php echo $item->alias ?>">
            <?php echo $item->name ?>
          </button>
        </h2>
        <?php if (!empty($item->child)) { ?>
          <div id="<?php echo $item->alias ?>" class="accordion-collapse collapse" aria-labelledby="<?php echo 'heading-' . $item->alias ?>" data-bs-parent="#accordionExample">
            <div class="accordion-body">
              <ul>
                <?php foreach ($item->child as $item2) { ?>
                  <li style="padding: 5px">
                    <a style="text-decoration: none" href="<?php echo $item2->link ?>"><?php echo $item2->name ?></a>
                  </li>
                <?php } ?>
              </ul>
            </div>
          </div>
        <?php } ?>
      </div>
    <?php } else { ?>
      <a href="<?php echo $item->link ?>" class="accordion-item" style="display: block;">
        <span style="background-color: white; color: #333; margin-right: 20px; box-shadow: unset; font-weight: 600" class="accordion-button <?php echo empty($item->child) ? 'no-child' : '' ?>">
          <?php echo $item->name ?>
        </span>
      </a>

    <?php } ?>
  <?php } ?>

</div>