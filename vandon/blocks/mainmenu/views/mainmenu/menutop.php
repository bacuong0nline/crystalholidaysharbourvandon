<?php global $tmpl;
$tmpl->addStylesheet('menu', 'blocks/mainmenu/assets/css');
//$tmpl -> addScript('compactmenu','blocks/mainmenu/assets/js');

$Itemid = FSInput::get('Itemid', 1, 'int');
$lang = isset($_SESSION['lang']) ? $_SESSION['lang'] : 'vi';
?>
<!-- <div class='mainmenu mainmenu-<?php echo $style; ?>'>-->
<div class="main-menu d-flex justify-content-end align-items-center">
	<?php
	$Itemid  = FSInput::get('Itemid', 1, 'int');
	$total = count($list);
	$i = 1;
	$count_children = 0;
	$summner_children = 0;
	// echo "<li class='item first_item " . ($Itemid == 1 ? 'activated' : '') . "' ><a href='" . URL_ROOT . "' ><span> Trang chủ</span></a>  </li>";

	echo "<ul class='d-flex align-items-center mb-0' >";
	foreach ($list as $item) {
		$class =  $item->id ==  $Itemid ? 'activated' : '';
		//		 		if($i == 0)
		//		 			  $class .= ' first-item';
		if ($i == ($total))
			$class .= ' last-item';
		$count_children++;
		if ($count_children == $summner_children && $summner_children)
			$class .= ' last-item';
		$link = FSRoute::_($item->link);
		//echo "<li class='item " . $class . " item" . $i . "' ><a class='item-detail' href='" . $link . "' ><span> " . $item->name . "</span></a>  </li>";
		echo '<li>';
		if ($item->image) echo '<img src="' . $item->image . '" alt="' . $item->name . '" />';
		echo '<a href="' . $link . '" class="pl-1 text-uppercase mb-0 remove-link link-menu">' . $item->name . '</a>';
		echo '</li>';
		// sepa
		// if($i < $total - 1)
		// 	echo "<li class='sepa' ><span>&nbsp; | &nbsp;</span></li>";
		$i++;
	}
	$quantity = 0;
	if (isset($_SESSION['cart'])) {
		$product_list = $_SESSION['cart'];
		foreach ($product_list as $item) {
			$quantity += $item[1];
		}
	}
	echo '
		<a style="position:relative" href="javascript:void(0)" class="cart btn-cart">
		<i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 23px; color: white"></i>
		<div class="ml-1 count-products">';
	if (!empty($_SESSION['cart'])) {
		echo $quantity;
	}
	echo '</div>
	</a>
	';
	echo '<a href="index.php?module=products&view=cat&task=cat2&Itemid=3" class="contact-btn"> Đặt hàng <img src="/images/paper-plane.svg" alt="paper-plane"> </a>';
	echo "</ul>";
	?>
</div>
<!--</div>-->
<!-- <div class="row no-gutters pt-4">
            <div class="col d-flex align-items-center justify-content-end">
              <img src="/images/fire.jpg" alt="khuyen-mai" />
              <a class="pl-1 text-uppercase mb-0 remove-link link-menu">Khuyến mại</a>
            </div>
            <div class="col d-flex align-items-center justify-content-end">
              <a href="<?php echo  FSRoute::_("index.php?module=contents&view=content") ?>" class="text-uppercase mb-0 remove-link link-menu">Giới thiệu</a>
            </div>
            <div class="col d-flex align-items-center justify-content-end">
              <a href="<?php echo FSRoute::_("index.php?module=news&view=home") ?>" class="text-uppercase mb-0 remove-link link-menu">Tin tức</a>
            </div>
            <div class="col d-flex align-items-center justify-content-end">
              <a href="<?php echo FSRoute::_("index.php?module=contact&view=contact") ?>" class="text-uppercase mb-0 remove-link link-menu">Liên hệ</a>
            </div>
          </div> -->