<?php

use Guzzle\Http\Url;

global $tmpl, $config;
// $tmpl->addStylesheet('header');
//$tmpl -> addScript('styles','blocks/mainmenu/assets/js');

$Itemid = FSInput::get("Itemid", 1, 'int');
$check = $Itemid == 1 || $Itemid == 80 || $Itemid == 81 || $Itemid == 82 || ($Itemid == 83 && IS_MOBILE);
$tmpl->addStylesheet('multilevel2', 'blocks/mainmenu/assets/css');


?>

<?php
$root_total = 0;
$root_last = 0;
for ($i = 1; $i <= count($list); $i++) {
    if (@$list[$i]->level == 0) {
        $root_total++;
        $root_last = $i;
    }
}
?>
<?php $url = $_SERVER['REQUEST_URI']; ?>
<?php $url = substr($url, strlen(URL_ROOT_REDUCE)); ?>
<?php $url = preg_replace('/(-page)\w+/', '', $url); ?>
<?php $url = preg_replace('/\/sap-xep:\w+/', '', $url); ?>
<?php $url = URL_ROOT . $url; ?>
<div class="nav">

    <div style="flex: 1;" class="d-flex justify-content-between align-items-center">
        <div class='nav-menu mainmenu-2 mainmenu-<?php echo $style; ?> clearfix'>
            <div class="main-menu d-flex justify-content-end align-items-center">
                <ul class="parent-ul d-flex align-items-center mb-0" style="list-style: none; padding-left: 0px" id="menu-desktop">
                    <?php
                    $html = "";
                    $i = 1;
                    $num_child = array();
                    $parant_close = 0;
                    foreach ($list as $item) {
                    ?>
                    <?php
                        $link = FSRoute::_($item->link);
                        $class = '';
                        $class .= ' level' . $item->level;

                        if ($url == $link)
                            $class .= ' activated ';

                        // level = 1
                        if ($item->level == 0) {
                            if ($i == 1)
                                $class .= ' first-item';
                            if ($i == ($root_last - 1))
                                $class .= ' last-item';
                            if ($i != ($root_last - 1))
                                $class .= ' menu-item';

                            $caret = '<span class="caret"></span>';
                        }

                        // level > 1
                        else {
                            $parent = $item->parent_id;
                            // total children
                            $total_children_of_parent = @$list[$parent]->children;
                            if (isset($num_child[$parent])) {
                                if ($total_children_of_parent == $num_child[$parent]) {
                                    $class .= ' first-sitem sub-menu-item ';
                                } else {
                                    $class .= ' mid-sitem sub-menu-item ';
                                }
                            }
                            if ($total_children_of_parent > 1) {
                                $class .= ' dropdown-submenu ';
                            }

                            $caret = '';
                        }
                        $name = $item->name;
                        $html .= '<li data-menuanchor='. str_replace('/#', '', $item->link).' class="li_header ' . $class . '" >';
                        $html .= '<a href="' . $link . '" data-target="#">';
                        if ($item->icon) {
                            $html .= '<i class="' . $item->icon . '"></i>';
                        }
                        if ($item->level != 0) {
                            if ($item->image) {
                                $html .= '<img src="' . URL_ROOT . $item->image . '"/>';
                            }
                        }
                        $html .= $name;
                        $num_child[$item->id] = $item->children;
                        if (@$item->children > 0) {
                            // echo 1;
                            $html .= '
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-down" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
                                </svg>
                            ';
                        }

                        $html .= '</a>';
                        // browse child
                        if ($item->children > 0)
                            $html .= '<ul class="dropdown-menu"><div class="container">';

                        if (@$num_child[$item->parent_id] == 1) {
                            // if item has children => close in children last, don't close this item 
                            if ($item->children > 0) {
                                $parant_close++;
                            } else {
                                $parant_close++;
                                for ($i = 0; $i < $parant_close; $i++) {
                                    $html .= '</div></ul>';
                                }
                                $parant_close = 0;
                                $num_child[$item->parent_id]--;
                            }
                        }
                        if (((@$num_child[$item->parent_id] == 0) && (@$item->parent_id > 0)) || !$item->children) {
                        }
                        if (@$num_child[$item->parent_id] >= 1)
                            $num_child[$item->parent_id]--;

                        $html .= '</li>';

                        $i++;
                    }
                    echo $html;
                    ?>
                    <!-- NEWS_TYPE for menu	-->
                    <?php
                    $quantity = 0;
                    if (isset($_SESSION['cart'])) {
                        $product_list = $_SESSION['cart'];
                        foreach ($product_list as $item) {
                            $quantity += $item[1];
                        }
                    }
                    ?>
                    <li data-bs-toggle="modal" data-bs-target="#staticBackdrop2">
                        <span style="cursor: pointer; color: white; background-color: #DB801A; text-transform: uppercase; padding: 10px; border-radius: 5px; font-size: 13px; font-weight: 500"><?php echo FSText::_("Đăng ký")?></span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>