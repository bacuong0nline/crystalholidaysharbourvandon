<?php
global $tmpl, $config;
// $tmpl->addStylesheet('leftmenu', 'blocks/mainmenu/assets/css');
$url_current = $_SERVER['REQUEST_URI'];
$url_current = substr(URL_ROOT, 0, strlen(URL_ROOT) - 1) . $url_current;

// print_r($list);die;


?>
<?php
$Itemid = 7;
$root_total = 0;
$root_last = 0;
for ($i = 1; $i <= count($list); $i++) {
    if (@$list[$i]->level == 0) {
        $root_total++;
        $root_last = $i;
    }
}
?>
<?php $url = $_SERVER['REQUEST_URI']; ?>
<?php $url = substr($url, strlen(URL_ROOT_REDUCE)); ?>
<?php $url = preg_replace('/(-page)\w+/', '', $url); ?>
<?php $url = preg_replace('/\/sap-xep:\w+/', '', $url); ?>
<?php $url = URL_ROOT . $url; ?>
<div class="menu_right clearfix">
    <ul class="nav navbar-nav">
        <?php
        $html = "";
        $i = 1;
        $num_child = array();
        $parant_close = 0;
        foreach ($list as $key => $item) {
        ?>
        <?php
            $link = FSRoute::_($item->link);
            $class = '';
            $class .= ' level' . $item->level;

            if ($url == $link)
                $class .= ' activated ';

            // level = 1
            if ($item->level == 0) {
                if ($i == 1)
                    $class .= ' first-item';
                if ($i == ($root_last - 1))
                    $class .= ' last-item';
                if ($i != ($root_last - 1))
                    $class .= ' menu-item';

                $caret = '<span class="caret"></span>';
            }

            // level > 1
            else {
                $parent = $item->parent_id;
                // total children
                $total_children_of_parent = @$list[$parent]->children;
                if (isset($num_child[$parent])) {
                    if ($total_children_of_parent == $num_child[$parent]) {
                        $class .= ' first-sitem sub-menu-item ';
                    } else {
                        $class .= ' mid-sitem sub-menu-item ';
                    }
                }
                if ($total_children_of_parent > 1) {
                    $class .= ' dropdown-submenu ';
                }

                $caret = '';
            }
            $name = $item->name;
            $html .= '<li class="li_header ' . $class . '" >';
            $html .= '<a href="' . $link . '" data-target="#">';
            if ($item->icon) {
                $html .= '<i class="' . $item->icon . '"></i>';
            }
            if ($item->level != 0) {
                if ($item->image) {
                    $html .= '<img src="' . URL_ROOT . $item->image . '"/>';
                }
            }
            $html .= $name;

   
            $html .= '</a>';
            if($item->children > 0) {
                $html .= '<span data-bs-toggle="collapse" href="#collapseExample-'.$key.'" role="button" aria-expanded="false" aria-controls="collapseExample-'.$key.'" style="position: absolute; right: 10px;"><svg style="width: 20px; fill: #db8019" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><!--! Font Awesome Pro 6.3.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M201.4 406.6c12.5 12.5 32.8 12.5 45.3 0l192-192c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L224 338.7 54.6 169.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3l192 192z"/></svg></span>';
            }
            // browse child
            $num_child[$item->id] = $item->children;
            if ($item->children > 0)
                $html .= '<ul class="dropdown-menu collapse"  id="collapseExample-'.$key.'"><div class="container">';

            if (@$num_child[$item->parent_id] == 1) {
                // if item has children => close in children last, don't close this item 
                if ($item->children > 0) {
                    $parant_close++;
                } else {
                    $parant_close++;
                    for ($i = 0; $i < $parant_close; $i++) {
                        $html .= '</div></ul>';
                    }
                    $parant_close = 0;
                    $num_child[$item->parent_id]--;
                }
            }
            if (((@$num_child[$item->parent_id] == 0) && (@$item->parent_id > 0)) || !$item->children) {
            }
            if (@$num_child[$item->parent_id] >= 1)
                $num_child[$item->parent_id]--;

            $html .= '</li>';

            $i++;
        }
        echo $html;
        ?>
        <!-- NEWS_TYPE for menu	-->
        <?php
        $quantity = 0;
        if (isset($_SESSION['cart'])) {
            $product_list = $_SESSION['cart'];
            foreach ($product_list as $item) {
                $quantity += $item[1];
            }
        }
        ?>
    </ul><!-- nav -->
</div>