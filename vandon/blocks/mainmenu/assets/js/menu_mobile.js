$(function() {
    var logo = $('.logo-image img').attr('src');
    var template  = '<div class="moby-inner">';
    template +=      '<div class="head-menu-mobile">';
    template +=             '<img src="'+logo+'" class="img-responsive">';
    template +=     '<div class="moby-close"><span class="moby-close-icon"></span></div>'; // Reserved class for closing moby
    template +=     '</div>';
    template +=     '<div class="moby-wrap">';
    template +=             '<div class="moby-menu"></div>'; // Must be included in template
    template +=     '</div>';
    template += '</div>';

var mobyMenu = new Moby({
    menu       : $('#navigation-menu'), // The menu that will be cloned
    mobyTrigger: $('#moby-button'), // Button that will trigger the Moby menu to open
    menuClass		 : 'left-side',
    subMenuOpenIcon : '<span><i class="fal fa-chevron-down"></i></span>',
    subMenuCloseIcon : '<span><i class="fal fa-chevron-up"></i></span>',
    template : template,
});
});