<?php
/*
 * Huy write
 */
	// models 
	include 'blocks/productscat/models/productscat.php';
	class ProductscatBControllersProductscat
	{
		function __construct()
		{
		}
		function display($parameters,$title)
		{
			$model = new ProductscatBModelsProductscat();

			$list_cat = $model->get_list_cat();
			$list_new = $model->getListNew();
			$list_sell = $model->getListSell();
			$list_hot = $model->getListHot();
			
			//Hien thi san pham o danh muc cap 1
			$idCategory = FSInput::get("cid");
			if($idCategory != null) {
				$list_hot_by_id = $model->getListHotById($idCategory);
				$list_category_child = $model->get_list_category_child($idCategory);
				if (count($list_category_child) == 1) {
					$i = 0;
					foreach ($list_category_child as $category_child) {
						$list_category_child[$i]->products = $model->get_list_products2($category_child->id);
						$i++;
					}
				}
				if (count($list_category_child) > 1) {
					$limit = 4;
					$i = 0;
					foreach ($list_category_child as $category_child) {
						$list_category_child[$i]->products = $model->get_list_products2($category_child->id, $limit);
						$i++;
					}
				}

				$query_body = $model->set_query_body($idCategory);
				$total = $model->getTotal($query_body);
				$pagination = $model->getPagination($total);
			
			}
			// print_r($list_category_child);die;

			// print_r($_REQUEST);
			$list_2pro = $model->getList2pro();

			//Hien thi san pham trang chu
			$i = 0;
			foreach ($list_cat as $cat) {
					$list_cat[$i]->products = $model->get_list_products($cat->id);
					$i++;
			}
			//Hien thi san pham danh muc cap 2
			$category_name = "";
			$category_child = $model->get_list_product_child_category();
			if (!empty($category_child)) {
				$category_name = $model->get_record_by_id($idCategory, 'fs_products_categories');
			}
			// print_r($category_child);die;


			$list = $model->get_list();
			$style = $parameters->getParams('style');
			$style = $style?$style:'default';
			
			// call views
			include 'blocks/productscat/views/productscat/'.$style.'.php';
		}
	}
	
?>