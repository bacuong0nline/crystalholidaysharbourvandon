<?php
global $tmpl, $config;

?>

<div class="block-product-filter block-productscat-default_top">
  <div class="col no-gutters">
    <div class="row mt-3 no-gutters">
      <div class="col col-12 d-flex justify-content-center">
        <p class="title col-4 no-gutters">Xu hướng</p>
        <nav class="col-7 no-gutters">
          <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-link no-gutters d-md-flex justify-content-md-end justify-content-lg-center col-3 active" id="nav-new-tab" data-toggle="tab" href="#nav-new" role="tab" aria-controls="nav-trending" aria-selected="false">Mới</a>
            <a class="nav-link no-gutters col-5 col-lg-3 d-lg-flex justify-content-lg-start" id="nav-promotion-tab" data-toggle="tab" href="#nav-promotion" role="tab" aria-controls="nav-promotion" aria-selected="false">Khuyến mãi</a>
            <a class="nav-link no-gutters d-md-flex d-lg-flex justify-content-md-start justify-content-lg-start col-3" id="nav-trending-tab" data-toggle="tab" href="#nav-trending" role="tab" aria-controls="nav-home" aria-selected="true">Nổi bật</a>
          </div>
        </nav>
      </div>
    </div>
  </div>
  <div class="w-100 mt-4"></div>
  <div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-new" role="tabpanel" aria-labelledby="nav-new-tab">
      <div class="owl-carousel owl-theme" id="new">
        <?php foreach ($list_new as $item) { ?>
          <div class="col d-flex justify-content-start no-gutters">
            <div class="product">
              <a href="<?php echo FSRoute::_('index.php?module=products&view=product&code=' . $item->alias . '&id=' . $item->id) ?>">
                <img src="<?php echo ($item->old_id > 0 && strpos($item->image, 'images/products/products') !== false) ? URL_ROOT . str_replace('images/products/products/', 'images/products/products/medium_', $item->image) : URL_ROOT . str_replace('original', 'resized', $item->image) ?>" alt="<?php $item->alias ?>" />
                <p class="truncate">
                  <?php echo $item->name ?>
                </p>
              </a>
              <div class="d-md-flex justify-content-md-center text-center no-gutters">
                <?php if ($item->price == 0 && $item->price_old == 0) { ?>
                  <span class="new-fee big--text">Liên hệ</span>
                <?php } else { ?>
                  <p class="new-fee"><?php echo number_format($item->price, 0, '.', '.') . " đ" ?></p>
                  <?php if ($item->price_old != 0) { ?>
                    <p class="old-fee"><?php echo number_format($item->price_old, 0, '.', '.') . " đ" ?></p>
                  <?php } ?>
                <?php } ?>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
    <div class="tab-pane fade" id="nav-promotion" role="tabpanel" aria-labelledby="nav-promotion-tab">
      <?php if (!empty($list_sell)) { ?>
        <div class="owl-carousel owl-theme" id="promotion">
          <?php foreach ($list_sell as $item) { ?>
            <div class="col d-flex justify-content-start no-gutters">
              <div class="product">
                <a href="<?php echo FSRoute::_('index.php?module=products&view=product&code=' . $item->alias . '&id=' . $item->id . '&tablename=' . $item->tablename . '') ?>">
                  <img src="<?php echo ($item->old_id > 0 && strpos($item->image, 'images/products/products') !== false) ? URL_ROOT . str_replace('images/products/products/', 'images/products/products/medium_', $item->image) : URL_ROOT . str_replace('original', 'resized', $item->image) ?>" alt="<?php $item->alias ?>" />
                  <p class="truncate">
                    <?php echo $item->name ?>
                  </p>
                </a>
                <div class="d-flex justify-content-center no-gutters">
                  <?php if ($item->price == 0 && $item->price_old == 0) { ?>
                    <span class="new-fee big--text">Liên hệ</span>
                  <?php } else { ?>
                    <p class="new-fee"><?php echo number_format($item->price, 0, '.', '.') . " đ" ?></p>
                    <?php if ($item->price_old != 0) { ?>
                      <p class="old-fee"><?php echo number_format($item->price_old, 0, '.', '.') . " đ" ?></p>
                    <?php } ?>
                  <?php } ?>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      <?php } else { ?>
        <p>Không có sản phẩm hiển thị</p>
      <?php } ?>
    </div>
    <div class="tab-pane fade" id="nav-trending" role="tabpanel" aria-labelledby="nav-trending-tab">
      <div class="owl-carousel owl-theme" id="trending">
        <?php foreach ($list_hot as $item) { ?>
          <div class="col d-flex justify-content-start no-gutters">
            <div class="product">
              <a href="<?php echo FSRoute::_('index.php?module=products&view=product&code=' . $item->alias . '&id=' . $item->id . '&tablename=' . $item->tablename . '') ?>">
                <img src="<?php echo ($item->old_id > 0 && strpos($item->image, 'images/products/products') !== false) ? URL_ROOT . str_replace('images/products/products/', 'images/products/products/medium_', $item->image) : URL_ROOT . str_replace('original', 'resized', $item->image) ?>" alt="<?php $item->alias ?>" />
                <p class="truncate">
                  <?php echo $item->name ?>
                </p>
              </a>
              <div class="d-flex justify-content-center no-gutters">
                <?php if ($item->price == 0 && $item->price_old == 0) { ?>
                  <span class="new-fee big--text">Liên hệ</span>
                <?php } else { ?>
                  <p class="new-fee"><?php echo number_format($item->price, 0, '.', '.') . " đ" ?></p>
                  <?php if ($item->price_old != 0) { ?>
                    <p class="old-fee"><?php echo number_format($item->price_old, 0, '.', '.') . " đ" ?></p>
                  <?php } ?>
                <?php } ?>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>