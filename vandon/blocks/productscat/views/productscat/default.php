<?php
global $tmpl, $config;
//$tmpl->addStylesheet('default', 'blocks/productscat/assets/css');
// $tmpl->addScript("default", "blocks/productscat/assets/js");
$total = count($list);
$ccode = FSInput::get('ccode');
$link = FSRoute::_("index.php?module=products&view=cat_child");
?>

<?php foreach ($list_cat as $key => $cat) { ?>
  <div class="block-products block-productscat-default" id="<?php echo $cat->id ?>">
    <div class="col no-gutters">
      <div class="row mt-3 no-gutters title title-line">
        <div class="col no-gutters">
          <a href="<?php echo FSRoute::_('index.php?module=products&view=cat&cid=' . $cat->id . '&ccode=' . $cat->alias . '&Itemid=207') ?>">
            <h4><?php echo $cat->name ?></h4>
          </a>
        </div>
      </div>
    </div>
    <div class="w-100"></div>
    <div class="col mt-4">
      <div class="row">
        <div class="owl-carousel owl-theme products-slide" id="<?php echo $cat->alias ?>">
          <?php foreach ($cat->products as $item) { ?>
            <div class="col d-flex justify-content-start no-gutters pl-1">
              <div class="product">
                <a href="<?php echo FSRoute::_('index.php?module=products&view=product&code=' . $item->alias . '&id=' . $item->id) ?>">
                  <img src="<?php echo ($item->old_id > 0 && strpos($item->image, 'images/products/products') !== false) ? URL_ROOT . str_replace('images/products/products/', 'images/products/products/medium_', $item->image) : URL_ROOT . str_replace('original', 'resized', $item->image) ?>" alt="<?php $item->alias ?>" />
                  <p class="truncate">
                    <?php echo $item->name ?>
                  </p>
                </a>
                <div class="d-md-flex justify-content-md-center text-center no-gutters">
                  <?php if ($item->price == 0 && $item->price_old == 0) { ?>
                    <span class="new-fee big--text">Liên hệ</span>
                  <?php } else { ?>
                    <p class="new-fee"><?php echo number_format($item->price, 0, '.', '.') . " đ" ?></p>
                    <?php if ($item->price_old != 0) { ?>
                      <p class="old-fee"><?php echo number_format($item->price_old, 0, '.', '.') . " đ" ?></p>
                    <?php } ?>
                  <?php } ?>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
    <div>
      <?php echo $tmpl->load_direct_blocks("banners", array("style" => "advertisment", "products_categories" => $cat->id)) ?>
    </div>
  </div>
<?php } ?>
<div class="row no-gutters">
  <div class="col-12 col-md-6 no-gutters d-flex justify-content-md-start mt-md-3 mb-md-3">
    <a href="<?php echo FSRoute::_('index.php?module=contact&view=contact')?>" class="btn-bot-home d-flex justify-content-center">
      <img src="/img/footer/place.svg" alt="" class="mr-3" />
      <h4>Hệ thống showroom</h4>
      <div class="next">
        <i class="fa fa-chevron-right" aria-hidden="true"></i>
      </div>
    </a>
  </div>
  <div class="col-12 col-md-6 d-flex no-gutters agency-home justify-content-md-end mt-md-3 mb-md-3">
    <a href="<?php echo FSRoute::_('index.php?module=policy&view=policy&Itemid=53')?>" class="btn-bot-home d-flex justify-content-center">
      <img src="/img/footer/note.svg" alt="" class="mr-3" />
      <h4>Chính sách đại lý</h4>
      <div class="next">
        <i class="fa fa-chevron-right" aria-hidden="true"></i>
      </div>
    </a>
  </div>
</div>