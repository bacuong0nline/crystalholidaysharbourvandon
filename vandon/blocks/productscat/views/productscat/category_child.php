<?php
global $tmpl, $config;
$tmpl->addScript("default", "blocks/productscat/assets/js");
// print_r($category_name);
$tmpl->setTitle($category_name->name);
$tmpl->addMetades($category_name->name);
$arr_sort = array(
  array(1, FSText::_('Giá giảm dần')),
  array(2, FSText::_('Giá tăng dần')),
  array(3, FSText::_('Tên sản phẩm')),
  array(4, FSText::_('Mới nhất')),
  array(5, FSText::_('Cũ nhất'))
);

$sort = FSInput::get("sort")

?>

<input type='hidden' name="module" id="link_sort" value="<?php echo $link_sort; ?>">

<div class="row products common no-gutters fetch-data-ajax">
  <div class="container ">
    <div class="col no-gutters mt-4">
      <div class="row mt-2 no-gutters">
        <div class="col no-gutters">
          <div class="col title  title-line no-gutters">
            <h4><?php echo $category_name->name ?></h4>
          </div>
        </div>
        <div class="col no-gutters d-flex justify-content-end">
          <select class="select2-box" id="sort">
            <option value="<?php echo FSRoute::_("index.php?module=products&view=cat_child&ccode=".$category_name->alias."&cid=".$category_name->id."&Itemid=208")?>">Sắp xếp theo</option>
            <?php foreach($arr_sort as $key=>$item) { 
                $link_sort = FSRoute::addParameters('sort', $item[0]);
              ?>
              <option value="<?php echo $link_sort?>" <?php echo $sort == $item[0] ? "selected" : null?>><?php echo $item[1]?></option>
            <?php } ?>
          </select> 
        </div>
        <div class="w-100"></div>
        <?php foreach ($category_child as $child) { ?>
          <div class="col-6 col-md-3 d-flex justify-content-start no-gutters">
            <div style="width: 100%">
              <div class="sale">
                <?php if ($child->price_old != 0) { ?>
                  <img src="/img/sanpham1/tag-sale.png" class="sale-img" />
                <?php } ?>
                <span class="sale-value <?php echo $child->price_old != 0 ? null : "sale-disabled" ?>">
                  <?php echo '-' . round(100 - ($child->price / $child->price_old) * 100) . '%' ?>
                </span>
              </div>
              <div class="product">
                <a href="<?php echo FSRoute::_('index.php?module=products&view=product&code=' . $child->alias . '&id=' . $child->id) ?>">
                  <img src="<?php echo ($child->old_id > 0 && strpos($child->image, 'images/products/products') !== false) ? URL_ROOT . str_replace('images/products/products/', 'images/products/products/medium_', $child->image) : URL_ROOT . str_replace('original', 'resized', $child->image) ?>" alt="<?php $child->alias ?>" />
                  <p class="truncate">
                    <?php echo $child->name ?>
                  </p>
                </a>
                <div class="d-flex justify-content-center no-gutters">
                  <?php if ($child->price == 0 && $child->price_old == 0) { ?>
                    <span class="new-fee">Liên hệ</span>
                  <?php } else { ?>
                    <p class="new-fee"><?php echo number_format($child->price, 0, '.', '.') . " đ" ?></p>
                    <?php if ($child->price_old != 0) { ?>
                      <p class="old-fee"><?php echo number_format($child->price_old, 0, '.', '.') . " đ" ?></p>
                    <?php } ?>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>
      <div class="row no-gutters" style="margin-bottom: 60px">
        <div class="col no-gutters d-flex justify-content-center">
          <?php if ($pagination) {
            echo $pagination->showPagination($maxpage = 5);
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</div>