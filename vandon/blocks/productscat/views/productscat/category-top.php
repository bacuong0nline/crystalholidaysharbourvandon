<?php
global $tmpl, $config;
$tmpl->addScript("default", "blocks/productscat/assets/js");
?>

<div class="col no-gutters">
  <div class="row no-gutters title-line">
    <div class="col no-gutters">
      <div class="title">
        <h4>Sản phẩm nổi bật</h4>
      </div>
    </div>
  </div>

</div>
<div class="w-100"></div>
<div class="col no-gutters mt-4">
  <div class="row no-gutters">
    <?php foreach ($list_hot_by_id as $item) { ?>
      <div class="col-md-3 col-6 d-flex justify-content-start no-gutters">
        <div style="width: 100%">
          <div class="sale">
            <?php if ($item->price_old != "0") { ?>
              <img src="./img/sanpham1/tag-sale.png" />
            <?php } ?>
            <span class="sale-value <?php echo $item->price_old != "0" ? null : "sale-disabled" ?>">
              <?php echo '-'. round(100 - ($item->price / $item->price_old) * 100) .'%'?>
            </span>
          </div>
          <div class="product">
            <a href="<?php echo FSRoute::_('index.php?module=products&view=product&code=' . $item->alias . '&id=' . $item->id) ?>">
              <img src="<?php echo ($item->old_id > 0 && strpos($item->image, 'images/products/products') !== false) ? URL_ROOT . str_replace('images/products/products/', 'images/products/products/medium_', $item->image) : URL_ROOT . str_replace('original', 'resized', $item->image) ?>" alt="<?php $item->alias ?>" />
              <p class="truncate">
                <?php echo $item->name ?>
              </p>
            </a>
            <div class="d-flex justify-content-center no-gutters">
              <?php if ($item->price == 0 && $item->price_old == 0) { ?>
                <span class="new-fee">Liên hệ</span>
              <?php } else { ?>
                <p class="new-fee"><?php echo number_format($item->price, 0, '.', '.') . " đ" ?></p>
                <?php if ($item->price_old != 0) { ?>
                  <p class="old-fee"><?php echo number_format($item->price_old, 0, '.', '.') . " đ" ?></p>
                <?php } ?>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
  </div>
</div>