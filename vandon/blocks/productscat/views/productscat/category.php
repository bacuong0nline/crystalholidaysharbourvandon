<?php
global $tmpl, $config;

$total = count($list);
// print_r($list_category_child);die;
?>
<div class="col no-gutters mt-4">
  <div class="row mt-2 no-gutters">
    <?php foreach ($list_category_child as $child) { ?>
      <div class="col mt-4 no-gutters">
        <div class="col title  title-line no-gutters">
          <?php if (count($list_category_child) > 1) { ?>
            <a href="<?php echo FSRoute::_("index.php?module=products&view=cat_child&cid=" . $child->id . "&ccode=" . $child->alias . "&Itemid=208") ?>"><?php echo $child->name ?></a>
          <?php } else { ?>
            <a href="<?php echo FSRoute::_("index.php?module=products&view=cat&cid=" . $child->id . "&ccode=" . $child->alias . "&Itemid=208") ?>"><?php echo $child->name ?></a>
          <?php } ?>
        </div>
        <?php if (count($list_category_child) > 1) { ?>
          <div class="col">
            <a href="<?php echo FSRoute::_("index.php?module=products&view=cat_child&cid=" . $child->id . "&ccode=" . $child->alias . "&Itemid=208") ?>" class="show-more">Xem tất cả ></a>
          </div>
        <?php } ?>
      </div>
      <div class="w-100"></div>
      <?php if (!empty($child->products)) { ?>
        <?php foreach ($child->products as $item) { ?>
          <div class="col-md-3 col-6 d-flex justify-content-start no-gutters">
            <div style="width: 100%">
              <div class="sale">
                <?php if ($item->price_old != "0") { ?>
                  <img src="./img/sanpham1/tag-sale.png" class="sale-img" />
                <?php } ?>
                <span class="sale-value <?php echo $item->price_old != "0" ? null : "sale-disabled" ?>">
                  <?php echo '-'. round(100 - ($item->price / $item->price_old) * 100) .'%'?>
                </span>
              </div>
              <div class="product">
                <?php // $id_product = $child->tablename == 'fs_products' || $child->tablename == '' ? $item->id : $item->record_id 
                ?>
                <a href="<?php echo FSRoute::_('index.php?module=products&view=product&code=' . $item->alias . '&id=' . $item->id . '&Itemid=126') ?>">
                  <img src="<?php echo ($item->old_id > 0 && strpos($item->image, 'images/products/products') !== false) ? URL_ROOT . str_replace('images/products/products/', 'images/products/products/medium_', $item->image) : URL_ROOT . str_replace('original', 'resized', $item->image) ?>" alt="<?php $item->alias ?>" />
                  <p class="truncate">
                    <?php echo $item->name ?>
                  </p>
                </a>
                <div class="d-flex justify-content-center no-gutters">
                  <?php if ($item->price == 0 && $item->price_old == 0) { ?>
                    <span class="new-fee">Liên hệ</span>
                  <?php } else { ?>
                    <p class="new-fee"><?php echo number_format($item->price, 0, '.', '.') . " đ" ?></p>
                    <?php if ($item->price_old != 0) { ?>
                      <p class="old-fee"><?php echo number_format($item->price_old, 0, '.', '.') . " đ" ?></p>
                    <?php } ?>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
      <?php } else { ?>
        <p>Không có sản phẩm</p>
      <?php } ?>
      <div class="w-100"></div>
    <?php } ?>
  </div>
  <div class="row no-gutters" style="margin-bottom: 60px">
    <div class="col no-gutters d-flex justify-content-center">
      <?php if ($pagination && count($list_category_child) == 1) {
        echo $pagination->showPagination(1);
      }
      ?>
    </div>
  </div>
</div>