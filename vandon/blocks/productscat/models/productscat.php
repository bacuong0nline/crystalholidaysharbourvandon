
<?php
class ProductscatBModelsProductscat
{
	function __construct()
	{
		$fstable = FSFactory::getClass('fstable');
		$table_filter = "";
		$limit = 12;
		$page = FSInput::get('page');
		$this->limit = $limit;
		$this->page = $page;
		//$this->table_name = $fstable->_('fs_products');
		$this->table = $fstable->_('fs_products', 1);
		$this->table_categories = $fstable->_('fs_products_categories', 1);
	}


	function get_list()
	{
		$where = '';
		$query = " SELECT *
						FROM " . $this->table . " 
						WHERE published = 1 " . $where . " ORDER BY ordering DESC";
		global $db;
		$sql = $db->query($query);
		$result = $db->getObjectList();
		return $result;
	}

	function get_list_cat()
	{
		$where = '';
		$query = " SELECT *
						FROM " . $this->table_categories . " 
						WHERE published = 1 AND show_in_homepage = 1 AND parent_id = 0 " . $where . " ORDER BY ordering ASC";
		global $db;
		$sql = $db->query($query);
		$result = $db->getObjectList();
		return $result;
	}

	function get_list_products($cat_id)
	{
		global $db;
		$query = "  SELECT id, name, image, summary, alias, category_alias, category_name, category_id, price, price_old, tablename, discount_tag, old_id
                    FROM " . $this->table . "
                    WHERE published = 1 AND category_id_wrapper like '%," . $cat_id . ",%' AND show_in_home = 1
                    ORDER BY ordering ASC, updated_time DESC LIMIT 12";
		$db->query($query);
		$result = $db->getObjectList();
		// print_r($db);die;
		return $result;
	}
	function get_list_products2($cat_id, $limit = '')
	{
		global $db;
		$query = "  SELECT id, name, image, summary, alias, category_alias, category_name, category_id, price, price_old, tablename, discount_tag, old_id
                    FROM " . $this->table . "
                    WHERE (published = 1 AND category_id_wrapper like '%," . $cat_id . ",%') OR (published = 1 AND multi_categories like '%,$cat_id,%')
                    ORDER BY ordering ASC, updated_time DESC";
		if ($limit != '') {
			$query .= " LIMIT " . $limit . "";
			$db->query($query);
			$result = $db->getObjectList();
			return $result;
		} else {
			$db->query_limit($query, $this->limit, $this->page);;
			$result = $db->getObjectList();
			return $result;
		}
	}
	// function get_list_products2($cat_id, $table_filter){
	//     global $db;
	//     if (empty($table_filter) || $table_filter == $this->table) {
	//         $table_filter = $this->table;
	//         $query = "  SELECT id, name, image, summary, alias, category_alias, category_name, category_id, price, price_old, discount_tag
	//         FROM " . $table_filter . "
	//         WHERE published = 1 AND category_id_wrapper like '%," . $cat_id . ",%'
	//         ORDER BY created_time DESC LIMIT 4";
	//     } else {
	//     $query = "SELECT a.id, a.record_id, a.name, a.image, a.summary, a.alias, a.category_alias, a.category_name, a.category_id, a.price, a.price_old, b.discount_tag
	//             FROM ".$table_filter." a INNER JOIN ".$this->table." b ON a.record_id = b.id
	//             WHERE a.published = 1 AND a.category_id_wrapper like '%," . $cat_id . ",%'
	//             ORDER BY a.created_time DESC
	//             LIMIT 4";
	//     }

	//     $db->query($query);
	//     $result = $db->getObjectList();
	//     // print_r($db);
	//     return $result;
	// }

	function getListNew()
	{
		global $db;
		$query = "  SELECT id, name, image, alias, category_alias, category_name, category_id, category_alias, created_time, price_old, price, tablename, old_id
                    FROM " . $this->table . "
                    WHERE published = 1 ORDER BY created_time DESC LIMIT 20";
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}

	function getListSell()
	{
		global $db;
		$query = "  SELECT id, name, image, alias, category_alias, category_name, category_id, category_alias, is_sell,price_old, price, tablename, old_id
                    FROM " . $this->table . "
                    WHERE published = 1 AND is_sell = 1 ORDER BY updated_time DESC LIMIT 12 ";
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}

	function getListHot()
	{
		global $db;
		$query = "  SELECT id, name, image, alias, category_alias, category_name, category_id, category_alias, is_hot, price_old, price, tablename, old_id
                    FROM " . $this->table . "
                    WHERE published = 1 AND is_hot = 1 ORDER BY updated_time DESC LIMIT 12 ";
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}
	function getListHotById($idCategory)
	{
		global $db;
		$query = "  SELECT id, name, image, alias, category_alias, category_name, category_id, category_alias, is_hot, price_old, price, discount_tag, tablename, old_id
                    FROM " . $this->table . "
                    WHERE published = 1 AND is_hot = 1 AND category_id_wrapper like '%,$idCategory,%' ORDER BY ordering ASC LIMIT 12 ";
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}
	function get_list_category_child($idCategory)
	{
		global $db;
		$query = "  SELECT id, name, image, alias, tablename
                    FROM " . $this->table_categories . "
                    WHERE published = 1 AND list_parents like '%,$idCategory,%' ORDER BY ordering ASC";
		$db->query($query);
		$result = $db->getObjectList();
		if (count($result) > 1) {
			array_shift($result);
		}
		// print_r($result);die;
		return $result;
	}
	function getList2pro()
	{
		global $db;
		$query = "  SELECT id, name, image, alias, category_alias, category_name, category_id, category_alias, is_hot, is_sell, created_time
                    FROM " . $this->table . "
                    WHERE published = 1 AND is_hot = 1 AND is_sell = 1 ORDER BY created_time DESC LIMIT 2";
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}
	function set_query_body($cid)
	{
		$query_ordering = '';
		$where = "";
		$query = ' FROM ' . $this->table . '
                              WHERE ( category_id = ' . $cid . ' 
                                  OR category_id_wrapper like "%,' . $cid . ',%" )
                                  AND published = 1
                                ' . $where . ' ';
		return $query;
	}
	function getTotal($query_body)
	{
		if (!$query_body)
			return;
		global $db;
		$query = "SELECT count(*)";
		$query .= $query_body;
		$sql = $db->query($query);
		$total = $db->getResult();
		return $total;
	}
	function getPagination($total)
	{
		FSFactory::include_class('Pagination');
		$pagination = new Pagination($this->limit, $total, $this->page);
		return $pagination;
	}
	function get_list_product_child_category()
	{
		global $db;
		$ccode = FSInput::get('ccode');
		$cid = FSInput::get('cid');
		$sort = FSInput::get('sort');
		$query = "SELECT id, name, image, alias, category_alias, category_name, price, price_old, category_id, category_alias, is_hot, is_sell, created_time, tablename,old_id
							FROM " . $this->table . "
							WHERE (published = 1 AND category_id_wrapper like '%," . $cid . ",%') OR (published = 1 AND multi_categories like '%,$cid,%')";
		if ($sort) {
			switch ($sort) {
				case 1:
					$query .= "ORDER BY price DESC";
					break;
				case 2:
					$query .= "ORDER BY price ASC";
					break;
				case 3:
					$query .= "ORDER BY name ASC";
					break;
				case 4:
					$query .= "ORDER BY created_time ASC";
					break;
				case 5:
					$query .= "ORDER BY created_time DESC";
					break;
			}
		} else {
			$query .= "ORDER BY ordering ASC, updated_time DESC";
		}
		$db->query_limit($query, $this->limit, $this->page);;
		$result = $db->getObjectList();
		return $result;
	}
	function get_record_by_id($id, $table_name = '', $select = '*')
	{
		if (!$id)
			return;
		if (!$table_name)
			$table_name = $this->table_name;
		$query = " SELECT " . $select . "
                          FROM " . $table_name . "
                          WHERE id = $id ";

		global $db;
		$sql = $db->query($query);
		$result = $db->getObject();
		return $result;
	}
}

?>