<?php
$params = array(
    'suffix' => array(
        'name' => 'Hậu tố',
        'type' => 'text',
        'default' => '_news_listt'
    ),
    'limit' => array(
        'name' => 'Giới hạn',
        'type' => 'text',
        'default' => '3'
    ),
    'type' => array(
        'name' => 'Lấy theo',
        'type' => 'select',
        'value' => array(
            'ordering' => 'Mới nhất',
//                'ramdom'=>'Ngẫu nhiên',
            'home' => 'Hiển thị trang chủ'
        ),
//					'attr' => array('multiple' => 'multiple'),
    ),
    //'limit_video' => array(
//					'name' => 'Giới hạn video',
//					'type' => 'text',
//					'default' => '6'
//		),  			
    'style' => array(
        'name' => 'Style',
        'type' => 'select',
        'value' => array(
            'slide' => 'Default',
        )
    ),
    'category_id' => array(
        'name' => 'Nhóm danh mục',
        'type' => 'select',
        'value' => get_category(),
//        'attr' => array('multiple' => 'multiple'),
    ),

);

function get_category()
{
    global $db;
    $query = " SELECT name, id 
						FROM fs_news_categories 
						WHERE level = 0
						";
    $sql = $db->query($query);
    $result = $db->getObjectList();
    if (!$result)
        return;
    $arr_group = array();
    foreach ($result as $item) {
        $arr_group[$item->id] = $item->name;
    }
    return $arr_group;
}

?>