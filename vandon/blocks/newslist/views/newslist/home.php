<?php
global $tmpl;
//$tmpl->addStylesheet('home', 'blocks/newslist/assets/css');
?>

<div class="hot-news-homepage block-newslist">
    <p class="mg-0 title">
        <img src="/images/hot-news.svg" alt="<?php echo FSText::_('Tin nổi bật') ?>" class="img-responsive">
        <span><?php echo FSText::_('Tin nổi bật') ?></span>
    </p>
    <ul class="list-news-home">
        <?php foreach ($list_hot as $item) {?>
        <li>
            <a href="<?php echo FSRoute::_('index.php?module=news&view=news&code='.$item->alias.'&id='.$item->id) ?>" title="<?php echo $item->title ?>">
                <img src="/images/Path 3199.svg" alt="<?php echo $item->title ?>" class="img-responsive">
                <span class="news-title"><?php echo $item->title ?></span>
            </a>
        </li>
        <?php } ?>
    </ul>
    <div class="show-all-news">
        <a href="<?php echo FSRoute::_('index.php?module=news&view=home') ?>" title="<?php echo FSText::_('Xem tất cả') ?>">
            <?php echo FSText::_('Xem tất cả') ?>
            <i class="fa fa-angle-double-right"></i>
        </a>
    </div>
</div>


