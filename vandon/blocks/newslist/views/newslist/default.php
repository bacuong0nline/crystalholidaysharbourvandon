<?php
global $tmpl;
//$tmpl->addScript('default', 'blocks/newslist/assets/js');
?>
<?php if(FSInput::get('Itemid') != 34) {?>
    <div class="item-grid">
        <h3 class="h3_right"><?php echo FSText::_('Tin nổi bật') ?></h3>
        <div class="list-right">
            <?php foreach ($list_hot as $item) {
                $link = FSRoute::_('index.php?module=news&code='.$item->id.'&id='.$item->id);
                ?>
                <div class="item-right">
                    <a href="<?php echo $link ?>" class="img" title="<?php echo $item->title ?>">
                        <img src="<?php echo URL_ROOT.str_replace('/original/','/resized/',$item->image) ?>" alt="<?php echo $item->title ?>" class="img-responsive">
                    </a>
                    <div class="content">
                        <a href="<?php echo $link ?>" class="name" title="<?php echo $item->title ?>">
                            <?php echo $item->title ?>
                        </a>
                        <p class="detail-time-hit">
                            <i class="fa fa-clock-o"></i>
                            <?php echo date('d-m-Y') ?>
                        </p>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>
<div class="item-grid">
    <h3 class="h3_right"><?php echo FSText::_('Tin đọc nhiều') ?></h3>
    <div class="list-right">
        <?php foreach ($list_hit as $item) {
            $link = FSRoute::_('index.php?module=news&code='.$item->id.'&id='.$item->id);
            ?>
            <div class="item-right">
                <a href="<?php echo $link ?>" class="img" title="<?php echo $item->title ?>">
                    <img src="<?php echo URL_ROOT.str_replace('/original/','/resized/',$item->image) ?>" alt="<?php echo $item->title ?>" class="img-responsive">
                </a>
                <div class="content">
                    <a href="<?php echo $link ?>" class="name" title="<?php echo $item->title ?>">
                        <?php echo $item->title ?>
                    </a>
                    <p class="detail-time-hit">
                        <i class="fa fa-clock-o"></i>
                        <?php echo date('d-m-Y',strtotime($item->created_time)) ?>
                    </p>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<?php if(FSInput::get('Itemid') == 34) {?>
    <?php if(@$list_relate) {?>
        <div class="item-grid">
            <h2 class="h3_right"><?php echo FSText::_('Tin liên quan') ?></h2>
            <div class="list-right">
                <?php foreach ($list_relate as $item) {
                    $link = FSRoute::_('index.php?module=news&code='.$item->id.'&id='.$item->id);
                    ?>
                    <div class="item-right">
                        <a href="<?php echo $link ?>" class="img" title="<?php echo $item->title ?>">
                            <img src="<?php echo URL_ROOT.str_replace('/original/','/resized/',$item->image) ?>" alt="<?php echo $item->title ?>" class="img-responsive">
                        </a>
                        <div class="content">
                            <a href="<?php echo $link ?>" class="name" title="<?php echo $item->title ?>">
                                <?php echo $item->title ?>
                            </a>
                            <p class="detail-time-hit">
                                <i class="fa fa-clock-o"></i>
                                <?php echo date('H:i:s d-m-Y',strtotime($item->created_time)) ?>
                            </p>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
<?php } ?>



