<div class="block-home block-newslist">
    <div class="block-newslist-slide block_id_<?php echo $id ?>">
        <div class="title-block">
            <h2><?php echo FSText::_('Dự án nổi bật') ?></h2>
            <a href="<?php echo FSRoute::_('index.php?module=news&view=cat&ccode='.$cat_title->alias.'&id='.$cat_title->id) ?>" title="<?php echo FSText::_('Xem tất cả')?>">
                <?php echo FSText::_('Xem tất cả') ?>
            </a>
        </div>
        <div class="news-container">
            <?php foreach ($list as $item) {?>
                <div class="news-item">
                    <a class="news-image" href="<?php echo FSRoute::_('index.php?module=news&view=news&code='.$item->alias.'&id='.$item->id) ?>" title="<?php echo $item->title ?>">
                        <img src="<?php echo URL_ROOT.str_replace('original','resize',$item->image) ?>" alt="<?php echo $item->title ?>" class="img-resonsive">
                    </a>
                    <a class="news-name" href="<?php echo FSRoute::_('index.php?module=news&view=news&code='.$item->alias.'&id='.$item->id) ?>" title="<?php echo $item->title ?>">
                       <?php echo $item->title ?>
                    </a>
                    <a class="news-category" href="<?php echo FSRoute::_('index.php?module=news&view=cat&ccode='.$item->category_alias.'&id='.$item->category_id) ?>" title="<?php echo $item->category_name ?>">
                        <i class="fa fa-list-ul" aria-hidden="true"></i>
                        <?php echo $item->category_name ?>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>