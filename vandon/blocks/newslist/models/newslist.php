
<?php

class NewslistBModelsNewslist {

    function __construct() {
        $fstable = FSFactory::getClass('fstable');
        $this->table_news = $fstable->_('fs_news',1);
        $this->table_categories = $fstable->_('fs_news_categories',1);
        $this->table_tags = $fstable->_('fs_tags',1);
//            $this->table_video = $fstable->_('fs_video');
    }

    function get_list_hot(){
        global $db;
        $query = "SELECT id,alias,title,image,created_time
                  FROM ".$this->table_news."
                  WHERE published = 1 and is_hot = 1 order by id desc, created_time desc";
        $sql = $db->query_limit($query,4);
        $result = $db->getObjectList();
        return $result;
    }

    function get_list_hit(){
        global $db;
        $query = "SELECT id,alias,title,image,created_time
                  FROM ".$this->table_news."
                  WHERE published = 1 order by hits desc, created_time desc";
        $sql = $db->query_limit($query,10);
        $result = $db->getObjectList();
        return $result;
    }

    function get_list_tag(){
        global $db;
        $query = "SELECT id,alias,name
                  FROM ".$this->table_tags."
                  WHERE published = 1 order by ordering desc";
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    function get_level($id){
        global $db;
        $query = " SELECT level
					FROM " . $this->table_categories . "
					WHERE published = 1 and id = $id
					ORDER BY ordering asc
							";
        $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    function get_cats($level,$id) {
        global $db;
        if($level != 0){
            $query = ' SELECT id, alias, image, name
					FROM ' . $this->table_categories . ' 
					WHERE published = 1 and is_right = 1
					ORDER BY ordering asc
							';
        } else{
            $query = " SELECT id, alias, image, name
					FROM " . $this->table_categories . "
					WHERE published = 1 and parent_id = $id
					ORDER BY ordering asc
							";
        }

        $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    function get_list($id,$limit, $type){
        global $db;
        $limit = $limit ? $limit : 5;
        $order = "";
        $show = "";
        switch ($type){
            case 'ordering':
                $order .= " ordering asc, ";
                break;
            case 'home':
                $order .= " ordering asc, ";
                $show .= " and show_in_homepage = 1 ";
                break;
        }
        $query = " SELECT id, alias, image, title, author, created_time, category_name, category_alias, category_id
					FROM ". $this->table_name ." 
					WHERE published = 1 $show and category_id_wrapper like '%,".$id.",%'
					ORDER BY $order id desc, created_time desc
							";
        $db->query_limit($query, $limit);
        $result = $db->getObjectList();
        return $result;
    }

    function get_list_news_pro(){
        global $db;
        $query = ' SELECT id, alias, image, summary, created_time, title, author
					FROM ' . $this->table_name . ' 
					WHERE published = 1 and is_new_video = 1
					ORDER BY id desc, created_time DESC
							';
        $db->query_limit($query, 5);
        $result = $db->getObjectList();
        return $result;
    }

    function get__title($cat_id){
        global $db;
        $query = " SELECT id,name,alias
					FROM fs_news_categories 
					WHERE published = 1 and id = ".$cat_id."
							";
        //echo $query;
        $db->query($query);
        $result = $db->getObject();
        return $result;
    }

    function get_str_relate($id){
        global $db;
        $query = " SELECT id,news_related
					FROM ".FSTable::_('fs_news',1)."
					WHERE published = 1 and id = ".$id."
							";
        $db->query($query);
        $result = $db->getObject();
        return $result;
    }

    function get_list_relate($news_related){
        global $db;
        $query = "SELECT id, alias, title, created_time, image
                  FROM ".$this->table_name."
                  WHERE published = 1 and id in (0".$news_related."0)";
        $sql = $db->query($query);
        $result = $db->getObjectList();
        // print_r($result);die;
        return $result;
    }
}

?>