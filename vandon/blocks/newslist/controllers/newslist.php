<?php

/*
 * Huy write
 */
// models 
include 'blocks/newslist/models/newslist.php';

class NewslistBControllersNewslist {

    function __construct() {
        
    }

    function display($parameters,$title,$id=null) {
        $ordering = $parameters->getParams('ordering');
        $type = $parameters->getParams('type');
        $limit = $parameters->getParams('limit');
        $limit = $limit ? $limit : 4;
        $style = $parameters->getParams('style');
        $cat = $parameters->getParams('category_id');
        // call models
        $model = new NewslistBModelsNewslist();
//        $style = $style ? $style : 'default';
        $list_hot = $model->get_list_hot();
        $list_hit = $model->get_list_hit();
        // $list_tag = $model->get_list_tag();

        $item_id = FSInput::get('Itemid');
        if($item_id == 34){
            $str = $model->get_str_relate(FSInput::get('id'));
            if($str->news_related)
                $list_relate = $model->get_list_relate($str->news_related);
        }

//        if($style == 'default'){
//            if(!empty(FSInput::get('id'))){
//                $level = $model->get_level(FSInput::get('id'));
//                $cats = $model->get_cats($level,FSInput::get('id'));
//                foreach ($cats as $i=>$item){
//                    $cats[$i]->news = $model->get_list($item->id,'','');
//                    $cats[$i]->count = $this->get_count('published = 1 and category_id_wrapper like "%,'.$item->id.',%" ','fs_news');
//                }
//                $list_news_pro = $model->get_list_news_pro();
//                $count = $this->get_count('published = 1 and is_new_video = 1','fs_news');
//            }
//        }
//        if($style == 'slide'){
//            $list = $model->get_list($cat, $limit, $type);
//            $cat_title = $model->get__title($cat);
//        }

        // call views
        include 'blocks/newslist/views/newslist/' . $style . '.php';
    }

    function get_count($where = '', $table_name = '', $select = '*')
    {
        if (!$where)
            return;
        if (!$table_name)
            $table_name = $this->table_name;
        $query = ' SELECT count(' . $select . ')
					  FROM ' . $table_name . '
					  WHERE ' . $where;
        global $db;
        $sql = $db->query($query);
        $result = $db->getResult();
        return $result;
    }



}

?>