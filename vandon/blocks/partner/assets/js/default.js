$("document").ready(function() {
    $('.partner-box').owlCarousel({
        loop:true,
        margin:10,
        // nav:true,
        dots:false,
        autoplay: true,
        responsive:{
            0:{
                items:2
            },
            600:{
                items:5
            },
            1000:{
                items:5
            }
        }
    })
})