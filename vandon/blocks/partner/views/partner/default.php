<?php global $tmpl;
  $tmpl->addScript("default", "blocks/partner/assets/js");
?>
<div class="<?php echo $params['background'] == 1 ? 'snowflake1' : 'snowflake2' ?> partner" style="position: relative;">
  <div class="container pb-3">
    <p class="text-uppercase text-center" data-aos="fade-right"><?php echo @$params['title1']  ?></p>
    <h5 class="title text-center text-uppercase title-block" data-aos="fade-left"><?php echo @$params['title'] ?></h5>
    <div class="col-12 d-flex flex-column align-items-center">
      <?php if (@$params['title2']) { ?>
        <p class="text-center font-weight-strong title-block-2"><?php echo @$params['title2'] ?></p>
      <?php } ?>
      <?php if (@$params['summary']) { ?>
        <p class="text-center summary2 mb-0" style="width: 70%"><?php echo @$params['summary'] ?></p>
      <?php } ?>
    </div>
  </div>
  <div class="container partner-wrapper">
    <div class="d-flex partner-box owl-carousel owl-theme">
      <?php foreach ($partners as $item) { ?>
        <div class="item">
          <img src="<?php echo $item->image ?>" alt="<?php echo $item->title ?>" />
        </div>
      <?php } ?>
    </div>
  </div>
  <?php if (@$params['background'] == 1) { ?>
    <img class="wave2" src="./images/wave2.png" alt="wave2" />
  <?php } else { ?>
    <img class="wave1" src="./images/wave1.png" alt="wave1" />
  <?php } ?>
</div>