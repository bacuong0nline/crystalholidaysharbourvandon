$(document).ready(function () {
  $(".discount-btn").click(function () {
    $(".coupon").slideToggle('medium', function () {
      if ($(this).is(':visible'))
        $(this).css('display', 'flex');
    })
  })
  $(".add-voucher").click(function() {
    $("#voucher-modal").modal('show')
  })
  $(".name-voucher").click(function() {
    $(this).parent().parent().next().toggle();
  })
  $(".use-voucher").click(function () {

    let voucher = $(".voucher").val()
    let _this = this;
    if ($(this).attr("data-used") == '' || $(this).attr("data-used") != voucher) {
      let html =
        `<div class="fill"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div><div>`;
      $("#main").after(html)
      $.ajax({
        url: 'index.php?module=payment&view=payment&raw=1&task=voucher',
        type: 'POST',
        data: { voucher: voucher, total: $(this).attr("data-total") },
        success: function (res) {
          data = JSON.parse(res);
          console.log(data);
          if (!data.error) {
            let html = `<div>
            <i class="fa fa-tag"></i>
              <span>${data.voucher_detail}</span>
            </div>`
            $(".check-voucher").html(html);
            $(".voucher-end").html(currencyFormat(parseInt(data.discount))).parent().css("display", "")
            $(".total-end-discount").html(data.total).parent().css("display", "")
          } else {
            let html = `<div>
            <i class="fa fa-tag"></i>
              <span>${data.voucher_detail}</span>
            </div>`
            $(".check-voucher").html(html);
            // $(".voucher-end").empty()
            $(".voucher-box").attr("style", "display: none !important");
            // $(".total-end-discount").empty()
            $(".total-box-end").attr("style", "display: none !important");
          }
          $("#voucher-modal").modal("hide");

        }
      })
    }

  })
})

function currencyFormat(num) {
  return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')+'đ'
}