<?php
include 'blocks/cart/models/cart.php';
class CartBControllersCart
{
  function __construct()
  {
  }
  function display($parameters, $title)
  {

    $model = new CartBModelsCart();

    $quantity = 0;
    if (isset($_SESSION['cart'])) {
      $product_list = $_SESSION['cart'];
      foreach ($product_list as $item) {
        $quantity += $item[1];
      }
    }
    @$list_cart = $_SESSION['cart'];

    $total_price = 0;
    $data = array();
    $_SESSION['price_options_value'] = 0;

    if (!empty($list_cart))
      foreach ($list_cart as $key1 => $item) {
        $data[$item[0]] = $model->getProduct($item[0]);
        $products_info = $data[$item[0]];
        $total = 0;
        $total = $products_info->price * $item[1];
        $total_price += $total;
        $options_display = '';

        if (!empty($item[3])) {
          foreach ($item[3] as $key => $val) {
            if (empty($val['id'])) {
              foreach ($val as $val2) {
                $price = @$val2['price'] != "0.00" ? format_money_0(@$val2['quantity'] * @$val2['price']) : null;
                $quantity_option = @$val2['quantity'] > 1 ? 'x ' . @$val2['quantity'] . '' : '';
                $options_display .= '<p class="mb-0" style="font-size: 12px; color: #6e6e6e">' . @$val2['name_display'] . ' ' . $quantity_option . ' ' . $price . ' </p>';
                $_SESSION['price_options_value'] += $val2['price'] * @$val2['quantity'];
              };
            } else {
              $price = @$val['price'] != "0.00" ? format_money_0(@$val['quantity'] * @$val['price']) : null;
              $quantity_option = @$val['quantity'] > 1 ? 'x ' . @$val['quantity'] . '' : '';
              $options_display .= '<p class="mb-0" style="font-size: 12px; color: #6e6e6e">' . @$val['name_display'] . ' ' . $quantity_option . ' ' . $price . ' </p>';
              $_SESSION['price_options_value'] += $val['price'] * @$val['quantity'];
            };
          };

        }
        $list_cart[$key1][4] = $options_display;
      };
    if (@$_SESSION['discount'])
      $_SESSION['total_price'] = $total_price - $_SESSION['discount'] + $_SESSION['price_options_value'];
    else
      $_SESSION['total_price'] = $total_price + $_SESSION['price_options_value'];

    $style = $parameters->getParams('style');
    $style = $style ? $style : 'default';

    $vouchers = $model->getAllVoucher();

    include 'blocks/cart/views/cart/' . $style . '.php';
  }
  function get_options($cat_child)
  {

    $model = new CartBModelsCart();
    $temp = $model->get_records("id in (0" . $cat_child->options . "0)", "fs_products_options", "*", "ordering asc");

    $list_options = array();

    $temp = $this->cleanArray($temp);
    $list_options = array();

    if (!empty($temp)) {
      foreach ($temp as $key => $item) {
        $list_options[$item->id] = $model->get_records("parent_id = $item->id ", 'fs_products_values', '*', 'id asc');
      };
    }

    return $list_options;
  }
  // function get_options($cat_child)
  // {
  //   $model = new CartBModelsCart();

  //   $temp = $model->get_records("id in (0" . $cat_child->options . "0)", "fs_products_options", "*", "ordering asc");

  //   $list_options = array();

  //   $temp = $this->cleanArray($temp);
  //   $list_options = array();


  //   if (!empty($temp)) {
  //     foreach ($temp as $key => $item) {
  //       $list_options[$item->id] = $model->get_records("parent_id = $item->id ", 'fs_products_values', '*', 'id asc');
  //     };
  //   }

  //   return $list_options;
  // }
  function cleanArray($array)
  {
    if (is_array($array)) {
      foreach ($array as $key => $sub_array) {
        $result = $this->cleanArray($sub_array);
        if ($result === false) {
          unset($array[$key]);
        } else {
          $array[$key] = $result;
        }
      }
    }

    if (empty($array)) {
      return false;
    }

    return $array;
  }
}
