<?php
global $tmpl;
$tmpl->addStylesheet('default', 'blocks/cart/assets/css');
$tmpl->addScript('default', 'blocks/cart/assets/js');
$Itemid = FSInput::get("Itemid");
$total_price = 0;

?>

<?php if ($Itemid != 3) { ?>
  <div id="cartModal" class="sidenav">
    <div class="d-flex ml-2 mb-3">
      <h4 style="font-size: 20px" class="font-weight-bold text-uppercase"><?php echo FSText::_("Giỏ hàng") ?></h4>
      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    </div>
    <div class="line"></div>
    <?php if (!empty($list_cart)) { ?>
      <div style="height: 65%; overflow: auto;" class="cart-items" id="style-3">
        <?php
        foreach ($list_cart as $key1 => $row) {
          $products_info = $data[$row[0]];
          $total = 0;
          $total = $products_info->price * $row[1];
          $total_price += $products_info->price * $row[1];
          foreach ($row[3] as $item2) {
            if (!is_array(@$item2[0])) {
              $total_price += $item2['price'] * $item2['quantity'];
            } else {
              foreach ($item2 as $val) {
                $total_price += $val['price'] * $val['quantity'];
              }
            }
          }
          if ($products_info->price == 0) {
            $check_total_price = true;
          }
          $cat_child = $model->get_record_by_id($products_info->category_id, 'fs_products_categories');
          $list_options = $this->get_options($cat_child);
          @$name = '';

          if (!empty($list_options)) {
            $i = 0;
            foreach ($list_options as $key => $item) {
              $obj = $model->get_record_by_id($key, 'fs_products_options');
              if ($i > 0) {
                @$name .= ', ';
              }
              @$name .= $obj->name;
              $i++;
            };
          }
        ?>
          <?php echo $tmpl->cart_item($products_info, $row, $key1, $total, @$name) ?>
        <?php } ?>
      </div>
      <div class="footer-cart">
        <!-- <a class="font-weight-bold font-italic discount-btn"><?php echo FSText::_("+ Nhập mã giảm giá") ?></a> -->

        <div class="d-flex">
          <p class="add-voucher font-weight-bold text-right" style="color: #f68b58; font-size: 13px; cursor: pointer"><?php echo FSText::_("Mã giảm giá") ?></p>
          <i title="Làm mới" class="fa fa-refresh ml-2" style="color: orange; cursor: pointer" aria-hidden="true"></i>
        </div> 
        <p class="check-voucher">
        </p>

        <div class="line-discount"></div>
        <div class="d-flex justify-content-between">
          <p><?php echo FSText::_("Tạm tính") ?></p>
          <p class="font-weight-bold total-end"><?php echo format_money($total_price) ?></p>
        </div>
        <div class="d-flex justify-content-between voucher-box" style="display: <?php echo !empty($_SESSION['discount']) ? '' : 'none !important' ?>">
          <p><?php echo FSText::_("Mã giảm giá") ?></p>
          <p class="font-weight-bold voucher-end"><?php echo format_money($_SESSION['discount']) ?></p>
        </div>
        <!-- <div class="d-flex justify-content-between total-box-end" style="display: <?php echo !empty($_SESSION['voucher']) ? '' : 'none !important' ?>"> -->
          <!-- <p><?php echo FSText::_("Tổng thanh toán") ?></p> -->
          <!-- <p class="font-weight-bold total-end-discount"><?php echo format_money($_SESSION['total_price']) ?></p> -->
        <!-- </div> -->
        <div class="d-flex align-items-center justify-content-center flex-column">
          <a href="<?php echo URL_ROOT ?>index.php?module=payment&view=payment&Itemid=3" class="order-btn"><?php echo FSText::_("Đặt hàng") ?></a>
          <!-- <a href="" class="guest-checkout-btn">Guest Checkout</a> -->
          <!-- <p class="text-center mt-3" style="font-size: 12px; color: #777"><?php echo FSText::_("Chấp nhận thanh toán qua") ?></p> -->
          <!-- <img src="/images/hinhthucthanhtoan.jpg" alt="hinhthucthanhtoan"> -->
        </div>
      </div>
    <?php } else { ?>
      <div style="height: 70%" class="d-flex flex-column justify-content-center align-items-center">
        <svg color="#ccc" xmlns="http://www.w3.org/2000/svg" width="75" height="75" fill="currentColor" class="bi bi-basket" viewBox="0 0 16 16">
          <path d="M5.757 1.071a.5.5 0 0 1 .172.686L3.383 6h9.234L10.07 1.757a.5.5 0 1 1 .858-.514L13.783 6H15a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1v4.5a2.5 2.5 0 0 1-2.5 2.5h-9A2.5 2.5 0 0 1 1 13.5V9a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h1.217L5.07 1.243a.5.5 0 0 1 .686-.172zM2 9v4.5A1.5 1.5 0 0 0 3.5 15h9a1.5 1.5 0 0 0 1.5-1.5V9H2zM1 7v1h14V7H1zm3 3a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 4 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 6 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 8 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 1 .5-.5zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 1 .5-.5z" />
        </svg>
        <p class="mt-3"><?php echo FSText::_("Giỏ hàng trống") ?></p>
      </div>
    <?php } ?>

    <input type="hidden" name="change-value" id="change-value" value="">
  </div>
<?php } ?>

