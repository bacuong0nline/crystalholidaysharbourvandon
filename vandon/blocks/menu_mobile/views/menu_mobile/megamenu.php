<?php
    global $tmpl;
    $tmpl -> addScript('megamenu','blocks/mainmenu/assets/js');
    $tmpl -> addStylesheet('megamenu','blocks/mainmenu/assets/css');
    $Itemid = FSInput::get('Itemid');

?>

<div id='cssmenu' class="row-item">
	<ul id='megamenu' class="menu clearfix">
        <?php $i = 0;?>
        <?php foreach($level_0 as $item):?>
		    <?php $link = FSRoute::_('index.php?module=products&view=cat&cid=' . $item->id . '&ccode=' . $item->alias ); ?>
            <?php $link_c = FSRoute::_('index.php?module=products&view=cat_child&cid=' . $item->id . '&ccode=' . $item->alias ); ?>
		    <?php $class = 'level_0';?>
<!--		--><?php //$class .= $item -> description ? ' long ': ' sort' ?>
		    <?php if($arr_activated[$item->id]) $class .= ' activated ';?>

		    <li class="<?php echo $class; ?> dropdown">
			<a href="<?php echo $link; ?>" id="menu_item_<?php echo $item->id;?>" class="menu_item_a" title="<?php echo htmlspecialchars($item -> name);?>">
				<?php echo $item -> name;?>
			</a>
            <!-- LEVEL 1 -->
			<?php if(isset($children[$item -> id]) && count($children[$item -> id])  ){?>
			<span class="click-show-menu" data-id='<?php echo $item->id ?>' aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></span>
            <div class="dropdown-menu mega-dropdown-menu" id="<?php echo $item->id ?>">
                <div class="">
                <ul>
				<?php }?>
				<?php if(isset($children[$item -> id]) && count($children[$item -> id])  ){?>
					<?php $j = 0;?>
					<?php foreach($children[$item -> id] as $key=>$child){?>
                    <?php $countchild = count($children[$item -> id])/2?>
<!--						--><?php //$link1 = $child -> link? FSRoute::_($child -> link):''; ?>
                        <li class='sub-menu-level<?php echo $child->level;?> '>
							<a href="<?php echo $link_c; ?>" class="<?php echo $class?> sub-menu-item" id="menu_item_<?php echo $child->id;?>" title="<?php echo htmlspecialchars($child -> name);?>">
								<?php if($child->image){ ?>
                                    <i class="icon-2" style="background: url('<?php echo URL_ROOT.$child->image ?>') no-repeat center center;"></i>
                                <?php } ?>
                                <?php echo $child -> name;?>
		   		            </a>
                            <?php if(isset($children[$child -> id]) && count($children[$child -> id])  ){?>
                            <span class="click-show-menu" data-id='<?php echo $child->id ?>'>&nbsp;</span>
                            <ul id="<?php echo $child->id ?>">
                                <?php foreach($children[$child -> id] as $key=>$child2){?>
<!--                                    --><?php //$link = $item -> link? FSRoute::_($child2 -> link):''; ?>
                                    <li class='sub-menu-level<?php echo $child2->level;?> '>
                                        <a href="<?php echo $link_c; ?>" class="<?php echo $class?> sub-menu-item" id="menu_item_<?php echo $child->id;?>" title="<?php echo htmlspecialchars($child2 -> name);?>">
                                            <?php if($child2->image){ ?>
                                                <i class="icon-2" style="background: url('<?php echo URL_ROOT.$child->image ?>') no-repeat center center;"></i>
                                            <?php } ?>
                                            <?php echo $child2 -> name;?>
                                        </a>
                                    </li>
                                <?php }?>
                            </ul>
                            <?php }?>
						</li>
						<?php $j++;?>
					<?php }?>
				<?php }?>
				<?php if(isset($children[$item -> id]) && count($children[$item -> id])  ){?>
			    </ul>
                </div>
            </div>
			<?php }?>
		</li>
		    <?php $i ++; ?>
		<?php endforeach;?>
		<!-- CHILDREN -->
	</ul>
</div>