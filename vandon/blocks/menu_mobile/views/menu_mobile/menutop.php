<?php
    global $tmpl,$config;  
    $tmpl -> addStylesheet('menutop','blocks/mainmenu/assets/css');
    $url_current = $_SERVER['REQUEST_URI'];
    $url_current = substr(URL_ROOT, 0, strlen(URL_ROOT)-1).$url_current;
?>

<ul>
	<?php $i = 0;
		foreach($level_0 as $item):?>
		<?php $link = $item -> link? FSRoute::_($item -> link):''; ?>
		<?php $class = 'level_0';?>
		<?php $class .= $item -> description ? ' long ': ' sort' ?>
		<?php if($arr_activated[$item->id]) $class .= ' activated ';?>
	
		<li class="item <?php echo $class; ?>">
			<a href="<?php echo $link; ?>" target="<?php echo $item->target;?>" title="<?php echo htmlspecialchars($item -> name);?>">
				<?php echo $item -> name;?>
			</a>
				<?php if(isset($children[$item -> id]) && count($children[$item -> id])  ){?>
					<ul class="child-menu" >
    			<?php }?>
    				<?php if(isset($children[$item -> id]) && count($children[$item -> id])  ){?>
    					<?php $j = 0;?>
    					<?php foreach($children[$item -> id] as $key=>$child){?>
    						<?php $link = $item -> link? FSRoute::_($child -> link):''; ?>
						
    						<li class='item-level1 <?php if($arr_activated[$child->id]) $class .= ' activated ';?> '>
    							<a href="<?php echo $link; ?>" class="<?php echo $class?>" target="<?php echo $item->target;?>" title="<?php echo htmlspecialchars($child -> name);?>">
                                    <?php echo $child -> name;?>
    		   		            </a>
    						</li>
    						<?php $j++;?>
    					<?php }?>
    				<?php }?>
    			<?php if(isset($children[$item -> id]) && count($children[$item -> id])  ){?>
				    </ul>  
				<?php }?>
		</li>	
		<?php $i ++; ?>	
	<?php endforeach;?>
	<li class="item hotline"><a title="<?php echo $config['hotline']?>" href=""><span><?php echo FSText::_('Hotline:') ?> </span><?php echo $config['hotline']?></a></li>
	<li class="item click-search"><a title="Search"><?php echo FSText::_('Search') ?></a></li>
</ul>

    