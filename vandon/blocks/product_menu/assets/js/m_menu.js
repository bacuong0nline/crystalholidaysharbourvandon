function run_menu_mobile(){
    $('.sb-toggle').click(function(){
        if($('#cssmenu').css('display') == 'none'){
//				$('#cssmenu').css('display','block');
            $("#cssmenu").fadeIn(300,function(){});
        }else{
            $("#cssmenu").fadeOut(300,function(){});
//				$('#cssmenu').css('display','none');
        }
    });

    $('#cssmenu ul li span').click(function(e){
        var id = $(this).attr('data-id');
        $( this ).toggleClass( "active" );
        $('#'+id).toggle("slow");
    });
    $('#cssmenu button').click(function(e){
        var id = $(this).attr('data-id');
        $( this ).toggleClass( "active" );
        $('#'+id).toggle("slow");
    });
}
$( document ).ready(function() {
    run_menu_mobile();
});