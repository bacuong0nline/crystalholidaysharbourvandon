<?php 
	class Product_menuBModelsProduct_menu
	{
		function __construct()
		{
            $fstable = FSFactory::getClass('fstable');
            $this->table_name = $fstable->_('fs_products_categories',1);
			$this->table_menu = $fstable->_('fs_menus_items',1);

		}
		function getListCat($str_cats){
			$where = '';
			if($str_cats)
					$where .= ' AND list_parents LIKE "%,'.$str_cats.',%" ';	
			$query = "SELECT name,alias,id,level,parent_id as parent_id, summary, alias,list_parents,show_in_homepage, published, icon
						  FROM ".$this->table_name." AS a
						  WHERE published = 1 ".$where." AND show_in_homepage = 1
						  ORDER BY ordering ASC, id ASC
						  ";
			global $db;
			$db->query($query);
			$category_list = $db->getObjectList();

			if(!$category_list)
				return;
			$tree_class  = FSFactory::getClass('tree','tree/');
			return $list = $tree_class -> indentRows2($category_list,3);
		}
		function getListMenu() {
			global $db;

			$sql = " SELECT id,image,link, name, level, parent_id as parent_id, 
													target, description,is_type,is_link,summary,bk_color,icon
								FROM ".$this->table_menu."
								WHERE published  = 1
							AND group_id = 1 or group_id = 5
							ORDER BY ordering
									";

			$db->query($sql);
			$result = $db->getObjectList();
			$tree_class = FSFactory::getClass('tree', 'tree/');
			return $list = $tree_class->indentRows($result, 3);
	}
       
}
?>