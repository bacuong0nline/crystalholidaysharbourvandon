<?php
global $tmpl;
$tmpl->addStylesheet('menu', 'blocks/product_menu/assets/css');

?>

<div id='cssmenu_content'>
<!--	CONTENT -->
<dl class='menu-product' >
    <?php
    $Itemid = 5; // config
    $num_child = array();
    $parant_close = 0;
    $i = 0;
    $count_children = 0;
    $summner_children = 0;
    $id = 0;
    if ($need_check)
        $id = FSInput::get('id', 0, 'int');

    $total = count($list);
    foreach ($list as $item) {
        $class = '';
        if ($need_check) {
            $class = $item->alias == $ccode ? 'activated' : '';
        }
        $link = FSRoute::_('index.php?module=products&view=cat&cid=' . $item->id . '&ccode=' . $item->alias .'&Itemid=207') ;
        $link_c = FSRoute::_('index.php?module=products&view=cat&cid=' . $item->id . '&ccode=' . $item->alias .'&Itemid=207') ;

        $class .= ' level_' . $item->level;
        if ($i == 0)
            $class .= ' first-item';
        if ($i == ($total - 1))
            $class .= ' last-item';
        
//        if($item->icon){
//            $icon = "<img src=" .URL_ROOT. str_replace('original', 'resized', $item->icon). " class='img-responsive'>";
//        } else {
//            $icon = '';
//        }

        if ($item->level) {
            $count_children ++;
            if ($count_children == $summner_children && $summner_children)
                $class .= ' last-item';

            echo "<dt class=' $class child_" . $item->parent_id . "' ><a href='" . $link_c . "'  >  " . $item->name."</a> ";
        } else {
            $count_children = 0;
            $summner_children = $item->children;
            
            echo "<dt class=' $class  d-flex pt-1 pb-1' style='font-weight: 300; max-height: 57.8px' id='pr_" . $item->id . "' >";
            echo "<img class='icon-menu-item' src='/$item->icon' alt='living-room'>";
            echo "<div style='display: flex; align-items: center; flex-direction:column; justify-content: center'>";
            echo "<a title='$item->name' style='position: relative; font-weight: bold' href='" . $link . "'  class='lv_0'>  " . $item->name . "</a> ";
            echo "<a href='" . $link . "' class='lv_0 mb-0'><p style='font-size: 12px;'  class='truncate-summary-cat mb-0'>$item->summary</p></a>";
            echo "</div>";
        }
        ?>
        <?php
        $num_child[$item->id] = $item->children;
        if ($item->children > 0) {
            if ($item->level)
                echo "<dl id='c_" . $item->id . "' class='wrapper_children wrapper_children_level" . $item->level . "'>";
            else
                echo "<dl id='c_" . $item->id . "' class='wrapper_children_level" . $item->level . "' >";
        }

        if (@$num_child[$item->parent_id] == 1) {
            // if item has children => close in children last, don't close this item 
            if ($item->children > 0) {
                $parant_close ++;
            } else {
                $parant_close ++;
                for ($i = 0; $i < $parant_close; $i++) {
//                      echo "<li class='sub-footer'></li></ul>";
                    echo "</dl>";
                }
                $parant_close = 0;
                $num_child[$item->parent_id] --;
            }

            if (( (@$num_child[$item->parent_id] == 0) && (@$item->parent_id > 0 ) ) || !$item->children) {
                echo "</dt>";
            }
            if (@$num_child[$item->parent_id] >= 1)
                $num_child[$item->parent_id] --;
        }


        if (isset($num_child[$item->parent_id]) && ($num_child[$item->parent_id] == 1))
            echo "</dl>";
        if (isset($num_child[$item->parent_id]) && ($num_child[$item->parent_id] >= 1))
            $num_child[$item->parent_id] --;
    }
    ?>
</dl>
<!--	end CONTENT -->
</div>