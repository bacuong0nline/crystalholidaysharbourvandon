<?php global $tmpl, $user, $config;
$tmpl->addStylesheet('megamenu_mobile', 'blocks/product_menu/assets/css');
$tmpl->addStylesheet('jquery.mmenu.all', 'blocks/product_menu/assets/css');
$tmpl->addScript('jquery.mmenu.min.all', 'blocks/product_menu/assets/js');
$tmpl->addScript('megamenu_moblie', 'blocks/product_menu/assets/js');
$Itemid = FSInput::get('Itemid', 1, 'int');
$ccode = FSInput::get('ccode');
$link_info_members = FSRoute::_("index.php?module=members&view=info&Itemid=209");
$link_signin_signup = FSRoute::_("index.php?module=members&view=members&lang=vi");
if (!empty($_COOKIE['avatar'])) {
    $avatar = $_COOKIE['avatar'];
}

?>
<nav id="navigation-menu">
    <ul class="menus-<?php echo $style; ?> row-item ">
        <li>
            <a href="<?php echo URL_ROOT ?>" class="m-auto" style="display: block; width: fit-content">
                <img style="height: 90%; width: 130px" src="<?php echo $config['logo'] ?>" alt="logo" />
            </a>
        </li>
        <?php
        $Itemid = 5; // config
        $num_child = array();
        $parant_close = 0;
        $i = 0;
        $count_children = 0;
        $summner_children = 0;
        $id = 0;
        if ($need_check)
            $id = FSInput::get('id', 0, 'int');

        $total = count($list_menu);
        $x = 0;
        $kil = 0;
        // echo '<a class="mm-btn mm-btn_close mm-navbar__btn" href="#navigation-menu"  aria-owns="page"><span class="mm-sronly" style="font-size: 50px; color: #81954e; margin-left: 10px">&times</span></a>';

        foreach ($list_menu as $item) {
            $class = '';
            if ($need_check) {
                $class = $item->alias == $ccode ? 'open' : '';
            }
            $link = FSRoute::_($item->link);


            $class .= ' level_' . $item->level . ' ';
            if ($i == ($total - 1))
                $class .= ' last-item ';

            if ($item->level) {

                $count_children++;
                if ($count_children == $summner_children && $summner_children)
                    $class .= ' last-item ';
                if ($item->level == 2)
                    $x++;
                if ($item->level == 1) {
                    $x = 0;
                    $kil++;
                    $col = ' col-lg-4 ';
                    $link_all = $link;
                } else
                    $col = ' ';
                $chil = 'child_' . $item->parent_id;
                if ($x <= 12)
                    echo '<li class="item ' . $class . $col . $chil . '   ' . $kil . '" >
                        <a style="font-weight: bold; color: #BE202F; min-height: 55px; display: flex; align-items: center; border-bottom: 1px solid #ffffff30" class="name_' . $item->level . '" href="' . $link . '"  > ' . $item->name . '</a> 
                     ';
                elseif ($x == 13)
                    echo '<li class="item ' . $class . $col . $chil . ' " >
                        <b><a style="color: #f58220" class="name_' . $item->level . '" href="' . $link_all . '"  > Xem tất cả</a> </b>
                     ';
            } else {
                $count_children = 0;
                $summner_children = $item->children;
                // echo "<li class='item $class  ' id='pr_" . $item->id . "' style='width:100%;display:block;float:left'>";
                // echo "<a  href='" . $link . "' >" . $item->name . "</a>";
                // echo "<a  href='" . $link . "' >" . $item->name . "</a>";
                echo "<li class='item $class  ' id='pr_" . $item->id . "' style='width:100%;display:flex; border-bottom: 1px solid #ffffff30'>";
                echo "<img style='filter: brightness(0) invert(1);' class='icon-menu-item' src='/$item->icon' alt='/$item->icon'>";
                echo "<a style='display: flex; align-items: center; width: 100%; text-transform: uppercase; color: #BE202F'  href='" . $link . "' ><b>" . $item->name . "</b></a>";
            }
        ?>
            <?php
            $num_child[$item->id] = $item->children;
            if ($item->children > 0) {
                if ($item->level)
                    echo "<ul id='c_" . $item->id . "' class=' sub-menu wrapper_children wrapper_children_level" . $item->level . "' >";
                else {
                    echo "<div class='menu_box row'> <ul id='c_" . $item->id . "' class=' col-lg-9 sub-menu wrapper_children_level" . $item->level . "' >";
                    $idlitr = $item->id;
                }
            }

            if (@$num_child[$item->parent_id] == 1) {
                if ($item->children > 0) {
                    $parant_close++;
                } else {
                    $parant_close++;
                    for ($i = 0; $i < $parant_close; $i++) {

                        echo "</ul><!--end1:" . ($i) . " -->";
                    }
                    $parant_close = 0;
                    $num_child[$item->parent_id]--;
                }

                if (((@$num_child[$item->parent_id] == 0) && (@$item->parent_id > 0)) || !$item->children) {
                    echo "</li>";
                }
                if (@$num_child[$item->parent_id] >= 1)
                    $num_child[$item->parent_id]--;
            }
            ?>
        <?php

            if (isset($num_child[$item->parent_id]) && ($num_child[$item->parent_id] == 1))
                echo "</ul><!--end -->";
            if (isset($num_child[$item->parent_id]) && ($num_child[$item->parent_id] >= 1))
                $num_child[$item->parent_id]--;
        }
        ?>



    </ul>
</nav>