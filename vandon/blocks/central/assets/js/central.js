$(document).ready(function() {
    $(".footer-bottom").click(function() {
        $(".detail-footer-bottom").slideDown()
        $(this).hide();
    })

    $(".show-footer-bottom").click(function() {
        $(".footer-bottom").toggle()
        $(".detail-footer-bottom").slideUp()
    })

    $('.central-box-2').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    })

    $('.address-2').owlCarousel({
        animateOut: 'animate__fadeOutUp',
        animateIn: 'animate__flipInX',
        loop: true,
        margin: 10,
        nav: false,
        dots: false,
        autoplay:true,
        responsive: {
            0: {
                items: 1
            }
        }
    })
})