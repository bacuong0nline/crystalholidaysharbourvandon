<?php
$params = array(
	'suffix' => array(
		'name' => 'Hậu tố',
		'type' => 'text',
		'default' => '_education',
	),
	'title1' => array(
		'name' => 'Tiêu đề phụ 1',
		'type' => 'text',
		'default' => '',
	),
	'title' => array(
		'name' => 'Tiêu đề chính',
		'type' => 'text',
		'default' => '',
	),
	'title2' => array(
		'name' => 'Tiêu đề phụ 2',
		'type' => 'text',
		'default' => '',
	),
	'summary' => array(
		'name' => 'Tóm tắt',
		'type' => 'text',
		'default' => '',
	),
	'background' => array(
		'name' => 'Màu nền',
		'type' => 'select',
		'value' => array(
			1 => 'Trắng',
			2 => 'Xám'
		)
	),
	'limit' => array(
		'name' => 'Giới hạn',
		'type' => 'text',
		'default' => '5',
	),
	'button' => array(
		'name' => 'Hiển thị button "Tìm hiểu thêm"',
		'type' => 'select',
		'value' => array(
			1 => 'Có',
			0 => 'Không',
			// 'style3' => 'Kiểu 3',
		)
	),
	'style' => array(
		'name' => 'Style',
		'type' => 'select',
		'value' => array(
			'style1' => 'Kiểu 1',
			'style2' => 'Kiểu 2',
			// 'style3' => 'Kiểu 3',
		)
	),

	// 'image' => array(
	// 	'name' => 'Ảnh',
	// 	'class' => 'image-select',
	// 	'type' => 'select',
	// 	'value' => get_image(),
	// 	// 'attr' => array('multiple' => 'multiple'),
	// ),
);
function get_category()
{
	global $db;
	$query = " SELECT name, id
						FROM fs_education_categories where level = 0
						";
	$sql = $db->query($query);
	$result = $db->getObjectList();
	if (!$result)
		return;
	$arr_group = array();
	foreach ($result as $item) {
		$arr_group[$item->id] = $item->name;
	}
	return $arr_group;
}
function get_image()
{
	global $db;
	$query = " SELECT name, id, image
						FROM fs_banners
						";
	$sql = $db->query($query);
	$result = $db->getObjectList();
	if (!$result)
		return;
	$arr_group = array();
	foreach ($result as $item) {
		$arr_group[$item->id] = $item->name;
	}
	return $arr_group;
}
?>
