<?php
class CentralBModelsCentral
{
	function __construct()
	{
		$fstable = FSFactory::getClass('fstable');
		$this->table_name = $fstable->_('fs_central', 1);
		// $this->table_news = $fstable->_('fs_news', 1);
		$this->table_products_categories = $fstable->_('fs_products_categories', 1);
		$this->table_categories = $fstable->_('fs_central_categories', 1);
		$this->table_address = $fstable->_('fs_address', 1);

	}
	function getList($category_id, $cat_home = null)
	{
		$where = '';
		if ($category_id) {
			$where .= ' AND category_id = ' . $category_id . ' ';
		}
		// return;
		$module = FSInput::get('module');
		$view = FSInput::get('view');
		$ccode = FSInput::get('ccode');
		$cid = FSInput::get('cid');
		//			$cat = $this->get_cats($ccode);
		$filter = FSInput::get('filter');

		$where2 = '';

		$Itemid = FSInput::get('Itemid', 1, 'int');
		$where .= " AND (listItemid = 'all'
							OR listItemid like '%,$Itemid,%')" . $where2;

		$query = ' SELECT *
						  FROM ' . $this->table_name . ' AS a
						  WHERE published = 1
						 ' . $where . ' ORDER BY ordering, id';
		global $db;

		$db->query($query);
		$list = $db->getObjectList();
		return $list;
	}

	function get_address() {
		$query = " SELECT id, name, alias, address, link
		FROM " . $this->table_address . " AS a
		where published = 1
		order by ordering asc
		";
		// echo $query;die;
		global $db;
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}

	function get_category_child($cat_id) {
		$query = " SELECT id, name, alias, image, summary, title_home
		FROM " . $this->table_categories . " AS a
		WHERE parent_id = $cat_id order by ordering limit 5
		";
		// echo $query;die;
		global $db;
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}
    function get_central_system($limit)
    {
        global $db;
        $query = " SELECT id, title, image, alias FROM $this->table_name where published = 1 order by ordering asc LIMIT $limit";
        $sql = $db->query($query);
        $result = $db->getObjectList();

        return $result;
    }
	function get_central_system2()
    {
        global $db;
        $query = " SELECT id, title, image, alias FROM $this->table_name order by ordering asc LIMIT 5";
        $sql = $db->query($query);
        $result = $db->getObjectList();

        return $result;
    }
}
