<?php
global $tmpl;
$tmpl->addScript('central', 'blocks/central/assets/js');
?>
<div class="<?php echo $background == 1 ? 'snowflake1' : 'snowflake2' ?>" style="width: 100%; background-size: 100%; background-repeat: no-repeat; background-position: center center; position: relative; padding-bottom: 100px;">
    <div class="container pb-3">
        <p class="text-uppercase text-center title-block-1" data-aos="fade-right"><?php echo @$params['title1']  ?></p>
        <h5 class="title text-center text-uppercase title-block" data-aos="fade-left"><?php echo @$params['title'] ?></h5>
        <div class="col-12 d-flex flex-column align-items-center">
            <?php if (@$params['title2']) { ?>
                <p class="text-center font-weight-strong title-block-2"><?php echo @$params['title2'] ?></p>
            <?php } ?>
            <?php if (@$params['summary']) { ?>
                <p class="text-center summary2" style="width: 70%"><?php echo @$params['summary'] ?></p>
            <?php } ?>
        </div>
        <div class="container desktop">
            <?php if (count($central_system) <= 5) { ?>
                <div class="grid-container specialized-training-box central">
                    <?php foreach ($central_system as $key => $item) { ?>
                        <?php $tmpl->central_item($item, $key) ?>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php if (count($central_system) == 6) { ?>
                <div class="central-box">
                    <?php foreach ($central_system as $key => $item) { ?>
                        <a data-aos="fade-in" data-aos-delay="<?php echo $key * 300 ?>" data-aos-anchor-placement="top-bottom" href="<?php echo FSRoute::_("index.php?module=central&view=central&id=$item->id&code=$item->alias") ?>" class="central-<?php echo $key + 1 ?>">
                            <img src="<?php echo str_replace('original', 'resized', $item->image) ?>" alt="<?php echo $item->image ?>" />
                            <p class="name-central"><?php echo $item->title ?></p>
                        </a>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
        <div class="container mobile">
            <div class="central-box-2 owl-theme owl-carousel">
                <?php foreach ($central_system as $key => $item) { ?>
                    <a data-aos="fade-in" data-aos-delay="<?php echo $key * 300 ?>" data-aos-anchor-placement="top-bottom" href="<?php echo FSRoute::_("index.php?module=central&view=central&id=$item->id&code=$item->alias") ?>">
                        <img style="height: 219px; object-fit: cover" src="<?php echo str_replace('original', 'resized', $item->image)  ?>" alt="<?php echo $item->image ?>" />
                        <!-- <p class="name-central"><?php echo $item->title ?></p> -->
                    </a>
                <?php } ?>
            </div>
        </div>
        <?php if ($params['button']) { ?>
            <div class="d-flex justify-content-center mt-5">
                <a href="<?php echo FSRoute::_("index.php?module=address&view=address&Itemid=3&lang=vi") ?>" style="width: inherit; height: inherit;" class="font-weight-strong btn-register-2 text-uppercase align-items-center p-3">
                    <?php echo FSText::_("Tìm hiểu thêm") ?> &nbsp;&nbsp;&nbsp;
                    <svg width="10" height="10" viewBox="0 0 10 10" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M5 0L4.11562 0.884375L7.60625 4.375H0V5.625H7.60625L4.11562 9.11562L5 10L10 5L5 0Z"></path>
                    </svg>
                </a>
            </div>
        <?php } ?>
    </div>
    <div class="container central-system-wrapper"></div>
    <?php if (@$background == 1) { ?>
        <img class="wave2" src="./images/wave2.png" alt="wave2" />
    <?php } else { ?>
        <img class="wave1" src="./images/wave1.png" alt="wave1" />
    <?php } ?>
</div>