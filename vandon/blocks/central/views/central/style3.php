<?php
global $tmpl;
$tmpl->addScript('central', 'blocks/central/assets/js');
?>

<div style="width: 150px; " class="address-2 owl-carousel owl-theme">
    <?php foreach ($address as $item) { ?>
        <a title="<?php echo $item->address?>" target="_blank" href="<?php echo $item->link?>" style="display: block; white-space: nowrap; overflow: hidden; width: 150px; font-weight: 600; color: white"><?php echo $item->name ?></a>
    <?php } ?>
</div>