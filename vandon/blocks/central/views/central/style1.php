<?php
global $tmpl;
$tmpl->addScript('central', 'blocks/central/assets/js');
?>
<div class="detail-footer-bottom" style="background-color: #F4F4F4">
    <h6 class="text-center text-uppercase pb-2 show-footer-bottom" style="padding-top: 20px;">
        <?php echo FSText::_("Hệ thống trung tâm của anh ngữ Việt Mỹ") ?>
        <svg width="11" height="7" viewBox="0 0 11 7" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M5.5 0.5L0.303848 6.5L10.6962 6.5L5.5 0.5Z" fill="black" />
        </svg>
    </h6>
    <div class="container d-flex flex-wrap">
        <?php foreach ($address as $item) { ?>
            <div class="col-md-3 d-flex align-items-center mb-3">
                <svg style="min-width: 16px" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clip-path="url(#clip0_1652_10992)">
                        <path d="M7.99775 2C8.80025 2 9.55425 2.3125 10.1207 2.88C10.6873 3.4455 10.9998 4.199 10.9998 5C10.9998 5.801 10.6873 6.5545 10.1348 7.1065C10.0508 7.1895 9.01575 8.219 7.99825 9.827C6.98825 8.2215 5.95725 7.196 5.87925 7.12C5.31225 6.5545 4.99975 5.801 4.99975 5C4.99975 4.199 5.31225 3.4455 5.87875 2.879C6.44525 2.3125 7.15675 2 7.99775 2ZM7.99775 0C6.72025 0 5.44125 0.4885 4.46475 1.465C2.51175 3.417 2.51175 6.583 4.46475 8.5355C4.46475 8.5355 7.99775 12.0005 7.99775 16.0005C7.99775 12.0005 11.5347 8.5355 11.5347 8.5355C13.4878 6.5835 13.4878 3.4175 11.5347 1.465C10.5582 0.4885 9.27925 0 7.99775 0Z" fill="#999999" />
                        <path d="M8.99976 5C8.99976 5.5525 8.55226 6 7.99776 6C7.40526 6 6.99976 5.5525 6.99976 5C6.99976 4.4475 7.40526 4 7.99776 4C8.55226 4 8.99976 4.4475 8.99976 5Z" fill="#999999" />
                    </g>
                    <defs>
                        <clipPath id="clip0_1652_10992">
                            <rect width="16" height="16" fill="white" />
                        </clipPath>
                    </defs>
                </svg>

                <div class="ml-3" style="color: #515959">
                    <p class="mb-0"><?php echo $item->name ?></p>
                    <p class="mb-0"><?php echo $item->address ?></p>
                </div>
            </div>
        <?php } ?>
    </div>
</div>