<?php global $tmpl;
$tmpl->addScript('slide', 'blocks/banners/assets/js');
// $tmpl->addScript('adv', 'blocks/banners/assets/js');
$countItem = count($list_banner_categories);
$totalWidth = 0;
$paddingRight = 0;
foreach($list_banner_categories as $item) {
  $size_image = getimagesize($item->image);
  $totalWidth += $size_image[0];
}
if ($countItem > 1) {
  $paddingRight = (1200 - $totalWidth) / ($countItem - 1);
}
// print_r($list);die;
?>

<div class="row advertisment no-gutters mt-3">
  <div class="container">
    <div class="row no-gutters advertisment_wrapper">
      <?php foreach ($list_banner_categories as $key=>$item) { ?>
        <a href="<?php echo $item->link ?>" class="advertisment__div" style="<?php echo $key == ($countItem - 1) ? null : "padding-right: ".$paddingRight."px"?>">
          <img src="<?php echo $item->image ?>" alt="" >
          <!-- <span class="buy-now">Mua ngay</span> -->
        </a>
      <?php } ?>
    </div>
  </div>
</div>