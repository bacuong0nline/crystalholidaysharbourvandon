<?php global $tmpl;
$tmpl->addStylesheet('banner-category', 'blocks/banners/assets/css');

?>
<?php if (!empty($list_banner_categories)) { ?>
  <div class="col-md-9 banner-category no-gutters mb-md-5 mb-2">
    <div>
      <?php foreach ($list_banner_categories as $item) { ?>
        <img src="<?php echo $item->image ?>" alt="">
        <p class="category-name"><?php print_r($category->name) ?></p>
        <p class="category-summary"><?php print_r($category->summary) ?></p>
      <?php } ?>
    </div>
  </div>
<?php } ?>