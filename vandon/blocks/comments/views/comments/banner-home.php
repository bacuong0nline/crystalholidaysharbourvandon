<?php global $tmpl;
$tmpl->addStylesheet('banner-home', 'blocks/banners/assets/css');

$tmpl->addScript('slide', 'blocks/banners/assets/js');
?>
<div class="slide-home">
    <div class="owl-carousel owl-theme" id="banner">
        <?php foreach ($list as $key=>$item) { ?>
            <a style="position: relative; display: block; height: 450px" href="<?php echo $item->link ?>">
                <img src="<?php echo '/' . $item->image ?>" alt="" style="object-fit:cover; max-width: 100%; max-height: 100%;height: inherit !important;">
                <div class="center">
                    <div class="animate__animated animate__delay-1s <?php echo 'slide'.'-'.$key?>">
                        <?php echo $item->content ?>
                        <style>
                            .slide-home .owl-item.active <?php echo '.'.'slide'.'-'.$key?> {
                                animation: <?php echo str_replace("animate__", '', $item->effect) ?> ease 1s
                            }
                        </style>
                    </div>
                </div>
            </a>
        <?php } ?>
    </div>
</div>

<style>
    .center {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }

    .center p {
        color: white;
    }
</style>