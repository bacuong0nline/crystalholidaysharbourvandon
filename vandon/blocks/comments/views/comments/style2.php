<div style="position: relative; padding-bottom: 100px;" class="specialized-training snowflake1">
    <img class="wave1" style="position: absolute; top: -110px; width: 100%" src="./images/wave1.png" alt="wave1" />
    <div class="container pb-3">
        <p class="text-uppercase text-center title-block-1" data-aos="fade-right"><?php echo @$params['title1']  ?></p>
        <h5 class="title text-center text-uppercase title-block" data-aos="fade-left"><?php echo @$params['title'] ?></h5>
        <div class="col-12 d-flex flex-column align-items-center">
            <?php if (@$params['title2']) { ?>
                <p class="text-center font-weight-strong title-block-2"><?php echo @$params['title2'] ?></p>
            <?php } ?>
            <?php if (@$params['summary']) { ?>
                <p class="text-center summary2" style="width: 70%"><?php echo @$params['summary'] ?></p>
            <?php } ?>
        </div>
    </div>
    <div class="container">
        <div class="grid-container specialized-training-box">
            <?php foreach ($cat->child as $key => $item) { ?>
                <div class="item<?php echo $key + 1 ?> specialized-training-item">
                    <img src="<?php echo $item->image ?>" alt="<?php echo $item->image ?>" />
                    <div class="summary-training d-flex align-items-center">
                        <div>
                            <p class="text-uppercase font-weight-strong"><?php echo $item->name ?></p>
                            <p><?php echo $item->summary ?></p>
                        </div>
                    </div>
                    <p class="text-uppercase text-center title-training"><?php echo $item->name ?></p>
                </div>
            <?php } ?>
        </div>
    </div>
</div>