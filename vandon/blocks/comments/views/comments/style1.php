<?php
global $tmpl;
$tmpl->addScript('comment', 'blocks/comments/assets/js');
?>
<?php if (!empty($cat_comments->comments)) { ?>
    <div class="<?php echo $background == 1 ? 'snowflake1' : 'snowflake2' ?>" style="width: 100%; background-size: 100%; background-repeat: no-repeat; background-position: center center; position: relative; ">
        <div class="container pb-3">
            <p class="text-uppercase text-center title-block-1" data-aos="fade-right"><?php echo @$params['title1']  ?></p>
            <h5 class="title text-center text-uppercase title-block" data-aos="fade-left"><?php echo @$params['title'] ?></h5>
            <div class="col-12 d-flex flex-column align-items-center">
                <?php if (@$params['title2']) { ?>
                    <p class="text-center font-weight-strong title-block-2"><?php echo @$params['title2'] ?></p>
                <?php } ?>
                <?php if (@$params['summary']) { ?>
                    <p class="text-center summary2" style="width: 70%"><?php echo @$params['summary'] ?></p>
                <?php } ?>
            </div>
        </div>
        <div class="container target-box comment-box">
            <div class="owl-carousel owl-theme comments">
                <?php foreach ($cat_comments->comments as $item) { ?>
                    <div class="">
                        <div class="comment">
                            <p class="summary"><?php echo $item->comment ?></p>
                            <div class="name">
                                <?php if($item->title == 1) { 
                                    $a = 'Ông';   
                                } else if($item->title == 2) { 
                                    $a = 'Bà';
                                } else {
                                    $a = '';
                                }
                                    ?>
                                <img onerror="this.src='./images/no-user-image.jpeg'" src="<?php echo './' . str_replace('original', 'resized', $item->image) ?>" alt="<?php echo $item->image ?>">
                                <p class="text-capitalize mb-0"><?php echo $a ?> <span class="font-weight-bold"><?php echo $item->name ?></span></p>
                                <p class="text-capitalize "><?php echo $item->address ?></p>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <?php if ($background == 1) { ?>
            <img class="wave2" src="./images/wave2.png" alt="wave2" />
        <?php } else { ?>
            <img class="wave1" src="./images/wave1.png" alt="wave1" />
        <?php } ?>
    </div>
<?php } ?>