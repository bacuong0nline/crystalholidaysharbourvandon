<?php foreach ($list as $item) { ?>
  <?php if (@$item->image) { ?>
    <a style="position: relative; display: block" href="<?php echo $item->link ?>">
      <!-- <div style="background-image: url(<?php echo '/' . $item->image ?>); height: 450px; width: 100%; background-size: contain; background-repeat: no-repeat;">
      </div> -->
      <img src="<?php echo '/' . $item->image ?>" alt="" style="max-width: 100%;max-height: 100%;height: inherit !important;">
      <div class="center">
        <div class="animate__animated animate__delay-1s <?php echo $item->effect?>">
          <?php echo $item->content?>
        </div>  
      </div>
    </a>
  <?php } ?>
<?php } ?>
<style>
  .center {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
  .center p {
    color: white;
  }
</style>