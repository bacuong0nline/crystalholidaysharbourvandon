<?php
$params = array(
	'suffix' => array(
		'name' => 'Hậu tố',
		'type' => 'text',
		'default' => '_comments',
	),

	'title1' => array(
		'name' => 'Tiêu đề phụ 1',
		'type' => 'text',
		'default' => '',
	),
	'title' => array(
		'name' => 'Tiêu đề chính',
		'type' => 'text',
		'default' => '',
	),
	'title2' => array(
		'name' => 'Tiêu đề phụ 2',
		'type' => 'text',
		'default' => '',
	),
	'summary' => array(
		'name' => 'Tóm tắt',
		'type' => 'text',
		'default' => '',
	),
	'background' => array(
		'name' => 'Màu nền',
		'type' => 'select',
		'value' => array(
			1 => 'Trắng',
			2 => 'Xám'
		)
	),
	'style' => array(
		'name' => 'Style',
		'type' => 'select',
		'value' => array(
			'style1' => 'Kiểu 1',
			'style3' => 'Kiểu 2'

		)
	),

	'category_id' => array(
		'name' => 'Nhóm cảm nghĩ và chia sẻ',
		'class' => 'category-select',
		'type' => 'select',
		'value' => get_category(),
	),

);
function get_category()
{
	global $db;
	$query = " SELECT name, id
						FROM fs_comments_categories
						";
	$sql = $db->query($query);
	$result = $db->getObjectList();
	if (!$result)
		return;
	$arr_group = array();
	foreach ($result as $item) {
		$arr_group[$item->id] = $item->name;
	}
	return $arr_group;
}
function get_image()
{
	global $db;
	$query = " SELECT name, id, image
						FROM fs_banners
						";
	$sql = $db->query($query);
	$result = $db->getObjectList();
	if (!$result)
		return;
	$arr_group = array();
	foreach ($result as $item) {
		$arr_group[$item->id] = $item->name;
	}
	return $arr_group;
}
?>
