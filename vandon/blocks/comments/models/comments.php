<?php
class CommentsBModelsComments
{
	function __construct()
	{
		$fstable = FSFactory::getClass('fstable');
		$this->table_name = $fstable->_('fs_comments', 1);
		$this->table_comments_categories = $fstable->_('fs_comments_categories', 1);
		$this->table_education_categories = $fstable->_('fs_education_categories', 1);
		$this->table_menus = $fstable->_('fs_menus_items', 1);
	}
	function getList($category_id, $cat_home = null)
	{
		$where = '';
		if ($category_id) {
			$where .= ' AND category_id = ' . $category_id . ' ';
		}
		// return;
		$module = FSInput::get('module');
		$view = FSInput::get('view');
		$ccode = FSInput::get('ccode');
		$cid = FSInput::get('cid');
		$filter = FSInput::get('filter');


		$where2 = '';

		$Itemid = FSInput::get('Itemid', 1, 'int');
		$where .= " AND (listItemid = 'all'
							OR listItemid like '%,$Itemid,%')" . $where2;

		$query = ' SELECT *
						  FROM ' . $this->table_name . ' AS a
						  WHERE published = 1
						 ' . $where . ' ORDER BY ordering, id';
		global $db;
		$db->query($query);
		$list = $db->getObjectList();
		return $list;
	}
	function getListAdvertisment()
	{
		$query = " SELECT *
		FROM " . $this->table_name . " AS a
		WHERE category_id = 4
		";
		global $db;
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}
	function get_info_menu($itemId)
	{
		$query = " SELECT name
		FROM " . $this->table_menus . " AS a
		WHERE id = $itemId
		";
		global $db;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}
	function getList2($products_categories)
	{
		$where = '';
		if (!$products_categories)
			return;
		$where .= " AND products_categories like '%,$products_categories,%' ";
		$module = FSInput::get('module');
		$view = FSInput::get('view');
		$ccode = FSInput::get('ccode');
		$filter = FSInput::get('filter');

		// Itemid
		$Itemid = FSInput::get('Itemid', 1, 'int');
		$where .= " AND (listItemid = 'all'
							OR listItemid like '%,$Itemid,%')
							";

		$query = ' SELECT name,id,category_id,type,image,flash,content,link,height,width,is_news
						  FROM ' . $this->table_name . ' AS a
						  WHERE published = 1
						 ' . $where . ' ORDER BY ordering, id ';
		// print_r($query);die;
		global $db;
		$db->query($query);
		$list = $db->getObjectList();
		// print_r($list);die;
		return $list;
	}


	function get_info_categories($id_category)
	{
		if (!$id_category)
			return;
		$query = " SELECT id,name,alias,summary
							FROM " . $this->table_education_categories . " AS a
							WHERE published = 1 AND id = $id_category
							";
		global $db;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}
	function get_cat_comment($cid)
	{
		if (!$cid)
			return;
		$query = " SELECT *
							FROM " . $this->table_comments_categories . " AS a
							WHERE published = 1 AND education_categories like '%,$cid,%'
							";
		global $db;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}
	function get_list_comment($cid)
	{
		if (!$cid)
			return;
		$query = " SELECT *
						FROM " . $this->table_name . " AS a
						WHERE published = 1 AND category_id = $cid
						";
		// echo $query;die;
		global $db;
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}
	function getBannerFooter()
	{
		$Itemid = FSInput::get('Itemid', 1, 'int');

		$where = '';
		$where .= " AND (listItemid = 'all'
					OR listItemid like '%,$Itemid,%')
					";
		$query = " SELECT id,name,alias, image, link
						FROM " . $this->table_name . " AS a
						WHERE published = 1 AND category_id = 16 " . $where . "";

		global $db;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}
}
