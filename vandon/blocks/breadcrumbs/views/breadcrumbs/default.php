<?php
global $tmpl;
$tmpl->addScript('breadcrumbs', 'blocks/breadcrumbs/assets/js');
?>

<?php if(isset($breadcrumbs)){?>
    <div class='breadcrumb'>
        <div class='breadcrumbs-first'>
            <a href="<?php echo URL_ROOT ?>">Trang chủ</a>
        </div>
    	<?php $i = 0; ?>
    	<?php if(count($breadcrumbs)){?>
    		<?php foreach($breadcrumbs as $item){?>
    			<div class='breadcrumbs_sepa'>
                    <span><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                </div>
    			<select name="breadcrumb_item breadcrumb_<?php echo $i?>" class="breadcrumb_item breadcrumb_<?php echo $i?>">
    				<?php if(count($item)){?>
    					<?php foreach($item as $cat_item){?>
    						<option value="<?php echo $cat_item ['link'];?>" <?php  echo $cat_item['selected'] ?"selected='selected'":""; ?> ><?php echo $cat_item ['name'];?></option>
    					<?php }?>
    				<?php }?>
    			</select>
    			<?php $i ++;?>
    		<?php }?>
    	<?php }?>
    <div class="clearfix"></div>
    </div>
<?php }?>