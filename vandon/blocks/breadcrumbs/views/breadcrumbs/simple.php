<?php
    global $tmpl; 
    $tmpl -> addStylesheet('breadcrumbs_simple','blocks/breadcrumbs/assets/css');
    if($breadcrumbs){
        $total = count($breadcrumbs);
    }
if($breadcrumbs){
?>	
<div class='breadcrumbs mt-2 row-item'>
    <div class="container pl-0 pr-0">
        <ol class="breadcrumb row-item">
    	<?php if(isset($breadcrumbs) && !empty($breadcrumbs)){?>
    		<li  class='breadcrumb-item breadcumbs-first'>
    			<?php global $config;?>
    			<a style="font-size: 13px" title='<?php echo $config['site_name'] ?>' href="<?php echo URL_ROOT?>" rel='nofollow' >
                    <?php echo FSText::_("Trang chủ"); ?>
                </a>
    		</li >
    		<?php $i = 0; ?>
    			<?php foreach($breadcrumbs as $item){?>
    				<?php if(!$item[1]){?>
    					<li class="pl-0 breadcrumb-item <?php echo $i==($total-1)?  'active':'' ?>" >
                            <i class="fa fa-angle-right" aria-hidden="true" style="display: flex; align-items: center; font-size: 10px"></i>
                            <span style="font-size: 13px"><?php echo getWord(20,$item[0]); ?></span>
                        </li>
    				<?php } else {?>
    					<li class="pl-0 breadcrumb-item <?php echo $i==($total-1)?  'active':'' ?>">
                            <i class="fa fa-angle-right" style="display: flex; align-items: center" aria-hidden="true"></i>
                            <a style="font-size: 13px" href="<?php echo $item[1]; ?>" title="<?php echo $item[0]; ?>" >
                                <?php echo getWord(20,$item[0]); ?>
                            </a>
                        </li>
    				<?php }?>
    				<?php $i ++;?>
    			<?php }?>
    	<?php }?>
        </ol><!-- END: .breadcrumb-content -->
    </div>
</div><!-- END: .breadcrumb -->
<div class="clear"></div>
<?php } ?>