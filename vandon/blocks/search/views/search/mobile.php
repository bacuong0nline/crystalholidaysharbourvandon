<?php
global $tmpl;
$tmpl->addStylesheet("search", "blocks/search/assets/css");
// $tmpl->addScript("jquery.autocomplete", "blocks/search/assets/js");
// $tmpl->addScript("search-mobile", "blocks/search/assets/js");
?>

<form class="search-form-mobile pb-4" action="" name="search_form" id="search_form_mobile" method="get" onsubmit="javascript: submitSearch();return false;">

<div class="row no-gutters">
    <?php $link = FSRoute::_('index.php?module=search&view=search'); ?>
        <select name="" id="" class="select-search">
            <option value="">Sản phẩm</option>
        </select>
        <input id="keyword-mobile" name="keyword" class="search" type="text" placeholder="Bạn tìm kiếm sản phẩm nào?" />
        <div class="line"></div>
        <input type='hidden' name="module" value="search" />
        <input type='hidden' name="module" id="link_search_mobile" value="<?php echo $link; ?>">
        <input type='hidden' name="view" value="search" />
        <input type='hidden' name="Itemid" value="135" />
    <a href="#" class="search-submit text-decoration-none">Tìm kiếm</a>
</div>
</form>