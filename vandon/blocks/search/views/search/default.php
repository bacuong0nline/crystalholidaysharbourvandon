<?php
global $tmpl;
$tmpl->addStylesheet("search", "blocks/search/assets/css");
$tmpl->addScript("search", "blocks/search/assets/js");

?>


<span class="me-5" style="position: relative">
    <?php $link = FSRoute::_('index.php?module=search&view=search'); ?>
    <svg style="cursor: pointer" class="expand-search" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
        <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
    </svg>
    <form id="form_search" class="search-form" style="width: initial; display:none;  position: absolute; right: -14px; top: -15px; z-index: 999"  name="form_search" method="post" onsubmit="submitSearch();return false;"> 
        <input placeholder="<?php echo FSText::_("Tìm kiếm...") ?>" type="text" id="keyword" name="keyword" class="input-search" style="height: 48px; opacity: 0px; font-size: 13px; padding: 10px 15px 10px 15px; padding-left: 10px; width: 0px; border-radius: 30px; border: 1px solid #ccc">
        <a class="collapse-search" style="position: absolute; right: 15px; top: 15px;">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#333" class="bi bi-search" viewBox="0 0 16 16">
                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
            </svg>
        </a>
        <input type="hidden" id="link_search" name="link_search" value="<?php echo $link; ?>">
    </form>
</span>

<style>
    .collapse-search svg {
        fill: #333 !important;
    }
</style>