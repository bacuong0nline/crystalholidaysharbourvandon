<?php
global $tmpl;
$tmpl->addStylesheet("search", "blocks/search/assets/css");
// $tmpl->addScript("jquery.autocomplete", "blocks/search/assets/js");
$tmpl->addScript("search", "blocks/search/assets/js");
// $tmpl->addScript("search-mobile", "blocks/search/assets/js");

?>

<!-- <form class="search-form pb-4" action="" name="search_form" id="search_form" method="get" onsubmit="javascript: submitSearch();return false;">

    <div class="row no-gutters">
        <?php $link = FSRoute::_('index.php?module=search&view=search'); ?>
        <select name="" id="" class="select-search">
            <option value="">Sản phẩm</option>
        </select>
        <div id="result">
            <input id="keyword" name="keyword" class="search" type="text" placeholder="Bạn tìm kiếm sản phẩm nào?" />
        </div>
        <div class="line"></div>
        <input type='hidden' name="module" value="search" />
        <input type='hidden' name="module" id="link_search" value="<?php echo $link; ?>">
        <input type='hidden' name="view" value="search" />
        <input type='hidden' name="Itemid" value="135" />
        <a href="#" class="search-submit text-decoration-none">Tìm kiếm</a>
    </div>
</form> -->

<div class="search-box ">
    <?php $link = FSRoute::_('index.php?module=search&view=search'); ?>
    <form id="form_search" class="form_search" name="form_search" method="post" onsubmit="submitSearch();return false;">
        <input class="form-control input_search" type="text" id="keyword" name="keyword" value="<?php echo FSText::_("Gõ từ khóa tìm kiếm")?>" onblur="if(this.value=='') this.value=<?php echo FSText::_('Gõ từ khóa tìm kiếm')?>" onfocus="if(this.value==<?php echo FSText::_('Gõ từ khóa tìm kiếm') ?>) this.value=''">
        <!-- <a class="bt-search-main" id="bt-search" onclick="submit_form_search();"> <i class="fa fa-search"></i></a> -->
        <input type="hidden" id="link_search" name="link_search" value="<?php echo $link; ?>">
    </form>
</div>