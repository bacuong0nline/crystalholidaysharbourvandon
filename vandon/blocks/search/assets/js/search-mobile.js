$('#keyword-mobile').autocomplete({
  serviceUrl: "/index.php?module=products&view=search&raw=1&task=get_ajax_search",
  groupBy: "brand",
  minChars: 2,
  formatResult: function (n, t) {
      t = t.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
      var i = n.data.text.split(" "), r = "";
      for (j = 0; j < i.length; j++)
          r += t.toLowerCase().indexOf(i[j].toLowerCase()) >= 0 ? "<strong>" + i[j] + "</strong> " : i[j] + " ";
          old_price = n.data.unit === undefined ? '' : '/ ' + n.data.unit
          $txt = '<a href = "' + n.value + '" > <img src = "' + n.data.image + '" /> <label> <span class="name"> ' + r + ' </span> <span class = "price"> ' + n.data.price + old_price + '</span></label></a>'
      return $txt + ' <div class="clearfix"></div>';
  },
  onSelect: function (n) {
      $(".control input[name=kwd]").val(n.data.text)
  }
});
function submitSearchMobile() {
  url = '';
  var keyword = $('#keyword-mobile').val();
  keyword = encodeURIComponent(encodeURIComponent(keyword));
  //		var link_search = $('#link_search').val();
  var link_search = $('#link_search_mobile').val();
  if (keyword != 'Nhập từ khóa tìm kiếm...' && keyword != '') {

      url += '&keyword=' + keyword;
      var check = 1;
  } else {
      var check = 0;
  }
  if (check == 0) {
      alert('Bạn phải nhập tham số tìm kiếm');
      return false;
  }
  var link = link_search + '/' + keyword + '.html';
  window.location.href = link;
  return false;
}

$(".btn-search").click(function () {
  $("#search_form_mobile").submit()
})