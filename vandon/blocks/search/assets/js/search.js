// $('#keyword').autocomplete({
//     serviceUrl: "/index.php?module=products&view=search&raw=1&task=get_ajax_search",
//     groupBy: "brand",
//     minChars: 2,
//     appendTo: "#result",
//     formatResult: function (n, t) {
//         t = t.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
//         var i = n.data.text.split(" "), r = "";
//         for (j = 0; j < i.length; j++)
//             r += t.toLowerCase().indexOf(i[j].toLowerCase()) >= 0 ? "<strong>" + i[j] + "</strong> " : i[j] + " ";
//             old_price = n.data.unit === undefined ? '' : '/ ' + n.data.unit
//             $txt = '<a href = "' + n.value + '" > <img src = "' + n.data.image + '" /> <label> <span class="name"> ' + r + ' </span> <span class = "price"> ' + n.data.price + old_price + '</span></label></a>'
//         return $txt + ' <div class="clearfix"></div>';
//     },
//     onSelect: function (n) {
//         $(".control input[name=kwd]").val(n.data.text)
//     }
// });

$(document).ready(function() {
    $(".expand-search").click(function () {
        console.log(2)
        $(".search-form").fadeIn()
        setTimeout(function () {
            $(".input-search").css({ "width": "190px", "transition": ".3s", "opacity": "1" })
        }, 300)
    })

    $(".collapse-search").click(function () {
        $(".input-search").css({ "width": "0px", "transition": ".3s", "opacity": "0" })
        $(".search-form").fadeOut()
    })

    $(window).click(function () {
        $(".input-search").css({ "width": "0px", "transition": ".3s", "opacity": "0" })
        $(".search-form").fadeOut()    });

    $('.search-form, .expand-search, .collapse-search').click(function (event) {
        event.stopPropagation();
    });
})

function submitSearch() {
    url = '';
    var keyword = $('#keyword').val();
    keyword = encodeURIComponent(encodeURIComponent(keyword));
    //		var link_search = $('#link_search').val();
    var link_search = $('#link_search').val();
    if (keyword != 'Nhập từ khóa tìm kiếm...' && keyword != '') {

        url += '&keyword=' + keyword;
        var check = 1;
    } else {
        var check = 0;
    }
    if (check == 0) {
        alert('Bạn phải nhập tham số tìm kiếm');
        return false;
    }
    var link = link_search + '/' + keyword + '.html';
    console.log(link);
    window.location.href = link;
    return false;
}

$(".btn-search").click(function () {
    $("#search_form").submit()
})