<?php
class EducationBModelsEducation
{
	function __construct()
	{
		$fstable = FSFactory::getClass('fstable');
		$this->table_name = $fstable->_('fs_education', 1);
		$this->table_news = $fstable->_('fs_news', 1);
		$this->table_products_categories = $fstable->_('fs_products_categories', 1);
		$this->table_categories = $fstable->_('fs_education_categories', 1);
		$this->table_menus = $fstable->_('fs_menus_items', 1);

	}
	function getList($category_id, $cat_home = null)
	{
		$where = '';
		if ($category_id) {
			$where .= ' AND category_id = ' . $category_id . ' ';
		}
		// return;
		$module = FSInput::get('module');
		$view = FSInput::get('view');
		$ccode = FSInput::get('ccode');
		$cid = FSInput::get('cid');
		//			$cat = $this->get_cats($ccode);
		$filter = FSInput::get('filter');

		$where2 = '';

		$Itemid = FSInput::get('Itemid', 1, 'int');
		$where .= " AND (listItemid = 'all'
							OR listItemid like '%,$Itemid,%')" . $where2;

		$query = ' SELECT *
						  FROM ' . $this->table_name . ' AS a
						  WHERE published = 1
						 ' . $where . ' ORDER BY ordering, id';
		global $db;

		$db->query($query);
		$list = $db->getObjectList();
		return $list;
	}

	function get_category_detail($cat_id) {
		$query = " SELECT id, name, alias
		FROM " . $this->table_categories . " AS a
		WHERE id = $cat_id limit 5
		";
		// echo $query;die;
		global $db;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}

	function get_category_child($cat_id) {
		$query = " SELECT id, name, alias, image, summary, title_home, hover
		FROM " . $this->table_categories . " AS a
		WHERE parent_id = $cat_id and published = 1 order by ordering limit 5
		";
		// echo $query;die;
		global $db;
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}
	function get_link($cid) {
		$query = " SELECT link
		FROM " . $this->table_menus . " AS a
		WHERE link like '%cid=$cid&%' and link like '%module=education%'
		";
		// echo $query;die;
		global $db;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}
}
