<?php
$params = array(
	'suffix' => array(
		'name' => 'Hậu tố',
		'type' => 'text',
		'default' => '_education',
	),
	//		'id' => array(
	//					'name' => 'Id (dùng cho qcáo bên ngoài trang)',
	//					'type' => 'text',
	//					'default' => 'divAdLeft',
	//					'comment' => 'divAdLeft dùng cho bên trái, divAdRight cho bên phải'
	//					),
	'title1' => array(
		'name' => 'Tiêu đề phụ 1',
		'type' => 'text',
		'default' => '',
	),
	'title' => array(
		'name' => 'Tiêu đề chính',
		'type' => 'text',
		'default' => '',
	),
	'title2' => array(
		'name' => 'Tiêu đề phụ 2',
		'type' => 'text',
		'default' => '',
	),

	'summary2' => array(
		'name' => 'Tóm tắt',
		'type' => 'text',
		'default' => '',
	),
	'background' => array(
		'name' => 'Màu nền',
		'type' => 'select',
		'value' => array(
			1 => 'Trắng',
			2 => 'Xám'
		)
	),
	'style' => array(
		'name' => 'Style',
		'type' => 'select',
		'value' => array(
			'style1' => 'Kiểu 1',
			'style2' => 'Kiểu 2',
		)
	),

	'category_id' => array(
		'name' => 'Nhóm chương trình đào tạo',
		'class' => 'category-select',
		'type' => 'select',
		'value' => get_category(),
		// 'attr' => array('multiple' => 'multiple'),
	),
	// 'image' => array(
	// 	'name' => 'Ảnh',
	// 	'class' => 'image-select',
	// 	'type' => 'select',
	// 	'value' => get_image(),
	// 	// 'attr' => array('multiple' => 'multiple'),
	// ),
);
function get_category()
{
	global $db;
	$query = " SELECT name, id
						FROM fs_education_categories where level = 0
						";
	$sql = $db->query($query);
	$result = $db->getObjectList();
	if (!$result)
		return;
	$arr_group = array();
	foreach ($result as $item) {
		$arr_group[$item->id] = $item->name;
	}
	return $arr_group;
}
function get_image()
{
	global $db;
	$query = " SELECT name, id, image
						FROM fs_banners
						";
	$sql = $db->query($query);
	$result = $db->getObjectList();
	if (!$result)
		return;
	$arr_group = array();
	foreach ($result as $item) {
		$arr_group[$item->id] = $item->name;
	}
	return $arr_group;
}
?>
