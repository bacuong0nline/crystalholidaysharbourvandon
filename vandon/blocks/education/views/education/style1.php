<?php
global $tmpl;
$tmpl->addStyleSheet("style1", "blocks/education/assets/css");
?>
<div style="position: relative; padding-bottom: 100px;" class="route <?php echo @$background == 1 ? 'snowflake1' : 'snowflake2' ?>">
    <div class="container pb-3">
        <p class="text-uppercase text-center title-block-1" data-aos="fade-right"><?php echo @$params['title1']  ?></p>
        <h5 class="title text-center text-uppercase title-block" data-aos="fade-left"><?php echo @$params['title'] ?></h5>
        <div class="col-12 d-flex flex-column align-items-center">
            <?php if (@$params['title2']) { ?>
                <p class="text-center font-weight-strong title-block-2"><?php echo @$params['title2'] ?></p>
            <?php } ?>
            <?php if (@$params['summary2']) { ?>
                <p class="text-center summary2" style="width: 76%"><?php echo @$params['summary2'] ?></p>
            <?php } ?>
        </div>
    </div>
    <div class="container d-flex flex-wrap flex-md-row justify-content-center justify-content-md-between">
        <?php foreach ($cat->child as $key => $item) { ?>
            <a href="<?php echo $item->link ?>" class="col-6 route-item col-md" title="<?php echo $item->name ?>">
                <img class="image-education" data-aos="fade-zoom-in" data-aos-delay="<?php echo 300 * $key ?>" data-aos-duration="900" src="<?php echo $item->image ?>" alt="<?php echo $item->image ?>" />
                <img class="image-hover-education" src="<?php echo $item->hover ?>" alt="<?php echo $item->hover ?>" />
                <div class="d-flex flex-column align-items-center">
                    <p class="text-uppercase text-center title-education mb-1" style="font-size: 18px; font-weight: 600; color: #333"><?php echo $item->name ?></p>
                    <p class="text-center" style="color: #767676; width: 60%"><?php echo $item->title_home ?></p>
                </div>
            </a>
        <?php } ?>
    </div>
    <?php if (@$background == 1) { ?>
        <img class="wave2" src="./images/wave2.png" alt="wave2" />
    <?php } else { ?>
        <img class="wave1" src="./images/wave1.png" alt="wave1" />
    <?php } ?>
</div>