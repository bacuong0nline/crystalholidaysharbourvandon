<div style="position: relative; padding-bottom: 100px;" class="specialized-training snowflake1">
    <div class="container pb-3">
        <p class="text-uppercase text-center title-block-1" data-aos="fade-right"><?php echo @$params['title1']  ?></p>
        <h5 class="title text-center text-uppercase title-block" data-aos="fade-left"><?php echo @$params['title'] ?></h5>
        <div class="col-12 d-flex flex-column align-items-center">
            <?php if (@$params['title2']) { ?>
                <p class="text-center font-weight-strong title-block-2"><?php echo @$params['title2'] ?></p>
            <?php } ?>
            <?php if (@$params['summary2']) { ?>
                <p class="text-center summary2" style="width: 70%"><?php echo @$params['summary2'] ?></p>
            <?php } ?>
        </div>
    </div>
    <div class="container">
        <?php if (count($cat->child) == 5) { ?>
            <div class="grid-container specialized-training-box">
                <?php foreach ($cat->child as $key => $item) { ?>
                    <a data-aos="<?php echo ($key + 1) % 2 == 0 ? 'fade-right' : 'fade-left' ?>" data-aos-delay="<?php echo $key * 300 ?>" href="<?php echo @$item->link ?>" class="item<?php echo $key + 1 ?> specialized-training-item">
                        <img src="<?php echo $item->image ?>" alt="<?php echo $item->image ?>" />
                        <div class="summary-training d-flex flex-column justify-content-between">
                            <div>
                                <p style="font-weight: 600; font-size: 16px;" class="text-uppercase"><?php echo $item->name ?></p>
                                <p><?php echo $item->summary ?></p>
                            </div>
                            <div>
                                <p class="mb-0">
                                    <?php echo FSText::_("Chi tiết") ?>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                                    </svg>
                                </p>
                            </div>
                        </div>
                        <p class="text-uppercase text-center title-training"><?php echo $item->name ?></p>
                    </a>
                <?php } ?>
            </div>
        <?php } else { ?>
            <div class="grid-container-2 specialized-training-box">
                <?php foreach ($cat->child as $key => $item) { ?>
                    <a data-aos="<?php echo ($key + 1) % 2 == 0 ? 'fade-right' : 'fade-left' ?>" data-aos-delay="<?php echo $key * 300 ?>" href="<?php echo @$item->link ?>" class="item<?php echo $key + 1 ?> specialized-training-item">
                        <img src="<?php echo $item->image ?>" alt="<?php echo $item->image ?>" />
                        <div class="summary-training d-flex flex-column justify-content-between">
                            <div>
                                <p style="font-weight: 600; font-size: 16px;" class="text-uppercase"><?php echo $item->name ?></p>
                                <p><?php echo $item->summary ?></p>
                            </div>
                            <div>
                                <p class="mb-0">
                                    <?php echo FSText::_("Chi tiết") ?>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                                    </svg>
                                </p>
                            </div>
                        </div>
                        <p class="text-uppercase text-center title-training"><?php echo $item->name ?></p>
                    </a>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
    <img style="position: absolute; bottom: 0px; width: 100%" src="./images/wave2.png" alt="wave2" />
</div>