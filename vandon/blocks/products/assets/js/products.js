$(document).ready(function () {
   $('.sider-slick-add').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        arrows: false,
        dots: true,
        responsive: [
            {
                 breakpoint: 767,
                 settings: {
                      slidesToShow: 2,
                      slidesToScroll: 2,
                 }
            },
            {
                 breakpoint: 900,
                 settings: {
                      slidesToShow: 2,
                      slidesToScroll: 2,
                 }
            }
       ]
    });
});

function change_image_price_bg(price,image,id,id_block,element) {
    $("#block_id_"+id_block+" #img-bg-hot-"+id).attr("src", image);
    $("#block_id_"+id_block+" #price-bg-"+id).html(price);
    $('#block_id_'+id_block+' ul li').removeClass('active');
    $(element).parent("li").addClass("active");
}
