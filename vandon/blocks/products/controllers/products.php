<?php

/*
 * Huy write
 */
// models 
include 'blocks/products/models/products.php';

class ProductsBControllersProducts {

    function __construct() {
        
    }

    function display($parameters) {
        $model = new ProductsBModelsProducts();
        $limit = $parameters->getParams('limit');
        $type = $parameters->getParams('type');
        $style = $parameters->getParams('style');
        $category_id = $parameters->getParams('category_id');
        $price_range = $parameters->getParams('price_range');
        $list = $model->getListCat();
        if (!empty($list))
            foreach ($list as $item) {
                $banner = $model->getBannerCatHome($item->id);
                $item->childs = $model->getCatChild($item->id);
                $item->products = $model->getProductsCat($item->id);
                $item->banner_image = @$banner->image;
                $item->banner_link = @$banner->link;
            }
        include 'blocks/products/views/products/' . $style . '.php';
    }
    


}

?>