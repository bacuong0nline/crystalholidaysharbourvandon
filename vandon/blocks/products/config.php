<?php

$params = array(
    'suffix' => array(
        'name' => 'Hậu tố',
        'type' => 'text',
        'default' => '_products'
    ),
    'limit' => array(
        'name' => 'Giới hạn',
        'type' => 'text',
        'default' => '6'
    ),
    'type' => array(
        'name' => 'Lấy theo',
        'type' => 'select',
        'value' => array(
            'ordering' => 'Mới nhất',
            'hot' => 'Bán chạy',
        ),
        // 'attr' => array('multiple' => 'multiple'),
    ),
    'price_range' => array(
        'name' => 'Tầm giá',
        'type' => 'select',
        'value' => array(
            '1' => 'Cao cấp',
            '2' => 'Trung cấp',
            '3' => 'Phổ thông',
        ),
        // 'attr' => array('multiple' => 'multiple'),
    ),
    'style' => array(
        'name' => 'Style',
        'type' => 'select',
        'value' => array(
            'default' => 'Có nền',
            'default_cat' => 'Không có nền',
            'default_left' => 'Bên trái',
            'default_bot' => 'Bên dưới'
        ),
    ),
    'category_id' => array(
        'name' => 'Nhóm danh mục',
        'type' => 'select',
        'value' => get_category(),
	'attr' => array('multiple' => 'multiple'),
    ),
);

function get_category() {
    global $db;
    $query = " SELECT name, id 
						FROM fs_products_categories 
						";
    $sql = $db->query($query);
    $result = $db->getObjectList();
    if (!$result)
        return;
    $arr_group = array();
    foreach ($result as $item) {
        $arr_group[$item->id] = $item->name;
    }
    return $arr_group;
}

?>