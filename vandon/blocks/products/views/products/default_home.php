<?php
global $config, $tmpl;
$tmpl->addStylesheet("left", "blocks/products/assets/css");
?>
<?php if (!empty($list)) { ?>
    <?php foreach ($list as $item) { ?>
        <?php if (!empty($item->products)) { ?>
            <div>
                <div class="row pt-3 title-cat-home">
                    <a href="<?php echo FSRoute::_("index.php?module=products&view=cat&cid=$item->id&ccode=$item->alias") ?>" class="col d-flex pl-0 remove-link title-box">
                        <img src="<?php echo $item->icon_home ?>" alt="<?php echo $item->icon_home ?>">
                        <h5 class="text-uppercase d-flex align-items-end ml-3 title-category"><?php echo $item->name ?></h5>
                    </a>
                    <div class="cat-child-home" style="margin-bottom: 7px;">
                        <dl class="d-flex justify-content-between align-items-end" style="height: 100%">
                            <?php foreach ($item->childs as $val) { ?>
                                <dt class="font-weight-normal ml-3">
                                    <a href="<?php echo FSRoute::_("index.php?module=products&view=cat&cid=$val->id&ccode=$val->alias") ?>" class="remove-link">
                                        <?php echo $val->name ?>
                                    </a>
                                </dt>
                            <?php } ?>
                        </dl>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 border-bottom mb-3">
                    </div>
                </div>
                <div class="desktop-cat-child row <?php echo count($item->products) > 1 ? 'justify-content-between' : '' ?> ">
                    <?php foreach ($item->products as $key => $row) { ?>
                        <?php if ($key == 0) { ?>
                            <?php echo $tmpl->product_item($row) ?>
                        <?php } ?>
                    <?php } ?>
                    <a href="<?php echo $item->banner_link ?>" class="banner-category-home" style="background-image: url(<?php echo $item->banner_image ?>), url(/images/no-image2.jpg); width: 600px; height: 360px; background-repeat: no-repeat; background-size: contain">
                    </a>
                    <?php foreach ($item->products as $key => $row) { ?>
                        <?php if ($key > 0) {  ?>
                            <?php echo $tmpl->product_item($row) ?>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="mobile-cat-child row <?php echo count($item->products) > 1 ? 'justify-content-between' : '' ?> ">
                    <a href="<?php echo $item->banner_link ?>" class="banner-category-home-mobile">
                        <img style="margin-bottom: 10px; width: 100%; max-height: 250px; height: auto; display: block; background-repeat: no-repeat; object-fit: contain" onerror="/images/no-image2.jpg" src="<?php echo $item->banner_image ?>" alt="" />
                    </a>
                    <?php foreach ($item->products as $key => $row) { ?>
                        <?php echo $tmpl->product_item($row) ?>
                    <?php } ?>
                </div>
            </div>
            <?php if ($tmpl->count_block('middle')) { ?>
                <?php echo $tmpl->load_position('middle', '', $item->id); ?>
            <?php } ?>
        <?php } ?>
    <?php } ?>
<?php } ?>