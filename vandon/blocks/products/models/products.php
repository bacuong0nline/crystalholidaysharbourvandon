<?php

class ProductsBModelsProducts {

    function __construct() {
        $fstable = FSFactory::getClass('fstable');
        $this->table_name  = $fstable->_('fs_products',1);
        $this->table_categories  = $fstable->_('fs_products_categories',1);
        $this->table_banner  = $fstable->_('fs_banners',1);
    }
    function setQuery($limit,$type,$style,$str_cats,$price_orange) {
        $ccode = FSInput::get('ccode');
        $where = '';
        $order = '';
        if ($str_cats)
            $where .= ' AND category_id in (' . $str_cats .')' ;
//        $where .= ' AND category_id_wrapper LIKE "%,' . $str_cats . ',%" ';

        switch ($type) {
            case 'home':
                $where .= '  AND show_in_homepage = 1 ';
                break; 
            case 'hot':
                $where .= '  AND is_hot = 1 ';
                break;
            case 'ordering':
                $order .= ' ordering DESC , created_time DESC, ';
                break;
        }
        switch ($price_orange) {
            case '0':
                $where .= ' ';
                break;
            case '1':
                $where .= '  AND price_range = 1 ';
                break;
            case '2':
                $where .= ' AND price_range = 2 ';
                break;
            case '3':
                $where .= ' AND price_range = 3 ';
                break;
        }
        $order .= ' id DESC';
        $query = ' SELECT name,price_old,price,discount,discount_unit,id,alias,category_id,image,category_alias,bestseller
						  FROM ' . $this->table_name . '
						  WHERE  published = 1 ' . $where . '
						  ORDER BY  ' . $order . '
						  LIMIT ' . $limit
        ;
        return $query;
    }

    function get_list($limit,$type,$style,$str_cats,$price_orange) {
        global $db;
        $query = $this->setQuery($limit,$type,$style,$str_cats,$price_orange);
        if (!$query)
            return;
        $sql = $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }
    
    function get_cat($id){
        $where = '';
        if ($id) {
            $where .= ' AND id in (' . $id .')' ;
        }
        global $db;
        $query = ' SELECT id,name, alias, list_parents,image,level,parent_id,icon
					FROM ' . $this->table_categories . ' 
					WHERE published = 1 ' . $where . '
					ORDER BY ordering
							';
        $db->query($query);
        $result = $db->getObject();
        return $result;
    }

    function get_price_colors($id){
        $where = '';
        if ($id) {
            $where .= ' record_id =' . $id;
        } else{
            return false;
        }
        global $db;
        $query = ' SELECT id,record_id,color_id,color_code,image,price
					FROM ' . $this->table_price . ' 
					WHERE ' . $where . '
					ORDER BY id
							';
        $db->query($query);
        $result = $db->getObjectList();
        return $result;
    }

    function getListCat(){

        $query = "SELECT name, alias, id, level, parent_id as parent_id, alias, list_parents, icon, icon_home
						  FROM ".$this->table_categories." AS a
						  WHERE published = 1 and level = 0 AND show_in_homepage = 1
						  ORDER BY ordering ASC, id ASC
						 ";
        global $db;
        $db->query($query);
        $category_list = $db->getObjectList();
        if(!$category_list)
            return;
        $tree_class  = FSFactory::getClass('tree','tree/');
        return $list = $tree_class -> indentRows($category_list,3);
    }
    function getProductsCat($id) {
        $query = "SELECT name, image, alias, id, category_id, category_alias, price, price_old
                FROM ".$this->table_name." AS a
                WHERE published = 1 and category_id_wrapper like '%,".$id.",%' AND show_in_home = 1
                ORDER BY  ordering ASC, created_time DESC LIMIT 6";
        global $db;
        $db->query($query);
        $result = $db->getObjectList();
        if(!$result)
            return;
        return $result;
    }
    function getCatChild($id) {
        $query = "SELECT name, alias, id, level, parent_id as parent_id, alias, list_parents
						  FROM ".$this->table_categories." AS a
						  WHERE published = 1 and parent_id = $id
						  ORDER BY ordering ASC, id ASC
						 ";
        global $db;
        $db->query($query);
        $category_list = $db->getObjectList();
        return $category_list;
    }
    function getBannerCatHome($id) {
        $query = "SELECT name, alias, id, category_id, image, link
						  FROM ".$this->table_banner." AS a
						  WHERE published = 1 and listItemid like '%,1,%' and products_categories like '%,$id,%'
						  ORDER BY ordering ASC, id ASC
						 ";
        global $db;
        $db->query($query);
        $result = $db->getObject();
        return $result;
    }
}
?>
