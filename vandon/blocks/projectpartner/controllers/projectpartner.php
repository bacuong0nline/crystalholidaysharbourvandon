<?php
  include 'blocks/projectpartner/models/projectpartner.php';
  class ProjectpartnerBControllersProjectpartner {
    function __construct() {
    }
    function display($parameters,$title) {

      $model = new ProjectpartnerBModelsProjectpartner();

      $id = FSInput::get('cid');
      $limit = 4;

      $cat = $model->getListCat();
      
      $cat = filterProjectPartner($cat, $model, $id, $limit);

      // print_r($list_project_partner);die;



      $style = $parameters->getParams('style');
      $style = $style ? $style : 'default';


      include 'blocks/projectpartner/views/projectpartner/'.$style.'.php';
    }
  }
  function filterProjectPartner($cat, $model, $id, $limit) {
    $i = 0;
    foreach($cat as $item) {
      $project_partner_items = $model->getListProjectPartner($item->id);
      $list_project_partner = array();
      foreach($project_partner_items as $row ) {
        $arrdisplay = explode(',', $row->display_category_id);
        if (in_array($id, $arrdisplay)) {
          array_push($list_project_partner, $row);
        }
        $countItems = count($list_project_partner);
        if ($countItems > 4)  {
          $cat[$i]->pp = array_slice($list_project_partner, 0, $limit);
        } else {
          $cat[$i]->pp = $list_project_partner;
        }
      }
      $i++;
    }
    return $cat;
  }
?>