<?php
  class ProjectpartnerBModelsProjectpartner extends FSModels {
		function __construct()
		{
            $fstable = FSFactory::getClass('fstable');
            $table_filter = "";

            $this->table_name = $fstable->_('fs_project_partner',1);
            $this->table_categories = $fstable->_('fs_project_partner_categories',1);
    }
    function getListCat() {
      $where = '';
      $query = " SELECT *
      FROM " . $this->table_categories . " 
      WHERE published = 1 ".$where." ORDER BY ordering DESC";
      global $db;
      $sql = $db->query($query);
      $result = $db->getObjectList();
      return $result;
    }
    function getListProjectPartner($id) {
      $where = "AND category_id = ".$id." ";
      $query = " SELECT *
      FROM " . $this->table_name . " 
      WHERE published = 1 ".$where." ORDER BY created_time DESC";
      global $db;
      $sql = $db->query($query);
      $result = $db->getObjectList();
      return $result;
    }
  }

?>