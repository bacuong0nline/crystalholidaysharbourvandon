<?php
global $tmpl;
$tmpl->addStylesheet('default', 'blocks/projectpartner/assets/css');
// print_r($cat);die;

?>

  <?php foreach ($cat as $row) { ?>
    <?php if(!empty($row->pp)) { ?>
    <div class="row no-gutters">
      <div class="col mt-4 no-gutters">
        <div class="col no-gutters title no-gutters title-line">
          <a href="<?php echo FSRoute::_("index.php?module=projectpartner&view=cat&ccode=".$row->alias."&cid=".$row->id."") ?>"><?php echo $row->name ?></a>
        </div>
        <div class="col">
          <a href="<?php echo FSRoute::_("index.php?module=projectpartner&view=cat&ccode=".$row->alias."&cid=".$row->id."") ?>" class="show-more">Xem tất cả ></a>
        </div>
      </div>
      <div class="w-100"></div>
      <div class="col no-gutters">
        <div class="row no-gutters mt-3">
            <?php foreach ($row->pp as $item) { ?>
              <div class="col-6 col-md-3 project">
                <a href="<?php echo FSRoute::_("index.php?module=projectpartner&view=home&id=" . $item->id . "&code=" . $item->alias . " ")?>">
                  <img src="<?php echo $item->image ?>" alt="<?php echo $item->alias ?>" />
                  <p class="truncate">
                    <?php echo $item->title ?>
                  </p>
                </a>
              </div>
            <?php } ?>
        </div>
      </div>
    </div>
    <?php } ?>
  <?php } ?>
