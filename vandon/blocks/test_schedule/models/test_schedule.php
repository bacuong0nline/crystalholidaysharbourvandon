<?php
class Test_scheduleBModelsTest_schedule
{
	function __construct()
	{
		$fstable = FSFactory::getClass('fstable');
		$this->table_name = $fstable->_('fs_banners', 1);
		$this->table_news = $fstable->_('fs_news', 1);
		$this->table_products_categories = $fstable->_('fs_products_categories', 1);
		$this->table_categories = $fstable->_('fs_banners_categories', 1);
		$this->table_menus = $fstable->_('fs_menus_items', 1);
		$this->table_block = $fstable->_('fs_blocks', 1);

	}

	function get_content($params2)
	{
		$query = " SELECT *
		FROM " . $this->table_block . " AS a
		WHERE params = '$params2'
		";
		global $db;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}
	function get_link($cid) {
		$query = " SELECT link
		FROM " . $this->table_menus . " AS a
		WHERE link like '%cid=$cid&%' and link like '%module=schedule%'
		";
		// echo $query;die;
		global $db;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}
	function get_list_schedule()
    {
        $fs_table = FSFactory::getClass('fstable');

        global $db;
        $query = " SELECT id, name, image";
        $query .= " FROM " . $fs_table->getTable('fs_schedule_categories', 1) . "
        WHERE  published = 1 and show_in_homepage = 1 ORDER BY ordering ASC, id DESC LIMIT 4";
        $sql = $db->query($query);
        $result = $db->getObjectList();

        return $result;
    }
	function getList2($products_categories)
	{
		$where = '';
		if (!$products_categories)
			return;
		$where .= " AND products_categories like '%,$products_categories,%' ";
		$module = FSInput::get('module');
		$view = FSInput::get('view');
		$ccode = FSInput::get('ccode');
		$filter = FSInput::get('filter');

		// Itemid
		$Itemid = FSInput::get('Itemid', 1, 'int');
		$where .= " AND (listItemid = 'all'
							OR listItemid like '%,$Itemid,%')
							";

		$query = ' SELECT name,id,category_id,type,image,flash,content,link,height,width,is_news
						  FROM ' . $this->table_name . ' AS a
						  WHERE published = 1
						 ' . $where . ' ORDER BY ordering, id ';
		// print_r($query);die;
		global $db;
		$db->query($query);
		$list = $db->getObjectList();
		// print_r($list);die;
		return $list;
	}
	function get_news()
	{
		$query = " SELECT id,title,alias,category_id,category_name,category_alias
						  FROM " . $this->table_news . " AS a
						  WHERE published = 1 LIMIT 2
						  ";
		global $db;
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}

	function get_info_categories($id_category)
	{
		if (!$id_category)
			return;
		$query = " SELECT id,name,alias,summary
							FROM " . $this->table_products_categories . " AS a
							WHERE published = 1 AND id = $id_category
							";
		global $db;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}
	function getBannerFooter()
	{
		$Itemid = FSInput::get('Itemid', 1, 'int');

		$where = '';
		$where .= " AND (listItemid = 'all'
					OR listItemid like '%,$Itemid,%')
					";
		$query = " SELECT id,name,alias, image, link
						FROM " . $this->table_name . " AS a
						WHERE published = 1 AND category_id = 16 " . $where . "";

		global $db;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}

	function get_news2()
	{
		$query = " SELECT id,title,alias,category_id,category_name,category_alias
						  FROM " . $this->table_news . " AS a
						  WHERE published = 1 AND show_in_homepage = 1 order by id desc LIMIT 6
						  ";
		global $db;
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}
	function get_image_info($image_id, $cat, $itemId)
	{
		$query = " SELECT *
		FROM " . $this->table_name . " AS a
		WHERE published = 1 AND id = $image_id and products_categories like '%," . $cat . ",%' and listItemId like '%," . $itemId . ",%'";
		// echo $query;die;
		global $db;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}
}
