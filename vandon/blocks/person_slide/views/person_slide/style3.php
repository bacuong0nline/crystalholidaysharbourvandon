<?php
global $tmpl;
$tmpl->addScript('default', 'blocks/person_slide/assets/js', 'bottom');
?>
<div class="<?php echo @$background == 1 ? 'snowflake1' : 'snowflake2' ?>" style="
		background-image: url(); 
		width: 100%; background-size: 100%;
		background-repeat: no-repeat;
		background-position: center center;
		position: relative;
		padding-bottom: 150px !important;
		">
    <div class="container pb-3 snowflake1-box">
        <div class="pb-3">
            <p class="text-uppercase text-center title-block-1" data-aos="fade-right"><?php echo @$params['title1']  ?></p>
            <h5 class="title text-center text-uppercase title-block" data-aos="fade-left"><?php echo @$params['title'] ?></h5>
            <div class="col-12 d-flex flex-column align-items-center">
                <?php if (@$params['title2']) { ?>
                    <p class="text-center font-weight-strong title-block-2"><?php echo @$params['title2'] ?></p>
                <?php } ?>
                <?php if (@$params['summary2']) { ?>
                    <p class="text-center summary2" style="width: 70%"><?php echo @$params['summary2'] ?></p>
                <?php } ?>
            </div>
        </div>
        <div class="container">
            <div class="lecturers owl-theme owl-carousel">
                <?php foreach ($list_ranking as $item) { ?>
                    <div class="adviser-box" style="height: 500px !important">
                        <div>
                            <img style="width: 100%; height: 276px; object-fit: cover" src="<?php echo $item->image ?>" alt="">
                        </div>
                        <div class="description">
                            <p class="text-uppercase font-weight-strong text-center"><?php echo $item->title ?></p>
                            <p class="text-center" style="color: #767676"><?php echo FSText::_("Học viên tại VMG English") ?></p>
                            <p class="text-center mb-0" style="color: #767676"><?php echo FSText::_("Điểm thi IELTS: ") ?></p>
                            <p class="text-center" style="font-weight: 600"><?php echo $item->ielts ?></p>
                            <p class="text-center mb-0" style="color: #767676"><?php echo FSText::_("Điểm thi TOEIC: ") ?></p>
                            <p class="text-center" style="font-weight: 600"><?php echo $item->toeic ?></p>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

    </div>
    <?php if (@$background == 1) { ?>
        <img class="wave2" src="./images/wave2.png" alt="wave2" />
    <?php } else { ?>
        <img class="wave1" src="./images/wave1.png" alt="wave1" />
    <?php } ?>
</div>