<?php
global $tmpl;
$tmpl->addScript('default', 'blocks/person_slide/assets/js', 'bottom');
?>
<div class="lecturers-wrapper" style="background-size: 40%; background-repeat: no-repeat; background-position: center center; position: relative;">
    <div class="container pb-3">
        <h2 class="text-center title-recruitment" style="padding-bottom: 40px;" data-aos="fade-right"><?php echo FSText::_("Đội ngũ lãnh đạo")  ?></h2>
        <h5 class="title text-center text-uppercase title-block" data-aos="fade-left"><?php echo @$params['title'] ?></h5>
        <div class="col-12 d-flex flex-column align-items-center">
            <?php if (@$params['title2']) { ?>
                <p class="text-center font-weight-strong title-block-2"><?php echo @$params['title2'] ?></p>
            <?php } ?>
            <?php if (@$params['summary2']) { ?>
                <p class="text-center summary2" style="width: 70%"><?php echo @$params['summary2'] ?></p>
            <?php } ?>
        </div>
    </div>
    <div class="container">
        <div class="lecturers owl-carousel owl-theme">
            <?php foreach ($list as $item) { ?>
                <div class="lecturers-box">
                    <div>
                        <img style="width: 100%;" src="<?php echo $item->image ?>" alt="">
                    </div>
                    <div class="description">
                        <p style="margin-top: 20px" class="text-uppercase fw-bold text-center"><?php echo $item->title ?></p>
                        <span class="d-flex justify-content-center">
                            <p class="text-center"><?php echo $item->experience ?></p>
                        </span>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>