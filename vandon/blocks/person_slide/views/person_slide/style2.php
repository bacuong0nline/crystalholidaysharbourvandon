<?php
global $tmpl;
$tmpl->addScript('default', 'blocks/person_slide/assets/js', 'bottom');
?>
<div class="<?php echo @$background == 1 ? 'snowflake1' : 'snowflake2' ?>" style="
		background-image: url(); 
		width: 100%; background-size: 100%;
		background-repeat: no-repeat;
		background-position: center center;
		position: relative;
		">
    <div class="container pb-3 snowflake1-box adviser-wrapper">
        <div class="pb-3">
            <p class="text-uppercase text-center title-block-1" data-aos="fade-right"><?php echo @$params['title1']  ?></p>
            <h5 class="title text-center text-uppercase title-block" data-aos="fade-left"><?php echo @$params['title'] ?></h5>
            <div class="col-12 d-flex flex-column align-items-center">
                <?php if (@$params['title2']) { ?>
                    <p class="text-center font-weight-strong title-block-2"><?php echo @$params['title2'] ?></p>
                <?php } ?>
                <?php if (@$params['summary2']) { ?>
                    <p class="text-center summary2" style="width: 70%"><?php echo @$params['summary2'] ?></p>
                <?php } ?>
            </div>
        </div>
        <div class="container">
            <div class="lecturers owl-theme owl-carousel">
                <?php foreach ($list_adviser as $item) { ?>
                    <div class="adviser-box">
                        <div>
                            <img style="width: 100%; height: 276px; object-fit: cover" src="<?php echo $item->image ?>" alt="">
                        </div>
                        <div class="description">
                            <p class="text-uppercase font-weight-strong text-center"><?php echo $item->title ?></p>
                            <span class="d-flex">
                                <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M15 5.25H3C2.17157 5.25 1.5 5.92157 1.5 6.75V14.25C1.5 15.0784 2.17157 15.75 3 15.75H15C15.8284 15.75 16.5 15.0784 16.5 14.25V6.75C16.5 5.92157 15.8284 5.25 15 5.25Z" stroke="#D1A53D" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round" />
                                    <path d="M12 15.75V3.75C12 3.35218 11.842 2.97064 11.5607 2.68934C11.2794 2.40804 10.8978 2.25 10.5 2.25H7.5C7.10218 2.25 6.72064 2.40804 6.43934 2.68934C6.15804 2.97064 6 3.35218 6 3.75V15.75" stroke="#D1A53D" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                                <p class="ml-2"><?php echo FSText::_("Chức vụ") ?>: <br /> <?php echo $item->position ?></p>
                            </span>
                            <span class="d-flex">
                                <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M16.5001 12.69V14.94C16.5009 15.1489 16.4581 15.3556 16.3745 15.547C16.2908 15.7384 16.168 15.9102 16.0141 16.0514C15.8602 16.1926 15.6785 16.3001 15.4806 16.367C15.2828 16.434 15.0731 16.4588 14.8651 16.44C12.5572 16.1892 10.3403 15.4006 8.39257 14.1375C6.58044 12.986 5.04407 11.4496 3.89257 9.63751C2.62506 7.68091 1.83625 5.45326 1.59007 3.13501C1.57133 2.92761 1.59598 2.71858 1.66245 2.52123C1.72892 2.32388 1.83575 2.14253 1.97615 1.98873C2.11654 1.83493 2.28743 1.71204 2.47792 1.6279C2.6684 1.54376 2.87433 1.50021 3.08257 1.50001H5.33257C5.69655 1.49643 6.04942 1.62532 6.32539 1.86266C6.60137 2.1 6.78163 2.4296 6.83257 2.79001C6.92754 3.51006 7.10366 4.21706 7.35757 4.89751C7.45848 5.16595 7.48032 5.4577 7.4205 5.73817C7.36069 6.01865 7.22172 6.27609 7.02007 6.48001L6.06757 7.43251C7.13524 9.31017 8.68991 10.8648 10.5676 11.9325L11.5201 10.98C11.724 10.7784 11.9814 10.6394 12.2619 10.5796C12.5424 10.5198 12.8341 10.5416 13.1026 10.6425C13.783 10.8964 14.49 11.0725 15.2101 11.1675C15.5744 11.2189 15.9071 11.4024 16.145 11.6831C16.3828 11.9638 16.5092 12.3222 16.5001 12.69Z" stroke="#D1A53D" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                                <span class="ml-2">
                                    <p class="mb-0"><?php echo FSText::_("Hotline") ?>:</p>
                                    <p><?php echo $item->hotline ?></p>
                                </span>
                            </span>
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>


    </div>
    <?php if (@$background == 1) { ?>
        <img class="wave2" src="./images/wave2.png" alt="wave2" />
    <?php } else { ?>
        <img class="wave1" src="./images/wave1.png" alt="wave1" />
    <?php } ?>
</div>