$(document).ready(function() {
    $('.lecturers').owlCarousel({
        loop:true,
        margin:33,
        nav:false,
        dots:false,
        autoplay: true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:4
            }
        }
    })
})