<?php
class Person_slideBModelsPerson_slide
{
	function __construct()
	{
		$fstable = FSFactory::getClass('fstable');
		$this->table_name_lecturers = $fstable->_('fs_lecturers', 1);
		$this->table_name_adviser = $fstable->_('fs_adviser', 1);
		$this->table_name_ranking = $fstable->_('fs_ranking', 1);

	}

	function get_list_lecturer()
	{
		$query = " SELECT *
		FROM " . $this->table_name_lecturers . " AS a
		where published = 1 order by ordering asc limit 20
		";
		global $db;
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}
	function get_list_adviser()
	{
		$query = " SELECT *
		FROM " . $this->table_name_adviser . " AS a
		where published = 1 order by ordering asc limit 20
		";
		global $db;
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}
	function get_list_ranking()
	{
		$query = " SELECT *
		FROM " . $this->table_name_ranking . " AS a
		where published = 1 order by ordering asc limit 20
		";
		global $db;
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}
	function getList2($products_categories)
	{
		$where = '';
		if (!$products_categories)
			return;
		$where .= " AND products_categories like '%,$products_categories,%' ";
		$module = FSInput::get('module');
		$view = FSInput::get('view');
		$ccode = FSInput::get('ccode');
		$filter = FSInput::get('filter');

		// Itemid
		$Itemid = FSInput::get('Itemid', 1, 'int');
		$where .= " AND (listItemid = 'all'
							OR listItemid like '%,$Itemid,%')
							";

		$query = ' SELECT name,id,category_id,type,image,flash,content,link,height,width,is_news
						  FROM ' . $this->table_name . ' AS a
						  WHERE published = 1
						 ' . $where . ' ORDER BY ordering, id ';
		// print_r($query);die;
		global $db;
		$db->query($query);
		$list = $db->getObjectList();
		// print_r($list);die;
		return $list;
	}
	function get_news()
	{
		$query = " SELECT id,title,alias,category_id,category_name,category_alias
						  FROM " . $this->table_news . " AS a
						  WHERE published = 1 LIMIT 2
						  ";
		global $db;
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}

	function get_info_categories($id_category)
	{
		if (!$id_category)
			return;
		$query = " SELECT id,name,alias,summary
							FROM " . $this->table_products_categories . " AS a
							WHERE published = 1 AND id = $id_category
							";
		global $db;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}
	function getBannerFooter()
	{
		$Itemid = FSInput::get('Itemid', 1, 'int');

		$where = '';
		$where .= " AND (listItemid = 'all'
					OR listItemid like '%,$Itemid,%')
					";
		$query = " SELECT id,name,alias, image, link
						FROM " . $this->table_name . " AS a
						WHERE published = 1 AND category_id = 16 " . $where . "";

		global $db;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}

	function get_news2()
	{
		$query = " SELECT id,title,alias,category_id,category_name,category_alias
						  FROM " . $this->table_news . " AS a
						  WHERE published = 1 AND show_in_homepage = 1 order by id desc LIMIT 6
						  ";
		global $db;
		$db->query($query);
		$result = $db->getObjectList();
		return $result;
	}
	function get_image_info($image_id, $cat, $itemId)
	{
		$query = " SELECT *
		FROM " . $this->table_name . " AS a
		WHERE published = 1 AND id = $image_id and products_categories like '%," . $cat . ",%' and listItemId like '%," . $itemId . ",%'";
		// echo $query;die;
		global $db;
		$db->query($query);
		$result = $db->getObject();
		return $result;
	}
}
