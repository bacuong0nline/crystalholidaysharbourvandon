<?php
$params = array(
	'suffix' => array(
		'name' => 'Hậu tố',
		'type' => 'text',
		'default' => '_contents2',
	),
	'title1' => array(
		'name' => 'Tiêu đề phụ 1',
		'type' => 'text',
		'default' => '',
	),
	'title' => array(
		'name' => 'Tiêu đề chính',
		'type' => 'text',
		'default' => '',
	),
	'title2' => array(
		'name' => 'Tiêu đề phụ 2',
		'type' => 'text',
		'default' => '',
	),
	'summary' => array(
		'name' => 'Tóm tắt',
		'type' => 'text',
		'default' => '',
	),
	'background' => array(
		'name' => 'Màu nền',
		'type' => 'select',
		'value' => array(
			1 => 'Trắng',
			2 => 'Xám'
		)
	),
	'style' => array(
		'name' => 'Style',
		'type' => 'select',
		'value' => array(
			'default' => 'Mặc định'
		)
	),

	// 'category_id' => array(
	// 	'name' => 'Nhóm chương trình đào tạo',
	// 	'class' => 'category-select',
	// 	'type' => 'select',
	// 	'value' => get_category(),
	// ),
	// 'image' => array(
	// 	'name' => 'Ảnh',
	// 	'class' => 'image-select',
	// 	'type' => 'select',
	// 	'value' => get_image(),
	// 	// 'attr' => array('multiple' => 'multiple'),
	// ),
);

?>