<?php
global $tmpl;
$tmpl->addStylesheet('styles', 'blocks/contact_bot/assets/css');
$Itemid = 7;
FSFactory::include_class('fsstring');
?>

<div class="<?php echo @$background == 1 ? 'snowflake1' : 'snowflake2' ?>" style="background-position: center center; position: relative; padding-bottom: 100px;">
    <div class="container pb-3 snowflake1-box">
        <div class="pb-3 snowflake1-box">
            <p class="text-uppercase text-center title-block-1"><?php echo $params['title1'] ?></p>
            <h5 class="title text-center text-uppercase mb-4">
                <?php echo $params['title'] ?>
            </h5>
            <p class="text-uppercase text-center"><?php echo $params['title2'] ?></p>
            <div class="d-flex justify-content-center">
                <p style="width: 70%" class="summary-central text-center grey--text">
                    <?php echo $params['summary'] ?>
                </p>
            </div>
        </div>
        <div class="container contents2-description">
            <?php echo $data->content ?>
        </div>
    </div>
    <?php if (@$background == 1) { ?>
        <img class="wave2" src="./images/wave2.png" alt="wave2" />
    <?php } else { ?>
        <img class="wave1" src="./images/wave1.png" alt="wave1" />
    <?php } ?>
</div>