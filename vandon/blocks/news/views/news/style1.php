<?php
global $tmpl;
$tmpl->addScript('default', 'blocks/news/assets/js');
?>


<div class="parent-wrapper <?php echo $background == 1 ? 'snowflake1' : 'snowflake2' ?>" style=" position: relative; padding-bottom: 100px;">
    <div class="container pb-3">
        <p class="text-uppercase text-center title-block-1" data-aos="fade-right"><?php echo @$params['title1']  ?></p>
        <h5 class="title text-center text-uppercase title-block" data-aos="fade-left"><?php echo @$params['title'] ?></h5>
        <div class="col-12 d-flex flex-column align-items-center">
            <?php if (@$params['title2']) { ?>
                <p class="text-center font-weight-strong title-block-2"><?php echo @$params['title2'] ?></p>
            <?php } ?>
            <?php if (@$params['summary']) { ?>
                <p class="text-center summary2 mb-0" style="width: 70%"><?php echo @$params['summary'] ?></p>
            <?php } ?>
        </div>
    </div>
    <div class="container pt-5 pb-3">
        <div class="owl-carousel owl-theme news">
            <?php foreach ($list_news as $val2) { ?>
                <?php echo $tmpl->news_item($val2) ?>
            <?php } ?>
        </div>
    </div>
    <div class="d-flex justify-content-center mt-3">
        <a href="<?php echo FSRoute::_("index.php?module=news&view=cat&cid=$cat->id&ccode=$cat->alias") ?>" style="width: inherit; height: inherit; font-weight: 500" class="text-uppercase btn-register-2 align-items-center p-3">
            <?php echo FSText::_("Xem tất cả") ?>
            &nbsp;&nbsp;&nbsp;
            <svg width="10" height="10" viewBox="0 0 10 10" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path d="M5 0L4.11562 0.884375L7.60625 4.375H0V5.625H7.60625L4.11562 9.11562L5 10L10 5L5 0Z" />
            </svg>
        </a>
    </div>
    <?php if ($background == 1) { ?>
        <img class="wave2" src="./images/wave2.png" alt="wave2" />
    <?php } else { ?>
        <img class="wave1" src="./images/wave1.png" alt="wave1" />
    <?php } ?>
</div>