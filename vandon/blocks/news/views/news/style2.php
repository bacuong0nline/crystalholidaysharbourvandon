<?php global $tmpl; ?>
<div class="parent-wrapper snowflake2" style="background-color: #F8F9F9; position: relative; padding-bottom: 100px;">
  <?php foreach ($list_cat_news as $item) { ?>
    <div class="container">
      <p class="text-uppercase text-center title-block-1" data-aos="fade-right"><?php echo @$params['title1']  ?></p>
      <h5 class="title text-center text-uppercase title-block" data-aos="fade-left"><?php echo @$params['title'] ?></h5>
      <div class="col-12 d-flex flex-column align-items-center">
        <?php if (@$params['title2']) { ?>
          <p class="text-center font-weight-strong title-block-2"><?php echo @$params['title2'] ?></p>
        <?php } ?>
        <?php if (@$params['summary']) { ?>
          <p class="text-center summary2 mb-0" style="width: 70%"><?php echo @$params['summary'] ?></p>
        <?php } ?>
      </div>
    </div>
    <ul class="col-12 d-flex align-items-start justify-content-center nav nav-tabs" role="tablist">
      <?php foreach ($item->cat_child as $key => $val) { ?>
        <li class="nav-item">
          <a style="height: inherit" class="nav-link btn-register-4 text-center align-items-center p-3 mr-2 <?php echo $key == 0 ? 'active' : '' ?>" id="<?php echo $val->alias ?>-tab" data-toggle="tab" href="#<?php echo $val->alias ?>" role="tab" aria-controls="<?php echo $val->alias ?>" aria-selected="false">
            <?php echo $val->name ?>
          </a>
        </li>
      <?php } ?>
    </ul>
    <div class="container pt-5 pb-3">
      <div class="tab-content">
        <?php foreach ($item->cat_child as $key => $val) { ?>
          <div class="tab-pane fade <?php echo $key == 0 ? 'active show' : '' ?>" id="<?php echo $val->alias ?>" role="tabpanel" aria-labelledby="<?php echo $val->alias ?>-tab">
            <div class="owl-carousel owl-theme news">
              <?php foreach ($val->news as $val2) { ?>
                <?php echo $tmpl->news_item($val2) ?>
              <?php } ?>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  <?php } ?>
  <img class="wave1" src="./images/wave1.png" alt="wave2" />
</div>