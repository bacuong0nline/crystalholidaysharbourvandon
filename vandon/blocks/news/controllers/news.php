<?php

/*
 * Huy write
 */
// models 
include 'blocks/news/models/news.php';

class NewsBControllersNews {

    function __construct() {
        
    }

    function display($parameters,$title,$id=null) {
        $ordering = $parameters->getParams('ordering');
        $type = $parameters->getParams('type');
        $limit = $parameters->getParams('limit');
        $limit = $limit ? $limit : 4;
        $style = $parameters->getParams('style');
        $cat = $parameters->getParams('category_id');
        // call models
        $params = $parameters->params;
        $model = new NewsBModelsNews();

        $list_news = $model->get_news($params['category_id']);

        $cat = $model->get_cat($params['category_id']);

        $item_id = FSInput::get('Itemid');
        if($item_id == 34){
            $str = $model->get_str_relate(FSInput::get('id'));
            if($str->news_related)
                $list_relate = $model->get_list_relate($str->news_related);
        }

        $list_cat_news = $model->get_list_cat($params['category_id']);
        foreach($list_cat_news as $cat) {
            $cat->cat_child = $model->get_list_child($params['category_id']);
			foreach($cat->cat_child as $item) {
				$item->news = $model->get_news($item->id);
			}
        }


		$background = $params['background'];

        // call views
        include 'blocks/news/views/news/' . $style . '.php';
    }

    function get_count($where = '', $table_name = '', $select = '*')
    {
        if (!$where)
            return;
        if (!$table_name)
            $table_name = $this->table_name;
        $query = ' SELECT count(' . $select . ')
					  FROM ' . $table_name . '
					  WHERE ' . $where;
        global $db;
        $sql = $db->query($query);
        $result = $db->getResult();
        return $result;
    }



}

?>