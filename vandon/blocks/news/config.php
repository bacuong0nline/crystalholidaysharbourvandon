<?php
$params = array(
    'suffix' => array(
        'name' => 'Hậu tố',
        'type' => 'text',
        'default' => '_news'
    ),
    // 'limit' => array(
    //     'name' => 'Giới hạn',
    //     'type' => 'text',
    //     'default' => '3'
    // ),
    'title1' => array(
        'name' => 'Tiêu đề phụ 1',
        'type' => 'text',
        'default' => ''
    ),
    'title' => array(
        'name' => 'Tiêu đề chính',
        'type' => 'text',
        'default' => ''
    ),
    'title2' => array(
        'name' => 'Tiêu đề phụ 2',
        'type' => 'text',
        'default' => ''
    ),
    'summary' => array(
        'name' => 'Tóm tắt',
        'type' => 'text',
        'default' => ''
    ),
	'background' => array(
		'name' => 'Màu nền',
		'type' => 'select',
		'value' => array(
			1 => 'Trắng',
			2 => 'Xám'
		)
	),
    // 'type' => array(
    //     'name' => 'Lấy theo',
    //     'type' => 'select',
    //     'value' => array(
    //         'ordering' => 'Mới nhất',
    //         'ramdom'=>'Ngẫu nhiên',
    //         'home' => 'Hiển thị trang chủ'
    //     ),
    // ),
    //'limit_video' => array(
//					'name' => 'Giới hạn video',
//					'type' => 'text',
//					'default' => '6'
//		),  			
    'style' => array(
        'name' => 'Style',
        'type' => 'select',
        'value' => array(
            'style1' => 'Kiểu 1',
            'style2' => 'Kiểu 2',

        )
    ),
    'category_id' => array(
        'name' => 'Nhóm danh mục',
        'type' => 'select',
        'value' => get_category(),
//        'attr' => array('multiple' => 'multiple'),
    ),

);

function get_category()
{
    global $db;
    $query = " SELECT *
						FROM fs_news_categories 
						WHERE published = 1 and level = 0
						";
    $sql = $db->query($query);
    $result = $db->getObjectList();
    if (!$result)
        return;
    $arr_group = array();
    foreach ($result as $item) { 
        $arr_group[$item->id] = $item->name;
    }
    return $arr_group;
}

?>