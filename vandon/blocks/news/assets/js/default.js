$(document).ready(function(){
    $('.news').owlCarousel({
        loop:true,
        margin:33,
        nav:true,
        autoplay: true,
        responsive:{
            0:{
                items:1.5,
                // autoplay: true
            },
            600:{
                items:4
            },
            1000:{
                items:4,
            }
        }
    })
});
