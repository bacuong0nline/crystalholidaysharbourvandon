<div class="<?php echo $background == 1 ? 'snowflake1' : 'snowflake2' ?>" style="width: 100%; background-size: 100%; background-repeat: no-repeat; background-position: center center; position: relative; ">
    <div class="container pb-3">
        <p class="text-uppercase text-center title-block-1" data-aos="fade-right"><?php echo @$params['title1']  ?></p>
        <h5 class="title text-center text-uppercase title-block" data-aos="fade-left"><?php echo @$params['title'] ?></h5>
        <div class="col-12 d-flex flex-column align-items-center">
            <?php if (@$params['title2']) { ?>
                <p class="text-center font-weight-strong title-block-2"><?php echo @$params['title2'] ?></p>
            <?php } ?>
            <?php if (@$params['summary']) { ?>
                <p class="text-center summary2 mb-0" style="width: 70%"><?php echo @$params['summary'] ?></p>
            <?php } ?>
        </div>
    </div>
    <div class="container d-flex recruitment-style-3">
        <div class="col-12 col-md-6 video-box">
            <?php foreach ($gallery as $key => $item) { ?>
                <div data-src="<?php echo $item->image?>" class="<?php echo $key > 0 ? 'd-none' : '' ?>">
                    <img style="width: 100%; min-height: 380px; object-fit: cover" src="<?php echo $item->image?>" alt="chi-nhanh" />
                    <h6 class="text-uppercase text-center font-weight-strong mt-3"><?php echo $category->title?></h6>
                </div>
            <?php } ?>
        </div>
        <a class="col-12 col-md-6 link" href="<?php echo FSRoute::_("index.php?module=address&view=address&Itemid=3&lang=vi")?>" class="col-md-6">
            <img src="./images/chi-nhanh-2.jpg" alt="chi-nhanh-2" />
            <h6 class="text-uppercase text-center font-weight-strong mt-3"><?php echo FSText::_("Chi nhánh VMG English")?></h6>
        </a>
    </div>
    <?php if (@$background == 1) { ?>
        <img class="wave2" src="./images/wave2.png" alt="wave2" />
    <?php } else { ?>
        <img class="wave1" src="./images/wave1.png" alt="wave1" />
    <?php } ?>
</div>

<script>
            window.onload = function(){
                let elements = document.getElementsByClassName('video-box');

                for (let item of elements) {
                    lightGallery(item, {
                    thumbnail: true,
                    animateThumb: false,
                    zoomFromOrigin: false,
                    allowMediaOverlap: true,
                    toggleThumb: true,
                    plugins: [lgThumbnail, lgVideo, lgZoom]
                    })
                }
            }

            </script>