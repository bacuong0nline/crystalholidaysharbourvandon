<?php
$params = array(
	// 'suffix' => array(
	// 	'name' => 'Hậu tố',
	// 	'type' => 'text',
	// 	'default' => '',
	// ),
	//		'id' => array(
	//					'name' => 'Id (dùng cho qcáo bên ngoài trang)',
	//					'type' => 'text',
	//					'default' => 'divAdLeft',
	//					'comment' => 'divAdLeft dùng cho bên trái, divAdRight cho bên phải'
	//					),
	'title1' => array(
		'name' => 'Tiêu đề phụ 1',
		'type' => 'text',
		'default' => '',
	),
	'title' => array(
		'name' => 'Tiêu đề chính',
		'type' => 'text',
		'default' => '',
	),
	'title2' => array(
		'name' => 'Tiêu đề phụ 2',
		'type' => 'text',
		'default' => '',
	),
	'summary' => array(
		'name' => 'Tóm tắt',
		'type' => 'text',
		'default' => '',
	),
	'background' => array(
		'name' => 'Màu nền',
		'type' => 'select',
		'value' => array(
			1 => 'Trắng',
			2 => 'Xám'
		)
	),
	'style' => array(
		'name' => 'Style',
		'type' => 'select',
		'value' => array(
			'style1' => 'Kiểu 1',
			'style2' => 'Kiểu 2',
			'style3' => 'Kiểu 3',
			// 'style4' => 'Kiểu 4',
			// 'vertical-adv' => 'Quảng cáo dọc'
		)
		),
		'category_id' => array(
			'name' => 'Nhóm thư viện ảnh',
			'class' => 'category-select',
			'type' => 'select',
			'value' => get_category(),
		),
);
function get_category()
{
	global $db;
	$fstable = FSFactory::getClass('fstable');
	$table_name = $fstable->_('fs_gallery', 1);

	$query = " SELECT title, id
						FROM $table_name
						";
	$sql = $db->query($query);
	$result = $db->getObjectList();
	if (!$result)
		return;
	$arr_group = array();
	foreach ($result as $item) {
		$arr_group[$item->id] = $item->title;
	}
	return $arr_group;
}
// function get_category()
// {
// 	global $db;
// 	$query = " SELECT name, id
// 						FROM fs_banners_categories
// 						";
// 	$sql = $db->query($query);
// 	$result = $db->getObjectList();
// 	if (!$result)
// 		return;
// 	$arr_group = array();
// 	foreach ($result as $item) {
// 		$arr_group[$item->id] = $item->name;
// 	}
// 	return $arr_group;
// }
function get_image()
{
	global $db;
	$query = " SELECT name, id, image
						FROM fs_banners
						";
	$sql = $db->query($query);
	$result = $db->getObjectList();
	if (!$result)
		return;
	$arr_group = array();
	foreach ($result as $item) {
		$arr_group[$item->id] = $item->name;
	}
	return $arr_group;
}
