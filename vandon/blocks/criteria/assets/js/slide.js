
var owl = $('#banner');
owl.owlCarousel({
    animateOut: 'animate__fadeOut',
    animateIn: 'animate__fadeIn',
    items: 1,
    lazyLoad: true,
    loop: true,
    nav: false,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
});
